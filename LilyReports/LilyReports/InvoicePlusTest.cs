﻿using DevExpress.XtraReports.UI;
using System.Drawing;

namespace LilyReports
{
    public partial class InvoicePlusTest : XtraReport
    {
        public InvoicePlusTest(bool NoBox)
        {
            InitializeComponent();
            if (NoBox) { xrLabel31.BackColor = Color.White; }
            else { xrLabel31.BackColor = Color.DimGray; }
        }

    }
}
