﻿using System.Windows;
using DevExpress.Xpf.Core;

namespace LilyReports
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            LoadGlobalTheme();
        }

        protected void LoadGlobalTheme()
        {
            ThemeManager.ApplicationThemeName = Theme.MetropolisLightName;
        }
    }
}
