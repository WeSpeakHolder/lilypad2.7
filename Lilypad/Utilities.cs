﻿using System;
using System.Data;
using System.Net.Security;
using System.Diagnostics;
using System.Collections;
using System.Configuration;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.EntityClient;
using Shell32;
using IWshRuntimeLibrary;
using System.IO;
using System.Text.RegularExpressions;
using System.IO.Compression;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Controls;
using System.Windows;
using System.Threading;
using System.Windows.Media;
using System.Reflection;
using System.Windows.Media.Animation;
using System.Linq;
using System.Management;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.Properties;
using MahApps.Metro;
using Lilypad.ViewModel;
using Lilypad.Model;
using MySql.Data.MySqlClient;
using NetFwTypeLib;
using System.Deployment.Application;

namespace Lilypad
{

    public class TimeoutWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest w = base.GetWebRequest(address);
            w.Timeout = 3 * 1000;
            return w;
        }
    }

    public static class LilypadAnimations
    {

        public async static void ElasticHoverEnter(this UIElement target, TimeSpan duration)
        {
            TranslateTransform trans = new TranslateTransform();
            target.RenderTransform = trans;

            double toval = -20;
            double fval = 20;

            if (target.GetType() == typeof(Button))
            {
                Button bt = (Button)target;
                toval = -bt.ActualHeight;
                fval = bt.ActualHeight;
            }
            if (target.GetType() == typeof(StackPanel))
            {
                StackPanel sp = (StackPanel)target;
                toval = -sp.ActualHeight;
                fval = sp.ActualHeight;
            }

            DoubleAnimation anim1 = new DoubleAnimation(toval, duration);
            DoubleAnimation anim2 = new DoubleAnimation(fval, 0, duration);

            QuadraticEase q1 = new QuadraticEase(); q1.EasingMode = EasingMode.EaseOut;
            anim1.EasingFunction = q1;

            trans.BeginAnimation(TranslateTransform.YProperty, anim1);
            await Task.Delay(duration);
            trans.BeginAnimation(TranslateTransform.YProperty, anim2);

        }

        public static void ElasticHoverLeave(this UIElement target, TimeSpan duration)
        {
            TranslateTransform trans = new TranslateTransform();
            target.RenderTransform = trans;

            double toval = -20;
            double fval = 20;

            if (target.GetType() == typeof(Button))
            {
                Button bt = (Button)target;
                toval = -bt.ActualHeight;
                fval = bt.ActualHeight;
            }
            if (target.GetType() == typeof(StackPanel))
            {
                StackPanel sp = (StackPanel)target;
                toval = -sp.ActualHeight;
                fval = sp.ActualHeight;
            }

            //   DoubleAnimation anim1 = new DoubleAnimation(toval, duration);
            DoubleAnimation anim2 = new DoubleAnimation(fval, duration);

            QuadraticEase q1 = new QuadraticEase(); q1.EasingMode = EasingMode.EaseOut;
            anim2.EasingFunction = q1;

            trans.BeginAnimation(TranslateTransform.YProperty, anim2);
            // trans.BeginAnimation(TranslateTransform.YProperty, anim2);


        }

        public static void SmoothOpacityFadeInOut(this StackPanel target, Double targetOpacity, TimeSpan duration)
        {
            DoubleAnimation anim1 = new DoubleAnimation(targetOpacity, duration);
            QuadraticEase quad1 = new QuadraticEase(); quad1.EasingMode = EasingMode.EaseIn;
            anim1.EasingFunction = quad1;
            target.Background.BeginAnimation(UIElement.OpacityProperty, anim1);
        }


    }



    public class TimeOutSocket
    {
        private static bool IsConnectionSuccessful = false;
        private static Exception socketexception;
        private static ManualResetEvent TimeoutObject = new ManualResetEvent(false);

        public static bool Connect(IPEndPoint remoteEndPoint, int timeoutMSec)
        {
            TimeoutObject.Reset();
            socketexception = null;

            string serverip = Convert.ToString(remoteEndPoint.Address);
            int serverport = remoteEndPoint.Port;
            TcpClient tcpclient = new TcpClient();

            tcpclient.BeginConnect(serverip, serverport,
                new AsyncCallback(CallBackMethod), tcpclient);

            if (TimeoutObject.WaitOne(timeoutMSec, false))
            {
                if (IsConnectionSuccessful)
                {
                    return true;
                }
                else
                {
                    return false;
                    throw socketexception;
                }
            }
            else
            {
                tcpclient.Close();
                return false;
            }
        }
        private static void CallBackMethod(IAsyncResult asyncresult)
        {
            try
            {
                IsConnectionSuccessful = false;
                TcpClient tcpclient = asyncresult.AsyncState as TcpClient;

                if (tcpclient.Client != null)
                {
                    tcpclient.EndConnect(asyncresult);
                    IsConnectionSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                IsConnectionSuccessful = false;
                socketexception = ex;
            }
            finally
            {
                TimeoutObject.Set();
            }
        }
    }

    static public class cEventHelper
    {
        static Dictionary<Type, List<FieldInfo>> dicEventFieldInfos = new Dictionary<Type, List<FieldInfo>>();

        static BindingFlags AllBindings
        {
            get { return BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static; }
        }

        static List<FieldInfo> GetTypeEventFields(Type t)
        {
            if (dicEventFieldInfos.ContainsKey(t))
                return dicEventFieldInfos[t];

            List<FieldInfo> lst = new List<FieldInfo>();
            BuildEventFields(t, lst);
            dicEventFieldInfos.Add(t, lst);
            return lst;
        }

        static void BuildEventFields(Type t, List<FieldInfo> lst)
        {

            // NEW version of this routine uses .GetEvents and then uses .DeclaringType
            // to get the correct ancestor type so that we can get the FieldInfo.
            foreach (EventInfo ei in t.GetEvents(AllBindings))
            {
                Type dt = ei.DeclaringType;
                FieldInfo fi = dt.GetField(ei.Name, AllBindings);
                if (fi != null)
                    lst.Add(fi);
            }

            // OLD version of the code - called itself recursively to get all fields
            // for 't' and ancestors and then tested each one to see if it's an EVENT
            // Much less efficient than the new code
            /*
                  foreach (FieldInfo fi in t.GetFields(AllBindings))
                  {
                    EventInfo ei = t.GetEvent(fi.Name, AllBindings);
                    if (ei != null)
                    {
                      lst.Add(fi);
                      Console.WriteLine(ei.Name);
                    }
                  }
                  if (t.BaseType != null)
                    BuildEventFields(t.BaseType, lst);*/
        }

        //--------------------------------------------------------------------------------
        static EventHandlerList GetStaticEventHandlerList(Type t, object obj)
        {
            MethodInfo mi = t.GetMethod("get_Events", AllBindings);
            return (EventHandlerList)mi.Invoke(obj, new object[] { });
        }

        //--------------------------------------------------------------------------------
        public static void RemoveAllEventHandlers(object obj) { RemoveEventHandler(obj, ""); }

        //--------------------------------------------------------------------------------
        public static void RemoveEventHandler(object obj, string EventName)
        {
            if (obj == null)
                return;

            Type t = obj.GetType();
            List<FieldInfo> event_fields = GetTypeEventFields(t);
            EventHandlerList static_event_handlers = null;

            foreach (FieldInfo fi in event_fields)
            {
                if (EventName != "" && string.Compare(EventName, fi.Name, true) != 0)
                    continue;

                // After hours and hours of research and trial and error, it turns out that
                // STATIC Events have to be treated differently from INSTANCE Events...
                if (fi.IsStatic)
                {
                    // STATIC EVENT
                    if (static_event_handlers == null)
                        static_event_handlers = GetStaticEventHandlerList(t, obj);

                    object idx = fi.GetValue(obj);
                    Delegate eh = static_event_handlers[idx];
                    if (eh == null)
                        continue;

                    Delegate[] dels = eh.GetInvocationList();
                    if (dels == null)
                        continue;

                    EventInfo ei = t.GetEvent(fi.Name, AllBindings);
                    foreach (Delegate del in dels)
                        ei.RemoveEventHandler(obj, del);
                }
                else
                {
                    // INSTANCE EVENT
                    EventInfo ei = t.GetEvent(fi.Name, AllBindings);
                    if (ei != null)
                    {
                        object val = fi.GetValue(obj);
                        Delegate mdel = (val as Delegate);
                        if (mdel != null)
                        {
                            foreach (Delegate del in mdel.GetInvocationList())
                                ei.RemoveEventHandler(obj, del);
                        }
                    }
                }
            }
        }

        //--------------------------------------------------------------------------------
    }

    public static class Toaster
    {
        public static void SavedChanges()
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            mvm.SaveChangesCommand.Execute(null);
        }

        public static void Loading()
        {
            Thread t = new Thread(() =>
            {
                MainWindow mw = new MainWindow();
                mw.Dispatcher.Invoke(() =>
                {
                    MainViewModel mvm = (MainViewModel)mw.DataContext;

                    mw.toastActual.FillColor = Brushes.DodgerBlue;
                    mw.toastActual.IsLoading = true;
                    mw.toastActual.IsError = false;
                    mvm.ToastMessage = "Loading...";
                    mvm.ToastSubMessage = "";
                    mvm.ChangesSaved = true;
                });
            });
            //  MainWindow mw = (MainWindow)App.Current.MainWindow;

        }

        public static void DismissLoading()
        {
            MainWindow mw = (MainWindow)App.Current.MainWindow;
            MainViewModel mvm = (MainViewModel)mw.DataContext;
            mvm.ChangesSaved = false;
        }

        public static void Loading(string msg, string submsg)
        {
            MainWindow mw = (MainWindow)App.Current.MainWindow;
            MainViewModel mvm = (MainViewModel)mw.DataContext;
            mw.toastActual.FillColor = Brushes.DodgerBlue;
            mw.toastActual.IsLoading = true;
            mw.toastActual.IsError = false;
            mvm.ToastMessage = msg;
            mvm.ToastSubMessage = submsg;
            mvm.ChangesSaved = true;
        }

        public static void Notification(string msg)
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            mvm.ToastMessage = msg;
            mvm.ToastSubMessage = "";
            mvm.NotifyCommand.Execute(null);
        }
        public static void Notification(string msg, string submsg)
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            mvm.ToastMessage = msg;
            mvm.ToastSubMessage = submsg;
            mvm.NotifyCommand.Execute(null);
        }
    }

    public static class TaFFFy
    {

        public static bool RunFileConsistencyCheck_Hopper()
        {
            bool completed;
            List<hopperorder> hopperOrders = new List<hopperorder>();
            List<string> hopperPaths = new List<string>();
            List<string> hopperNums = new List<string>();

            if ((Directory.GetDirectories(Properties.Settings.Default.Data_ProductionFolderPath).Count()) > 0)
            {
                hopperPaths = Directory.GetDirectories(Properties.Settings.Default.Data_ProductionFolderPath).ToList();
            }

            using (Entities ctx = new Entities())
            {
                hopperOrders = (from k in ctx.hopperorders select k).ToList();
                var hn = (from t in hopperOrders select t.ReceiptNum).ToList();
                hopperNums = hn.Select(i => i.ToString()).ToList();
            }

            for (int i = 0; i < hopperOrders.Count(); i++)
            {
                try
                {
                    bool NeedsPathRefresh = false;

                    hopperorder thisPU = (hopperorder)hopperOrders[i];

                    List<designdetail> designDetails = new List<designdetail>();

                    using (Entities ctx = new Entities())
                    {
                        hopperorder editable = (from g in ctx.hopperorders where g.ReceiptNum == thisPU.ReceiptNum select g).FirstOrDefault();
                        string thisPath;
                        if (hopperPaths.Any(x => x.Contains(thisPU.ReceiptNum.ToString())))
                        {
                            thisPath = hopperPaths.Where(x => x.Contains(thisPU.ReceiptNum.ToString())).First();

                            if (editable.JobFolderPath != thisPath)
                            {
                                NeedsPathRefresh = true;
                                editable.JobFolderPath = thisPath;
                                thisPU.JobFolderPath = thisPath;
                                ctx.SaveChanges();
                            }
                        }
                        // Check if the actual path is different from the MySQL entry.

                        designDetails = (from j in ctx.designdetails where j.ReceiptNum == thisPU.ReceiptNum select j).ToList();
                    }

                    for (int iv = 0; iv < designDetails.Count(); iv++)
                    {
                        try
                        {
                            designdetail thisDD = (designdetail)designDetails[iv];

                            using (Entities ctx = new Entities())
                            {
                                designdetail editableDD = (from t in ctx.designdetails where t.DesignID == thisDD.DesignID select t).First();

                                if (NeedsPathRefresh)
                                {
                                    string curPath = editableDD.DesignFolderPath.ToString();
                                    string desName = curPath.Substring(curPath.LastIndexOf('\\') + 1);
                                    string newPath = Path.Combine(thisPU.JobFolderPath, desName);
                                    editableDD.DesignFolderPath = newPath;
                                    ctx.SaveChanges();
                                }

                            }

                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                            completed = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    new Error(ex);
                    completed = false;
                }
            }

            completed = true;
            return completed;
        }

        public static bool RunStaleOrderCleanup_Hopper()
        {
            bool allok = true;

            List<hopperorder> pickedUporders = new List<hopperorder>();
            List<hopperorder> completedOrders = new List<hopperorder>();

            using (Entities ctx = new Entities())
            {
                pickedUporders = (from k in ctx.hopperorders where k.Status == "Picked Up" select k).ToList();
            }

            for (int i = 0; i < pickedUporders.Count(); i++)
            {
                try
                {
                    hopperorder thisPU = (hopperorder)pickedUporders[i];
                    Utilities.MarkHopperOrderPickedUp(thisPU.ReceiptNum);
                }
                catch (Exception ex)
                {
                    new Error(ex);
                    allok = false;
                }
            }

            return allok;
        }
    }

    public static class StringExtension
    {
        public static string GetLast(this string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }

        public static string GetTextBetween(this string str, string startTag, string endTag, bool inclusive)
        {
            string rtn = null;

            int s = str.IndexOf(startTag);
            if (s >= 0)
            {
                if (!inclusive)
                    s += startTag.Length;

                int e = str.IndexOf(endTag, s);
                if (e > s)
                {
                    if (inclusive)
                        e += startTag.Length;

                    rtn = str.Substring(s, e - s);
                }
            }

            return rtn;
        }
    }

    public sealed class RunWithTimeout<TResult>
    {
        readonly TimeSpan _timeout;

        public RunWithTimeout(TimeSpan timeout)
        {
            _timeout = timeout;
        }

        public TResult Run(Func<TResult> function)
        {
            if (function == null) throw new ArgumentNullException("function");

            var sync = new object();
            var isCompleted = false;

            WaitCallback watcher = obj =>
            {
                var watchedThread = obj as Thread;

                lock (sync)
                {
                    if (!isCompleted)
                    {
                        Monitor.Wait(sync, _timeout);
                    }
                }
                // CAUTION: the call to Abort() can be blocking in rare situations
                // http://msdn.microsoft.com/en-us/library/ty8d3wta.aspx
                // Hence, it should not be called with the 'lock' as it could deadlock
                // with the 'finally' block below.

                if (!isCompleted)
                {
                    watchedThread.Abort();
                }
            };

            try
            {
                ThreadPool.QueueUserWorkItem(watcher, Thread.CurrentThread);
                return function();
            }
            catch (ThreadAbortException)
            {
                // This is our own exception.
                Thread.ResetAbort();

                throw new TimeoutException(string.Format("The operation has timed out after {0}.", _timeout));
            }
            finally
            {
                lock (sync)
                {
                    isCompleted = true;
                    Monitor.Pulse(sync);
                }
            }
        }

        public static TResult Run(TimeSpan timeout, Func<TResult> function)
        {
            return new RunWithTimeout<TResult>(timeout).Run(function);
        }
    }

    static class Utilities
    {

        public static async Task<string> UploadErrorReportToCloud(Exception ex)
        {
            var baseURL = @"http://www.skyislandsoftware.com/errorReport.php";
            var mip = "";
            var os = "";
            var uptime = "";
            var xclass = "";
            var msg = "";
            var sT = "";
            var EFrogger = (((MainViewModel)Application.Current.MainWindow.DataContext).LoggedInFrogger.FrogFirstName);

            xclass = ex.GetType().ToString();
            msg = ex.Message;

            if (ex.StackTrace != null)
            {
                sT += ex.StackTrace;
            }
            else
            {
                sT += "STACKTRACE_NULL";
            }

            var params1 = @"?storeid=" + Settings.Default.Store_ID + "&storeip=ip&employee=" + EFrogger + "&datetime=" +
                          DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "&machinename=" + Environment.MachineName +
                          "&machineip=" + mip + "&machineos=" + os + "&machineuptime=" + uptime + "&exclass=" + xclass +
                          "&exmessage=" + msg + "&extrace=" + sT;
            var outputURL = baseURL + params1;

            var wc = new WebClient();

            var req = new Uri(outputURL.LimitTo(2040));
            wc.UploadStringCompleted += wc_UploadStringCompleted;


            try
            {
                return await wc.UploadStringTaskAsync(req, "POST", "");
            }
            catch (Exception exx)
            {
                new SilentError(exx);
                return "Error";
            }
        }

        private static void wc_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {

        }

        private static readonly Random Random = new Random();

        /// <summary>
        /// Generate passwords
        /// </summary>
        /// <param name="passwordLength"></param>
        /// <param name="specCharacters"> </param>
        /// <returns></returns>
        public static string PasswordGenerator(int passwordLength, bool specCharacters)
        {
            int seed = Random.Next(1, int.MaxValue);
            //const string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            const string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            const string specialCharacters = @"!#$%()*+-/:;<=>?@[\]_";

            var chars = new char[passwordLength];
            var rd = new Random(seed);

            for (var i = 0; i < passwordLength; i++)
            {
                // If we are to use special characters
                if (specCharacters && i % Random.Next(3, passwordLength) == 0)
                {
                    chars[i] = specialCharacters[rd.Next(0, specialCharacters.Length)];
                }
                else
                {
                    chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
                }
            }

            return new string(chars);
        }

        public static LilyReports.InvoicePlusTest GetProductionSheet(hopperorder order)
        {
            DataSet setL = GetProductionDataSetHopperOrder(order.ReceiptNum);
            var bin = setL.Tables[0].Rows[0]["BoxNum"].ToString();
            LilyReports.InvoicePlusTest invoice;
            if (bin == "NBY" || bin == "") { invoice = new LilyReports.InvoicePlusTest(true); }
            else { invoice = new LilyReports.InvoicePlusTest(false); }
            invoice.DataSource = setL;
            return invoice;
            //   string PDFPath = CompletedOrder.JobFolderPath + "\\ProductionSheet.pdf";
            //   string jpgPath = CompletedOrder.JobFolderPath + "\\ProductionSheet.jpg";
            //   invoice.ExportToPdf(PDFPath);
        }

        public static bool CheckIfColumnExists(string table, string column)
        {
            string q = @"SHOW COLUMNS FROM " + table + " LIKE '" + column + "';";
            bool res = RunMySQLQuery(q) != string.Empty;
            return res;
        }

        public static bool CheckIfTableExists(string table)
        {
            string q = @"SHOW TABLES LIKE '" + table + "';";
            bool res = RunMySQLQuery(q) != string.Empty;
            return res;
        }

        public static bool ExportLilypadOptionsToLocalFile()
        {
            var d = Properties.Settings.Default;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("--- Begin Lilypad Server Config File ---");
            sb.AppendLine("-x-AUTOMATION-x-");
            sb.AppendLine("s:" + "AutoPrintPSheets;v:" + d.Auto_PrintInvoicesAfterOrder);
            sb.AppendLine("s:" + "DefaultSizeToItem;v:" + d.Pref_DefaultSizeToGarment);
            sb.AppendLine("-x-BUSINESS-x-");
            sb.AppendLine("s:" + "StoreAddress1;v:" + d.Store_Address1);
            sb.AppendLine("s:" + "StoreAddress2;v:" + d.Store_Address2);
            sb.AppendLine("s:" + "StoreCity;v:" + d.Store_City);
            sb.AppendLine("s:" + "StoreEmail;v:" + d.Store_Email);
            sb.AppendLine("s:" + "StoreHoursMF;v:" + d.Store_HoursMF);
            sb.AppendLine("s:" + "StoreHoursSat;v:" + d.Store_HoursSat);
            sb.AppendLine("s:" + "StoreHoursSun;v:" + d.Store_HoursSun);
            sb.AppendLine("s:" + "StoreID;v:" + d.Store_ID);
            sb.AppendLine("s:" + "StoreName;v:" + d.Store_Name);
            sb.AppendLine("s:" + "StoreOwner;v:" + d.Store_Owner);
            sb.AppendLine("s:" + "StorePhone;v:" + d.Store_Phone);
            sb.AppendLine("s:" + "StoreState;v:" + d.Store_State);
            sb.AppendLine("s:" + "StoreZip;v:" + d.Store_Zip);
            sb.AppendLine("-x-DATABASES-x-");
            sb.AppendLine("s:" + "MySQLUser;v:" + d.MySQL_UserName);
            sb.AppendLine("s:" + "MySQLPassword;v:" + d.MySQL_Password);
            sb.AppendLine("s:" + "MySQLIP;v:" + d.MySQL_ServerIP);
            sb.AppendLine("s:" + "RecordThreshold;v:" + d.Data_RecordSelectThreshold);
            sb.AppendLine("s:" + "IllustratorEnabled;v:" + d.System_IllustratorEnabled);
            sb.AppendLine("s:" + "PathAlphaData;v:" + d.Data_AlphaData);
            sb.AppendLine("s:" + "PathArchive;v:" + d.Data_ArchiveFolderPath);
            sb.AppendLine("s:" + "PathPending;v:" + d.Data_PendingFolderPath);
            sb.AppendLine("s:" + "PathProduction;v:" + d.Data_ProductionFolderPath);
            sb.AppendLine("s:" + "PathResource;v:" + d.Data_ResourcePath);
            sb.AppendLine("s:" + "PathTemplates;v:" + d.Data_TemplatesPath);
            sb.AppendLine("-x-INTEROP-x-");
            sb.AppendLine("s:" + "EmailUsername;v:" + d.Interop_Email_Username);
            sb.AppendLine("s:" + "EmailPassword;v:" + d.Interop_Email_Password);
            sb.AppendLine("s:" + "EmailSendFrom;v:" + d.Interop_Email_From);
            sb.AppendLine("s:" + "EmailUseGmail;v:" + d.Interop_UseGmail);
            sb.AppendLine("s:" + "EmailSMTPServer;v:" + d.Interop_SMTPServer);
            sb.AppendLine("s:" + "EmailSMTPPort;v:" + d.Interop_SMTPPort);
            sb.AppendLine("s:" + "EmailSMTPSSL;v:" + d.Interop_SMTPUseSSL);
            sb.AppendLine("--- End Lilypad Server Config File ---");
            string lilyPath = ((new DirectoryInfo(Properties.Settings.Default.Data_TemplatesPath).Parent).FullName);
            string outputPath = Path.Combine(lilyPath, "config.txt");
            try
            {
                if (System.IO.File.Exists(outputPath)) System.IO.File.Delete(outputPath);
                System.IO.File.AppendAllText(outputPath, sb.ToString());
                return true;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return false;
            }
        }

        public static bool ImportLilypadOptionsFromFile(string path)
        {
            var d = Properties.Settings.Default;
            StringBuilder sb = new StringBuilder();
            string[] lines = System.IO.File.ReadAllLines(path);
            string[] vs = new string[] {";v:"};
            foreach (string s in lines)
            {
                if (s.Contains("AutoPrintPSheets") && !s.Trim().EndsWith("v:")) { d.Auto_PrintInvoicesAfterOrder = bool.Parse((s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1])); }
                if (s.Contains("DefaultSizeToItem") && !s.Trim().EndsWith("v:")) { d.Pref_DefaultSizeToGarment = bool.Parse((s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1])); }
                if (s.Contains("StoreAddress1") && !s.Trim().EndsWith("v:")) { d.Store_Address1 = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("StoreAddress2") && !s.Trim().EndsWith("v:")) { d.Store_Address2 = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("StoreCity") && !s.Trim().EndsWith("v:")) { d.Store_City = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("StoreEmail") && !s.Trim().EndsWith("v:")) { d.Store_Email = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("StoreHoursMF") && !s.Trim().EndsWith("v:")) { d.Store_HoursMF = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("StoreHoursSat") && !s.Trim().EndsWith("v:")) { d.Store_HoursSat = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("StoreHoursSun") && !s.Trim().EndsWith("v:")) { d.Store_HoursSun = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("StoreID") && !s.Trim().EndsWith("v:")) { d.Store_ID = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("StoreName") && !s.Trim().EndsWith("v:")) { d.Store_Name = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("StoreOwner") && !s.Trim().EndsWith("v:")) { d.Store_Owner = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("StorePhone") && !s.Trim().EndsWith("v:")) { d.Store_Phone = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("StoreState") && !s.Trim().EndsWith("v:")) { d.Store_State = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("StoreZIP") && !s.Trim().EndsWith("v:")) { d.Store_Zip = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("MySQLUser") && !s.Trim().EndsWith("v:")) { d.MySQL_UserName = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("MySQLPassword") && !s.Trim().EndsWith("v:")) { d.MySQL_Password = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("MySQLIP") && !s.Trim().EndsWith("v:")) { d.MySQL_ServerIP = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("RecordThreshold") && !s.Trim().EndsWith("v:")) { d.Data_RecordSelectThreshold = int.Parse(s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]); }
                if (s.Contains("IllustratorEnabled") && !s.Trim().EndsWith("v:")) { d.System_IllustratorEnabled = bool.Parse(s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]); }
                if (s.Contains("PathAlphaData") && !s.Trim().EndsWith("v:")) { d.Data_AlphaData = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("PathArchive") && !s.Trim().EndsWith("v:")) { d.Data_ArchiveFolderPath = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("PathPending") && !s.Trim().EndsWith("v:")) { d.Data_PendingFolderPath = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("PathProduction") && !s.Trim().EndsWith("v:")) { d.Data_ProductionFolderPath = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("PathResource") && !s.Trim().EndsWith("v:")) { d.Data_ResourcePath = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("PathTemplates") && !s.Trim().EndsWith("v:")) { d.Data_TemplatesPath = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("EmailUsername") && !s.Trim().EndsWith("v:")) { d.Interop_Email_Username = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("EmailPassword") && !s.Trim().EndsWith("v:")) { d.Interop_Email_Password = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("EmailSendFrom") && !s.Trim().EndsWith("v:")) { d.Interop_Email_From = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("EmailUseGmail") && !s.Trim().EndsWith("v:")) { d.Interop_UseGmail = bool.Parse(s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]); }
                if (s.Contains("EmailSMTPServer") && !s.Trim().EndsWith("v:")) { d.Interop_SMTPServer = s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]; }
                if (s.Contains("EmailSMTPPort") && !s.Trim().EndsWith("v:")) { d.Interop_SMTPPort = int.Parse(s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]); }
                if (s.Contains("EmailSMTPSSL") && !s.Trim().EndsWith("v:")) { d.Interop_SMTPUseSSL = bool.Parse(s.Split(vs, StringSplitOptions.RemoveEmptyEntries)[1]); }
            }
            try
            {
                d.Save();
                return true;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return false;
            }

        }

        public static string RunMySQLQuery(string query)
        {
            var connS = ConfigurationManager.ConnectionStrings["Entities"].ConnectionString;
            var dbsb = new EntityConnectionStringBuilder(connS);
            string result = "";
            try
            {
                using (var conn = new MySqlConnection(dbsb.ProviderConnectionString))
                {
                    conn.Open();
                    MySqlCommand com = conn.CreateCommand();
                    com.CommandText = query;
                    MySqlDataReader rr = com.ExecuteReader();
                    if (rr.HasRows)
                    {
                        while (rr.Read())
                        {
                            result = rr.GetString(0);
                        }
                        conn.Close();
                        return result;
                    }
                    else
                    {
                        return string.Empty;
                    }

                }
            }
            catch (Exception ex)
            {
                new Error(ex);
                result = string.Empty;
                return result;
            }
        }

        private static IList<orderdetail> GetDetailLineItems(int receiptNo)
        {
            IList<orderdetail> intrnl;

            using (Entities ctx = new Entities())
            {
                intrnl = (from q in ctx.orderdetails where q.ReceiptNum == receiptNo select q).ToList();
            }

            return intrnl;
        }

        private static string RandomString(int size, bool lowerCase)
        {
            var builder = new StringBuilder();
            var random = new Random();
            char ch;
            for (var i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        public static string GetRandom3DigitCode()
        {
            var builder = new StringBuilder();
            builder.Append(RandomString(2, false));
            builder.Append(RandomNumber(1, 9));
            return builder.ToString();
        }

        public static string GenerateAlphaPONumber()
        {
            var mvm = (MainViewModel)Application.Current.MainWindow.DataContext;

            var datenow = DateTime.Now.Month.ToString("d2") + DateTime.Now.Day +
                          DateTime.Now.Year.ToString().Substring(2, 2);
            var froginitials = mvm.LoggedInFrogger.FrogInitials;
            var final = "L" + datenow + froginitials + "-" + GetRandom3DigitCode();
            return final;
        }

        private static DataSet GetProductionDataSetHopperOrder(int receiptNo)
        {
            string process;
            string designer;
            string priority;

            var mavm = (MainViewModel)App.Current.MainWindow.DataContext;

            customer thiscust;
            receipt thisrcpt;
            hopperorder CompletedOrder;

            using (Entities ctx = new Entities())
            {
                CompletedOrder = (from a in ctx.hopperorders where a.ReceiptNum == receiptNo select a).FirstOrDefault();
            }
            using (Entities ctx = new Entities())
            {
                process = (from q in ctx.printprocesses where q.ProcessID == CompletedOrder.PrintProcess select q.ProcessName).FirstOrDefault();
                designer = (from k in ctx.froggers where k.FrogID == CompletedOrder.DesignFrog select k.FrogFirstName).FirstOrDefault();
                priority = (from u in ctx.priorities where u.PriorityValue == CompletedOrder.Priority select u.PriorityAbbrev).FirstOrDefault();
                thiscust = (from i in ctx.customers where i.CustomerID == CompletedOrder.CustomerID select i).FirstOrDefault();
                thisrcpt = (from t in ctx.receipts where t.RcptNumber == CompletedOrder.ReceiptNum select t).FirstOrDefault();
            }

            DataTable table = new DataTable("order");
            DataColumn tableKeyColumn = new DataColumn("ReceiptNum", typeof(int));
            DataColumn tableCKeyColumn = new DataColumn("CustID", typeof(int));
            table.Columns.Add(tableKeyColumn);
            table.Columns.Add("FirstName", typeof(string));
            table.Columns.Add("LastName", typeof(string));
            table.Columns.Add("OrderDate", typeof(string));
            table.Columns.Add("DueDate", typeof(string));
            table.Columns.Add("PrintProcess", typeof(string));
            table.Columns.Add("Designer", typeof(string));
            table.Columns.Add("BoxNum", typeof(string));
            table.Columns.Add("Priority", typeof(string));
            table.Columns.Add("PmtStatus", typeof(string));
            table.Columns.Add(tableCKeyColumn);
            table.Columns.Add("Footer", typeof(string));
            table.Columns.Add("StoreInfo", typeof(string));

            // PAID POP Identifier And Logic.
            string pmtstat;
            decimal tendered;
            decimal total;
            bool result;

            tendered = thisrcpt.RcptTotalTendered;
            total = thisrcpt.RcptTotal;

            if (tendered < total) { result = false; }
            else { result = true; }
            if (result) { pmtstat = "PAID"; }
            else { pmtstat = "POP"; }

            string foot = Properties.Settings.Default.Invoices_Footer;
            var dueby = CompletedOrder.DueDate;
            var orddd = CompletedOrder.OrderDate;

            string dueStr = dueby.ToString("dddd, MMMM d") + " by " + dueby.ToString("h:mmtt");
            string ordStr = orddd.ToString("dddd, MMMM d") + " - " + orddd.ToString("h:mmtt");

            string sInfo = Properties.Settings.Default.Store_Phone.ToString() + Environment.NewLine;
            sInfo += Properties.Settings.Default.Store_Address1.ToString();
            if (!string.IsNullOrEmpty(Properties.Settings.Default.Store_Address2))
            {
                sInfo += ", " + Properties.Settings.Default.Store_Address2.ToString();
            }
            sInfo += Environment.NewLine;
            sInfo += Properties.Settings.Default.Store_City + ", " + Properties.Settings.Default.Store_State + " " + Properties.Settings.Default.Store_Zip;

            table.Rows.Add(CompletedOrder.ReceiptNum, CompletedOrder.CustFirst, CompletedOrder.CustLast, ordStr, dueStr, process, designer, CompletedOrder.BoxNum, priority, pmtstat, CompletedOrder.CustomerID, foot, sInfo);
            table.AcceptChanges();

            DataTable lines = new DataTable("lineitems");
            DataColumn lineKeyColumn = new DataColumn("ReceiptNum", typeof(int));
            lines.Columns.Add("DetailID", typeof(int));
            lines.Columns.Add(lineKeyColumn);
            lines.Columns.Add("StockID", typeof(string));
            lines.Columns.Add("Description", typeof(string));
            lines.Columns.Add("Quantity", typeof(int));
            lines.Columns.Add("Color", typeof(string));
            lines.Columns.Add("Size", typeof(string));
            lines.Columns.Add("LinePrice", typeof(decimal));
            lines.Columns.Add("LineExtend", typeof(decimal));

            DataTable receiptDT = new DataTable("receipt");
            DataColumn rcptKeyColumn = new DataColumn("ReceiptNum", typeof(int));
            receiptDT.Columns.Add(rcptKeyColumn);
            receiptDT.Columns.Add("SubTotal", typeof(decimal));
            receiptDT.Columns.Add("SalesTax", typeof(decimal));
            receiptDT.Columns.Add("Total", typeof(decimal));

            DataTable customerDT = new DataTable("customer");
            DataColumn custKeyColumn = new DataColumn("CustID", typeof(int));
            customerDT.Columns.Add(custKeyColumn);
            customerDT.Columns.Add("CustomerName", typeof(string));
            customerDT.Columns.Add("Company", typeof(string));
            customerDT.Columns.Add("Street1", typeof(string));
            customerDT.Columns.Add("Street2", typeof(string));
            customerDT.Columns.Add("Phone", typeof(string));
            customerDT.Columns.Add("Email", typeof(string));

            IList<orderdetail> localList = GetDetailLineItems(receiptNo);

            foreach (orderdetail deet in localList)
            {
                lines.Rows.Add(deet.DetailID, deet.ReceiptNum, deet.StockIDPOS, deet.Description, deet.Quantity, deet.Color, deet.Size, deet.LinePrice, deet.LineExtend);
            }

            string custname = thiscust.FirstName + " " + thiscust.LastName;
            string streetl2 = thiscust.City + " " + thiscust.State + " " + thiscust.Zip;

            customerDT.Rows.Add(thiscust.CustomerID, custname, thiscust.Company, thiscust.StreetAddress, streetl2, thiscust.Phone, thiscust.Email);
            receiptDT.Rows.Add(receiptNo, thisrcpt.RcptSTotal, thisrcpt.RcptTotalTax, thisrcpt.RcptTotal);

            DataSet set = new DataSet("Dataset");
            set.Tables.Add(table);
            set.Tables.Add(lines);
            set.Tables.Add(customerDT);
            set.Tables.Add(receiptDT);
            set.AcceptChanges();
            DataRelation relation = new DataRelation("line_relation", tableKeyColumn, lineKeyColumn);
            DataRelation relation2 = new DataRelation("line_relation2", tableCKeyColumn, custKeyColumn);
            DataRelation relation3 = new DataRelation("line_relation3", tableKeyColumn, rcptKeyColumn);
            set.Relations.Add(relation);
            set.Relations.Add(relation2);
            set.Relations.Add(relation3);

            return set;
        }

        public static string LoremIpsum(int minWords, int maxWords, int minSentences, int maxSentences, int numParagraphs)
        {

            var words = new[]{"lorem", "ipsum", "dolor", "sit", "amet", "consectetuer",
        "adipiscing", "elit", "sed", "diam", "nonummy", "nibh", "euismod",
        "tincidunt", "ut", "laoreet", "dolore", "magna", "aliquam", "erat"};

            int seed = Random.Next(1, maxSentences - minSentences);
            var rand = new Random(seed);
            int numSentences = rand.Next(maxSentences - minSentences)
                + minSentences + 1;
            int numWords = rand.Next(maxWords - minWords) + minWords + 1;

            string result = string.Empty;

            for (int p = 0; p < numParagraphs; p++)
            {
                result += Environment.NewLine;
                for (int s = 0; s < numSentences; s++)
                {
                    for (int w = 0; w < numWords; w++)
                    {
                        if (w > 0) { result += " "; }
                        result += words[rand.Next(words.Length)];
                    }
                    result += ". ";
                }
                result += Environment.NewLine;
            }

            return result;
        }

        public static string RemoveInvalidFilepathCharacters(string filename)
        {
            return Path.GetInvalidFileNameChars().Aggregate(filename, (current, c) => current.Replace(c.ToString(), string.Empty));
        }

        public static bool DBisOnline()
        {
            bool isValid;
            isValid = false;


            try
            {
                var connS =
                    System.Configuration.ConfigurationManager.ConnectionStrings["Entities"].ConnectionString.ToString();
                System.Data.EntityClient.EntityConnectionStringBuilder dbsb =
                    new System.Data.EntityClient.EntityConnectionStringBuilder(connS);
                using (
                    MySql.Data.MySqlClient.MySqlConnection conn =
                        new MySql.Data.MySqlClient.MySqlConnection(dbsb.ProviderConnectionString))
                {
                    Thread thread = new Thread(TryOpen);
                    ManualResetEvent mre = new ManualResetEvent(false);
                    thread.Start(new Tuple<MySql.Data.MySqlClient.MySqlConnection, ManualResetEvent>(conn, mre));
                    isValid = mre.WaitOne(2000);

                    if (!isValid)
                    {
                        thread.Abort();
                    }
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
                return false;
            }

            return isValid;
        }

        public static IPEndPoint GetIPEndPointFromHostName(string hostName, int port, bool throwIfMoreThanOneIP)
        {
            var addresses = System.Net.Dns.GetHostAddresses(hostName);
            if (addresses.Length == 0)
            {
                throw new ArgumentException(
                    "Unable to retrieve address from specified host name.",
                    "hostName"
                );
            }
            else if (throwIfMoreThanOneIP && addresses.Length > 1)
            {
                throw new ArgumentException(
                    "There is more that one IP address to the specified host.",
                    "hostName"
                );
            }
            return new IPEndPoint(addresses[0], port); // Port gets validated here.
        }


        public static bool TestSMTP(string server, int port)
        {
            bool res = false;
            IPEndPoint host = GetIPEndPointFromHostName(server, port, false);

            if (TimeOutSocket.Connect(host, 200))
            {
                res = true;
            }
            return res;
        }

        public static void InitialImportTemplates()
        {
            string lilyPath = ((new DirectoryInfo(Properties.Settings.Default.Data_TemplatesPath).Parent).FullName);
            string dlwssPath = Path.Combine(lilyPath, "Downloads", "temp.zip");
            using (var client = new WebClient())
            {
                TemporaryTemplatesPath = dlwssPath;
                client.DownloadFileCompleted += client_DownloadFileCompleted;
                client.DownloadFileAsync(new Uri("http://skyislandsoftware.com/lilypad/TemplatesFinal.zip"), dlwssPath);
            }
        }

        public static string TemporaryTemplatesPath { get; set; }

        static void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            RunDownloadComplete();
        }

        static async void RunDownloadComplete()
        {
            string outPath = TemporaryTemplatesPath;
            Ionic.Zip.ZipFile zf = new Ionic.Zip.ZipFile(outPath);
            zf.ExtractAll(Properties.Settings.Default.Data_TemplatesPath);
            zf.Dispose();
            await Task.Delay(100);
            try
            {
                System.IO.File.Delete(outPath);

                MessageBox.Show("Successfully installed template package.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Successfully installed template package but failed to delete temporary zip.");
            }
        }

        public static bool InitialFolderInstall(string path)
        {
            string lilyPath = Path.Combine(path, "Lilypad");
            string alphaPath = Path.Combine(lilyPath, "AlphaData");
            string dlwssPath = Path.Combine(lilyPath, "Downloads");
            string tmplsPath = Path.Combine(lilyPath, "Templates");
            string ordsMPath = Path.Combine(lilyPath, "Orders");
            string ordsAPath = Path.Combine(ordsMPath, "Archived");
            string ordsPPath = Path.Combine(ordsMPath, "Pending");
            string ordsRPath = Path.Combine(ordsMPath, "Production");
            try
            {
                if (!Directory.Exists(lilyPath))
                {
                    Directory.CreateDirectory(lilyPath);
                    QuickShareFolder(lilyPath, "Lilypad", "Lilypad Main Folder");
                    DirectorySecurity sec = Directory.GetAccessControl(lilyPath);
                    SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    sec.AddAccessRule(new FileSystemAccessRule(everyone,
                        FileSystemRights.FullControl | FileSystemRights.Synchronize,
                        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None,
                        AccessControlType.Allow));
                    Directory.SetAccessControl(lilyPath, sec);
                }
                if (!Directory.Exists(dlwssPath))
                {
                    Directory.CreateDirectory(dlwssPath);
                    DirectorySecurity sec = Directory.GetAccessControl(dlwssPath);
                    SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    sec.AddAccessRule(new FileSystemAccessRule(everyone,
                        FileSystemRights.FullControl | FileSystemRights.Synchronize,
                        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None,
                        AccessControlType.Allow));
                    Directory.SetAccessControl(dlwssPath, sec);
                }
                if (!Directory.Exists(alphaPath))
                {
                    Directory.CreateDirectory(alphaPath);
                    DirectorySecurity sec = Directory.GetAccessControl(alphaPath);
                    SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    sec.AddAccessRule(new FileSystemAccessRule(everyone,
                        FileSystemRights.FullControl | FileSystemRights.Synchronize,
                        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None,
                        AccessControlType.Allow));
                    Directory.SetAccessControl(alphaPath, sec);
                }
                if (!Directory.Exists(tmplsPath))
                {
                    Directory.CreateDirectory(tmplsPath);
                    DirectorySecurity sec = Directory.GetAccessControl(tmplsPath);
                    SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    sec.AddAccessRule(new FileSystemAccessRule(everyone,
                        FileSystemRights.FullControl | FileSystemRights.Synchronize,
                        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None,
                        AccessControlType.Allow));
                    Directory.SetAccessControl(tmplsPath, sec);
                }
                if (!Directory.Exists(ordsMPath))
                {
                    Directory.CreateDirectory(ordsMPath);
                    QuickShareFolder(ordsMPath, "Orders", "Lilypad Orders Folder");
                    DirectorySecurity sec = Directory.GetAccessControl(ordsMPath);
                    SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    sec.AddAccessRule(new FileSystemAccessRule(everyone,
                        FileSystemRights.FullControl | FileSystemRights.Synchronize,
                        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None,
                        AccessControlType.Allow));
                    Directory.SetAccessControl(ordsMPath, sec);
                }
                if (!Directory.Exists(ordsAPath))
                {
                    Directory.CreateDirectory(ordsAPath);
                    DirectorySecurity sec = Directory.GetAccessControl(ordsAPath);
                    SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    sec.AddAccessRule(new FileSystemAccessRule(everyone,
                        FileSystemRights.FullControl | FileSystemRights.Synchronize,
                        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None,
                        AccessControlType.Allow));
                    Directory.SetAccessControl(ordsAPath, sec);
                }
                if (!Directory.Exists(ordsPPath))
                {
                    Directory.CreateDirectory(ordsPPath);
                    DirectorySecurity sec = Directory.GetAccessControl(ordsPPath);
                    SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    sec.AddAccessRule(new FileSystemAccessRule(everyone,
                        FileSystemRights.FullControl | FileSystemRights.Synchronize,
                        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None,
                        AccessControlType.Allow));
                    Directory.SetAccessControl(ordsPPath, sec);
                }
                if (!Directory.Exists(ordsRPath))
                {
                    Directory.CreateDirectory(ordsRPath);
                    DirectorySecurity sec = Directory.GetAccessControl(ordsRPath);
                    SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    sec.AddAccessRule(new FileSystemAccessRule(everyone,
                        FileSystemRights.FullControl | FileSystemRights.Synchronize,
                        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None,
                        AccessControlType.Allow));
                    Directory.SetAccessControl(ordsRPath, sec);
                }

                Properties.Settings.Default.Data_ArchiveFolderPath = ordsAPath;
                Properties.Settings.Default.Data_PendingFolderPath = ordsPPath;
                Properties.Settings.Default.Data_ProductionFolderPath = ordsRPath;
                Properties.Settings.Default.Data_TemplatesPath = tmplsPath;
                Properties.Settings.Default.Data_AlphaData = alphaPath;
                Properties.Settings.Default.Save();

                InitialImportTemplates();

                return false;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return false;
            }
        }

        private static void QuickShareFolder(string FolderPath, string ShareName, string Description)
        {
            try
            {
                // Create a ManagementClass object

                ManagementClass managementClass = new ManagementClass("Win32_Share");

                // Create ManagementBaseObjects for in and out parameters

                ManagementBaseObject inParams = managementClass.GetMethodParameters("Create");
                ManagementBaseObject outParams;

                // Set the input parameters

                inParams["Description"] = Description;
                inParams["Name"] = ShareName;
                inParams["Path"] = FolderPath;
                inParams["Type"] = 0x0; // Disk Drive
                inParams["Access"] = null;

                outParams = managementClass.InvokeMethod("Create", inParams, null);

                NTAccount ntAccount = new NTAccount("Everyone");
                SecurityIdentifier userSID = (SecurityIdentifier)ntAccount.Translate(typeof(SecurityIdentifier));
                byte[] utenteSIDArray = new byte[userSID.BinaryLength];
                userSID.GetBinaryForm(utenteSIDArray, 0);

                //Trustee
                ManagementObject userTrustee = new ManagementClass(new ManagementPath("Win32_Trustee"), null);
                userTrustee["Name"] = "Everyone";
                userTrustee["SID"] = utenteSIDArray;

                //ACE
                ManagementObject userACE = new ManagementClass(new ManagementPath("Win32_Ace"), null);
                userACE["AccessMask"] = 2032127;                                 //Full access
                userACE["AceFlags"] = AceFlags.ObjectInherit | AceFlags.ContainerInherit;
                userACE["AceType"] = AceType.AccessAllowed;
                userACE["Trustee"] = userTrustee;

                ManagementObject userSecurityDescriptor = new ManagementClass(new ManagementPath("Win32_SecurityDescriptor"), null);
                userSecurityDescriptor["ControlFlags"] = 4; //SE_DACL_PRESENT
                userSecurityDescriptor["DACL"] = new object[] { userACE };

                //UPGRADE SECURITY PERMISSION
                ManagementClass mc = new ManagementClass("Win32_Share");
                ManagementObject share = new ManagementObject(mc.Path + ".Name='" + ShareName + "'");
                share.InvokeMethod("SetShareInfo", new object[] { Int32.MaxValue, Description, userSecurityDescriptor });
                // Check to see if the method invocation was successful

                if ((uint)(outParams.Properties["ReturnValue"].Value) != 0)
                {
                    throw new Exception("Unable to share directory.");
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "error!");
            }
        }

        public static bool InitialDatabaseInstall()
        {
            UpdateConfig();
            bool result = false;
            byte[] gzip = Properties.Resources.lilypadblank_sql;
            string extractedSql;
            string testSql = "USE lilypad;";

            using (GZipStream stream = new GZipStream(new MemoryStream(gzip), CompressionMode.Decompress))
            {
                using (var reader = new StreamReader(stream))
                {
                    extractedSql = reader.ReadToEnd();
                }
            }

            var connS = System.Configuration.ConfigurationManager.ConnectionStrings["Entities"].ConnectionString.ToString();
            System.Data.EntityClient.EntityConnectionStringBuilder dbsb = new System.Data.EntityClient.EntityConnectionStringBuilder(connS);
            MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection(dbsb.ProviderConnectionString);

            try
            {
                conn.Open();
            }
            catch
            {
                string conn2 = @"server=" + Properties.Settings.Default.MySQL_ServerIP + ";user id=" + Properties.Settings.Default.MySQL_UserName + ";password=" + Properties.Settings.Default.MySQL_Password + ";";
                MySql.Data.MySqlClient.MySqlConnection dbb2 = new MySql.Data.MySqlClient.MySqlConnection(conn2);
                dbb2.Open();
                MySql.Data.MySqlClient.MySqlCommand com = new MySql.Data.MySqlClient.MySqlCommand(extractedSql, dbb2);
                com.CommandTimeout = 0;
                try
                {
                    com.ExecuteNonQuery();
                    result = true;
                }
                catch (Exception exx)
                {
                    new Error(exx);
                    result = false;
                }

                dbb2.Close();
            }

            conn.Close();
            InstallUpdateSQLScript();

            return result;
        }

        public static bool TriedOnce;

        public static void InstallUpdateSQLScript()
        {
            string query = Properties.Resources.Installbugfix20.ToString();
            string query2 = Properties.Resources.Installbugfix202.ToString();
            string q2 = Properties.Resources.Install_2_5_0;
            string alphaentries1Query = Properties.Resources.Installbugfix27082018.ToString();
            string alphaentries2Query = Properties.Resources.Installbugfix270820181300.ToString();
            string alphaentries3Query = Properties.Resources.Installbugfix270820181305.ToString();
            string ultraprintquery = Properties.Resources.Installbugfix100520191039.ToString();
            string ultraprintbugfixes = Properties.Resources.Installbugfix201908191013.ToString();
            string upwfixes = Properties.Resources.Installbugfix201908281228.ToString();

            if (Utilities.DBisOnline())
            {                

                bool DBUpdateflag = false;
                if (!CheckIfTableExists("preorders"))
                {
                    try
                    {
                        using (Entities ctx = new Entities())
                        {
                            ctx.Database.ExecuteSqlCommand(q2);
                            //ShowSimpleDialog("Database Operations", "Successfully updated the Lilypad database.");
                            DBUpdateflag = true;
                        }
                    }
                    catch (System.Data.EntityCommandExecutionException)
                    {
                        TriedOnce = true;
                        InstallUpdateSQLScript();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }

                if (!CheckIfColumnExists("hopperorders", "InvStatus"))
                {
                    try
                    {
                        using (Entities ctx = new Entities())
                        {
                            ctx.Database.ExecuteSqlCommand(query);
                            // ShowSimpleDialog("Database Operations", "Successfully updated the Lilypad database.");
                            DBUpdateflag = true;
                        }
                    }
                    catch (System.Data.EntityCommandExecutionException)
                    {
                        TriedOnce = true;
                        InstallUpdateSQLScript();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }
                if (!CheckIfColumnExists("hopperorders", "IsPreorder"))
                {
                    try
                    {
                        using (Entities ctx = new Entities())
                        {
                            ctx.Database.ExecuteSqlCommand(query2);
                            //ShowSimpleDialog("Database Operations", "Successfully updated the Lilypad database.");
                            DBUpdateflag = true;
                        }
                    }
                    catch (System.Data.EntityCommandExecutionException)
                    {
                        TriedOnce = true;
                        InstallUpdateSQLScript();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }

                if (CheckIfColumnExists("alphaentries1", "IDStockAlpha"))
                {
                    try
                    {
                        using (Entities ctx = new Entities())
                        {
                            ctx.Database.ExecuteSqlCommand(alphaentries1Query);
                            //ShowSimpleDialog("Database Operations", "Successfully updated the Lilypad database.");
                            DBUpdateflag = true;
                        }
                    }
                    catch (System.Data.EntityCommandExecutionException)
                    {
                        TriedOnce = true;
                        InstallUpdateSQLScript();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }

                if (CheckIfColumnExists("alphaentries1", "IDAlpha"))
                {
                    try
                    {
                        using (Entities ctx = new Entities())
                        {
                            ctx.Database.ExecuteSqlCommand(alphaentries2Query);
                            //ShowSimpleDialog("Database Operations", "Successfully updated the Lilypad database.");
                            DBUpdateflag = true;
                        }
                    }
                    catch (System.Data.EntityCommandExecutionException)
                    {
                        TriedOnce = true;
                        InstallUpdateSQLScript();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }

                if (CheckIfColumnExists("alphaentries1", "IDItemCodeAlpha"))
                {
                    try
                    {
                        using (Entities ctx = new Entities())
                        {
                            ctx.Database.ExecuteSqlCommand(alphaentries3Query);
                            //ShowSimpleDialog("Database Operations", "Successfully updated the Lilypad database.");
                            DBUpdateflag = true;
                        }
                    }
                    catch (System.Data.EntityCommandExecutionException)
                    {
                        TriedOnce = true;
                        InstallUpdateSQLScript();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }

                if (!CheckIfTableExists("ultraprintorderdata"))
                {
                    try
                    {
                        using (Entities ctx = new Entities())
                        {
                            ctx.Database.ExecuteSqlCommand(ultraprintquery);
                            //ShowSimpleDialog("Database Operations", "Successfully updated the Lilypad database.");
                            DBUpdateflag = true;
                        }
                    }
                    catch (System.Data.EntityCommandExecutionException)
                    {
                        TriedOnce = true;
                        InstallUpdateSQLScript();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }

                if (CheckIfTableExists("ultraprintorderdata"))
                {
                    if (!CheckIfColumnExists("ultraprintorderdata", "IsTransfered") && !CheckIfColumnExists("ultraprintorderdata", "CustFirstName") && !CheckIfColumnExists("ultraprintorderdata", "IsLastUpdated"))
                    {
                        try
                        {
                            using (Entities ctx = new Entities())
                            {
                                ctx.Database.ExecuteSqlCommand(ultraprintbugfixes);
                                // ShowSimpleDialog("Database Operations", "Successfully updated the Lilypad database.");
                                DBUpdateflag = true;
                            }
                        }
                        catch (System.Data.EntityCommandExecutionException)
                        {
                            TriedOnce = true;
                            InstallUpdateSQLScript();
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }
                }

                if (CheckIfTableExists("ultraprintorderdata"))
                {
                    if (CheckIfColumnExists("ultraprintorderdata", "IsTransfered") && CheckIfColumnExists("ultraprintorderdata", "IsLastUpdated") && !CheckIfColumnExists("ultraprintorderdata", "TransferedFrom"))
                    {
                        try
                        {
                            using (Entities ctx = new Entities())
                            {
                                ctx.Database.ExecuteSqlCommand(upwfixes);
                                //ShowSimpleDialog("Database Operations", "Successfully updated the Lilypad database.");
                                DBUpdateflag = true;
                            }
                        }
                        catch (System.Data.EntityCommandExecutionException)
                        {
                            TriedOnce = true;
                            InstallUpdateSQLScript();
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }
                }
                if (DBUpdateflag)
                {
                    Properties.Settings.Default.App_FirstRunUpdate = false;
                    Properties.Settings.Default.Save();
                    string path = "C:\\Program Files (x86)\\Lilypad";
                    string fullpath = System.IO.Path.Combine(path, "ScriptUpdateFlagFile.txt");
                    try
                    {
                        if (System.IO.File.Exists(fullpath))
                        {
                            System.IO.File.Delete(fullpath);                            
                        }
                    }
                    catch(Exception ex)
                    {

                    }
                    ShowSimpleDialog("Database Operations", "Successfully updated the Lilypad database.");
                }

            }
        }

        public static bool InternetConnected()
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    using (var stream = wc.OpenRead("http://www.google.com"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool TestSkyIslandServerHTTP()
        {
            try
            {
                using (var wc = new WebClient())
                {
                    using (var stream = wc.OpenRead("https://www.skyislandsoftware.com"))
                    {
                        return true;
                    }
                }
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static bool CloudisOnline()
        {
            bool isValid;
            isValid = false;

            var connS = ConfigurationManager.ConnectionStrings["Cloud"].ConnectionString.ToString();
            EntityConnectionStringBuilder dbsb = new EntityConnectionStringBuilder(connS);
            using (MySqlConnection conn = new MySqlConnection(dbsb.ProviderConnectionString))
            {

                Thread thread = new Thread(TryOpen);
                ManualResetEvent mre = new ManualResetEvent(false);
                thread.Start(new Tuple<MySqlConnection, ManualResetEvent>(conn, mre));
                isValid = mre.WaitOne(2000);

                if (!isValid)
                {
                    thread.Abort();
                }

                conn.Close();
            }

            return isValid;
        }
        private static void TryOpen(object input)
        {
            Tuple<MySqlConnection, ManualResetEvent> parameters = (Tuple<MySqlConnection, ManualResetEvent>)input;

            try
            {
                parameters.Item1.Open();
                parameters.Item1.Close();
                parameters.Item2.Set();
            }
            catch
            {
                // Eat any exception, we're not interested in it
            }
        }


        public static void ForceLogin()
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            if (!mvm.FrogIsLoggedIn)
            {
                mvm.FrogIsAdmin = true;
                mvm.FrogIsManager = true;
                mvm.FrogIsLoggedIn = true;
            }
            else
            {
                mvm.FrogIsLoggedIn = false;
                mvm.FrogIsManager = true;
                mvm.FrogIsAdmin = false;
            }

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                mvm.ShowBusiness = mvm.FrogIsOwner ? true : false;
            }
            else
            {
                mvm.ShowBusiness = mvm.FrogIsOwner ? true : !mvm.ShowBusiness;                
            }
        }

        public static void DeleteDirectoryRecursive(string path)
        {
            foreach (var directory in Directory.GetDirectories(path))
            {
                DeleteDirectoryRecursive(directory);
            }

            try
            {
                Directory.Delete(path, true);
            }
            catch (IOException)
            {
                Directory.Delete(path, true);
            }
            catch (UnauthorizedAccessException)
            {
                Directory.Delete(path, true);
            }
        }

        public static List<string> GetMachineIPs()
        {
            //Gets the machine names that are connected on LAN 

            Process netUtility = new Process();
            netUtility.StartInfo.FileName = "net.exe";
            netUtility.StartInfo.CreateNoWindow = true;
            netUtility.StartInfo.Arguments = "view";
            netUtility.StartInfo.RedirectStandardOutput = true;
            netUtility.StartInfo.UseShellExecute = false;
            netUtility.StartInfo.RedirectStandardError = true;
            netUtility.Start();

            StreamReader streamReader = new StreamReader(netUtility.StandardOutput.BaseStream, netUtility.StandardOutput.CurrentEncoding);

            string line = "";
            var ComputerList = new List<string>();

            while ((line = streamReader.ReadLine()) != null)
            {

                if (line.StartsWith("\\"))
                {
                    ComputerList.Add(line.Substring(2).Substring(0, line.Substring(2).IndexOf(" ")).ToUpper());
                }
            }

            streamReader.Close();
            netUtility.WaitForExit(1000);
            return ComputerList;
        }

        public static string[] SizeOrderArray = { "OS", "NB", "3MOS", "3 MOS", "3-6MOS", "6MOS", "6 MOS", "6-12MOS", "12MOS", "12 MOS", "12-18MOS", "18MOS", "18 MOS", "18-24MOS", "24MOS", "24 MOS", "2T", "3T", "4T", "5/6", "7", "XS", "S", "M", "L", "XL", "XLT", "2XL", "2XLT", "3XL", "3XLT", "4XL", "4XLT", "5XL", "5XLT", "6XL", "7XL" };
        public static T DeepClone<T>(this T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        public static bool ContainsCaseInsensitive(this string source, string toCheck)
        {
            return source.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        public static string GetNewTemporaryDirectory()
        {
            string outPut;
            string tempPath = System.IO.Path.GetTempPath();
            try
            {
                string path = Path.Combine(tempPath, "Lilypad", "Design_Files", GetNewTempFolderPath());
                Directory.CreateDirectory(path);
                outPut = path;
                return outPut;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return "Error";
            }
        }

        private static bool CreateFirewallRuleUDPInbound()
        {
            INetFwMgr icfMgr = null;
            try
            {
                Type TicfMgr = Type.GetTypeFromProgID("HNetCfg.FwMgr");
                icfMgr = (INetFwMgr)Activator.CreateInstance(TicfMgr);
            }
            catch (Exception ex)
            {
                new Error(ex);
                return false;
            }

            try
            {
                // Inbound UDP Rule
                INetFwRule fRule = (INetFwRule) Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
                fRule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
                fRule.Name = "MySQL Server for Lilypad";
                fRule.Description = "Allows clients to access the database server across your network";
                fRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;
                fRule.Enabled = true;
                fRule.InterfaceTypes = "All";
                fRule.Protocol = 17; // UDP Protocol ID
                fRule.LocalPorts = "3306";
                fRule.RemotePorts = "3306";

                INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
                firewallPolicy.Rules.Add(fRule);
                return true;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return false;
            }
        }

        private static bool CreateFirewallRuleTCPInbound()
        {
            INetFwMgr icfMgr = null;
            try
            {
                Type TicfMgr = Type.GetTypeFromProgID("HNetCfg.FwMgr");
                icfMgr = (INetFwMgr)Activator.CreateInstance(TicfMgr);
            }
            catch (Exception ex)
            {
                new Error(ex);
                return false;
            }

            try
            {
                // Inbound UDP Rule
                INetFwRule fRule = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
                fRule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
                fRule.Name = "MySQL Server for Lilypad";
                fRule.Description = "Allows clients to access the database server across your network";
                fRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;
                fRule.Enabled = true;
                fRule.InterfaceTypes = "All";
                fRule.Protocol = 6; // TCP Protocol ID
                fRule.LocalPorts = "3306";
                fRule.RemotePorts = "3306";

                INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
                firewallPolicy.Rules.Add(fRule);
                return true;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return false;
            }
        }

        private static bool CreateFirewallRuleUDPOutbound()
        {
            INetFwMgr icfMgr = null;
            try
            {
                Type TicfMgr = Type.GetTypeFromProgID("HNetCfg.FwMgr");
                icfMgr = (INetFwMgr)Activator.CreateInstance(TicfMgr);
            }
            catch (Exception ex)
            {
                new Error(ex);
                return false;
            }

            try
            {
                // Outbound TCP Rule
                INetFwRule fRule = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
                fRule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
                fRule.Name = "MySQL Server for Lilypad";
                fRule.Description = "Allows clients to access the database server across your network";
                fRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_OUT;
                fRule.Enabled = true;
                fRule.InterfaceTypes = "All";
                fRule.Protocol = 17; // UDP Protocol ID
                fRule.LocalPorts = "3306";
                fRule.RemotePorts = "3306";

                INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
                firewallPolicy.Rules.Add(fRule);
                return true;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return false;
            }
        }

        private static bool CreateFirewallRuleTCPOutbound()
        {
            INetFwMgr icfMgr = null;
            try
            {
                Type TicfMgr = Type.GetTypeFromProgID("HNetCfg.FwMgr");
                icfMgr = (INetFwMgr)Activator.CreateInstance(TicfMgr);
            }
            catch (Exception ex)
            {
                new Error(ex);
                return false;
            }

            try
            {
                // Outbound TCP Rule
                INetFwRule fRule = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
                fRule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
                fRule.Name = "MySQL Server for Lilypad";
                fRule.Description = "Allows clients to access the database server across your network";
                fRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_OUT;
                fRule.Enabled = true;
                fRule.InterfaceTypes = "All";
                fRule.Protocol = 6; // TCP Protocol ID
                fRule.LocalPorts = "3306";
                fRule.RemotePorts = "3306";

                INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
                firewallPolicy.Rules.Add(fRule);
                return true;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return false;
            }
        }

        public static bool CreateFirewallExceptions()
        {
            return CreateFirewallRuleTCPInbound() && CreateFirewallRuleTCPOutbound() && CreateFirewallRuleUDPInbound() && CreateFirewallRuleUDPOutbound();
        }

        public static void CreateFavoriteFolderLinks()
        {
            CreateShortcut(Settings.Default.Data_PendingFolderPath, Settings.Default.Data_PendingFolderPath, Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), @"Links\Pending.lnk"), "Shortcut to Pending Orders");
            CreateShortcut(Settings.Default.Data_ProductionFolderPath, Settings.Default.Data_ProductionFolderPath, Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), @"Links\Production.lnk"), "Shortcut to Production Orders");
            CreateShortcut(Settings.Default.Data_ArchiveFolderPath, Settings.Default.Data_ArchiveFolderPath, Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), @"Links\Archive.lnk"), "Shortcut to Archived Orders");
            CreateShortcut(Settings.Default.Data_TemplatesPath, Settings.Default.Data_TemplatesPath, Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), @"Links\Templates.lnk"), "Shortcut to Lilypad Templates");
        }

        public static void CreateShortcut(string appTargetPath, string appWorkingDirectory, string shortcutTargetPath, string description)
        {
            WshShellClass wsh = new WshShellClass();
            IWshRuntimeLibrary.IWshShortcut shortcut = (IWshShortcut)wsh.CreateShortcut(shortcutTargetPath);
            shortcut.Arguments = "";
            shortcut.TargetPath = appTargetPath;
            shortcut.WindowStyle = 1;
            shortcut.Description = description;
            shortcut.WorkingDirectory = appWorkingDirectory;
            shortcut.Save();
        }

        public static string GetNewTempFolderPath()
        {
            MainWindow parent = (MainWindow)Application.Current.MainWindow;
            var pVM = (MainViewModel)parent.DataContext;

            string datenow = DateTime.Now.Month.ToString("d2") + DateTime.Now.Day.ToString() + DateTime.Now.Year.ToString().Substring(2, 2) + "." + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
            string froginitials = pVM.LoggedInFrogger.FrogInitials;
            string final = "TEMP_" + datenow + "-" + froginitials;
            return final;
        }

        public static T FindParentByType<T>(DependencyObject child) where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParentByType<T>(parentObject);
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
    (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        public static IList<string> GetThemes()
        {
            List<string> themes = new List<string>();
            themes.Add("Amber");
            themes.Add("Blue");
            themes.Add("Brown");
            themes.Add("Cobalt");
            themes.Add("Crimson");
            themes.Add("Cyan");
            themes.Add("Emerald");
            themes.Add("Green");
            themes.Add("Indigo");
            themes.Add("Lime");
            themes.Add("Magenta");
            themes.Add("Mauve");
            themes.Add("Olive");
            themes.Add("Orange");
            themes.Add("Pink");
            themes.Add("Purple");
            themes.Add("Red");
            themes.Add("Sienna");
            themes.Add("Steel");
            themes.Add("Teal");
            themes.Add("Violet");
            themes.Add("Yellow");

            return themes;
        }

        public static void LoadTheme(string theme)
        {
            if (!string.IsNullOrEmpty(theme))
            {
                ThemeManager.ChangeAppTheme(App.Current, "BaseLight");
                ThemeManager.ChangeAppStyle(App.Current, ThemeManager.GetAccent(theme), ThemeManager.GetAppTheme("BaseLight"));
            }
        }

        public static string GetHostName(string ipAddress)
        {
            try
            {
                IPHostEntry entry = Dns.GetHostEntry(ipAddress);
                if (entry != null)
                {
                    return entry.HostName;
                }
            }
            catch (SocketException ex)
            {
                //unknown host or
                //not every IP has a name
                //log exception (manage it)
            }

            return null;
        }

        public static void UpdateConfig()
        {
            // september update: auto-apply latest config if available

            try
            {
                string server = Properties.Settings.Default.Data_ServerName;
                string config = @"\Lilypad\config.txt";
                string finalPath = string.Empty;

                if (string.IsNullOrEmpty(server))
                {
                    var host = GetHostName(Properties.Settings.Default.MySQL_ServerIP);
                    server = string.IsNullOrEmpty(host) ? "PRINTSTATION" : host;
                }

                finalPath = @"\\" + server + config;

                if (System.IO.File.Exists(finalPath))
                {
                    ImportLilypadOptionsFromFile(finalPath);
                }

            }
            catch (Exception ex)
            {

            }

            try
            {
                string host = Properties.Settings.Default.MySQL_ServerIP;
                string pass = Properties.Settings.Default.MySQL_Password;
                string user = Properties.Settings.Default.MySQL_UserName;
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var connectionStringsSection = (ConnectionStringsSection) config.GetSection("connectionStrings");
                connectionStringsSection.ConnectionStrings["Entities"].ConnectionString =
@"metadata=res://*/Model.Model.csdl|res://*/Model.Model.ssdl|res://*/Model.Model.msl;provider=MySql.Data.MySqlClient;provider connection string='server=" +
                    host + ";user id=" + user + ";password=" + pass +
                    ";persistsecurityinfo=True;port=3306;database=lilypad;allowzerodatetime=False;convertzerodatetime=True'";
                connectionStringsSection.ConnectionStrings["Entities"].ProviderName = "System.Data.EntityClient";
                config.Save(ConfigurationSaveMode.Modified, true);
                ConfigurationManager.RefreshSection("connectionStrings");
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        public static int GetNthIndex(string s, char t, int n)
        {
            int count = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == t)
                {
                    count++;
                    if (count == n)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        private static int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }


        #region Drag and Drop Utilities

        public static bool HasVerticalOrientation(FrameworkElement itemContainer)
        {
            bool hasVerticalOrientation = true;
            if (itemContainer != null)
            {
                Panel panel = VisualTreeHelper.GetParent(itemContainer) as Panel;
                StackPanel stackPanel;
                WrapPanel wrapPanel;

                if ((stackPanel = panel as StackPanel) != null)
                {
                    hasVerticalOrientation = (stackPanel.Orientation == Orientation.Vertical);
                }
                else if ((wrapPanel = panel as WrapPanel) != null)
                {
                    hasVerticalOrientation = (wrapPanel.Orientation == Orientation.Vertical);
                }
                // You can add support for more panel types here.
            }
            return hasVerticalOrientation;
        }

        public static void InsertItemInItemsControl(ItemsControl itemsControl, object itemToInsert, int insertionIndex)
        {
            if (itemToInsert != null)
            {
                IEnumerable itemsSource = itemsControl.ItemsSource;

                if (itemsSource == null)
                {
                    itemsControl.Items.Insert(insertionIndex, itemToInsert);
                }
                // Is the ItemsSource IList or IList<T>? If so, insert the dragged item in the list.
                else if (itemsSource is IList)
                {
                    ((IList)itemsSource).Insert(insertionIndex, itemToInsert);
                }
                else
                {
                    Type type = itemsSource.GetType();
                    Type genericIListType = type.GetInterface("IList`1");
                    if (genericIListType != null)
                    {
                        type.GetMethod("Insert").Invoke(itemsSource, new object[] { insertionIndex, itemToInsert });
                    }
                }
            }
        }

        public static int RemoveItemFromItemsControl(ItemsControl itemsControl, object itemToRemove)
        {
            int indexToBeRemoved = -1;
            if (itemToRemove != null)
            {
                indexToBeRemoved = itemsControl.Items.IndexOf(itemToRemove);

                if (indexToBeRemoved != -1)
                {
                    IEnumerable itemsSource = itemsControl.ItemsSource;
                    if (itemsSource == null)
                    {
                        itemsControl.Items.RemoveAt(indexToBeRemoved);
                    }
                    // Is the ItemsSource IList or IList<T>? If so, remove the item from the list.
                    else if (itemsSource is IList)
                    {
                        ((IList)itemsSource).RemoveAt(indexToBeRemoved);
                    }
                    else
                    {
                        Type type = itemsSource.GetType();
                        Type genericIListType = type.GetInterface("IList`1");
                        if (genericIListType != null)
                        {
                            type.GetMethod("RemoveAt").Invoke(itemsSource, new object[] { indexToBeRemoved });
                        }
                    }
                }
            }
            return indexToBeRemoved;
        }

        public static bool IsInFirstHalf(FrameworkElement container, Point clickedPoint, bool hasVerticalOrientation)
        {
            if (hasVerticalOrientation)
            {
                return clickedPoint.Y < container.ActualHeight / 2;
            }
            return clickedPoint.X < container.ActualWidth / 2;
        }

        public static bool IsMovementBigEnough(Point initialMousePosition, Point currentPosition)
        {
            return (Math.Abs(currentPosition.X - initialMousePosition.X) >= SystemParameters.MinimumHorizontalDragDistance ||
                 Math.Abs(currentPosition.Y - initialMousePosition.Y) >= SystemParameters.MinimumVerticalDragDistance);
        }

        #endregion


        public async static void ManualFocusViaSendKeys()
        {
            Thread t = new Thread(async () =>
            {
                await Task.Delay(500);
                System.Windows.Forms.SendKeys.SendWait("{LEFT}");
            });
            t.Start();
        }

        public async static void ExitLilypad()
        {
            ManualFocusViaSendKeys();
            MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Shutdown", "Are you sure you would like to exit Lilypad?");
            if (result == MessageDialogResult.Affirmative)
            {
                Application.Current.Shutdown();
            }
        }

        public async static Task<MessageDialogResult> ShowMetroYNDialog(string title, string message)
        {
            var wind = (MetroWindow)Application.Current.MainWindow;
            wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            wind.MetroDialogOptions.AnimateHide = true;
            return await wind.ShowMessageAsync(title, message, MessageDialogStyle.AffirmativeAndNegative, wind.MetroDialogOptions);
        }

        public async static void ShowYoutubeVideo(object input)
        {
            var wind = (MetroWindow)Application.Current.MainWindow;
            wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiag = new SimpleDialog();
            newdiag.Background = Brushes.Transparent;
            newdiag.DialogBody = input;
            await wind.ShowMetroDialogAsync(newdiag);
        }

        public async static void ShowSimpleDialog(object input)
        {
            var wind = (MetroWindow)Application.Current.MainWindow;
            wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiag = new SimpleDialog();
            newdiag.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
            newdiag.DialogBody = input;
            await wind.ShowMetroDialogAsync(newdiag);
        }

        public async static void ShowSimpleDialogReceipts(string header, string caption)
        {
            var wind = (MetroWindow)Application.Current.MainWindow;
            StackPanel sp = new StackPanel();
            sp.HorizontalAlignment = HorizontalAlignment.Left;
            Label headerLbl = new Label();
            headerLbl.FontSize = 26;
            headerLbl.Margin = new Thickness(100, -25, 0, 0);
            headerLbl.Foreground = Brushes.White;
            headerLbl.FontFamily = new FontFamily("Segoe UI");
            headerLbl.Content = header;
            Label subheader = new Label();
            subheader.FontSize = 14;
            subheader.Foreground = Brushes.White;
            subheader.FontFamily = new FontFamily("Segoe UI Light");
            subheader.Margin = new Thickness(0, 0, 0, 10);
            subheader.Content = caption;
            subheader.Width = wind.ActualWidth / 1;
            //subheader.Height = wind.ActualHeight/ 4;
            sp.Children.Add(headerLbl);
            sp.Children.Add(subheader);
            double pos = wind.ActualWidth / 2.5;
            wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiag = new SimpleDialog();
            newdiag.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
            Button okbutton = new Button();
            okbutton.Content = "ok";
            okbutton.HorizontalAlignment = HorizontalAlignment.Right;
            okbutton.Width = 100;
            okbutton.Margin = new Thickness(pos, 15, 0, 50);
            okbutton.Click += (e, sender) => { wind.HideMetroDialogAsync(newdiag); };
            sp.Children.Add(okbutton);
            newdiag.DialogBody = sp;
            ManualFocusViaSendKeys();
            await wind.ShowMetroDialogAsync(newdiag);
        }

        public async static void ShowSimpleDialog(string header, string caption)
        {
            var wind = (MetroWindow)Application.Current.MainWindow;
            StackPanel sp = new StackPanel();
            sp.HorizontalAlignment = HorizontalAlignment.Left;
            Label headerLbl = new Label();
            headerLbl.FontSize = 26;
            headerLbl.Margin = new Thickness(0, -25, 0, 0);
            headerLbl.Foreground = Brushes.White;
            headerLbl.FontFamily = new FontFamily("Segoe UI");
            headerLbl.Content = header;
            Label subheader = new Label();
            subheader.FontSize = 14;
            subheader.Foreground = Brushes.White;
            subheader.FontFamily = new FontFamily("Segoe UI Light");
            subheader.Margin = new Thickness(0, 0, 0, 10);
            subheader.Content = caption;
            subheader.Width = wind.ActualWidth / 2;
            sp.Children.Add(headerLbl);
            sp.Children.Add(subheader);
            double pos = wind.ActualWidth / 2.5;
            wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiag = new SimpleDialog();
            newdiag.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
            Button okbutton = new Button();
            okbutton.Content = "ok";
            okbutton.HorizontalAlignment = HorizontalAlignment.Right;
            okbutton.Width = 100;
            okbutton.Margin = new Thickness(pos, 15, 0, 0);
            okbutton.Click += (e, sender) => { wind.HideMetroDialogAsync(newdiag); };
            sp.Children.Add(okbutton);
            newdiag.DialogBody = sp;
            ManualFocusViaSendKeys();
            await wind.ShowMetroDialogAsync(newdiag);
        }

        public async static void ShowSimpleDialog(string header, string caption, object input)
        {
            StackPanel sp = new StackPanel();
            sp.HorizontalAlignment = HorizontalAlignment.Left;
            Label headerLbl = new Label();
            headerLbl.FontSize = 26;
            headerLbl.Margin = new Thickness(0, -25, 0, -5);
            headerLbl.Foreground = Brushes.White;
            headerLbl.FontFamily = new FontFamily("Segoe UI");
            headerLbl.Content = header;
            Label subheader = new Label();
            subheader.FontSize = 14;
            subheader.Foreground = Brushes.White;
            subheader.FontFamily = new FontFamily("Segoe UI Light");
            subheader.Margin = new Thickness(0, 0, 0, 10);
            subheader.Content = caption;
            sp.Children.Add(headerLbl);
            sp.Children.Add(subheader);
            UIElement elem = (UIElement)input;
            sp.Children.Add(elem);
            var wind = (MetroWindow)Application.Current.MainWindow;
            wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiag = new SimpleDialog();
            newdiag.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
            newdiag.DialogBody = sp;
            ManualFocusViaSendKeys();
            await wind.ShowMetroDialogAsync(newdiag);
        }

        public async static void ShowInitialSetup()
        {
            StackPanel sp = new StackPanel();
            sp.HorizontalAlignment = HorizontalAlignment.Left;
            Label headerLbl = new Label();
            headerLbl.FontSize = 26;
            headerLbl.Margin = new Thickness(0, -25, 0, -5);
            headerLbl.Foreground = Brushes.White;
            headerLbl.FontFamily = new FontFamily("Segoe UI");
            headerLbl.Content = "Initial Setup";
            Label subheader = new Label();
            subheader.FontSize = 14;
            subheader.Foreground = Brushes.White;
            subheader.FontFamily = new FontFamily("Segoe UI Light");
            subheader.Margin = new Thickness(0, 0, 0, 10);
            subheader.Content = "Welcome to Lilypad! Please take a few moments to setup your data paths.";
            sp.Children.Add(headerLbl);
            sp.Children.Add(subheader);
            Dialogs.InitialSetup elem = new Dialogs.InitialSetup();
            sp.Children.Add(elem);
            var wind = (MetroWindow)Application.Current.MainWindow;
            wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiag = new SimpleDialog();
            newdiag.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
            newdiag.DialogBody = sp;
            await wind.ShowMetroDialogAsync(newdiag);
        }

        public async static void ShowImageEdit(EmployeesViewModel input)
        {
            StackPanel sp = new StackPanel();
            sp.HorizontalAlignment = HorizontalAlignment.Left;
            Label headerLbl = new Label();
            headerLbl.FontSize = 26;
            headerLbl.Margin = new Thickness(0, -25, 0, -5);
            headerLbl.Foreground = Brushes.White;
            headerLbl.FontFamily = new FontFamily("Segoe UI");
            headerLbl.Content = "Image Editor";
            Label subheader = new Label();
            subheader.FontSize = 14;
            subheader.Foreground = Brushes.White;
            subheader.FontFamily = new FontFamily("Segoe UI Light");
            subheader.Margin = new Thickness(0, 0, 0, 10);
            subheader.Content = "Select an image to use as your user avatar.";
            sp.Children.Add(headerLbl);
            sp.Children.Add(subheader);
            Dialogs.ImageEdit elem = new Dialogs.ImageEdit(input);
            sp.Children.Add(elem);
            var wind = (MetroWindow)Application.Current.MainWindow;
            wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiag = new SimpleDialog();
            newdiag.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
            newdiag.DialogBody = sp;
            await wind.ShowMetroDialogAsync(newdiag);
        }

        public async static void ShowArtSetup(DesignSetupCardViewModel input)
        {
            StackPanel sp = new StackPanel();
            sp.HorizontalAlignment = HorizontalAlignment.Left;
            Label headerLbl = new Label();
            headerLbl.FontSize = 26;
            headerLbl.Margin = new Thickness(0, -25, 0, -5);
            headerLbl.Foreground = Brushes.White;
            headerLbl.FontFamily = new FontFamily("Segoe UI");
            headerLbl.Content = "Artwork Setup";
            Label subheader = new Label();
            subheader.FontSize = 14;
            subheader.Foreground = Brushes.White;
            subheader.FontFamily = new FontFamily("Segoe UI Light");
            subheader.Margin = new Thickness(0, 0, 0, 10);
            subheader.Content = "Select artwork templates to add or remove from this design.";
            sp.Children.Add(headerLbl);
            sp.Children.Add(subheader);
            Dialogs.ArtSetup elem = new Dialogs.ArtSetup(input);
            sp.Children.Add(elem);
            var wind = (MetroWindow)Application.Current.MainWindow;
            wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiag = new SimpleDialog();
            newdiag.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
            newdiag.DialogBody = sp;
            await wind.ShowMetroDialogAsync(newdiag);
        }

        public async static void ShowUltraPrintSetup(OrderNewViewModel input)
        {
            StackPanel sp = new StackPanel();
            sp.HorizontalAlignment = HorizontalAlignment.Left;
            Label headerLbl = new Label();
            headerLbl.FontSize = 26;
            headerLbl.Margin = new Thickness(-250, -25, 0, -5);
            headerLbl.Foreground = Brushes.White;
            headerLbl.FontFamily = new FontFamily("Roboto");
            headerLbl.Content = "UltraPrint Setup";
            Label subheader = new Label();
            subheader.FontSize = 14;
            subheader.Foreground = Brushes.White;
            subheader.FontFamily = new FontFamily("Roboto");
            subheader.Margin = new Thickness(0, 0, 0, 10);
            subheader.Content = "";
            sp.Children.Add(headerLbl);
            sp.Children.Add(subheader);
            Dialogs.UltraPrint elem = new Dialogs.UltraPrint(input);
            sp.Children.Add(elem);
            var wind = (MetroWindow)Application.Current.MainWindow;
            wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiag = new SimpleDialog();
            newdiag.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
            newdiag.DialogBody = sp;
            await wind.ShowMetroDialogAsync(newdiag);
        }


        public async static void ShowUltraPrintExistingTransferSetup(OrderNewViewModel input)
        {
            StackPanel spss = new StackPanel();
            spss.HorizontalAlignment = HorizontalAlignment.Left;
            Label headerLbl = new Label();
            headerLbl.FontSize = 26;
            headerLbl.Margin = new Thickness(-250, -25, 0, -5);
            headerLbl.Foreground = Brushes.White;
            headerLbl.FontFamily = new FontFamily("Roboto");
            headerLbl.Content = "UltraPrint Setup";
            Label subheader = new Label();
            subheader.FontSize = 14;
            subheader.Foreground = Brushes.White;
            subheader.FontFamily = new FontFamily("Roboto");
            subheader.Margin = new Thickness(0, 0, 0, 10);
            subheader.Content = "";
            spss.Children.Add(headerLbl);
            spss.Children.Add(subheader);
            Dialogs.UltraPrintExistingTransferNewSetUp elem = new Dialogs.UltraPrintExistingTransferNewSetUp(input);
            spss.Children.Add(elem);
            var windss = (MetroWindow)Application.Current.MainWindow;
            windss.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiagss = new SimpleDialog();
            newdiagss.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
            newdiagss.DialogBody = spss;
            await windss.ShowMetroDialogAsync(newdiagss);
        }


        public static bool ShowUltraPrintUsage(HopperViewModel input)
        {
            StackPanel sp = new StackPanel();
            sp.HorizontalAlignment = HorizontalAlignment.Left;
            Label headerLbl = new Label();
            headerLbl.FontSize = 26;
            headerLbl.Margin = new Thickness(-250, -25, 0, -5);
            headerLbl.Foreground = Brushes.White;
            headerLbl.FontFamily = new FontFamily("Roboto");
            headerLbl.Content = "UltraPrint Usage";
            Label subheader = new Label();
            subheader.FontSize = 14;
            subheader.Foreground = Brushes.White;
            subheader.FontFamily = new FontFamily("Roboto");
            subheader.Margin = new Thickness(0, 0, 0, 10);
            subheader.Content = "";
            sp.Children.Add(headerLbl);
            sp.Children.Add(subheader);
            Dialogs.UltraPrintUsage elem = new Dialogs.UltraPrintUsage(input);
            sp.Children.Add(elem);
            var wind = (MetroWindow)Application.Current.MainWindow;
            wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiag = new SimpleDialog();
            newdiag.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
            newdiag.DialogBody = sp;
            wind.ShowMetroDialogAsync(newdiag);
            return true;
        }

        public static bool ShowUltraPrintTransfer(OrderNewViewModel inputs)
        {
            StackPanel spt = new StackPanel();
            spt.HorizontalAlignment = HorizontalAlignment.Left;
            Label headerLbl1 = new Label();
            headerLbl1.FontSize = 26;
            headerLbl1.Margin = new Thickness(-250, -25, 0, -5);
            headerLbl1.Foreground = Brushes.White;
            headerLbl1.FontFamily = new FontFamily("Roboto");
            headerLbl1.Content = "UltraPrint Existing Transfer";
            Label subheader1 = new Label();
            subheader1.FontSize = 14;
            subheader1.Foreground = Brushes.White;
            subheader1.FontFamily = new FontFamily("Roboto");
            subheader1.Margin = new Thickness(0, 0, 0, 10);
            subheader1.Content = "";
            spt.Children.Add(headerLbl1);
            spt.Children.Add(subheader1);
            Dialogs.UltraPrintExistingTransfer element = new Dialogs.UltraPrintExistingTransfer(inputs);
            spt.Children.Add(element);
            var winds = (MetroWindow)Application.Current.MainWindow;
            winds.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiags = new SimpleDialog();
            newdiags.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
            newdiags.DialogBody = spt;
            winds.ShowMetroDialogAsync(newdiags);
            return true;
        }

        public static bool ShowUltraPrintTransferUsage(HopperViewModel input)
        {
            StackPanel sp = new StackPanel();
            sp.HorizontalAlignment = HorizontalAlignment.Left;
            Label headerLbl = new Label();
            headerLbl.FontSize = 26;
            headerLbl.Margin = new Thickness(-250, -25, 0, -5);
            headerLbl.Foreground = Brushes.White;
            headerLbl.FontFamily = new FontFamily("Roboto");
            headerLbl.Content = "UltraPrint Transfer Usage";
            Label subheader = new Label();
            subheader.FontSize = 14;
            subheader.Foreground = Brushes.White;
            subheader.FontFamily = new FontFamily("Roboto");
            subheader.Margin = new Thickness(0, 0, 0, 10);
            subheader.Content = "";
            sp.Children.Add(headerLbl);
            sp.Children.Add(subheader);
            Dialogs.UltraPrintExistingTransferUsage elem = new Dialogs.UltraPrintExistingTransferUsage(input);
            sp.Children.Add(elem);
            var wind = (MetroWindow)Application.Current.MainWindow;
            wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
            SimpleDialog newdiag = new SimpleDialog();
            newdiag.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
            newdiag.DialogBody = sp;
            wind.ShowMetroDialogAsync(newdiag);
            return true;
        }

        public static async void MarkHopperOrderCompleted(int id)
        {
            var pVM = (MainViewModel)Application.Current.MainWindow.DataContext;
            int thisID = id;
            hopperorder thisorder;
            log thislog = new log();

            using (Entities ctx = new Entities())
            {
                if (ctx.hopperorders.Any(x => x.ReceiptNum == thisID))
                {
                    thisorder = (from k in ctx.hopperorders where k.ReceiptNum == thisID select k).FirstOrDefault();
                    thisorder.Status = "Completed";

                    thislog.Action = pVM.LoggedInFrogger.FrogFirstName + " marked this order as completed";
                    thislog.LogDateTime = DateTime.Now;
                    thislog.ReceiptNum = thisID;

                    try
                    {
                        ctx.logs.Add(thislog);
                        ctx.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }

                    // Correspondence after submission
                    CorrespondenceViewModel cvm = new CorrespondenceViewModel();
                    customer cdata = (from q in ctx.customers where q.CustomerID == thisorder.CustomerID select q).FirstOrDefault();
                    string frogname = pVM.LoggedInFrogger.FrogFirstName;
                    var nl = Environment.NewLine;
                    string body;
                    if (Properties.Settings.Default.Interop_Email_OverrideMessage)
                    {
                        body = Properties.Settings.Default.Interop_Email_Message.ToString();
                    }
                    else
                    {
                        body = "Hi " + cdata.FirstName + "," + nl +
            "Your custom order has been printed and is ready to be picked up! " + nl +
            "Our store hours this week are " + Properties.Settings.Default.Store_HoursMF + " Monday thru Friday, " + Properties.Settings.Default.Store_HoursSat + " on Saturday, and " + Properties.Settings.Default.Store_HoursSun + " on Sunday. If you have any questions or need to make special arrangements to pick up your order, please give us a call at " + Properties.Settings.Default.Store_Phone + " and let us know in advance." + nl +
            "Thank you for your order! We truly appreciate your business, and hope to see you in soon!" + nl +
            "-" + frogname;
                    }
                    cvm.CompletedOrder = thisorder;
                    cvm.EmailTo = cdata.Email;
                    cvm.EmailSubject = "Your Big Frog Order (#" + thisorder.ReceiptNum + ") Is Ready!";
                    cvm.EmailText = body;
                    Dialogs.Correspondence cWin = new Dialogs.Correspondence();
                    cWin.DataContext = cvm;

                    var wind = (MetroWindow)Application.Current.MainWindow;
                    wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
                    SimpleDialog newdiag = new SimpleDialog();
                    newdiag.Background = (System.Windows.Media.Brush)App.Current.FindResource("AccentColorBrush");
                    newdiag.DialogBody = cWin;
                    await wind.ShowMetroDialogAsync(newdiag);

                }
            }


        }

        public async static void DeleteHopperOrderAndRelatedData(int id)
        {
            MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you would like to delete this order? This cannot be undone!");
            if (result == MessageDialogResult.Affirmative)
            {
                Entities ctx = new Entities();
                try
                {
                    hopperorder thisorder = (from k in ctx.hopperorders where k.ReceiptNum == id select k).FirstOrDefault();

                    string dir = thisorder.JobFolderPath.ToString();
                    string dirdlt = dir + @" +DELETED+";
                    if (Directory.Exists(dir))
                    {
                        if (!Directory.Exists(dirdlt))
                        {
                            try
                            {
                                Directory.Move(dir, dirdlt);
                            }
                            catch (Exception ex)
                            {
                                new Error(ex);
                            }
                        }
                    }

                    orderdetail[] thisdetail = (from j in ctx.orderdetails where j.ReceiptNum == id select j).ToArray();
                    calllist thiscl = (from u in ctx.calllists where u.HopperReceipt == id select u).FirstOrDefault();
                    receipt thisreceipt = (from h in ctx.receipts where h.RcptNumber == id select h).FirstOrDefault();
                    designdetail[] thisddetail = (from t in ctx.designdetails where t.ReceiptNum == id select t).ToArray();
                    designdetailslineitem[] thisddli = (from y in ctx.designdetailslineitems where y.ReceiptNum == id select y).ToArray();

                    log[] thislog = (from p in ctx.logs where p.ReceiptNum == id select p).ToArray();
                    alphacurrentorder[] thisalphacurr = (from o in ctx.alphacurrentorders where o.ReceiptNum == id select o).ToArray();
                    alphaentry[] thisalphaent = (from e in ctx.alphaentries where e.ReceiptNum == id select e).ToArray();
                    ultraprintorder[] thisultras = (from b in ctx.ultraprintorders where b.ReceiptNum == id select b).ToArray();

                    ultraprintorderdata[] thisultrasdata = (from b in ctx.ultraprintorderdatas where b.receiptnum == id select b).ToArray();

                    if (thisreceipt != null) { ctx.receipts.Remove(thisreceipt); }
                    if (thislog != null) { foreach (log lg in thislog) { ctx.logs.Remove(lg); } }
                    if (thisalphacurr != null) { foreach (alphacurrentorder aec in thisalphacurr) { ctx.alphacurrentorders.Remove(aec); } }
                    if (thisalphaent != null) { foreach (alphaentry aen in thisalphaent) { ctx.alphaentries.Remove(aen); } }
                    if (thisultras != null) { foreach (ultraprintorder up in thisultras) { ctx.ultraprintorders.Remove(up); } }
                    if (thisddli != null) { foreach (designdetailslineitem dli in thisddli) { ctx.designdetailslineitems.Remove(dli); } }
                    if (thisddetail != null) { foreach (designdetail dt in thisddetail) { ctx.designdetails.Remove(dt); } }
                    if (thisdetail != null) { foreach (orderdetail deet in thisdetail) { ctx.orderdetails.Remove(deet); } }
                    if (thiscl != null) { ctx.calllists.Remove(thiscl); }
                    if (thisorder != null) { ctx.hopperorders.Remove(thisorder); }

                    if (thisultrasdata != null) { foreach (ultraprintorderdata up in thisultrasdata) { ctx.ultraprintorderdatas.Remove(up); } }

                    ctx.SaveChanges();
                    Utilities.ShowSimpleDialog("Operation Successful", "Successfully removed hopper order entry from Lilypad.");
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }

        }

        public static string GetLocalIPAddress()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                return null;
            }

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            return host.AddressList.FirstOrDefault(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();
        }

        public async static void DeleteOrderAndRelatedData(int id)
        {
            MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you would like to delete this order? This cannot be undone!");
            if (result == MessageDialogResult.Affirmative)
            {
                Entities ctx = new Entities();
                try
                {
                    order thisorder = (from k in ctx.orders where k.ReceiptNum == id select k).FirstOrDefault();
                    //do file operations to prevent overlap of new designs
                    string dir = thisorder.JobFolderPath.ToString();
                    string dirdlt = dir + @" +DELETED+";
                    if (Directory.Exists(dir))
                    {
                        if (!Directory.Exists(dirdlt))
                        {
                            Directory.Move(dir, dirdlt);
                        }
                    }

                    orderdetail[] thisdetail = (from j in ctx.orderdetails where j.ReceiptNum == id select j).ToArray();
                    calllist thiscl = (from u in ctx.calllists where u.ReceiptNum == id select u).FirstOrDefault();
                    receipt thisreceipt = (from h in ctx.receipts where h.RcptNumber == id select h).FirstOrDefault();
                    designdetail[] thisddetail = (from t in ctx.designdetails where t.ReceiptNum == id select t).ToArray();
                    designdetailslineitem[] thisddli = (from y in ctx.designdetailslineitems where y.ReceiptNum == id select y).ToArray();

                    log[] thislog = (from p in ctx.logs where p.ReceiptNum == id select p).ToArray();
                    alphacurrentorder[] thisalphacurr = (from o in ctx.alphacurrentorders where o.ReceiptNum == id select o).ToArray();
                    alphaentry[] thisalphaent = (from e in ctx.alphaentries where e.ReceiptNum == id select e).ToArray();
                    ultraprintorder[] thisultras = (from b in ctx.ultraprintorders where b.ReceiptNum == id select b).ToArray();

                    if (thisreceipt != null) { ctx.receipts.Remove(thisreceipt); }
                    if (thislog != null) { foreach (log lg in thislog) { ctx.logs.Remove(lg); } }
                    if (thisalphacurr != null) { foreach (alphacurrentorder aec in thisalphacurr) { ctx.alphacurrentorders.Remove(aec); } }
                    if (thisalphaent != null) { foreach (alphaentry aen in thisalphaent) { ctx.alphaentries.Remove(aen); } }
                    if (thisultras != null) { foreach (ultraprintorder up in thisultras) { ctx.ultraprintorders.Remove(up); } }
                    if (thisddli != null) { foreach (designdetailslineitem dli in thisddli) { ctx.designdetailslineitems.Remove(dli); } }
                    if (thisddetail != null) { foreach (designdetail dt in thisddetail) { ctx.designdetails.Remove(dt); } }
                    if (thiscl != null) { ctx.calllists.Remove(thiscl); }
                    if (thisdetail != null) { foreach (orderdetail deet in thisdetail) { ctx.orderdetails.Remove(deet); } }
                    if (thisorder != null) { ctx.orders.Remove(thisorder); }

                    ctx.SaveChanges();
                    Utilities.ShowSimpleDialog("Operation Successful", "Successfully removed completed order entry from Lilypad.");
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }


        public static void MarkHopperOrderPickedUp(int id)
        {
            var pVM = (MainViewModel)Application.Current.MainWindow.DataContext;
            int thisID = id;

            hopperorder thisorder = new hopperorder();
            order neworder = new order();

            using (Entities ctx = new Entities())
            {
                if (ctx.hopperorders.Any(x => x.ReceiptNum == thisID))
                {
                    thisorder = (from k in ctx.hopperorders where k.ReceiptNum == thisID select k).FirstOrDefault();

                    if (ctx.calllists.Any(x => x.ReceiptNum == thisID))
                    {
                        calllist cl = (calllist)ctx.calllists.First(x => x.ReceiptNum == thisID);
                        try
                        {
                            ctx.calllists.Remove(cl);
                            ctx.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }

                    if (ctx.calllists.Any(x => x.HopperReceipt == thisID))
                    {
                        calllist cl = (calllist)ctx.calllists.First(x => x.HopperReceipt == thisID);
                        try
                        {
                            ctx.calllists.Remove(cl);
                            ctx.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }

                    if (ctx.orders.Any(x => x.ReceiptNum == thisID))
                    {
                        order cl = (order)ctx.orders.First(x => x.ReceiptNum == thisID);
                        try
                        {
                            ctx.orders.Remove(cl);
                            ctx.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }

                    neworder.BoxNum = thisorder.BoxNum;
                    neworder.Archived = true;
                    neworder.CareTags = 1;
                    neworder.Correspondence = thisorder.Correspondence;
                    neworder.CustomerID = thisorder.CustomerID;
                    neworder.DesignFrog = thisorder.DesignFrog;
                    neworder.DueDate = thisorder.DueDate;
                    neworder.Irregulars = thisorder.Irregulars;
                    neworder.JobFolderPath = thisorder.JobFolderPath;
                    neworder.Notes = thisorder.Notes;
                    neworder.OrderDate = thisorder.OrderDate;
                    neworder.OrderFrog = thisorder.OrderFrog;
                    neworder.PrintFrog = pVM.LoggedInFrogger.FrogID;
                    neworder.PrintProcess = thisorder.PrintProcess;
                    neworder.Priority = thisorder.Priority;
                    neworder.ReceiptNum = thisorder.ReceiptNum;
                    neworder.SearchTags = thisorder.SearchTags;
                    neworder.TotalCost = thisorder.TotalCost;
                    neworder.TotalShirts = thisorder.TotalShirts;
                    neworder.ArtworkStatus = thisorder.ArtworkStatus;
                    neworder.InventoryStatus = thisorder.InventoryStatus;
                    neworder.Status = 11;
                    neworder.CompletedDate = DateTime.Now;

                    try
                    {
                        ctx.orders.Add(neworder);
                        ctx.SaveChanges();
                        ctx.hopperorders.Remove(thisorder);
                        ctx.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }

                    try
                    {
                        using (Entities ctx2 = new Entities())
                        {
                            log thislog = new log();
                            thislog.Action = pVM.LoggedInFrogger.FrogFirstName + " marked this order as picked up.";
                            thislog.LogDateTime = DateTime.Now;
                            thislog.ReceiptNum = thisID;
                            ctx2.logs.Add(thislog);
                            ctx2.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }

                    try
                    {
                        string arc = Settings.Default.Data_ArchiveFolderPath.ToString();
                        string path = thisorder.JobFolderPath;
                        System.IO.DirectoryInfo dd = new System.IO.DirectoryInfo(path);
                        string des = System.IO.Path.Combine(arc, dd.Name);
                        MoveDirectory(path, des);
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }

                    Task.Factory.StartNew(() => { CheckIOQueueForPendingOperations(); });
                }
                else
                {
                    ShowSimpleDialog("Order Missing", "That order doesn't exist in the hopper or has already been marked as picked up.");
                }
            }

        }

        public static string[] GarmentSizeArray =
        {
            "OS", "NB", "3MOS", "3 MOS", "3-6MOS", "6MOS", "6 MOS", "6-12MOS",
            "12MOS", "12 MOS", "12-18MOS", "18MOS", "18 MOS", "18-24MOS", "24MOS", "24 MOS", "2T", "3T", "4T", "5/6",
            "7", "XS", "S", "M", "L", "XL", "XLT", "2XL", "2XLT", "3XL", "3XLT", "4XL", "4XLT", "5XL", "5XLT", "6XL",
            "7XL"
        };

        public static bool IsInDesignTime = DesignerProperties.GetIsInDesignMode(new DependencyObject());
        public static bool IsDebuggerAttached = Debugger.IsAttached;

        public static string LimitTo(this string data, int length)
        {
            return (data == null || data.Length < length)
              ? data
              : data.Substring(0, length);
        }

        public static bool ClearPathAttributes(string path)
        {
            if (Directory.Exists(path))
            {
                string[] subDirs = Directory.GetDirectories(path);

                foreach (string dir in subDirs)
                {
                    ClearPathAttributes(dir);
                }

                string[] files = Directory.GetFiles(path);

                foreach (string file in files)
                {
                    System.IO.File.SetAttributes(file, FileAttributes.Normal);
                }
                return true;
            }
            else return false;
        }

        public static bool MoveDirectoryWithAttributeCheck(string source, string destination)
        {
            if (Directory.Exists(source))
            {
                try
                {
                    ClearPathAttributes(source);
                    Directory.Move(source, destination);
                    return true;
                }
                catch (Exception ex)
                {
                    new Error(ex);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool MoveDirectory(string source, string dest)
        {
            //Now Create all of the directories
            if (Directory.Exists(source))
            {

                try
                {
                    foreach (string dirPath in Directory.GetDirectories(source, "*",
                      SearchOption.AllDirectories))
                        Directory.CreateDirectory(dirPath.Replace(source, dest));
                }
                catch (System.IO.IOException iox)
                {
                    using (Entities ctx = new Entities())
                    {
                        ioqueue qq = new ioqueue();
                        qq.Destination = dest;
                        qq.OperationType = "CREATE";
                        qq.LastAttempt = DateTime.Now;
                        ctx.ioqueues.Add(qq);
                    }
                    return false;
                }

                try
                {

                    //Copy all the files & Replaces any files with the same name
                    foreach (string newPath in Directory.GetFiles(source, "*.*",
                        SearchOption.AllDirectories))
                        System.IO.File.Copy(newPath, newPath.Replace(source, dest), true);
                }
                catch (System.IO.IOException iox)
                {
                    using (Entities ctx = new Entities())
                    {
                        ioqueue qq = new ioqueue();
                        qq.Source = source;
                        qq.Destination = dest;
                        qq.OperationType = "COPY";
                        qq.LastAttempt = DateTime.Now;
                        ctx.ioqueues.Add(qq);
                    }
                    return false;
                }

                try
                {
                    Directory.Delete(source, true);
                    return true;
                }
                catch (System.IO.IOException iox)
                {
                    using (Entities ctx = new Entities())
                    {
                        ioqueue qq = new ioqueue();
                        qq.Source = source;
                        qq.OperationType = "DELETE";
                        qq.LastAttempt = DateTime.Now;
                        ctx.ioqueues.Add(qq);
                    }
                    return false;
                }
            }
            else return false;
        }

        public static bool CheckIOQueueForPendingOperations()
        {
            bool success = false;
            TimeSpan taffySpan = new TimeSpan();

            if (TimeSpan.TryParse(Properties.Settings.Default.Tafffy_Interval.ToString(), out taffySpan))
            {
                DateTime minDate = DateTime.Now.Subtract(taffySpan);

                IList<ioqueue> ioList = new List<ioqueue>();

                using (Entities ctx = new Entities())
                {
                    if (ctx.ioqueues.Any(x => x.OperationSuccessful == false && x.LastAttempt <= minDate))
                    {
                        try
                        {
                            ioList = ctx.ioqueues.Where(x => x.OperationSuccessful == false && x.LastAttempt <= minDate).ToList();
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }

                    if (ioList.Count > 0)
                    {
                        foreach (ioqueue io in ioList)
                        {
                            string op = io.OperationType.ToUpper();

                            if (op == "DELETE")
                            {
                                try
                                {
                                    Directory.Delete(io.Source, true);
                                    io.LastAttempt = DateTime.Now;
                                    io.OperationSuccessful = true;
                                    success = true;
                                }
                                catch (System.IO.IOException iox)
                                {
                                    io.LastAttempt = DateTime.Now;
                                    io.OperationSuccessful = false;
                                    success = false;
                                }
                            }
                            else if (op == "COPY")
                            {
                                try
                                {
                                    foreach (string newPath in Directory.GetFiles(io.Source, "*.*", SearchOption.AllDirectories))
                                        System.IO.File.Copy(newPath, newPath.Replace(io.Source, io.Destination), true);
                                    io.LastAttempt = DateTime.Now;
                                    io.OperationSuccessful = true;
                                    success = true;
                                }
                                catch (System.IO.IOException iox)
                                {
                                    io.LastAttempt = DateTime.Now;
                                    io.OperationSuccessful = false;
                                    success = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        success = true;
                    }
                }
            }
            else
            {
                try
                {
                    Properties.Settings.Default.Tafffy_Interval = "00:12:00:00";
                    Properties.Settings.Default.Save();
                    return false;
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }


            return success;
        }


        public static string BytesToString(long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];
        }


    }
}