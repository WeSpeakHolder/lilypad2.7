using System;
using System.Reflection;
using System.Deployment.Application;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Properties;


namespace Lilypad.ViewModel
{
    public class SettingsAboutViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public Version AssemblyVersion
        {
            get { return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version; }
        }

        public string Version
        {
            get { return Settings.Default.App_Version; }
        }

        public string VersionName
        {
            get { return Settings.Default.App_VersionName; }
        }

        public string VersionSecurity
        {
            get { return Settings.Default.App_Security; }
        }

        public string VersionBuild
        {
            get
            {
                return ApplicationDeployment.IsNetworkDeployed
                    ? ApplicationDeployment.CurrentDeployment.CurrentVersion.Revision.ToString()
                    : Assembly.GetExecutingAssembly().GetName().Version.Revision.ToString();
            }
        }

        public string VersionStage
        {
            get { return Settings.Default.App_VersionStage; }
        }

        public string Copyright
        {
            get { return Settings.Default.App_Copyright; }
        }

        public string LastUpdatedString
        {
            get { return Settings.Default.App_LastUpdateDate.ToString("MMMM dd, yyyy - h:mm tt"); }
        }

    }
}