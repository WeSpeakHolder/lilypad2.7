using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public LoginViewModel()
        {
            mainlog = "";
        }

        private static string beginner = @"--/  Lilypad ";
        private static string ender = @"  /--------------------";
        private static string version = Properties.Settings.Default.App_Version.ToString();
        private static string lastver = Properties.Settings.Default.App_LastVersion.ToString();
        private static string sep = beginner + version + ender;
        private static string sep2 = beginner + lastver + ender;

        private static string bugfixsep = @"Bugs Fixed --------------------------------";
        private static string itemsaddedsep = @"Items Added ------------------------------";
        private static string changessep = @"Changes Implemented ----------------------";
        private static string knownissep = @"Known Issues -----------------------------";


        private string mainlog;
        public string MainLog
        {
            get { return mainlog; }
            set { mainlog = value; 
                OnPropertyChanged("MainLog");
                OnPropertyChanged("ThisVersionLog");
                OnPropertyChanged("BugFixes");
                OnPropertyChanged("ItemsAdded");
                OnPropertyChanged("ChangesMade");
                OnPropertyChanged("KnownIssues");
            }
        }

        public string ThisVersionLog
        {
            get
            {
                if (!string.IsNullOrEmpty(MainLog)) return MainLog.GetTextBetween(sep, sep2, true);
                else return "";
            }
        }

        public string BugFixes
        {
            get
            {
                if (!string.IsNullOrEmpty(ThisVersionLog)) return ThisVersionLog.GetTextBetween(bugfixsep, itemsaddedsep, false);
                else return "";
            }
        }
        public string ItemsAdded
        {
            get
            {
                if (!string.IsNullOrEmpty(ThisVersionLog)) return ThisVersionLog.GetTextBetween(itemsaddedsep, changessep, false);
                else return "";
            }
        }
        public string ChangesMade
        {
            get
            {
                if (!string.IsNullOrEmpty(ThisVersionLog)) return ThisVersionLog.GetTextBetween(changessep, knownissep, false);
                else return "";
            }
        }
        public string KnownIssues
        {
            get
            {
                if (!string.IsNullOrEmpty(ThisVersionLog)) return ThisVersionLog.GetTextBetween(knownissep, sep2, false);
                else return "";
            }
        }
        
    }
}