using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.Controls.Tags;


namespace Lilypad.ViewModel
{
    public class HopperArchivedViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public HopperArchivedViewModel()
        {
            tagcollection = new ObservableCollection<Tag>();
        }
        public void SaveChanges()
        {
            if (selectedorder != null)
            {
                using (Entities ctx = new Entities())
                {
                    order toEdit = (from k in ctx.orders where k.ReceiptNum == selectedorder.ReceiptNum select k).FirstOrDefault();
                    toEdit.BoxNum = selectedorder.BoxNum;
                    toEdit.CareTags = selectedorder.CareTags;
                    toEdit.Correspondence = selectedorder.Correspondence;
                    toEdit.DesignFrog = selectedorder.DesignFrog;
                    toEdit.DueDate = selectedorder.DueDate;
                    toEdit.Irregulars = selectedorder.Irregulars;
                    toEdit.JobFolderPath = selectedorder.JobFolderPath;
                    toEdit.Notes = selectedorder.Notes;
                    toEdit.OrderDate = selectedorder.OrderDate;
                    toEdit.PrintProcess = selectedorder.PrintProcess;
                    toEdit.Priority = selectedorder.Priority;
                    toEdit.Status = selectedorder.status1.StatusID;
                    toEdit.SearchTags = selectedorder.SearchTags;
                    ctx.SaveChanges();
                }
            }
        }

        public bool NeedsSave;

        public async void DispatchSaveConfirmScreen()
        {
            MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Changes", "Are you sure you would like to submit these changes to the order?");
            if (result == MessageDialogResult.Affirmative)
            {
                SaveChanges();
                NeedsSave = false;
            }
        }

        private order selectedorder;
        public order SelectedOrder
        {
            get { return selectedorder; }
            set {
                if (NeedsSave)
                {
                    DispatchSaveConfirmScreen();
                }
                else
                {
                    selectedorder = value;
                    OnPropertyChanged("SelectedOrder");
                    ResetTagCollection();
                } 
            }
        }

        private ObservableCollection<Tag> tagcollection;
        public ObservableCollection<Tag> TagCollection
        {
            get { return tagcollection; }
            set { tagcollection = value; OnPropertyChanged("TagCollection"); }
        }

        private string tagsasstring;
        public string TagsAsString
        {
            get { return tagsasstring; }
            set { tagsasstring = value; OnPropertyChanged("TagsAsString"); }
        }


        public void ResetTagCollection()
        {
            if (SelectedOrder != null)
            {
                if (tagcollection.Any()) { tagcollection.Clear(); }
                char[] sep = new char[] { ',' };
                string[] tags = selectedorder.SearchTags.Split(sep, StringSplitOptions.RemoveEmptyEntries).ToArray();

                foreach (string tg in tags)
                {
                    string ne = tg.Trim();
                    Controls.Tags.Tag newTag = new Controls.Tags.Tag(ne);
                    newTag.CanDelete = false;
                    tagcollection.Add(newTag);
                }
            }
        }
    }
}   
