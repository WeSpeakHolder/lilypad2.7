using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using DevExpress.Xpf.Mvvm;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.Controls.Tags;
using Lilypad.Properties;

namespace Lilypad.ViewModel
{
    public class AlphaViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public AlphaViewModel()
        {
            var parsedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd") + " " + Properties.Settings.Default.Interop_AlphaDeadlineTime);
            DeadLine = parsedDate;
            DeadLineCountdown = new CountdownViewModel(DeadLine);
            currentorderItems = new ObservableCollection<alphacurrentorder>();
            currentorderItems.CollectionChanged += currentorderItems_CollectionChanged;
        }

        void currentorderItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            decimal sum = 0.00m;

            foreach (alphacurrentorder order in CurrentOrderItems)
            {
                if (order.SalePrice == 0.00m)
                {
                    decimal quant = order.Quantity * order.UnitPrice;
                    order.LineCost = quant;
                    sum += quant;
                }
                else
                {
                    decimal quant = order.Quantity * order.SalePrice;
                    order.LineCost = quant;
                    sum += quant;
                }
            }

            int sum3 = CurrentOrderItems.Sum(x => x.Quantity);
            TotalItemCount = sum3;
            EstimatedPrice = sum;
        }

        private alphaorder pastorder;
        public alphaorder SelectedPastOrder
        {
            get { return pastorder; }
            set { pastorder = value; OnPropertyChanged("SelectedPastOrder"); }
        }



        bool canCheckDetails;
        public bool CanCheckDetails
        {
            get { return canCheckDetails; }
            set { canCheckDetails = value; OnPropertyChanged("CanCheckDetails"); }
        }

        bool transmitready;
        public bool TransmitReady
        {
            get { return transmitready; }
            set { transmitready = value; OnPropertyChanged("TransmitReady"); }
        }

        alphacurrentorder selectedItem;
        public alphacurrentorder SelectedAlphaItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value; OnPropertyChanged("SelectedAlphaItem");
            }
        }

        alphacurrentorder newItem;
        public alphacurrentorder NewItem
        {
            get { return newItem; }
            set
            {
                newItem = value; OnPropertyChanged("NewItem");
            }
        }

        ObservableCollection<alphacurrentorder> currentorderItems;
        public ObservableCollection<alphacurrentorder> CurrentOrderItems
        {
            get { return currentorderItems; }
            set { currentorderItems = value; OnPropertyChanged("CurrentOrderItems"); }
        }

        int totalitems;
        public int TotalItemCount
        {
            get
            {
                return totalitems;
            }
            set
            {
                totalitems = value; OnPropertyChanged("TotalItemCount"); 
            }
        }

        decimal estprice;
        public decimal EstimatedPrice
        {
            get { return estprice; }
            set { estprice = value; OnPropertyChanged("EstimatedPrice"); }
        }

        DateTime deadline;
        public DateTime DeadLine
        {
            get { return deadline; }
            set { deadline = value; OnPropertyChanged("DeadLine"); }
        }

        public CountdownViewModel DeadLineCountdown { get; private set; }

        DelegateCommand saveCommand;
        public DelegateCommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new DelegateCommand(Save);
                }
                return saveCommand;
            }
        }

        void Save()
        {

        }
    }
}