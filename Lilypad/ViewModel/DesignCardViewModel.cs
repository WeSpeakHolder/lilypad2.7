using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    public class DesignCardViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public DesignCardViewModel()
        {
            IsBlankCard = true;
            
        }

        private int designnum;
        public int DesignNum
        {
            get { return designnum; }
            set { designnum = value; OnPropertyChanged("DesignNum"); }
        }

        private designdetail selecteddesigndetail;
        public designdetail SelectedDesignDetail
        {
            get { return selecteddesigndetail; }
            set { selecteddesigndetail = value; OnPropertyChanged("SelectedDesignDetail"); }
        }


        private IList<LineViewModel> lineitems;
        public IList<LineViewModel> LineItems
        {
            get { return lineitems; }
            set { lineitems = value; OnPropertyChanged("LineItems"); }
        }

        private LineViewModel selectedlineitem;
        public LineViewModel SelectedLineItem
        {
            get { return selectedlineitem; }
            set { selectedlineitem = value; OnPropertyChanged("SelectedLineItem"); }
        }

        public async void CopyArtFile(string artFile, string Type, string destination)
        {
            string datapth = Properties.Settings.Default.Data_TemplatesPath.ToString();
            string sourceFile;
            if (Type == "A")
            {
                sourceFile = datapth + "\\";
            }
            else if (Type == "Y")
            {
                sourceFile = datapth + @"\Youth\";
            }
            else if (Type == "S")
            {
                sourceFile = datapth + @"\Special\";
            }
            else
            {
                sourceFile = datapth + "\\";
            }
            string artname = artFile + ".cdr";
            string source = sourceFile + artname;
            string destFinal = destination + artname;

            if (!Directory.Exists(destination))
            {
                Directory.CreateDirectory(destination);
            }

            if (File.Exists(destFinal))
            {
                MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Overwrite", "The file already exists in the target directory. Would you like to overwrite it?");
                if (result == MessageDialogResult.Affirmative)
                {
                    try
                    {
                        File.Copy(source, destFinal, true);
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }
            }
            else
            {
                try
                {
                    File.Copy(source, destFinal, true);
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }

        public void AddNewArtworkIfAny(object parameter)
        {
            int movedTotal = 0;
            if (FlaggedArtworkItems.Any())
            {
                try
                {
                    foreach (string item in FlaggedArtworkItems)
                    {
                        string type = "";
                        string destination = SelectedDesignDetail.DesignFolderPath + @"\CDR\";
                        bool isYouth = SelectedDesignDetail.AP_YOUTH;

                        if (item == "Apron" || item == "Bandana" || item == "CapFront" || item == "RoundCoasters" || item == "GolfFlag"
                                || item == "GolfTowel" || item == "Koozies" || item == "Mousepad" || item == "Puzzle" || item == "SquareCoasters" || item == "ToteBag" || item == "WineBag")
                        {
                            type = "S";
                      //      CopyArtFile(item, type, destination);
                            movedTotal = movedTotal + 1;
                        }
                        else
                        {
                            if (isYouth)
                            {
                                type = "Y";
                          //      CopyArtFile("Youth" + item, type, destination);
                                movedTotal = movedTotal + 1;
                            }
                            else if (!isYouth)
                            {
                                type = "A";
                       //         CopyArtFile(item, type, destination);
                                movedTotal = movedTotal + 1;
                            }
                        }

                        if (!CurrentArtworkItems.Any(x => x == item))
                        {
                            CurrentArtworkItems.Add(item);
                        }
                    }

                    FlaggedArtworkItems.Clear();
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }

        ObservableCollection<string> flaggeditems;
        public ObservableCollection<string> FlaggedArtworkItems
        {
            get
            {
                if (flaggeditems == null)
                {
                    flaggeditems = new ObservableCollection<string>();
                }
                return flaggeditems;
            }
            set
            {
                flaggeditems = value; OnPropertyChanged("FlaggedArtworkItems");
            }
        }

        ObservableCollection<string> currentartitems;
        public ObservableCollection<string> CurrentArtworkItems
        {
            get
            {
                if (currentartitems == null)
                {
                    currentartitems = new ObservableCollection<string>();
                }
                return currentartitems;
            }
            set
            {
                currentartitems = value; OnPropertyChanged("CurrentArtworkItems");
            }
        }

        bool isBlankCard;
        public bool IsBlankCard
        {
            get
            {
                return isBlankCard;
            }
            set
            {
                isBlankCard = value; OnPropertyChanged("IsBlankCard");
            }
        }

        int shirtcount;
        public int ShirtCount
        {
            get
            {
                if (LineItems != null)
                {
                    shirtcount = (from q in LineItems select q).Sum(x => x.SelectedLineItem.Quantity);
                }
                return shirtcount;
            }
            set { shirtcount = value; OnPropertyChanged("ShirtCount"); OnPropertyChanged("ArtCount"); OnPropertyChanged("PrintCount"); }
        }

        int artcount;
        public int ArtCount
        {
            get
            {
                return artcount;
            }
            set { artcount = value; OnPropertyChanged("ShirtCount"); OnPropertyChanged("ArtCount"); OnPropertyChanged("PrintCount"); }
        }

        int printcount;
        public int PrintCount
        {
            get
            {
                printcount = ShirtCount * ArtCount;

                return printcount;
            }

            set { printcount = value; OnPropertyChanged("ShirtCount"); OnPropertyChanged("ArtCount"); OnPropertyChanged("PrintCount"); }
        }


        int designid;
        public int CardDesignID
        {
            get
            {
                if (designid == 0)
                {
                    if (SelectedDesignDetail != null)
                    {
                        designid = SelectedDesignDetail.DesignNum;
                    }
                }
                return designid;
            }
            set { designid = value; OnPropertyChanged("CardDesignID"); }
        }

        bool busy;
        public bool Busy
        {
            get { return busy; }
            set { busy = value; OnPropertyChanged("Busy"); }
        }

        bool issaved;
        public bool IsSaved
        {
            get { return issaved; }
            set { issaved = value; OnPropertyChanged("IsSaved"); }
        }

        public void SaveChanges()
        {
            Busy = true;
            if (IsBlankCard)
            {
                FlaggedArtworkItems.Clear();
                CurrentArtworkItems.Clear();
                if (SelectedDesignDetail.AP_Apron && !CurrentArtworkItems.Any(x => x == "Apron")) { FlaggedArtworkItems.Add("Apron"); }
                if (SelectedDesignDetail.AP_Back && !CurrentArtworkItems.Any(x => x == "Back")) { FlaggedArtworkItems.Add("Back"); }
                if (SelectedDesignDetail.AP_BackBottom && !CurrentArtworkItems.Any(x => x == "BackBottom")) { FlaggedArtworkItems.Add("BackBottom"); }
                if (SelectedDesignDetail.AP_Bandana && !CurrentArtworkItems.Any(x => x == "Bandana")) { FlaggedArtworkItems.Add("Bandana"); }
                if (SelectedDesignDetail.AP_Cap && !CurrentArtworkItems.Any(x => x == "CapFront")) { FlaggedArtworkItems.Add("CapFront"); }
                if (SelectedDesignDetail.AP_ClCoaster && !CurrentArtworkItems.Any(x => x == "RoundCoasters")) { FlaggedArtworkItems.Add("RoundCoasters"); }
                if (SelectedDesignDetail.AP_Collar && !CurrentArtworkItems.Any(x => x == "Collar")) { FlaggedArtworkItems.Add("Collar"); }
                if (SelectedDesignDetail.AP_FLC && !CurrentArtworkItems.Any(x => x == "FrontLeftCrest")) { FlaggedArtworkItems.Add("FrontLeftCrest"); }
                if (SelectedDesignDetail.AP_FLCW && !CurrentArtworkItems.Any(x => x == "WomensFrontLeftCrest")) { FlaggedArtworkItems.Add("WomensFrontLeftCrest"); }
                if (SelectedDesignDetail.AP_FRC && !CurrentArtworkItems.Any(x => x == "FrontRightCrest")) { FlaggedArtworkItems.Add("FrontRightCrest"); }
                if (SelectedDesignDetail.AP_FRCW && !CurrentArtworkItems.Any(x => x == "WomensFrontRightCrest")) { FlaggedArtworkItems.Add("WomensFrontRightCrest"); }
                if (SelectedDesignDetail.AP_Front && !CurrentArtworkItems.Any(x => x == "Front")) { FlaggedArtworkItems.Add("Front"); }
                if (SelectedDesignDetail.AP_FrontBottom && !CurrentArtworkItems.Any(x => x == "FrontBottom")) { FlaggedArtworkItems.Add("FrontBottom"); }
                if (SelectedDesignDetail.AP_GolfFlag && !CurrentArtworkItems.Any(x => x == "GolfFlag")) { FlaggedArtworkItems.Add("GolfFlag"); }
                if (SelectedDesignDetail.AP_GolfTowel && !CurrentArtworkItems.Any(x => x == "GolfTowel")) { FlaggedArtworkItems.Add("GolfTowel"); }
                if (SelectedDesignDetail.AP_Koozie && !CurrentArtworkItems.Any(x => x == "Koozies")) { FlaggedArtworkItems.Add("Koozies"); }
                if (SelectedDesignDetail.AP_LeftSide && !CurrentArtworkItems.Any(x => x == "LeftSide")) { FlaggedArtworkItems.Add("LeftSide"); }
                if (SelectedDesignDetail.AP_LeftSleeve && !CurrentArtworkItems.Any(x => x == "LeftSleeve")) { FlaggedArtworkItems.Add("LeftSleeve"); }
                if (SelectedDesignDetail.AP_Mousepad && !CurrentArtworkItems.Any(x => x == "Mousepad")) { FlaggedArtworkItems.Add("Mousepad"); }
                if (SelectedDesignDetail.AP_Puzzle && !CurrentArtworkItems.Any(x => x == "Puzzle")) { FlaggedArtworkItems.Add("Puzzle"); }
                if (SelectedDesignDetail.AP_RightSide && !CurrentArtworkItems.Any(x => x == "RightSide")) { FlaggedArtworkItems.Add("RightSide"); }
                if (SelectedDesignDetail.AP_RightSleeve && !CurrentArtworkItems.Any(x => x == "RightSleeve")) { FlaggedArtworkItems.Add("RightSleeve"); }
                if (SelectedDesignDetail.AP_SqCoaster && !CurrentArtworkItems.Any(x => x == "SquareCoasters")) { FlaggedArtworkItems.Add("SquareCoasters"); }
                if (SelectedDesignDetail.AP_ToteBag && !CurrentArtworkItems.Any(x => x == "ToteBag")) { FlaggedArtworkItems.Add("ToteBag"); }
                if (SelectedDesignDetail.AP_WineBag && !CurrentArtworkItems.Any(x => x == "WineBag")) { FlaggedArtworkItems.Add("WineBag"); }
                if (SelectedDesignDetail.AP_Yoke && !CurrentArtworkItems.Any(x => x == "Yoke")) { FlaggedArtworkItems.Add("Yoke"); }

                AddNewArtworkIfAny(null);
                IsSaved = true;
                OnPropertyChanged("LineItems");
                OnPropertyChanged("ShirtCount"); OnPropertyChanged("ArtCount"); OnPropertyChanged("PrintCount");
            }
            else
            {
                using (Entities ctx = new Entities())
                {
                    try
                    {
                        var ThisDetail = (from q in ctx.designdetails where q.DesignID == SelectedDesignDetail.DesignID select q).FirstOrDefault();

                        // Update designdetails from Viewmodel back to database
                        ThisDetail.DesignFolderPath = SelectedDesignDetail.DesignFolderPath;
                        ThisDetail.DesignDescription = SelectedDesignDetail.DesignDescription;

                        if (SelectedDesignDetail.AP_Apron && ThisDetail.AP_Apron != SelectedDesignDetail.AP_Apron) { FlaggedArtworkItems.Add("Apron"); } ThisDetail.AP_Apron = SelectedDesignDetail.AP_Apron;
                        if (SelectedDesignDetail.AP_Back && ThisDetail.AP_Back != SelectedDesignDetail.AP_Back) { FlaggedArtworkItems.Add("Back"); } ThisDetail.AP_Back = SelectedDesignDetail.AP_Back;
                        if (SelectedDesignDetail.AP_BackBottom && ThisDetail.AP_BackBottom != SelectedDesignDetail.AP_BackBottom) { FlaggedArtworkItems.Add("BackBottom"); } ThisDetail.AP_BackBottom = SelectedDesignDetail.AP_BackBottom;
                        if (SelectedDesignDetail.AP_Bandana && ThisDetail.AP_Bandana != SelectedDesignDetail.AP_Bandana) { FlaggedArtworkItems.Add("Bandana"); } ThisDetail.AP_Bandana = SelectedDesignDetail.AP_Bandana;
                        if (SelectedDesignDetail.AP_Cap && ThisDetail.AP_Cap != SelectedDesignDetail.AP_Cap) { FlaggedArtworkItems.Add("CapFront"); } ThisDetail.AP_Cap = SelectedDesignDetail.AP_Cap;
                        if (SelectedDesignDetail.AP_ClCoaster && ThisDetail.AP_ClCoaster != SelectedDesignDetail.AP_ClCoaster) { FlaggedArtworkItems.Add("RoundCoasters"); } ThisDetail.AP_ClCoaster = SelectedDesignDetail.AP_ClCoaster;
                        if (SelectedDesignDetail.AP_Collar && ThisDetail.AP_Collar != SelectedDesignDetail.AP_Collar) { FlaggedArtworkItems.Add("Collar"); } ThisDetail.AP_Collar = SelectedDesignDetail.AP_Collar;
                        if (SelectedDesignDetail.AP_FLC && ThisDetail.AP_FLC != SelectedDesignDetail.AP_FLC) { FlaggedArtworkItems.Add("FrontLeftCrest"); } ThisDetail.AP_FLC = SelectedDesignDetail.AP_FLC;
                        if (SelectedDesignDetail.AP_FLCW && ThisDetail.AP_FLCW != SelectedDesignDetail.AP_FLCW) { FlaggedArtworkItems.Add("WomensFrontLeftCrest"); } ThisDetail.AP_FLCW = SelectedDesignDetail.AP_FLCW;
                        if (SelectedDesignDetail.AP_FRC && ThisDetail.AP_FRC != SelectedDesignDetail.AP_FRC) { FlaggedArtworkItems.Add("FrontRightCrest"); } ThisDetail.AP_FRC = SelectedDesignDetail.AP_FRC;
                        if (SelectedDesignDetail.AP_FRCW && ThisDetail.AP_FRCW != SelectedDesignDetail.AP_FRCW) { FlaggedArtworkItems.Add("WomensFrontRightCrest"); } ThisDetail.AP_FRCW = SelectedDesignDetail.AP_FRCW;
                        if (SelectedDesignDetail.AP_Front && ThisDetail.AP_Front != SelectedDesignDetail.AP_Front) { FlaggedArtworkItems.Add("Front"); } ThisDetail.AP_Front = SelectedDesignDetail.AP_Front;
                        if (SelectedDesignDetail.AP_FrontBottom && ThisDetail.AP_FrontBottom != SelectedDesignDetail.AP_FrontBottom) { FlaggedArtworkItems.Add("FrontBottom"); } ThisDetail.AP_FrontBottom = SelectedDesignDetail.AP_FrontBottom;
                        if (SelectedDesignDetail.AP_GolfFlag && ThisDetail.AP_GolfFlag != SelectedDesignDetail.AP_GolfFlag) { FlaggedArtworkItems.Add("GolfFlag"); } ThisDetail.AP_GolfFlag = SelectedDesignDetail.AP_GolfFlag;
                        if (SelectedDesignDetail.AP_GolfTowel && ThisDetail.AP_GolfTowel != SelectedDesignDetail.AP_GolfTowel) { FlaggedArtworkItems.Add("GolfTowel"); } ThisDetail.AP_GolfTowel = SelectedDesignDetail.AP_GolfTowel;
                        if (SelectedDesignDetail.AP_Koozie && ThisDetail.AP_Koozie != SelectedDesignDetail.AP_Koozie) { FlaggedArtworkItems.Add("Koozies"); } ThisDetail.AP_Koozie = SelectedDesignDetail.AP_Koozie;
                        if (SelectedDesignDetail.AP_LeftSide && ThisDetail.AP_LeftSide != SelectedDesignDetail.AP_LeftSide) { FlaggedArtworkItems.Add("LeftSide"); } ThisDetail.AP_LeftSide = SelectedDesignDetail.AP_LeftSide;
                        if (SelectedDesignDetail.AP_LeftSleeve && ThisDetail.AP_LeftSleeve != SelectedDesignDetail.AP_LeftSleeve) { FlaggedArtworkItems.Add("LeftSleeve"); } ThisDetail.AP_LeftSleeve = SelectedDesignDetail.AP_LeftSleeve;
                        if (SelectedDesignDetail.AP_Mousepad && ThisDetail.AP_Mousepad != SelectedDesignDetail.AP_Mousepad) { FlaggedArtworkItems.Add("Mousepad"); } ThisDetail.AP_Mousepad = SelectedDesignDetail.AP_Mousepad;
                        if (SelectedDesignDetail.AP_Puzzle && ThisDetail.AP_Puzzle != SelectedDesignDetail.AP_Puzzle) { FlaggedArtworkItems.Add("Puzzle"); } ThisDetail.AP_Puzzle = SelectedDesignDetail.AP_Puzzle;
                        if (SelectedDesignDetail.AP_RightSide && ThisDetail.AP_RightSide != SelectedDesignDetail.AP_RightSide) { FlaggedArtworkItems.Add("RightSide"); } ThisDetail.AP_RightSide = SelectedDesignDetail.AP_RightSide;
                        if (SelectedDesignDetail.AP_RightSleeve && ThisDetail.AP_RightSleeve != SelectedDesignDetail.AP_RightSleeve) { FlaggedArtworkItems.Add("RightSleeve"); } ThisDetail.AP_RightSleeve = SelectedDesignDetail.AP_RightSleeve;
                        if (SelectedDesignDetail.AP_SqCoaster && ThisDetail.AP_SqCoaster != SelectedDesignDetail.AP_SqCoaster) { FlaggedArtworkItems.Add("SquareCoasters"); } ThisDetail.AP_SqCoaster = SelectedDesignDetail.AP_SqCoaster;
                        if (SelectedDesignDetail.AP_ToteBag && ThisDetail.AP_ToteBag != SelectedDesignDetail.AP_ToteBag) { FlaggedArtworkItems.Add("ToteBag"); } ThisDetail.AP_ToteBag = SelectedDesignDetail.AP_ToteBag;
                        if (SelectedDesignDetail.AP_WineBag && ThisDetail.AP_WineBag != SelectedDesignDetail.AP_WineBag) { FlaggedArtworkItems.Add("WineBag"); } ThisDetail.AP_WineBag = SelectedDesignDetail.AP_WineBag;
                        if (SelectedDesignDetail.AP_Yoke && ThisDetail.AP_Yoke != SelectedDesignDetail.AP_Yoke) { FlaggedArtworkItems.Add("Yoke"); } ThisDetail.AP_Yoke = SelectedDesignDetail.AP_Yoke;
                        ThisDetail.AP_YOUTH = SelectedDesignDetail.AP_YOUTH;
                        ThisDetail.SizeToGarment = SelectedDesignDetail.SizeToGarment;

                        AddNewArtworkIfAny(null);

                        // Update lineitems from Viewmodel back to database

                        foreach (LineViewModel item in LineItems)
                        {
                            if (item.SelectedLineItem.UniqueID != 0)
                            {
                                int Key = item.SelectedLineItem.UniqueID;
                                designdetailslineitem itemToEdit = (from k in ctx.designdetailslineitems where k.UniqueID == Key select k).FirstOrDefault();
                                itemToEdit.Color = item.SelectedLineItem.Color;
                                itemToEdit.Description = item.SelectedLineItem.Description;
                                itemToEdit.DesignID = item.SelectedLineItem.DesignID;
                                itemToEdit.Quantity = item.SelectedLineItem.Quantity;
                                itemToEdit.ReceiptNum = item.SelectedLineItem.ReceiptNum;
                                itemToEdit.Size = item.SelectedLineItem.Size;
                                itemToEdit.StockIDPOS = item.SelectedLineItem.StockIDPOS;
                            }
                            // ctx.SaveChanges();
                        }


                        ctx.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                    finally
                    {
                        IsSaved = true;
                        OnPropertyChanged("LineItems");
                        OnPropertyChanged("ShirtCount"); OnPropertyChanged("ArtCount"); OnPropertyChanged("PrintCount");
                    }
                }
            }
        }
    }
}   
