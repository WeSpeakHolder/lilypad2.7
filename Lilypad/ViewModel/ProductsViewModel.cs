using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    public class ProductsViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public ProductsViewModel()
        {
            relateditems = new List<alphaitem>();
            colorslist = new List<alphaitem>();
        }

        public ProductsViewModel(string input)
        {
            using (Entities ctx = new Entities())
            {
                selecteditem = (from q in ctx.alphaitems where q.Style_Code == input select q).FirstOrDefault();
            }
        }

        public bool IsExpanderVisible
        {
            get { return false; }
        }

        private int designnum;
        public int DesignNum
        {
            get { return designnum; }
            set { designnum = value; OnPropertyChanged("DesignNum"); }
        }

        private string lookedupitem;
        public string LookedUpItem
        {
            get { return lookedupitem; }
            set { lookedupitem = value; OnPropertyChanged("LookedUpItem"); }
        }

        private alphaitem selecteditem;
        public alphaitem SelectedItem
        {
            get { return selecteditem; }
            set { selecteditem = value; OnPropertyChanged("SelectedItem"); }
        }


        private IList<alphaitem> relateditems;
        public IList<alphaitem> RelatedItems
        {
            get { return relateditems; }
            set { relateditems = value; OnPropertyChanged("RelatedItems"); }
        }

        private List<alphaitem> colorslist;
        public List<alphaitem> ColorsList
        {
            get { return colorslist; }
            set { colorslist = value; OnPropertyChanged("ColorsList"); }
        }
    }
}   
