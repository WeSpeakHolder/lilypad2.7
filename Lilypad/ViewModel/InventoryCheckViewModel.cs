using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    public class InventoryCheckViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public InventoryCheckViewModel()
        {

        }

        private int sizeindex;
        public int SizeIndex
        {
            get { return sizeindex; }
            set { sizeindex = value; OnPropertyChanged("SizeIndex"); }
        }

        private string sizecode;
        public string SizeCode
        {
            get { return sizecode; }
            set { sizecode = value; OnPropertyChanged("SizeCode"); }
        }

        private string sizename;
        public string SizeName
        {
            get { return sizename; }
            set { sizename = value; OnPropertyChanged("SizeName"); }
        }

        private string itemnumber;
        public string ItemNumber
        {
            get { return itemnumber; }
            set { itemnumber = value; OnPropertyChanged("ItemNumber"); }
        }

        private decimal price;
        public decimal Price
        {
            get { return price; }
            set { price = value; OnPropertyChanged("Price"); }
        }

        private string specialprice;
        public string SpecialPrice
        {
            get { return specialprice; }
            set { specialprice = value; OnPropertyChanged("SpecialPrice"); }
        }

        private bool isspecialprice;
        public bool IsSpecialPrice
        {
            get { return isspecialprice; }
            set { isspecialprice = value; OnPropertyChanged("IsSpecialPrice"); }
        }

        private string specialexpiry;
        public string SpecialExpiry
        {
            get { return specialexpiry; }
            set { specialexpiry = value; OnPropertyChanged("SpecialExpiry"); }
        }

        private string whse_seattle;
        public string WHSE_Seattle { get { return whse_seattle; } set { whse_seattle = value; OnPropertyChanged("WHSE_Seattle"); } }

        private string whse_fresno;
        public string WHSE_Fresno { get { return whse_fresno; } set { whse_fresno = value; OnPropertyChanged("WHSE_Fresno"); } }

        private string whse_denver;
        public string WHSE_Denver { get { return whse_denver; } set { whse_denver = value; OnPropertyChanged("WHSE_Denver"); } }

        private string whse_lenexa;
        public string WHSE_Lenexa { get { return whse_lenexa; } set { whse_lenexa = value; OnPropertyChanged("WHSE_Lenexa"); } }

        private string whse_dallas;
        public string WHSE_Dallas { get { return whse_dallas; } set { whse_dallas = value; OnPropertyChanged("WHSE_Dallas"); } }

        private string whse_houston;
        public string WHSE_Houston { get { return whse_houston; } set { whse_houston = value; OnPropertyChanged("WHSE_Houston"); } }

        private string whse_verona;
        public string WHSE_Verona { get { return whse_verona; } set { whse_verona = value; OnPropertyChanged("WHSE_Verona"); } }

        private string whse_chicago;
        public string WHSE_Chicago { get { return whse_chicago; } set { whse_chicago = value; OnPropertyChanged("WHSE_Chicago"); } }

        private string whse_plymouth;
        public string WHSE_Plymouth { get { return whse_plymouth; } set { whse_plymouth = value; OnPropertyChanged("WHSE_Plymouth"); } }

        private string whse_middleboro;
        public string WHSE_Middleboro { get { return whse_middleboro; } set { whse_middleboro = value; OnPropertyChanged("WHSE_Middleboro"); } }

        private string whse_harrisburg;
        public string WHSE_Harrisburg { get { return whse_harrisburg; } set { whse_harrisburg = value; OnPropertyChanged("WHSE_Harrisburg"); } }

        private string whse_charlotte;
        public string WHSE_Charlotte { get { return whse_charlotte; } set { whse_charlotte = value; OnPropertyChanged("WHSE_Charlotte"); } }

        private string whse_atlanta;
        public string WHSE_Atlanta { get { return whse_atlanta; } set { whse_atlanta = value; OnPropertyChanged("WHSE_Atlanta"); } }

        private string whse_orlando;
        public string WHSE_Orlando { get { return whse_orlando; } set { whse_orlando = value; OnPropertyChanged("WHSE_Orlando"); } }

    }
}   
