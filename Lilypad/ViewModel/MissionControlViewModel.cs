using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;

namespace Lilypad.ViewModel
{
    public class MissionControlViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public MissionControlViewModel()
        {
            settingsabout = new RelayCommand(OpenSettings_About);
            settingsalphabroder = new RelayCommand(OpenSettings_Alphabroder);
            settingsautomation = new RelayCommand(OpenSettings_Automation);
            settingsbusiness = new RelayCommand(OpenSettings_Business);
            settingsinterface = new RelayCommand(OpenSettings_Interface);
            settingsdatabases = new RelayCommand(OpenSettings_Databases);
            settingsemployees = new RelayCommand(OpenSettings_Employees);
            settingsinterop = new RelayCommand(OpenSettings_Interop);
            settingssystem = new RelayCommand(OpenSettings_System);
            settingstools = new RelayCommand(OpenSettings_Tools);
            if (currentcontent == null) { currentcontent = new Views.Settings.About(); }
        }

        private object currentcontent;
        public object CurrentContent
        {
            get { return currentcontent; }
            set { currentcontent = value; OnPropertyChanged("CurrentContent"); }
        }

        private RelayCommand settingsabout;
        public RelayCommand Settings_About { get { return settingsabout; } }

        private RelayCommand settingsalphabroder;
        public RelayCommand Settings_Alphabroder { get { return settingsalphabroder; } }

        private RelayCommand settingsautomation;
        public RelayCommand Settings_Automation { get { return settingsautomation; } }

        private RelayCommand settingsbusiness;
        public RelayCommand Settings_Business { get { return settingsbusiness; } }

        private RelayCommand settingsdatabases;
        public RelayCommand Settings_Databases { get { return settingsdatabases; } }

        private RelayCommand settingsemployees;
        public RelayCommand Settings_Employees { get { return settingsemployees; } }

        private RelayCommand settingsinterface;
        public RelayCommand Settings_Interface { get { return settingsinterface; } }

        private RelayCommand settingsinterop;
        public RelayCommand Settings_Interop { get { return settingsinterop; } }

        private RelayCommand settingssystem;
        public RelayCommand Settings_System { get { return settingssystem; } }

        private RelayCommand settingstools;
        public RelayCommand Settings_Tools { get { return settingstools; } }

        public void OpenSettings_About()
        {
            CurrentContent = new Views.Settings.About();
        }

        public void OpenSettings_Alphabroder()
        {
            CurrentContent = new Views.Settings.Alphabroder();
        }

        public void OpenSettings_Automation()
        {
            CurrentContent = new Views.Settings.Automation();
        }

        public void OpenSettings_Business()
        {
            CurrentContent = new Views.Settings.Business();
        }
        public void OpenSettings_Databases()
        {
            CurrentContent = new Views.Settings.Databases();
        }

        public void OpenSettings_Employees()
        {
            CurrentContent = new Views.Settings.Employees();
        }

        public void OpenSettings_MyProfile()
        {
            CurrentContent = new Views.Settings.Employees(true);
        }

        public void OpenSettings_Interface()
        {
            CurrentContent = new Views.Settings.Interface();
        }
        public void OpenSettings_Interop()
        {
            CurrentContent = new Views.Settings.Interop();
        }
        public void OpenSettings_System()
        {
            CurrentContent = new Views.Settings.SystemSettings();
        }
        public void OpenSettings_Tools()
        {
            CurrentContent = new Views.Settings.Tools();
        }
    }
}