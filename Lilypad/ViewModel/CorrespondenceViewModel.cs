using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using DevExpress.Xpf.Mvvm;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.Controls.Tags;
using Lilypad.Properties;

namespace Lilypad.ViewModel
{
    public class CorrespondenceViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        hopperorder completedOrder;
        public hopperorder CompletedOrder
        {
            get { return completedOrder; }
            set
            {
                completedOrder = value; OnPropertyChanged("CompletedOrder");
            }
        }

        string emailTo;
        public string EmailTo
        {
            get { return emailTo; }
            set { emailTo = value; OnPropertyChanged("EmailTo"); }
        }

        string emailSubject;
        public string EmailSubject
        {
            get { return emailSubject; }
            set { emailSubject = value; OnPropertyChanged("EmailSubject"); }
        }

        string emailText;
        public string EmailText
        {
            get { return emailText; }
            set { emailText = value; OnPropertyChanged("EmailText"); }
        }

        DateTime calltime;
        public DateTime CallTime
        {
            get { return calltime; }
            set { calltime = value; OnPropertyChanged("CallTime"); }
        }
    }
}