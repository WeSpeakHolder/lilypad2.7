using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    [Serializable]
    public class LineViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public LineViewModel()
        {
            SelectedLineItem = new designdetailslineitem();
        }

        public LineViewModel(designdetailslineitem input)
        {
            selectedlineitem = new designdetailslineitem();
            SelectedLineItem.Color = input.Color;
            SelectedLineItem.Description = input.Description;
            SelectedLineItem.DesignID = input.DesignID;
            SelectedLineItem.Quantity = input.Quantity;
            SelectedLineItem.ReceiptNum = input.ReceiptNum;
            SelectedLineItem.Size = input.Size;
            SelectedLineItem.StockIDPOS = input.StockIDPOS;
            SelectedLineItem.UniqueID = input.UniqueID;
        }


        private int linenum;
        public int LineNum
        {
            get { return linenum; }
            set { linenum = value; OnPropertyChanged("LineNum"); }
        }


        private int designnum;
        public int DesignNum
        {
            get { return designnum; }
            set { designnum = value; OnPropertyChanged("DesignNum"); }
        }

        decimal linecost;
        public decimal LineCost
        {
            get { return linecost; }
            set { linecost = value; OnPropertyChanged("LineCost"); }
        }

        decimal lineprice;
        public decimal LinePrice
        {
            get { return lineprice; }
            set { lineprice = value; OnPropertyChanged("LinePrice"); }
        }

        decimal lineextend;
        public decimal LineExtend
        {
            get { return lineextend; }
            set { lineextend = value; OnPropertyChanged("LineExtend"); }
        }

        string linestockid;
        public string LineStockID
        {
            get { return linestockid; }
            set { linestockid = value; OnPropertyChanged("LineStockID"); }
        }

        designdetailslineitem selectedlineitem;
        public designdetailslineitem SelectedLineItem
        {
            get { return selectedlineitem; }
            set { selectedlineitem = value; OnPropertyChanged("SelectedLineItem"); }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }
    }
}