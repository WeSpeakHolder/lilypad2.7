using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using GalaSoft.MvvmLight.Command;
using Lilypad.Model;

namespace Lilypad.ViewModel
{
    public class InventoryOrderDetailsViewModel : INotifyPropertyChanged
    {
        private bool canCheckDetails;
        private IList<alphaentries1> currentorderItems;
        private DateTime deadline;
        private alphaentries1 detailitem;
        private decimal estprice;
        private alphacurrentorder newItem;
        private RelayCommand saveCommand;
        private string thisorderalphaid;
        private string thisorderid;
        private int totalitems;
        private bool transmitready;

        public InventoryOrderDetailsViewModel()
        {
            currentorderItems = new ObservableCollection<alphaentries1>();
        }

        public alphaentries1 SelectedDetailItem
        {
            get { return detailitem; }
            set
            {
                detailitem = value;
                OnPropertyChanged("SelectedDetailItem");
            }
        }

        public string ThisOrderID
        {
            get { return thisorderid; }
            set
            {
                thisorderid = value;
                OnPropertyChanged("ThisOrderID");
            }
        }

        public string ThisOrderAlphaID
        {
            get { return thisorderalphaid; }
            set
            {
                thisorderalphaid = value;
                OnPropertyChanged("ThisOrderAlphaID");
            }
        }

        public bool CanCheckDetails
        {
            get { return canCheckDetails; }
            set
            {
                canCheckDetails = value;
                OnPropertyChanged("CanCheckDetails");
            }
        }

        public bool TransmitReady
        {
            get { return transmitready; }
            set
            {
                transmitready = value;
                OnPropertyChanged("TransmitReady");
            }
        }

        public IList<alphaentries1> CurrentOrderItems
        {
            get { return currentorderItems; }
            set
            {
                currentorderItems = value;
                OnPropertyChanged("CurrentOrderItems");
            }
        }

        public alphacurrentorder NewItem
        {
            get { return newItem; }
            set
            {
                newItem = value;
                OnPropertyChanged("NewItem");
            }
        }

        public int TotalItemCount
        {
            get { return totalitems; }
            set
            {
                totalitems = value;
                OnPropertyChanged("TotalItemCount");
            }
        }

        public decimal EstimatedPrice
        {
            get { return estprice; }
            set
            {
                estprice = value;
                OnPropertyChanged("EstimatedPrice");
            }
        }

        public DateTime DeadLine
        {
            get { return deadline; }
            set
            {
                deadline = value;
                OnPropertyChanged("DeadLine");
            }
        }

        public RelayCommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new RelayCommand(Save);
                }
                return saveCommand;
            }
        }

        private void Save()
        {
        }

        #region PropertyChanged Controller

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }

        #endregion PropertyChanged Controller
    }
}