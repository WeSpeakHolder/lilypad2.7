using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    public class LineItemViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public LineItemViewModel()
        {
            
        }

        designdetailslineitem selectedlineitem;
        public designdetailslineitem SelectedLineItem
        {
            get { return selectedlineitem; }
            set { selectedlineitem = value; OnPropertyChanged("SelectedLineItem"); }
        }

        int quantity;
        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; OnPropertyChanged("Quantity"); }
        }

        string stockcode;
        public string StockCode
        {
            get { return stockcode; }
            set { stockcode = value; OnPropertyChanged("StockCode"); }
        }

        string color;
        public string Color
        {
            get { return color; }
            set { color = value; OnPropertyChanged("Color"); }
        }

        string size;
        public string Size
        {
            get { return size; }
            set { size = value; OnPropertyChanged("Size"); }
        }

        string onorder;
        public string OnOrder
        {
            get { return onorder; }
            set { onorder = value; OnPropertyChanged("OnOrder"); }
        }

        alphacurrentorder selectedalphaorder;
        public alphacurrentorder SelectedAlphaOrder
        {
            get { return selectedalphaorder; }
            set { selectedalphaorder = value; OnPropertyChanged("SelectedAlphaOrder"); }
        }
    }
}   
