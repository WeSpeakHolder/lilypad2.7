using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using Lilypad.Model;
using Lilypad.Properties;

namespace Lilypad.ViewModel
{
    public class PreordersViewModel : INotifyPropertyChanged
    {
        private bool newordervalid;
        private preorder selectedneworder;
        private preorder selectedorder;
        private IList<LineViewModel> inputmasterlineitems;
        public PreordersViewModel()
        {
            selectedneworder = new preorder();
            PreordersList = new ObservableCollection<preorder>();
            FroggerList = new ObservableCollection<frogger>();
            Statuses = new ObservableCollection<preorderstatu>();
            Processes = new ObservableCollection<printprocess>();
            inputmasterlineitems = new ObservableCollection<LineViewModel>();
            LoadData();
        }

        private IList<preorder> preorderslist;

        public IList<preorder> PreordersList
        {
            get { return preorderslist; }
            set { preorderslist = value;
                OnPropertyChanged("PreordersList");
            }
        }

        private string originalnotes;
        public string OriginalNotes
        {
            get { return originalnotes; }
            set
            {
                originalnotes = value;
                OnPropertyChanged("OriginalNotes");
            }
        }

        private string originalartnotes;
        public string OriginalArtNotes
        {
            get { return originalartnotes; }
            set
            {
                originalartnotes = value;
                OnPropertyChanged("OriginalArtNotes");
            }
        }

        private IList<frogger> froggerlist;

        public IList<frogger> FroggerList
        {
            get { return froggerlist; }
            set
            {
                froggerlist = value;
                OnPropertyChanged("FroggerList");
            }
        }

        private IList<preorderstatu> statuses;

        public IList<preorderstatu> Statuses
        {
            get { return statuses; }
            set
            {
                statuses = value;
                OnPropertyChanged("Statuses");
            }
        }

        private IList<printprocess> processes;

        public IList<printprocess> Processes
        {
            get { return processes; }
            set
            {
                processes = value;
                OnPropertyChanged("Processes");
            }
        }

        public void LoadOrderItems()
        {
            InputMasterLineItems = new ObservableCollection<LineViewModel>();

            try
            {
                if (SelectedOrder != null)
                {
                    using (Entities ctx = new Entities())
                    {
                        int id = SelectedOrder.PreorderID;
                        List<preorderitem> items = ctx.preorderitems.Where(x => x.PreorderID == id && x.DesignID == null).ToList();
                        foreach (preorderitem d in items)
                        {
                            LineViewModel lvm = new LineViewModel();
                            lvm.SelectedLineItem.Color = d.StockColor;
                            lvm.SelectedLineItem.Description = d.StockDescrip;
                            lvm.LineCost = d.EstimatedCost.HasValue ? d.EstimatedCost.Value : 0.00m;
                            lvm.SelectedLineItem.Quantity = d.StockQty.HasValue ? d.StockQty.Value : 0;
                            lvm.SelectedLineItem.ReceiptNum = d.PreorderID;
                            lvm.SelectedLineItem.Size = d.StockSize;
                            lvm.SelectedLineItem.StockIDPOS = d.StockCode;
                            lvm.SelectedLineItem.UniqueID = d.UniqueID;
                            InputMasterLineItems.Add(lvm);
                        }
                    }
                }
                else
                {
                    int id = 0;
                    if (SelectedOrder != null)
                    {
                        id = SelectedOrder.PreorderID;
                    }
                    using (Entities ctx = new Entities())
                    {
                        List<orderdetail> items = ctx.orderdetails.Where(x => x.ReceiptNum == id).ToList();
                        foreach (orderdetail d in items)
                        {
                            LineViewModel lvm = new LineViewModel();
                            lvm.SelectedLineItem.Color = d.Color;
                            lvm.SelectedLineItem.Description = d.Description;
                            lvm.LineCost = d.LineCost.HasValue ? d.LineCost.Value : 0.00m;
                            lvm.LinePrice = 0.00m;
                            lvm.SelectedLineItem.Quantity = d.Quantity;
                            lvm.SelectedLineItem.ReceiptNum = d.ReceiptNum;
                            lvm.SelectedLineItem.Size = d.Size;
                            lvm.SelectedLineItem.StockIDPOS = d.StockIDPOS;
                            lvm.SelectedLineItem.UniqueID = d.DetailID;
                            InputMasterLineItems.Add(lvm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        public void ReloadGridData()
        {
            using (Entities ctx = new Entities())
            {
                PreordersList = ctx.preorders.ToList();
            }
        }

        public IList<LineViewModel> InputMasterLineItems
        {
            get { return inputmasterlineitems; }
            set
            {
                inputmasterlineitems = value;
                OnPropertyChanged("InputMasterLineItems");
            }
        }

        public void LoadData()
        {
            using (Entities ctx = new Entities())
            {
               // PreordersList = ctx.preorders.ToList();
                FroggerList = ctx.froggers.Where(x => x.CurrentlyEmployed).ToList();
                Statuses = ctx.preorderstatus.ToList();
                Processes = ctx.printprocesses.ToList();
            }
        }
        

        public preorder SelectedOrder
        {
            get { return selectedorder; }
            set
            {
                selectedorder = value;
                OnPropertyChanged("SelectedOrder");
            }
        }

        public preorder SelectedNewOrder
        {
            get { return selectedneworder; }
            set
            {
                selectedneworder = value;
                OnPropertyChanged("SelectedNewOrder");
            }
        }

        public bool NewOrderValid
        {
            get { return newordervalid; }
            set
            {
                newordervalid = value;
                OnPropertyChanged("NewOrderValid");
            }
        }

        public string FormattedPhoneNumber
        {
            get
            {
                if (SelectedNewOrder != null)
                {
                    return SelectedNewOrder.PhoneNum;
                }
                return string.Empty;
            }
            set
            {
                SelectedNewOrder.PhoneNum = FormatNumber(value);
                OnPropertyChanged("FormattedPhoneNumber");
            }
        }

        public string FormatNumber(string input)
        {
            if (input != null)
            {
                string[] chars = { "-", "(", ")", " " };
                var PhoneNumber = input;
                foreach (var c in chars)
                {
                    PhoneNumber = PhoneNumber.Replace(c, string.Empty);
                }
                if (PhoneNumber == null)
                    return string.Empty;

                switch (PhoneNumber.Length)
                {
                    case 7:
                        return Regex.Replace(PhoneNumber, @"(\d{3})(\d{4})", "$1-$2");

                    case 10:
                        return Regex.Replace(PhoneNumber, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");

                    case 11:
                        return Regex.Replace(PhoneNumber, @"(\d{1})(\d{3})(\d{3})(\d{4})", "$1-$2-$3-$4");

                    default:
                        return PhoneNumber;
                }
            }
            return string.Empty;
        }

        public void ValidateNewOrder()
        {
            if (SelectedNewOrder != null)
            {
                var valid = true;
                if (string.IsNullOrEmpty(SelectedNewOrder.First) && string.IsNullOrEmpty(SelectedNewOrder.Company))
                {
                    valid = false;
                }
                if (string.IsNullOrEmpty(SelectedNewOrder.First) && !string.IsNullOrEmpty(SelectedNewOrder.Company))
                {
                    valid = true;
                }
                if (string.IsNullOrEmpty(SelectedNewOrder.First) && !string.IsNullOrEmpty(SelectedNewOrder.Last))
                {
                    valid = true;
                }
                if (!string.IsNullOrEmpty(SelectedNewOrder.First) && !string.IsNullOrEmpty(SelectedNewOrder.Last))
                {
                    valid = true;
                }
                NewOrderValid = valid;
            }
        }

        public void CreateNewPreorder()
        {
            var p = CreateNewPreorderEntry();
         //   UI.NavigateDetails(p);
        }

        public preorder CreateNewPreorderEntry()
        {
            var newly = new preorder();
            var pr = new preorder();
            pr.StatusID = 13;
            pr.Status = "New Pre-Order";
            pr.ShirtsCount = SelectedNewOrder.ShirtsCount;
            pr.PhoneNum = FormattedPhoneNumber;
            pr.JobFolderPath = CreateNewWorkingFolder();
            pr.OrderFrog = ((MainViewModel)Application.Current.MainWindow.DataContext).LoggedInFrogger.FrogID;
            pr.Last = SelectedNewOrder.Last;
            pr.First = SelectedNewOrder.First;
            pr.EmailAddr = SelectedNewOrder.EmailAddr;
            pr.CreatedDate = DateTime.Now;
            pr.Company = SelectedNewOrder.Company;

            try
            {
                using (var ctx = new Entities())
                {
                    ctx.preorders.Add(pr);
                    ctx.SaveChanges();
                    newly = ctx.preorders.FirstOrDefault(x => x.PreorderID == pr.PreorderID);
                }
                return newly;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return null;
            }
        }

        private int? addnewqty;
        public int? AddNewQty
        {
            get { return addnewqty; }
            set
            {
                addnewqty = value;
                OnPropertyChanged("AddNewQty");
            }
        }

        private string addnewstock;
        public string AddNewStock
        {
            get { return addnewstock; }
            set
            {
                addnewstock = value;
                OnPropertyChanged("AddNewStock");
            }
        }

        private string addnewcolor;
        public string AddNewColor
        {
            get { return addnewcolor; }
            set
            {
                addnewcolor = value;
                OnPropertyChanged("AddNewColor");
            }
        }

        private string addnewsize;
        public string AddNewSize
        {
            get { return addnewsize; }
            set
            {
                addnewsize = value;
                OnPropertyChanged("AddNewSize");
            }
        }

        private decimal? addnewquote;
        public decimal? AddNewQuote
        {
            get { return addnewquote; }
            set
            {
                addnewquote = value;
                OnPropertyChanged("AddNewQuote");
            }
        }

        public string CreateNewWorkingFolder()
        {
            var formattedDate = DateTime.Now.ToString("MM-dd-yyyy");
            var firstName = SelectedNewOrder.First;
            var lastName = SelectedNewOrder.Last;
            var company = SelectedNewOrder.Company;
            var broshrd = Settings.Default.Data_PendingFolderPath;
            var jobFolder = "";
            var foldername = "";

            if (SelectedNewOrder.PreorderID != null)
            {
                if (string.IsNullOrEmpty(company))
                {
                    if (string.IsNullOrEmpty(lastName))
                    {
                        foldername = firstName + " - " + formattedDate + "";
                    }
                    else if (string.IsNullOrEmpty(firstName))
                    {
                        foldername = lastName + " - " + formattedDate + "";
                    }
                    else if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
                    {
                        foldername = lastName + ", " + firstName + " - " + formattedDate + "";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(lastName))
                    {
                        foldername = company + " - " + firstName + " - " + formattedDate + "";
                    }
                    else if (string.IsNullOrEmpty(firstName))
                    {
                        foldername = company + " - " + lastName + " - " + formattedDate + "";
                    }
                    else if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
                    {
                        foldername = company + " - " + lastName + ", " + firstName + " - " + formattedDate + "";
                    }
                }

                jobFolder = broshrd + "\\" + foldername;
            }

            try
            {
                if (!Directory.Exists(jobFolder))
                {
                    var cleaned = Utilities.RemoveInvalidFilepathCharacters(foldername);
                    jobFolder = broshrd + "\\" + cleaned;
                    Directory.CreateDirectory(jobFolder);
                }
                return jobFolder;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return string.Empty;
            }
        }



        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }

        #endregion PropertyChanged Controller
    }
}