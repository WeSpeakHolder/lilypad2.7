using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    public class CrashboardViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        public CrashboardViewModel()
        {
            selectederrors = new ObservableCollection<error>();
        }

        private IList<error> allerrors;
        public IList<error> AllErrors
        {
            get
            {
                if (allerrors == null)
                {
                    if (Utilities.InternetConnected())
                    {
                        try
                        {
                            using (Cloud cl = new Cloud())
                            {
                                allerrors = (from x in cl.errors.Include("bigfrogstore") where x.Archived == false select x).ToList();
                            }
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }
                } 
                
                return allerrors;
            }
            set { allerrors = value; OnPropertyChanged("AllErrors"); }
        }

        private ObservableCollection<error> selectederrors;
        public ObservableCollection<error> SelectedErrors
        {
            get { return selectederrors; }
            set { selectederrors = value; OnPropertyChanged("SelectedErrors"); }
        }        

        private error selectederror;
        public error SelectedError
        {
            get { return selectederror; }
            set { selectederror = value; OnPropertyChanged("SelectedError"); }
        }


        
    }
}