using DevExpress.Xpf.Mvvm;
using System;
using System.Linq;
using System.Reflection;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    public class CallCardViewModel : ViewModelBase
    {
        string customerName;
        public string CustomerName
        {
            get { return customerName; }
            set { SetProperty(ref customerName, value, "CustomerName"); }
        }

        string customerCompany;
        public string CustomerCompany
        {
            get { return customerCompany; }
            set { SetProperty(ref customerCompany, value, "CustomerCompany"); }
        }

        DateTime callTime;
        public DateTime CallTime
        {
            get { return callTime; }
            set { SetProperty(ref callTime, value, "CallTime"); }
        }

        string callCommon;
        public string CallCommon
        {
            get { return callCommon; }
            set { SetProperty(ref callCommon, value, "CallCommon"); }
        }

        string phoneIn;
        public string PhoneIn
        {
            get { return phoneIn; }
            set { SetProperty(ref phoneIn, value, "PhoneIn"); }
        }

        string phoneFormatted;
        public string PhoneFormatted
        {
            get { return phoneFormatted; }
            set { SetProperty(ref phoneFormatted, value, "PhoneFormatted"); }
        }

        bool isCalled;
        public bool IsCalled
        {
            get { return isCalled; }
            set { SetProperty(ref isCalled, value, "IsCalled"); }
        }

        int receiptNum;
        public int ReceiptNum
        {
            get { return receiptNum; }
            set { SetProperty(ref receiptNum, value, "ReceiptNum"); }
        }

        int callID;
        public int CallID
        {
            get { return callID; }
            set { SetProperty(ref callID, value, "CallID"); }
        }

        public void DeleteItem()
        {
            using (Entities ctx = new Entities())
            {
                calllist item = (from q in ctx.calllists where q.UniqueID == CallID select q).FirstOrDefault();
                ctx.calllists.Remove(item);
                ctx.SaveChanges();
            }
        }

        public void SaveChanges()
        {
            using (Entities ctx = new Entities())
            {
                calllist item = (from q in ctx.calllists where q.UniqueID == CallID select q).FirstOrDefault();
                item.Called = IsCalled;
                ctx.SaveChanges();
            }
        }
    }
}
