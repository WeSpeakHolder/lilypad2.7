using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using GalaSoft.MvvmLight.Command;
using Lilypad.Model;
using Lilypad.Properties;

namespace Lilypad.ViewModel
{
    public class InventoryViewModel : INotifyPropertyChanged
    {
        private bool canCheckDetails;
        public List<alphacurrentorder> CurrentOrderItems;
        public List<alphacurrentorder> result;
        private DateTime deadline;
        private decimal estprice;
        private alphacurrentorder newItem;
        private bool newitemforcustomer;
        private alphaorder pastorder;
        private RelayCommand saveCommand;
        private alphacurrentorder selectedItem;
        private int totalitems;
        private bool transmitready;

        public InventoryViewModel()
        {
            StartCountdown();
            using (var ctx = new Entities())
            {
                var onload = ctx.alphacurrentorders.ToList();
                foreach (var item in onload)
                {
                    item.IsArchiveable = false;
                }
                ctx.SaveChanges();
            }
            LoadCurrentOrderItems();
            CurrentOrderItemsObservable.CollectionChanged += currentorderItems_CollectionChanged;
            UpdateTotals();
        }

        public alphaorder SelectedPastOrder
        {
            get { return pastorder; }
            set
            {
                pastorder = value;
                OnPropertyChanged("SelectedPastOrder");
            }
        }

        public bool NewItemForCustomer
        {
            get { return newitemforcustomer; }
            set
            {
                newitemforcustomer = value;
                OnPropertyChanged("NewItemForCustomer");
            }
        }

        public bool CanCheckDetails
        {
            get { return canCheckDetails; }
            set
            {
                canCheckDetails = value;
                OnPropertyChanged("CanCheckDetails");
            }
        }

        public bool TransmitReady
        {
            get { return transmitready; }
            set
            {
                transmitready = value;
                OnPropertyChanged("TransmitReady");
            }
        }

        public alphacurrentorder SelectedAlphaItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value;
                OnPropertyChanged("SelectedAlphaItem");
            }
        }

        public alphacurrentorder NewItem
        {
            get { return newItem; }
            set
            {
                newItem = value;
                OnPropertyChanged("NewItem");
            }
        }

        public ObservableCollection<alphacurrentorder> CurrentOrderItemsObservable
        {
            get { return new ObservableCollection<alphacurrentorder>(result); }
        }

        public int TotalItemCount
        {
            get { return totalitems; }
            set
            {
                totalitems = value;
                OnPropertyChanged("TotalItemCount");
            }
        }

        public decimal EstimatedPrice
        {
            get { return estprice; }
            set
            {
                estprice = value;
                OnPropertyChanged("EstimatedPrice");
            }
        }

        public DateTime DeadLine
        {
            get { return deadline; }
            set
            {
                deadline = value;
                OnPropertyChanged("DeadLine");
            }
        }

        public CountdownViewModel DeadLineCountdown { get; private set; }

        public RelayCommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new RelayCommand(Save);
                }
                return saveCommand;
            }
        }

        public void StartCountdown()
        {
            var parsedDate =
                DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd") + " " + Settings.Default.Interop_AlphaDeadlineTime);
            DeadLine = parsedDate;
            DeadLineCountdown = new CountdownViewModel(DeadLine);
            //    currentorderItems = new ObservableCollection<alphacurrentorder>();
            //    currentorderItems.CollectionChanged += currentorderItems_CollectionChanged;
        }

        private void currentorderItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            UpdateTotals();
        }

        private void UpdateTotals()
        {
            var sum = 0.00m;

            foreach (var order in CurrentOrderItems)
            {
                if (order.SalePrice == 0.00m)
                {
                    var quant = order.Quantity * order.UnitPrice;
                    order.LineCost = quant;
                    sum += quant;
                }
                else
                {
                    var quant = order.Quantity * order.SalePrice;
                    order.LineCost = quant;
                    sum += quant;
                }
            }

            var sum3 = CurrentOrderItems.Sum(x => x.Quantity);
            TotalItemCount = sum3;
            EstimatedPrice = sum;
        }

        public void LoadCurrentOrderItems()
        {
            using (var ctx = new Entities())
            {
                CurrentOrderItems = ctx.alphacurrentorders.ToList();
                result = CurrentOrderItems.GroupBy(g => new { g.ReceiptNum, g.ItemDescription })
                        .Select(g => g.First())
                        .ToList();
            }
        }

        public void AddItemToOrder(alphacurrentorder item)
        {
            CurrentOrderItems.Add(item);
            OnPropertyChanged("CurrentOrderItems");
            OnPropertyChanged("CurrentOrderItemsObservable");
            UpdateTotals();
        }

        public void RemoveItemFromOrder(alphacurrentorder item)
        {
            CurrentOrderItems.Remove(item);
            OnPropertyChanged("CurrentOrderItems");
            OnPropertyChanged("CurrentOrderItemsObservable");
            UpdateTotals();
        }

        private void Save()
        {
        }

        #region PropertyChanged Controller

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }

        #endregion PropertyChanged Controller
    }
}