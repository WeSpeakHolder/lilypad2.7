using System;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using Lilypad.Controls.Tags;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using Lilypad.Model;
using System.Deployment.Application;


namespace Lilypad.ViewModel
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        private List<string> alphaitembrands;
        private List<string> alphaitemcodes;
        private List<string> alphaitemcolors;
        private readonly CultureInfo ci = Thread.CurrentThread.CurrentCulture;
        public List<string> AlphaItemBrands
        {
            get { return alphaitembrands; }
            set
            {
                alphaitembrands = value;
                OnPropertyChanged("AlphaItemBrands");
            }
        }

        public List<string> AlphaItemCodes
        {
            get { return alphaitemcodes; }
            set
            {
                alphaitemcodes = value;
                OnPropertyChanged("AlphaItemCodes");
            }
        }

        public List<string> AlphaItemColors
        {
            get { return alphaitemcolors; }
            set
            {
                alphaitemcolors = value;
                OnPropertyChanged("AlphaItemColors");
            }
        }

        public List<string> AlphaItemColorsCase
        {
            get { return AlphaItemColors.Select(x => ci.TextInfo.ToTitleCase(x.ToLower())).ToList(); }
        }


        public MainViewModel()
        {
            openmmcommand = new RelayCommand(OpenMainMenu);
            gohomecommand = new RelayCommand(GoHome);
            gotestcommand = new RelayCommand(GoTest);
            goproductscommand = new RelayCommand(GoTest2);
            goartlabcommand = new RelayCommand(GoArtLab);
            goinventorycommand = new RelayCommand(GoInventory);
            gohoppercommand = new RelayCommand(GoHopper);
            goultraPrintcommand = new RelayCommand(GoUltraPrint);
            gosandboxcommand = new RelayCommand(GoSandbox);
            gonewordercommand = new RelayCommand(GoNewOrder);
            goproductscommand = new RelayCommand(GoProducts);
            goinventoryorderscommand = new RelayCommand(GoInventoryOrders);
          //  goinventoryorderdetailscommand = new RelayCommand(GoInventoryOrderDetails);
            gopreorderscommand = new RelayCommand(GoPreorders);
            gohoppernewcommand = new RelayCommand(GoHopperNew);
            gocrashboardcommand = new RelayCommand(GoCrashboard);
            goinitialsetupcommand = new RelayCommand(GoInitialSetup);
            notifycommand = new RelayCommand(ShowNotice);
            logoutcommand = new RelayCommand(Logout);
            gobarcodescannercommand = new RelayCommand(OpenScanner);
            savechangescommand = new RelayCommand(SaveChanges);
            designcardlist = new List<string>();
            AlphaItemBrands = new List<string>();
            AlphaItemCodes = new List<string>();
            AlphaItemColors = new List<string>();
            navigatemissioncontrol = new RelayCommand(NavigateTo_MissionControl);
            if (tabcontent == null) { tabcontent = new Views.Login(); }
            if (tabpage == null) { tabpage = new Uri("Views/Login.xaml", UriKind.RelativeOrAbsolute); }
            if (Utilities.DBisOnline())
            {
                using (Entities ctx = new Entities())
                {
                    try
                    {
                        CurrentFroggers = ctx.froggers.Where(x => x.CurrentlyEmployed).ToList();
                        prioritieslist = ctx.priorities.ToList();
                        productionstatuses = ctx.productionstatus.ToList();
                        correspondenceslist = ctx.correspondences.ToList();
                        printprocesseslist = ctx.printprocesses.ToList();
                        artworkstatuslist = ctx.artworkstatus.ToList();
                        inventorystatuslist = ctx.inventorydatastatus.ToList();

                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }
            }
            detailssaveneeded = false;
            isnavlocked = false;
            menuOpen = false;
            frogisloggedin = false;
            tagcollection = new ObservableCollection<Tag>();
            loggedinfrogger = new frogger();
            loggedinfrogger.FrogID = 1;
            loggedinfrogger.FrogFirstName = "Lilypad";
        }



        private bool frogisloggedin;
        public bool FrogIsLoggedIn
        {
            get { return frogisloggedin; }
            set { frogisloggedin = value; OnPropertyChanged("FrogIsLoggedIn"); }
        }

        private bool frogisadmin;
        public bool FrogIsAdmin
        {
            get { return frogisadmin; }
            set { frogisadmin = value; OnPropertyChanged("FrogIsAdmin"); }
        }

        private bool frogisowner;
        public bool FrogIsOwner
        {
            get { return frogisowner; }
            set { frogisowner = value; OnPropertyChanged("FrogIsOwner"); }
        }

        private bool frogismanager;
        public bool FrogIsManager
        {
            get { return frogismanager; }
            set { frogismanager = value; OnPropertyChanged("FrogIsManager"); }
        }
        private bool showBusiness;
        public bool ShowBusiness
        {
            get { return showBusiness; }
            set { showBusiness = value; OnPropertyChanged("ShowBusiness"); }
        }

       private System.Windows.Visibility digitalFlag;
        public System.Windows.Visibility DigitalFlag
        {
            get { return digitalFlag; }
            set { digitalFlag = value; OnPropertyChanged("DigitalFlag"); }
        }

        private static string win7 = @"C:\ProgramData\Microsoft\User Account Pictures\Default Pictures\usertile10.bmp";
        private static string win8 = @"C:\ProgramData\Microsoft\Default Account Pictures\guest.bmp";
        private static string win88 = @"C:\ProgramData\Microsoft\User Account Pictures\guest.bmp";

        private frogger loggedinfrogger;
        public frogger LoggedInFrogger
        {
            get { return loggedinfrogger; }
            set { 
                loggedinfrogger = value;
                ShowBusiness = false;
                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    DigitalFlag = Visibility.Visible;
                    if (loggedinfrogger.IsAdministrator) { FrogIsAdmin = true; } else { FrogIsAdmin = false; }
                    if (loggedinfrogger.IsManager) { FrogIsManager = true; } else { FrogIsManager = false; }
                    if (loggedinfrogger.IsStoreOwner == true) { FrogIsOwner = true; ShowBusiness = true; } else { FrogIsOwner = false; }
                }
                else
                {
                    DigitalFlag = Visibility.Collapsed;
                    if (loggedinfrogger.IsAdministrator) { FrogIsAdmin = true; ShowBusiness = true; } else { FrogIsAdmin = false; }
                    if (loggedinfrogger.IsManager) { FrogIsManager = true; } else { FrogIsManager = false; }
                    if (loggedinfrogger.IsStoreOwner == true) { FrogIsOwner = true; ShowBusiness = true; } else { FrogIsOwner = false; }
                }
                FrogIsLoggedIn = true;

                OnPropertyChanged("LoggedInFrogger");
            }
        }

        private List<frogger> currentfroggers;
        public List<frogger> CurrentFroggers
        {
            get { return currentfroggers; }
            set { currentfroggers = value; OnPropertyChanged("CurrentFroggers"); }
        }

        public ObservableCollection<frogger> FroggersObservableList
        {
            get { return new ObservableCollection<frogger>(CurrentFroggers); }
        }

        private List<productionstatu> productionstatuses;
        public List<productionstatu> ProductionStatuses
        {
            get { return productionstatuses; }
            set { productionstatuses = value; OnPropertyChanged("ProductionStatuses"); }
        }

        private List<correspondence> correspondenceslist;
        public List<correspondence> CorrespondencesList
        {
            get { return correspondenceslist; }
            set { correspondenceslist = value; OnPropertyChanged("CorrespondencesList"); }
        }

        private List<artworkstatu> artworkstatuslist;
        public List<artworkstatu> ArtworkStatusList
        {
            get { return artworkstatuslist; }
            set { artworkstatuslist = value; OnPropertyChanged("ArtworkStatusList"); }
        }


        private List<inventorydatastatu> inventorystatuslist;
        public List<inventorydatastatu> InventoryStatusList
        {
            get { return inventorystatuslist; }
            set { inventorystatuslist = value; OnPropertyChanged("InventoryStatusList"); }
        }

        private List<printprocess> printprocesseslist;
        public List<printprocess> PrintProcessesList
        {
            get { return printprocesseslist; }
            set { printprocesseslist = value; OnPropertyChanged("PrintProcessesList"); }
        }

        private List<priority> prioritieslist;
        public List<priority> PrioritiesList
        {
            get { return prioritieslist; }
            set { prioritieslist = value; OnPropertyChanged("PrioritiesList"); }
        }

        private List<designdetail> selectedhopperorderdesigns;
        public List<designdetail> SelectedHopperOrderDesigns
        {
            get { return selectedhopperorderdesigns; }
            set { selectedhopperorderdesigns = value; OnPropertyChanged("SelectedHopperOrderDesigns"); }
        }


        private designdetail selecteddetail;
        public designdetail SelectedDesignDetail
        {
            get { return selecteddetail; }
            set { selecteddetail = value; OnPropertyChanged("SelectedDesignDetail"); CalculatePrints(); }
        }

        private ObservableCollection<Tag> tagcollection;
        public ObservableCollection<Tag> TagCollection
        {
            get { return tagcollection; }
            set { tagcollection = value; OnPropertyChanged("TagCollection"); }
        }

        public void ResetTagCollection()
        {
            if (SelectedDetailHopperOrder != null)
            {
                if (tagcollection.Any()) { tagcollection.Clear(); }
                char[] sep = new char[] { ',' };
                string[] tags = SelectedDetailHopperOrder.SearchTags.Split(sep, StringSplitOptions.RemoveEmptyEntries).ToArray();

                foreach (string tg in tags)
                {
                    string ne = tg.Trim();
                    Controls.Tags.Tag newTag = new Controls.Tags.Tag(ne);
                    newTag.CanDelete = false;
                    tagcollection.Add(newTag);
                }
            }
        }

        public void CalculatePrints()
        {
            printsindetail = new List<bool>();
            printsindetail.Add(selecteddetail.AP_Apron);
            printsindetail.Add(selecteddetail.AP_Back);
            printsindetail.Add(selecteddetail.AP_BackBottom);
            printsindetail.Add(selecteddetail.AP_Bandana);
            printsindetail.Add(selecteddetail.AP_Cap);
            printsindetail.Add(selecteddetail.AP_ClCoaster);
            printsindetail.Add(selecteddetail.AP_Collar);
            printsindetail.Add(selecteddetail.AP_FLC);
            printsindetail.Add(selecteddetail.AP_FLCW);
            printsindetail.Add(selecteddetail.AP_FRC);
            printsindetail.Add(selecteddetail.AP_FRCW);
            printsindetail.Add(selecteddetail.AP_Front);
            printsindetail.Add(selecteddetail.AP_FrontBottom);
            printsindetail.Add(selecteddetail.AP_GolfFlag);
            printsindetail.Add(selecteddetail.AP_GolfTowel);
            printsindetail.Add(selecteddetail.AP_Koozie);
            printsindetail.Add(selecteddetail.AP_LeftSide);
            printsindetail.Add(selecteddetail.AP_LeftSleeve);
            printsindetail.Add(selecteddetail.AP_Mousepad);
            printsindetail.Add(selecteddetail.AP_Puzzle);
            printsindetail.Add(selecteddetail.AP_RightSide);
            printsindetail.Add(selecteddetail.AP_RightSleeve);
            printsindetail.Add(selecteddetail.AP_SqCoaster);
            printsindetail.Add(selecteddetail.AP_ToteBag);
            printsindetail.Add(selecteddetail.AP_WineBag);

            TotalPrints = printsindetail.Where(x => x).Count();
        }

        private List<bool> printsindetail;
        public List<bool> PrintsInDetail
        {
            get { return printsindetail; }
            set { printsindetail = value; OnPropertyChanged("PrintsInDetail"); }
        }

        private List<string> designcardlist;
        public List<string> DesignCardList
        {
            get { return designcardlist; }
            set { designcardlist = value; OnPropertyChanged("DesignCardList"); }
        }

        private int totalprints;
        public int TotalPrints
        {
            get
            {
                return totalprints;
            }
            set
            {
                totalprints = value; OnPropertyChanged("TotalPrints");
            }
        }

        private string resultstring;
        public string ResultString
        {
            get { return resultstring; }
            set { resultstring = value; OnPropertyChanged("ResultString"); }
        }

        private RelayCommand openmmcommand;
        public RelayCommand OpenMainMenuCommand { get { return openmmcommand; } }

        private RelayCommand gohomecommand;
        public RelayCommand GoHomeCommand { get { return gohomecommand; } }

        private RelayCommand goartlabcommand;
        public RelayCommand GoArtLabCommand { get { return goartlabcommand; } }

        private RelayCommand goinventorycommand;
        public RelayCommand GoInventoryCommand { get { return goinventorycommand; } }

        private RelayCommand gonewordercommand;
        public RelayCommand GoNewOrderCommand { get { return gonewordercommand; } }

        private RelayCommand gohoppercommand;
        public RelayCommand GoHopperCommand { get { return gohoppercommand; } }

        private RelayCommand goultraPrintcommand;
        public RelayCommand goUltraPrintcommand { get { return goultraPrintcommand; } }

        private RelayCommand gohoppernewcommand;
        public RelayCommand GoHopperNewCommand { get { return gohoppernewcommand; } }

        private RelayCommand gosandboxcommand;
        public RelayCommand GoSandboxCommand { get { return gosandboxcommand; } }

        private RelayCommand gotestcommand;
        public RelayCommand GoTestCommand { get { return gotestcommand; } }

        private RelayCommand goproductscommand;
        public RelayCommand GoProductsCommand { get { return goproductscommand; } }

        private RelayCommand gopreorderscommand;
        public RelayCommand GoPreordersCommand { get { return gopreorderscommand; } }

        private RelayCommand gocrashboardcommand;
        public RelayCommand GoCrashboardCommand { get { return gocrashboardcommand; } }

        private RelayCommand savechangescommand;
        public RelayCommand SaveChangesCommand { get { return savechangescommand; } }

        private RelayCommand notifycommand;
        public RelayCommand NotifyCommand { get { return notifycommand; } }

        private RelayCommand goinitialsetupcommand;
        public RelayCommand GoInitialSetupCommand { get { return goinitialsetupcommand; } }

        private RelayCommand logoutcommand;
        public RelayCommand LogoutCommand { get { return logoutcommand; } }

        private RelayCommand gobarcodescannercommand;
        public RelayCommand GoBarcodeScannerCommand { get { return gobarcodescannercommand; } }

        private RelayCommand goinventoryorderscommand;
        public RelayCommand GoInventoryOrdersCommand { get { return goinventoryorderscommand; } }

        private RelayCommand goinventoryorderdetailscommand;
        public RelayCommand GoInventoryOrderDetailsCommand { get { return goinventoryorderdetailscommand; } }


        private RelayCommand navigatemissioncontrol;
        public RelayCommand Navigate_MissionControl { get { return navigatemissioncontrol; } }

        private Object tabcontent;
        public Object CurrentTabContent
        {
            get { return tabcontent; }
            set { tabcontent = value; OnPropertyChanged("CurrentTabContent"); }
        }

        private Uri tabpage;
        public Uri CurrentPage
        {
            get { return tabpage; }
            set { tabpage = value; OnPropertyChanged("CurrentPage"); }
        }

        private bool menuOpen;
        public bool MenuOpen
        {
            get { return menuOpen; }
            set { menuOpen = value; OnPropertyChanged("MenuOpen"); }
        }

        private bool detailsshown;
        public bool DetailsShown
        {
            get { return detailsshown; }
            set { detailsshown = value; OnPropertyChanged("DetailsShown"); }
        }

        private bool scannerOpen;
        public bool ScannerOpen
        {
            get { return scannerOpen; }
            set { scannerOpen = value; OnPropertyChanged("ScannerOpen"); }
        }

        private bool changesSaved;
        public bool ChangesSaved
        {
            get { return changesSaved; }
            set { changesSaved = value; OnPropertyChanged("ChangesSaved"); }
        }

        private string toastmsg;
        public string ToastMessage
        {
            get { return toastmsg; }
            set { toastmsg = value; OnPropertyChanged("ToastMessage"); }
        }

        private string toastsubmsg;
        public string ToastSubMessage
        {
            get { return toastsubmsg; }
            set { toastsubmsg = value; OnPropertyChanged("ToastSubMessage"); }
        }

        private bool detailssaveneeded;
        public bool DetailsSaveNeeded
        {
            get { return detailssaveneeded; }
            set { detailssaveneeded = value; OnPropertyChanged("DetailsSaveNeeded"); }
        }

        private order selecteddetailorder;
        public order SelectedDetailOrder
        {
            get { return selecteddetailorder; }
            set { selecteddetailorder = value; OnPropertyChanged("SelectedDetailOrder"); }
        }

        private hopperorder selecteddetailhopperorder;
        public hopperorder SelectedDetailHopperOrder
        {
            get { return selecteddetailhopperorder; }
            set
            {
                selecteddetailhopperorder = value; OnPropertyChanged("SelectedDetailHopperOrder"); ResetTagCollection();
                if (Utilities.DBisOnline())
                {
                    using (Entities ctx = new Entities())
                    {
                        SelectedHopperOrderDesigns = ctx.designdetails.Where(x => x.ReceiptNum == selecteddetailhopperorder.ReceiptNum).ToList();
                    }
                }
            }
        }


        private bool isnavlocked;
        public bool IsNavigationLocked
        {
            get { return isnavlocked; }
            set { isnavlocked = value; OnPropertyChanged("IsNavigationLocked"); }
        }

        public async void GoHome()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("Views/Home_Dashboard.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("Views/Home_Dashboard.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public void GoTest()
        {
            CurrentPage = new Uri("/TestHarness.xaml", UriKind.Relative);
        }

        public void GoTest2()
        {
            CurrentPage = new Uri("/Test2.xaml", UriKind.Relative);
        }

        public async void GoArtLab()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/ArtLab.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/ArtLab.xaml", UriKind.Relative);
            }
        }

        public async void GoInventory()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/Inventory.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/Inventory.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public async void GoPreorders()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/Preorders.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/Preorders.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public async void GoSandbox()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/Sandbox.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/Sandbox.xaml", UriKind.Relative);
            }
        }

        public async void Logout()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/Login.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/Login.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public async void GoNewOrder()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/NewOrder.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/NewOrder.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public async void GoCrashboard()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/Crashboard.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/Crashboard.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public async void GoProducts()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/Products.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/Products.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public async void GoInitialSetup()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/InitialSetupPage.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/InitialSetupPage.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public async void GoInventoryOrders()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/InventoryOrders.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/InventoryOrders.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public async void GoInventoryOrderDetails(alphaorder parameter)
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    InventoryOrderDetailsParameter = parameter;
                    CurrentPage = new Uri("/Views/InventoryOrderDetails.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                InventoryOrderDetailsParameter = parameter;
                CurrentPage = new Uri("/Views/InventoryOrderDetails.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        private object inventoryorderdetailsparameter;

        public object InventoryOrderDetailsParameter
        {
            get { return inventoryorderdetailsparameter; }
            set { inventoryorderdetailsparameter = value;
                OnPropertyChanged("InventoryOrderDetailsParameter");
            }
        }

        public async void GoHopper()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/Hopper.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/Hopper.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public async void GoUltraPrint()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/UltraPrint.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/UltraPrint.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public async void GoHopperNew()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/HopperNew.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/HopperNew.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public async void NavigateTo_MissionControl()
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    CurrentPage = new Uri("/Views/MissionControl.xaml", UriKind.Relative);
                    IsNavigationLocked = false;
                }
            }
            else
            {
                CurrentPage = new Uri("/Views/MissionControl.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        public async void NavigateTo_MissionControl(bool myProfile)
        {
            if (IsNavigationLocked)
            {
                Utilities.ManualFocusViaSendKeys();
                MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Navigation", "You have unsaved changes on this page. Are you sure you want to navigate away and lose these changes?");
                if (res == MessageDialogResult.Affirmative)
                {
                    browsingmyprofile = true;
                    CurrentPage = new Uri("/Views/MissionControl.xaml", UriKind.Relative);
                }
            }
            else
            {
                browsingmyprofile = true;
                CurrentPage = new Uri("/Views/MissionControl.xaml", UriKind.Relative);
            }
            Toaster.DismissLoading();
        }

        private bool browsingmyprofile;
        public bool BrowsingMyProfile
        {
            get { return browsingmyprofile; }
            set { browsingmyprofile = value; OnPropertyChanged("BrowsingMyProfile"); }
        }

        public void OpenMainMenu()
        {
            if (frogisloggedin)
            {
                if (!MenuOpen)
                {
                    MenuOpen = true;
                }
                else
                {
                    MenuOpen = false;
                }
            }
        }

        public void OpenScanner()
        {
            if (!ScannerOpen)
            {
                ScannerOpen = true;
            }
            else
            {
                ScannerOpen = false;
            }
        }

        public void ShowDetails()
        {
            if (!ScannerOpen)
            {
                ScannerOpen = true;
            }
            else
            {
                ScannerOpen = false;
            }
        }

        System.Threading.CancellationTokenSource tokenSource = new System.Threading.CancellationTokenSource();

        public async void SaveChanges()
        {
            MainWindow mw = (MainWindow)App.Current.MainWindow;
            mw.toastActual.FillColor = Brushes.YellowGreen;
            mw.toastActual.IsLoading = true;
            mw.toastActual.IsError = false;
            ToastMessage = "Saving Changes...";
            ToastSubMessage = "";
            ChangesSaved = true;
            ChangesSaved = false;
            await Task.Delay(500, tokenSource.Token);
            mw.toastActual.IsLoading = false;
            mw.toastActual.IsError = false;
            ToastMessage = "Saved Changes.";
        }

        public void SaveChangesToSelectedOrder()
        {
            var selectedorder = SelectedDetailHopperOrder;

            if (selectedorder != null)
            {
                using (Entities ctx = new Entities())
                {
                    hopperorder toEdit = (from k in ctx.hopperorders where k.ReceiptNum == selectedorder.ReceiptNum select k).FirstOrDefault();
                    toEdit.BoxNum = selectedorder.BoxNum;
                    toEdit.CareTags = selectedorder.CareTags;
                    toEdit.Correspondence = selectedorder.Correspondence;
                    toEdit.DesignFrog = selectedorder.DesignFrog;
                    toEdit.DueDate = selectedorder.DueDate;
                    toEdit.Irregulars = selectedorder.Irregulars;
                    toEdit.IsDropDead = selectedorder.IsDropDead;
                    toEdit.IsNoRush = selectedorder.IsNoRush;
                    toEdit.JobFolderPath = selectedorder.JobFolderPath;
                    toEdit.Notes = selectedorder.Notes;
                    toEdit.OrderDate = selectedorder.OrderDate;
                    toEdit.PrintProcess = selectedorder.PrintProcess;
                    toEdit.Priority = selectedorder.Priority;
                    toEdit.TotalPrints = selectedorder.TotalPrints;
                    toEdit.TotalShirts = selectedorder.TotalShirts;
                    toEdit.Status = selectedorder.Status;
                    toEdit.SearchTags = selectedorder.SearchTags;
                    ctx.SaveChanges();
                }
            }
        }

        public async void ShowNotice()
        {
            MainWindow mw = (MainWindow)App.Current.MainWindow;
            mw.toastActual.FillColor = Brushes.DarkOrange;
            mw.toastActual.IsLoading = false;
            mw.toastActual.IsError = true;
            ChangesSaved = true;
            await Task.Delay(2200, tokenSource.Token);
            ChangesSaved = false;
        }
    }
}