using System;
using System.ComponentModel;
using System.Windows;
using GalaSoft.MvvmLight.Command;
using Lilypad.Model;

namespace Lilypad.ViewModel
{
    public class InventoryOrdersViewModel : INotifyPropertyChanged
    {
        private bool canCheckDetails;
        private DateTime deadline;
        private decimal estprice;
        private alphaorder foundorder;
        private alphacurrentorder newItem;
        private alphaorder pastorder;
        private RelayCommand saveCommand;
        private int totalitems;
        private bool transmitready;

        public alphaorder SelectedPastOrder
        {
            get { return pastorder; }
            set
            {
                pastorder = value;
                OnPropertyChanged("SelectedPastOrder");
            }
        }

        public alphaorder SelectedFoundOrder
        {
            get { return foundorder; }
            set
            {
                foundorder = value;
                OnPropertyChanged("SelectedFoundOrder");
            }
        }

        public bool CanCheckDetails
        {
            get { return canCheckDetails; }
            set
            {
                canCheckDetails = value;
                OnPropertyChanged("CanCheckDetails");
            }
        }

        public bool TransmitReady
        {
            get { return transmitready; }
            set
            {
                transmitready = value;
                OnPropertyChanged("TransmitReady");
            }
        }

        public alphacurrentorder NewItem
        {
            get { return newItem; }
            set
            {
                newItem = value;
                OnPropertyChanged("NewItem");
            }
        }

        public int TotalItemCount
        {
            get { return totalitems; }
            set
            {
                totalitems = value;
                OnPropertyChanged("TotalItemCount");
            }
        }

        public decimal EstimatedPrice
        {
            get { return estprice; }
            set
            {
                estprice = value;
                OnPropertyChanged("EstimatedPrice");
            }
        }

        public DateTime DeadLine
        {
            get { return deadline; }
            set
            {
                deadline = value;
                OnPropertyChanged("DeadLine");
            }
        }

        public RelayCommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new RelayCommand(Save);
                }
                return saveCommand;
            }
        }

        private void Save()
        {
        }

        #region PropertyChanged Controller

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }

        #endregion PropertyChanged Controller
    }
}