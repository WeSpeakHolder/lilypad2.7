using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    public class TaskCardViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public TaskCardViewModel()
        {
            using (Entities ctx = new Entities())
            {
                froggerlist = ctx.froggers.Where(x => x.CurrentlyEmployed == true).ToList();
            }
            
        }

        private task selectedtask;
        public task SelectedTask
        {
            get { return selectedtask; }
            set { selectedtask = value; OnPropertyChanged("SelectedTask"); }
        }

        private int priority;
        public int Priority
        {
            get { return priority; }
            set { priority = value; OnPropertyChanged("Priority"); }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; OnPropertyChanged("Description"); }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged("Title"); }
        }

        private int assignedfrogger;
        public int AssignedFrogger
        {
            get { return assignedfrogger; }
            set { assignedfrogger = value; OnPropertyChanged("AssignedFrogger"); }
        }

        private List<frogger> froggerlist;
        public List<frogger> FroggerList
        {
            get { return froggerlist; }
            set { froggerlist = value; OnPropertyChanged("FroggerList"); }
        }

        private DateTime createddatetime;
        public DateTime CreatedDateTime
        {
            get { return createddatetime; }
            set { createddatetime = value; OnPropertyChanged("CreatedDateTime"); }
        }

        private bool issaved;
        public bool IsSaved
        {
            get { return issaved; }
            set { issaved = value; OnPropertyChanged("IsSaved"); }
        }


        private bool saveneeded;
        public bool SaveNeeded
        {
            get { return saveneeded; }
            set { saveneeded = value; OnPropertyChanged("SaveNeeded"); }
        }

        private bool isglobal;
        public bool IsGlobal
        {
            get { return isglobal; }
            set { isglobal = value; OnPropertyChanged("IsGlobal"); }
        }

        private bool iscompleted;
        public bool IsCompleted
        {
            get { return iscompleted; }
            set { iscompleted = value; OnPropertyChanged("IsCompleted"); }
        }

        public void SaveChanges()
        {
           
        }
    }
}   
