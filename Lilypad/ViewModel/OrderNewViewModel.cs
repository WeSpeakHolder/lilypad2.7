using System;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.Controls.Tags;
using Lilypad.Properties;

namespace Lilypad.ViewModel
{

    public class AscendingSorter : IComparer
    {
        public int Compare(object x, object y)
        {
            OrderNewViewModel vmX = x as OrderNewViewModel;
            OrderNewViewModel vmY = y as OrderNewViewModel;
            return vmY.NewHopperOrder.CustFirst.CompareTo(vmX.NewHopperOrder.CustFirst);
        }
    }

    public class DescendingSorter : IComparer
    {
        public int Compare(object x, object y)
        {
            OrderNewViewModel vmX = x as OrderNewViewModel;
            OrderNewViewModel vmY = y as OrderNewViewModel;
            return vmX.NewHopperOrder.CustFirst.CompareTo(vmY.NewHopperOrder.CustFirst);
        }
    }

    public class OrderNewViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public OrderNewViewModel()
        {
            tagcollection = new ObservableCollection<Tag>();
            newhopperorder = new hopperorder();
            newhopperorder.DueDate = DateTime.Now;
            inputrows = new ObservableCollection<OrderNewViewModel>();
            inputmasterlineitems = new ObservableCollection<LineViewModel>();
            inputlineitems = new ObservableCollection<LineViewModel>();
            toorderfromalpha = new ObservableCollection<LineViewModel>();
            OrderInputComplete = false;

            filter = "Search";

            FilteredRowView = CollectionViewSource.GetDefaultView(InputRows) as ListCollectionView;
            FilteredRowView.Filter = (o =>
            {
                bool vis;
                if (string.IsNullOrEmpty(Filter))
                {
                    vis = true;
                }
                else if (Filter == "Search")
                {
                    vis = true;
                }
                else
                {
                    vis = false;

                    if (((OrderNewViewModel)o).NewHopperOrder.CustLast.ContainsCaseInsensitive(Filter)) vis = true;
                    if (((OrderNewViewModel)o).NewHopperOrder.CustFirst.ContainsCaseInsensitive(Filter)) vis = true;
                    if (((OrderNewViewModel)o).NewHopperOrder.CustCompany.ContainsCaseInsensitive(Filter)) vis = true;
                }

                return vis;
            });

        }

        public DataTable GetCustomerDataFromTJobs()
        {
            DataTable dt = new DataTable();

            string path = Settings.Default.Data_ResourcePath;
            int threshold = Convert.ToInt32(Settings.Default.Data_RecordSelectThreshold);
            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + @"\TJobs.mdb";
            string Select = @"SELECT DISTINCT TOP " + threshold + " GroupDescription, ReceiptNum, CustId, CustomerName, CustomerCompany, OrderDateTime, DueDateTime FROM TJobsTracking WHERE CustId<>1 ORDER BY ReceiptNum DESC";
            OleDbConnection pwConn = new OleDbConnection(dbConnect); OleDbDataAdapter adapter = new OleDbDataAdapter(Select, pwConn);
            try
            {
                pwConn.Open();
                adapter.Fill(dt);
                pwConn.Close();
            }
            catch (Exception ex)
            {
                new Error(ex);
            }

            return dt;
        }

        public Dictionary<int, int> ReceiptDictionary;

        public DataTable GetDataFromParcom(int receipt)
        {
            try
            {
                DataTable dt2 = new DataTable();

                string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();

                string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\ParcomW.mdb";
                string select = @"SELECT Line_Recpt, Line_ProcessRef2, Line_ProdNum, Line_StockNumber, Line_Qnty, SubSelection1, SubSelection2, Line_Description, Line_Cost, Line_Price, Line_Extend, Line_LineNum, Line_Status FROM LineItems WHERE Line_Recpt = " + receipt + " ORDER BY Line_LineNum ASC";
                OleDbConnection pwConn = new OleDbConnection(dbConnect);
                OleDbDataAdapter adapter = new OleDbDataAdapter(select, pwConn);
                pwConn.Open();
                adapter.Fill(dt2);
                pwConn.Close();
                return dt2;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return null;
            }
        }

        public DataTable ReceiptData { get; set; }

        public bool IsCurrentData { get; set; }

        public IList<LineViewModel> GetOrderItemsAsVMList(DataTable inputtable)
        {
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
            inputtable.AcceptChanges();
            DataTable localtable = inputtable;

            IList<LineViewModel> internallist = new List<LineViewModel>();

            foreach (DataRow row in localtable.Rows)
            {
                designdetailslineitem item = new designdetailslineitem();
                item.ReceiptNum = Convert.ToInt32(row["Line_Recpt"]);
                item.Quantity = Convert.ToInt32(row["Line_Qnty"]);
                item.Size = Convert.ToString(row["SubSelection1"]).Trim();
                item.Color = myTI.ToTitleCase(Convert.ToString(row["SubSelection2"]).Trim().ToLower());
                item.StockIDPOS = Convert.ToString(row["Line_StockNumber"]).Trim();
                item.Description = myTI.ToTitleCase(Convert.ToString(row["Line_Description"]).ToString().Trim().ToLower());
                LineViewModel newVM = new LineViewModel(item);
                newVM.LineCost = Convert.ToDecimal(row["Line_Cost"]);
                newVM.LineExtend = Convert.ToDecimal(row["Line_Extend"]);
                newVM.LinePrice = Convert.ToDecimal(row["Line_Price"]);
                internallist.Add(newVM);
            }

            return internallist;

        }

        public DataTable CleanedUpData(DataTable data)
        {
            DataTable localtable = data;
            string stockout = "";
            string thisheader = "";
            string thisline = "0";
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
            int shirtsum = 0;
            foreach (DataRow row in localtable.Rows)
            {
                var size = Convert.ToString(row["SubSelection1"]).Trim();
                var chh = Convert.ToString(row["Line_ProcessRef2"]).Trim();
                var status = Convert.ToString(row["Line_Status"]).Trim();
                var colinp = Convert.ToString(row["SubSelection2"]).Trim().ToLower();
                var LineNum = Convert.ToString(row["Line_LineNum"]).Split('.')[0];
                string color = myTI.ToTitleCase(colinp);
                string stockin = Convert.ToString(row["Line_StockNumber"]).Trim();
                bool ischilditem = string.IsNullOrEmpty(chh);
                bool isblanktitle = string.IsNullOrEmpty(stockin);
                int quant = Convert.ToInt32(row["Line_Qnty"]);

                if (!ischilditem)
                {
                    string inputs1 = row["Line_Description"].ToString().Trim().ToLower();
                    thisheader = myTI.ToTitleCase(inputs1);
                    thisline = LineNum;
                    row.Delete();
                }
                else
                {
                    stockout = "";
                    if (!String.IsNullOrEmpty(stockin))
                    {

                        if (stockin.Contains('_')) { stockout = stockin.Split('_')[0].ToString(); stockout = stockout.Substring(2, stockout.Length - 2); }
                        else { stockout = stockin.Substring(2, stockin.Length - 2); }

                        if (thisheader != null)
                        {
                            string newstring = thisheader + " - " + color + " - " + size;
                            row.SetField<string>(7, newstring);

                            if (!isblanktitle)
                            {
                                row.SetField<string>(3, stockout);

                                if (stockout == "SPECIALPRODUCT")
                                {
                                    string ss2 = thisheader.ToUpper();
                                    row.SetField<string>(3, ss2);
                                }
                            }
                            else
                            {
                                string stockextracted = thisheader.Split(' ')[0].ToString();
                                row.SetField<string>(3, stockextracted);
                            }
                        }
                    }
                }
                if (status == "D")
                {
                    row.Delete();
                }
                shirtsum += quant;
            }

            TotalItems = shirtsum;

            return localtable;
        }

        public DataTable GetDataFromCurrentData(int receipt)
        {
            try
            {
                DataTable dt = new DataTable();

                string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();

                string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\currentdata.mdb";
                string select = @"SELECT Line_Recpt, Line_ProcessRef2, Line_ProdNum, Line_StockNumber, Line_Qnty, SubSelection1, SubSelection2, Line_Description, Line_Cost, Line_Price, Line_Extend, Line_LineNum, Line_Status FROM LineItems WHERE Line_Recpt = " + receipt + " ORDER BY Line_LineNum ASC";
                OleDbConnection pwConn = new OleDbConnection(dbConnect);
                OleDbDataAdapter adapter = new OleDbDataAdapter(select, pwConn);
                pwConn.Open();
                adapter.Fill(dt);
                pwConn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return null;
            }
        }
        public DataTable GetDataFromThisYear(int receipt)
        {
            try
            {
                DataTable dt = new DataTable();

                string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();
                string filepath = DateTime.Now.Year.ToString() + "Data.mdb";
                string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\" + filepath;
                string select = @"SELECT Line_Recpt, Line_ProcessRef2, Line_ProdNum, Line_StockNumber, Line_Qnty, SubSelection1, SubSelection2, Line_Description, Line_Cost, Line_Price, Line_Extend, Line_LineNum, Line_Status FROM LineItems WHERE Line_Recpt = " + receipt + " ORDER BY Line_LineNum ASC";
                OleDbConnection pwConn = new OleDbConnection(dbConnect);
                OleDbDataAdapter adapter = new OleDbDataAdapter(select, pwConn);
                pwConn.Open();
                adapter.Fill(dt);
                pwConn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return null;
            }
        }

        public DataTable GetDataFromLastYear(int receipt)
        {
            try
            {
                DataTable dt = new DataTable();

                string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();
                string filepath = DateTime.Now.AddYears(-1).Year.ToString() + "Data.mdb";
                string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\" + filepath;
                string select = @"SELECT Line_Recpt, Line_ProcessRef2, Line_ProdNum, Line_StockNumber, Line_Qnty, SubSelection1, SubSelection2, Line_Description, Line_Cost, Line_Price, Line_Extend, Line_LineNum, Line_Status FROM LineItems WHERE Line_Recpt = " + receipt + " ORDER BY Line_LineNum ASC";
                OleDbConnection pwConn = new OleDbConnection(dbConnect);
                OleDbDataAdapter adapter = new OleDbDataAdapter(select, pwConn);
                pwConn.Open();
                adapter.Fill(dt);
                pwConn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return null;
            }
        }
        private string[] GetCustomerFromID(int id)
        {
            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();
            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\ParcomW.mdb";
            DataTable custData1 = new DataTable();

            string Select2 = @"SELECT FirstName, LastName, Company FROM CustsMain WHERE CustId = " + id + "";

            OleDbConnection myDbConn;

           //try
            {
                myDbConn = new OleDbConnection(dbConnect);
                OleDbCommand myComm = new OleDbCommand(Select2, myDbConn);
                OleDbDataAdapter myDA = new OleDbDataAdapter(myComm);
                myDbConn.Open();
                myDA.Fill(custData1);
                myDbConn.Close();
            }
           //catch (Exception ex)
          //{
                       
           //}

            if (custData1.Rows != null)
            {            string first = custData1.Rows[0]["FirstName"].ToString();
                string last = custData1.Rows[0]["LastName"].ToString();
                string comp = custData1.Rows[0]["Company"].ToString();
                string[] outarray = new string[] { first, last, comp };
                return outarray;
            }
            else
            {
                return new string[] { "", "", "" };
            }

        }

        private DataTable GetReceiptData(int input, string database)
        {
            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();

            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\" + database;

            string SelectOrderData = @" SELECT TOP 1 Rcpt_Number, Rcpt_Node, Rcpt_CustNumber, Rcpt_CCardNumber, Rcpt_DiscPercentage, Rcpt_CashTendered, Rcpt_CheckTendered,
                                        Rcpt_CCardTendered, Rcpt_ChargeTendered, Rcpt_TotalTendered, Rcpt_TotalTax, Rcpt_STotal, Rcpt_Total, Rcpt_DateTimeStamp
                                        FROM Receipts 
                                        WHERE Rcpt_CustNumber = " + input +
                          " ORDER BY Rcpt_Number DESC";

            OleDbConnection tjConn = null;

            try
            {
                tjConn = new OleDbConnection(dbConnect);

                OleDbCommand orderComm = new OleDbCommand(SelectOrderData, tjConn);
                OleDbDataAdapter myDA1 = new OleDbDataAdapter(orderComm);

                DataTable output = new DataTable();
                tjConn.Open();
                myDA1.Fill(output);
                tjConn.Close();

                ReceiptData = output;
                return output;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return null;
            }
        }

        public void GetItemsForSelectedOrder()
        {
            InputMasterLineItems.Clear();
            ToOrderFromAlpha.Clear();

            int receipt = SelectedOrderNewVM.NewHopperOrder.ReceiptNum;

            if (IDExistsInCData(receipt))
            {
                try
                {
                    var LineList = GetOrderItemsAsVMList(CleanedUpData(GetDataFromCurrentData(receipt)));
                    foreach (LineViewModel line in LineList)
                    {
                        var extracted = new designdetailslineitem();
                        extracted = line.SelectedLineItem;
                        var newccvm = new LineViewModel(extracted);
                        newccvm.LineCost = line.LineCost;
                        newccvm.LineExtend = line.LineExtend;
                        newccvm.LinePrice = line.LinePrice;
                        if (extracted.StockIDPOS.ToLower() != "decorating") {
                            InputMasterLineItems.Add(newccvm);
                        }
                    }

                    int custI = GetCustIDFromCurrentData(receipt);
                    string[] custss = GetCustomerFromID(custI);
                    SelectedOrderNewVM.NewHopperOrder.CustomerID = custI;
                    SelectedOrderNewVM.NewHopperOrder.CustFirst = custss[0];
                    SelectedOrderNewVM.NewHopperOrder.CustLast = custss[1];
                    SelectedOrderNewVM.NewHopperOrder.CustCompany = custss[2];
                    ReceiptData = GetReceiptData(custI, "currentdata.mdb");
                }
                catch (Exception ex)
                {
                    //new Error(ex);
                    Utilities.ShowSimpleDialog("Customer data missing", "Customer information for the order is missing which may indicate a corrupt record in POS.\nPlease check the order in POS as it may need to be re-entered in order to correct the problem.");

                    MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;

                    mvm.CurrentPage = new Uri("/Views/NewOrder.xaml", UriKind.Relative);
                    mvm.IsNavigationLocked = false;
                }
            }
            else if (IDExistsInParcom(receipt))
            {
                try
                {
                    var LineList = GetOrderItemsAsVMList(CleanedUpData(GetDataFromParcom(receipt)));
                    foreach (LineViewModel line in LineList)
                    {
                        var extracted = new designdetailslineitem();
                        extracted = line.SelectedLineItem;
                        var newccvm = new LineViewModel(extracted);
                        newccvm.LineCost = line.LineCost;
                        newccvm.LineExtend = line.LineExtend;
                        newccvm.LinePrice = line.LinePrice;
                        InputMasterLineItems.Add(newccvm);
                    }

                    int custI = GetCustIDFromParcom(receipt);
                    string[] custss = GetCustomerFromID(custI);
                    SelectedOrderNewVM.NewHopperOrder.CustomerID = custI;
                    SelectedOrderNewVM.NewHopperOrder.CustFirst = custss[0];
                    SelectedOrderNewVM.NewHopperOrder.CustLast = custss[1];
                    SelectedOrderNewVM.NewHopperOrder.CustCompany = custss[2];
                    ReceiptData = GetReceiptData(custI, "ParcomW.mdb");
                }
                catch (Exception ex)
                {
                    //new Error(ex);
                    Utilities.ShowSimpleDialog("Customer data missing", "Customer information for the order is missing which may indicate a corrupt record in POS.\nPlease check the order in POS as it may need to be re-entered in order to correct the problem.");

                    MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;

                    mvm.CurrentPage = new Uri("/Views/NewOrder.xaml", UriKind.Relative);
                    mvm.IsNavigationLocked = false;
                }
            }

            else if (IDExistsInArchiveThisYear(receipt))
            {
                try
                {
                    var LineList = GetOrderItemsAsVMList(CleanedUpData(GetDataFromThisYear(receipt)));
                    foreach (LineViewModel line in LineList)
                    {
                        var extracted = new designdetailslineitem();
                        extracted = line.SelectedLineItem;
                        var newccvm = new LineViewModel(extracted);
                        newccvm.LineCost = line.LineCost;
                        newccvm.LineExtend = line.LineExtend;
                        newccvm.LinePrice = line.LinePrice;
                        InputMasterLineItems.Add(newccvm);
                    }

                    int custI = GetCustIDFromThisYearArchive(receipt);
                    string[] custss = GetCustomerFromID(custI);
                    SelectedOrderNewVM.NewHopperOrder.CustomerID = custI;
                    SelectedOrderNewVM.NewHopperOrder.CustFirst = custss[0];
                    SelectedOrderNewVM.NewHopperOrder.CustLast = custss[1];
                    SelectedOrderNewVM.NewHopperOrder.CustCompany = custss[2];

                    string filepath = DateTime.Now.Year.ToString() + "Data.mdb";

                    ReceiptData = GetReceiptData(custI, filepath);
                }
                catch (Exception ex)
                {
                    //new Error(ex);
                    Utilities.ShowSimpleDialog("Customer data missing", "Customer information for the order is missing which may indicate a corrupt record in POS.\nPlease check the order in POS as it may need to be re-entered in order to correct the problem.");

                    MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;

                    mvm.CurrentPage = new Uri("/Views/NewOrder.xaml", UriKind.Relative);
                    mvm.IsNavigationLocked = false;
                }
            }
            else
            {
                try
                {
                    var LineList = GetOrderItemsAsVMList(CleanedUpData(GetDataFromLastYear(receipt)));
                    foreach (LineViewModel line in LineList)
                    {
                        var extracted = new designdetailslineitem();
                        extracted = line.SelectedLineItem;
                        var newccvm = new LineViewModel(extracted);
                        newccvm.LineCost = line.LineCost;
                        newccvm.LineExtend = line.LineExtend;
                        newccvm.LinePrice = line.LinePrice;
                        InputMasterLineItems.Add(newccvm);
                    }

                    int custI = GetCustIDFromLastYearArchive(receipt);
                    string[] custss = GetCustomerFromID(custI);
                    SelectedOrderNewVM.NewHopperOrder.CustomerID = custI;
                    SelectedOrderNewVM.NewHopperOrder.CustFirst = custss[0];
                    SelectedOrderNewVM.NewHopperOrder.CustLast = custss[1];
                    SelectedOrderNewVM.NewHopperOrder.CustCompany = custss[2];

                    string filepath = DateTime.Now.AddYears(-1).Year.ToString() + "Data.mdb";

                    ReceiptData = GetReceiptData(custI, filepath);
                }
                catch (Exception ex)
                {
                    //new Error(ex);
                    Utilities.ShowSimpleDialog("Customer data missing", "Customer information for the order is missing which may indicate a corrupt record in POS.\nPlease check the order in POS as it may need to be re-entered in order to correct the problem.");

                    MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;

                    mvm.CurrentPage = new Uri("/Views/NewOrder.xaml", UriKind.Relative);
                    mvm.IsNavigationLocked = false;
                }
            }
        }

        public int GetCustIDFromCurrentData(int receipt)
        {
            int custID;
            int output;
            DataTable dt = new DataTable();

            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();

            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\currentdata.mdb";
            string select = @"SELECT Rcpt_CustNumber FROM Receipts WHERE Rcpt_Number = " + receipt + "";
            OleDbConnection pwConn = new OleDbConnection(dbConnect);
            pwConn.Open();
            OleDbCommand command = new OleDbCommand(select, pwConn);
            OleDbDataReader reader = command.ExecuteReader();


            if (reader.HasRows)
            {
                reader.Read();
                custID = reader.GetInt32(0);
                output = custID;
            }
            else { output = 0; }

            pwConn.Close();
            return output;
        }

        public int GetCustIDFromThisYearArchive(int receipt)
        {
            int custID;
            int output;
            DataTable dt = new DataTable();
            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();
            string filepath = DateTime.Now.Year.ToString() + "Data.mdb";
            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\" + filepath;
            string select = @"SELECT Rcpt_CustNumber FROM Receipts WHERE Rcpt_Number = " + receipt + "";
            OleDbConnection pwConn = new OleDbConnection(dbConnect);
            pwConn.Open();
            OleDbCommand command = new OleDbCommand(select, pwConn);
            OleDbDataReader reader = command.ExecuteReader();


            if (reader.HasRows)
            {
                reader.Read();
                custID = reader.GetInt32(0);
                output = custID;
            }
            else { output = 0; }

            pwConn.Close();
            return output;
        }

        public int GetCustIDFromLastYearArchive(int receipt)
        {
            int custID;
            int output;
            DataTable dt = new DataTable();
            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();
            string filepath = DateTime.Now.AddYears(-1).Year.ToString() + "Data.mdb";
            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\" + filepath;
            string select = @"SELECT Rcpt_CustNumber FROM Receipts WHERE Rcpt_Number = " + receipt + "";
            OleDbConnection pwConn = new OleDbConnection(dbConnect);
            pwConn.Open();
            OleDbCommand command = new OleDbCommand(select, pwConn);
            OleDbDataReader reader = command.ExecuteReader();


            if (reader.HasRows)
            {
                reader.Read();
                custID = reader.GetInt32(0);
                output = custID;
            }
            else { output = 0; }

            pwConn.Close();
            return output;
        }


        public int GetCustIDFromParcom(int receipt)
        {
            int custID;
            int output;

            DataTable dt = new DataTable();

            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();

            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\ParcomW.mdb";
            string select = @"SELECT Rcpt_CustNumber FROM Receipts WHERE Rcpt_Number = " + receipt + "";
            OleDbConnection pwConn = new OleDbConnection(dbConnect);
            pwConn.Open();
            OleDbCommand command = new OleDbCommand(select, pwConn);
            OleDbDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Read();
                custID = reader.GetInt32(0);
                output = custID;
            }
            else { output = 0; }

            pwConn.Close();
            return output;

        }

        private bool IDExistsInCData(int receipt)
        {
            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();
            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\currentdata.mdb";

            string select = @"SELECT Line_Recpt FROM LineItems WHERE Line_Recpt = " + receipt + " ORDER BY Line_LineNum ASC";
            OleDbConnection pwConn = new OleDbConnection(dbConnect);
            OleDbCommand command = new OleDbCommand(select, pwConn);

            pwConn.Open();
            OleDbDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IDExistsInParcom(int receipt)
        {
            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();
            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\ParcomW.mdb";

            string select = @"SELECT Line_Recpt FROM LineItems WHERE Line_Recpt = " + receipt + " ORDER BY Line_LineNum ASC";
            OleDbConnection pwConn = new OleDbConnection(dbConnect);
            OleDbCommand command = new OleDbCommand(select, pwConn);

            pwConn.Open();
            OleDbDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IDExistsInArchiveThisYear(int receipt)
        {
            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();
            string filepath = DateTime.Now.Year.ToString() + "Data.mdb";
            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\" + filepath;

            string select = @"SELECT Line_Recpt FROM LineItems WHERE Line_Recpt = " + receipt + " ORDER BY Line_LineNum ASC";
            OleDbConnection pwConn = new OleDbConnection(dbConnect);
            OleDbCommand command = new OleDbCommand(select, pwConn);

            pwConn.Open();
            OleDbDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IDExistsInArchiveLastYear(int receipt)
        {
            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();
            string filepath = DateTime.Now.AddYears(-1).Year.ToString() + "Data.mdb";
            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\" + filepath;

            string select = @"SELECT Line_Recpt FROM LineItems WHERE Line_Recpt = " + receipt + " ORDER BY Line_LineNum ASC";
            OleDbConnection pwConn = new OleDbConnection(dbConnect);
            OleDbCommand command = new OleDbCommand(select, pwConn);

            pwConn.Open();
            OleDbDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public void ReloadInputOrders()
        {
            InputRows.Clear();

            DataTable orders = GetCustomerDataFromTJobs();
            for (int i = 0; i < orders.Rows.Count; i++)
            {
                DataRow row = orders.Rows[i];
                int receipt = Convert.ToInt32(row["ReceiptNum"]);
                using (Entities ctx = new Entities())
                {
                    if (ctx.orders.Any(o => o.ReceiptNum == receipt))
                    {
                        row.Delete();
                    }
                    else if (ctx.orderexceptions.Any(k => k.ReceiptNum == receipt))
                    {
                        row.Delete();
                    }
                    else if (ctx.hopperorders.Any(p => p.ReceiptNum == receipt))
                    {
                        row.Delete();
                    }
                    else if (ctx.receipts.Any(h => h.RcptNumber == receipt))
                    {
                        row.Delete();
                    }
                }

            }
            orders.AcceptChanges();


            foreach (DataRow row in orders.Rows)
            {
                int recpt = Convert.ToInt32(row["ReceiptNum"]);
                string cname = row["CustomerName"].ToString();
                string cfirst = cname.Split(' ')[0];
                string clast = cname.Remove(0, cfirst.Length + 1);
                var newVM = new OrderNewViewModel();
                newVM.CustomerFull = cname + " #" + row["ReceiptNum"].ToString();
                newVM.NewHopperOrder.CustFirst = cfirst;
                newVM.NewHopperOrder.CustLast = clast;
                newVM.NewHopperOrder.ReceiptNum = recpt;
                newVM.NewHopperOrder.CustCompany = row["CustomerCompany"].ToString();
                newVM.NewHopperOrder.POSDecortionType = row["GroupDescription"].ToString();
                newVM.NewHopperOrder.OrderDate = Convert.ToDateTime(row["OrderDateTime"]);
                InputRows.Add(newVM);
            }
        }

        private IList<LineViewModel> inputmasterlineitems;
        public IList<LineViewModel> InputMasterLineItems
        {
            get { return inputmasterlineitems; }
            set { inputmasterlineitems = value; OnPropertyChanged("InputMasterLineItems"); }
        }

        private IList<LineViewModel> inputlineitems;
        public IList<LineViewModel> InputLineItems
        {
            get { return inputlineitems; }
            set { inputlineitems = value; OnPropertyChanged("InputLineItems"); }
        }

        private IList<LineViewModel> toorderfromalpha;
        public IList<LineViewModel> ToOrderFromAlpha
        {
            get { return toorderfromalpha; }
            set { toorderfromalpha = value; OnPropertyChanged("ToOrderFromAlpha"); }
        }

        private IList<OrderNewViewModel> inputrows;
        public IList<OrderNewViewModel> InputRows
        {
            get { return inputrows; }
            set { inputrows = value; OnPropertyChanged("InputRows"); }
        }

        private string filter;
        public string Filter
        {
            get { return filter; }
            set { filter = value; FilteredRowView.Refresh(); OnPropertyChanged("Filter"); }
        }


        public ListCollectionView FilteredRowView { get; set; }

        private OrderNewViewModel selectedordernewvm;
        public OrderNewViewModel SelectedOrderNewVM
        {
            get { return selectedordernewvm; }
            set { selectedordernewvm = value; OnPropertyChanged("SelectedOrderNewVM"); }
        }


        private string custfull;
        public string CustomerFull
        {
            get { return custfull; }
            set { custfull = value; OnPropertyChanged("CustomerFull"); }
        }


        private string posDecortingType;
        public string POSDecortingType
        {
            get { return posDecortingType; }
            set { posDecortingType = value; OnPropertyChanged("POSDecortingType"); }
        }

        private int currentdesignnum;
        public int CurrentDesignNum
        {
            get { return currentdesignnum; }
            set { currentdesignnum = value; OnPropertyChanged("CurrentDesignNum"); }
        }


        private int totalitems;
        public int TotalItems
        {
            get { return totalitems; }
            set { totalitems = value; OnPropertyChanged("TotalItems"); }
        }

        private hopperorder newhopperorder;
        public hopperorder NewHopperOrder
        {
            get { return newhopperorder; }
            set { newhopperorder = value; OnPropertyChanged("NewHopperOrder"); }
        }

        private designdetail selecteddetail;
        public designdetail SelectedDesignDetail
        {
            get { return selecteddetail; }
            set { selecteddetail = value; OnPropertyChanged("SelectedDesignDetail"); }
        }

        private List<ultraprintorderdata> ultradata;
        public List<ultraprintorderdata> SelectedultraData
        {
            get { return ultradata; }
            set { ultradata = value; OnPropertyChanged("SelectedultraData"); }
        }

        private ObservableCollection<Tag> tagcollection;
        public ObservableCollection<Tag> TagCollection
        {
            get { return tagcollection; }
            set { tagcollection = value; OnPropertyChanged("TagCollection"); }
        }

        private bool designeropen;
        public bool DesignerOpen
        {
            get { return designeropen; }
            set { designeropen = value; OnPropertyChanged("DesignerOpen"); }
        }

        private bool setupopen;
        public bool SetupOpen
        {
            get { return setupopen; }
            set { setupopen = value; OnPropertyChanged("SetupOpen"); }
        }

        private bool verify_duedate;
        public bool Verify_DueDate
        {
            get { return verify_duedate; }
            set { verify_duedate = value; OnPropertyChanged("Verify_DueDate"); }
        }

        private bool verify_master;
        public bool Verify_Master
        {
            get { return verify_master; }
            set { verify_master = value; OnPropertyChanged("Verify_Master"); }
        }

        private bool verify_binnum;
        public bool Verify_BinNum
        {
            get { return verify_binnum; }
            set { verify_binnum = value; OnPropertyChanged("Verify_BinNum"); }
        }

        private bool verify_process;
        public bool Verify_Process
        {
            get { return verify_process; }
            set { verify_process = value; OnPropertyChanged("Verify_Process"); }
        }

        private bool verify_correspondence;
        public bool Verify_Correspondence
        {
            get { return verify_correspondence; }
            set { verify_correspondence = value; OnPropertyChanged("Verify_Correspondence"); }
        }

        private bool verify_priority;
        public bool Verify_Priority
        {
            get { return verify_priority; }
            set { verify_priority = value; OnPropertyChanged("Verify_Priority"); }
        }

        private bool verify_status;
        public bool Verify_Status
        {
            get { return verify_status; }
            set { verify_status = value; OnPropertyChanged("Verify_Status"); }
        }

        private bool verify_artwork;
        public bool Verify_Artwork
        {
            get { return verify_artwork; }
            set { verify_artwork = value; OnPropertyChanged("Verify_Artwork"); }
        }

        private bool verify_inventory;
        public bool Verify_Inventory
        {
            get { return verify_inventory; }
            set { verify_inventory = value; OnPropertyChanged("Verify_Inventory"); }
        }

        private bool verify_notes;
        public bool Verify_Notes
        {
            get { return verify_notes; }
            set { verify_notes = value; OnPropertyChanged("Verify_Notes"); }
        }


        private bool verify_metatags;
        public bool Verify_Metatags
        {
            get { return verify_metatags; }
            set { verify_metatags = value; OnPropertyChanged("Verify_Metatags"); }
        }

        private bool orderinputcomplete;
        public bool OrderInputComplete
        {
            get { return orderinputcomplete; }
            set { orderinputcomplete = value; OnPropertyChanged("OrderInputComplete"); }
        }

    }
}