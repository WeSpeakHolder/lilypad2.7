using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    public class SandboxViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public SandboxViewModel()
        {
            vmcollection = new ObservableCollection<DesignSetupCardViewModel>();
        }

        private int currentdesignnum;
        public int CurrentDesignNum
        {
            get { return currentdesignnum; }
            set { currentdesignnum = value; OnPropertyChanged("CurrentDesignNum"); }
        }

        private ObservableCollection<DesignSetupCardViewModel> vmcollection;
        public ObservableCollection<DesignSetupCardViewModel> VMCollection
        {
            get { return vmcollection; }
            set { vmcollection = value; OnPropertyChanged("VMCollection"); }
        }

        
    }
}