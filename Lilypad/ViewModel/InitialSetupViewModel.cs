using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;

namespace Lilypad.ViewModel
{
    public class InitialSetupViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public InitialSetupViewModel()
        {
            //page1welcome = new RelayCommand(GoToPage1);
            //page2agreement = new RelayCommand(GoToPage2);
            //page3presetup = new RelayCommand(GoToPage3);
            //page4storesetup = new RelayCommand(GoToPage4);
            //page5install = new RelayCommand(GoToPage5);
            //if (currentcontent == null) { currentcontent = new Views.Initial.Page1Welcome(); }
        }

        private object currentcontent;
        public object CurrentContent
        {
            get { return currentcontent; }
            set { currentcontent = value; OnPropertyChanged("CurrentContent"); }
        }

        private RelayCommand page1welcome;
        public RelayCommand Page1Welcome { get { return page1welcome; } }

        private RelayCommand page2agreement;
        public RelayCommand Page2Agreement { get { return page2agreement; } }

        private RelayCommand page3presetup;
        public RelayCommand Page3PreSetup { get { return page3presetup; } }

        private RelayCommand page4storesetup;
        public RelayCommand Page4StoreSetup { get { return page4storesetup; } }

        private RelayCommand page5install;
        public RelayCommand Page5Install { get { return page5install; } }

        public void GoToPage1()
        {
            CurrentContent = new Views.Initial.Page1Welcome();
        }

        public void GoToPage2()
        {
            CurrentContent = new Views.Initial.Page2Agreement();
        }

        public void GoToPage3()
        {
            CurrentContent = new Views.Initial.Page3PreSetup();
        }

        public void GoToPage4()
        {
            CurrentContent = new Views.Initial.Page4StoreSetup();
        }

        public void GoToPage5()
        {
            CurrentContent = new Views.Initial.Page5Install();
        }

        private Model.bigfrogstore store;
        public Model.bigfrogstore Store
        {
            get { return store; }
            set { store = value; OnPropertyChanged("Store"); }
        }

        public bool HasStoreOwner
        {
            get {
                if (store != null)
                {
                    if (string.IsNullOrEmpty(store.StoreOwner)) return false;
                    else return true;
                }
                else return true;
            }
        }

        private bool usesgmail;
        public bool UsesGmail
        {
            get { return usesgmail; }
            set { usesgmail = value; OnPropertyChanged("UsesGmail"); }
        }

        private bool licensevalid;
        public bool LicenseValid
        {
            get { return licensevalid; }
            set { licensevalid = value; OnPropertyChanged("LicenseValid"); }
        }

        private string licensekey;
        public string LicenseKey
        {
            get { return licensekey; }
            set { licensekey = value; OnPropertyChanged("LicenseKey"); }
        }

        private string lilypass;
        public string LilyPass
        {
            get { return lilypass; }
            set { lilypass = value; OnPropertyChanged("LilyPass"); }
        }

        private string ownername;
        public string OwnersName
        {
            get { return ownername; }
            set { ownername = value; OnPropertyChanged("OwnersName"); }
        }

        private string maindir;
        public string MainDirectory
        {
            get { return maindir; }
            set { maindir = value; OnPropertyChanged("MainDirectory"); }
        }

        private string gmailuser;
        public string GmailUser
        {
            get { return gmailuser; }
            set { gmailuser = value; OnPropertyChanged("GmailUser"); }
        }

        private string gmailpass;
        public string GmailPassword
        {
            get { return gmailpass; }
            set { gmailpass = value; OnPropertyChanged("GmailPassword"); }
        }

        private string gmailsendas;
        public string GmailSendas
        {
            get { return gmailsendas; }
            set { gmailsendas = value; OnPropertyChanged("GmailSendas"); }
        }

        private string storename;
        public string StoreName
        {
            get { return storename; }
            set { storename = value; OnPropertyChanged("StoreName"); }
        }

        private string storeaddr1;
        public string StoreAddress1
        {
            get { return storeaddr1; }
            set { storeaddr1 = value; OnPropertyChanged("StoreAddress1"); }
        }

        private string storeaddr2;
        public string StoreAddress2
        {
            get { return storeaddr2; }
            set { storeaddr2 = value; OnPropertyChanged("StoreAddress2"); }
        }

        private string storecity;
        public string StoreCity
        {
            get { return storecity; }
            set { storecity = value; OnPropertyChanged("StoreCity"); }
        }

        private string storestate;
        public string StoreState
        {
            get { return storestate; }
            set { storestate = value; OnPropertyChanged("StoreState"); }
        }

        private string storezip;
        public string StoreZip
        {
            get { return storezip; }
            set { storezip = value; OnPropertyChanged("StoreZip"); }
        }

        private string storeemail;
        public string StoreEmail
        {
            get { return storeemail; }
            set { storeemail = value; OnPropertyChanged("StoreEmail"); }
        }

        private string storephone;
        public string StorePhone
        {
            get { return storephone; }
            set { storephone = value; OnPropertyChanged("StorePhone"); }
        }

        private string storehoursmf;
        public string StoreHoursMF
        {
            get { return storehoursmf; }
            set { storehoursmf = value; OnPropertyChanged("StoreHoursMF"); }
        }

        private string storehourssat;
        public string StoreHoursSat
        {
            get { return storehourssat; }
            set { storehourssat = value; OnPropertyChanged("StoreHoursSat"); }
        }

        private string storehourssun;
        public string StoreHoursSun
        {
            get { return storehourssun; }
            set { storehourssun = value; OnPropertyChanged("StoreHoursSun"); }
        }

    }
}