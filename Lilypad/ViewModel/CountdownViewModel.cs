using System;
using System.IO;
using System.ComponentModel;
using System.Windows.Threading;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    public class CountdownViewModel : INotifyPropertyChanged
    {
        Func<TimeSpan> calc;
        DispatcherTimer timer;

        public CountdownViewModel(DateTime deadline)
            : this(() => deadline - DateTime.Now)
        {
        }

        public CountdownViewModel(Func<TimeSpan> calculator)
        {
            calc = calculator;

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            var temp = PropertyChanged;
            if (temp != null)
            {
                temp(this, new PropertyChangedEventArgs("CurrentValue"));
            }
        }

        public TimeSpan CurrentValue
        {
            get
            {
                var result = calc();
                if (result < TimeSpan.Zero)
                {
                    return TimeSpan.Zero;
                }
                return result;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}   
