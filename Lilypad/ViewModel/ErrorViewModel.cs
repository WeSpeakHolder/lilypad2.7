using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    public class ErrorViewModel : INotifyPropertyChanged
    {
        private Exception caughtexception;
        private string errordetails;
        private string errorheader;

        public string[] ErrorHeaders =
        {
           // "Ow.", 
            "Ruh roh..", 
            "Ah, jeez..", 
           // "PC LOAD LETTER", 
            "Our bad."
        };

        private string errormessage;

        public string[] ErrorMessages =
        {
            "Failure in rodent power generation module, Grid Alpha, Row 15, Column G.",
            "PC LOAD LETTER",
            "Have you tried unplugging the error for 30 seconds, then plugging it back in?",
            "Critical antimatter overload in dilithium crystal chamber",
            "Time-jump capabilities are offline: Check flux capacitor module",
            "An error occured while trying to display the correct error",
            "Please stare at error blankly until ready to continue",
            "Move along - nothing to see here",
            "This one appears to be NOT catastrophic. Hopefully.",
            "Lilypad either literally, or metaphorically, divided by zero.",
            "All warp cores are offline, captain.",
            "Standby for damage report, captain.",
            "This may - or may not - be what one would possibly call an 'error'.",
            "PC LOAD LETTER",
            "An error occured. If problem persists, don't do whatever you did.",
            "An error occured. If problem persists, don't do whatever you did.",
            "Lilypad tried to perform an illegal operation and now must await the king's justice.",
            "Lilypad tried to perform an illegal operation and now must await the king's justice.",
            "The boss will have a word with you now",
            "Well, now you've done it.",
            "Ow. Stop that."
        };

        private string errorreportstatus;
        private bool errorreportsuccessful;

        public ErrorViewModel()
        {
            var r = new Random(Guid.NewGuid().GetHashCode());
            ErrorHeader = ErrorHeaders[r.Next(0, ErrorHeaders.Length)];
            //  ErrorMessage = ErrorMessages[r.Next(0, ErrorMessages.Length)];
        }

        public Exception CaughtException
        {
            get { return caughtexception; }
            set
            {
                caughtexception = value;
                ErrorMessage = value.Message.Trim();
                OnPropertyChanged("CaughtException");
            }
        }

        public string ErrorHeader
        {
            get { return errorheader; }
            set
            {
                errorheader = value;
                OnPropertyChanged("ErrorHeader");
            }
        }

        public string ErrorMessage
        {
            get { return errormessage; }
            set
            {
                errormessage = value;
                OnPropertyChanged("ErrorMessage");
            }
        }

        public bool ErrorReportSuccessful
        {
            get { return errorreportsuccessful; }
            set
            {
                errorreportsuccessful = value;
                OnPropertyChanged("ErrorReportSuccessful");
            }
        }

        public string ErrorReportStatus
        {
            get { return errorreportstatus; }
            set
            {
                errorreportstatus = value;
                OnPropertyChanged("ErrorReportStatus");
            }
        }

        public string ErrorDetails
        {
            get { return errordetails; }
            set
            {
                errordetails = value;
                OnPropertyChanged("ErrorDetails");
            }
        }

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }

        #endregion PropertyChanged Controller
    }
}