using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using Lilypad.Model;
using Lilypad.Properties;

namespace Lilypad.ViewModel
{
    public class MetroDesignCardViewModel : INotifyPropertyChanged
    {
        private int artcount;
        private ObservableCollection<string> artfolders;
        private bool busy;
        private ObservableCollection<string> currentartitems;
        private int designid;
        private int designnum;
        private ObservableCollection<DesignFileViewModel> existingartitems;
        private IList<string> flaggeditems;
        private bool isBlankCard;
        private bool issaved;
        private IList<LineViewModel> lineitems;
        private bool needssave;
        private PreordersViewModel parentcontext;
        private int printcount;
        private customer selectedcustomer;
        private designdetail selecteddesigndetail;
        private LineViewModel selectedlineitem;
        private int shirtcount;

        public MetroDesignCardViewModel()
        {
            IsBlankCard = true;
            lineitems = new BindingList<LineViewModel>();
            flaggeditems = new ObservableCollection<string>();
            currentartitems = new ObservableCollection<string>();
            existingartitems = new ObservableCollection<DesignFileViewModel>();
            artfolders = new ObservableCollection<string>();
            needssave = false;
        }

        public MetroDesignCardViewModel(bool isBlankCard)
        {
            IsBlankCard = false;
            lineitems = new BindingList<LineViewModel>();
            flaggeditems = new ObservableCollection<string>();
            currentartitems = new ObservableCollection<string>();
            existingartitems = new ObservableCollection<DesignFileViewModel>();
            artfolders = new ObservableCollection<string>();
            needssave = false;
        }

        public int DesignNum
        {
            get { return designnum; }
            set
            {
                designnum = value;
                OnPropertyChanged("DesignNum");
            }
        }

        public customer SelectedCustomer
        {
            get { return selectedcustomer; }
            set
            {
                selectedcustomer = value;
                OnPropertyChanged("SelectedCustomer");
            }
        }

        public designdetail SelectedDesignDetail
        {
            get { return selecteddesigndetail; }
            set
            {
                selecteddesigndetail = value;
                OnPropertyChanged("SelectedDesignDetail");
                NeedsSave = true;
            }
        }

        public IList<LineViewModel> LineItems
        {
            get { return lineitems; }
            set
            {
                lineitems = value;
                OnPropertyChanged("LineItems");
            }
        }

        public LineViewModel SelectedLineItem
        {
            get { return selectedlineitem; }
            set
            {
                selectedlineitem = value;
                OnPropertyChanged("SelectedLineItem");
            }
        }

        public PreordersViewModel ParentContext
        {
            get { return parentcontext; }
            set
            {
                parentcontext = value;
                OnPropertyChanged("ParentContext");
            }
        }

        public bool NeedsSave
        {
            get { return needssave; }
            set
            {
                needssave = value;
                OnPropertyChanged("NeedsSave");
            }
        }

        public IList<string> FlaggedArtworkItems
        {
            get { return flaggeditems; }
            set
            {
                flaggeditems = value;
                OnPropertyChanged("FlaggedArtworkItems");
            }
        }

        public ObservableCollection<DesignFileViewModel> ExistingArtworkItems
        {
            get { return existingartitems; }
            set
            {
                existingartitems = value;
                OnPropertyChanged("ExistingArtworkItems");
            }
        }

        public ObservableCollection<string> CurrentArtworkItems
        {
            get { return currentartitems; }
            set
            {
                currentartitems = value;
                OnPropertyChanged("CurrentArtworkItems");
            }
        }

        public ObservableCollection<string> ArtworkFolders
        {
            get { return artfolders; }
            set
            {
                artfolders = value;
                OnPropertyChanged("ArtworkFolders");
            }
        }

        public bool IsBlankCard
        {
            get { return isBlankCard; }
            set
            {
                isBlankCard = value;
                OnPropertyChanged("IsBlankCard");
            }
        }

        public int ShirtCount
        {
            get
            {
                if (LineItems != null)
                {
                    shirtcount = (from q in LineItems select q).Sum(x => x.SelectedLineItem.Quantity);
                }
                return shirtcount;
            }
            set
            {
                shirtcount = value;
                OnPropertyChanged("ShirtCount");
                OnPropertyChanged("ArtCount");
                OnPropertyChanged("PrintCount");
            }
        }

        public int ArtCount
        {
            get
            {
                if (CurrentArtworkItems.Count > 0)
                {
                    artcount = CurrentArtworkItems.Count;
                }
                return artcount;
            }
            set
            {
                artcount = value;
                OnPropertyChanged("ShirtCount");
                OnPropertyChanged("ArtCount");
                OnPropertyChanged("PrintCount");
            }
        }

        public int PrintCount
        {
            get
            {
                printcount = ShirtCount * ArtCount;

                return printcount;
            }

            set
            {
                printcount = value;
                OnPropertyChanged("ShirtCount");
                OnPropertyChanged("ArtCount");
                OnPropertyChanged("PrintCount");
            }
        }

        public int CardDesignID
        {
            get
            {
                if (designid == 0)
                {
                    if (SelectedDesignDetail != null)
                    {
                        designid = SelectedDesignDetail.DesignNum;
                    }
                }
                return designid;
            }
            set
            {
                designid = value;
                OnPropertyChanged("CardDesignID");
            }
        }

        public bool Busy
        {
            get { return busy; }
            set
            {
                busy = value;
                OnPropertyChanged("Busy");
            }
        }

        public bool IsSaved
        {
            get { return issaved; }
            set
            {
                issaved = value;
                OnPropertyChanged("IsSaved");
            }
        }

        public void NotifyPropChanges(string input)
        {
            OnPropertyChanged(input);
        }

        public void SaveChanges()
        {
            if (IsBlankCard)
            {
                MoveFiles();
            }
        }

        public void LoadExistingItemsForDesign()
        {
            GetLineItems();
            GetArtwork();
        }

        private void GetLineItems()
        {
            if (lineitems != null)
            {
                lineitems.Clear();
            }

            var lineitemsii = new List<designdetailslineitem>();

            using (var ctx = new Entities())
            {
                lineitemsii = (from q in ctx.designdetailslineitems
                               where q.ReceiptNum == SelectedDesignDetail.ReceiptNum && q.DesignID == SelectedDesignDetail.DesignID
                               select q).ToList();
            }
            if (lineitemsii.Any())
            {
                foreach (var lineitem in lineitemsii)
                {
                    var newdd = new designdetailslineitem
                    {
                        Quantity = lineitem.Quantity,
                        Size = lineitem.Size,
                        StockIDPOS = lineitem.StockIDPOS,
                        Color = lineitem.Color,
                        Description = lineitem.Description,
                        ReceiptNum = lineitem.ReceiptNum,
                        DesignID = lineitem.DesignID,
                        UniqueID = lineitem.UniqueID
                    };

                    var newVM = new LineViewModel();
                    newVM.SelectedLineItem = newdd;

                    LineItems.Add(newVM);
                }
                NeedsSave = false;
            }
        }

        private void MoveFiles()
        {
            string newFolder;

            if (string.IsNullOrEmpty(SelectedDesignDetail.DesignDescription))
            {
                newFolder = "Design " + SelectedDesignDetail.DesignNum;
            }
            else
            {
                newFolder = "Design " + SelectedDesignDetail.DesignNum + " - " + SelectedDesignDetail.DesignDescription;
            }

            var newPath = "";

            if (!string.IsNullOrEmpty(SelectedDesignDetail.DesignFolderPath))
            {
                if (SelectedDesignDetail.DesignFolderPath.ToLower().Contains("design "))
                {
                    newPath = SelectedDesignDetail.DesignFolderPath;
                }
                else
                {
                    newPath = Path.Combine(SelectedDesignDetail.DesignFolderPath, newFolder);
                }
            }

            if (!Directory.Exists(newPath))
            {
                // cleaned = Utilities.GetCleanedFilename(newPath);
                try
                {
                    Directory.CreateDirectory(newPath);
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }

            SelectedDesignDetail.DesignFolderPath = newPath;

            if (CurrentArtworkItems.Count > 0)
            {
                try
                {
                    foreach (var item in CurrentArtworkItems)
                    {
                        var type = "";
                        var destination = SelectedDesignDetail.DesignFolderPath + @"\CDR\";
                        var isYouth = SelectedDesignDetail.AP_YOUTH;

                        if (item == "Apron" || item == "Bandana" || item == "CapFront" || item == "RoundCoasters" ||
                            item == "GolfFlag"
                            || item == "GolfTowel" || item == "Koozies" || item == "Mousepad" || item == "Puzzle" ||
                            item == "SquareCoasters" || item == "ToteBag" || item == "WineBag")
                        {
                            type = "S";
                            CopyArtFile(item, type, destination);
                        }
                        else
                        {
                            if (isYouth)
                            {
                                type = "Y";
                                CopyArtFile("Youth" + item, type, destination);
                            }
                            else if (!isYouth)
                            {
                                type = "A";
                                CopyArtFile(item, type, destination);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }

        public async void CopyArtFile(string artFile, string Type, string destination)
        {
            var datapth = Settings.Default.Data_TemplatesPath;
            string sourceFile;
            if (Type == "A")
            {
                sourceFile = datapth + "\\";
            }
            else if (Type == "Y")
            {
                sourceFile = datapth + @"\Youth\";
            }
            else if (Type == "S")
            {
                sourceFile = datapth + @"\Special\";
            }
            else
            {
                sourceFile = datapth + "\\";
            }
            var artname = artFile + ".cdr";
            var source = sourceFile + artname;
            var destFinal = destination + artname;

            if (!Directory.Exists(destination))
            {
                Directory.CreateDirectory(destination);
            }

            if (!File.Exists(destFinal))
            {
                try
                {
                    File.Copy(source, destFinal, true);
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }

                //   MessageDialogResult result = await UI.DialogYN("Confirm Overwrite", "The file already exists in the target directory. Would you like to overwrite it?");
                //    if (result == MessageDialogResult.Affirmative)
                {
                    //        try
                    {
                        //            File.Copy(source, destFinal, true);
                    }
                    //        catch (Exception ex)
                    {
                        //            new Error(ex);
                    }
                }
            }
        }

        public void DeleteDesignAndFiles()
        {
            if (SelectedDesignDetail != null)
            {
                var dpath = SelectedDesignDetail.DesignFolderPath;
                if (Directory.Exists(dpath))
                {
                    try
                    {
                        Utilities.DeleteDirectoryRecursive(dpath);
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }

                if (SelectedDesignDetail.DesignID > 0)
                {
                    using (var ctx = new Entities())
                    {
                        var dd =
                            (from c in ctx.designdetails where c.DesignID == SelectedDesignDetail.DesignID select c)
                                .FirstOrDefault();
                        ctx.designdetails.Remove(dd);
                        ctx.SaveChanges();
                    }
                }
            }
        }

        public bool CheckDesignFolder()
        {
            var result = true;

            if (string.IsNullOrEmpty(SelectedDesignDetail.DesignFolderPath))
            {
            }

            return result;
        }

        public void AddNewArtworkIfAny(object parameter)
        {
            var movedTotal = 0;
            if (FlaggedArtworkItems.Any())
            {
                try
                {
                    foreach (var item in FlaggedArtworkItems)
                    {
                        var type = "";
                        var destination = SelectedDesignDetail.DesignFolderPath + @"\CDR\";
                        var isYouth = SelectedDesignDetail.AP_YOUTH;

                        if (item == "Apron" || item == "Bandana" || item == "CapFront" || item == "RoundCoasters" ||
                            item == "GolfFlag"
                            || item == "GolfTowel" || item == "Koozies" || item == "Mousepad" || item == "Puzzle" ||
                            item == "SquareCoasters" || item == "ToteBag" || item == "WineBag")
                        {
                            type = "S";
                            //      CopyArtFile(item, type, destination);
                            movedTotal = movedTotal + 1;
                        }
                        else
                        {
                            if (isYouth)
                            {
                                type = "Y";
                                //      CopyArtFile("Youth" + item, type, destination);
                                movedTotal = movedTotal + 1;
                            }
                            else if (!isYouth)
                            {
                                type = "A";
                                //         CopyArtFile(item, type, destination);
                                movedTotal = movedTotal + 1;
                            }
                        }

                        if (!CurrentArtworkItems.Any(x => x == item))
                        {
                            CurrentArtworkItems.Add(item);
                            OnPropertyChanged("CurrentArtworkItems");
                            NeedsSave = true;
                        }
                    }

                    FlaggedArtworkItems.Clear();
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }

        public void GetArtwork()
        {
            if (SelectedDesignDetail != null)
            {
                if (!string.IsNullOrEmpty(SelectedDesignDetail.DesignFolderPath))
                {
                    IsBlankCard = false;
                    var desNum = 0;

                    if (ExistingArtworkItems.Count > 0)
                    {
                        ExistingArtworkItems.Clear();
                    }

                    if (ArtworkFolders.Count > 0)
                    {
                        ArtworkFolders.Clear();
                    }

                    // Get directories
                    try
                    {
                        if (Directory.Exists(SelectedDesignDetail.DesignFolderPath))
                        {
                            var dirs = Directory.GetDirectories(SelectedDesignDetail.DesignFolderPath);

                            foreach (var dir in dirs)
                            {
                                var fName = dir.Substring(dir.LastIndexOf('\\') + 1);
                                ArtworkFolders.Add(fName);

                                // Get files for each directory
                                var files =
                                    Directory.GetFiles(dir)
                                        .Where(x => !x.EndsWith(".db") && !x.EndsWith(".tmp"))
                                        .ToArray();

                                foreach (var file in files)
                                {
                                    var newVM = new DesignFileViewModel(file);

                                    if (file.EndsWith(".cdr"))
                                    {
                                        if (!file.ToLower().StartsWith("backup"))
                                        {
                                            desNum = desNum + 1;
                                        }
                                    }

                                    if (file.EndsWith(".ai"))
                                    {
                                        if (!file.ToLower().StartsWith("backup"))
                                        {
                                            desNum = desNum + 1;
                                        }
                                    }

                                    ExistingArtworkItems.Add(newVM);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                    DesignNum = desNum;
                }
            }
        }

        public void AddArtworkItems()
        {
            Busy = true;
            if (IsBlankCard)
            {
                FlaggedArtworkItems.Clear();
                CurrentArtworkItems.Clear();
                if (SelectedDesignDetail.AP_Apron && !CurrentArtworkItems.Any(x => x == "Apron"))
                {
                    FlaggedArtworkItems.Add("Apron");
                }
                if (SelectedDesignDetail.AP_Back && !CurrentArtworkItems.Any(x => x == "Back"))
                {
                    FlaggedArtworkItems.Add("Back");
                }
                if (SelectedDesignDetail.AP_BackBottom && !CurrentArtworkItems.Any(x => x == "BackBottom"))
                {
                    FlaggedArtworkItems.Add("BackBottom");
                }
                if (SelectedDesignDetail.AP_Bandana && !CurrentArtworkItems.Any(x => x == "Bandana"))
                {
                    FlaggedArtworkItems.Add("Bandana");
                }
                if (SelectedDesignDetail.AP_Cap && !CurrentArtworkItems.Any(x => x == "CapFront"))
                {
                    FlaggedArtworkItems.Add("CapFront");
                }
                if (SelectedDesignDetail.AP_ClCoaster && !CurrentArtworkItems.Any(x => x == "RoundCoasters"))
                {
                    FlaggedArtworkItems.Add("RoundCoasters");
                }
                if (SelectedDesignDetail.AP_Collar && !CurrentArtworkItems.Any(x => x == "Collar"))
                {
                    FlaggedArtworkItems.Add("Collar");
                }
                if (SelectedDesignDetail.AP_FLC && !CurrentArtworkItems.Any(x => x == "FrontLeftCrest"))
                {
                    FlaggedArtworkItems.Add("FrontLeftCrest");
                }
                if (SelectedDesignDetail.AP_FLCW && !CurrentArtworkItems.Any(x => x == "WomensFrontLeftCrest"))
                {
                    FlaggedArtworkItems.Add("WomensFrontLeftCrest");
                }
                if (SelectedDesignDetail.AP_FRC && !CurrentArtworkItems.Any(x => x == "FrontRightCrest"))
                {
                    FlaggedArtworkItems.Add("FrontRightCrest");
                }
                if (SelectedDesignDetail.AP_FRCW && !CurrentArtworkItems.Any(x => x == "WomensFrontRightCrest"))
                {
                    FlaggedArtworkItems.Add("WomensFrontRightCrest");
                }
                if (SelectedDesignDetail.AP_Front && !CurrentArtworkItems.Any(x => x == "Front"))
                {
                    FlaggedArtworkItems.Add("Front");
                }
                if (SelectedDesignDetail.AP_FrontBottom && !CurrentArtworkItems.Any(x => x == "FrontBottom"))
                {
                    FlaggedArtworkItems.Add("FrontBottom");
                }
                if (SelectedDesignDetail.AP_GolfFlag && !CurrentArtworkItems.Any(x => x == "GolfFlag"))
                {
                    FlaggedArtworkItems.Add("GolfFlag");
                }
                if (SelectedDesignDetail.AP_GolfTowel && !CurrentArtworkItems.Any(x => x == "GolfTowel"))
                {
                    FlaggedArtworkItems.Add("GolfTowel");
                }
                if (SelectedDesignDetail.AP_Koozie && !CurrentArtworkItems.Any(x => x == "Koozies"))
                {
                    FlaggedArtworkItems.Add("Koozies");
                }
                if (SelectedDesignDetail.AP_LeftSide && !CurrentArtworkItems.Any(x => x == "LeftSide"))
                {
                    FlaggedArtworkItems.Add("LeftSide");
                }
                if (SelectedDesignDetail.AP_LeftSleeve && !CurrentArtworkItems.Any(x => x == "LeftSleeve"))
                {
                    FlaggedArtworkItems.Add("LeftSleeve");
                }
                if (SelectedDesignDetail.AP_Mousepad && !CurrentArtworkItems.Any(x => x == "Mousepad"))
                {
                    FlaggedArtworkItems.Add("Mousepad");
                }
                if (SelectedDesignDetail.AP_Puzzle && !CurrentArtworkItems.Any(x => x == "Puzzle"))
                {
                    FlaggedArtworkItems.Add("Puzzle");
                }
                if (SelectedDesignDetail.AP_RightSide && !CurrentArtworkItems.Any(x => x == "RightSide"))
                {
                    FlaggedArtworkItems.Add("RightSide");
                }
                if (SelectedDesignDetail.AP_RightSleeve && !CurrentArtworkItems.Any(x => x == "RightSleeve"))
                {
                    FlaggedArtworkItems.Add("RightSleeve");
                }
                if (SelectedDesignDetail.AP_SqCoaster && !CurrentArtworkItems.Any(x => x == "SquareCoasters"))
                {
                    FlaggedArtworkItems.Add("SquareCoasters");
                }
                if (SelectedDesignDetail.AP_ToteBag && !CurrentArtworkItems.Any(x => x == "ToteBag"))
                {
                    FlaggedArtworkItems.Add("ToteBag");
                }
                if (SelectedDesignDetail.AP_WineBag && !CurrentArtworkItems.Any(x => x == "WineBag"))
                {
                    FlaggedArtworkItems.Add("WineBag");
                }
                if (SelectedDesignDetail.AP_Yoke && !CurrentArtworkItems.Any(x => x == "Yoke"))
                {
                    FlaggedArtworkItems.Add("Yoke");
                }

                AddNewArtworkIfAny(null);
                IsSaved = true;
                OnPropertyChanged("LineItems");
                OnPropertyChanged("ShirtCount");
                OnPropertyChanged("ArtCount");
                OnPropertyChanged("PrintCount");
            }
            else
            {
                using (var ctx = new Entities())
                {
                    try
                    {
                        var ThisDetail =
                            (from q in ctx.designdetails where q.DesignID == SelectedDesignDetail.DesignID select q)
                                .FirstOrDefault();

                        // Update designdetails from Viewmodel back to database
                        ThisDetail.DesignFolderPath = SelectedDesignDetail.DesignFolderPath;
                        ThisDetail.DesignDescription = SelectedDesignDetail.DesignDescription;

                        if (SelectedDesignDetail.AP_Apron && ThisDetail.AP_Apron != SelectedDesignDetail.AP_Apron)
                        {
                            FlaggedArtworkItems.Add("Apron");
                        }
                        ThisDetail.AP_Apron = SelectedDesignDetail.AP_Apron;
                        if (SelectedDesignDetail.AP_Back && ThisDetail.AP_Back != SelectedDesignDetail.AP_Back)
                        {
                            FlaggedArtworkItems.Add("Back");
                        }
                        ThisDetail.AP_Back = SelectedDesignDetail.AP_Back;
                        if (SelectedDesignDetail.AP_BackBottom &&
                            ThisDetail.AP_BackBottom != SelectedDesignDetail.AP_BackBottom)
                        {
                            FlaggedArtworkItems.Add("BackBottom");
                        }
                        ThisDetail.AP_BackBottom = SelectedDesignDetail.AP_BackBottom;
                        if (SelectedDesignDetail.AP_Bandana && ThisDetail.AP_Bandana != SelectedDesignDetail.AP_Bandana)
                        {
                            FlaggedArtworkItems.Add("Bandana");
                        }
                        ThisDetail.AP_Bandana = SelectedDesignDetail.AP_Bandana;
                        if (SelectedDesignDetail.AP_Cap && ThisDetail.AP_Cap != SelectedDesignDetail.AP_Cap)
                        {
                            FlaggedArtworkItems.Add("CapFront");
                        }
                        ThisDetail.AP_Cap = SelectedDesignDetail.AP_Cap;
                        if (SelectedDesignDetail.AP_ClCoaster &&
                            ThisDetail.AP_ClCoaster != SelectedDesignDetail.AP_ClCoaster)
                        {
                            FlaggedArtworkItems.Add("RoundCoasters");
                        }
                        ThisDetail.AP_ClCoaster = SelectedDesignDetail.AP_ClCoaster;
                        if (SelectedDesignDetail.AP_Collar && ThisDetail.AP_Collar != SelectedDesignDetail.AP_Collar)
                        {
                            FlaggedArtworkItems.Add("Collar");
                        }
                        ThisDetail.AP_Collar = SelectedDesignDetail.AP_Collar;
                        if (SelectedDesignDetail.AP_FLC && ThisDetail.AP_FLC != SelectedDesignDetail.AP_FLC)
                        {
                            FlaggedArtworkItems.Add("FrontLeftCrest");
                        }
                        ThisDetail.AP_FLC = SelectedDesignDetail.AP_FLC;
                        if (SelectedDesignDetail.AP_FLCW && ThisDetail.AP_FLCW != SelectedDesignDetail.AP_FLCW)
                        {
                            FlaggedArtworkItems.Add("WomensFrontLeftCrest");
                        }
                        ThisDetail.AP_FLCW = SelectedDesignDetail.AP_FLCW;
                        if (SelectedDesignDetail.AP_FRC && ThisDetail.AP_FRC != SelectedDesignDetail.AP_FRC)
                        {
                            FlaggedArtworkItems.Add("FrontRightCrest");
                        }
                        ThisDetail.AP_FRC = SelectedDesignDetail.AP_FRC;
                        if (SelectedDesignDetail.AP_FRCW && ThisDetail.AP_FRCW != SelectedDesignDetail.AP_FRCW)
                        {
                            FlaggedArtworkItems.Add("WomensFrontRightCrest");
                        }
                        ThisDetail.AP_FRCW = SelectedDesignDetail.AP_FRCW;
                        if (SelectedDesignDetail.AP_Front && ThisDetail.AP_Front != SelectedDesignDetail.AP_Front)
                        {
                            FlaggedArtworkItems.Add("Front");
                        }
                        ThisDetail.AP_Front = SelectedDesignDetail.AP_Front;
                        if (SelectedDesignDetail.AP_FrontBottom &&
                            ThisDetail.AP_FrontBottom != SelectedDesignDetail.AP_FrontBottom)
                        {
                            FlaggedArtworkItems.Add("FrontBottom");
                        }
                        ThisDetail.AP_FrontBottom = SelectedDesignDetail.AP_FrontBottom;
                        if (SelectedDesignDetail.AP_GolfFlag &&
                            ThisDetail.AP_GolfFlag != SelectedDesignDetail.AP_GolfFlag)
                        {
                            FlaggedArtworkItems.Add("GolfFlag");
                        }
                        ThisDetail.AP_GolfFlag = SelectedDesignDetail.AP_GolfFlag;
                        if (SelectedDesignDetail.AP_GolfTowel &&
                            ThisDetail.AP_GolfTowel != SelectedDesignDetail.AP_GolfTowel)
                        {
                            FlaggedArtworkItems.Add("GolfTowel");
                        }
                        ThisDetail.AP_GolfTowel = SelectedDesignDetail.AP_GolfTowel;
                        if (SelectedDesignDetail.AP_Koozie && ThisDetail.AP_Koozie != SelectedDesignDetail.AP_Koozie)
                        {
                            FlaggedArtworkItems.Add("Koozies");
                        }
                        ThisDetail.AP_Koozie = SelectedDesignDetail.AP_Koozie;
                        if (SelectedDesignDetail.AP_LeftSide &&
                            ThisDetail.AP_LeftSide != SelectedDesignDetail.AP_LeftSide)
                        {
                            FlaggedArtworkItems.Add("LeftSide");
                        }
                        ThisDetail.AP_LeftSide = SelectedDesignDetail.AP_LeftSide;
                        if (SelectedDesignDetail.AP_LeftSleeve &&
                            ThisDetail.AP_LeftSleeve != SelectedDesignDetail.AP_LeftSleeve)
                        {
                            FlaggedArtworkItems.Add("LeftSleeve");
                        }
                        ThisDetail.AP_LeftSleeve = SelectedDesignDetail.AP_LeftSleeve;
                        if (SelectedDesignDetail.AP_Mousepad &&
                            ThisDetail.AP_Mousepad != SelectedDesignDetail.AP_Mousepad)
                        {
                            FlaggedArtworkItems.Add("Mousepad");
                        }
                        ThisDetail.AP_Mousepad = SelectedDesignDetail.AP_Mousepad;
                        if (SelectedDesignDetail.AP_Puzzle && ThisDetail.AP_Puzzle != SelectedDesignDetail.AP_Puzzle)
                        {
                            FlaggedArtworkItems.Add("Puzzle");
                        }
                        ThisDetail.AP_Puzzle = SelectedDesignDetail.AP_Puzzle;
                        if (SelectedDesignDetail.AP_RightSide &&
                            ThisDetail.AP_RightSide != SelectedDesignDetail.AP_RightSide)
                        {
                            FlaggedArtworkItems.Add("RightSide");
                        }
                        ThisDetail.AP_RightSide = SelectedDesignDetail.AP_RightSide;
                        if (SelectedDesignDetail.AP_RightSleeve &&
                            ThisDetail.AP_RightSleeve != SelectedDesignDetail.AP_RightSleeve)
                        {
                            FlaggedArtworkItems.Add("RightSleeve");
                        }
                        ThisDetail.AP_RightSleeve = SelectedDesignDetail.AP_RightSleeve;
                        if (SelectedDesignDetail.AP_SqCoaster &&
                            ThisDetail.AP_SqCoaster != SelectedDesignDetail.AP_SqCoaster)
                        {
                            FlaggedArtworkItems.Add("SquareCoasters");
                        }
                        ThisDetail.AP_SqCoaster = SelectedDesignDetail.AP_SqCoaster;
                        if (SelectedDesignDetail.AP_ToteBag && ThisDetail.AP_ToteBag != SelectedDesignDetail.AP_ToteBag)
                        {
                            FlaggedArtworkItems.Add("ToteBag");
                        }
                        ThisDetail.AP_ToteBag = SelectedDesignDetail.AP_ToteBag;
                        if (SelectedDesignDetail.AP_WineBag && ThisDetail.AP_WineBag != SelectedDesignDetail.AP_WineBag)
                        {
                            FlaggedArtworkItems.Add("WineBag");
                        }
                        ThisDetail.AP_WineBag = SelectedDesignDetail.AP_WineBag;
                        if (SelectedDesignDetail.AP_Yoke && ThisDetail.AP_Yoke != SelectedDesignDetail.AP_Yoke)
                        {
                            FlaggedArtworkItems.Add("Yoke");
                        }
                        ThisDetail.AP_Yoke = SelectedDesignDetail.AP_Yoke;
                        ThisDetail.AP_YOUTH = SelectedDesignDetail.AP_YOUTH;
                        ThisDetail.SizeToGarment = SelectedDesignDetail.SizeToGarment;

                        AddNewArtworkIfAny(null);

                        // Update lineitems from Viewmodel back to database

                        foreach (var item in LineItems)
                        {
                            if (item.SelectedLineItem.UniqueID != 0)
                            {
                                var Key = item.SelectedLineItem.UniqueID;
                                var itemToEdit =
                                    (from k in ctx.designdetailslineitems where k.UniqueID == Key select k)
                                        .FirstOrDefault();
                                itemToEdit.Color = item.SelectedLineItem.Color;
                                itemToEdit.Description = item.SelectedLineItem.Description;
                                itemToEdit.DesignID = item.SelectedLineItem.DesignID;
                                itemToEdit.Quantity = item.SelectedLineItem.Quantity;
                                itemToEdit.ReceiptNum = item.SelectedLineItem.ReceiptNum;
                                itemToEdit.Size = item.SelectedLineItem.Size;
                                itemToEdit.StockIDPOS = item.SelectedLineItem.StockIDPOS;
                            }
                            // ctx.SaveChanges();
                        }

                        ctx.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                    finally
                    {
                        IsSaved = true;
                        OnPropertyChanged("LineItems");
                        OnPropertyChanged("ShirtCount");
                        OnPropertyChanged("ArtCount");
                        OnPropertyChanged("PrintCount");
                    }
                }
            }
        }

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }

        #endregion PropertyChanged Controller
    }
}