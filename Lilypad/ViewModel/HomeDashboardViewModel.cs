using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.IO;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;
using DevExpress.Xpf.Mvvm;

namespace Lilypad.ViewModel
{
    public class HomeDashboardViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public HomeDashboardViewModel()
        {
            orderscollection = new ObservableCollection<hopperorder>();
            ordersduetodaycollection = new ObservableCollection<hopperorder>();
            ordersduetmwcollection = new ObservableCollection<hopperorder>();
            myordersduetodaycollection = new ObservableCollection<hopperorder>();
            myordersduetmwcollection = new ObservableCollection<hopperorder>();
            alltaskscollection = new ObservableCollection<task>();
            mytaskcollection = new ObservableCollection<task>();
            selectedorder = new hopperorder();
        }

        public ObservableCollection<frogger> Froggers
        {
            get { return new ObservableCollection<frogger>(((MainViewModel)App.Current.MainWindow.DataContext).CurrentFroggers); }
        }

        public void FillOrdersCollection()
        {
            HopperOrdersCollection.Clear();

            List<hopperorder> founditems = new List<hopperorder>();

            using (Entities ctx = new Entities())
            {
                if (ctx.hopperorders.Any())
                {
                    founditems = (from t in ctx.hopperorders.Include("priority1").Include("printprocess1") select t).ToList();
                }

                foreach (hopperorder item in founditems)
                {
                    HopperOrdersCollection.Add(item);
                }

            }

            

            CalculateCounts();
            
            FillOrdersCollectionsFromPrimary();
        }

        public void FillOrdersCollectionsFromPrimary()
        {
            OrdersDueToday.Clear();
            OrdersDueTomorrow.Clear();
            MyOrdersDueToday.Clear();
            MyOrdersDueTomorrow.Clear();

            var pVM = (MainViewModel)Application.Current.MainWindow.DataContext;
            int thisFrog;
            if (pVM.LoggedInFrogger != null)
            { thisFrog = pVM.LoggedInFrogger.FrogID; }
            else { thisFrog = 0; }
            
            DateTime now = DateTime.Now;
            DateTime TodayStart = DateTime.Parse(now.ToString("yyyy-MM-dd") + " 00:00:00");
            DateTime TodayEnd = DateTime.Parse(now.ToString("yyyy-MM-dd") + " 23:59:59");
            DateTime TomorrowStart = DateTime.Parse(now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00");
            DateTime TomorrowEnd = DateTime.Parse(now.AddDays(1).ToString("yyyy-MM-dd") + " 23:59:59");

            var allToday = (from q in HopperOrdersCollection where q.DueDate <= TodayEnd && q.DueDate >= TodayStart select q).ToList();
            var allTmw = (from q in HopperOrdersCollection where q.DueDate <= TomorrowEnd && q.DueDate >= TomorrowStart select q).ToList();
            var mineToday = (from q in HopperOrdersCollection where q.DueDate <= TodayEnd && q.DueDate >= TodayStart && q.DesignFrog == thisFrog select q).ToList();
            var mineTmw = (from q in HopperOrdersCollection where q.DesignFrog == thisFrog select q).ToList();

            foreach (hopperorder order in allToday) { OrdersDueToday.Add(order); }
            foreach (hopperorder order in allTmw) { OrdersDueTomorrow.Add(order); }
            foreach (hopperorder order in mineToday) { MyOrdersDueToday.Add(order); }
            foreach (hopperorder order in mineTmw) { MyOrdersDueTomorrow.Add(order); }


            MyOrders_AwaitApproval = MyOrdersDueTomorrow.Where(x => x.Status == "Awaiting Art Approval").Count();
            MyOrders_AwaitPayment = MyOrdersDueTomorrow.Where(x => x.Status == "Awaiting Artwork").Count();
            MyOrders_Ready = MyOrdersDueTomorrow.Where(x => x.Status == "Ready To Print").Count();
            MyOrders_Working = MyOrdersDueTomorrow.Where(x => x.Status == "Working On Artwork").Count();
            MyOrders_OnHold = MyOrdersDueTomorrow.Where(x => x.Status == "On Hold").Count();

        }


        hopperorder selectedorder;
        public hopperorder SelectedHopperOrder
        {
            get { return selectedorder; }
            set { selectedorder = value; OnPropertyChanged("SelectedHopperOrder"); }
        }

        public void FillTasksCollection()
        {
            List<task> alltasks = new List<task>();
            List<task> mytasks = new List<task>();
            AllTasksCollection = new ObservableCollection<task>(alltasks);
            MyTasksCollection = new ObservableCollection<task>(mytasks);
            MainViewModel pvm = (MainViewModel)Application.Current.MainWindow.DataContext;

            if (pvm.LoggedInFrogger.FrogID > 0)
            {

                using (Entities ctx = new Entities())
                {
                    alltasks = (from t in ctx.tasks where t.IsCompleted == false select t).ToList();
                }

                mytasks = (from k in alltasks where k.FrogAssigned == pvm.LoggedInFrogger.FrogID || k.FrogCreated == pvm.LoggedInFrogger.FrogID || k.IsGlobal select k).ToList();

                foreach (task item in mytasks)
                {
                    if (!MyTasksCollection.Contains(item)) MyTasksCollection.Add(item); 
                }
                foreach (task item in alltasks)
                {
                    if (!AllTasksCollection.Contains(item)) AllTasksCollection.Add(item);
                }
            }
        }

        public DataTable salesData { get; set; }

        public void CalculateGoalsViaFRED()
        {
            string rPath = Properties.Settings.Default.Data_ResourcePath.ToString();
            string fPath = Path.Combine(rPath, "FREDLog.mdb").ToString();
            salesData = new DataTable();

            if (File.Exists(fPath))
            {
                goal thisGoal = new goal();
                decimal currsales = new decimal();

                try
                {
                    using (Entities ctx = new Entities())
                    {
                        int thisMonth = DateTime.Now.Month;
                        int nextMonth = thisMonth + 1;
                        int year = DateTime.Now.Year;
                        var goaldateStart = DateTime.Parse(year + "-" + thisMonth + "-01 00:00:00");
                        var goaldateEnd = DateTime.Parse(year + "-" + nextMonth + "-01 00:00:00");
                        if (ctx.goals.Any(x => x.GoalDate == goaldateStart.Date))
                        {
                            thisGoal = (from q in ctx.goals where q.GoalDate == goaldateStart.Date select q).FirstOrDefault();
                        }
                        else
                        {
                            thisGoal.GoalAmt = 1.00m;
                        }

                        string connString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + fPath + ";";

                        string gBegin = year + "-" + thisMonth + "-01";
                        string gEnd = year + "-" + nextMonth + "-01";

                        using (System.Data.OleDb.OleDbConnection dbConn = new System.Data.OleDb.OleDbConnection(connString))
                        {
                            string Select = @"SELECT EffectiveDate, TotalReceiptAmount FROM UploadDates WHERE EffectiveDate Between #" + gBegin + "# And #" + gEnd + "# ORDER BY EffectiveDate DESC";
                            System.Data.OleDb.OleDbDataAdapter adapter = new System.Data.OleDb.OleDbDataAdapter(Select, dbConn);
                            try
                            {
                                dbConn.Open();
                                adapter.Fill(salesData);
                                dbConn.Close();
                            }
                            catch (Exception ex)
                            {
                                new Error(ex);
                            }
                        }
                        var sum = salesData.AsEnumerable().Sum(x => x.Field<decimal>("TotalReceiptAmount"));
                        currsales = Math.Round(sum, 2);
                    }
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }

                MonthlyGoal = thisGoal;
                CurrentSales = currsales;
                decimal goalamt = thisGoal.GoalAmt;

                if (goalamt == 0) { goalamt = 1.00m; }
                if (currsales == 0) { currsales = 1; }

                var math = Math.Round(currsales / goalamt * 100);
                var percent = Convert.ToInt32(math);
                if (math > 100)
                {
                    PercentGoalComplete = percent;
                    Percent100 = 100;
                }
                else
                {
                    PercentGoalComplete = percent;
                    Percent100 = percent;
                }

                MonthCommon = DateTime.Now.ToString("MMMM yyyy");

            }
        }

        public void RecalculateGoals()
        {
            goal thisGoal = new goal();
            decimal currsales = new decimal();

            try
            {
                using (Entities ctx = new Entities())
                {
                    int thisMonth = DateTime.Now.Month;
                    int nextMonth = 0;
                    int year = DateTime.Now.Year;
                    int yearOut = 0; 

                    if (thisMonth < 12) 
                    { 
                        nextMonth = thisMonth++;
                        yearOut = year;
                    }
                    else { nextMonth = 1; yearOut = year++; }

                    var goaldateStart = DateTime.Parse(year + "-" + thisMonth + "-01 00:00:00");
                    var goaldateEnd = DateTime.Parse(year + "-" + nextMonth + "-01 00:00:00");
                    if (ctx.goals.Any(x => x.GoalDate == goaldateStart.Date))
                    {
                        thisGoal = (from q in ctx.goals where q.GoalDate == goaldateStart.Date select q).FirstOrDefault();
                    }
                    else
                    {
                        thisGoal.GoalAmt = 1.00m;
                    }
                    if (ctx.receipts.Any(x => x.RcptDateTime >= goaldateStart))
                    {
                        currsales = (from t in ctx.receipts where t.RcptDateTime >= goaldateStart && t.RcptDateTime < goaldateEnd select t.RcptTotal).Sum();
                    }
                    else
                    {
                        currsales = 1.00m;
                    }
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }

            MonthlyGoal = thisGoal;
            CurrentSales = currsales;
            decimal goalamt = thisGoal.GoalAmt;

            if (goalamt == 0) { goalamt = 1.00m; }
            if (currsales == 0) { currsales = 1; }

            var math = Math.Round(currsales / goalamt * 100);
            var percent = Convert.ToInt32(math);
            if (math > 100)
            {
                PercentGoalComplete = percent;
                Percent100 = 100;
            }
            else
            {
                PercentGoalComplete = percent;
                Percent100 = percent;
            }

            MonthCommon = DateTime.Now.ToString("MMMM yyyy");
        }

        public void CalculateCounts()
        {
            DateTime now = DateTime.Now;
            DateTime TodayStart = DateTime.Parse(now.ToString("yyyy-MM-dd") + " 00:00:00");
            DateTime TodayEnd = DateTime.Parse(now.ToString("yyyy-MM-dd") + " 23:59:59");
            DateTime TomorrowStart = DateTime.Parse(now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00");
            DateTime TomorrowEnd = DateTime.Parse(now.AddDays(1).ToString("yyyy-MM-dd") + " 23:59:59");

            // ------------------ Calculate Past Due Block 

            PastDue_TotalOrders = (from q in HopperOrdersCollection where q.DueDate < TodayStart && q.Status != "Completed" && q.Status != "Picked Up" select q).Count();
            PastDue_TotalItems = (from q in HopperOrdersCollection where q.DueDate < TodayStart select q.TotalShirts).Sum();
            PastDue_TotalPrints = (from q in HopperOrdersCollection where q.DueDate < TodayStart select q.TotalPrints).Sum();
            PastDue_Priority_RushItems = (from q in HopperOrdersCollection where q.DueDate <= TodayStart && q.Priority == 3 select q.TotalShirts).Sum();
            PastDue_Priority_RushPrints = (from q in HopperOrdersCollection where q.DueDate <= TodayStart && q.Priority == 3 select q.TotalPrints).Sum();
            PastDue_Priority_HighItems = (from q in HopperOrdersCollection where q.DueDate <= TodayStart && q.Priority == 2 select q.TotalShirts).Sum();
            PastDue_Priority_HighPrints = (from q in HopperOrdersCollection where q.DueDate <= TodayStart && q.Priority == 2 select q.TotalPrints).Sum();
            PastDue_Priority_StdItems = (from q in HopperOrdersCollection where q.DueDate <= TodayStart && q.Priority == 1 select q.TotalShirts).Sum();
            PastDue_Priority_StdPrints = (from q in HopperOrdersCollection where q.DueDate <= TodayStart && q.Priority == 1 select q.TotalPrints).Sum();

            // ------------------ Calculate Due Today Block 

            DueToday_TotalOrders = (from q in HopperOrdersCollection where q.DueDate <= TodayEnd && q.DueDate >= TodayStart select q).Count();
            DueToday_TotalItems = (from q in HopperOrdersCollection where q.DueDate <= TodayEnd && q.DueDate >= TodayStart select q.TotalShirts).Sum();
            DueToday_TotalPrints = (from q in HopperOrdersCollection where q.DueDate <= TodayEnd && q.DueDate >= TodayStart select q.TotalPrints).Sum();
            DueToday_Priority_RushItems = (from q in HopperOrdersCollection where q.DueDate <= TodayEnd && q.DueDate >= TodayStart && q.Priority == 3 select q.TotalShirts).Sum();
            DueToday_Priority_RushPrints = (from q in HopperOrdersCollection where q.DueDate <= TodayEnd && q.DueDate >= TodayStart && q.Priority == 3 select q.TotalPrints).Sum();
            DueToday_Priority_HighItems = (from q in HopperOrdersCollection where q.DueDate <= TodayEnd && q.DueDate >= TodayStart && q.Priority == 2 select q.TotalShirts).Sum();
            DueToday_Priority_HighPrints = (from q in HopperOrdersCollection where q.DueDate <= TodayEnd && q.DueDate >= TodayStart && q.Priority == 2 select q.TotalPrints).Sum();
            DueToday_Priority_StdItems = (from q in HopperOrdersCollection where q.DueDate <= TodayEnd && q.DueDate >= TodayStart && q.Priority == 1 select q.TotalShirts).Sum();
            DueToday_Priority_StdPrints = (from q in HopperOrdersCollection where q.DueDate <= TodayEnd && q.DueDate >= TodayStart && q.Priority == 1 select q.TotalPrints).Sum();

            // ------------------ Calculate Due Tomorrow Block 

            OrdersTakenToday = (from q in HopperOrdersCollection where q.OrderDate <= TodayEnd && q.OrderDate >= TodayStart select q).Count();

            DueTmw_TotalOrders = (from q in HopperOrdersCollection where q.DueDate <= TomorrowEnd && q.DueDate >= TomorrowStart select q).Count();
            DueTmw_TotalItems = (from q in HopperOrdersCollection where q.DueDate <= TomorrowEnd && q.DueDate >= TomorrowStart select q.TotalShirts).Sum();
            DueTmw_TotalPrints = (from q in HopperOrdersCollection where q.DueDate <= TomorrowEnd && q.DueDate >= TomorrowStart select q.TotalPrints).Sum();
            DueTmw_Priority_RushItems = (from q in HopperOrdersCollection where q.DueDate <= TomorrowEnd && q.DueDate >= TomorrowStart && q.Priority == 3 select q.TotalShirts).Sum();
            DueTmw_Priority_RushPrints = (from q in HopperOrdersCollection where q.DueDate <= TomorrowEnd && q.DueDate >= TomorrowStart && q.Priority == 3 select q.TotalPrints).Sum();
            DueTmw_Priority_HighItems = (from q in HopperOrdersCollection where q.DueDate <= TomorrowEnd && q.DueDate >= TomorrowStart && q.Priority == 2 select q.TotalShirts).Sum();
            DueTmw_Priority_HighPrints = (from q in HopperOrdersCollection where q.DueDate <= TomorrowEnd && q.DueDate >= TomorrowStart && q.Priority == 2 select q.TotalPrints).Sum();
            DueTmw_Priority_StdItems = (from q in HopperOrdersCollection where q.DueDate <= TomorrowEnd && q.DueDate >= TomorrowStart && q.Priority == 1 select q.TotalShirts).Sum();
            DueTmw_Priority_StdPrints = (from q in HopperOrdersCollection where q.DueDate <= TomorrowEnd && q.DueDate >= TomorrowStart && q.Priority == 1 select q.TotalPrints).Sum();

            try
            {
                using (Entities ctx = new Entities())
                {
                    CustsToCallToday = (from t in ctx.calllists where t.Called == false && t.Archived == false select t).Count();
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        int orderstakentoday;
        public int OrdersTakenToday
        {
            get { return orderstakentoday; }
            set { orderstakentoday = value; OnPropertyChanged("OrdersTakenToday"); }
        }


        int custstocalltoday;
        public int CustsToCallToday
        {
            get { return custstocalltoday; }
            set { custstocalltoday = value; OnPropertyChanged("CustsToCallToday"); }
        }


        ObservableCollection<hopperorder> orderscollection;
        public ObservableCollection<hopperorder> HopperOrdersCollection
        {
            get {
                if (orderscollection == null)
                {
                    FillOrdersCollection();
                }
                return orderscollection; }
            set { orderscollection = value; OnPropertyChanged("HopperOrdersCollection"); }
        }

        ObservableCollection<hopperorder> ordersduetodaycollection;
        public ObservableCollection<hopperorder> OrdersDueToday
        {
            get
            {
                return ordersduetodaycollection;
            }
            set { ordersduetodaycollection = value; OnPropertyChanged("OrdersDueToday"); }
        }

        ObservableCollection<hopperorder> ordersduetmwcollection;
        public ObservableCollection<hopperorder> OrdersDueTomorrow
        {
            get
            {
                return ordersduetmwcollection;
            }
            set { ordersduetmwcollection = value; OnPropertyChanged("OrdersDueTomorrow"); }
        }

        ObservableCollection<hopperorder> myordersduetmwcollection;
        public ObservableCollection<hopperorder> MyOrdersDueTomorrow
        {
            get
            {
                return myordersduetmwcollection;
            }
            set { myordersduetmwcollection = value; OnPropertyChanged("MyOrdersDueTomorrow"); }
        }

        ObservableCollection<hopperorder> myordersduetodaycollection;
        public ObservableCollection<hopperorder> MyOrdersDueToday
        {
            get
            {
                return myordersduetodaycollection;
            }
            set { myordersduetodaycollection = value; OnPropertyChanged("MyOrdersDueToday"); }
        }

        ObservableCollection<task> alltaskscollection;
        public ObservableCollection<task> AllTasksCollection
        {
            get
            {
                if (alltaskscollection == null)
                {
                    FillTasksCollection();
                }
                return alltaskscollection;
            }
            set { alltaskscollection = value; OnPropertyChanged("AllTasksCollection"); }
        }


        ObservableCollection<task> mytaskcollection;
        public ObservableCollection<task> MyTasksCollection
        {
            get
            {
                if (mytaskcollection == null)
                {
                    FillTasksCollection();
                }
                return mytaskcollection;
            }
            set { mytaskcollection = value; OnPropertyChanged("MyTasksCollection"); }
        }

        hopperorder selectedOrder;
        public hopperorder SelectedOrder
        {
            get { return selectedOrder; }
            set
            {
                selectedOrder = value; OnPropertyChanged("SelectedOrder");
            }
        }

        public void SaveGoalChanges()
        {
            using (Entities ctx = new Entities())
            {
                int thisMonth = DateTime.Now.Month;
                int year = DateTime.Now.Year;
                var goaldate = DateTime.Parse(year + "-" + thisMonth + "-01").Date;
                var thisGoal = (from q in ctx.goals where q.GoalDate == goaldate select q).FirstOrDefault();
                thisGoal.GoalAmt = MonthlyGoal.GoalAmt;
                if (ActualSales != 0.00m)
                {
                    thisGoal.ActualAmt = ActualSales;
                }
                else
                {
                    thisGoal.ActualAmt = 0.00m;
                }
                ctx.SaveChanges();
            }
        }

        public void EditGoal()
        {
            bool goalExists = false;
            int thisMonth = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            var goaldate = DateTime.Parse(year + "-" + thisMonth + "-01").Date;

            using (Entities ctx = new Entities())
            {
                goalExists = (from c in ctx.goals where c.GoalDate == goaldate select c).Any();
            }

            if (goalExists) { SaveGoalChanges(); }
            else { CreateNewGoal(); }
        }

        public void CreateNewGoal()
        {
            using (Entities ctx = new Entities())
            {
                int thisMonth = DateTime.Now.Month;
                int year = DateTime.Now.Year;

                Model.goal newGoal = new Model.goal();
                newGoal.ActualAmt = 0.00m;
                newGoal.GoalAmt = GoalAmount;
                newGoal.GoalDate = DateTime.Parse(DateTime.Now.Year + "-" + DateTime.Now.Month + "-01");
                ctx.goals.Add(newGoal);
                ctx.SaveChanges();
            }
        }

        goal monthlyGoal;
        public goal MonthlyGoal
        {
            get { return monthlyGoal; }
            set { monthlyGoal = value; OnPropertyChanged("MonthlyGoal"); }
        }

        public decimal GoalAmount
        {
            get
            {
                if (monthlyGoal != null)
                {
                    return monthlyGoal.GoalAmt;
                }
                else
                {
                    return 0.00m;
                }
            }
            set
            {
                if (monthlyGoal != null)
                {
                    monthlyGoal.GoalAmt = (Decimal)value; OnPropertyChanged("MonthlyGoal"); OnPropertyChanged("GoalAmount");
                }
                else
                {
                    monthlyGoal = new goal();
                    monthlyGoal.GoalAmt = (Decimal)value; OnPropertyChanged("MonthlyGoal"); OnPropertyChanged("GoalAmount");
                }
            }
        }

        string monthcommon;
        public string MonthCommon
        {
            get { return monthcommon; }
            set { monthcommon = value; OnPropertyChanged("MonthCommon"); }
        }

        int percentcomplete;
        public int PercentGoalComplete
        {
            get { return percentcomplete; }
            set { percentcomplete = value; OnPropertyChanged("PercentGoalComplete"); }
        }

        int percentcomplete100;
        public int Percent100
        {
            get { return percentcomplete100; }
            set { percentcomplete100 = value; OnPropertyChanged("Percent100"); }
        }


        decimal actualsales;
        public Decimal ActualSales
        {
            get { return actualsales; }
            set { actualsales = value; OnPropertyChanged("ActualSales"); }
        }

        decimal currentSales;
        public Decimal CurrentSales
        {
            get { return currentSales; }
            set { currentSales = value; OnPropertyChanged("CurrentSales"); }
        }


        // ---------------- MY ORDERS BLOCK

        bool ismyorders;
        public bool IsMyOrders
        {
            get { return ismyorders; }
            set { ismyorders = value; OnPropertyChanged("IsMyOrders"); }
        }

        int myorders_ready;
        public int MyOrders_Ready
        {
            get { return myorders_ready; }
            set { myorders_ready = value; OnPropertyChanged("MyOrders_Ready"); }
        }


        int myorders_awaitpmt;
        public int MyOrders_AwaitPayment
        {
            get { return myorders_awaitpmt; }
            set { myorders_awaitpmt = value; OnPropertyChanged("MyOrders_AwaitPayment"); }
        }

        int myorders_awaitapp;
        public int MyOrders_AwaitApproval
        {
            get { return myorders_awaitapp; }
            set { myorders_awaitapp = value; OnPropertyChanged("MyOrders_AwaitApproval"); }
        }

        int myorders_working;
        public int MyOrders_Working
        {
            get { return myorders_working; }
            set { myorders_working = value; OnPropertyChanged("MyOrders_Working"); }
        }

        int myorders_onhold;
        public int MyOrders_OnHold
        {
            get { return myorders_onhold; }
            set { myorders_onhold = value; OnPropertyChanged("MyOrders_OnHold"); }
        }

        // ---------------- PAST DUE BLOCK


        int pastdue_totalorders;
        public int PastDue_TotalOrders
        {
            get { return pastdue_totalorders; }
            set { pastdue_totalorders = value; OnPropertyChanged("PastDue_TotalOrders"); }
        }

        int pastdue_totalitems;
        public int PastDue_TotalItems
        {
            get { return pastdue_totalitems; }
            set { pastdue_totalitems = value; OnPropertyChanged("PastDue_TotalItems"); }
        }

        int pastdue_totalprints;
        public int PastDue_TotalPrints
        {
            get { return pastdue_totalprints; }
            set { pastdue_totalprints = value; OnPropertyChanged("PastDue_TotalPrints"); }
        }

        int pastdue_rushitems;
        public int PastDue_Priority_RushItems
        {
            get { return pastdue_rushitems; }
            set { pastdue_rushitems = value; OnPropertyChanged("PastDue_Priority_RushItems"); }
        }

        int pastdue_rushprints;
        public int PastDue_Priority_RushPrints
        {
            get { return pastdue_rushprints; }
            set { pastdue_rushprints = value; OnPropertyChanged("PastDue_Priority_RushPrints"); }
        }

        int pastdue_highitems;
        public int PastDue_Priority_HighItems
        {
            get { return pastdue_highitems; }
            set { pastdue_highitems = value; OnPropertyChanged("PastDue_Priority_HighItems"); }
        }

        int pastdue_highprints;
        public int PastDue_Priority_HighPrints
        {
            get { return pastdue_highprints; }
            set { pastdue_highprints = value; OnPropertyChanged("PastDue_Priority_HighPrints"); }
        }

        int pastdue_stditems;
        public int PastDue_Priority_StdItems
        {
            get { return pastdue_stditems; }
            set { pastdue_stditems = value; OnPropertyChanged("PastDue_Priority_StdItems"); }
        }

        int pastdue_stdprints;
        public int PastDue_Priority_StdPrints
        {
            get { return pastdue_stdprints; }
            set { pastdue_stdprints = value; OnPropertyChanged("PastDue_Priority_StdPrints"); }
        }

        // ---------------- DUE TODAY BLOCK

        int duetoday_totalorders;
        public int DueToday_TotalOrders
        {
            get { return duetoday_totalorders; }
            set { duetoday_totalorders = value; OnPropertyChanged("DueToday_TotalOrders"); }
        }

        int duetoday_totalitems;
        public int DueToday_TotalItems
        {
            get { return duetoday_totalitems; }
            set { duetoday_totalitems = value; OnPropertyChanged("DueToday_TotalItems"); }
        }

        int duetoday_totalprints;
        public int DueToday_TotalPrints
        {
            get { return duetoday_totalprints; }
            set { duetoday_totalprints = value; OnPropertyChanged("DueToday_TotalPrints"); }
        }

        int duetoday_rushitems;
        public int DueToday_Priority_RushItems
        {
            get { return duetoday_rushitems; }
            set { duetoday_rushitems = value; OnPropertyChanged("DueToday_Priority_RushItems"); }
        }

        int duetoday_rushprints;
        public int DueToday_Priority_RushPrints
        {
            get { return duetoday_rushprints; }
            set { duetoday_rushprints = value; OnPropertyChanged("DueToday_Priority_RushPrints"); }
        }

        int duetoday_highitems;
        public int DueToday_Priority_HighItems
        {
            get { return duetoday_highitems; }
            set { duetoday_highitems = value; OnPropertyChanged("DueToday_Priority_HighItems"); }
        }

        int duetoday_highprints;
        public int DueToday_Priority_HighPrints
        {
            get { return duetoday_highprints; }
            set { duetoday_highprints = value; OnPropertyChanged("DueToday_Priority_HighPrints"); }
        }

        int duetoday_stditems;
        public int DueToday_Priority_StdItems
        {
            get { return duetoday_stditems; }
            set { duetoday_stditems = value; OnPropertyChanged("DueToday_Priority_StdItems"); }
        }

        int duetoday_stdprints;
        public int DueToday_Priority_StdPrints
        {
            get { return duetoday_stdprints; }
            set { duetoday_stdprints = value; OnPropertyChanged("DueToday_Priority_StdPrints"); }
        }

        // ---------------- DUE TOMORROW BLOCK

        int duetmw_totalorders;
        public int DueTmw_TotalOrders
        {
            get { return duetmw_totalorders; }
            set { duetmw_totalorders = value; OnPropertyChanged("DueTmw_TotalOrders"); }
        }

        int duetmw_totalitems;
        public int DueTmw_TotalItems
        {
            get { return duetmw_totalitems; }
            set { duetmw_totalitems = value; OnPropertyChanged("DueTmw_TotalItems"); }
        }

        int duetmw_totalprints;
        public int DueTmw_TotalPrints
        {
            get { return duetmw_totalprints; }
            set { duetmw_totalprints = value; OnPropertyChanged("DueTmw_TotalPrints"); }
        }

        int duetmw_rushitems;
        public int DueTmw_Priority_RushItems
        {
            get { return duetmw_rushitems; }
            set { duetmw_rushitems = value; OnPropertyChanged("DueTmw_Priority_RushItems"); }
        }

        int duetmw_rushprints;
        public int DueTmw_Priority_RushPrints
        {
            get { return duetmw_rushprints; }
            set { duetmw_rushprints = value; OnPropertyChanged("DueTmw_Priority_RushPrints"); }
        }

        int duetmw_highitems;
        public int DueTmw_Priority_HighItems
        {
            get { return duetmw_highitems; }
            set { duetmw_highitems = value; OnPropertyChanged("DueTmw_Priority_HighItems"); }
        }

        int duetmw_highprints;
        public int DueTmw_Priority_HighPrints
        {
            get { return duetmw_highprints; }
            set { duetmw_highprints = value; OnPropertyChanged("DueTmw_Priority_HighPrints"); }
        }

        int duetmw_stditems;
        public int DueTmw_Priority_StdItems
        {
            get { return duetmw_stditems; }
            set { duetmw_stditems = value; OnPropertyChanged("DueTmw_Priority_StdItems"); }
        }

        int duetmw_stdprints;
        public int DueTmw_Priority_StdPrints
        {
            get { return duetmw_stdprints; }
            set { duetmw_stdprints = value; OnPropertyChanged("DueTmw_Priority_StdPrints"); }
        }
       

        // ---------------- COMMANDS

        DelegateCommand saveCommand;
        public DelegateCommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new DelegateCommand(Save);
                }
                return saveCommand;
            }
        }

        void Save()
        {
            try
            {
                using (Entities context = new Entities())
                {
                    var thisorder = context.hopperorders.FirstOrDefault(o => o.ReceiptNum == SelectedOrder.ReceiptNum);
                    thisorder.Priority = SelectedOrder.Priority;
                    thisorder.SearchTags = SelectedOrder.SearchTags;
                    thisorder.DueDate = SelectedOrder.DueDate;
                    thisorder.DesignFrog = SelectedOrder.DesignFrog;
                    thisorder.Status = SelectedOrder.Status;
                    thisorder.PrintProcess = SelectedOrder.PrintProcess;
                    thisorder.BoxNum = SelectedOrder.BoxNum;
                    thisorder.TotalShirts = SelectedOrder.TotalShirts;
                    thisorder.Irregulars = SelectedOrder.Irregulars;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}