﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.Controls.Tags;


namespace Lilypad.ViewModel
{
    public class UltraPrintViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;
        public ICollection<ultraprintorderdata> ultraprintorderdataList;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public UltraPrintViewModel()
        {
            tagcollection = new ObservableCollection<Tag>();
            SaveCompleted = true;
            NeedsSave = false;

            using (Entities ctx = new Entities())
            {
                ultraprintorderdataList = (from q in ctx.ultraprintorderdatas select q)
                                        .ToList();

            }
        }

        private ultraprintorderdata selectedorder;
        public ultraprintorderdata SelectedOrder
        {
            get { return selectedorder; }
            set
            {

                if (NeedsSave)
                {
                    DispatchSaveConfirmScreen();
                }
                else
                {
                    selectedorder = value;
                    OnPropertyChanged("SelectedOrder");
                }
            }
        }
       

        public void SaveChanges()
        {
            if (selectedorder != null)
            {
                using (Entities ctx = new Entities())
                {
                    ultraprintorderdata toEdit = (from k in ctx.ultraprintorderdatas where k.receiptnum == selectedorder.receiptnum select k).FirstOrDefault();
                    toEdit.Location = selectedorder.Location;
                    toEdit.Qty = selectedorder.Qty;
                    toEdit.Type = selectedorder.Type;
                    toEdit.DesignName = selectedorder.DesignName;
                    toEdit.size = selectedorder.size;
                    toEdit.Used = selectedorder.Used;
                    toEdit.Bin = selectedorder.Bin;
                    toEdit.receiptnum = selectedorder.receiptnum;
                    toEdit.createddate = selectedorder.createddate;
                    toEdit.expiredate = selectedorder.expiredate;
                    toEdit.completeddate = selectedorder.completeddate;
                    toEdit.Vendor = selectedorder.Vendor;
                    
                    ctx.SaveChanges();
                }
            }
        }

        public async void DispatchSaveConfirmScreen()
        {
            MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Changes", "Are you sure you would like to submit these changes to the order?");
            if (result == MessageDialogResult.Affirmative)
            {
                SaveChanges();
                NeedsSave = false;

                Views.Hopper mia = (Views.Hopper)(Application.Current.MainWindow as MainWindow).navFrame.Content;
                mia.grid.RefreshData();
            }
            else
            {
                NeedsSave = false;
            }

        }

        private string tagsasstring;
        public string TagsAsString
        {
            get { return tagsasstring; }
            set { tagsasstring = value; OnPropertyChanged("TagsAsString"); }
        }


        private bool needssave;
        public bool NeedsSave
        {
            get { return needssave; }
            set { needssave = value; OnPropertyChanged("NeedsSave"); }
        }
        public bool SaveCompleted;

        private ObservableCollection<Tag> tagcollection;
        public ObservableCollection<Tag> TagCollection
        {
            get { return tagcollection; }
            set { tagcollection = value; OnPropertyChanged("TagCollection"); }
        }
    }
}
