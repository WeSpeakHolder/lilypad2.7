using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;


namespace Lilypad.ViewModel
{
    public class DesignFileViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public DesignFileViewModel()
        {
            
        }

        public DesignFileViewModel(string path)
        {
           if (File.Exists(path))
            {
                FileInfo newInfo = new FileInfo(path);
                ThisFileInfo = newInfo;
                filepath = newInfo.FullName;
                filename = newInfo.Name;
                filesize = BytesToString(newInfo.Length);

               if (newInfo.Name.Contains('.'))
               {
                   filetype = newInfo.Name.Split('.')[1].ToLower();
                   filename = newInfo.Name.Split('.')[0].ToString();
               }
               else
               {
                   filetype = "file";
               }

                
            }
        }

        static string BytesToString(long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 0);
            return (Math.Sign(byteCount) * num).ToString() + " " + suf[place];
        }

        FileInfo fileinfo;
        FileInfo ThisFileInfo
        {
            get { return fileinfo; }
            set { fileinfo = value; OnPropertyChanged("ThisFileInfo"); }
        }

        string filepath;
        public string FilePath
        {
            get { return filepath; }
            set { filepath = value; OnPropertyChanged("FilePath"); }
        }

        string filename;
        public string FileName
        {
            get { return filename; }
            set { filename = value; OnPropertyChanged("FileName"); }
        }

        string filesize;
        public string FileSize
        {
            get { return filesize; }
            set { filesize = value; OnPropertyChanged("FileSize"); }
        }

        string filetype;
        public string FileType
        {
            get { return filetype; }
            set { filetype = value; OnPropertyChanged("FileType"); }
        }
    }
}   
