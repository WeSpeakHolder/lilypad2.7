using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.ViewModel;


namespace Lilypad.ViewModel
{
    public class EmployeesViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public EmployeesViewModel()
        {

        }

        public IList<string> Themes
        {
            get { return Utilities.GetThemes(); }
        }

        private IList<frogger> froggerlist;
        public IList<frogger> FroggerList
        {
            get { return froggerlist; }
            set { froggerlist = value; OnPropertyChanged("FroggerList"); }
        }

        private frogger selectedfrogger;
        public frogger SelectedFrogger
        {
            get { return selectedfrogger; }
            set { selectedfrogger = value; OnPropertyChanged("SelectedFrogger"); }
        }

        public bool CanEdit
        {
            get
            {
                bool perm = false;
                MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
                if (mvm.LoggedInFrogger.IsManager) perm = true;
                if (mvm.LoggedInFrogger.IsAdministrator) perm = true;
                if (mvm.LoggedInFrogger.IsStoreOwner) perm = true;
                return perm;
            }
        }

    }
}