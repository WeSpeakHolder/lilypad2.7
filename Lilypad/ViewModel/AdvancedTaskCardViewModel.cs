using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using DevExpress.Xpf.Mvvm;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.Controls.Tags;
using Lilypad.Properties;

namespace Lilypad.ViewModel
{
    public class AdvancedTaskCardViewModel : INotifyPropertyChanged
    {

        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        task selectedTask;
        public task SelectedTask
        {
            get { return selectedTask; }
            set { selectedTask = value; OnPropertyChanged("SelectedTask"); }
        }

        bool hasorder;
        public bool HasOrder
        {
            get { return hasorder; }
            set { hasorder = value; OnPropertyChanged("HasOrder"); }
        }

        bool hasfrogger;
        public bool HasAssignedFrogger
        {
            get { return hasfrogger; }
            set { hasfrogger = value; OnPropertyChanged("HasAssignedFrogger"); }
        }

        bool hasticket;
        public bool HasTicket
        {
            get { return hasticket; }
            set { hasticket = value; OnPropertyChanged("HasTicket"); }
        }

        bool hasfiles;
        public bool HasFiles
        {
            get { return hasfiles; }
            set { hasfiles = value; OnPropertyChanged("HasFiles"); }
        }

        int selectedattachedorder;
        public int SelectedAttachedOrder
        {
            get { return selectedattachedorder; }
            set { selectedattachedorder = value; OnPropertyChanged("SelectedAttachedOrder"); }
        }

        int selectedaassignedfrogger;
        public int SelectedAssignedFrogger
        {
            get { return selectedaassignedfrogger; }
            set { selectedaassignedfrogger = value; OnPropertyChanged("SelectedAssignedFrogger"); }
        }

        string workingtempfolder;
        public string WorkingTempFolder
        {
            get { return workingtempfolder; }
            set { workingtempfolder = value; OnPropertyChanged("WorkingTempFolder"); }
        }

        public void SaveChanges()
        {
            using (Entities ctx = new Entities())
            {
                var thisTask = (from q in ctx.tasks where q.UniqueID == SelectedTask.UniqueID select q).FirstOrDefault();
                thisTask.AssignedDate = SelectedTask.AssignedDate;
                thisTask.BackgroundColor = SelectedTask.BackgroundColor;
                thisTask.FrogAssigned = SelectedTask.FrogAssigned;
                thisTask.FrogCreated = SelectedTask.FrogCreated;
                thisTask.IsCompleted = SelectedTask.IsCompleted;
                thisTask.IsGlobal = SelectedTask.IsGlobal;
                thisTask.IsWorking = SelectedTask.IsWorking;
                thisTask.Related_Order = SelectedTask.Related_Order;
                thisTask.Related_Ticket = SelectedTask.Related_Ticket;
                thisTask.TaskTitle = SelectedTask.TaskTitle;
                thisTask.TaskCompletedDate = SelectedTask.TaskCompletedDate;
                thisTask.TaskDate = SelectedTask.TaskDate;
                thisTask.TaskDescrip = SelectedTask.TaskDescrip;
                thisTask.TaskPriority = SelectedTask.TaskPriority;
                ctx.SaveChanges();
            }
        }

        public void CreateNewTask()
        {
            var parentVM = (MainViewModel)Application.Current.MainWindow.DataContext;
            using (Entities ctx = new Entities())
            {
                task nT = new task();
                nT.FrogCreated = parentVM.LoggedInFrogger.FrogID;
                nT.IsCompleted = false;
                nT.IsGlobal = SelectedTask.IsGlobal;
                nT.IsWorking = true;
                nT.Related_Order = SelectedTask.Related_Order;
                nT.Related_Ticket = SelectedTask.Related_Ticket;
                nT.TaskCompletedDate = DateTime.MinValue;
                nT.TaskDate = DateTime.Now;
                nT.TaskDescrip = SelectedTask.TaskDescrip;
                nT.TaskPriority = SelectedTask.TaskPriority;
                nT.TaskTitle = SelectedTask.TaskTitle;
                nT.FrogAssigned = SelectedTask.FrogAssigned;
                nT.BackgroundColor = SelectedTask.BackgroundColor;
                nT.AssignedDate = SelectedTask.AssignedDate;
                ctx.tasks.Add(nT);
                ctx.SaveChanges();
            }
        }

        public void DeleteItem()
        {
            using (Entities ctx = new Entities())
            {
                var thisTask = (from q in ctx.tasks where q.UniqueID == SelectedTask.UniqueID select q).FirstOrDefault();
                ctx.tasks.Remove(thisTask);
                ctx.SaveChanges();
            }
        }

        // Commands accessors

        private DelegateCommand<AdvancedTaskCardViewModel> removeattachedorderCommand;
        public DelegateCommand<AdvancedTaskCardViewModel> RemoveAttachedOrderCommand
        {
            get { return removeattachedorderCommand; }
        }

        private DelegateCommand<AdvancedTaskCardViewModel> viewattachedorderCommand;
        public DelegateCommand<AdvancedTaskCardViewModel> ViewAttachedOrderCommand
        {
            get { return viewattachedorderCommand; }
        }

        private DelegateCommand<AdvancedTaskCardViewModel> addalphacommand;
        public DelegateCommand<AdvancedTaskCardViewModel> AddToAlphaCommand
        {
            get { return addalphacommand; }
        }
    }
}