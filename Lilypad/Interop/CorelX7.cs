﻿using System;
using System.Net;
using System.Net.Mail;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.IO.Compression;
using System.Text;
using System.Threading.Tasks;

namespace Lilypad.Interop
{
    public static class CorelX7
    {
        public static bool ExtractThumbnail(string path)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(path);
            var desFolder = fi.Directory.Parent;
            var desPath = System.IO.Path.Combine(desFolder.FullName.ToString(), "BMP");
            var fileName = fi.Name.Split('.')[0] + ".bmp";
            var filePath = System.IO.Path.Combine(desPath, fileName);

            if (!System.IO.Directory.Exists(desPath)) System.IO.Directory.CreateDirectory(desPath);

            string zipPath = path;
            string extractPath = desPath; 

            using (ZipArchive archive = ZipFile.OpenRead(zipPath))
            {
                archive.Entries.Where(x => x.FullName.Contains("thumbnail.bmp")).FirstOrDefault().ExtractToFile(filePath);

            }
            return true;
        }

    }
}
