﻿using System;
using System.Net;
using System.Net.Mail;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lilypad.Interop
{
    public class GmailHelper
    {
        public bool Send(string to, string subject, string body, Array attachments)
        {
            if (Properties.Settings.Default.System_EmailsEnabled)
            {
                string smtpAddr = "smtp.gmail.com";
                int portNum = 587;
                bool SSL = true;

                string login = Properties.Settings.Default.Interop_Email_Username;
                string emailfrom = Properties.Settings.Default.Interop_Email_From;
                string password = Properties.Settings.Default.Interop_Email_Password;
                string store = Properties.Settings.Default.Store_Name;
                string emailto = to;
                string subj = subject;
                string btext = body.Replace(Environment.NewLine, "<br /><br />");
                try
                {
                    using (MailMessage msg = new MailMessage())
                    {
                        msg.From = new MailAddress(emailfrom, store);
                        msg.To.Add(emailto);
                        msg.Subject = subject;
                        msg.Body = btext;
                        msg.IsBodyHtml = true;
                        if (attachments != null)
                        {
                            foreach (string attach in attachments)
                            {
                                msg.Attachments.Add(new Attachment(attach));
                            }
                        }

                        using (SmtpClient smtp = new SmtpClient(smtpAddr, portNum))
                        {
                            smtp.UseDefaultCredentials = false;
                            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                            smtp.Credentials = new NetworkCredential(login, password);
                            smtp.EnableSsl = SSL;
                            smtp.Send(msg);
                        }
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    new Error(ex);
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
    }
}
