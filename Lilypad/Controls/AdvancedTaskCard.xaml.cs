﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Lilypad.Model;
using Lilypad.ViewModel;
using Lilypad.Controls.DesignCards;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.LayoutControl;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.WindowsUI;



namespace Lilypad.Controls
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class AdvancedTaskCard : UserControl
    {

        #region DependencyProperty Members

        public static DependencyProperty TaskUniqueIDProperty = DependencyProperty.Register("LUniqueID", typeof(int), typeof(AdvancedTaskCard));
        public int LUniqueID
        {
            get { return (int)GetValue(TaskUniqueIDProperty); }
            set { SetValue(TaskUniqueIDProperty, value); }
        }

        public static DependencyProperty TaskTitleProperty = DependencyProperty.Register("LTaskTitle", typeof(string), typeof(AdvancedTaskCard));
        public string LTaskTitle
        {
            get { return (string)GetValue(TaskTitleProperty); }
            set { SetValue(TaskTitleProperty, value); }
        }

        public static DependencyProperty TaskDescriptionProperty = DependencyProperty.Register("LTaskDescription", typeof(string), typeof(AdvancedTaskCard));
        public string LTaskDescription
        {
            get { return (string)GetValue(TaskDescriptionProperty); }
            set { SetValue(TaskDescriptionProperty, value); }
        }

        public static DependencyProperty BackgroundColorProperty = DependencyProperty.Register("LBackgroundColor", typeof(string), typeof(AdvancedTaskCard));
        public string LBackgroundColor
        {
            get { return (string)GetValue(BackgroundColorProperty); }
            set { SetValue(BackgroundColorProperty, value); }
        }

        public static DependencyProperty TaskPriorityProperty = DependencyProperty.Register("LTaskPriority", typeof(int), typeof(AdvancedTaskCard));
        public int LTaskPriority
        {
            get { return (int)GetValue(TaskPriorityProperty); }
            set { SetValue(TaskPriorityProperty, value); }
        }

        public static DependencyProperty TaskRelatedOrderProperty = DependencyProperty.Register("LTaskRelatedOrder", typeof(int), typeof(AdvancedTaskCard));
        public int LTaskRelatedOrder
        {
            get { return (int)GetValue(TaskRelatedOrderProperty); }
            set { SetValue(TaskRelatedOrderProperty, value); }
        }

        public static DependencyProperty TaskFrogCreatedProperty = DependencyProperty.Register("LFrogCreated", typeof(int), typeof(AdvancedTaskCard));
        public int LFrogCreated
        {
            get { return (int)GetValue(TaskFrogCreatedProperty); }
            set { SetValue(TaskFrogCreatedProperty, value); }
        }

        public static DependencyProperty TaskFrogAssignedProperty = DependencyProperty.Register("LFrogAssigned", typeof(int), typeof(AdvancedTaskCard));
        public int LFrogAssigned
        {
            get { return (int)GetValue(TaskFrogAssignedProperty); }
            set { SetValue(TaskFrogAssignedProperty, value); }
        }

        public static DependencyProperty TaskFrogAssignedDateProperty = DependencyProperty.Register("LFrogAssignedDateTime", typeof(DateTime), typeof(AdvancedTaskCard));
        public DateTime LFrogAssignedDateTime
        {
            get { return (DateTime)GetValue(TaskFrogAssignedDateProperty); }
            set { SetValue(TaskFrogAssignedDateProperty, value); }
        }

        public static DependencyProperty TaskRelatedTicketProperty = DependencyProperty.Register("LTaskRelatedTicket", typeof(string), typeof(AdvancedTaskCard));
        public string LTaskRelatedTicket
        {
            get { return (string)GetValue(TaskRelatedTicketProperty); }
            set { SetValue(TaskRelatedTicketProperty, value); }
        }

        public static DependencyProperty TaskedDateProperty = DependencyProperty.Register("LTaskDate", typeof(DateTime), typeof(AdvancedTaskCard));
        public DateTime LTaskDate
        {
            get { return (DateTime)GetValue(TaskedDateProperty); }
            set { SetValue(TaskedDateProperty, value); }
        }

        public static DependencyProperty TaskFolderPathProperty = DependencyProperty.Register("LTaskFolderPath", typeof(string), typeof(AdvancedTaskCard));
        public string LTaskFolderPath
        {
            get { return (string)GetValue(TaskFolderPathProperty); }
            set { SetValue(TaskFolderPathProperty, value); }
        }

        public static DependencyProperty TaskIsWorkingProperty = DependencyProperty.Register("LTaskIsWorking", typeof(bool), typeof(AdvancedTaskCard));
        public bool LTaskIsWorking
        {
            get { return (bool)GetValue(TaskIsWorkingProperty); }
            set { SetValue(TaskIsWorkingProperty, value); }
        }

        public static DependencyProperty TaskIsCompletedProperty = DependencyProperty.Register("LTaskIsCompleted", typeof(bool), typeof(AdvancedTaskCard));
        public bool LTaskIsCompleted
        {
            get { return (bool)GetValue(TaskIsCompletedProperty); }
            set { SetValue(TaskIsCompletedProperty, value); }
        }

        public static DependencyProperty TaskIsGlobalProperty = DependencyProperty.Register("LTaskIsGlobal", typeof(bool), typeof(AdvancedTaskCard));
        public bool LTaskIsGlobal
        {
            get { return (bool)GetValue(TaskIsGlobalProperty); }
            set { SetValue(TaskIsGlobalProperty, value); }
        }

        public static DependencyProperty HasRelatedOrderProperty = DependencyProperty.Register("LHasRelatedOrder", typeof(bool), typeof(AdvancedTaskCard));
        public bool LHasRelatedOrder
        {
            get { return (bool)GetValue(HasRelatedOrderProperty); }
            set { SetValue(HasRelatedOrderProperty, value); }
        }

        public static DependencyProperty HasFilesProperty = DependencyProperty.Register("LHasFiles", typeof(bool), typeof(AdvancedTaskCard));
        public bool LHasFiles
        {
            get { return (bool)GetValue(HasFilesProperty); }
            set { SetValue(HasFilesProperty, value); }
        }

        public static DependencyProperty HasAssignedFroggerProperty = DependencyProperty.Register("LHasAssignedFrogger", typeof(bool), typeof(AdvancedTaskCard));
        public bool LHasAssignedFrogger
        {
            get { return (bool)GetValue(HasAssignedFroggerProperty); }
            set { SetValue(HasAssignedFroggerProperty, value); }
        }

        #endregion

        public AdvancedTaskCard()
        {
            InitializeComponent();
            HasAttached = false;
            if (string.IsNullOrEmpty(LTaskTitle)) { LTaskTitle = "Title 2"; }
            if (string.IsNullOrEmpty(LTaskDescription)) { LTaskDescription = "Descriptions"; }
            if (string.IsNullOrEmpty(LBackgroundColor)) { LBackgroundColor = "DodgerBlue"; }
            if (LTaskPriority == 0) { LTaskPriority = 3; }
            PriorityEditor.EditValueChanged -= PriorityChanged;
            DescriptionEditor.TextChanged -= TaskDescriptionChanged;
            TitleTextBox.TextChanged -= TitleChanged;
            InitialLoad = true;
            NeedsSave = false;
        }

        public AdvancedTaskCard(task inputtask)
        {
            InitializeComponent();
        }


        private void CheckClicked(object sender, MouseButtonEventArgs e)
        {
            if (LTaskIsCompleted == false)
            {
                CheckIcon.Source = new BitmapImage(new Uri("/Images/CheckSmallRed.png", UriKind.Relative));
                TaskCardActual.Opacity = 0.5;
                LTaskIsCompleted = true;
                NeedsSave = true;
            }
            else
            {
                SaveAllChanges();
                TaskCardActual.MouseLeave -= CardMouseLeft;
                (this.Parent as FlowLayoutControl).Children.Remove(this);
                using (Entities ctx = new Entities())
                {
                    task thisTask = (from a in ctx.tasks where a.UniqueID == LUniqueID select a).FirstOrDefault();
                    ctx.tasks.Remove(thisTask);
                    ctx.SaveChanges();
                }
            }
        }

        private void Unclicked(object sender, MouseButtonEventArgs e)
        {
            if (LTaskIsCompleted == true)
            {
                CheckIcon.Source = new BitmapImage(new Uri("/Images/CheckSmall.png", UriKind.Relative));
                TaskCardActual.Opacity = 1.0;
                LTaskIsCompleted = false;
                NeedsSave = true;
            }
        }

        private void IconSPMouseEnter(object sender, MouseEventArgs e)
        {
            StackPanel thisSP = (StackPanel)sender;
            StackPanel subSP = thisSP.Children.OfType<StackPanel>().FirstOrDefault();
            Image thisImg = subSP.Children.OfType<Image>().Where(f => f.Tag != null).FirstOrDefault();
            DoubleAnimation newanim = new DoubleAnimation(0.8, TimeSpan.FromSeconds(0.4));
            ExponentialEase expo = new ExponentialEase();
            expo.EasingMode = EasingMode.EaseOut;
            newanim.EasingFunction = expo;
            thisImg.BeginAnimation(OpacityProperty, newanim);
        }

        private void IconSPMouseLeave(object sender, MouseEventArgs e)
        {
            StackPanel thisSP = (StackPanel)sender;
            StackPanel subSP = thisSP.Children.OfType<StackPanel>().FirstOrDefault();
            Image thisImg = subSP.Children.OfType<Image>().Where(f => f.Tag != null).FirstOrDefault();
            DoubleAnimation newanim = new DoubleAnimation(0.3, TimeSpan.FromSeconds(0.4));
            ExponentialEase expo = new ExponentialEase();
            expo.EasingMode = EasingMode.EaseOut;
            newanim.EasingFunction = expo;
            thisImg.BeginAnimation(OpacityProperty, newanim);
        }

        private void AttachClicked(object sender, MouseButtonEventArgs e)
        {
            ControlsSP.Height = 0;
        }

        private void SetUpReceiptLookup()
        {
            using (Entities context = new Entities())
            {
                var found = (from q in context.hopperorders orderby q.customer.LastName ascending select new { Value = q.ReceiptNum, LastName = q.customer.LastName, FirstName = q.customer.FirstName, ReceiptNum = q.ReceiptNum }).ToList();
                var frogs = (from k in context.froggers orderby k.FrogID ascending select new { Value = k.FrogID, Name = k.FrogFirstName }).ToList();
                var cc = found.AsEnumerable().Select(x => new { Value = x.ReceiptNum, Name = x.LastName + " #" + x.ReceiptNum.ToString() + "" }).ToList();

                ReceiptLookup.ItemsSource = cc;
                ReceiptLookup.ValueMember = "Value";
                ReceiptLookup.DisplayMember = "Name";

                FrogLookup.ItemsSource = frogs;
                FrogLookup.ValueMember = "Value";
                FrogLookup.DisplayMember = "Name";
            }
        }

        private void OrderClicked(object sender, MouseButtonEventArgs e)
        {
            CloseAllEditors();
            DoubleAnimation newanim = new DoubleAnimation(0, TimeSpan.FromSeconds(0.4));
            ExponentialEase expo = new ExponentialEase();
            expo.EasingMode = EasingMode.EaseOut;
            newanim.EasingFunction = expo;
            TextBoxSp.BeginAnimation(HeightProperty, newanim);

            OrderEditorSP.Visibility = System.Windows.Visibility.Visible;
        }

        private void FroggerClicked(object sender, MouseButtonEventArgs e)
        {
            CloseAllEditors();

            DoubleAnimation newanim = new DoubleAnimation(0, TimeSpan.FromSeconds(0.4));
            ExponentialEase expo = new ExponentialEase();
            expo.EasingMode = EasingMode.EaseOut;
            newanim.EasingFunction = expo;
            TextBoxSp.BeginAnimation(HeightProperty, newanim);

            FroggerEditorSP.Visibility = System.Windows.Visibility.Visible;
        }

        private void TicketClicked(object sender, MouseButtonEventArgs e)
        {
            CloseAllEditors();
            DoubleAnimation newanim = new DoubleAnimation(0, TimeSpan.FromSeconds(0.4));
            ExponentialEase expo = new ExponentialEase();
            expo.EasingMode = EasingMode.EaseOut;
            newanim.EasingFunction = expo;
            TextBoxSp.BeginAnimation(HeightProperty, newanim);
        }

        private void FilesClicked(object sender, MouseButtonEventArgs e)
        {
            CloseAllEditors();

            DoubleAnimation newanim = new DoubleAnimation(0, TimeSpan.FromSeconds(0.4));
            ExponentialEase expo = new ExponentialEase();
            expo.EasingMode = EasingMode.EaseOut;
            newanim.EasingFunction = expo;
            TextBoxSp.BeginAnimation(HeightProperty, newanim);

            FilesEditorSP.Visibility = System.Windows.Visibility.Visible;
        }

        private void CloseAllEditors()
        {
            OrderEditorSP.Visibility = System.Windows.Visibility.Collapsed;
            FroggerEditorSP.Visibility = System.Windows.Visibility.Collapsed;
            FilesEditorSP.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void TaskCardLoaded(object sender, RoutedEventArgs e)
        {
            SetUpReceiptLookup();
            SetBarColor();
            LoadValuesAndFiles();
        }

        private void LoadValuesAndFiles()
        {
            PriorityEditor.SelectedIndex = LTaskPriority - 1;

            if (!string.IsNullOrEmpty(LTaskFolderPath))
            {
                bool exists = Directory.Exists(LTaskFolderPath);
                if (exists)
                {
                    HasAttached = true;
                    LoadFilesFromDirectory();
                }
            }
            int relatedReceipt = LTaskRelatedOrder;
            if (relatedReceipt > 0)
            {
                ReceiptLookup.EditValue = relatedReceipt;
                HasAttached = true;
            }
            int relatedFrogger = LFrogAssigned;
            if (relatedFrogger > 0)
            {
                FrogLookup.EditValue = relatedFrogger;
                HasAttached = true;
            }

            if (HasAttached)
            {
                AttachedLabel.Content = "VIEW ATTACHED";
                AddIcon.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void LoadFilesFromDirectory()
        {
            string[] files = Directory.GetFiles(LTaskFolderPath).Where(x => !x.EndsWith(".db") && !x.EndsWith(".tmp")).ToArray();
            AddFilesToContainer(files);
        }


        private void FilesSPDroppedFiles(object sender, DragEventArgs e)
        {
            var capturedFiles = ((DataObject)e.Data).GetFileDropList();
            string[] output = new string[capturedFiles.Count];
            capturedFiles.CopyTo(output, 0);

            if (string.IsNullOrEmpty(LTaskFolderPath))
            {
                LTaskFolderPath = Utilities.GetNewTemporaryDirectory();
                NeedsSave = true;
            }

            CopyFilesToContainer(output);
        }

        private void CopyFilesToContainer(string[] input)
        {
            foreach (string st in input)
            {
                string destname = new FileInfo(st).Name.ToString();
                string destpath = Path.Combine(LTaskFolderPath, destname);
                if (File.Exists(destpath))
                {
                        File.Copy(st, destpath, true);
                }
                else
                {
                    File.Copy(st, destpath, true);
                    var newVM = new DesignFileViewModel(destpath);
                    var newView = new DesignFile();
                    newView.DataContext = newVM;
                    FileContainer.Children.Add(newView);
                }
            }
            DragAreaSP.Background = Brushes.LightGray;
        }


        private void AddFilesToContainer(string[] input)
        {
            foreach (string st in input)
            {
                string destname = new FileInfo(st).Name.ToString();
                string destpath = Path.Combine(LTaskFolderPath, destname);

                var newVM = new DesignFileViewModel(destpath);
                var newView = new DesignFile();
                newView.DataContext = newVM;
                FileContainer.Children.Add(newView);
            }
            DragAreaSP.Background = Brushes.LightGray;
        }

        private void HoverDrop(object sender, DragEventArgs e)
        {
            DragAreaSP.Background = Brushes.YellowGreen;
        }

        private void HoverLeave(object sender, DragEventArgs e)
        {
            DragAreaSP.Background = Brushes.LightGray;
        }

        private void OpenFolderClicked(object sender, MouseButtonEventArgs e)
        {
            bool exists = Directory.Exists(LTaskFolderPath);
            if (exists)
            {
                Process.Start(LTaskFolderPath);
            }
        }

        private void CloseControlPanel(object sender, MouseButtonEventArgs e)
        {
            ControlsSP.Height = 30;
            DoubleAnimation newanim = new DoubleAnimation(235, TimeSpan.FromSeconds(0.4));
            ExponentialEase expo = new ExponentialEase();
            expo.EasingMode = EasingMode.EaseOut;
            newanim.EasingFunction = expo;
            TextBoxSp.BeginAnimation(HeightProperty, newanim);
        }

        private void SetBarColor()
        {
            string backg = "";
            if (LTaskPriority == 1) { backg = "YellowGreen"; }
            else if (LTaskPriority == 2) { backg = "Orange"; }
            else if (LTaskPriority == 3) { backg = "Red"; }
            else { backg = "DodgerBlue"; }
            LBackgroundColor = backg;

        }

        private bool initialload;
        public bool InitialLoad
        {
            get
            {
                return initialload;
            }
            set { initialload = value; }
        }

        private bool needssave;
        public bool NeedsSave
        {
            get { return needssave; }
            set { needssave = value; }
        }

        private bool hasattached;
        public bool HasAttached
        {
            get { return hasattached; }
            set { hasattached = value; }
        }

        private void PriorityChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (PriorityEditor.EditValue != null)
            {
                LTaskPriority = (int)PriorityEditor.EditValue;
                SetBarColor();
                PriorityEditor.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (!InitialLoad) { NeedsSave = true; }
        }

        private void ShowPriorityEditor(object sender, MouseButtonEventArgs e)
        {
            PriorityEditor.Visibility = System.Windows.Visibility.Visible;
        }

        private void CardMouseLeft(object sender, MouseEventArgs e)
        {
            if (NeedsSave)
            {
                // Save Operation
                SaveAllChanges();
                //     new Notification().Notify("Successfully saved all changes.");
                NeedsSave = false;
            }
        }

        private void CreateNewTask()
        {

        }

        private void SaveAllChanges()
        {
            int ID = LUniqueID;
            using (Entities ctx = new Entities())
            {
                task thisTask = (from q in ctx.tasks where q.UniqueID == ID select q).FirstOrDefault();
                if (thisTask.FrogAssigned > 0)
                {
                    thisTask.FrogAssigned = (int)FrogLookup.EditValue;
                    if (LFrogAssignedDateTime != DateTime.MinValue)
                    {
                        thisTask.AssignedDate = LFrogAssignedDateTime;
                    }
                    else
                    {
                        thisTask.AssignedDate = DateTime.Now;
                    }
                }
                thisTask.BackgroundColor = LBackgroundColor;
                thisTask.FolderPath = LTaskFolderPath;
                thisTask.IsCompleted = LTaskIsCompleted;
                thisTask.IsGlobal = LTaskIsGlobal;
                thisTask.IsWorking = LTaskIsWorking;
                if (ReceiptLookup.EditValue != null)
                {
                    thisTask.Related_Order = (int)ReceiptLookup.EditValue;
                }
                else
                {
                    thisTask.Related_Order = 0;
                }
                if (LTaskIsCompleted)
                {
                    thisTask.TaskCompletedDate = DateTime.Now;
                }

                thisTask.TaskDescrip = LTaskDescription;
                thisTask.TaskPriority = LTaskPriority;
                thisTask.TaskTitle = LTaskTitle;

                ctx.SaveChanges();
            }
        }

        private void TaskDescriptionChanged(object sender, TextChangedEventArgs e)
        {
            if (!InitialLoad) { NeedsSave = true; }
        }

        private void DescripBindingLoaded(object sender, RoutedEventArgs e)
        {
            InitialLoad = false;
            PriorityEditor.EditValueChanged += PriorityChanged;
            DescriptionEditor.TextChanged += TaskDescriptionChanged;
            TitleTextBox.TextChanged += TitleChanged;
        }

        private void TitleChanged(object sender, TextChangedEventArgs e)
        {
            if (!InitialLoad) { NeedsSave = true; }
        }

        private void ViewOrderClicked(object sender, EventArgs e)
        {
            Keyboard.Focus(AttachedLabel);
            if ((int)ReceiptLookup.EditValue > 0)
            {
                var frame = Utilities.FindParentByType<NavigationFrame>(this);
                frame.Navigate("HopperOrderDetails", LTaskRelatedOrder);
            }
        }

        private void RemoveOrderClicked(object sender, EventArgs e)
        {
            if (LTaskRelatedOrder > 0)
            {
                LTaskRelatedOrder = 0;
                NeedsSave = true;
            }
        }

        private void AssignFroggerClicked(object sender, EventArgs e)
        {
            int froggerToAssign = Convert.ToInt32(FrogLookup.EditValue);
            if (froggerToAssign > 0)
            {
                LFrogAssigned = froggerToAssign;
                NeedsSave = true;
            }
        }

        private void UnassignFroggerClicked(object sender, EventArgs e)
        {
            if (LFrogAssigned > 0)
            {
                LFrogAssigned = 0;
                NeedsSave = true;
            }
        }

        private void AssignGloballyClicked(object sender, EventArgs e)
        {
            LTaskIsGlobal = true;
            NeedsSave = true;
        }

        private void UnassignGloballyClicked(object sender, EventArgs e)
        {
            LTaskIsGlobal = false;
            NeedsSave = true;
        }

        private void ReceiptLookupChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (!InitialLoad) { NeedsSave = true; }
        }

        private void AssignedFroggerChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (!InitialLoad) { NeedsSave = true; }
        }
    }
}
