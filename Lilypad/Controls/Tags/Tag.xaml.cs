﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;
using Lilypad.ViewModel;

namespace Lilypad.Controls.Tags
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public class Metatag
    {
        private string tagtext;
        public string TagText
        {
            get { return tagtext; }
            set { tagtext = value; }
        }
    }

    public partial class Tag : UserControl, INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public Tag(string tag)
        {
            InitializeComponent();
            this.DataContext = this;
            publictag = new Metatag();
            publictag.TagText = tag;
            candelete = true;
        }

        public Tag(string tag, bool canDelete)
        {
            InitializeComponent();
            this.DataContext = this;
            publictag = new Metatag();
            publictag.TagText = tag;
            candelete = canDelete;
        }

        private Metatag publictag;
        public Metatag PublicTag
        {
            get { return publictag; }
            set { publictag = value; OnPropertyChanged("PublicTag"); }
        }

        private bool candelete;
        public bool CanDelete
        {
            get { return candelete; }
            set { candelete = value; OnPropertyChanged("CanDelete"); }
        }

        private void RegHover(object sender, MouseEventArgs e)
        {
            if (CanDelete)
            {
                Reg.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void DeleteLeave(object sender, MouseEventArgs e)
        {
            if (CanDelete)
            {
                Reg.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void DeleteClick(object sender, MouseButtonEventArgs e)
        {
            var no = Utilities.FindParentByType<Views.NewOrder>(this);
            OrderNewViewModel vm = (OrderNewViewModel)no.DataContext;
            vm.TagCollection.Remove(this);
            no.VerifyOrderDetails();
        }

    }
}
