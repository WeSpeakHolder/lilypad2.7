﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.Controls.DesignCards;
using Lilypad.ViewModel;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class BoolCheck : UserControl
    {
        #region DependencyProperty Members

        public static DependencyProperty CheckShownProperty = DependencyProperty.Register("CheckShown", typeof(bool), typeof(BoolCheck));
        public bool CheckShown
        {
            get { return (bool)GetValue(CheckShownProperty); }
            set { SetValue(CheckShownProperty, value); }
        }

        public static DependencyProperty CanEditProperty = DependencyProperty.Register("CanEdit", typeof(bool), typeof(BoolCheck));
        public bool CanEdit
        {
            get { return (bool)GetValue(CanEditProperty); }
            set { SetValue(CanEditProperty, value); }
        }

        public static DependencyProperty SizeProperty = DependencyProperty.Register("IconSize", typeof(double), typeof(BoolCheck));
        public double IconSize
        {
            get { return (double)GetValue(SizeProperty); }
            set { SetValue(SizeProperty, value); }
        }

        #endregion

        public BoolCheck()
        {
            InitializeComponent();
        }

        private void ItemEdited(object sender, TextChangedEventArgs e)
        {

        }

        private void SetupArtClicked(object sender, EventArgs e)
        {
            var vm = (DesignSetupCardViewModel)this.DataContext;
            Utilities.ShowArtSetup(vm);
        }

        private void ControlClicked(object sender, MouseButtonEventArgs e)
        {
            if (this.CanEdit)
            {
                if (this.CheckShown) this.CheckShown = false;
                else if (!this.CheckShown) this.CheckShown = true;
            }
        }
    }
}
