﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Lilypad.Model;
using Lilypad.View;
using Lilypad.ViewModel;
using MahApps.Metro.Controls.Dialogs;
using Path = System.IO.Path;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for LineCard.xaml
    /// </summary>
    public partial class MetroDesignCard : UserControl, ICloneable
    {
        public MetroDesignCard()
        {
            InitializeComponent();
            Foreground = Brushes.Black;
            fullyLoaded = false;
            ExistingArtworkItems = new ObservableCollection<MetroDesignFile>();
            ExistingArtworkItems.CollectionChanged += ExistingArtworkItems_CollectionChanged;
            CurrentArtworkItems = new ObservableCollection<string>();
            CurrentArtworkItems.CollectionChanged += CurrentArtworkItems_CollectionChanged;
            ArtworkItemsToAdd = new ObservableCollection<string>();
            ArtworkItemsToAdd.CollectionChanged += ArtworkItemsToAdd_CollectionChanged;
            ArtworkItemsToRemove = new ObservableCollection<string>();
            ArtworkItemsToRemove.CollectionChanged += ArtworkItemsToRemove_CollectionChanged;
            LineItems = new ObservableCollection<LineViewModel>();
          //  LineItems.CollectionChanged += LineItems_CollectionChanged;
            // GetArtwork
        }

        public MetroDesignCard(designdetail input)
        {
            InitializeComponent();
            Foreground = Brushes.Black;
            fullyLoaded = false;
            ExistingArtworkItems = new ObservableCollection<MetroDesignFile>();
            ExistingArtworkItems.CollectionChanged += ExistingArtworkItems_CollectionChanged;
            CurrentArtworkItems = new ObservableCollection<string>();
            CurrentArtworkItems.CollectionChanged += CurrentArtworkItems_CollectionChanged;
            ArtworkItemsToAdd = new ObservableCollection<string>();
            ArtworkItemsToAdd.CollectionChanged += ArtworkItemsToAdd_CollectionChanged;
            ArtworkItemsToRemove = new ObservableCollection<string>();
            ArtworkItemsToRemove.CollectionChanged += ArtworkItemsToRemove_CollectionChanged;
            LineItems = new ObservableCollection<LineViewModel>();
           
            LoadedDD = input;
            // SetDesignDetail(input);
            // GetArtwork
        }

        void LineItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (fullyLoaded)
            {
                using (Entities ctx = new Entities())
                {
                    foreach (LineViewModel line in LineItems)
                    {
                        var pp = ctx.preorderitems.FirstOrDefault(x => x.UniqueID == line.SelectedLineItem.UniqueID);
                        if (pp != null)
                        {
                            pp.DesignID = line.SelectedLineItem.DesignID;
                            pp.EstimatedCost = line.LinePrice;
                            pp.PreorderID = line.SelectedLineItem.ReceiptNum;
                            pp.StockCode = line.SelectedLineItem.StockIDPOS;
                            pp.StockColor = line.SelectedLineItem.Color;
                            pp.StockDescrip = line.SelectedLineItem.Description;
                            pp.StockInvStatus = "";
                            pp.StockInvStatusID = 0;
                            pp.StockItemCost = line.LineCost;
                            pp.StockMfg = "";
                            pp.StockQty = line.SelectedLineItem.Quantity;
                            pp.StockSize = line.SelectedLineItem.Size;
                            ctx.SaveChanges();
                        }
                        else
                        {
                            pp.DesignID = line.SelectedLineItem.DesignID;
                            pp.EstimatedCost = line.LinePrice;
                            pp.PreorderID = line.SelectedLineItem.ReceiptNum;
                            pp.StockCode = line.SelectedLineItem.StockIDPOS;
                            pp.StockColor = line.SelectedLineItem.Color;
                            pp.StockDescrip = line.SelectedLineItem.Description;
                            pp.StockInvStatus = "";
                            pp.StockInvStatusID = 0;
                            pp.StockItemCost = line.LineCost;
                            pp.StockMfg = "";
                            pp.StockQty = line.SelectedLineItem.Quantity;
                            pp.StockSize = line.SelectedLineItem.Size;
                            ctx.preorderitems.Add(pp);
                            ctx.SaveChanges();
                        }
                    }
                }
            }
        }

        public preorderdesigndetail LoadedPreorderDD { get; set; }
        public designdetail LoadedDD { get; set; }

        public MetroDesignCard(preorderdesigndetail input)
        {
            InitializeComponent();
            Foreground = Brushes.Black;
            fullyLoaded = false;
            IsPreorder = true;
            ExistingArtworkItems = new ObservableCollection<MetroDesignFile>();
            ExistingArtworkItems.CollectionChanged += ExistingArtworkItems_CollectionChanged;
            CurrentArtworkItems = new ObservableCollection<string>();
            CurrentArtworkItems.CollectionChanged += CurrentArtworkItems_CollectionChanged;
            ArtworkItemsToAdd = new ObservableCollection<string>();
            ArtworkItemsToAdd.CollectionChanged += ArtworkItemsToAdd_CollectionChanged;
            ArtworkItemsToRemove = new ObservableCollection<string>();
            ArtworkItemsToRemove.CollectionChanged += ArtworkItemsToRemove_CollectionChanged;
            LineItems = new ObservableCollection<LineViewModel>();
            LoadedPreorderDD = input;
            SetDesignDetail(input);
            // GetArtwork
        }

        private void SetDesignDetail(preorderdesigndetail input)
        {

            SelectedDesignDetail = new designdetail();
            SelectedDesignDetail.DesignDescription = input.DesignDescription;
            SelectedDesignDetail.DesignID = input.DesignID;
            SelectedDesignDetail.DesignNum = input.DesignNum;
            SelectedDesignDetail.Notes = input.Notes;
            SelectedDesignDetail.PrintProcess = input.PrintProcess;
            SelectedDesignDetail.PrintToFile = input.PrintToFile;
            SelectedDesignDetail.ReceiptNum = input.PreorderID;
            SelectedDesignDetail.SizeToGarment = input.SizeToGarment;
            SelectedDesignDetail.DesignFolderPath = input.DesignFolderPath;
            string n = "Design " + input.DesignNum.ToString();
            HorizontalTooltip.Text = string.IsNullOrEmpty(SelectedDesignDetail.DesignDescription) ? n : SelectedDesignDetail.DesignDescription;
            if (SelectedDesignDetail.DesignID > 0)
            {
                using (Entities ctx = new Entities())
                {
                    preorderdesigndetail d = ctx.preorderdesigndetails.FirstOrDefault(x => x.DesignID == SelectedDesignDetail.DesignID);
                    if (d != null)
                    {
                        d.DesignDescription = SelectedDesignDetail.DesignDescription;
                        d.DesignFolderPath = SelectedDesignDetail.DesignFolderPath;
                        d.DesignID = SelectedDesignDetail.DesignID;
                        d.DesignNum = SelectedDesignDetail.DesignNum;
                        d.Notes = string.Empty;
                        d.PrintProcess = SelectedDesignDetail.PrintProcess;
                        d.PrintToFile = SelectedDesignDetail.PrintToFile;
                        d.PreorderID = SelectedDesignDetail.ReceiptNum;
                        d.SizeToGarment = SelectedDesignDetail.SizeToGarment;
                        ctx.SaveChanges();
                    }
                }
            }
            OnPropertyChanged("SelectedDesignDetail");
            ManuallySetLabels();
        }
        private void SetDesignDetail(designdetail input)
        {
            SelectedDesignDetail = input;
            SelectedDesignDetail.DesignFolderPath = input.DesignFolderPath;
            string n = "Design " + input.DesignNum.ToString();
            HorizontalTooltip.Text = string.IsNullOrEmpty(SelectedDesignDetail.DesignDescription) ? n : SelectedDesignDetail.DesignDescription;

            if (IsPreorder)
            {
                if (SelectedDesignDetail.DesignID > 0)
                {
                    using (Entities ctx = new Entities())
                    {
                        preorderdesigndetail d =
                            ctx.preorderdesigndetails.FirstOrDefault(x => x.DesignID == SelectedDesignDetail.DesignID);
                        if (d != null)
                        {
                            d.DesignDescription = SelectedDesignDetail.DesignDescription;
                            d.DesignFolderPath = SelectedDesignDetail.DesignFolderPath;
                            d.DesignID = SelectedDesignDetail.DesignID;
                            d.DesignNum = SelectedDesignDetail.DesignNum;
                            d.Notes = string.Empty;
                            d.PrintProcess = SelectedDesignDetail.PrintProcess;
                            d.PrintToFile = SelectedDesignDetail.PrintToFile;
                            d.PreorderID = SelectedDesignDetail.ReceiptNum;
                            d.SizeToGarment = SelectedDesignDetail.SizeToGarment;
                            ctx.SaveChanges();
                        }
                    }
                }

            }
            else
            {

                if (SelectedDesignDetail.DesignID > 0)
                {
                    using (Entities ctx = new Entities())
                    {
                        designdetail d =
                            ctx.designdetails.FirstOrDefault(x => x.DesignID == SelectedDesignDetail.DesignID);
                        if (d != null)
                        {
                            d.DesignDescription = SelectedDesignDetail.DesignDescription;
                            d.DesignFolderPath = SelectedDesignDetail.DesignFolderPath;
                            d.DesignID = SelectedDesignDetail.DesignID;
                            d.DesignNum = SelectedDesignDetail.DesignNum;
                            d.Notes = string.Empty;
                            d.PrintProcess = SelectedDesignDetail.PrintProcess;
                            d.PrintToFile = SelectedDesignDetail.PrintToFile;
                            d.ReceiptNum = SelectedDesignDetail.ReceiptNum;
                            d.SizeToGarment = SelectedDesignDetail.SizeToGarment;
                            ctx.SaveChanges();
                        }
                    }
                }
            }
            OnPropertyChanged("SelectedDesignDetail");
            ManuallySetLabels();
        }

        private bool IsPreorder { get; set; }
        public object Clone()
        {
            return MemberwiseClone();
        }

        private void ArtworkItemsToRemove_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        private void ArtworkItemsToAdd_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        private void CurrentArtworkItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        private void ExistingArtworkItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        private void SetupArtClicked(object sender, EventArgs e)
        {
            var vm = (DesignSetupCardViewModel)DataContext;
            //   Utilities.ShowArtSetup(vm);
        }

        private void DeleteClick(object sender, MouseButtonEventArgs e)
        {
            var elem = Utilities.FindParentByType<ListViewItem>((DependencyObject)sender);
            var vm = (LineViewModel)elem.Content;
            var view = Utilities.FindParentByType<ListView>(elem);
            var list = (IList<LineViewModel>)view.ItemsSource;
            list.Remove(vm);
        }

        private void DeleteCardClick(object sender, MouseButtonEventArgs e)
        {
        }

        private void DesignCardActual_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        private bool fullyLoaded { get; set; }

        private async void LoadInitial()
        {
            await Task.Delay(10);

            LoadLineItems();

            if (SelectedDesignDetail != null)
            {
                if (!string.IsNullOrEmpty(SelectedDesignDetail.DesignFolderPath))
                {
                    LoadArtworkFiles();
                }
            }
            if (IsHorizontal)
            {
                if (IsExpanded) { DesignBodyGrid.Width = 295; ChevronRect2.RotateToFrom(TimeSpan.FromMilliseconds(200), 0, 180); }
                else DesignBodyGrid.Width = 0;
            }
            else
            {
                if (IsExpanded)
                {

                    MainCard.Height = 320;
                }
                else
                {
                    MainCard.Height = 40;
                }
            }
        }
        private void DesignCardLoaded(object sender, RoutedEventArgs e)
        {
            fullyLoaded = true;
            LoadInitial();
        }

        private void LoadLineItems()
        {
            try
            {
                using (Entities ctx = new Entities())
                {
                    LineItems = new ObservableCollection<LineViewModel>();
                    if (!IsPreorder)
                    {
                        var items = ctx.designdetailslineitems.Where(x => x.DesignID == SelectedDesignDetail.DesignID).ToList();
                        foreach (designdetailslineitem item in items)
                        {
                            LineViewModel lvm = new LineViewModel();
                            lvm.SelectedLineItem = item;
                            lvm.DesignNum = SelectedDesignDetail.DesignID;

                            LineItems.Add(lvm);
                            OnPropertyChanged("LineItems");
                        }
                    }
                    else
                    {
                        var items = ctx.preorderitems.Where(x => x.DesignID == SelectedDesignDetail.DesignID).ToList();
                        foreach (preorderitem item in items)
                        {
                            LineViewModel lvm = new LineViewModel();
                            designdetailslineitem dl = new designdetailslineitem();
                            dl.Color = item.StockColor;
                            dl.Description = item.StockDescrip;
                            dl.DesignID = item.DesignID.Value;
                            dl.Quantity = item.StockQty.Value;
                            dl.ReceiptNum = item.PreorderID;
                            dl.Size = item.StockSize;
                            dl.StockIDPOS = item.StockCode;
                            dl.UniqueID = item.UniqueID;
                            lvm.SelectedLineItem = dl;
                            LineItems.Add(lvm);
                            OnPropertyChanged("LineItems");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private void LoadArtworkFiles()
        {
            if (ExistingArtworkItems.Any()) ExistingArtworkItems.Clear();

            if (!string.IsNullOrEmpty(SelectedDesignDetail.DesignFolderPath))
            {
                DesignFailureLabel.OpacityAnimation(0, TimeSpan.FromSeconds(1));
                string des = SelectedDesignDetail.DesignFolderPath;
                try
                {
                    if (Directory.Exists(des))
                    {
                        string[] d = Directory.GetDirectories(des);
                        foreach (string dir in d)
                        {
                            string[] files = Directory.GetFiles(dir, "*.cdr");
                            foreach (string file in files)
                            {
                                FileInfo fi = new FileInfo(file);
                                MetroDesignFile mdf = new MetroDesignFile();
                                mdf.FileExtension = "CDR";
                                mdf.FileIsNew = false;
                                mdf.FileTitle = fi.Name.Split('.')[0];
                                mdf.FilePath = fi.FullName;
                                ExistingArtworkItems.Add(mdf);
                                OnPropertyChanged("ExistingArtworkItems");
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    DesignFailureLabel.OpacityAnimation(1, TimeSpan.FromSeconds(1));
                }
            }
        }

        private void DeleteMouseOver(object sender, MouseEventArgs e)
        {
        }

        private void DeleteMouseout(object sender, MouseEventArgs e)
        {
        }

        private void SaveChangesClicked(object sender, EventArgs e)
        {
            SaveChanges();
        }

        private void SaveChanges()
        {
            var vm = (DesignSetupCardViewModel)DesignCardActual.DataContext;
            if (vm.NeedsSave)
            {
                vm.SaveChanges();
                vm.NeedsSave = false;
            }
        }

        private void FileContainerLoaded(object sender, RoutedEventArgs e)
        {
        }

        private void FolderSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void TopMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                ExpandCollapse();
            }
        }

        public void Expand()
        {
            if (IsHorizontal)
            {

                ChevronRect.RotateToFrom(TimeSpan.FromMilliseconds(200), 0, 180);
                IsExpanded = true;
                if (Width > 100) MainCard.WidthAnimation(this.Width, TimeSpan.FromMilliseconds(400));
                else MainCard.WidthAnimation(295, TimeSpan.FromMilliseconds(400));
            }
            else
            {

                ChevronRect.RotateToFrom(TimeSpan.FromMilliseconds(200), 0, 180);
                IsExpanded = true;
                MainCard.HeightAnimation(320, TimeSpan.FromMilliseconds(400));
            }
        }

        public void Collapse()
        {
            if (IsHorizontal)
            {
                ChevronRect.RotateToFrom(TimeSpan.FromMilliseconds(200), 180, 0);
                IsExpanded = false;
                MainCard.WidthAnimation(0, TimeSpan.FromMilliseconds(400));
            }
            else
            {
                ChevronRect.RotateToFrom(TimeSpan.FromMilliseconds(200), 180, 0);
                IsExpanded = false;
                MainCard.HeightAnimation(40, TimeSpan.FromMilliseconds(400));
            }
        }

        public void AddToNewItemsList(string input)
        {
            CurrentArtworkItems.Add(input);
        }

        private void ExpandClicked(object sender, EventArgs e)
        {
            ExpandCollapse();
        }

        public void ExpandCollapse()
        {
            if (IsHorizontal)
            {
                if (IsExpanded)
                {
                    ChevronRect2.RotateToFrom(TimeSpan.FromMilliseconds(200), 180, 0);
                    IsExpanded = false;
                    DesignBodyGrid.WidthAnimation(0, TimeSpan.FromMilliseconds(400));
                }

                else
                {
                    ChevronRect2.RotateToFrom(TimeSpan.FromMilliseconds(200), 0, 180);
                    IsExpanded = true;
                    if (Width > 100) DesignBodyGrid.WidthAnimation(this.Width, TimeSpan.FromMilliseconds(400));
                    else DesignBodyGrid.WidthAnimation(295, TimeSpan.FromMilliseconds(400));
                }
            }
            else
            {
                if (IsExpanded)
                {
                    ChevronRect.RotateToFrom(TimeSpan.FromMilliseconds(200), 180, 0);
                    IsExpanded = false;
                    MainCard.HeightAnimation(40, TimeSpan.FromMilliseconds(400));
                }

                else
                {
                    ChevronRect.RotateToFrom(TimeSpan.FromMilliseconds(200), 0, 180);
                    IsExpanded = true;
                    MainCard.HeightAnimation(320, TimeSpan.FromMilliseconds(400));
                }
            }

        }

        private void ModifyClicked(object sender, EventArgs e)
        {
            var mc = new MetroDesignCardEditor(this);
            mc.Owner = Application.Current.MainWindow;
            mc.WindowStartupLocation = WindowStartupLocation.Manual;
            mc.ProcessesList = new ObservableCollection<printprocess>(((MainViewModel)Application.Current.MainWindow.DataContext).PrintProcessesList);
            mc.CurrentArtworkItems = new ObservableCollection<string>(CurrentArtworkItems);
            mc.ExistingArtworkItems = ExistingArtworkItems;
            var mousepos = Mouse.GetPosition(ModifyTile);
            var mouseposA = ModifyTile.PointToScreen(mousepos);

            if (IsHorizontal)
            {
                mc.Top = mouseposA.Y - (mc.Height);
                mc.Left = mouseposA.X;
            }
            else
            {
                mc.Top = mouseposA.Y - (mc.Height / 2);
                mc.Left = mouseposA.X - (mc.Width);
            }

            var showDialog = mc.ShowDialog();
            var saved = showDialog != null && (bool)showDialog;
            if (saved)
            {
                SetDesignDetail(mc.SelectedDesignDetail);
                if (mc.ArtworkItemsToAdd.Any())
                {
                    foreach (var i in mc.ArtworkItemsToAdd)
                    {
                        if (Directory.Exists(SelectedDesignDetail.DesignFolderPath))
                        {
                            if (!ExistingArtworkItems.Any(x => x.FileTitle == i))
                            {
                                string dir = SelectedDesignDetail.DesignFolderPath.ToString();
                                string file = i + ".cdr";
                                MetroDesignFile md = new MetroDesignFile();
                                md.FileTitle = i;
                                md.FileExtension = "CDR";
                                md.FilePath = Path.Combine(dir, file);
                                ExistingArtworkItems.Add(md);
                                OnPropertyChanged("ExistingArtworkItems");
                                MoveFile(i);
                            }
                        }
                        else
                        {
                            if (!CurrentArtworkItems.Any(x => x == i))
                            {
                                CurrentArtworkItems.Add(i);
                                OnPropertyChanged("CurrentArtworkItems");
                            }
                        }

                    }
                }
                if (mc.ArtworkItemsToRemove.Any())
                {
                    if (Directory.Exists(SelectedDesignDetail.DesignFolderPath))
                    {
                        foreach (var i in mc.ArtworkItemsToRemove)
                        {
                            MetroDesignFile f = ExistingArtworkItems.FirstOrDefault(x => x.FileTitle == i);
                            File.Delete(f.FilePath);
                            ExistingArtworkItems.Remove(f);
                            OnPropertyChanged("ExistingArtworkItems");
                        }
                    }
                    else
                    {
                        foreach (var i in mc.ArtworkItemsToRemove)
                        {
                            CurrentArtworkItems.Remove(i);
                            OnPropertyChanged("CurrentArtworkItems");
                        }
                    }

                }
                mc.Close();
                LoadArtworkFiles();
            }
        }

        private void MoveFile(string item)
        {
            try
            {

                string type = "";
                string destination = SelectedDesignDetail.DesignFolderPath + @"\CDR\";
                bool isYouth = SelectedDesignDetail.AP_YOUTH;

                if (item == "Apron" || item == "Bandana" || item == "CapFront" || item == "RoundCoasters" || item == "GolfFlag"
                        || item == "GolfTowel" || item == "Koozies" || item == "Mousepad" || item == "Puzzle" || item == "SquareCoasters" || item == "ToteBag" || item == "WineBag")
                {
                    type = "S";
                    CopyArtFile(item, type, destination);
                }
                else
                {
                    if (isYouth)
                    {
                        type = "Y";
                        CopyArtFile("Youth" + item, type, destination);
                    }
                    else if (!isYouth)
                    {
                        type = "A";
                        CopyArtFile(item, type, destination);
                    }
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private async void DeleteClicked(object sender, EventArgs e)
        {
            var res = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you want to delete this design? This cannot be undone!");
            if (res == MessageDialogResult.Affirmative)
            {
                var p = Utilities.FindParentByType<StackPanel>(this);
                if (p is StackPanel)
                {
                    var nn = Utilities.FindParentByType<Views.Hopper>(p);
                    if (nn != null)
                    {
                        var id = DesignCardActual.SelectedDesignDetail.DesignID;
                        var fold = DesignCardActual.SelectedDesignDetail.DesignFolderPath;
                        if (id > 0)
                        {
                            using (Entities ctx = new Entities())
                            {
                                List<designdetailslineitem> dli = ctx.designdetailslineitems.Where(x => x.DesignID == id).ToList();
                                if (dli.Count > 0)
                                {
                                    foreach (designdetailslineitem item in dli)
                                    {
                                        ctx.designdetailslineitems.Remove(item);
                                        ctx.SaveChanges();
                                    }
                                }
                            }
                            using (Entities ctx = new Entities())
                            {
                                designdetail dd = ctx.designdetails.Where(x => x.DesignID == id).FirstOrDefault();
                                if (dd != null)
                                {
                                    ctx.designdetails.Remove(dd);
                                    ctx.SaveChanges();
                                }
                            }
                        }
                        if (Directory.Exists(fold))
                        {
                            Utilities.DeleteDirectoryRecursive(fold);
                        }
                        var oc = Utilities.FindParentByType<MetroCardLarge>(this);
                      //  if (oc != null) nn.DesignsSP.Children.Remove(oc);
                     //   nn.DesignCount.Content = nn.DesignsSP.Children.OfType<MetroCardLarge>().Count();
                    }

                  

                    var n3 = Utilities.FindParentByType<Views.Preorders>(p);
                    if (n3 != null)
                    {
                        var id = DesignCardActual.SelectedDesignDetail.DesignID;
                        var fold = DesignCardActual.SelectedDesignDetail.DesignFolderPath;
                        if (id > 0)
                        {
                            using (Entities ctx = new Entities())
                            {
                                List<preorderitem> dli = ctx.preorderitems.Where(x => x.DesignID == id).ToList();
                                if (dli.Count > 0)
                                {
                                    foreach (preorderitem item in dli)
                                    {
                                        ctx.preorderitems.Remove(item);
                                        ctx.SaveChanges();
                                    }
                                }
                            }
                            using (Entities ctx = new Entities())
                            {
                                preorderdesigndetail dd = ctx.preorderdesigndetails.Where(x => x.DesignID == id).FirstOrDefault();
                                if (dd != null)
                                {
                                    ctx.preorderdesigndetails.Remove(dd);
                                    ctx.SaveChanges();
                                }
                            }
                        }
                        if (Directory.Exists(fold))
                        {
                            Utilities.DeleteDirectoryRecursive(fold);
                        }
                        
                        n3.DesignsSP.Children.Remove(this);
                      //  n3.DesignCount.Content = n3.DesignsSP.Children.OfType<MetroCardLarge>().Count();
                    }
                }
            }
        }

        public async void CopyArtFile(string artFile, string Type, string destination)
        {
            string datapth = Properties.Settings.Default.Data_TemplatesPath.ToString();
            string sourceFile;
            if (Type == "A")
            {
                sourceFile = datapth + "\\";
            }
            else if (Type == "Y")
            {
                sourceFile = datapth + @"\Youth\";
            }
            else if (Type == "S")
            {
                sourceFile = datapth + @"\Special\";
            }
            else
            {
                sourceFile = datapth + "\\";
            }

            string artname;
            if (Properties.Settings.Default.System_IllustratorEnabled)
            {
                artname = artFile + ".ai";
            }
            else
            {
                artname = artFile + ".cdr";
            }

            string source = sourceFile + artname;
            string destFinal = destination + artname;

            if (!Directory.Exists(destination))
            {
                Directory.CreateDirectory(destination);
            }

            if (File.Exists(destFinal))
            {
                MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Overwrite", "The file already exists in the target directory. Would you like to overwrite it?");
                if (result == MessageDialogResult.Affirmative)
                {
                    try
                    {
                        File.Copy(source, destFinal, true);
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }
            }
            else
            {
                try
                {
                    File.Copy(source, destFinal, true);
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }

        private async void DeleteItemClicked(object sender, EventArgs e)
        {
            // delete item
            MessageDialogResult mdr = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you want to delete this line item?");
            if (mdr == MessageDialogResult.Affirmative)
            {
                if (((MetroButton)sender).Tag != null)
                {
                    int id = Convert.ToInt32(((MetroButton)sender).Tag);
                    LineViewModel lv = LineItems.FirstOrDefault(x => x.SelectedLineItem.UniqueID == id);
                    LineItems.Remove(lv);
                    using (Entities ctx = new Entities())
                    {
                        if (IsPreorder)
                        {
                            var dd = ctx.preorderitems.FirstOrDefault(x => x.UniqueID == id);
                            ctx.preorderitems.Remove(dd);
                            ctx.SaveChanges();
                        }
                        else
                        {
                            var dd = ctx.designdetailslineitems.FirstOrDefault(x => x.UniqueID == id);
                            ctx.designdetailslineitems.Remove(dd);
                            ctx.SaveChanges();
                        }
                    }
                }
            }
        }


        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        #region DependencyProperty Members

        public static DependencyProperty SelectedDesignDetailProperty =
            DependencyProperty.Register("SelectedDesignDetail", typeof(designdetail), typeof(MetroDesignCard));

        public designdetail SelectedDesignDetail
        {
            get { return (designdetail)GetValue(SelectedDesignDetailProperty); }
            set { SetValue(SelectedDesignDetailProperty, value); }
        }

        public static DependencyProperty ExistingArtworkItemsProperty =
            DependencyProperty.Register("ExistingArtworkItems", typeof(ObservableCollection<MetroDesignFile>),
                typeof(MetroDesignCard));

        public ObservableCollection<MetroDesignFile> ExistingArtworkItems
        {
            get { return (ObservableCollection<MetroDesignFile>)GetValue(ExistingArtworkItemsProperty); }
            set { SetValue(ExistingArtworkItemsProperty, value); }
        }

        public static DependencyProperty CurrentArtworkItemsProperty = DependencyProperty.Register(
            "CurrentArtworkItems", typeof(ObservableCollection<string>), typeof(MetroDesignCard));

        public ObservableCollection<string> CurrentArtworkItems
        {
            get { return (ObservableCollection<string>)GetValue(CurrentArtworkItemsProperty); }
            set { SetValue(CurrentArtworkItemsProperty, value); }
        }

        public static DependencyProperty ArtworkItemsToAddProperty = DependencyProperty.Register("ArtworkItemsToAdd",
            typeof(ObservableCollection<string>), typeof(MetroDesignCard));

        public ObservableCollection<string> ArtworkItemsToAdd
        {
            get { return (ObservableCollection<string>)GetValue(ArtworkItemsToAddProperty); }
            set { SetValue(ArtworkItemsToAddProperty, value); }
        }

        public static DependencyProperty ArtworkItemsToRemoveProperty =
            DependencyProperty.Register("ArtworkItemsToRemove", typeof(ObservableCollection<string>),
                typeof(MetroDesignCard));

        public ObservableCollection<string> ArtworkItemsToRemove
        {
            get { return (ObservableCollection<string>)GetValue(ArtworkItemsToRemoveProperty); }
            set { SetValue(ArtworkItemsToRemoveProperty, value); }
        }

        public static DependencyProperty LineItemsProperty = DependencyProperty.Register("LineItems",
            typeof(ObservableCollection<LineViewModel>), typeof(MetroDesignCard));

        public ObservableCollection<LineViewModel> LineItems
        {
            get { return (ObservableCollection<LineViewModel>)GetValue(LineItemsProperty); }
            set { SetValue(LineItemsProperty, value); }
        }

        public static DependencyProperty IsHorizontalProperty = DependencyProperty.Register("IsHorizontal", typeof(bool),
    typeof(MetroDesignCard));

        public bool IsHorizontal
        {
            get { return (bool)GetValue(IsHorizontalProperty); }
            set { SetValue(IsHorizontalProperty, value); }
        }

        public static DependencyProperty IsNewDesignProperty = DependencyProperty.Register("IsNewDesign", typeof(bool),
            typeof(MetroDesignCard));

        public bool IsNewDesign
        {
            get { return (bool)GetValue(IsNewDesignProperty); }
            set { SetValue(IsNewDesignProperty, value); }
        }

        public static DependencyProperty IsExpandedProperty = DependencyProperty.Register("IsExpanded", typeof(bool),
            typeof(MetroDesignCard));

        public bool IsExpanded
        {
            get { return (bool)GetValue(IsExpandedProperty); }
            set { SetValue(IsExpandedProperty, value); }
        }

        public static DependencyProperty ShirtCountProperty = DependencyProperty.Register("ShirtCount", typeof(int), typeof(MetroDesignCard));
        public int ShirtCount
        {
            get { return (int)GetValue(ShirtCountProperty); }
            set { SetValue(ShirtCountProperty, value); }
        }

        public static DependencyProperty PrintCountProperty = DependencyProperty.Register("PrintCount", typeof(int), typeof(MetroDesignCard));
        public int PrintCount
        {
            get { return (int)GetValue(PrintCountProperty); }
            set { SetValue(PrintCountProperty, value); }
        }

        #endregion DependencyProperty Members

        private void TopMouseDown2(object sender, MouseButtonEventArgs e)
        {

        }

        private void OpenFolderClick(object sender, EventArgs e)
        {
            if (Directory.Exists(SelectedDesignDetail.DesignFolderPath))
            {
                try
                {
                    Process.Start(SelectedDesignDetail.DesignFolderPath);
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }

        private void LineItemTextChanged(object sender, RoutedEventArgs e)
        {
            mb2.Icon = "appbar_save";
            mb2.OpacityAnimation(1, TimeSpan.FromMilliseconds(400));
        }

        private async void SaveChangesToLineItems()
        {
            using (Entities ctx = new Entities())
            {
                    foreach (LineViewModel lvm in LineItems)
                    {
                        var line = ctx.preorderitems.Where(x => x.UniqueID == lvm.SelectedLineItem.UniqueID).FirstOrDefault();
                        line.StockColor = lvm.SelectedLineItem.Color;
                        line.StockDescrip = lvm.SelectedLineItem.Description;
                        line.DesignID = lvm.SelectedLineItem.DesignID;
                        line.StockQty = lvm.SelectedLineItem.Quantity;
                        line.PreorderID = lvm.SelectedLineItem.ReceiptNum;
                        line.StockSize = lvm.SelectedLineItem.Size;
                        line.StockCode = lvm.SelectedLineItem.StockIDPOS;
                        line.UniqueID = lvm.SelectedLineItem.UniqueID;
                        ctx.SaveChanges();
                    }
            }

            mb2.Icon = "appbar_check";
            await Task.Delay(1000);
            mb2.OpacityAnimation(0, TimeSpan.FromMilliseconds(400));
            await Task.Delay(400);
            mb2.Icon = "appbar_save";
        }

        private void ManuallySetLabels()
        {
            if (SelectedDesignDetail != null)
            {
                DesignNumLabel.Content = SelectedDesignDetail.DesignNum;
                DesignDescriptionLabel.Content = SelectedDesignDetail.DesignDescription;
                DesignPlatenLabel.Content = SelectedDesignDetail.AP_YOUTH ? "Small Platen" : "Large Platen";
                DesignPrintSizeLabel.Content = SelectedDesignDetail.PrintToFile ? "Print As-Is" : "Size to Item";
                using (Entities ctx = new Entities())
                {
                    DesignPrintProcessLabel.Content =
                        ctx.printprocesses.FirstOrDefault(x => x.ProcessID == SelectedDesignDetail.PrintProcess)
                            .ProcessName;
                }
            }
        }

        private void SaveLineChanges(object sender, EventArgs e)
        {
            SaveChangesToLineItems();
        }

        private async void AddLineItems(object sender)
        {

            await Task.Delay(50); 
            Views.Preorders odk = (Views.Preorders)Utilities.FindParentByType<Views.Preorders>(this);
            if (odk != null)
            {
                    using (Entities ctx = new Entities())
                    {
                        foreach (LineViewModel lvm in LineItems)
                        {
                            if (lvm.SelectedLineItem.UniqueID > 0)
                            {
                                preorderitem li = new preorderitem();
                                li.StockColor = lvm.SelectedLineItem.Color;
                                li.StockDescrip = lvm.SelectedLineItem.Description;
                                li.DesignID = SelectedDesignDetail.DesignID;
                                li.StockQty = lvm.SelectedLineItem.Quantity;
                                li.PreorderID = lvm.SelectedLineItem.ReceiptNum;
                                li.StockSize = lvm.SelectedLineItem.Size;
                                li.StockCode = lvm.SelectedLineItem.StockIDPOS;
                                li.EstimatedCost = lvm.LineCost;
                                ctx.preorderitems.Add(li);
                                ctx.SaveChanges();
                            }
                        }
                    }
            }
        }

        private void ItemsDropped(object sender, DragEventArgs e)
        {
            AddLineItems(sender);
        }

        private void FolderClick(object sender, EventArgs e)
        {
            if (SelectedDesignDetail != null)
            {
                string p = SelectedDesignDetail.DesignFolderPath;
                string cdr = Path.Combine(SelectedDesignDetail.DesignFolderPath, "CDR");

                if (!string.IsNullOrEmpty(p))
                {
                    if (Directory.Exists(cdr))
                    {
                        Process.Start(cdr);
                    }
                    else if (Directory.Exists(p))
                    {
                        Process.Start(p);
                    }
                }
            }
        }
    }
}