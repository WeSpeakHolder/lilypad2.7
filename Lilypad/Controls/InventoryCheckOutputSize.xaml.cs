﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Data;
using System.Windows.Documents;
using System.Net;
using System.Windows.Input;
using System.Xml.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.ViewModel;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class InventoryCheckOutputSize : UserControl
    {
        public InventoryCheckOutputSize()
        {
            InitializeComponent();
        }

        public InventoryCheckOutputSize(InventoryCheckViewModel inputvm)
        {
            InitializeComponent();
            this.DataContext = inputvm;
        }
        public InventoryCheckOutputSize(XDocument doc)
        {
            InitializeComponent();
            InventoryCheckViewModel newVM = new InventoryCheckViewModel();
            string resultAsString = doc.ToString();

                newVM.WHSE_Fresno = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "CN").Value));
                newVM.WHSE_Seattle = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "WA").Value));
                newVM.WHSE_Denver = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "CD").Value));
                newVM.WHSE_Lenexa = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "KS").Value));
                newVM.WHSE_Dallas = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "TD").Value));
                newVM.WHSE_Houston = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "TE").Value));
                newVM.WHSE_Verona = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "WI").Value));
                newVM.WHSE_Chicago = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "CC").Value));
                newVM.WHSE_Plymouth = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "OP").Value));
                newVM.WHSE_Middleboro = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "MA").Value));
                newVM.WHSE_Harrisburg = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "PH").Value));
                newVM.WHSE_Harrisburg = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "CT").Value));
                newVM.WHSE_Atlanta = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "GD").Value));
                newVM.WHSE_Orlando = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "FO").Value));
                newVM.SizeCode = doc.Root.Element("item").Attribute("size-code").Value.ToString();
                newVM.ItemNumber = doc.Root.Element("item").Attribute("item-number").Value.ToString();
                string search = "special-price=";

                if (resultAsString.Contains(search))
                {
                    string elem = doc.Root.Element("item").Attribute("special-price").Value.ToString();
                    string expiry = doc.Root.Element("item").Attribute("special-expiry").Value.ToString();

                    newVM.SpecialExpiry = expiry;
                    newVM.SpecialPrice = elem;
                }
                this.DataContext = newVM;            
        }

        public InventoryCheckOutputSize(XDocument doc, int sizeindex)
        {
            InitializeComponent();
            InventoryCheckViewModel newVM = new InventoryCheckViewModel();
            string resultAsString = doc.ToString();

            newVM.WHSE_Fresno = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "CN").Value));
            newVM.WHSE_Seattle = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "WA").Value));
            newVM.WHSE_Denver = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "CD").Value));
            newVM.WHSE_Lenexa = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "KS").Value));
            newVM.WHSE_Dallas = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "TD").Value));
            newVM.WHSE_Houston = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "TE").Value));
            newVM.WHSE_Verona = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "WI").Value));
            newVM.WHSE_Chicago = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "CC").Value));
            newVM.WHSE_Plymouth = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "OP").Value));
            newVM.WHSE_Middleboro = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "MA").Value));
            newVM.WHSE_Harrisburg = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "PH").Value));
            newVM.WHSE_Harrisburg = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "CT").Value));
            newVM.WHSE_Atlanta = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "GD").Value));
            newVM.WHSE_Orlando = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "FO").Value));
            newVM.SizeCode = doc.Root.Element("item").Attribute("size-code").Value.ToString();
            newVM.ItemNumber = doc.Root.Element("item").Attribute("item-number").Value.ToString();
            newVM.SizeIndex = sizeindex;
            string search = "special-price=";

            if (resultAsString.Contains(search))
            {
                string elem = doc.Root.Element("item").Attribute("special-price").Value.ToString();
                string expiry = doc.Root.Element("item").Attribute("special-expiry").Value.ToString();

                newVM.SpecialExpiry = expiry;
                newVM.SpecialPrice = elem;
            }
            this.DataContext = newVM;
        }

        public int Delay;

        public InventoryCheckOutputSize(DesignSetupCardViewModel vm)
        {
            InitializeComponent();
            this.DataContext = vm;
        }


        private void FullyLoaded(object sender, RoutedEventArgs e)
        {
            DoubleAnimation anim = new DoubleAnimation(1, TimeSpan.FromMilliseconds(400));
            ThicknessAnimation anim3 = new ThicknessAnimation(new Thickness(-30, 0, 0, 0), new Thickness(0, 0, 0, 0), new Duration(TimeSpan.FromMilliseconds(300)));

            CheckActual.BeginAnimation(OpacityProperty, anim);
            CheckActual.BeginAnimation(MarginProperty, anim3);
        }

        private void ShowDollars(object sender, MouseButtonEventArgs e)
        {
            PriceDol.Visibility = System.Windows.Visibility.Visible;
            PriceLex.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void ShowLex(object sender, MouseButtonEventArgs e)
        {
            PriceLex.Visibility = System.Windows.Visibility.Visible;
            PriceDol.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}
