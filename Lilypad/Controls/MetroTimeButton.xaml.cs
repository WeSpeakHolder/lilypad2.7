﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for SaveButton.xaml
    /// </summary>
    public partial class MetroTimeButton : Tile
    {
        public MetroTimeButton()
        {
            InitializeComponent();
            IconBGColor = Brushes.Gray;
        }

        private void LoadTooltip()
        {
            if (!string.IsNullOrEmpty(TooltipText))
            {
                var t = new Tooltip();
                t.Text = TooltipText;
                ToolTip = t;
            }
        }

        private void ButtonLoaded(object sender, RoutedEventArgs e)
        {
            LoadTooltip();
        }

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        #region DependencyProperty Members

        public static DependencyProperty NumLabelProperty = DependencyProperty.Register("NumLabel",
            typeof(string), typeof(MetroTimeButton));

        public string NumLabel
        {
            get { return (string)GetValue(NumLabelProperty); }
            set { SetValue(NumLabelProperty, value); }
        }

        public static DependencyProperty DayLabelProperty = DependencyProperty.Register("DayLabel",
            typeof(string), typeof(MetroTimeButton));

        public string DayLabel
        {
            get { return (string)GetValue(DayLabelProperty); }
            set { SetValue(DayLabelProperty, value); }
        }

        public static DependencyProperty TooltipTextProperty = DependencyProperty.Register("TooltipText",
            typeof(string), typeof(MetroTimeButton));

        public string TooltipText
        {
            get { return (string)GetValue(TooltipTextProperty); }
            set { SetValue(TooltipTextProperty, value); }
        }

        public static DependencyProperty IconBGColorProperty = DependencyProperty.Register("IconBGColor", typeof(Brush),
            typeof(MetroTimeButton));

        public Brush IconBGColor
        {
            get { return (Brush)GetValue(IconBGColorProperty); }
            set { SetValue(IconBGColorProperty, value); }
        }

        public static DependencyProperty IconColorProperty = DependencyProperty.Register("IconColor", typeof(Brush),
            typeof(MetroTimeButton));

        public Brush IconColor
        {
            get { return (Brush)GetValue(IconColorProperty); }
            set { SetValue(IconColorProperty, value); }
        }

        #endregion DependencyProperty Members
    }
}