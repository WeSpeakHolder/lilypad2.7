﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.Model;
using Lilypad.ViewModel;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls.DesignCards
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class LineItem : UserControl
    {


        #region DependencyProperty Members

        public static DependencyProperty QuantityProperty = DependencyProperty.Register("Quantity", typeof(int), typeof(LineItem));
        public int Quantity
        {
            get { return (int)GetValue(QuantityProperty); }
            set { SetValue(QuantityProperty, value); }
        }

        public static DependencyProperty StockCodeProperty = DependencyProperty.Register("StockCode", typeof(string), typeof(LineItem));
        public string StockCode
        {
            get { return (string)GetValue(StockCodeProperty); }
            set { SetValue(StockCodeProperty, value); }
        }

        public static DependencyProperty ColorProperty = DependencyProperty.Register("Color", typeof(string), typeof(LineItem));
        public string Color
        {
            get { return (string)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public static DependencyProperty SizeProperty = DependencyProperty.Register("Size", typeof(string), typeof(LineItem));
        public string Size
        {
            get { return (string)GetValue(SizeProperty); }
            set { SetValue(SizeProperty, value); }
        }

        public static DependencyProperty IsOnOrderProperty = DependencyProperty.Register("IsOnOrder", typeof(string), typeof(LineItem));
        public string IsOnOrder
        {
            get { return (string)GetValue(IsOnOrderProperty); }
            set { SetValue(IsOnOrderProperty, value); }
        }

        public static DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(designdetailslineitem), typeof(LineItem));
        public designdetailslineitem SelectedItem
        {
            get { return (designdetailslineitem)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }


        #endregion

        public LineItem()
        {
            InitializeComponent();
            IsLoadedFully = false;
        }

        public LineItem(designdetailslineitem lineitem)
        {
            InitializeComponent();
            IsOnOrder = lineitem.Description.ToString();
            Quantity = lineitem.Quantity;
            StockCode = lineitem.StockIDPOS;
            Color = lineitem.Color;
            Size = lineitem.Size;
        }

        private void LoadProperties()
        {
            if (SelectedItem != null)
            {
                designdetailslineitem lvm = (designdetailslineitem)this.DataContext;
                lvm.Quantity = SelectedItem.Quantity;
                lvm.StockIDPOS = SelectedItem.StockIDPOS;
                lvm.Color = SelectedItem.Color;
                lvm.Size = SelectedItem.Size;
                lvm.Description = SelectedItem.Description;
            }
        }

        public DoubleAnimation SlideDown(double toValue)
        {
            DoubleAnimation anim = new DoubleAnimation(toValue, TimeSpan.FromMilliseconds(300));
            QuadraticEase eas = new QuadraticEase();
            eas.EasingMode = EasingMode.EaseIn;
            anim.EasingFunction = eas;
            return anim;
        }

        public DoubleAnimation FadeOut(double toValue)
        {
            DoubleAnimation anim = new DoubleAnimation(toValue, TimeSpan.FromMilliseconds(700));
            return anim;
        }

        private void ContactEditorMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                quantityEditor.BeginAnimation(HeightProperty, SlideDown(21));
            }
        }

        private void ContactRightButton(object sender, MouseButtonEventArgs e)
        {
            quantityEditor.BeginAnimation(HeightProperty, SlideDown(0));
        }

        private void StockIDRightButton(object sender, MouseButtonEventArgs e)
        {
            stockIDEditor.BeginAnimation(HeightProperty, SlideDown(0));
        }

        private void StockEditorMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                stockIDEditor.BeginAnimation(HeightProperty, SlideDown(21));
            }
        }

        private void LineItemLoaded(object sender, RoutedEventArgs e)
        {
            LoadProperties();
            IsLoadedFully = true;
        }

        public bool IsLoadedFully { get; set; }

        private void ColorEditorMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                colorEditor.BeginAnimation(HeightProperty, SlideDown(21));
            }
        }

        private void EditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ((UIElement)sender).BeginAnimation(HeightProperty, SlideDown(0));
            }
        }

        private void EditorChanged(object sender, TextChangedEventArgs e)
        {
            if (IsLoadedFully)
            {
                DesignCardNew dCard = Utilities.FindParentByType<DesignCardNew>((DependencyObject)this);
                dCard.MarkChangesNeeded();
            }
        }

        private void SizeRightButton(object sender, MouseButtonEventArgs e)
        {
                sizeEditor.BeginAnimation(HeightProperty, SlideDown(0));
        }

        private void SizeEditorMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                sizeEditor.BeginAnimation(HeightProperty, SlideDown(21));
            }
        }

        private void LineItemActual_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            quantityEditor.BeginAnimation(HeightProperty, SlideDown(0));
            stockIDEditor.BeginAnimation(HeightProperty, SlideDown(0));
            colorEditor.BeginAnimation(HeightProperty, SlideDown(0));
            sizeEditor.BeginAnimation(HeightProperty, SlideDown(0));
        }

        private async void DeleteClicked(object sender, MouseButtonEventArgs e)
        {
            MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you wish to delete this line item?");
            if (res == MessageDialogResult.Affirmative)
            {
                DeleteSP.Opacity = 0.1;
                this.Opacity = 0.25;
                this.IsEnabled = false;
                DesignCardNew dCard = Utilities.FindParentByType<DesignCardNew>((DependencyObject)this);
                dCard.MarkChangesNeeded();
            }
        }

        private void DeleteMouseOver(object sender, MouseEventArgs e)
        {
            DeleteSP.BeginAnimation(OpacityProperty, new DoubleAnimation(0.8, TimeSpan.FromMilliseconds(250)));
        }

        private void DeleteMouseout(object sender, MouseEventArgs e)
        {
            DeleteSP.BeginAnimation(OpacityProperty, new DoubleAnimation(0.2, TimeSpan.FromMilliseconds(250)));
        }



    }
}
