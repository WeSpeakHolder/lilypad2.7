﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.ViewModel;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls.DesignCards
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class SetupCard : UserControl
    {
        public SetupCard()
        {
            InitializeComponent();
        }

        public SetupCard(DesignSetupCardViewModel vm)
        {
            InitializeComponent();
            this.DataContext = vm;
        }

        private void ItemEdited(object sender, TextChangedEventArgs e)
        {

        }

        private void SetupArtClicked(object sender, EventArgs e)
        {
            var vm = (DesignSetupCardViewModel)this.DataContext;
            Utilities.ShowArtSetup(vm);
        }

        private void DeleteEnter(object sender, MouseEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            rect.Fill = Brushes.Red;
            rect.Opacity = 0.9;
        }

        private void DeleteLeave(object sender, MouseEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            rect.Fill = Brushes.Black;
            rect.Opacity = 0.1;
        }

        private void DeleteClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem elem = Utilities.FindParentByType<ListViewItem>((DependencyObject)sender);
            LineViewModel vm = (LineViewModel)elem.Content;
            ListView view = Utilities.FindParentByType<ListView>((DependencyObject)elem);
            IList<LineViewModel> list = (IList<LineViewModel>)view.ItemsSource;
            list.Remove(vm);
        }

        private void DeleteCardClick(object sender, MouseButtonEventArgs e)
        {
            Expander exp = (Expander)this.Parent;
            StackPanel par = (StackPanel)exp.Parent;
            Views.NewOrder order = Utilities.FindParentByType<Views.NewOrder>((DependencyObject)par);
            par.Children.Remove(exp);
            order.ResetDesignNumOrders();
        }
    }
}
