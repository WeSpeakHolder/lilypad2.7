﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;
using Lilypad.Model;
using Lilypad.ViewModel;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls.DesignCards
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class DesignFile : UserControl
    {
        public DesignFile()
        {
            InitializeComponent();
        }

        private void CDRFileClicked(object sender, MouseButtonEventArgs e)
        {
            DesignFileViewModel vm = (DesignFileViewModel)this.DataContext;
            if (vm.FilePath != null)
            {
                Process.Start(vm.FilePath);
            }
        }

        private void OpenInCorel(object sender, RoutedEventArgs e)
        {
            DesignFileViewModel vm = (DesignFileViewModel)this.DataContext;
            string pth = vm.FilePath.ToString();

            if (File.Exists(pth))
            {
                        FileInfo inf = new FileInfo(pth);
                        try
                        {
                            Thread t = new Thread(() =>
                            {
                                Process proc = new Process();
                                ProcessStartInfo prs = new ProcessStartInfo();
                                prs.FileName = inf.FullName;
                                //  prs.UseShellExecute = false;
                                proc.StartInfo = prs;
                                proc.Start();
                            });
                            t.Start();

                        }
                        catch (Exception ex)
                        {
                            
                        }
            }
        }

        private void OpenInPhotoshop(object sender, RoutedEventArgs e)
        {

            DesignFileViewModel vm = (DesignFileViewModel)this.DataContext;
            if (vm.FilePath != null)
            {
                try
                {
                    System.Diagnostics.Process.Start(vm.FilePath);
                }
                catch (Win32Exception ex)
                {
                    System.Threading.Thread.Sleep(5000);
                    System.Diagnostics.Process.Start(vm.FilePath);
                }
            }
        }

        private void OpenInFolder(object sender, RoutedEventArgs e)
        {
            string filepath = "";
            DesignFileViewModel vm = (DesignFileViewModel)this.DataContext;
            if (vm.FilePath != null) filepath = vm.FilePath;

            string parent = Directory.GetParent(filepath).FullName;
            System.Diagnostics.Process.Start(parent);
        }

        private async void DeleteFile(object sender, RoutedEventArgs e)
        {
            string filepath = "";
            DesignFileViewModel vm = (DesignFileViewModel)this.DataContext;
            if (vm.FilePath != null) filepath = vm.FilePath;

            MahApps.Metro.Controls.Dialogs.MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Delete", "Are you sure you want to delete this file? This cannot be undone!");
            if (result == MahApps.Metro.Controls.Dialogs.MessageDialogResult.Affirmative)
            {
                try
                {
                    File.Delete(filepath);
                    Utilities.ShowSimpleDialog("Notification", "File deleted successfully.");
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }

        }

        private void OpenInCorel2(object sender, MouseButtonEventArgs e)
        {
            OpenInCorel(null, null);
        }

        private void RenameFile(object sender, RoutedEventArgs e)
        {
            FilenameEditor.Clear();
            FilenameEditor.Visibility = System.Windows.Visibility.Visible;
            Keyboard.Focus(FilenameEditor);
        }

        private void RefreshFiles(object sender, RoutedEventArgs e)
        {

        }

        private void FilenameeditorKeydown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                if (FilenameEditor.Text != "")
                {
                    RenameThisFile();
                }
                else
                {
                    Utilities.ShowSimpleDialog("Notification", "Filename cannot be blank.");
                }
            }
        }

        private void RenameThisFile()
        {
            DesignFileViewModel vm = (DesignFileViewModel)this.DataContext;

            string newname = FilenameEditor.Text;
            string filepath = "";
            string filetype = "";
            string parent = "";
            string fullpath = "";
            string newname2 = "";

            if (vm.FilePath != null)
            {
                filepath = vm.FilePath;
                parent = Directory.GetParent(filepath).ToString();
                filetype = "." + vm.FileType;
                newname2 = newname + filetype;
                fullpath = System.IO.Path.Combine(parent, newname2);
            }
            try
            {
                File.Move(filepath, fullpath);
                FilenameEditor.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch (IOException iox)
            {
                new Error(iox);
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }
    }
}
