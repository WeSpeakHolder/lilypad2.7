﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;
using Lilypad.Model;
using Lilypad.ViewModel;
using Lilypad.Controls.DesignCards;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls.DesignCards
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class ViewCard : UserControl
    {
        public ViewCard()
        {
            InitializeComponent();
        }

        public ViewCard(designdetail input)
        {
            InitializeComponent();
            DesignSetupCardViewModel vm = new DesignSetupCardViewModel();
            vm.SelectedDesignDetail = input;
            this.DataContext = vm;
        }

        public ViewCard(DesignSetupCardViewModel vm)
        {
            InitializeComponent();
            vm.GetLineItems();
            this.DataContext = vm;

        }

        public string DesignPath;

        public System.IO.FileSystemWatcher fileSystemWatcher;
        // File watching system
        private void StartFileSystemWatcher(string directory)
        {
            string folderPath = directory;

            // If there is no folder selected, to nothing
            if (string.IsNullOrWhiteSpace(folderPath))
                return;

            fileSystemWatcher = new System.IO.FileSystemWatcher();

            // Set folder path to watch
            fileSystemWatcher.Path = folderPath;

            // Gets or sets the type of changes to watch for.
            // In this case we will watch change of filename, last modified time, size and directory name
            fileSystemWatcher.NotifyFilter = System.IO.NotifyFilters.FileName |
                System.IO.NotifyFilters.LastWrite |
                System.IO.NotifyFilters.Size |
                System.IO.NotifyFilters.DirectoryName;


            // Event handlers that are watching for specific event
            fileSystemWatcher.Created += new System.IO.FileSystemEventHandler(fileSystemWatcher_Created);
            fileSystemWatcher.Changed += new System.IO.FileSystemEventHandler(fileSystemWatcher_Changed);
            fileSystemWatcher.Deleted += new System.IO.FileSystemEventHandler(fileSystemWatcher_Deleted);
            fileSystemWatcher.Renamed += new System.IO.RenamedEventHandler(fileSystemWatcher_Renamed);

            // NOTE: If you want to monitor specified files in folder, you can use this filter
            // fileSystemWatcher.Filter

            // START watching
            fileSystemWatcher.EnableRaisingEvents = true;
            FileSystemWatcherActivated = true;
        }

        // ----------------------------------------------------------------------------------
        // Events that do all the monitoring
        // ----------------------------------------------------------------------------------

        public bool FileSystemWatcherActivated = false;

        void fileSystemWatcher_Created(object sender, System.IO.FileSystemEventArgs e)
        {
            if (FileSystemWatcherActivated) Dispatcher.Invoke(new Action(() => { GetArtworkFiles(); }));
        }

        void fileSystemWatcher_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            if (FileSystemWatcherActivated) Dispatcher.Invoke(new Action(() => { GetArtworkFiles(); }));
        }

        void fileSystemWatcher_Deleted(object sender, System.IO.FileSystemEventArgs e)
        {
            if (FileSystemWatcherActivated) Dispatcher.Invoke(new Action(() => { GetArtworkFiles(); var vm = (DesignSetupCardViewModel)this.DataContext; vm.FlaggedArtworkItems.Clear(); }));
        }

        void fileSystemWatcher_Renamed(object sender, System.IO.RenamedEventArgs e)
        {
            if (FileSystemWatcherActivated) Dispatcher.Invoke(new Action(() => { GetArtworkFiles(); }));
        }

        // ----------------------------------------------------------------------------------

        void DisplayFileSystemWatcherInfo(System.IO.WatcherChangeTypes watcherChangeTypes, string name, string oldName = null)
        {
            if (watcherChangeTypes == System.IO.WatcherChangeTypes.Renamed)
            {
                // When using FileSystemWatcher event's be aware that these events will be called on a separate thread automatically!!!
                // If you call method AddListLine() in a normal way, it will throw following exception: 
                // "The calling thread cannot access this object because a different thread owns it"
                // To fix this, you must call this method using Dispatcher.BeginInvoke(...)!
                Dispatcher.BeginInvoke(new Action(() => { AddListLine(string.Format("{0} -> {1} to {2} - {3}", watcherChangeTypes.ToString(), oldName, name, DateTime.Now)); }));
            }
            else
            {
                Dispatcher.BeginInvoke(new Action(() => { AddListLine(string.Format("{0} -> {1} - {2}", watcherChangeTypes.ToString(), name, DateTime.Now)); }));
            }
        }
        public void AddListLine(string text)
        {
            Utilities.ShowSimpleDialog("Result", text);
        }
        public void GetArtworkFiles()
        {
            FileViewerTabControl.Items.Clear();

            DesignSetupCardViewModel vm = (DesignSetupCardViewModel)ViewCardActual.DataContext;
            if (vm.SelectedDesignDetail != null)
            {
                DesignPath = vm.SelectedDesignDetail.DesignFolderPath.ToString();
                if (Directory.Exists(DesignPath))
                {
                    int fNum = 1;
                    int desNum = 0;

                    if (Directory.GetDirectories(DesignPath).Length > 0)
                    {
                        foreach (string dir in Directory.GetDirectories(DesignPath))
                        {
                            DirectoryInfo info = new DirectoryInfo(dir);
                            string nameasExt = "*." + info.Name.ToLower();

                            // Get files for each directory
                            string[] files = Directory.GetFiles(dir).Where(x => !x.EndsWith(".db") && !x.EndsWith(".tmp")).ToArray();

                            DevExpress.Xpf.LayoutControl.FlowLayoutControl lc = new DevExpress.Xpf.LayoutControl.FlowLayoutControl();
                            lc.Orientation = Orientation.Vertical;
                            lc.Padding = new Thickness(0);
                            lc.ScrollBars = ScrollBars.None;
                            ItemsControl container = new ItemsControl();
                            container.VerticalContentAlignment = System.Windows.VerticalAlignment.Top;
                            container.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Left;
                            container.Margin = new Thickness(5, 7, 0, 5);
                            foreach (string file in files)
                            {
                                DesignFileViewModel newVM = new DesignFileViewModel(file);
                                DesignFile newViewer = new DesignFile();
                                newViewer.DataContext = newVM;
                                container.Items.Add(newViewer);

                                if (file.EndsWith(".cdr"))
                                {
                                    desNum = desNum + 1;
                                }
                            }

                            lc.Children.Add(container);
                            // Add everything to the tab controller
                            DXTabItem item = new DXTabItem();
                            item.Tag = info.FullName;
                            item.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Left;
                            item.VerticalContentAlignment = System.Windows.VerticalAlignment.Top;
                            TextBlock tbb = new TextBlock(); tbb.Text = info.Name;
                            tbb.Tag = info.FullName;
                            tbb.MouseDown += tbb_MouseDown;
                            item.Header = tbb;
                            item.Content = lc;
                            item.Style = (Style)FindResource("Light");
                            FileViewerTabControl.Items.Add(item);
                            fNum += 1;
                        }
                    }
                    else
                    {
                        string[] files = Directory.GetFiles(DesignPath).Where(x => !x.EndsWith(".db") && !x.EndsWith(".tmp")).ToArray();

                        DevExpress.Xpf.LayoutControl.FlowLayoutControl lc = new DevExpress.Xpf.LayoutControl.FlowLayoutControl();
                        lc.Orientation = Orientation.Vertical;
                        lc.Padding = new Thickness(0);
                        lc.ScrollBars = ScrollBars.None;
                        ItemsControl container = new ItemsControl();
                        container.VerticalContentAlignment = System.Windows.VerticalAlignment.Top;
                        container.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Left;
                        container.Margin = new Thickness(5, 7, 0, 5);
                        foreach (string file in files)
                        {
                            DesignFileViewModel newVM = new DesignFileViewModel(file);
                            DesignFile newViewer = new DesignFile();
                            newViewer.DataContext = newVM;
                            container.Items.Add(newViewer);
                            desNum = desNum + 1;
                        }

                        lc.Children.Add(container);
                        // Add everything to the tab controller
                        DXTabItem item = new DXTabItem();
                        item.Tag = DesignPath;
                        item.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Left;
                        item.VerticalContentAlignment = System.Windows.VerticalAlignment.Top;
                        TextBlock tbb = new TextBlock(); tbb.Text = "Main"; tbb.Tag = DesignPath; tbb.MouseDown += tbb_MouseDown;
                        item.Header = tbb;
                        item.Content = lc;
                        item.Style = (Style)FindResource("Light");
                        FileViewerTabControl.Items.Add(item);
                        fNum += 1;
                    }

                    vm.ArtCount = desNum;
                }
            }
        }

        void tbb_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount >= 2)
            {
                TextBlock ss = (TextBlock)sender;
                Thread t = new Thread(() =>
                {
                    string file = ss.Tag.ToString();
                    System.Diagnostics.Process.Start(file);
                });
                t.Start();

            }
        }



        private void ItemEdited(object sender, TextChangedEventArgs e)
        {

        }

        private void SetupArtClicked(object sender, EventArgs e)
        {
            var vm = (DesignSetupCardViewModel)this.DataContext;
            Utilities.ShowArtSetup(vm);
        }

        private void DeleteEnter(object sender, MouseEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            rect.Fill = Brushes.Red;
            rect.Opacity = 0.9;
        }

        private void DeleteLeave(object sender, MouseEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            rect.Fill = Brushes.Black;
            rect.Opacity = 0.1;
        }

        private void DeleteClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem elem = Utilities.FindParentByType<ListViewItem>((DependencyObject)sender);
            LineViewModel vm = (LineViewModel)elem.Content;
            ListView view = Utilities.FindParentByType<ListView>((DependencyObject)elem);
            IList<LineViewModel> list = (IList<LineViewModel>)view.ItemsSource;
            list.Remove(vm);
        }

        private void DeleteCardClick(object sender, MouseButtonEventArgs e)
        {
            Expander exp = (Expander)this.Parent;
            StackPanel par = (StackPanel)exp.Parent;
            Views.Hopper hopper = Utilities.FindParentByType<Views.Hopper>((DependencyObject)par);
            par.Children.Remove(exp);
        }

        private void viewCardLoaded(object sender, RoutedEventArgs e)
        {
            GetArtworkFiles();
        }
    }
}
