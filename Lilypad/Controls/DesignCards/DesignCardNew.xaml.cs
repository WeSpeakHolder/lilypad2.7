﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Media.Animation;
using Lilypad.Model;
using Lilypad.ViewModel;
using Lilypad.Controls.DesignCards;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls.DesignCards
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class DesignCardNew : UserControl
    {

        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }
        #endregion

        public DesignCardNew()
        {
            InitializeComponent();
            LoadedFully = new bool();
            designfolders = new List<TabHeader>();
            LoadedFully = false;
            LineItems = new List<designdetailslineitem>();
        }
        private void viewCardLoaded(object sender, RoutedEventArgs e)
        {
            LoadedFully = true;
            LoadLocalProperties();
            GetArtworkFolders(DesignFolderPath);
            LoadArtwork();
            StartFileSystemWatcher(DesignFolderPath);
        }

        private bool LoadedFully;

        public DoubleAnimation SlideDown(double toValue)
        {
            DoubleAnimation anim = new DoubleAnimation(toValue, TimeSpan.FromMilliseconds(300));
            QuadraticEase eas = new QuadraticEase();
            eas.EasingMode = EasingMode.EaseIn;
            anim.EasingFunction = eas;
            return anim;
        }

        

        private void LoadLocalProperties()
        {
            int id = (Int32)this.Tag;
            designdetail dd = new designdetail();
            using (Entities ctx = new Entities())
            {
                dd = ctx.designdetails.Where(x => x.DesignID == id).FirstOrDefault();
                int deID = dd.DesignID;
                LineItems = ctx.designdetailslineitems.Where(x => x.DesignID == deID).ToList();
            }
            DesignFolderPath = dd.DesignFolderPath;
        }

        public List<designdetailslineitem> LineItems
        {
            get { return (List<designdetailslineitem>)GetValue(itemslistproperty); }
            set { SetValue(itemslistproperty, value); OnPropertyChanged("LineItems"); ItemsCount = ((List<designdetailslineitem>)value).Sum(x => x.Quantity); }
        }

        public int ItemsCount
        {
            get { return (int)GetValue(itemscountproperty); }
            set { SetValue(itemscountproperty, value); OnPropertyChanged("ItemsCount"); }
        }

        public static readonly DependencyProperty itemscountproperty = DependencyProperty.Register("ItemsCount", typeof(int), typeof(DesignCardNew));
        public static readonly DependencyProperty itemslistproperty = DependencyProperty.Register("LineItems", typeof(List<designdetailslineitem>), typeof(DesignCardNew));
        
        private void LoadArtwork()
        {
            if (FoldersContainer.Children.Count > 0)
            {
                FoldersContainer.Children.Clear();
            }

            foreach (TabHeader th in DesignFolders)
            {
                th.MouseDown += (s, args) => { GetArtworkFiles(th.SelectedItem.ToString()); };
                th.MouseDoubleClick += (s, args) => { System.Diagnostics.Process.Start(th.SelectedItem.ToString()); };
                FoldersContainer.Children.Add(th);
            }
        }

        public System.IO.FileSystemWatcher fileSystemWatcher;
        // File watching system
        private void StartFileSystemWatcher(string directory)
        {
            string folderPath = directory;

            // If there is no folder selected, to nothing
            if (string.IsNullOrWhiteSpace(folderPath))
                return;

            if (!System.IO.Directory.Exists(folderPath))
                return;

            fileSystemWatcher = new System.IO.FileSystemWatcher();

            // Set folder path to watch
            fileSystemWatcher.Path = folderPath;

            // Gets or sets the type of changes to watch for.
            // In this case we will watch change of filename, last modified time, size and directory name
            fileSystemWatcher.NotifyFilter = System.IO.NotifyFilters.FileName |
                System.IO.NotifyFilters.LastWrite |
                System.IO.NotifyFilters.Size |
                System.IO.NotifyFilters.DirectoryName;


            // Event handlers that are watching for specific event
            fileSystemWatcher.Created += new System.IO.FileSystemEventHandler(fileSystemWatcher_Created);
            fileSystemWatcher.Changed += new System.IO.FileSystemEventHandler(fileSystemWatcher_Changed);
            fileSystemWatcher.Deleted += new System.IO.FileSystemEventHandler(fileSystemWatcher_Deleted);
            fileSystemWatcher.Renamed += new System.IO.RenamedEventHandler(fileSystemWatcher_Renamed);

            // NOTE: If you want to monitor specified files in folder, you can use this filter
            // fileSystemWatcher.Filter

            // START watching
            fileSystemWatcher.EnableRaisingEvents = true;
            FileSystemWatcherActivated = true;
        }

        public bool FileSystemWatcherActivated = false;

        void fileSystemWatcher_Created(object sender, System.IO.FileSystemEventArgs e)
        {
            if (FileSystemWatcherActivated) Dispatcher.Invoke(new Action(() => { GetArtworkFolders(DesignFolderPath); }));
        }

        void fileSystemWatcher_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            if (FileSystemWatcherActivated) Dispatcher.Invoke(new Action(() => { GetArtworkFolders(DesignFolderPath); }));
        }

        void fileSystemWatcher_Deleted(object sender, System.IO.FileSystemEventArgs e)
        {
            if (FileSystemWatcherActivated) Dispatcher.Invoke(new Action(() => { GetArtworkFolders(DesignFolderPath); }));
        }

        void fileSystemWatcher_Renamed(object sender, System.IO.RenamedEventArgs e)
        {
            if (FileSystemWatcherActivated) Dispatcher.Invoke(new Action(() => { GetArtworkFolders(DesignFolderPath); }));
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                DescriptionEditor.BeginAnimation(HeightProperty, SlideDown(32));
            }
        }

        private void DescripChanged(object sender, TextChangedEventArgs e)
        {
            MarkChangesNeeded();
        }

        public async void MarkChangesNeeded()
        {
            if (LoadedFully)
            {
                if (!SaveChangesNeeded)
                {
                    Rectangle rec = new Rectangle();
                    VisualBrush vis = new VisualBrush();
                    vis.Visual = (Visual)App.Current.FindResource("appbar_save");
                    rec.Fill = Brushes.White;
                    rec.OpacityMask = vis;
                    rec.Height = 18;
                    rec.Width = 18;
                    SaveTile.Background = Brushes.Orange;
                    SaveTile.Content = rec;
                    SaveChangesNeeded = true;
                }
            }
        }

        public async void SaveChanges()
        {
            if (LoadedFully)
            {
                List<LineItem> deletedItems = new List<LineItem>();

                if (SaveChangesNeeded)
                {
                    int thisID = Convert.ToInt32(this.Tag.ToString());
                    designdetail thisDeet;
                    using (Entities ctx = new Entities())
                    {
                        thisDeet = ctx.designdetails.Where(x => x.DesignID == thisID).FirstOrDefault();
                        thisDeet.DesignDescription = DescriptionEditor.Text;
                        thisDeet.SizeToGarment = (bool)SizingLabel.Tag;
                        thisDeet.PrintProcess = (Int32)processEditor.EditValue;
                        thisDeet.AP_YOUTH = (bool)PlatenLabel.Tag;

                        if (ctx.designdetailslineitems.Where(x => x.DesignID == thisID).Any())
                        {
                            foreach (LineItem item in LinesContainer.Children.OfType<LineItem>())
                            {
                                designdetailslineitem dditem = (designdetailslineitem)item.SelectedItem;
                                designdetailslineitem lvm = (designdetailslineitem)item.DataContext;
                                designdetailslineitem sqlitem = ctx.designdetailslineitems.Where(x => x.UniqueID == dditem.UniqueID).FirstOrDefault();
                                sqlitem.Quantity = lvm.Quantity;
                                sqlitem.StockIDPOS = lvm.StockIDPOS;
                                sqlitem.Color = lvm.Color;
                                sqlitem.Size = lvm.Size;
                                sqlitem.Description = lvm.Description;

                                if (!item.IsEnabled)
                                {
                                    deletedItems.Add(item);
                                    ctx.designdetailslineitems.Remove(sqlitem);
                                }

                            }
                        }

                        ctx.SaveChanges();
                    }
                    SaveChangesNeeded = false;

                    Label lb = new Label();
                    lb.Style = (Style)App.Current.FindResource("Bold");
                    lb.Content = dNum.Content;
                    lb.FontSize = dNum.FontSize;
                    lb.Foreground = dNum.Foreground;

                    Rectangle rec = new Rectangle();
                    VisualBrush vis = new VisualBrush();
                    vis.Visual = (Visual)App.Current.FindResource("appbar_check");
                    rec.Fill = Brushes.White;
                    rec.OpacityMask = vis;
                    rec.Height = 16;
                    rec.Width = 18;

                    SaveTile.Background = Brushes.YellowGreen;
                    SaveTile.Content = rec;

                    DescriptionEditor.BeginAnimation(HeightProperty, SlideDown(0));
                    processEditor.BeginAnimation(HeightProperty, SlideDown(0));

                    if (deletedItems.Any())
                    {
                        foreach (LineItem item in deletedItems)
                        {
                            LinesContainer.Children.Remove(item);
                        }
                    }

                    await Task.Delay(100);
                    foreach (LineItem item in LinesContainer.Children.OfType<LineItem>())
                    {
                        item.IsEnabled = false;
                        await Task.Delay(10);
                        item.IsEnabled = true;
                    }

                    await Task.Delay(900);
                    SaveTile.Content = lb;
                    await Task.Delay(900);
                    SaveTile.Background = (Brush)App.Current.FindResource("AccentColorBrush");


                }
            }
        }

        private bool savechangesneeded;
        public bool SaveChangesNeeded
        {
            get { return savechangesneeded; }
            set { savechangesneeded = value; OnPropertyChanged("SaveChangesNeeded"); }
        }

        private string designfolderpath;
        public string DesignFolderPath
        {
            get { return designfolderpath; }
            set { designfolderpath = value; OnPropertyChanged("DesignFolderPath"); }
        }

        private Int32 designnum;
        public Int32 DesignNum
        {
            get { return designnum; }
            set { designnum = value; OnPropertyChanged("DesignNum"); }
        }

        private List<TabHeader> designfolders;
        public List<TabHeader> DesignFolders
        {
            get { return designfolders; }
            set { designfolders = value; OnPropertyChanged("DesignFolders"); }
        }

        private void SaveTileClicked(object sender, EventArgs e)
        {
            SaveChanges();
        }

        private void TitleEditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SaveChanges();
            }
        }

        private void ProcessEditor_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            MarkChangesNeeded();
        }

        private void editprocess(object sender, MouseButtonEventArgs e)
        {
            processEditor.BeginAnimation(HeightProperty, SlideDown(19));
        }

        private void ProcessRightButton(object sender, MouseButtonEventArgs e)
        {
            processEditor.BeginAnimation(HeightProperty, SlideDown(0));
        }

        private void SizingMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                if ((bool)SizingLabel.Tag == true)
                {
                    SizingLabel.Tag = false;
                    SizingLabel.Content = "Print As-Is";
                }
                else
                {
                    SizingLabel.Tag = true;
                    SizingLabel.Content = "Size To Item";
                }

                MarkChangesNeeded();
            }
        }

        private void PlatenMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                if ((bool)PlatenLabel.Tag == true)
                {
                    PlatenLabel.Tag = false;
                    PlatenLabel.Content = "Large Platen";
                }
                else
                {
                    PlatenLabel.Tag = true;
                    PlatenLabel.Content = "Small Platen";
                }

                MarkChangesNeeded();
            }
        }

        public void GetArtworkFolders(string DesignPath)
        {
            if (DesignFolders.Count > 0)
            {
                DesignFolders.Clear();
            }

            if (Directory.Exists(DesignPath))
            {
                bool isFirst = new bool();

                if (Directory.GetDirectories(DesignPath).Length > 0)
                {
                    foreach (string dir in Directory.GetDirectories(DesignPath))
                    {
                        DirectoryInfo info = new DirectoryInfo(dir);

                        if (!isFirst) { TabHeader th = new TabHeader(info.Name.ToString(), true); th.SelectedItem = info.FullName.ToString(); DesignFolders.Add(th); isFirst = true; GetArtworkFiles(info.FullName.ToString()); }
                        else { TabHeader th = new TabHeader(info.Name.ToString()); th.SelectedItem = info.FullName.ToString(); DesignFolders.Add(th); }

                        if (Directory.GetDirectories(info.FullName).Length > 0)
                        {
                            foreach (string dir2 in Directory.GetDirectories(info.FullName))
                            {
                                DirectoryInfo info2 = new DirectoryInfo(dir);

                                if (!isFirst) { TabHeader th = new TabHeader(info2.Name.ToString(), true); th.SelectedItem = info2.FullName.ToString(); DesignFolders.Add(th); isFirst = true; GetArtworkFiles(info2.FullName.ToString()); }
                                else { TabHeader th = new TabHeader(info2.Name.ToString()); th.SelectedItem = info2.FullName.ToString(); DesignFolders.Add(th); }
                            }
                        }

                    }

                    
                }
            }
        }

        public void GetArtworkFiles(string DesignPath)
        {
            int desNum = 0;

            if (FilesContainer.Children.Count > 0)
            {
                FilesContainer.Children.Clear();
            }

            // Get files for each directory
            string[] files = Directory.GetFiles(DesignPath).Where(x => !x.EndsWith(".db") && !x.EndsWith(".tmp")).ToArray();

            foreach (string file in files)
            {
                DesignFileViewModel newVM = new DesignFileViewModel(file);
                DesignFile newViewer = new DesignFile();
                newViewer.DataContext = newVM;

                if (file.EndsWith(".cdr"))
                {
                    if (!file.ToLower().StartsWith("backup"))
                    {
                        desNum = desNum + 1;
                    }
                }

                if (file.EndsWith(".ai"))
                {
                    if (!file.ToLower().StartsWith("backup"))
                    {
                        desNum = desNum + 1;
                    }
                }

                FilesContainer.Children.Add(newViewer);
            }

            DesignNum = desNum;
        }

    }
}
