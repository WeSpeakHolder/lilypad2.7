﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for SaveButton.xaml
    /// </summary>
    public partial class MetroIconBox : Tile
    {
        public MetroIconBox()
        {
            InitializeComponent();
            Circle = false;
        }

        private void ButtonLoaded(object sender, RoutedEventArgs e)
        {

        }

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        #region DependencyProperty Members

        public static DependencyProperty CircleProperty = DependencyProperty.Register("Circle", typeof(bool),
            typeof(MetroIconBox));

        public bool Circle
        {
            get { return (bool)GetValue(CircleProperty); }
            set { SetValue(CircleProperty, value); }
        }

        public static DependencyProperty PortraitProperty = DependencyProperty.Register("Portrait", typeof(bool),
            typeof(MetroIconBox));

        public bool Portrait
        {
            get { return (bool)GetValue(PortraitProperty); }
            set { SetValue(PortraitProperty, value); }
        }

        public static DependencyProperty MainLabelProperty = DependencyProperty.Register("MainLabel",
            typeof(string), typeof(MetroIconBox));

        public string MainLabel
        {
            get { return (string)GetValue(MainLabelProperty); }
            set { SetValue(MainLabelProperty, value); }
        }

        public static DependencyProperty SubLabelProperty = DependencyProperty.Register("SubLabel",
    typeof(string), typeof(MetroIconBox));

        public string SubLabel
        {
            get { return (string)GetValue(SubLabelProperty); }
            set { SetValue(SubLabelProperty, value); }
        }

        public static DependencyProperty IconNameProperty = DependencyProperty.Register("Icon", typeof(string),
            typeof(MetroIconBox));

        public string Icon
        {
            get { return (string)GetValue(IconNameProperty); }
            set { SetValue(IconNameProperty, value); }
        }

        public static DependencyProperty IconBGColorProperty = DependencyProperty.Register("IconBGColor", typeof(Brush),
            typeof(MetroIconBox));

        public Brush IconBGColor
        {
            get { return (Brush)GetValue(IconBGColorProperty); }
            set { SetValue(IconBGColorProperty, value); }
        }

        public static DependencyProperty IconColorProperty = DependencyProperty.Register("IconColor", typeof(Brush),
            typeof(MetroIconBox));

        public Brush IconColor
        {
            get { return (Brush)GetValue(IconColorProperty); }
            set { SetValue(IconColorProperty, value); }
        }

        #endregion DependencyProperty Members
    }
}