﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.Model;
using Lilypad.ViewModel;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class TaskCard : UserControl
    {
        public TaskCard()
        {
            AllLoaded = new bool();
            AllLoaded = false;
            InitializeComponent();
        }

        public TaskCard(TaskCardViewModel vm)
        {
            InitializeComponent();
            this.DataContext = vm;
        }

        public TaskCard(task task1)
        {
            InitializeComponent();
            TaskCardViewModel tvm = new TaskCardViewModel();
            tvm.SelectedTask = task1;
            tvm.AssignedFrogger = task1.FrogAssigned;
            tvm.CreatedDateTime = task1.TaskDate;
            tvm.Description = task1.TaskDescrip;
            tvm.IsCompleted = task1.IsCompleted;
            tvm.IsGlobal = task1.IsGlobal;
            tvm.IsSaved = true;
            tvm.Priority = task1.TaskPriority;
            tvm.Title = task1.TaskTitle;
            this.DataContext = tvm;
        }

        private void ItemEdited(object sender, TextChangedEventArgs e)
        {

        }

        private void SetupArtClicked(object sender, EventArgs e)
        {
            var vm = (DesignSetupCardViewModel)this.DataContext;
            Utilities.ShowArtSetup(vm);
        }

        private void DeleteEnter(object sender, MouseEventArgs e)
        {
            Border rect = (Border)sender;
            rect.Opacity = 1.0;
        }

        private void DeleteLeave(object sender, MouseEventArgs e)
        {
            Border rect = (Border)sender;
            rect.Opacity = 0.7;
        }

        private void DeleteClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem elem = Utilities.FindParentByType<ListViewItem>((DependencyObject)sender);
            LineViewModel vm = (LineViewModel)elem.Content;
            ListView view = Utilities.FindParentByType<ListView>((DependencyObject)elem);
            IList<LineViewModel> list = (IList<LineViewModel>)view.ItemsSource;
            list.Remove(vm);
        }

        private async void DeleteCardClick(object sender, MouseButtonEventArgs e)
        {
            MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you would like to delete this task?");
            if (result == MessageDialogResult.Affirmative)
            {
                var vm = (TaskCardViewModel)TaskCardActual.DataContext;
                if (vm.SelectedTask != null)
                {
                    if (vm.SelectedTask.UniqueID > 0)
                    {
                        using (Entities ctx = new Entities())
                        {
                            task thistask = (from a in ctx.tasks where a.UniqueID == vm.SelectedTask.UniqueID select a).FirstOrDefault();
                            thistask.IsCompleted = true;
                            ctx.SaveChanges();
                        }
                    }
                }

                Expander exp = (Expander)this.Parent;
                StackPanel par = (StackPanel)exp.Parent;
                par.Children.Remove(exp);
            }

        }

        private void CardMouseLeft(object sender, MouseEventArgs e)
        {
            TaskCardViewModel vm = (TaskCardViewModel)this.DataContext;
            if (vm.SaveNeeded)
            {
                SaveChanges();
            }
        }

        private void DescripGotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tt = sender as TextBox;
            if (tt.Text == "Description")
            {
                tt.Clear();
                tt.Focus();
                Keyboard.Focus(tt);
            }
        }

        private void PriorityChanged(object sender, RoutedEventArgs e)
        {
            if (AllLoaded)
            {
                TaskCardViewModel vm = (TaskCardViewModel)this.DataContext;
                vm.SaveNeeded = true;

                int val = Convert.ToInt32(PriorityBox.EditValue);

                SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));

                if (val == 1) brush = new SolidColorBrush(Color.FromArgb(255, 51, 161, 51));
                if (val == 2) brush = new SolidColorBrush(Color.FromArgb(255, 255, 150, 0));
                if (val == 3) brush = new SolidColorBrush(Color.FromArgb(255, 255, 36, 0));

                var exp = Utilities.FindParentByType<Expander>((DependencyObject)TaskCardActual);

                exp.Background = brush;
                exp.BorderBrush = brush;
            }
        }

        private void AssingedFroggerChanged(object sender, RoutedEventArgs e)
        {
            if (AllLoaded)
            {
                TaskCardViewModel vm = (TaskCardViewModel)this.DataContext;
                vm.SaveNeeded = true;
            }
        }

        private void NoteChanged(object sender, TextChangedEventArgs e)
        {
            if (AllLoaded)
            {
                TaskCardViewModel vm = (TaskCardViewModel)this.DataContext;
                vm.SaveNeeded = true;
            }
        }

        public DoubleAnimation SlideDown(double toValue)
        {
            DoubleAnimation anim = new DoubleAnimation(toValue, TimeSpan.FromMilliseconds(300));
            QuadraticEase eas = new QuadraticEase();
            eas.EasingMode = EasingMode.EaseIn;
            anim.EasingFunction = eas;
            return anim;
        }

        public DoubleAnimation FadeOut(double toValue)
        {
            DoubleAnimation anim = new DoubleAnimation(toValue, TimeSpan.FromMilliseconds(700));
            return anim;
        }

        private async void SaveChanges()
        {
            TaskCardViewModel vm = (TaskCardViewModel)TaskCardActual.DataContext;

            if (vm.SelectedTask != null)
            {
                if (vm.SelectedTask.UniqueID > 0)
                {
                    using (Entities ctx = new Entities())
                    {
                        task thistask = (from a in ctx.tasks where a.UniqueID == vm.SelectedTask.UniqueID select a).FirstOrDefault();
                        thistask.TaskDescrip = vm.Description.ToString().LimitTo(499);
                        thistask.TaskPriority = Convert.ToInt32(PriorityBox.EditValue);
                        thistask.TaskTitle = vm.Title.ToString();
                        thistask.FrogAssigned = Convert.ToInt32(AssignedBox.EditValue);
                        thistask.IsGlobal = vm.IsGlobal;
                        try
                        {
                            ctx.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }
                }
            }
            else
            {
                MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
                using (Entities ctx = new Entities())
                {
                    task newTask = new task();
                    newTask.FrogCreated = mvm.LoggedInFrogger.FrogID;
                    newTask.TaskDate = DateTime.Now;
                    newTask.TaskDescrip = vm.Description.ToString();
                    if (PriorityBox.EditValue != null)
                    {
                        newTask.TaskPriority = Convert.ToInt32(PriorityBox.EditValue);
                    }
                    else
                    {
                        newTask.TaskPriority = 1;
                    }
                    newTask.TaskTitle = vm.Title.ToString();
                    newTask.IsGlobal = vm.IsGlobal;
                    newTask.Related_Ticket = "";
                    newTask.BackgroundColor = "";
                    newTask.FolderPath = "";
                    newTask.FrogAssigned = Convert.ToInt32(AssignedBox.EditValue);
                    try
                    {
                        if (!ctx.tasks.Any(x => x.TaskDescrip == vm.Description && x.TaskTitle == vm.Title))
                        {
                            ctx.tasks.Add(newTask);
                            ctx.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }
            }

            this.Dispatcher.Invoke(async () =>
                {
                    VisualBrush vb = new VisualBrush();
                    vb.Stretch = Stretch.Fill;
                    vb.Visual = FindResource("appbar_check") as Visual;
                    Rectangle ree = new Rectangle();
                    ree.Height = 20;
                    ree.Width = 20;
                    ree.Fill = Brushes.LightGreen;
                    ree.OpacityMask = vb;
                    SaveTile.Content = ree;

                    await Task.Delay(1000);
                    SaveTile.BeginAnimation(OpacityProperty, FadeOut(0.0));

                    // save stuff
                    await Task.Delay(800);
                    vm.SaveNeeded = false;
                    VisualBrush vb2 = new VisualBrush();
                    vb2.Stretch = Stretch.Fill;
                    vb2.Visual = FindResource("appbar_save") as Visual;
                    Rectangle ree2 = new Rectangle();
                    ree2.Height = 20;
                    ree2.Width = 20;
                    ree2.Fill = Brushes.White;
                    ree2.OpacityMask = vb2;
                    SaveTile.Content = ree2;
                    SaveTile.BeginAnimation(OpacityProperty, FadeOut(1.0));
                });
        }

        private void SaveClicked(object sender, EventArgs e)
        {
            SaveChanges();
        }

        bool AllLoaded;
        private void TCLoaded(object sender, RoutedEventArgs e)
        {
            AllLoaded = true;
        }

        private void Globalize(object sender, EventArgs e)
        {
            TaskCardViewModel vm = (TaskCardViewModel)TaskCardActual.DataContext;

            if (!vm.IsGlobal)
            {
                if (vm.SelectedTask != null)
                {
                    if (vm.SelectedTask.UniqueID > 0)
                    {
                        using (Entities ctx = new Entities())
                        {
                            task thistask = (from a in ctx.tasks where a.UniqueID == vm.SelectedTask.UniqueID select a).FirstOrDefault();
                            thistask.IsGlobal = true;
                            try
                            {
                                ctx.SaveChanges();
                                vm.IsGlobal = true;
                                vm.SaveNeeded = true;
                            }
                            catch (Exception ex)
                            {
                                new Error(ex);
                            }
                        }
                    }
                }
                else
                {
                    vm.IsGlobal = true;
                    vm.SaveNeeded = true;
                }
            }
            else
            {
                vm.IsGlobal = false;
                vm.SaveNeeded = true;
            }
        }

        private void TaskCardActual_MouseLeave(object sender, MouseEventArgs e)
        {

        }
    }
}
