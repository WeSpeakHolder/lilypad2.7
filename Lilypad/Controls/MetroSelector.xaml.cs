﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for MetroSelector.xaml
    /// </summary>
    public partial class MetroSelector : Tile
    {
        public MetroSelector()
        {
            InitializeComponent();
        }

        public MetroSelector(bool ischecked)
        {
            InitializeComponent();
            IsSelected = ischecked;
        }

        private void DeleteButtonDown(object sender, MouseButtonEventArgs e)
        {
        }

        private void PreorderStarLoaded(object sender, RoutedEventArgs e)
        {
        }

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        #region DependencyProperty Members

        public static DependencyProperty IsCheckedProperty = DependencyProperty.Register("IsSelected", typeof(bool),
            typeof(MetroSelector));

        public bool IsSelected
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        public static DependencyProperty LabelProperty = DependencyProperty.Register("Label", typeof(string),
            typeof(MetroSelector));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        #endregion DependencyProperty Members
    }
}