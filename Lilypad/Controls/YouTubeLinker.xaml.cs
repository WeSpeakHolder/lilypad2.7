﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.Controls.DesignCards;
using Lilypad.ViewModel;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class YouTubeLinker : UserControl
    {
        #region DependencyProperty Members

        public static DependencyProperty LinkProperty = DependencyProperty.Register("Link", typeof(string), typeof(YouTubeLinker));
        public string Link
        {
            get { return (string)GetValue(LinkProperty); }
            set { SetValue(LinkProperty, value); }
        }

        #endregion

        public YouTubeLinker()
        {
            InitializeComponent();
        }

        private void LaunchYouTubeVideo(object sender, MouseButtonEventArgs e)
        {
            string link = this.Link.ToString();
            Dialogs.YouTubePlayer player = new Dialogs.YouTubePlayer(link);
            Utilities.ShowYoutubeVideo(player);
        }
    }
}
