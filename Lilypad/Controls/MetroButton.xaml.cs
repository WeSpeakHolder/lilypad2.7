﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for SaveButton.xaml
    /// </summary>
    public partial class MetroButton : Tile
    {
        public MetroButton()
        {
            InitializeComponent();
        }

        private void LoadTooltip()
        {
            if (!string.IsNullOrEmpty(TooltipText))
            {
                var t = new Tooltip();
                t.Text = TooltipText;
                ToolTip = t;
            }
        }

        private void ButtonLoaded(object sender, RoutedEventArgs e)
        {
            LoadTooltip();
        }

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        #region DependencyProperty Members

        public static DependencyProperty CircleProperty = DependencyProperty.Register("Circle", typeof(bool),
            typeof(MetroButton));

        public bool Circle
        {
            get { return (bool)GetValue(CircleProperty); }
            set { SetValue(CircleProperty, value); }
        }

        public static DependencyProperty PortraitProperty = DependencyProperty.Register("Portrait", typeof(bool),
            typeof(MetroButton));

        public bool Portrait
        {
            get { return (bool)GetValue(PortraitProperty); }
            set { SetValue(PortraitProperty, value); }
        }

        public static DependencyProperty TooltipTextProperty = DependencyProperty.Register("TooltipText",
            typeof(string), typeof(MetroButton));

        public string TooltipText
        {
            get { return (string)GetValue(TooltipTextProperty); }
            set { SetValue(TooltipTextProperty, value); }
        }

        public static DependencyProperty IconNameProperty = DependencyProperty.Register("Icon", typeof(string),
            typeof(MetroButton));

        public string Icon
        {
            get { return (string)GetValue(IconNameProperty); }
            set { SetValue(IconNameProperty, value); }
        }

        public static DependencyProperty IconBGColorProperty = DependencyProperty.Register("IconBGColor", typeof(Brush),
            typeof(MetroButton));

        public Brush IconBGColor
        {
            get { return (Brush)GetValue(IconBGColorProperty); }
            set { SetValue(IconBGColorProperty, value); }
        }

        public static DependencyProperty IconColorProperty = DependencyProperty.Register("IconColor", typeof(Brush),
            typeof(MetroButton));

        public Brush IconColor
        {
            get { return (Brush)GetValue(IconColorProperty); }
            set { SetValue(IconColorProperty, value); }
        }

        #endregion DependencyProperty Members

    }
}