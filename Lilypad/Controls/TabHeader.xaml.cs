﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;
using Lilypad.ViewModel;

namespace Lilypad.Controls
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class TabHeader : UserControl, INotifyPropertyChanged
    {

        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public TabHeader()
        {

        }

        public TabHeader(string tag)
        {
            InitializeComponent();
            this.DataContext = this;
            Header = tag;
            isselected = false;
        }

        public TabHeader(string tag, bool isSelected)
        {
            InitializeComponent();
            this.DataContext = this;
            Header = tag;
            isselected = isSelected;
        }

        private string header;
        public string Header
        {
            get { return header; }
            set { header = value; OnPropertyChanged("Header"); }
        }

        private object selecteditem;
        public object SelectedItem
        {
            get { return selecteditem; }
            set { selecteditem = value; OnPropertyChanged("SelectedItem"); }
        }

        private bool isselected;
        public bool IsSelected
        {
            get { return isselected; }
            set { isselected = value; OnPropertyChanged("IsSelected"); }
        }

        private void selectClick(object sender, MouseButtonEventArgs e)
        {
            var parent = Utilities.FindParentByType<DevExpress.Xpf.LayoutControl.FlowLayoutControl>((DependencyObject)this); 

            if (parent != null)
            {
                foreach (TabHeader th in parent.Children.OfType<TabHeader>())
                {
                    th.IsSelected = false;
                }
            }

            if (!IsSelected)
            {
                IsSelected = true;
            }

        }


    }
}
