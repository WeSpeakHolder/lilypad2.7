﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.Model;
using Lilypad.ViewModel;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class TaskCardTwo : INotifyPropertyChanged
    {
        public TaskCardTwo()
        {
            AllLoaded = false;
            InitializeComponent();
        }

        public TaskCardTwo(task task1)
        {
            InitializeComponent();
            AllLoaded = false;
            SelectedTask = task1;
        }

        public static DependencyProperty SelectedTaskProperty = DependencyProperty.Register("SelectedTask", typeof(task),
    typeof(TaskCardTwo));

        public task SelectedTask
        {
            get { return (task)GetValue(SelectedTaskProperty); }
            set { SetValue(SelectedTaskProperty, value); }
        }

        public static DependencyProperty IsExpandedProperty = DependencyProperty.Register("IsExpanded", typeof(bool),
            typeof(TaskCardTwo));

        public bool IsExpanded
        {
            get { return (bool)GetValue(IsExpandedProperty); }
            set { SetValue(IsExpandedProperty, value); }
        }

        private void ItemEdited(object sender, TextChangedEventArgs e)
        {

        }

        private void SetupArtClicked(object sender, EventArgs e)
        {
            var vm = (DesignSetupCardViewModel)this.DataContext;
            Utilities.ShowArtSetup(vm);
        }

        private void DeleteEnter(object sender, MouseEventArgs e)
        {
            Border rect = (Border)sender;
            rect.Opacity = 1.0;
        }

        private void DeleteLeave(object sender, MouseEventArgs e)
        {
            Border rect = (Border)sender;
            rect.Opacity = 0.7;
        }

        private void DeleteClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem elem = Utilities.FindParentByType<ListViewItem>((DependencyObject)sender);
            LineViewModel vm = (LineViewModel)elem.Content;
            ListView view = Utilities.FindParentByType<ListView>((DependencyObject)elem);
            IList<LineViewModel> list = (IList<LineViewModel>)view.ItemsSource;
            list.Remove(vm);
        }

        private async void DeleteCardClick(object sender, MouseButtonEventArgs e)
        {
            MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you would like to delete this task?");
            if (result == MessageDialogResult.Affirmative)
            {
                var vm = (TaskCardViewModel)TaskCardActual.DataContext;
                if (vm.SelectedTask != null)
                {
                    if (vm.SelectedTask.UniqueID > 0)
                    {
                        using (Entities ctx = new Entities())
                        {
                            task thistask = (from a in ctx.tasks where a.UniqueID == vm.SelectedTask.UniqueID select a).FirstOrDefault();
                            thistask.IsCompleted = true;
                            ctx.SaveChanges();
                        }
                    }
                }

                Expander exp = (Expander)this.Parent;
                StackPanel par = (StackPanel)exp.Parent;
                par.Children.Remove(exp);
            }

        }

        private void CardMouseLeft(object sender, MouseEventArgs e)
        {
           if (SaveButton.Opacity == 1){ SaveChanges();}
        }

        private void DescripGotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tt = sender as TextBox;
            if (tt.Text == "Description")
            {
                tt.Clear();
                tt.Focus();
                Keyboard.Focus(tt);
            }
        }

        private void PriorityChanged(object sender, RoutedEventArgs e)
        {
            if (AllLoaded)
            {
                TaskCardViewModel vm = (TaskCardViewModel)this.DataContext;
                vm.SaveNeeded = true;

              //  int val = Convert.ToInt32(PriorityBox.EditValue);
                int val = 1;

                SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));

                if (val == 1) brush = new SolidColorBrush(Color.FromArgb(255, 51, 161, 51));
                if (val == 2) brush = new SolidColorBrush(Color.FromArgb(255, 255, 150, 0));
                if (val == 3) brush = new SolidColorBrush(Color.FromArgb(255, 255, 36, 0));

                var exp = Utilities.FindParentByType<Expander>((DependencyObject)TaskCardActual);

                exp.Background = brush;
                exp.BorderBrush = brush;
            }
        }

        private void AssingedFroggerChanged(object sender, RoutedEventArgs e)
        {
            if (AllLoaded)
            {
                TaskCardViewModel vm = (TaskCardViewModel)this.DataContext;
                vm.SaveNeeded = true;
            }
        }

        private void NoteChanged(object sender, TextChangedEventArgs e)
        {
            if (AllLoaded)
            {
                SaveButton.IsEnabled = true;
                SaveButton.OpacityAnimation(1, TimeSpan.FromMilliseconds(300));
            }
        }

        public DoubleAnimation SlideDown(double toValue)
        {
            DoubleAnimation anim = new DoubleAnimation(toValue, TimeSpan.FromMilliseconds(300));
            QuadraticEase eas = new QuadraticEase();
            eas.EasingMode = EasingMode.EaseIn;
            anim.EasingFunction = eas;
            return anim;
        }

        public DoubleAnimation FadeOut(double toValue)
        {
            DoubleAnimation anim = new DoubleAnimation(toValue, TimeSpan.FromMilliseconds(700));
            return anim;
        }

        private async void SaveAnimation()
        {
            SettingsPanel.OpacityAnimation(0, TimeSpan.FromMilliseconds(300));
            SettingsPanel.SetValue(Grid.ZIndexProperty, 0);
            SaveButton.Icon = "appbar_check";
            await Task.Delay(200);
            SaveButton.OpacityAnimation(0, TimeSpan.FromMilliseconds(300));
            await Task.Delay(300);
            SaveButton.Icon = "appbar_save";
            SaveButton.IsEnabled = false;
        }

        private void SaveChanges()
        {
            if (SelectedTask != null)
            {
                int id = SelectedTask.UniqueID;
                string td = SelectedTask.TaskDescrip.LimitTo(500);
                string tt = SelectedTask.TaskTitle;
                int pp = SelectedTask.TaskPriority;
                int ff = SelectedTask.FrogAssigned;
                bool ig = SelectedTask.IsGlobal;
                if (id > 0)
                {
                    using (Entities ctx = new Entities())
                    {
                        task thistask = ctx.tasks.FirstOrDefault(x => x.UniqueID == id);
                        if (thistask != null)
                        {
                            thistask.TaskDescrip = td;
                            thistask.TaskPriority = pp;
                            thistask.TaskTitle = tt;
                            thistask.FrogAssigned = ff;
                            thistask.IsGlobal = ig;
                            try
                            {
                                ctx.SaveChanges();
                                SaveAnimation();
                            }
                            catch (Exception ex)
                            {
                                new Error(ex);
                            }
                        }
                    }
                }
            }

        }

        private void SaveClicked(object sender, EventArgs e)
        {
            SaveChanges();
        }

        bool AllLoaded;
        private void TCLoaded(object sender, RoutedEventArgs e)
        {
            AllLoaded = true;
            if (IsExpanded) { TitleBody.Height = 200; ChevronRect.RotateToFrom(TimeSpan.FromMilliseconds(200), 0, 180); }
            else TitleBody.Height = 0;
            SetPriorityInitial();
            LoadGlobalInitial();
        }

        private void Globalize(object sender, EventArgs e)
        {
            TaskCardViewModel vm = (TaskCardViewModel)TaskCardActual.DataContext;

            if (!vm.IsGlobal)
            {
                if (vm.SelectedTask != null)
                {
                    if (vm.SelectedTask.UniqueID > 0)
                    {
                        using (Entities ctx = new Entities())
                        {
                            task thistask = (from a in ctx.tasks where a.UniqueID == vm.SelectedTask.UniqueID select a).FirstOrDefault();
                            thistask.IsGlobal = true;
                            try
                            {
                                ctx.SaveChanges();
                                vm.IsGlobal = true;
                                vm.SaveNeeded = true;
                            }
                            catch (Exception ex)
                            {
                                new Error(ex);
                            }
                        }
                    }
                }
                else
                {
                    vm.IsGlobal = true;
                    vm.SaveNeeded = true;
                }
            }
            else
            {
                vm.IsGlobal = false;
                vm.SaveNeeded = true;
            }
        }

        private void TaskCardActual_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void ExpandClicked(object sender, EventArgs e)
        {
            ExpandCollapse();
        }

        public void ExpandCollapse()
        {
            if (IsExpanded) Collapse();
            else Expand();
        }

        public void Expand()
        {
            if (!IsExpanded)
            {
                ChevronRect.RotateToFrom(TimeSpan.FromMilliseconds(200), 0, 180);
                IsExpanded = true;
                TitleBody.HeightAnimation(160, TimeSpan.FromMilliseconds(400));
                ModifyButton.IsEnabled = true;
                ModifyButton.OpacityAnimation(1, TimeSpan.FromSeconds(0.5));
                CompleteButton.IsEnabled = true;
                CompleteButton.OpacityAnimation(1, TimeSpan.FromSeconds(0.5));
            }
        }

        public void Collapse()
        {
            if (IsExpanded)
            {
                ChevronRect.RotateToFrom(TimeSpan.FromMilliseconds(200), 180, 0);
                IsExpanded = false;
                TitleBody.HeightAnimation(0, TimeSpan.FromMilliseconds(400));
                ModifyButton.IsEnabled = false;
                ModifyButton.OpacityAnimation(0, TimeSpan.FromSeconds(0.5));
                CompleteButton.IsEnabled = false;
                CompleteButton.OpacityAnimation(0, TimeSpan.FromSeconds(0.5));
            }
        }

        private void ChangePriorityClick(object sender, MouseButtonEventArgs e)
        {
            ChangeOrderPriority();
        }

        public void SetPriorityInitial()
        {
            if (SelectedTask != null && SelectedTask.TaskPriority > 0)
            {
                var priority = SelectedTask.TaskPriority;
                if (priority == 1)
                {
                    PriorityBar1.Background = Brushes.YellowGreen;
                    PriorityAbbrevLabel.Content = "S";
                    PriorityLabel.Content = "Standard";
                }
                else if (priority == 2)
                {
                    PriorityBar1.Background = Brushes.Orange;
                    PriorityAbbrevLabel.Content = "H";
                    PriorityLabel.Content = "High";
                }
                else
                {
                    PriorityBar1.Background = Brushes.Red;
                    PriorityAbbrevLabel.Content = "R";
                    PriorityLabel.Content = "Rush";
                }
            }
        }

        public void ChangeOrderPriority()
        {
            if (SelectedTask != null && SelectedTask.TaskPriority > 0)
            {
                var priority = SelectedTask.TaskPriority;

                if (priority == 1)
                {
                    SelectedTask.TaskPriority = 2;
                    PriorityBar1.Background = Brushes.Orange;
                    PriorityAbbrevLabel.Content = "H";
                    PriorityLabel.Content = "High";
                }
                else if (priority == 2)
                {
                    SelectedTask.TaskPriority = 3;
                    PriorityBar1.Background = Brushes.Red;
                    PriorityAbbrevLabel.Content = "R";
                    PriorityLabel.Content = "Rush";
                }
                else
                {
                    SelectedTask.TaskPriority = 1;
                    PriorityBar1.Background = Brushes.YellowGreen;
                    PriorityAbbrevLabel.Content = "S";
                    PriorityLabel.Content = "Standard";
                }
                OnPropertyChanged("SelectedTask");
                OnPropertyChanged("SelectedTask.TaskPriority");
                SaveButton.OpacityAnimation(1, TimeSpan.FromMilliseconds(300));
                SaveButton.IsEnabled = true;
            }
        }

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        private void AssignmentChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsLoaded)
            {
                SaveButton.OpacityAnimation(1, TimeSpan.FromMilliseconds(300));
                SaveButton.IsEnabled = true;
            }
        }

        private void ChangeGlobalClicked(object sender, EventArgs e)
        {
            if (IsLoaded)
            {
                if (AssignIcon.Icon == "appbar_people")
                {
                    SaveButton.OpacityAnimation(1, TimeSpan.FromMilliseconds(300));
                    SaveButton.IsEnabled = true;
                    AssignIcon.Icon = "appbar_globe_wire";
                    SelectedTask.IsGlobal = true;
                    EverybodyLabel.Opacity = 1;
                    StatusSelector.Opacity = 0;
                }
                else
                {
                    SaveButton.OpacityAnimation(1, TimeSpan.FromMilliseconds(300));
                    SaveButton.IsEnabled = true;
                    AssignIcon.Icon = "appbar_people";
                    SelectedTask.IsGlobal = false;
                    EverybodyLabel.Opacity = 0;
                    StatusSelector.Opacity = 1;
                }
            }
        }

        private void LoadGlobalInitial()
        {
            if (SelectedTask.IsGlobal)
            {
                AssignIcon.Icon = "appbar_globe_wire";
                SelectedTask.IsGlobal = true;
                EverybodyLabel.Opacity = 1;
                StatusSelector.Opacity = 0;
            }
            else
            {
                AssignIcon.Icon = "appbar_people";
                SelectedTask.IsGlobal = false;
                EverybodyLabel.Opacity = 0;
                StatusSelector.Opacity = 1;
            }
        }

        private void ModifyClicked(object sender, EventArgs e)
        {
            ModifyAnimation();
        }

        private async void ModifyAnimation()
        {
            if (SettingsPanel.Opacity == 1)
            {
                SettingsPanel.OpacityAnimation(0, TimeSpan.FromMilliseconds(500));
                await Task.Delay(500);
                SettingsPanel.SetValue(Grid.ZIndexProperty, -1);
            }
            else
            {
                SettingsPanel.OpacityAnimation(1, TimeSpan.FromMilliseconds(500));
                SettingsPanel.SetValue(Grid.ZIndexProperty, 50);
            }
        }

        private void CompleteClicked(object sender, EventArgs e)
        {
            CompleteConfirm();
        }

        private void CompleteConfirm()
        {
            if (CompleteConfirmPanel.Width == 0)
            {
                double w = SettingsPanel.ActualWidth;
                CompleteConfirmPanel.WidthAnimation(w, TimeSpan.FromMilliseconds(400));
                CompleteLabel.OpacityAnimation(1, TimeSpan.FromMilliseconds(600));
            }
            else
            {
                CompleteConfirmPanel.WidthAnimation(0, TimeSpan.FromMilliseconds(400));
                CompleteLabel.OpacityAnimation(0, TimeSpan.FromMilliseconds(300));
            }
        }

        private async void CompleteTask()
        {
            Collapse();
            await Task.Delay(500);
            this.OpacityAnimation(0, TimeSpan.FromMilliseconds(300));
            await Task.Delay(310);
            int id = SelectedTask.UniqueID;
            try
            {
                using (Entities ctx = new Entities())
                {
                    task t = ctx.tasks.FirstOrDefault(x => x.UniqueID == id);
                    t.IsCompleted = true;
                    t.TaskCompletedDate = DateTime.Now;
                    ctx.SaveChanges();
                }
                StackPanel parent = Utilities.FindParentByType<StackPanel>((DependencyObject) this);
                if (parent != null)
                {
                    parent.Children.Remove(this);
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private void CompleteYesClicked(object sender, RoutedEventArgs e)
        {
            CompleteTask();
        }

        private void CompleteNoClicked(object sender, RoutedEventArgs e)
        {
            CompleteConfirmPanel.WidthAnimation(0, TimeSpan.FromMilliseconds(400));
            CompleteLabel.OpacityAnimation(0, TimeSpan.FromMilliseconds(300));
        }
    }
}
