﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for SaveButton.xaml
    /// </summary>
    public partial class MetroLabelButton : Tile
    {
        public MetroLabelButton()
        {
            InitializeComponent();
        }



        private void ButtonLoaded(object sender, RoutedEventArgs e)
        {
            
        }

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        #region DependencyProperty Members


        public static DependencyProperty IconNameProperty = DependencyProperty.Register("Icon", typeof(string),
            typeof(MetroLabelButton));

        public string Icon
        {
            get { return (string)GetValue(IconNameProperty); }
            set { SetValue(IconNameProperty, value); }
        }

        public static DependencyProperty IconBGColorProperty = DependencyProperty.Register("IconBGColor", typeof(Brush),
            typeof(MetroLabelButton));

        public Brush IconBGColor
        {
            get { return (Brush)GetValue(IconBGColorProperty); }
            set { SetValue(IconBGColorProperty, value); }
        }

        public static DependencyProperty IconColorProperty = DependencyProperty.Register("IconColor", typeof(Brush),
            typeof(MetroLabelButton));

        public Brush IconColor
        {
            get { return (Brush)GetValue(IconColorProperty); }
            set { SetValue(IconColorProperty, value); }
        }

        public static DependencyProperty ButtonLabelProperty = DependencyProperty.Register("ButtonLabel", typeof(string),
    typeof(MetroLabelButton));

        public string ButtonLabel
        {
            get { return (string)GetValue(ButtonLabelProperty); }
            set { SetValue(ButtonLabelProperty, value); }
        }

        #endregion DependencyProperty Members

    }
}