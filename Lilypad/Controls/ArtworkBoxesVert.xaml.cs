﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for ArtworkBoxesVert.xaml
    /// </summary>
    public partial class ArtworkBoxesVert : UserControl
    {
        public ArtworkBoxesVert()
        {
            InitializeComponent();
        }

        public List<string> GetCheckedItemsAsList()
        {
            var output = new List<string>();
            foreach (var sp in ItemsSP.Children.OfType<StackPanel>())
            {
                foreach (var sp2 in sp.Children.OfType<StackPanel>())
                {
                    output.AddRange(from cb in sp2.Children.OfType<CheckBox>()
                                    where cb.IsChecked == true
                                    select cb.Content.ToString());
                }
            }
            return output;
        }

        public string[] GetCheckedItemsAsArray()
        {
            return
                ItemsSP.Children.OfType<CheckBox>()
                    .Where(x => x.IsChecked == true)
                    .Select(x => x.Content as string)
                    .ToArray();
        }

        private void ArtworkBoxes_OnLoaded(object sender, RoutedEventArgs e)
        {
        }
    }
}