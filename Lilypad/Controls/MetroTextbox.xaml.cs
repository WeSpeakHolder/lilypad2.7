﻿using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using MahApps.Metro.Controls;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for TabCard.xaml
    /// </summary>
    public partial class MetroTextbox : UserControl
    {
        public MetroTextbox()
        {
            IsNumeric = false;
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            RaiseTextChangedEvent();
        }

        private void CardEnter(object sender, MouseEventArgs e)
        {
        }

        private void CardLeave(object sender, MouseEventArgs e)
        {
        }

        private void TextboxLoaded(object sender, RoutedEventArgs e)
        {
            if (Foreground != null) Textbox.Foreground = Foreground;
            if (!string.IsNullOrEmpty(WatermarkLabel))
            {
                TextboxHelper.SetWatermark(Textbox, WatermarkLabel);
                MinWidth = 60;
            }
        }

        private void TextboxControlFocused(object sender, RoutedEventArgs e)
        {
            Textbox.Focus();
            Keyboard.Focus(Textbox);
        }

        private void Textbox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (IsNumeric)
            {
                e.Handled = IsAllowed(e.Text);
            }
            else
            {
                e.Handled = false;
            }
        }

        private static bool IsAllowed(string text)
        {
            var regex = new Regex("[^0-9.-]+"); //regex that matches only allowed numbers
            return regex.IsMatch(text);
        }

        private void TextboxMouseDown(object sender, MouseButtonEventArgs e)
        {
            Keyboard.Focus(Textbox);
        }

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        #region DependencyProperty Members

        public static DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string),
            typeof(MetroTextbox));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static DependencyProperty LabelProperty = DependencyProperty.Register("Label", typeof(string),
            typeof(MetroTextbox));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public static DependencyProperty FontSizeProperty = DependencyProperty.Register("FontSize", typeof(double),
            typeof(MetroTextbox));

        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public static DependencyProperty ShowCloseButtonProperty = DependencyProperty.Register("ShowCloseButton",
            typeof(bool), typeof(MetroTextbox));

        public bool ShowCloseButton
        {
            get { return (bool)GetValue(ShowCloseButtonProperty); }
            set { SetValue(ShowCloseButtonProperty, value); }
        }

        public static DependencyProperty WatermarkLabelProperty = DependencyProperty.Register("WatermarkLabel",
            typeof(string), typeof(MetroTextbox));

        public string WatermarkLabel
        {
            get { return (string)GetValue(WatermarkLabelProperty); }
            set { SetValue(WatermarkLabelProperty, value); }
        }

        public static DependencyProperty IsNumericProperty = DependencyProperty.Register("IsNumeric",
            typeof(bool), typeof(MetroTextbox));

        public bool IsNumeric
        {
            get { return (bool)GetValue(IsNumericProperty); }
            set { SetValue(IsNumericProperty, value); }
        }

        public static DependencyProperty ForegroundProperty = DependencyProperty.Register("Foreground",
            typeof(SolidColorBrush), typeof(MetroTextbox));

        public SolidColorBrush Foreground
        {
            get { return (SolidColorBrush)GetValue(ForegroundProperty); }
            set { SetValue(ForegroundProperty, value); }
        }

        public static readonly RoutedEvent TextChangedEvent = EventManager.RegisterRoutedEvent("TextChanged",
            RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(MetroTextbox));

        public event RoutedEventHandler TextChanged
        {
            add { AddHandler(TextChangedEvent, value); }
            remove { RemoveHandler(TextChangedEvent, value); }
        }

        private void RaiseTextChangedEvent()
        {
            var newArgs = new RoutedEventArgs(TextChangedEvent);
            RaiseEvent(newArgs);
        }

        #endregion DependencyProperty Members
    }
}