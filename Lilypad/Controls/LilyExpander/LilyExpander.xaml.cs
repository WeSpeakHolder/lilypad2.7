﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.Controls.DesignCards;
using Lilypad.ViewModel;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls.LilyExpander
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class LilyExpander : System.Windows.Controls.Expander
    {
        public LilyExpander()
        {
            InitializeComponent();
        }

        public LilyExpander(DesignSetupCardViewModel vm)
        {
            InitializeComponent();
            string name = "SetupCard" + vm.DesignNum;
            Expander thisCard = (Expander)this;
            MainWindow main = (MainWindow)Application.Current.MainWindow;
            main.RegisterName(name, thisCard);
            MainViewModel mvm = (MainViewModel)main.DataContext;
            mvm.DesignCardList.Add(name);
            this.DataContext = vm;
        }

        private void ItemEdited(object sender, TextChangedEventArgs e)
        {

        }

        private void SetupArtClicked(object sender, EventArgs e)
        {
            var vm = (DesignSetupCardViewModel)this.DataContext;
            Utilities.ShowArtSetup(vm);
        }
    }
}
