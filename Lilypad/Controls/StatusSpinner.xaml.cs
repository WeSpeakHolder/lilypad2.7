﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.Controls.DesignCards;
using Lilypad.ViewModel;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class StatusSpinner : UserControl
    {
        #region DependencyProperty Members

        public static DependencyProperty IsBusyProperty = DependencyProperty.Register("IsBusy", typeof(bool), typeof(StatusSpinner));
        public bool IsBusy
        {
            get { return (bool)GetValue(IsBusyProperty); }
            set { SetValue(IsBusyProperty, value); }
        }

        public static DependencyProperty IsDoneProperty = DependencyProperty.Register("IsDone", typeof(bool), typeof(StatusSpinner));
        public bool IsDone
        {
            get { return (bool)GetValue(IsDoneProperty); }
            set { SetValue(IsDoneProperty, value); }
        }

        public static DependencyProperty IsErrorProperty = DependencyProperty.Register("IsError", typeof(bool), typeof(StatusSpinner));
        public bool IsError
        {
            get { return (bool)GetValue(IsErrorProperty); }
            set { SetValue(IsErrorProperty, value); }
        }

        public static DependencyProperty IsNoticeProperty = DependencyProperty.Register("IsNotice", typeof(bool), typeof(StatusSpinner));
        public bool IsNotice
        {
            get { return (bool)GetValue(IsNoticeProperty); }
            set { SetValue(IsNoticeProperty, value); }
        }


        public static DependencyProperty SizeProperty = DependencyProperty.Register("IconSize", typeof(double), typeof(StatusSpinner));
        public double IconSize
        {
            get { return (double)GetValue(SizeProperty); }
            set { SetValue(SizeProperty, value); }
        }

        #endregion

        public StatusSpinner()
        {
            InitializeComponent();
        }
    }
}
