using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using System.Windows.Navigation;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.WindowsUI;

namespace Lilypad.Controls
{
    public class LilyNav : TransitioningContentControl, INavigationSystem, INotifyPropertyChanged
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public LilyNav()
        {
            
        }

        private NavigationService navservice;
        public NavigationService NavService
        {
            get { return navservice; }
            set { navservice = value; OnPropertyChanged("NavService"); }
        }

        public void GoBack()
        {
            if (NavService.CanGoBack)
            {
                NavService.GoBack();
            }
        }

    }

    public interface INavigationSystem
    {
        NavigationService NavService { get; set; }
    }
}