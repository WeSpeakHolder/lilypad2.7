﻿using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Lilypad.Model;
using Lilypad.ViewModel;
using MahApps.Metro.Controls.Dialogs;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for LineItemLookup.xaml
    /// </summary>
    public partial class LineItemLookup : UserControl
    {
        public LineItemLookup()
        {
            InitializeComponent();
        }

        public bool IsLoadedFully { get; set; }

        private void LineItemLookupLoaded(object sender, RoutedEventArgs e)
        {
            this.OpacityAnimation(1.0, TimeSpan.FromMilliseconds(400));
            if (!Utilities.IsInDesignTime) LoadBrands();
            ButtonEditorSP.Visibility = Visibility.Visible;
            if (EstCost != null && EstCost.Value > 0.00m)
            {
                HasEstCost = true;
                SideEditorSP.Width = 65;
                ButtonEditorSP.Visibility = Visibility.Collapsed;
            }
            if (SelectedPreorderItem != null && SelectedPreorderItem.UniqueID > 0)
            {
                DeleteButtonEditorSP.Visibility = Visibility.Visible;
            }
        }

        private void LoadBrands()
        {
            var mvm = (MainViewModel)Application.Current.MainWindow.DataContext;

            if (mvm.AlphaItemBrands.Count == 0 || mvm.AlphaItemColors.Count == 0 || mvm.AlphaItemCodes.Count == 0)
                using (var ctx = new Entities())
                {
                    mvm.AlphaItemBrands =
                         ctx.alphaitems.Select(x => x.Mill_Name).Distinct().OrderBy(x => x).ToList();
                    mvm.AlphaItemColors =
                         ctx.alphaitems.Select(x => x.Color_Name).Distinct().OrderBy(x => x).ToList();
                    mvm.AlphaItemCodes =
                         ctx.alphaitems.Select(x => x.Style_Code).Distinct().OrderBy(x => x).ToList();
                }
            BrandEditor.ItemsSource = mvm.AlphaItemBrands;
            StockCodeEditor.ItemsSource = mvm.AlphaItemCodes;
            StockColorEditor.ItemsSource = mvm.AlphaItemColorsCase;
        }

        private void LineItemLoaded(object sender, RoutedEventArgs e)
        {
            IsLoadedFully = true;
        }

        private void LineItemActual_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        private async void DeleteClicked(object sender, MouseButtonEventArgs e)
        {
            var res = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you wish to delete this line item?");
            if (res == MessageDialogResult.Affirmative)
            {
                var thisC = Utilities.FindParentByType<StackPanel>(this);
                thisC.Children.Remove(this);
                using (var ctx = new Entities())
                {
                    var pi = ctx.preorderitems.FirstOrDefault(x => x.UniqueID == SelectedPreorderItem.UniqueID);
                    ctx.preorderitems.Remove(pi);
                    ctx.SaveChanges();
                }
            }
        }

        private void ButtonClicked(object sender, MouseButtonEventArgs e)
        {
            if (SideEditorSP.Width == 65)
            {
                SideEditorSP.WidthAnimation(0, TimeSpan.FromSeconds(0.5));
                ButtonEditorVisual.Visual = (Visual)FindResource("appbar_arrow_right");
            }
            else
            {
                SideEditorSP.WidthAnimation(65, TimeSpan.FromSeconds(0.5));
                ButtonEditorVisual.Visual = (Visual)FindResource("appbar_arrow_left");
            }
        }

        public void ResetFields()
        {
            Quantity = null;
            Manufacturer = string.Empty;
            StockID = string.Empty;
            Color = string.Empty;
            Size = string.Empty;
            EstCost = null;
        }

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        #region DependencyProperty Members

        public static DependencyProperty SelectedPreorderItemProperty =
            DependencyProperty.Register("SelectedPreorderItem", typeof(preorderitem), typeof(LineItemLookup));

        public preorderitem SelectedPreorderItem
        {
            get { return (preorderitem)GetValue(SelectedPreorderItemProperty); }
            set { SetValue(SelectedPreorderItemProperty, value); }
        }

        public static DependencyProperty QuantityProperty = DependencyProperty.Register("Quantity", typeof(int?),
            typeof(LineItemLookup));

        public int? Quantity
        {
            get { return (int?)GetValue(QuantityProperty); }
            set { SetValue(QuantityProperty, value); }
        }

        public static DependencyProperty ManufacturerProperty = DependencyProperty.Register("Manufacturer",
            typeof(string), typeof(LineItemLookup));

        public string Manufacturer
        {
            get { return (string)GetValue(ManufacturerProperty); }
            set { SetValue(ManufacturerProperty, value); }
        }

        public static DependencyProperty StockIDProperty = DependencyProperty.Register("StockID", typeof(string),
            typeof(LineItemLookup));

        public string StockID
        {
            get { return (string)GetValue(StockIDProperty); }
            set { SetValue(StockIDProperty, value); }
        }

        public static DependencyProperty ColorProperty = DependencyProperty.Register("Color", typeof(string),
            typeof(LineItemLookup));

        public string Color
        {
            get { return (string)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public static DependencyProperty SizeProperty = DependencyProperty.Register("Size", typeof(string),
            typeof(LineItemLookup));

        public string Size
        {
            get { return (string)GetValue(SizeProperty); }
            set { SetValue(SizeProperty, value); }
        }

        public static DependencyProperty EstCostProperty = DependencyProperty.Register("EstCost", typeof(decimal?),
            typeof(LineItemLookup));

        public decimal? EstCost
        {
            get { return (decimal?)GetValue(EstCostProperty); }
            set { SetValue(EstCostProperty, value); }
        }

        public static DependencyProperty HasEstCostProperty = DependencyProperty.Register("HasEstCost", typeof(bool),
            typeof(LineItemLookup));

        public bool HasEstCost
        {
            get { return (bool)GetValue(HasEstCostProperty); }
            set { SetValue(HasEstCostProperty, value); }
        }

        #endregion DependencyProperty Members
    }
}