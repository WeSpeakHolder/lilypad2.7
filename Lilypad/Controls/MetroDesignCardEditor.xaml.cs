﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Lilypad.Model;
using Lilypad.View;
using Lilypad.ViewModel;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Ookii.Dialogs.Wpf;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for LineCard.xaml
    /// </summary>
    public partial class MetroDesignCardEditor : Window
    {
        public MetroDesignCardEditor()
        {
            InitializeComponent();
            fullyLoaded = false;
            LoadLocalProperties();
        }

        public MetroDesignCardEditor(MetroDesignCard input)
        {
                InitializeComponent();
                SelectedDesignCard = input;
                LoadLocalProperties();
        }

        private void LoadLocalProperties()
        {
            ExistingArtworkItems = new ObservableCollection<MetroDesignFile>();
            ExistingArtworkItems.CollectionChanged += ExistingArtworkItems_CollectionChanged;
            CurrentArtworkItems = new ObservableCollection<string>();
            CurrentArtworkItems.CollectionChanged += CurrentArtworkItems_CollectionChanged;
            ArtworkItemsToAdd = new ObservableCollection<string>();
            ArtworkItemsToAdd.CollectionChanged += ArtworkItemsToAdd_CollectionChanged;
            ArtworkItemsToRemove = new ObservableCollection<string>();
            ArtworkItemsToRemove.CollectionChanged += ArtworkItemsToRemove_CollectionChanged;
            LineItems = new ObservableCollection<LineViewModel>();
            LineItems.CollectionChanged += LineItems_CollectionChanged;
            OnPropertyChanged("SelectedDesignDetail");
        }

        private void ArtworkItemsToRemove_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        private void ArtworkItemsToAdd_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        private void LineItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        private void CurrentArtworkItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        private void ExistingArtworkItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        private void SetupArtClicked(object sender, EventArgs e)
        {
            var vm = (DesignSetupCardViewModel)DataContext;
            //   Utilities.ShowArtSetup(vm);
        }

        private void DeleteClick(object sender, MouseButtonEventArgs e)
        {
            var elem = Utilities.FindParentByType<ListViewItem>((DependencyObject)sender);
            var vm = (LineViewModel)elem.Content;
            var view = Utilities.FindParentByType<ListView>(elem);
            var list = (IList<LineViewModel>)view.ItemsSource;
            list.Remove(vm);
        }

        private void DeleteCardClick(object sender, MouseButtonEventArgs e)
        {
        }

        private void DesignCardActual_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        private void DesignCardLoaded(object sender, RoutedEventArgs e)
        {
            //    vm.SelectedDesignDetail = SelectedDesignDetail;
            //    this.DataContext = vm;
            //    DesignCardActual.DataContext = vm;

            fullyLoaded = true;
            var dir = "";
            if (SelectedDesignDetail != null)
            {
                if (!string.IsNullOrEmpty(SelectedDesignDetail.DesignFolderPath))
                {
                    dir = SelectedDesignDetail.DesignFolderPath;
                 //   if (Directory.Exists(dir)) StartFileSystemWatcher(dir);
                }
            }

            Activate();
        }

        private void DeleteMouseOver(object sender, MouseEventArgs e)
        {
        }

        private void DeleteMouseout(object sender, MouseEventArgs e)
        {
        }


        private void SaveChangesClicked(object sender, EventArgs e)
        {
            SaveChanges();
        }

        private void SaveChanges()
        {
            var vm = (DesignSetupCardViewModel)DesignEditorActual.DataContext;
            if (vm.NeedsSave)
            {
                vm.SaveChanges();
                vm.NeedsSave = false;
            }
        }

        public void AddToNewItemsList(string input)
        {
            CurrentArtworkItems.Add(input);
        }

        private async void SaveClicked(object sender, EventArgs e)
        {
            AddNewDesign();
            await FadeOut();
            DialogResult = true;
        }

        private async void CancelClicked(object sender, EventArgs e)
        {
            await FadeOut();
            DialogResult = false;
        }

        private void DesignEditorLoaded(object sender, RoutedEventArgs e)
        {
            if (SelectedDesignCard != null && SelectedDesignCard.SelectedDesignDetail != null)
            {
                var c = SelectedDesignCard.SelectedDesignDetail;
                var dd = new designdetail();

                dd.DesignID = c.DesignID;
                dd.DesignNum = c.DesignNum;
                dd.DesignDescription = c.DesignDescription;
                dd.DesignFolderPath = c.DesignFolderPath;
                dd.ReceiptNum = c.ReceiptNum;
                dd.PrintProcess = c.PrintProcess;
                dd.AP_YOUTH = c.AP_YOUTH;
                dd.SizeToGarment = c.SizeToGarment;
                SelectedDesignDetail = dd;
                ArtPrintSelector.Content = c.SizeToGarment ? "SIZE TO ITEM" : "PRINT AS-IS";
                platenButton.NumLabel = c.AP_YOUTH ? "S" : "L";

                List<string> nl = new List<string>();
                foreach (MetroDesignFile file in ExistingArtworkItems)
                {
                    nl.Add(file.FileTitle);
                }
                CurrentArtworkItems = new ObservableCollection<string>(nl);
                OnPropertyChanged("CurrentArtworkItems");
            }
        }

        private void PlatenClick(object sender, EventArgs e)
        {
            platenButton.NumLabel = platenButton.NumLabel == "L" ? "S" : "L";
            SelectedDesignDetail.AP_YOUTH = platenButton.NumLabel == "S" ? true : false;
        }

        private void ChangeArtPrint(object sender, MouseButtonEventArgs e)
        {
            ArtPrintSelector.Content = ArtPrintSelector.Content == "PRINT AS-IS" ? "SIZE TO ITEM" : "PRINT AS-IS";
            SelectedDesignDetail.PrintToFile = ArtPrintSelector.Content == "PRINT AS-IS" ? true : false;
            SelectedDesignDetail.SizeToGarment = ArtPrintSelector.Content == "SIZE TO ITEM" ? true : false;
        }

        private void EditPathClicked(object sender, EventArgs e)
        {
            var sp = Utilities.FindParentByType<StackPanel>(((DependencyObject)sender));
            var tb = sp.FindChildren<TextBox>(true).First();
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.SelectedPath))
            {
                tb.Text = dialog.SelectedPath;
            }
        }

        private void MainGridMouseDown(object sender, MouseButtonEventArgs e)
        {
            while (e.LeftButton == MouseButtonState.Pressed && e.RightButton == MouseButtonState.Released &&
                   e.ClickCount > 1)
            {
                DragMove();
            }
        }

        private void DescripKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }

        private async Task<bool> FadeIn()
        {
            MainBG.OpacityAnimation(1, TimeSpan.FromSeconds(0.3));
            await Task.Delay(100);
            BlurBG.OpacityAnimation(0.6, TimeSpan.FromSeconds(0.3));
            await Task.Delay(300);
            return true;
        }

        private async Task<bool> FadeOut()
        {
            BlurBG.OpacityAnimation(0, TimeSpan.FromSeconds(0.2));
            await Task.Delay(100);
            MainBG.OpacityAnimation(0, TimeSpan.FromSeconds(0.3));
            await Task.Delay(300);
            return true;
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            FadeIn();
        }

        private void WindowClosing(object sender, CancelEventArgs e)
        {
        }

        private void MarkForDeletionClicked(object sender, RoutedEventArgs e)
        {
            var list = FilesContainer.SelectedItems;
            if (list.Count > 0)
            {
                foreach (string item in list)
                {
                    if (!ArtworkItemsToRemove.Any(x => x == item)) ArtworkItemsToRemove.Add(item);
                    var lvi = (ListViewItem)FilesContainer.ItemContainerGenerator.ContainerFromItem(item);
                    var cp = VisualChildHelper.FindVisualChild<ContentPresenter>(lvi);
                    var mdf = VisualChildHelper.FindVisualChild<MetroDesignFile>(cp);
                    mdf.FileMarkedForDeletion = true;
                }
                FilesContainer.SelectedItem = null;
                ShowFileModifyNotice();
            }
        }


        #region VisualChildHelper

        public class VisualChildHelper
        {
            public static childItem FindVisualChild<childItem>(DependencyObject obj)
                where childItem : DependencyObject
            {
                for (var i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
                {
                    var child = VisualTreeHelper.GetChild(obj, i);
                    if (child != null && child is childItem)
                        return (childItem)child;
                    var childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
                return null;
            }
        }

        #endregion

        private void ShowFileModifyNotice()
        {
            ItemsNotice.OpacityAnimation(0.5, TimeSpan.FromSeconds(1));
        }

        private async void AddNewDesign()
        {
            if (NewArtworkBoxes.GetCheckedItemsAsList() != null)
            {
                foreach (var item in NewArtworkBoxes.GetCheckedItemsAsList())
                {
                    if (!ArtworkItemsToAdd.Any(x => x == item)) ArtworkItemsToAdd.Add(item);
                    if (!CurrentArtworkItems.Any(x => x == item))
                    {
                        CurrentArtworkItems.Add(item);
                        OnPropertyChanged("CurrentArtworkItems");
                        await Task.Delay(100);
                        ShowFileModifyNotice();
                        var lvi = (ListViewItem)FilesContainer.ItemContainerGenerator.ContainerFromItem(item);
                        var cp = VisualChildHelper.FindVisualChild<ContentPresenter>(lvi);
                        var mdf = VisualChildHelper.FindVisualChild<MetroDesignFile>(cp);
                        mdf.FileIsNew = true;
                    }
                }

                FilesContainer.SelectedItem = null;
            }
        }

        private async void AddNewDesignClicked(object sender, RoutedEventArgs e)
        {
            if (NewArtworkBoxes.Opacity == 0)
            {
                NewArtworkBoxes.DownwardSlide(TimeSpan.FromSeconds(0.3));
                NewArtworkBoxes.OpacityAnimation(0.5, TimeSpan.FromSeconds(0.5));
            }
            else
            {
                AddNewDesign();
            }
        }

        #region FileSystemWatcher

        public FileSystemWatcher fileSystemWatcher;
        // ----------------------------------------------------------------------------------
        // Events that do all the monitoring
        // ----------------------------------------------------------------------------------

        public bool FileSystemWatcherActivated;
        private bool fullyLoaded;

        // File watching system
        private void StartFileSystemWatcher(string directory)
        {
            var folderPath = directory;

            if (Directory.Exists(folderPath))
            {
                // If there is no folder selected, to nothing
                if (string.IsNullOrWhiteSpace(folderPath))
                    return;

                fileSystemWatcher = new FileSystemWatcher();

                // Set folder path to watch
                fileSystemWatcher.Path = folderPath;

                // Gets or sets the type of changes to watch for.
                // In this case we will watch change of filename, last modified time, size and directory name
                fileSystemWatcher.NotifyFilter = NotifyFilters.FileName |
                                                 NotifyFilters.LastWrite |
                                                 NotifyFilters.Size |
                                                 NotifyFilters.DirectoryName;

                // Event handlers that are watching for specific event
                fileSystemWatcher.Created += fileSystemWatcher_Created;
                fileSystemWatcher.Changed += fileSystemWatcher_Changed;
                fileSystemWatcher.Deleted += fileSystemWatcher_Deleted;
                fileSystemWatcher.Renamed += fileSystemWatcher_Renamed;

                // NOTE: If you want to monitor specified files in folder, you can use this filter
                // fileSystemWatcher.Filter

                // START watching
                fileSystemWatcher.EnableRaisingEvents = true;
                FileSystemWatcherActivated = true;
            }
        }

        private void fileSystemWatcher_Created(object sender, FileSystemEventArgs e)
        {
            if (FileSystemWatcherActivated)
                Dispatcher.Invoke(() =>
                {
                    var vm = (DesignSetupCardViewModel)DataContext;
                 //   vm.GetArtwork();
                });
        }

        private void fileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (FileSystemWatcherActivated)
                Dispatcher.Invoke(() =>
                {
                    var vm = (DesignSetupCardViewModel)DataContext;
                 //   vm.GetArtwork();
                });
        }

        private void fileSystemWatcher_Deleted(object sender, FileSystemEventArgs e)
        {
            if (FileSystemWatcherActivated)
                Dispatcher.Invoke(() =>
                {
                    var vm = (DesignSetupCardViewModel)DataContext;
                 //   vm.GetArtwork();
                    vm.FlaggedArtworkItems.Clear();
                });
        }

        private void fileSystemWatcher_Renamed(object sender, RenamedEventArgs e)
        {
            if (FileSystemWatcherActivated)
                Dispatcher.Invoke(() =>
                {
                    var vm = (DesignSetupCardViewModel)DataContext;
                //    vm.GetArtwork();
                });
        }

        #endregion

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        #region DependencyProperty Members

        public static DependencyProperty SelectedDesignDetailProperty =
            DependencyProperty.Register("SelectedDesignDetail", typeof(designdetail), typeof(MetroDesignCardEditor));

        public designdetail SelectedDesignDetail
        {
            get { return (designdetail)GetValue(SelectedDesignDetailProperty); }
            set { SetValue(SelectedDesignDetailProperty, value); }
        }

        public static DependencyProperty SelectedDesignCardProperty = DependencyProperty.Register("SelectedDesignCard",
            typeof(MetroDesignCard), typeof(MetroDesignCardEditor));

        public MetroDesignCard SelectedDesignCard
        {
            get { return (MetroDesignCard)GetValue(SelectedDesignCardProperty); }
            set { SetValue(SelectedDesignCardProperty, value); }
        }

        public static DependencyProperty ExistingArtworkItemsProperty =
            DependencyProperty.Register("ExistingArtworkItems", typeof(ObservableCollection<MetroDesignFile>),
                typeof(MetroDesignCardEditor));

        public ObservableCollection<MetroDesignFile> ExistingArtworkItems
        {
            get { return (ObservableCollection<MetroDesignFile>)GetValue(ExistingArtworkItemsProperty); }
            set { SetValue(ExistingArtworkItemsProperty, value); }
        }

        public static DependencyProperty CurrentArtworkItemsProperty = DependencyProperty.Register(
            "CurrentArtworkItems", typeof(ObservableCollection<string>), typeof(MetroDesignCardEditor));

        public ObservableCollection<string> CurrentArtworkItems
        {
            get { return (ObservableCollection<string>)GetValue(CurrentArtworkItemsProperty); }
            set { SetValue(CurrentArtworkItemsProperty, value); }
        }

        public static DependencyProperty ArtworkItemsToAddProperty = DependencyProperty.Register("ArtworkItemsToAdd",
            typeof(ObservableCollection<string>), typeof(MetroDesignCardEditor));

        public ObservableCollection<string> ArtworkItemsToAdd
        {
            get { return (ObservableCollection<string>)GetValue(ArtworkItemsToAddProperty); }
            set { SetValue(ArtworkItemsToAddProperty, value); }
        }

        public static DependencyProperty ArtworkItemsToRemoveProperty =
            DependencyProperty.Register("ArtworkItemsToRemove", typeof(ObservableCollection<string>),
                typeof(MetroDesignCardEditor));

        public ObservableCollection<string> ArtworkItemsToRemove
        {
            get { return (ObservableCollection<string>)GetValue(ArtworkItemsToRemoveProperty); }
            set { SetValue(ArtworkItemsToRemoveProperty, value); }
        }

        public static DependencyProperty IsNewDesignProperty = DependencyProperty.Register("IsNewDesign", typeof(bool),
            typeof(MetroDesignCardEditor));

        public bool IsNewDesign
        {
            get { return (bool)GetValue(IsNewDesignProperty); }
            set { SetValue(IsNewDesignProperty, value); }
        }

        public static DependencyProperty ProcessesListProperty = DependencyProperty.Register("ProcessesList",
            typeof(ObservableCollection<printprocess>), typeof(MetroDesignCardEditor));

        public ObservableCollection<printprocess> ProcessesList
        {
            get { return (ObservableCollection<printprocess>)GetValue(ProcessesListProperty); }
            set { SetValue(ProcessesListProperty, value); }
        }

        public static DependencyProperty LineItemsProperty = DependencyProperty.Register("LineItems",
            typeof(ObservableCollection<LineViewModel>), typeof(MetroDesignCardEditor));

        public ObservableCollection<LineViewModel> LineItems
        {
            get { return (ObservableCollection<LineViewModel>)GetValue(LineItemsProperty); }
            set { SetValue(LineItemsProperty, value); }
        }

        #endregion DependencyProperty Members
    }
}