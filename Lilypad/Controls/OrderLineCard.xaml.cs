﻿using System;
using System.Globalization;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.ViewModel;
using Lilypad.Controls.DesignCards;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

   public partial class OrderLineCard : UserControl
    {

        #region DependencyProperty Members

        public static DependencyProperty UniqueIDProperty = DependencyProperty.Register("UniqueID", typeof(int), typeof(OrderLineCard));
        public int UniqueID
        {
            get { return (int)GetValue(UniqueIDProperty); }
            set { SetValue(UniqueIDProperty, value); }
        }

        public static DependencyProperty ReceiptNumProperty = DependencyProperty.Register("ReceiptNumber", typeof(int), typeof(OrderLineCard));
        public int ReceiptNumber
        {
            get { return (int)GetValue(ReceiptNumProperty); }
            set { SetValue(ReceiptNumProperty, value); }
        }

        public static DependencyProperty PriorityProperty = DependencyProperty.Register("Priority", typeof(int), typeof(OrderLineCard));
        public int Priority
        {
            get { return (int)GetValue(PriorityProperty); }
            set { SetValue(PriorityProperty, value); }
        }

        public static DependencyProperty PrintProcessProperty = DependencyProperty.Register("PrintProcess", typeof(int), typeof(OrderLineCard));
        public int PrintProcess
        {
            get { return (int)GetValue(PrintProcessProperty); }
            set { SetValue(PrintProcessProperty, value); }
        }

        public static DependencyProperty CustomerIDProperty = DependencyProperty.Register("CustomerID", typeof(int), typeof(OrderLineCard));
        public int CustomerID
        {
            get { return (int)GetValue(CustomerIDProperty); }
            set { SetValue(CustomerIDProperty, value); }
        }

        public static DependencyProperty StatusIDProperty = DependencyProperty.Register("StatusID", typeof(int), typeof(OrderLineCard));
        public int StatusID
        {
            get { return (int)GetValue(StatusIDProperty); }
            set { SetValue(StatusIDProperty, value); }
        }

        public static DependencyProperty FroggerIDProperty = DependencyProperty.Register("FroggerID", typeof(int), typeof(OrderLineCard));
        public int FroggerID
        {
            get { return (int)GetValue(FroggerIDProperty); }
            set { SetValue(FroggerIDProperty, value); }
        }

        public static DependencyProperty FirstNameProperty = DependencyProperty.Register("CustomerFirstName", typeof(string), typeof(OrderLineCard));
        public string CustomerFirstName
        {
            get { return (string)GetValue(FirstNameProperty); }
            set { SetValue(FirstNameProperty, value); }
        }

        public static DependencyProperty LastNameProperty = DependencyProperty.Register("CustomerLastName", typeof(string), typeof(OrderLineCard));
        public string CustomerLastName
        {
            get { return (string)GetValue(LastNameProperty); }
            set { SetValue(LastNameProperty, value); }
        }

        public static DependencyProperty CompanyProperty = DependencyProperty.Register("CustomerCompany", typeof(string), typeof(OrderLineCard));
        public string CustomerCompany
        {
            get { return (string)GetValue(CompanyProperty); }
            set { SetValue(CompanyProperty, value); }
        }

        public static DependencyProperty CommonNameProperty = DependencyProperty.Register("CustomerCommonName", typeof(string), typeof(OrderLineCard));
        public string CustomerCommonName
        {
            get { return (string)GetValue(CommonNameProperty); }
            set { SetValue(CommonNameProperty, value); }
        }


        public static DependencyProperty StatusTextProperty = DependencyProperty.Register("StatusText", typeof(string), typeof(OrderLineCard));
        public string StatusText
        {
            get { return (string)GetValue(StatusTextProperty); }
            set { SetValue(StatusTextProperty, value); }
        }

        public static DependencyProperty FlexibleDeadlineProperty = DependencyProperty.Register("IsFlexibleDeadline", typeof(bool), typeof(OrderLineCard));
        public bool IsFlexibleDeadline
        {
            get { return (bool)GetValue(FlexibleDeadlineProperty); }
            set { SetValue(FlexibleDeadlineProperty, value); }
        }

        public static DependencyProperty DueDateTimeProperty = DependencyProperty.Register("DueDateTime", typeof(DateTime), typeof(OrderLineCard));
        public DateTime DueDateTime
        {
            get { return (DateTime)GetValue(DueDateTimeProperty); }
            set { SetValue(DueDateTimeProperty, value); }
        }

        public static DependencyProperty DueDateTextProperty = DependencyProperty.Register("DueDateText", typeof(string), typeof(OrderLineCard));
        public string DueDateText
        {
            get { return (string)GetValue(DueDateTextProperty); }
            set { SetValue(DueDateTextProperty, value); }
        }

        #endregion

        public OrderLineCard()
        {
            InitializeComponent();
        }

        private void CardLoaded(object sender, RoutedEventArgs e)
        {
            SetupDueDateText();
            SetupCommonName();
        }

        private void SetupCommonName()
        {
            if (string.IsNullOrEmpty(CustomerCompany))
            {
                CustomerCommonName = CustomerFirstName + " " + CustomerLastName;
            }
            else if (!string.IsNullOrEmpty(CustomerCompany))
            {
                CustomerCommonName = CustomerCompany;
            }
            else
            {
                CustomerCommonName = "Unidentified";
            }

        }

        private void SetupDueDateText()
        {
            var Today = DateTime.Now;
            string dayDue;
            string timeDue;
            string flexibility;

            if (DueDateTime.Date == Today.Date)
            {
                dayDue = "Today";
            }
            else if (DueDateTime.Date == Today.AddDays(1).Date)
            {
                dayDue = "Tomorrow";
            }
            else
            {
                dayDue = DueDateTime.ToString("ddd, M/d");
            }

            timeDue = " by " + DueDateTime.ToString("htt");

            if (IsFlexibleDeadline)
            {
                flexibility = " - Flexible";
            }
            else
            {
                flexibility = " - Absolute";
            }

            DueDateText = "Due " + dayDue + timeDue + flexibility;
        }

        private void OrderDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            int receipt = ReceiptNumber;
            MainWindow main = (MainWindow)App.Current.MainWindow;
            Views.Hopper hop = new Views.Hopper();
            main.navFrame.Navigate(hop, receipt);
        }

        private void MouseHover(object sender, MouseEventArgs e)
        {
            CustNameLabel.Foreground = Brushes.DarkGray;
        }

        private void MouseLeft(object sender, MouseEventArgs e)
        {
            CustNameLabel.Foreground = Brushes.DimGray;
        }
    }

    static class DateTimeExtensions
    {
        public static string ToShortDayName(this DateTime dateTime)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedDayName(dateTime.DayOfWeek);
        }
        public static string ToShortMonthName(this DateTime dateTime)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(dateTime.Month);
        }
    }
}
