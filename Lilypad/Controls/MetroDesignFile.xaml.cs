﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Lilypad.ViewModel;
using MahApps.Metro.Controls.Dialogs;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for MetroDesignFile.xaml
    /// </summary>
    public partial class MetroDesignFile : UserControl
    {
        public MetroDesignFile()
        {
            InitializeComponent();
        }

        private void SetIcon()
        {
            if (!string.IsNullOrEmpty(FileIcon))
            {
            }
        }

        private void CDRFileClicked(object sender, MouseButtonEventArgs e)
        {
            if (FilePath != null)
            {
                Process.Start(FilePath);
            }
        }

        private void OpenInCorel(object sender, RoutedEventArgs e)
        {
            if (File.Exists(FilePath))
            {
                var inf = new FileInfo(FilePath);
                try
                {
                    var proc = new Process();
                    var prs = new ProcessStartInfo();
                    prs.FileName = inf.FullName;
                    //  prs.UseShellExecute = false;
                    proc.StartInfo = prs;
                    proc.Start();
                }
                catch (Exception ex)
                {
                    Dispatcher.Invoke(() =>
                    {
                        Thread.Sleep(1000);
                        var proc = new Process();
                        var prs = new ProcessStartInfo();
                        prs.FileName = inf.FullName;
                        proc.StartInfo = prs;
                        proc.Start();
                    });
                }
            }
        }

        private void OpenInPhotoshop(object sender, RoutedEventArgs e)
        {
            if (FilePath != null)
            {
                try
                {
                    Process.Start(FilePath);
                }
                catch (Win32Exception ex)
                {
                    Thread.Sleep(5000);
                    Process.Start(FilePath);
                }
            }
        }

        private void OpenInFolder(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(FilePath))
            {
                var parent = Directory.GetParent(FilePath).FullName;
                Process.Start(parent);
            }
        }

        private async void DeleteFile(object sender, RoutedEventArgs e)
        {
            if (FilePath != null)
            {
                var result = await Utilities.ShowMetroYNDialog("Confirm Delete", "Are you sure you want to delete this file? This cannot be undone!");
                if (result == MessageDialogResult.Affirmative)
                {
                    try
                    {
                        File.Delete(FilePath);

                        var vm3 = (DesignFileViewModel)DataContext;
                        var ls = Utilities.FindParentByType<Controls.MetroDesignCard>(this);
                        if (ls != null)
                        {
                            var ovvm = (DesignSetupCardViewModel)ls.DataContext;
                            ovvm.CurrentArtworkItems.Remove(FileTitle);
                        }
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }
            }
            else
            {
                try
                {
                    var ls = Utilities.FindParentByType<MetroDesignCard>(this);
                    if (ls != null)
                    {
                        var filename = FileName.ToString();
                        ls.CurrentArtworkItems.Remove(filename);
                        ls.FilesContainer.Children.Remove(this);
                    }
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }

        private void OpenInCorel2(object sender, MouseButtonEventArgs e)
        {
            OpenInCorel(null, null);
        }

        private void RenameFile(object sender, RoutedEventArgs e)
        {
            FilenameEditor.Clear();
            FilenameEditor.Visibility = Visibility.Visible;
            Keyboard.Focus(FilenameEditor);
        }

        private void RefreshFiles(object sender, RoutedEventArgs e)
        {
        }

        private void FilenameeditorKeydown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                if (FilenameEditor.Text != "")
                {
                    RenameThisFile();
                }
                else
                {
                 //   UI.DialogSimple("Data Missing", "Filename cannot be blank.");
                }
            }
        }

        private void RenameThisFile()
        {
            var newname = FilenameEditor.Text;
            var filepath = "";
            var filetype = "";
            var parent = "";
            var fullpath = "";
            var newname2 = "";

            if (FilePath != null)
            {
                parent = Directory.GetParent(FilePath).ToString();
                filetype = "." + FileExtension;
                newname2 = newname + filetype;
                fullpath = Path.Combine(parent, newname2);
                FileTitle = newname;
            }
            try
            {
                File.Move(filepath, fullpath);
                FilenameEditor.Visibility = Visibility.Collapsed;
            }
            catch (IOException iox)
            {
                new Error(iox);
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private void DesignFileLoaded(object sender, RoutedEventArgs e)
        {
        }

        private void IconMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                CDRFileClicked(null, null);
            }
        }

        #region DependencyProperty Members

        public static DependencyProperty FileTitleProperty = DependencyProperty.Register("FileTitle", typeof(string),
            typeof(MetroDesignFile));

        public string FileTitle
        {
            get { return (string)GetValue(FileTitleProperty); }
            set { SetValue(FileTitleProperty, value); }
        }

        public static DependencyProperty FileExtensionProperty = DependencyProperty.Register("FileExtension",
            typeof(string), typeof(MetroDesignFile));

        public string FileExtension
        {
            get { return (string)GetValue(FileExtensionProperty); }
            set { SetValue(FileExtensionProperty, value); }
        }

        public static DependencyProperty FileIconProperty = DependencyProperty.Register("FileIcon", typeof(string),
            typeof(MetroDesignFile));

        public string FileIcon
        {
            get { return (string)GetValue(FileIconProperty); }
            set { SetValue(FileIconProperty, value); }
        }

        public static DependencyProperty FilePathProperty = DependencyProperty.Register("FilePath", typeof(string),
            typeof(MetroDesignFile));

        public string FilePath
        {
            get { return (string)GetValue(FilePathProperty); }
            set { SetValue(FilePathProperty, value); }
        }

        public static DependencyProperty FileMarkedForDeletionProperty =
            DependencyProperty.Register("FileMarkedForDeletion", typeof(bool), typeof(MetroDesignFile));

        public bool FileMarkedForDeletion
        {
            get { return (bool)GetValue(FileMarkedForDeletionProperty); }
            set { SetValue(FileMarkedForDeletionProperty, value); }
        }

        public static DependencyProperty FileIsNewProperty = DependencyProperty.Register("FileIsNew", typeof(bool),
            typeof(MetroDesignFile));

        public bool FileIsNew
        {
            get { return (bool)GetValue(FileIsNewProperty); }
            set { SetValue(FileIsNewProperty, value); }
        }

        #endregion DependencyProperty Members
    }
}