﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for Tooltip.xaml
    /// </summary>
    public partial class Tooltip : UserControl
    {
        public Tooltip()
        {
            InitializeComponent();
        }

        private void DeleteClick(object sender, MouseButtonEventArgs e)
        {
        }

        private void TabLoaded(object sender, RoutedEventArgs e)
        {
        }

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        #region DependencyProperty Members

        public static DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string),
            typeof(Tooltip));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        #endregion DependencyProperty Members
    }
}