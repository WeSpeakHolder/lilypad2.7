﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Lilypad.ViewModel;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for FolderSelector.xaml
    /// </summary>
    public partial class FolderSelector : UserControl
    {
        public FolderSelector()
        {
            InitializeComponent();
        }

        private void CardEnter(object sender, MouseEventArgs e)
        {
        }

        private void CardLeave(object sender, MouseEventArgs e)
        {
        }

        private void FolderSelectorLoaded(object sender, RoutedEventArgs e)
        {
            this.OpacityAnimation(1.0, TimeSpan.FromMilliseconds(400));
        }

        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        #region DependencyProperty Members

        public static DependencyProperty FolderNameProperty = DependencyProperty.Register("FolderName", typeof(string),
            typeof(FolderSelector));

        public string FolderName
        {
            get { return (string)GetValue(FolderNameProperty); }
            set { SetValue(FolderNameProperty, value); }
        }

        public static DependencyProperty DesignPathProperty = DependencyProperty.Register("DesignPath", typeof(string),
            typeof(FolderSelector));

        public string DesignPath
        {
            get { return (string)GetValue(DesignPathProperty); }
            set { SetValue(DesignPathProperty, value); }
        }

        #endregion DependencyProperty Members
    }
}