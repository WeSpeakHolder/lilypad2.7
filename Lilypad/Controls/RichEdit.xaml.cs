﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.Controls.DesignCards;
using Lilypad.ViewModel;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class RichEdit : UserControl
    {
        #region DependencyProperty Members

        public static DependencyProperty DocumentPathProperty = DependencyProperty.Register("DocumentPath", typeof(string), typeof(RichEdit));
        public string DocumentPath
        {
            get { return (string)GetValue(DocumentPathProperty); }
            set { SetValue(DocumentPathProperty, value); }
        }
        
        public static DependencyProperty EditorHeightProperty = DependencyProperty.Register("EditorHeight", typeof(double), typeof(RichEdit));
        public double EditorHeight
        {
            get { return (double)GetValue(EditorHeightProperty); }
            set { SetValue(EditorHeightProperty, value); }
        }

        public static DependencyProperty EditorWidthProperty = DependencyProperty.Register("EditorWidth", typeof(double), typeof(RichEdit));
        public double EditorWidth
        {
            get { return (double)GetValue(EditorWidthProperty); }
            set { SetValue(EditorWidthProperty, value); }
        }

        #endregion

        public RichEdit()
        {
            InitializeComponent();
        }

        private void RichEditor_Loaded(object sender, RoutedEventArgs e)
        {

        }

        public void LoadNewDocumentBackend(string inputPath)
        {
            try
            {
                RichEditorActual.LoadDocument(inputPath);
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private void EditorLostFocus(object sender, RoutedEventArgs e)
        {
            if (RichEditorActual.Document.Text.Length > 0)
            {
                RichEditorActual.SaveDocument();
            }
        }
    }
}
