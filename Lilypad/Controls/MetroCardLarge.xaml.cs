﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Lilypad.Model;
using Lilypad.View;
using Lilypad.ViewModel;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Ookii.Dialogs.Wpf;

namespace Lilypad.Controls
{
    /// <summary>
    ///     Interaction logic for MetroCardLarge.xaml
    /// </summary>
    public partial class MetroCardLarge : UserControl
    {
        public MetroCardLarge()
        {
            InitializeComponent();

        }




        #region PropertyChanged Controller

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChanged Controller

        #region DependencyProperty Members

        public static DependencyProperty CardContentProperty = DependencyProperty.Register("CardContent", typeof(object), typeof(MetroCardLarge));
        public object CardContent
        {
            get { return GetValue(CardContentProperty); }
            set { SetValue(CardContentProperty, value); }
        }

        #endregion DependencyProperty Members
    }
}