﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Lilypad.Model;
using Lilypad.ViewModel;
using Lilypad.Controls.DesignCards;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.LayoutControl;

namespace Lilypad.Controls
{
    /// <summary>
    /// Interaction logic for Tag.xaml
    /// </summary>
    /// 

    public partial class CallCard : UserControl
    {
        public CallCard()
        {
            InitializeComponent();
        }

        private void CheckClicked(object sender, MouseButtonEventArgs e)
        {
            CallCardViewModel vm = (CallCardViewModel)this.DataContext;

            if (vm.IsCalled == false)
            {
                CheckIcon.Fill = Brushes.YellowGreen;
                CrossOutBar.Visibility = System.Windows.Visibility.Visible;
                NameLabel.Foreground = Brushes.LightGray;
                CallTimeLabel.Foreground = Brushes.LightGray;
                PhoneLabel.Foreground = Brushes.LightGray;
                vm.IsCalled = true;
                vm.SaveChanges();
            }
            else
            {
                vm.DeleteItem();
                StackPanel pItem = (StackPanel)this.VisualParent;
                pItem.Children.Remove(this);

            }
        }

        private void Unclicked(object sender, MouseButtonEventArgs e)
        {
            CallCardViewModel vm = (CallCardViewModel)this.DataContext;
            CheckIcon.Fill = Brushes.Red;
            CrossOutBar.Visibility = System.Windows.Visibility.Collapsed;
            NameLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF707070"));
            CallTimeLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFb0b0b0"));
            PhoneLabel.Foreground = Brushes.Gray;
            vm.IsCalled = false;
            vm.SaveChanges();
        }

        private void CardHoverShowPhone(object sender, MouseEventArgs e)
        {
            DoubleAnimation opacAnim = new DoubleAnimation(1.0, new Duration(TimeSpan.FromSeconds(0.3)));
            QuadraticEase opacEase = new QuadraticEase(); opacEase.EasingMode = EasingMode.EaseIn;
            opacAnim.EasingFunction = opacEase;
            PhoneSP.BeginAnimation(OpacityProperty, opacAnim);
        }

        private void CardHoverHidePhone(object sender, MouseEventArgs e)
        {
            DoubleAnimation opacAnim = new DoubleAnimation(0.0, new Duration(TimeSpan.FromSeconds(0.3)));
            QuadraticEase opacEase = new QuadraticEase(); opacEase.EasingMode = EasingMode.EaseOut;
            opacAnim.EasingFunction = opacEase;
            PhoneSP.BeginAnimation(OpacityProperty, opacAnim);
        }

        private void EmailClicked(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
