﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;
using DevExpress.Xpf.Data;
using DevExpress.Xpf.Editors;
using System.Windows.Controls;
using System.Collections;
using DevExpress.Xpf.Grid;
using System.Windows.Interactivity;
using System.Windows.Threading;

namespace Lilypad
{
    public class SelectionBehavior : Behavior<GridControl>
    {
        public static bool GetSelectionChanged(DependencyObject obj)
        {
            return (bool)obj.GetValue(SelectionChangedProperty);
        }
        public static void SetSelectionChanged(DependencyObject obj, bool value)
        {
            obj.SetValue(SelectionChangedProperty, value);
        }
        public static readonly DependencyProperty SelectionChangedProperty =
            DependencyProperty.RegisterAttached("SelectionChanged", typeof(bool), typeof(SelectionBehavior),
                new PropertyMetadata(false, new PropertyChangedCallback((d, e) => {
                    if ((bool)e.NewValue)
                        Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() => {
                            SetSelectionChanged(d, false);
                        }), DispatcherPriority.Render);
                })));


        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectionChanged += OnGridSelectionChanged;
        }

        void OnGridSelectionChanged(object sender, GridSelectionChangedEventArgs e)
        {
            SetSelectionChanged(AssociatedObject, true);
        }

        protected override void OnDetaching()
        {
            AssociatedObject.SelectionChanged -= OnGridSelectionChanged;
            base.OnDetaching();
        }
    }

    public class GroupRowSelectionStateConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int rowHandle = (int)values[1];
            GridControl grid = (GridControl)values[2];

            return GetIsGroupSelected(grid, rowHandle);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        bool? GetIsGroupSelected(GridControl grid, int groupRowHandle)
        {
            int childrenCount = grid.GetChildRowCount(groupRowHandle);
            int selectedCount = 0;
            for (int i = 0; i < childrenCount; i++)
            {
                int childrenHandle = grid.GetChildRowHandle(groupRowHandle, i);

                if (grid.IsGroupRowHandle(childrenHandle))
                {
                    if (GetIsGroupSelected(grid, childrenHandle) == true)
                        selectedCount++;
                }
                else if (grid.View.IsRowSelected(childrenHandle))
                    selectedCount++;
            }

            if (selectedCount == 0)
                return false;
            else if (selectedCount == childrenCount)
                return true;
            else
                return null;
        }
    }

}
