using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Properties;
using Lilypad.Model;
using System.Windows.Data;


namespace Lilypad.Converters
{
    public class AlphaItemFeatures : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string cleaned = (string)value;
            string input = cleaned.Trim();
            string before = "\u2022  ";
            string after = Environment.NewLine;

            string final = "";
            char[] sep = new char[] { ';' };
            string[] outList = input.Split(sep, StringSplitOptions.RemoveEmptyEntries).ToArray();
            foreach (string str in outList)
            {
                final += before + str + after;
            }

            return final;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}