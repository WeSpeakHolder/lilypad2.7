using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Lilypad.Converters
{
    public class NullStringToVisibilityCollapsed : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (value is int)
                {
                    if ((int)value == 0) return Visibility.Collapsed;
                }
                if (string.IsNullOrEmpty(value.ToString()))
                {
                    return Visibility.Collapsed;
                }
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}