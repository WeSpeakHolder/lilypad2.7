﻿using System;
using System.Linq;
using System.Data;
using System.Globalization;
using System.Collections.Generic;
using System.Windows.Data;
using System.Threading.Tasks;

namespace Lilypad.Converters
{
    public class IntToPriorityName : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int input;
            string val;
            if (value != null)
            {
                val = value.ToString();
                bool result = Int32.TryParse(val, out input);
                if (result)
                {
                    using (Model.Entities ctx = new Model.Entities())
                    {
                        var p = (from c in ctx.priorities where c.PriorityValue == input select c.PriorityTitle).FirstOrDefault().ToString();
                        return p;
                    }
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
