using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Markup;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Grid;
using DevExpress.Data.Filtering;
using DevExpress.Data.Filtering.Helpers;


namespace Lilypad.Converters
{
    public class PriorityToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                int iIn = System.Convert.ToInt32(value);
                if (iIn == 1) return new LinearGradientBrush(Color.FromArgb(255, 135, 215, 0), Color.FromArgb(0, 87, 168, 0), 0);
                if (iIn == 2) return new LinearGradientBrush(Color.FromArgb(255, 253, 135, 10), Color.FromArgb(0, 226, 134, 15), 0);
                if (iIn == 3) return new LinearGradientBrush(Color.FromArgb(255, 217, 0, 21), Color.FromArgb(0, 170, 0, 0), 0);
                else
                {
                    return new LinearGradientBrush(Color.FromArgb(40, 255, 255, 255), Color.FromArgb(0, 255, 255, 255), 0);
                }
            }
            else
            {
                return new LinearGradientBrush(Color.FromArgb(40, 255, 255, 255), Color.FromArgb(0, 255, 255, 255), 0);
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return 0;
        }
    }
}