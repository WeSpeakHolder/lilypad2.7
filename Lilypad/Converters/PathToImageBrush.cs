using System;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Globalization;
using System.Windows.Data;


namespace Lilypad.Converters
{
    public class PathToImageBrush : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                string type = value.ToString();
                BitmapImage newImg = new BitmapImage(new Uri(type, UriKind.Absolute));
                ImageBrush brush = new ImageBrush(newImg);
                return brush;
            }
            else
            {
                return new ImageBrush();
            }


        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}