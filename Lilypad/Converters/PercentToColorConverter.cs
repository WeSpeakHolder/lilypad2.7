using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Properties;
using Lilypad.Model;
using System.Windows.Data;


namespace Lilypad.Converters
{
    public class PercentToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int input = (int)value;
            SolidColorBrush color;

            if (input >= 0 && input < 20) color = new SolidColorBrush(Color.FromArgb(255, 246, 54, 9));
            else if (input >= 20 && input < 40) color = new SolidColorBrush(Color.FromArgb(255, 246, 113, 9));
            else if (input >= 40 && input < 70) color = new SolidColorBrush(Color.FromArgb(255, 246, 188, 9));
            else if (input >= 70 && input < 100) color = new SolidColorBrush(Color.FromArgb(255, 115, 188, 0));
            else if (input == 100) color = (SolidColorBrush)App.Current.FindResource("AccentColorBrush");
            else color = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));

            return color;


        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}