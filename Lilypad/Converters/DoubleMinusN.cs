using System;
using System.Globalization;
using System.Windows.Data;

namespace Lilypad.Converters
{
    internal class DoubleMinusN : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var entry = double.Parse(value.ToString());
                if (parameter != null)
                {
                    var offset = double.Parse(parameter.ToString());
                    return entry - offset;
                }
                return entry - 20;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}