using System;
using System.Globalization;
using System.Windows.Data;

namespace Lilypad.Converters
{
    public class ArtSizeConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var input = bool.Parse(value.ToString());
            var output = "";

            if (input)
            {
                output = "Size To Item";
            }
            else
            {
                output = "Print As-Is";
            }

            return output;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}