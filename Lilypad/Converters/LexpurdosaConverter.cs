using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Properties;
using Lilypad.Model;
using System.Windows.Data;


namespace Lilypad.Converters
{
    public class LexpurdosaConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            decimal inp = (decimal)value;
            string input = inp.ToString();
            char[] characters = input.ToCharArray();
            string lexcode = "";

            foreach (char inum in characters)
            {
                if (inum == '1') { lexcode += "L"; }
                if (inum == '2') { lexcode += "E"; }
                if (inum == '3') { lexcode += "X"; }
                if (inum == '4') { lexcode += "P"; }
                if (inum == '5') { lexcode += "U"; }
                if (inum == '6') { lexcode += "R"; }
                if (inum == '7') { lexcode += "D"; }
                if (inum == '8') { lexcode += "O"; }
                if (inum == '9') { lexcode += "S"; }
                if (inum == '0') { lexcode += "A"; }
            }

            return lexcode;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return false;
        }
    }
}