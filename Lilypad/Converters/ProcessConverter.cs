﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Lilypad.Model;

namespace Lilypad.Converters
{
    public class ProcessConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var input = 0;
                var valid = int.TryParse(value.ToString(), out input);
                if (valid)
                {
                    var output = "";

                    if (input > 0)
                    {
                        using (var ctx = new Entities())
                        {
                            output = (from q in ctx.printprocesses where q.ProcessID == input select q.ProcessName).FirstOrDefault();
                        }
                        if (output != null) return output;
                    }
                    output = "Process";
                    return output;
                }
                return value;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}