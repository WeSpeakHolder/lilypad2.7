using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Markup;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Grid;
using DevExpress.Data.Filtering;
using DevExpress.Data.Filtering.Helpers;


namespace Lilypad.Converters
{
    public class PriorityToColorConverter2 : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((int) value == 1) return Brushes.YellowGreen; //new SolidColorBrush(Color.FromArgb(255, 51, 161, 51));
            if ((int) value == 2) return Brushes.Orange; //new SolidColorBrush(Color.FromArgb(255, 255, 150, 0));
            if ((int) value == 3) return Brushes.Red; // new SolidColorBrush(Color.FromArgb(255, 255, 36, 0));

            return new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}