using System;
using System.Globalization;
using System.Windows.Data;

namespace Lilypad.Converters
{
    internal class IconSizeM : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var entry = double.Parse(value.ToString());
                return Math.Round(entry / 2.2, 0);
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}