using System;
using System.Globalization;
using System.Windows.Data;

namespace Lilypad.Converters
{
    internal class DollarSignPrefix : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var entry = value.ToString();
                return @"$" + entry;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}