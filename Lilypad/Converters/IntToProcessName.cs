﻿using System;
using System.Data;
using System.Globalization;
using System.Collections.Generic;
using System.Windows.Data;
using System.Threading.Tasks;

namespace Lilypad.Converters
{
    public class IntToProcessName : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int input;
            string val;
            if (value != null)
            {
                val = value.ToString();
                bool result = Int32.TryParse(val, out input);
                if (result)
                {
                    string process;
                    //if (input == 1) process = "Digital";
                    //else if (input == 2) process = "Vinyl";
                    //else if (input == 3) process = "ScreenPrint";
                    //else if (input == 4) process = "UltraPrint";
                    //else if (input == 5) process = "Embroidery";
                    //else if (input == 6) process = "Multiple";

                    if (input == 1) process = "DTG";
                    else if (input == 2) process = "Vinyl";
                    else if (input == 3) process = "ScreenPrint";
                    else if (input == 4) process = "UltraPrint";
                    else if (input == 5) process = "Embroidery";
                    else if (input == 6) process = "Mixed";
                    else if (input == 7) process = "DTG-W";
                    else if (input == 8) process = "Dye Sublimation";
                    else if (input == 9) process = "Poly Pretreat";
                    else if (input == 10) process = "Bling";
                    else if (input == 11) process = "Printable Vinyl";
                    else if (input == 12) process = "Blank Shirts";
                    else if (input == 13) process = "Digital Heat Transfer";
                    else if (input == 14) process = "None";


                    else process = "";
                    return process;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
