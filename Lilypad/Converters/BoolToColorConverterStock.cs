using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Properties;
using Lilypad.Model;
using System.Windows.Data;


namespace Lilypad.Converters
{
    public class BoolToColorConverterStock : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var converter = new System.Windows.Media.BrushConverter();
            bool isTrue = (bool)value;

            var GreyColor = Brushes.Gray;
            var HighlightColor = (Brush)App.Current.FindResource("AccentColorBrush");

            if (isTrue)
            {
                return HighlightColor;
            }
            else
            {
                return GreyColor;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}