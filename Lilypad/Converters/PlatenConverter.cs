﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Windows.Data;
using System.Threading.Tasks;

namespace Lilypad.Converters
{
    public class PlatenConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool input = (bool)value;
            string output = "";

            if (input)
            {
                output = "Small Platen";
            }
            else
            {
                output = "Large Platen";
            }

            return output;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}
