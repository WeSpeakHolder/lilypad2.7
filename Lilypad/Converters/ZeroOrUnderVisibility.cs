using System;
using System.Globalization;
using System.Windows.Data;

namespace Lilypad.Converters
{
    internal class ZeroOrUnderVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                double d = double.Parse(value.ToString());
                return d < 1 ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            }
            else return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}