using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.ComponentModel;
using Lilypad.Properties;
using Lilypad.Model;
using System.Windows.Data;


namespace Lilypad.Converters
{
    public class AlphaItemDescription : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string input = value.ToString();
            string[] splitter = new string[] { " - " };
            return input.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0];
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}