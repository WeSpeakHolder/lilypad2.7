﻿using System;
using System.Data;
using System.Globalization;
using System.Collections.Generic;
using System.Windows.Data;
using System.Threading.Tasks;

namespace Lilypad.Converters.SizeConverters
{
    public class InvisiboxConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double input = System.Convert.ToDouble(value);
            return ((double)input + 4);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
