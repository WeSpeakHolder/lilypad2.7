﻿using System;
using System.Data;
using System.Globalization;
using System.Collections.Generic;
using System.Windows.Data;
using System.Threading.Tasks;

namespace Lilypad.Converters.SizeConverters
{
    public class HopperGridHeight : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int input;
            if (value != null)
            {
                bool result = Int32.TryParse(value.ToString(), out input);
                if (result)
                {
                    return input - 162;
                }
                else
                {
                    return input - 162;
                }
            }
            else
            {
                return 500;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
