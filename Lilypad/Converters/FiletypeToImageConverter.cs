using System;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Globalization;
using System.Windows.Data;


namespace Lilypad.Converters
{
    public class FiletypeToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string type = (string)value;

            if (type == "ai" || type == "cdr" || type == "doc" || type == "eps" || type == "jpg"
                 || type == "pdf" || type == "pdf" || type == "png" || type == "psd" || type == "svg")
            {
                return new BitmapImage(new Uri("FiletypeImages/" + type + ".png", UriKind.Relative));
            }
            else
            {
                return new BitmapImage(new Uri("FiletypeImages/other.png", UriKind.Relative));
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}