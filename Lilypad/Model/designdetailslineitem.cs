//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lilypad.Model
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public partial class designdetailslineitem
    {
        public int UniqueID { get; set; }
        public int DesignID { get; set; }
        public int ReceiptNum { get; set; }
        public string StockIDPOS { get; set; }
        public int Quantity { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string Description { get; set; }
    
        public virtual designdetail designdetail { get; set; }
    }
}
