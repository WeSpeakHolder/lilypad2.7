﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lilypad.Model
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Entities : DbContext
    {
        public Entities()
            : base("name=Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<alpha> alphas { get; set; }
        public DbSet<alphacurrentorder> alphacurrentorders { get; set; }
        public DbSet<alphaentries1> alphaentries1 { get; set; }
        public DbSet<alphaentry> alphaentries { get; set; }
        public DbSet<alphaitem> alphaitems { get; set; }
        public DbSet<alphaorder> alphaorders { get; set; }
        public DbSet<archivedpreorder> archivedpreorders { get; set; }
        public DbSet<calllist> calllists { get; set; }
        public DbSet<caretag> caretags { get; set; }
        public DbSet<correspondence> correspondences { get; set; }
        public DbSet<customer> customers { get; set; }
        public DbSet<decoorder> decoorders { get; set; }
        public DbSet<designdetail> designdetails { get; set; }
        public DbSet<designdetailslineitem> designdetailslineitems { get; set; }
        public DbSet<errorlog> errorlogs { get; set; }
        public DbSet<frogger> froggers { get; set; }
        public DbSet<globalsetting> globalsettings { get; set; }
        public DbSet<goal> goals { get; set; }
        public DbSet<hopperorder> hopperorders { get; set; }
        public DbSet<inventory> inventories { get; set; }
        public DbSet<inventorystatu> inventorystatus { get; set; }
        public DbSet<ioqueue> ioqueues { get; set; }
        public DbSet<issue> issues { get; set; }
        public DbSet<issuetype> issuetypes { get; set; }
        public DbSet<log> logs { get; set; }
        public DbSet<loss> losses { get; set; }
        public DbSet<losstype> losstypes { get; set; }
        public DbSet<orderdetail> orderdetails { get; set; }
        public DbSet<orderexception> orderexceptions { get; set; }
        public DbSet<order> orders { get; set; }
        public DbSet<orderstatu> orderstatus { get; set; }
        public DbSet<preorderdesigndetail> preorderdesigndetails { get; set; }
        public DbSet<preorderitem> preorderitems { get; set; }
        public DbSet<preorder> preorders { get; set; }
        public DbSet<preorderstatu> preorderstatus { get; set; }
        public DbSet<printcolor> printcolors { get; set; }
        public DbSet<printposition> printpositions { get; set; }
        public DbSet<printprocess> printprocesses { get; set; }
        public DbSet<priority> priorities { get; set; }
        public DbSet<productionstatu> productionstatus { get; set; }
        public DbSet<product> products { get; set; }
        public DbSet<quote> quotes { get; set; }
        public DbSet<receipt> receipts { get; set; }
        public DbSet<salesbyfrogger> salesbyfroggers { get; set; }
        public DbSet<salesbyprocess> salesbyprocesses { get; set; }
        public DbSet<salestotal> salestotals { get; set; }
        public DbSet<setupstatu> setupstatus { get; set; }
        public DbSet<shipment> shipments { get; set; }
        public DbSet<spotcolor> spotcolors { get; set; }
        public DbSet<status> status { get; set; }
        public DbSet<task> tasks { get; set; }
        public DbSet<ticket> tickets { get; set; }
        public DbSet<ultraprintinventory> ultraprintinventories { get; set; }
        public DbSet<ultraprintorderdata> ultraprintorderdatas { get; set; }
        public DbSet<ultraprintorder> ultraprintorders { get; set; }
        public DbSet<ultraprint> ultraprints { get; set; }
        public DbSet<artworkstatu> artworkstatus { get; set; }
        public DbSet<inventorydatastatu> inventorydatastatus { get; set; }
    }
}
