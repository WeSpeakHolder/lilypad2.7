//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lilypad.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class alpha
    {
        public alpha()
        {
            this.alphaentries = new HashSet<alphaentry>();
        }
    
        public int UniqueID { get; set; }
        public string AlphaOrderID { get; set; }
        public int Status { get; set; }
        public int TotalItems { get; set; }
        public decimal TotalCost { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public System.DateTime ReceiveDate { get; set; }
        public bool Archived { get; set; }
    
        public virtual orderstatu orderstatu { get; set; }
        public virtual ICollection<alphaentry> alphaentries { get; set; }
    }
}
