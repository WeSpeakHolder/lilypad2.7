//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lilypad.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class salesbyfrogger
    {
        public System.DateTime Date { get; set; }
        public string FroggerName { get; set; }
        public int OrdersSoldNum { get; set; }
        public decimal OrdersSoldAmt { get; set; }
        public int OrdersProdNum { get; set; }
        public decimal OrdersProdAmt { get; set; }
    }
}
