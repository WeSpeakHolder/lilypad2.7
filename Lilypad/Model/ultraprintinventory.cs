//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lilypad.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ultraprintinventory
    {
        public int InventoryID { get; set; }
        public int CustID { get; set; }
        public int QuantityOnHand { get; set; }
        public string Color { get; set; }
        public int UltraprintID { get; set; }
        public System.DateTime LastUsed { get; set; }
    }
}
