//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lilypad.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class alphaentry
    {
        public int UniqueID { get; set; }
        public int AlphaOrderID { get; set; }
        public int ReceiptNum { get; set; }
        public int Quantity { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public string StockIDPOS { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public decimal LineCost { get; set; }
        public System.DateTime AddedDateTime { get; set; }
        public System.DateTime OrderDateTime { get; set; }
        public System.DateTime CheckInDateTime { get; set; }
    
        public virtual alpha alpha { get; set; }
        public virtual order order { get; set; }
        public virtual product product { get; set; }
        public virtual orderstatu orderstatu { get; set; }
    }
}
