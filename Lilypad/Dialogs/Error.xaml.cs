﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using Lilypad.ViewModel;
using MahApps.Metro.Controls.Dialogs;

namespace Lilypad.Dialogs
{
    /// <summary>
    ///     Interaction logic for Error.xaml
    /// </summary>
    public partial class Error : UserControl
    {
        public string EFrogger;
        public bool ErrorUploading;
        private Exception LocalException;

        public Error(Exception ex)
        {
            InitializeComponent();
            var mvm = new ErrorViewModel();
            mvm.CaughtException = ex;
            LocalException = ex;
            // ReportError(ex);
        }

        public Error(Exception ex, string report)
        {
            InitializeComponent();
            var mvm = (ErrorViewModel)DataContext;
            mvm.CaughtException = ex;
            mvm.ErrorDetails = report;
            LocalException = ex;
            // ReportError(ex);
        }

        public async void ReportError(Exception ex)
        {
            var mvm = (ErrorViewModel)DataContext;
            try
            {
                var result = await Utilities.UploadErrorReportToCloud(ex);
                if (result != null && result == "OK")
                {
                    mvm.ErrorReportStatus = "Error successfully reported.";
                    mvm.ErrorReportSuccessful = true;
                }
                else
                {
                    mvm.ErrorReportStatus = result;
                    mvm.ErrorReportSuccessful = false;
                }
            }
            catch (Exception ex2)
            {
                new SilentError(ex2);
            }
        }

        private void CompletelyLoaded(object sender, RoutedEventArgs e)
        {
        }

        private void ViewDetailsClicked(object sender, MouseButtonEventArgs e)
        {
            if (DetailsGrid.Height == 0)
            {
                var anim = new DoubleAnimation(140, TimeSpan.FromMilliseconds(400));
                DetailsGrid.BeginAnimation(HeightProperty, anim);
            }
            else
            {
                var anim = new DoubleAnimation(0, TimeSpan.FromMilliseconds(400));
                DetailsGrid.BeginAnimation(HeightProperty, anim);
            }
        }

        private void SubmitDialogClick(object sender, EventArgs e)
        {
            var main = (MainWindow)Application.Current.MainWindow;
            var dialog = Utilities.FindParentByType<SimpleDialog>(this);
            main.HideMetroDialogAsync(dialog);
        }
    }
}