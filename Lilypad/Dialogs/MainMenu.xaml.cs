﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Lilypad.Controls;
using Lilypad.ViewModel;
using Lilypad.Views;

namespace Lilypad.Dialogs
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : UserControl
    {
        public MainMenu()
        {
            InitializeComponent();
        }


        private void RectMouse2(object sender, MouseButtonEventArgs e)
        {
            var parent = Utilities.FindParentByType<TransitioningContentControl>(this);
            var bigParent = parent.Parent as TabItem;
            var biggest = bigParent.Parent as TabControl;

            TabItem newTab = new TabItem();
            TransitioningContentControl tranControl = new TransitioningContentControl();
            newTab.Header = "New Tab";
            tranControl.Content = new MainMenu();
            newTab.Content = tranControl;
            biggest.Items.Add(newTab);
        }

        private void RectMouse3(object sender, MouseButtonEventArgs e)
        {
            var navver = Utilities.FindParentByType<LilyNav>(this);
            navver.GoBack();
        }

        private void SettingsClicked(object sender, EventArgs e)
        {
            MainViewModel mvm = (MainViewModel)Application.Current.MainWindow.DataContext;
            mvm.Navigate_MissionControl.Execute(null);
            mvm.MenuOpen = false;
        }

        private void ExitClicked(object sender, EventArgs e)
        {
            Utilities.ExitLilypad();
        }

        private void LogoutClicked(object sender, EventArgs e)
        {
            MainViewModel mvm = (MainViewModel)Application.Current.MainWindow.DataContext;
            Model.frogger frog = new Model.frogger();
            frog.FrogFirstName = "Lilypad";
            var win7 = @"C:\ProgramData\Microsoft\User Account Pictures\Default Pictures\usertile10.bmp";
            var win8 = @"C:\ProgramData\Microsoft\Default Account Pictures\guest.bmp";
            var win88 = @"C:\ProgramData\Microsoft\User Account Pictures\guest.bmp";
            if (System.IO.File.Exists(win7))
            {
                frog.FrogImagePath = win7;
            }
            else if (System.IO.File.Exists(win8))
            {
                frog.FrogImagePath = win8;
            }
            else
            {
                frog.FrogImagePath = win88;
            }
            frog.FrogID = 1;
            mvm.LoggedInFrogger = frog;
            mvm.FrogIsLoggedIn = false;
            mvm.Logout();
            mvm.MenuOpen = false;
        }

        private void UserHover(object sender, MouseEventArgs e)
        {
            var t = sender as DevExpress.Xpf.LayoutControl.Tile;
            DoubleAnimation da = new DoubleAnimation(0.8, new Duration(TimeSpan.FromSeconds(0.2)));
            t.BeginAnimation(OpacityProperty, da);
        }

        private void UserLeave(object sender, MouseEventArgs e)
        {
            var t = sender as DevExpress.Xpf.LayoutControl.Tile;
            DoubleAnimation da = new DoubleAnimation(0.3, new Duration(TimeSpan.FromSeconds(0.2)));
            t.BeginAnimation(OpacityProperty, da);
        }

        private void EditProfileClicked(object sender, EventArgs e)
        {
            MainWindow mw = (MainWindow)App.Current.MainWindow;
            MainViewModel mvm = (MainViewModel)mw.DataContext;
            mvm.MenuOpen = false;
            mvm.NavigateTo_MissionControl(true);
        }
    }
}
