﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.ViewModel;
using Lilypad.Model;
using Lilypad.Views;
using Lilypad.Controls.DesignCards;
using System.Collections.ObjectModel;
using System.Data.Entity;

namespace Lilypad.Dialogs
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class ArtSetup : UserControl
    {
        public ArtSetup()
        {
            InitializeComponent();
            DesignSetupCardViewModel mvm = new DesignSetupCardViewModel();
            designdetail deet = new designdetail();
            mvm.SelectedDesignDetail = deet;
            this.DataContext = mvm;
        }

        public ArtSetup(SetupCard card)
        {
            InitializeComponent();
            selectedcardvm = (DesignSetupCardViewModel)card.DataContext;
            selectedcard = card;
            this.DataContext = SelectedCardVM;
        }

        public ArtSetup(DesignSetupCardViewModel cardVM)
        {
            InitializeComponent();
            this.DataContext = cardVM;
        }

        private DesignSetupCardViewModel selectedcardvm;
        public DesignSetupCardViewModel SelectedCardVM 
        {
            get { return selectedcardvm; }
            set { selectedcardvm = value; }
        }

        private SetupCard selectedcard;
        public SetupCard SelectedCard
        {
            get { return selectedcard; }
            set { selectedcard = value; }
        }

        private void Grid_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            var parent = Utilities.FindParentByType<TransitioningContentControl>(this);
            var newPres = new MainMenu();
            parent.Content = newPres;
        }

        private void SubmitDialog(object sender, RoutedEventArgs e)
        {
            var main = (MainWindow)Application.Current.MainWindow;
            SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
            DesignSetupCardViewModel asvm = (DesignSetupCardViewModel)this.DataContext;
            asvm.SaveChanges();
            main.HideMetroDialogAsync(dialog);

        }

        private void CancelDialog(object sender, RoutedEventArgs e)
        {
            var main = (MainWindow)Application.Current.MainWindow;
            SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
            main.HideMetroDialogAsync(dialog);
        }

        private void TextboxFocused(object sender, RoutedEventArgs e)
        {
            if (DescriptionEditor.Text == "Add Description")
            {
                DescriptionEditor.Clear();
            }
            else if (DescriptionEditor.Text == null)
            {
                DescriptionEditor.Clear();
            }
        }


        private void SetupLoaded(object sender, RoutedEventArgs e)
        {
            using (var ctx = new Entities())
            {
                ctx.printprocesses.Load();
                Process.ItemsSource = ctx.printprocesses.Local;
                var itemListProcessEditor = Process.ItemsSource as ObservableCollection<printprocess>;
                Process.ItemsSource = null;
                Process.ItemsSource = itemListProcessEditor.OrderBy(x => x.ProcessOrderId).ToList();
            }
            
        }
    }
}
