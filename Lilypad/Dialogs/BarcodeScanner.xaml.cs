﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Lilypad.Controls;
using Lilypad.ViewModel;
using Lilypad.Model;

namespace Lilypad.Dialogs
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class BarcodeScanner : UserControl
    {
        public BarcodeScanner()
        {
            InitializeComponent();
        }

        private void ViewDetails(object sender, EventArgs e)
        {
            MainWindow mw = (MainWindow)App.Current.MainWindow;
            MainViewModel mvm = (MainViewModel)mw.DataContext;
            int thisID = Convert.ToInt32(ThisIdentifiedItem);

            Views.Hopper hop = new Views.Hopper();
            mw.navFrame.Navigate(hop, thisID);
            mvm.ScannerOpen = false;
        }

        private void MarkCompleted(object sender, EventArgs e)
        {
            Completed();
        }

        private void MarkPickedUp(object sender, EventArgs e)
        {
            PickedUp();
        }

        private void OpenFolderClick(object sender, EventArgs e)
        {
            if (foundHopper) { OpenHopperFolder(); }
            else { OpenFolder(); }
        }

        private void OpenFolder()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    int thisID = Convert.ToInt32(ThisIdentifiedItem);
                    order SelectedOrder;
                    using (Entities ctx = new Entities())
                    {
                        SelectedOrder = (from c in ctx.orders where c.ReceiptNum == thisID select c).FirstOrDefault();
                    }
                    var path = SelectedOrder.JobFolderPath.ToString();
                    if (Directory.Exists(path))
                    {
                        Process.Start(path);
                    }
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }));

            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            mvm.ScannerOpen = false;
        }

        private void OpenHopperFolder()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    int thisID = Convert.ToInt32(ThisIdentifiedItem);
                    hopperorder SelectedOrder;
                    using (Entities ctx = new Entities())
                    {
                        SelectedOrder = (from c in ctx.hopperorders where c.ReceiptNum == thisID select c).FirstOrDefault();
                    }
                    var path = SelectedOrder.JobFolderPath.ToString();
                    if (Directory.Exists(path))
                    {
                        Process.Start(path);
                    }
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }));

            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            mvm.ScannerOpen = false;
        }

        private void CancelClick(object sender, EventArgs e)
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            mvm.ScannerOpen = false;
        }

        private void ScannerLoaded(object sender, RoutedEventArgs e)
        {

        }

        private void ScannerKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                string input = ScanBox.Text.Trim();
                //  ResetAllScanObjects();
                // MessageBox.Show("TenderSoft Test Message 1:" + input,"TenderSoft Testing");
                ProcessScan(input);
                RefocusItem(null, null);
            }
        }

        private void ProcessScan(string input)
        {
            if (!string.IsNullOrEmpty(input) && input.Length > 2)
            {
                string result = "";
                string first2 = input.Substring(0, 2);
                string first3 = input.Substring(0, 3).ToLower();
                string last2 = input.GetLast(2);

                //MessageBox.Show("TenderSoft Test Message 2: \n Input 1 :" + first2 + " , \n Input 2: " + first3 + " , \n Input 3: " + last2, "TenderSoft Testing");
                 

                // Check if is pre-formatted, or is a command, otherwise process it. 
                //

                // If preformatted
                if (first2 == "LS" || first2 == "LO" || first2 == "LT")
                {
                    ProcessResult(input);
                }
                // If barcode command
                else if (last2 == "**")
                {
                    if (!string.IsNullOrEmpty(ThisIdentifiedItem)) ProcessCommand(input);
                }
                else if (first3 == "lbc")
                {
                    if (!string.IsNullOrEmpty(ThisIdentifiedItem)) ProcessCommand(input);
                }
                // If shipment
                else if (first2 == "1Z" || first2 == "C1")
                {
                    result = IsValidShipment(input);
                    ProcessResult(result);
                }
                else
                {
                    result = IsValidOther(input);
                    ProcessResult(result);
                }
            }
        }

        public void ProcessCommand(string input)
        {
            string extracted = input.Substring(4, input.Length - 4).ToLower();

            if (ThisIdentifiedItem != null)
            {
                if (extracted == "viewitem**" || input == "viewitem**")
                {
                    ViewDetails(null, null);
                }
                else if (extracted == "mrkcmplt**" || input == "mrkcmplt**")
                {
                    Completed();
                }
                else if (extracted == "mrkpkdup**" || input == "mrkpkdup**")
                {
                    PickedUp();
                }
                else if (extracted == "openfldr**" || input == "openfldr**")
                {
                    OpenFolderClick(null, null);
                }
                else
                {

                }
            }
        }

        private void Completed()
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            int thisID = Convert.ToInt32(ThisIdentifiedItem);
            Utilities.MarkHopperOrderCompleted(thisID);
            mvm.ScannerOpen = false;
        }

        private void PickedUp()
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            int thisID = Convert.ToInt32(ThisIdentifiedItem);
            Utilities.MarkHopperOrderPickedUp(thisID);
            mvm.ScannerOpen = false;
        }

        private void ProcessResult(string result)
        {
            string first2 = result.Substring(0, 2);
            string rest = result.Substring(3, result.Length - 3);


            if (result == "FAIL")
            {

            }
            else if (first2 == "LS")
            {
                //          shipment thisshipment;
                //           using (Entities ct = new Entities()) { thisshipment = ct.shipments.FirstOrDefault(m => m.TrackingNum == rest); }
                //           SetLabels("Shipment", thisshipment.Description, rest);
            }
            else if (first2 == "LH")
            {
                int inputAsInt;
                bool IsOnlyNumber = Int32.TryParse(rest, out inputAsInt);
                hopperorder thisorder;
                customer thiscust;
                using (Entities ct = new Entities()) { thisorder = ct.hopperorders.FirstOrDefault(k => k.ReceiptNum == inputAsInt); int idtosearch = thisorder.CustomerID; thiscust = ct.customers.FirstOrDefault(m => m.CustomerID == idtosearch); }
                SetLabels("Order", thiscust.FirstName + " " + thiscust.LastName, rest);
            }
            else if (first2 == "LO")
            {
                int inputAsInt;
                bool IsOnlyNumber = Int32.TryParse(rest, out inputAsInt);
                order thisorder;
                customer thiscust;
                using (Entities ct = new Entities()) { thisorder = ct.orders.FirstOrDefault(k => k.ReceiptNum == inputAsInt); int idtosearch = thisorder.CustomerID; thiscust = ct.customers.FirstOrDefault(m => m.CustomerID == idtosearch); }
                SetLabels("Order", thiscust.FirstName + " " + thiscust.LastName, rest);
            }
            else if (first2 == "LT")
            {

            }
        }


        private string IsValidShipment(string input)
        {
            bool foundShipment = false;

            using (Entities ctx = new Entities())
            {
                //     foundShipment = ctx.shipments.Any(o => o.TrackingNum == input);
            }

            if (foundShipment)
            {
                return "LS-" + input.Trim();
            }
            else return "FAIL";
        }

        public bool foundOrder;
        public bool foundHopper;
        public bool foundTicket;

        private string IsValidOther(string input)
        {
            int inputAsInt;
            bool IsOnlyNumber = Int32.TryParse(input, out inputAsInt);

            foundOrder = false;
            foundHopper = false;
            foundTicket = false;

            if (IsOnlyNumber)
            {
                using (Entities ctx = new Entities())
                {
                    foundOrder = ctx.orders.Any(o => o.ReceiptNum == inputAsInt);

                    if (!foundOrder)
                    {
                        foundHopper = ctx.hopperorders.Any(p => p.ReceiptNum == inputAsInt);
                    }
                }
            }

            //MessageBox.Show("TenderSoft Test Message 3: \n OutInput:" + inputAsInt + "\n IsOnlyNumber:" + IsOnlyNumber + "\n Order Found: " + foundOrder + "\n Hopper Found: " + foundHopper + "\n Ticket Found: " + foundTicket, "TenderSoft Testing");

            if (foundOrder) { return "LO-" + inputAsInt; }
            if (foundHopper) { return "LH-" + inputAsInt; }
            if (foundTicket) { return "LT-" + input; }
            else { Toaster.Notification("Invoice Not Found", "Unable to find an invoice in the system with that ID."); return "FAIL"; }
        }

        public string ThisIdentifiedItem { get; set; }

        private void SetLabels(string type, string item, string scanned)
        {
            if (type == "Order")
            {
             //   IdentifiedImage.Source = new BitmapImage(new Uri("/Images/Scantypes/Invoice.jpg", UriKind.Relative));
            }
            OrderControlButtons.Visibility = System.Windows.Visibility.Visible;
            VisBrush.Visual = App.Current.FindResource("appbar_base") as Visual;
            Spinner.Fill = Brushes.YellowGreen;
            Spinner.Visibility = System.Windows.Visibility.Visible;
            ItemTypeLabel.Visibility = System.Windows.Visibility.Visible;
            ItemTypeLabel.Content = type + ": ";
            ItemNameLabel.Foreground = Brushes.YellowGreen;
            ItemNameLabel.Content = item;
            ThisIdentifiedItem = scanned;
        }

        private void RefocusItem(object sender, MouseButtonEventArgs e)
        {
            ScanBox.Text = "";
            Keyboard.Focus(ScanBox);
            ScanBox.Focus();
        }

        private void UserControl_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            RefocusItem(null, null);
        }

        private void LostFocusEvent(object sender, RoutedEventArgs e)
        {
            RefocusItem(null, null);
        }
    }
}
