﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Management;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.ViewModel;
using Lilypad.Model;

namespace Lilypad.Dialogs
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class YouTubePlayer : UserControl
    {
        public YouTubePlayer(string input)
        {
            InitializeComponent();
            InputURL = input;
        }

        public string InputURL { get; set; }

        private void CompletelyLoaded(object sender, RoutedEventArgs e)
        {
            Browser.Navigate(InputURL);
        }

        private void SubmitDialog(object sender, RoutedEventArgs e)
        {
            MainWindow main = (MainWindow)Application.Current.MainWindow;
            MainViewModel mvm = (MainViewModel)main.DataContext;
            SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
            main.HideMetroDialogAsync(dialog);
        }

        private void GoFullscreen(object sender, RoutedEventArgs e)
        {

        }


    }
}
