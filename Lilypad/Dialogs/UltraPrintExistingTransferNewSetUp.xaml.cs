﻿using DevExpress.Xpf.Editors;
using Lilypad.Model;
using Lilypad.ViewModel;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Lilypad.Dialogs
{
    /// <summary>
    /// Interaction logic for UltraPrintExistingTransferNewSetUp.xaml
    /// </summary>
    public partial class UltraPrintExistingTransferNewSetUp : UserControl
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public int dynamicCount = 0;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public UltraPrintExistingTransferNewSetUp()
        {
            InitializeComponent();
            OrderNewViewModel mvm = new OrderNewViewModel();
            List<ultraprintorderdata> data = new List<ultraprintorderdata>();
            mvm.SelectedultraData = data;
            this.DataContext = mvm;
        }

        public UltraPrintExistingTransferNewSetUp(OrderNewViewModel mvm)
        {
            InitializeComponent();
            List<ultraprintorderdata> deet = new List<ultraprintorderdata>();
            this.DataContext = mvm;
            Label LableError;
            LableError = (Label)FindName("ErrorLabel");
            LableError.Visibility = System.Windows.Visibility.Hidden;
        }


        private void SubmitDialog(object sender, RoutedEventArgs e)
        {
            try
            {
                var vm = (OrderNewViewModel)this.DataContext;
                int loopingCount = dynamicCount;
                if (dynamicCount > 0)
                {
                    for (int i = 1; i <= loopingCount; i++)
                    {
                        ComboBoxEdit textbox1;
                        textbox1 = (ComboBoxEdit)FindName("Location" + i);
                        TextBox textbox2;
                        textbox2 = (TextBox)FindName("Qty" + i);
                        ComboBoxEdit textbox3;
                        textbox3 = (ComboBoxEdit)FindName("Type" + i);
                        TextBox textbox4;
                        textbox4 = (TextBox)FindName("Designname" + i);
                        TextBox textbox5;
                        textbox5 = (TextBox)FindName("Size" + i);
                        ComboBoxEdit textbox6;
                        textbox6 = (ComboBoxEdit)FindName("Vendor" + i);

                        if (String.IsNullOrEmpty(textbox1.Text) || String.IsNullOrEmpty(textbox2.Text) || String.IsNullOrEmpty(textbox3.Text) || String.IsNullOrEmpty(textbox4.Text) || String.IsNullOrEmpty(textbox5.Text) || String.IsNullOrEmpty(textbox6.Text))
                        {

                            Label LableError;
                            LableError = (Label)FindName("ErrorLabel");
                            LableError.Visibility = System.Windows.Visibility.Visible;
                            LableError.Content = "All fields required";
                            LableError.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF"));
                            LableError.FontSize = 25;
                            return;
                        }

                        int distance;
                        if (!int.TryParse(textbox2.Text, out distance))
                        {
                            Label LableError;
                            LableError = (Label)FindName("ErrorLabel");
                            LableError.Visibility = System.Windows.Visibility.Visible;
                            LableError.Content = "Qty must be an integer";
                            LableError.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF"));
                            LableError.FontSize = 23;
                            return;
                        }
                    }
                    for (int i = 1; i <= loopingCount; i++)
                    {
                        ultraprintorderdata deet = new ultraprintorderdata();
                        ComboBoxEdit textbox1;
                        textbox1 = (ComboBoxEdit)FindName("Location" + i);
                        deet.Location = textbox1.Text;
                        TextBox textbox2;
                        textbox2 = (TextBox)FindName("Qty" + i);
                        deet.Qty = Convert.ToInt32(textbox2.Text);
                        ComboBoxEdit textbox3;
                        textbox3 = (ComboBoxEdit)FindName("Type" + i);
                        deet.Type = textbox3.Text;
                        TextBox textbox4;
                        textbox4 = (TextBox)FindName("Designname" + i);
                        deet.DesignName = textbox4.Text;
                        TextBox textbox5;
                        textbox5 = (TextBox)FindName("Size" + i);
                        deet.size = textbox5.Text;
                        ComboBoxEdit textbox6;
                        textbox6 = (ComboBoxEdit)FindName("Vendor" + i);
                        deet.Vendor = textbox6.Text;

                        vm.SelectedultraData.Add(deet);
                    }

                    var main = (MainWindow)Application.Current.MainWindow;
                    SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
                    main.HideMetroDialogAsync(dialog);
                }
                else
                {
                    Label LableError;
                    LableError = (Label)FindName("ErrorLabel");
                    LableError.Visibility = System.Windows.Visibility.Visible;
                    LableError.Content = "Please input no. of designs";
                    LableError.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF0000"));
                    LableError.FontSize = 25;
                    return;
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void CancelDialog(object sender, RoutedEventArgs e)
        {
            var main = (MainWindow)Application.Current.MainWindow;
            SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
            main.HideMetroDialogAsync(dialog);
        }

        private void TextChangedEditor(object sender, TextChangedEventArgs e)
        {
            try
            {
                if ((sender as TextBox).Text == "")
                {
                    DuplicateControls(0);
                }
                else
                {
                    dynamicCount = Convert.ToInt32((sender as TextBox).Text);
                    DuplicateControls(Convert.ToInt32((sender as TextBox).Text));
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void DuplicateControls(int x)
        {
            object item = UltraPrintSetupSP.FindName("LoopingStackPanel");

            Label LableError;
            LableError = (Label)FindName("ErrorLabel");
            LableError.Visibility = System.Windows.Visibility.Hidden;

            if (x > 0)
            {
                if (item is StackPanel)
                {
                    StackPanel wantedChild = item as StackPanel;
                    wantedChild.VerticalAlignment = VerticalAlignment.Bottom;
                    wantedChild.Orientation = Orientation.Vertical;
                    StackPanel spp = new StackPanel();
                    spp.Orientation = Orientation.Horizontal;
                    Label l1 = new Label();
                    l1.Content = "Location";
                    l1.Width = 114;
                    l1.FontSize = 14;
                    l1.FontFamily = new FontFamily("Roboto");
                    l1.Margin = new Thickness(0, 20, 10, 0);
                    l1.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF"));
                    wantedChild.Children.Add(spp);
                    spp.Children.Add(l1);

                    Label l2 = new Label();
                    l2.Content = "Qty";
                    l2.Width = 48;
                    l2.FontSize = 14;
                    l2.Margin = new Thickness(60, 20, 40, 0);
                    l2.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF"));
                    spp.Children.Add(l2);

                    Label l6 = new Label();
                    l6.Content = "Vendor";
                    l6.Width = 166;
                    l6.FontSize = 14;
                    l6.Margin = new Thickness(-20, 20, -5, 0);
                    l6.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF"));
                    spp.Children.Add(l6);

                    Label l3 = new Label();
                    l3.Content = "Type";
                    l3.Width = 166;
                    l3.FontSize = 14;
                    l3.Margin = new Thickness(-40, 20, -110, 0);
                    l3.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF"));
                    spp.Children.Add(l3);

                    Label l4 = new Label();
                    l4.Content = "Design Name";
                    l4.Width = 274;
                    l4.FontSize = 14;
                    l4.Margin = new Thickness(160, 20, -180, 0);
                    l4.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF"));
                    spp.Children.Add(l4);

                    Label l5 = new Label();
                    l5.Content = "Size";
                    l5.Width = 174;
                    l5.FontSize = 14;
                    l5.Margin = new Thickness(160, 20, -257, 0);
                    l5.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF"));
                    spp.Children.Add(l5);

                    for (int i = 1; i <= x; i++)
                    {
                        wantedChild.VerticalAlignment = VerticalAlignment.Bottom;
                        wantedChild.Orientation = Orientation.Vertical;
                        StackPanel sp = new StackPanel();
                        sp.Orientation = Orientation.Horizontal;
                        sp.Name = "sp" + i;

                        ComboBoxEdit cmb = new ComboBoxEdit();
                        cmb.AutoComplete = true;
                        cmb.ShowError = false;
                        cmb.ShowErrorToolTip = false;
                        cmb.ShowBorder = true;
                        cmb.BorderThickness = new Thickness(12, 12, 12, 12);
                        cmb.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("White"));
                        cmb.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Gray"));
                        cmb.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Transparent"));

                        cmb.VerticalContentAlignment = VerticalAlignment.Center;
                        cmb.Style = (Style)FindResource("Light");
                        cmb.Items.Add("Front");
                        cmb.Items.Add("Back");
                        cmb.Items.Add("FLC");
                        cmb.Items.Add("FRC");
                        cmb.Items.Add("Left Sleeve");
                        cmb.Items.Add("Right Sleeve");
                        cmb.Items.Add("Yoke");
                        cmb.Items.Add("Other/Custom");
                        cmb.Name = "Location" + i;
                        cmb.Height = 28;
                        cmb.Width = 150;
                        cmb.FontSize = 12;
                        cmb.Margin = new Thickness(0, 20, 10, 0);
                        this.RegisterName("Location" + i, cmb);
                        wantedChild.Children.Add(sp);
                        sp.Children.Add(cmb);

                        TextBox txtQty = new TextBox();
                        txtQty.Name = "Qty" + i;
                        txtQty.Height = 28;
                        txtQty.Width = 50;
                        txtQty.MaxLength = 4;
                        txtQty.FontSize = 12;
                        txtQty.PreviewTextInput += Textbox_OnPreviewTextInput;
                        txtQty.Style = this.Resources["MyWaterMarkStyleQty"] as Style;
                        txtQty.Margin = new Thickness(10, 20, 30, 0);
                        this.RegisterName("Qty" + i, txtQty);
                        sp.Children.Add(txtQty);

                        ComboBoxEdit cmbVendor = new ComboBoxEdit();
                        cmbVendor.AutoComplete = true;
                        cmbVendor.ShowError = false;
                        cmbVendor.ShowErrorToolTip = false;
                        cmbVendor.BorderBrush = Brushes.WhiteSmoke;
                        cmbVendor.Foreground = Brushes.Gray;
                        cmbVendor.Background = Brushes.Transparent;
                        cmbVendor.VerticalContentAlignment = VerticalAlignment.Center;
                        cmbVendor.SelectedIndexChanged += VendorComboBox_SelectionChanged;
                        cmbVendor.Style = (Style)FindResource("Light");
                        cmbVendor.Items.Add("F&M Expressions");
                        cmbVendor.Items.Add("613 Originals");
                        cmbVendor.Items.Add("Apex");
                        cmbVendor.Items.Add("Other");
                        cmbVendor.Name = "Vendor" + i;
                        cmbVendor.Height = 28;
                        cmbVendor.Width = 120;
                        cmbVendor.FontSize = 12;
                        cmbVendor.Margin = new Thickness(0, 20, 30, 0);
                        this.RegisterName("Vendor" + i, cmbVendor);
                        sp.Children.Add(cmbVendor);

                        ComboBoxEdit txtType = new ComboBoxEdit();
                        txtType.AutoComplete = true;
                        txtType.ShowError = false;
                        txtType.ShowErrorToolTip = false;
                        txtType.BorderBrush = Brushes.WhiteSmoke;
                        txtType.Foreground = Brushes.Gray;
                        txtType.Background = Brushes.Transparent;
                        txtType.VerticalContentAlignment = VerticalAlignment.Center;
                        txtType.Style = (Style)FindResource("Light");
                        txtType.Name = "Type" + i;
                        txtType.Height = 28;
                        txtType.Width = 200;
                        txtType.MaxLength = 50;
                        txtType.FontSize = 12;
                        txtType.CharacterCasing = CharacterCasing.Upper;
                        txtType.Margin = new Thickness(-25, 20, -155, 0);
                        this.RegisterName("Type" + i, txtType);
                        sp.Children.Add(txtType);

                        TextBox txtDesignName = new TextBox();
                        txtDesignName.Name = "Designname" + i;
                        txtDesignName.Height = 28;
                        txtDesignName.Width = 230;
                        txtDesignName.CharacterCasing = CharacterCasing.Upper;
                        txtDesignName.FontSize = 12;
                        txtDesignName.Style = this.Resources["MyWaterMarkStyleDesignname"] as Style;
                        txtDesignName.Margin = new Thickness(170, 20, -237, 0);
                        this.RegisterName("Designname" + i, txtDesignName);
                        sp.Children.Add(txtDesignName);

                        TextBox txtSize = new TextBox();
                        txtSize.Name = "Size" + i;
                        txtSize.Height = 28;
                        txtSize.Width = 230;
                        txtSize.MaxLength = 50;
                        txtSize.FontSize = 12;
                        txtSize.Style = this.Resources["MyWaterMarkStyleSize"] as Style;
                        txtSize.CharacterCasing = CharacterCasing.Upper;
                        txtSize.Margin = new Thickness(260, 20, -280, 0);
                        this.RegisterName("Size" + i, txtSize);
                        sp.Children.Add(txtSize);
                    }
                }
            }
            else
            {
                clearData();
            }
        }


        private void Textbox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsAllowed(e.Text);
        }

        private static bool IsAllowed(string text)
        {
            var regex = new Regex("[^0-9]+"); //regex that matches only allowed numbers
            return regex.IsMatch(text);
        }

        private void ComboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DuplicateControls(0);
            DuplicateControls(Convert.ToInt32(((System.Windows.Controls.ContentControl)((object[])e.AddedItems)[0]).Content));
            dynamicCount = Convert.ToInt32(((System.Windows.Controls.ContentControl)((object[])e.AddedItems)[0]).Content);
        }

        private void VendorComboBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string loop = string.Empty;
            string sendercontrol = ((System.Windows.FrameworkElement)sender).Name;
            if (sendercontrol.Length <= 7) { loop = sendercontrol.Substring(sendercontrol.Length - 1, 1); }
            else {  loop = sendercontrol.Substring(sendercontrol.Length-2); }

            ComboBoxEdit combo1 = new ComboBoxEdit();
            combo1 = (ComboBoxEdit)FindName("Type" + loop);
            combo1.EditValue = "";
            ComboBoxEdit combo2 = new ComboBoxEdit();
            combo2 = (ComboBoxEdit)FindName("Type" + loop);
            combo2.EditValue = "";
            ComboBoxEdit combo4 = new ComboBoxEdit();
            combo4 = (ComboBoxEdit)FindName("Type" + loop);
            combo4.EditValue = "";
            ComboBoxEdit combo5 = new ComboBoxEdit();
            combo5 = (ComboBoxEdit)FindName("Type" + loop);
            combo5.EditValue = "";

            if (Convert.ToString(((BaseEdit)sender).EditValue) == "613 Originals") {
                combo1.Items.Clear();
                combo1.Items.Add("Single Image, One Color, 15 Cent");
                combo1.Items.Add("Single Image, One Color, 22 Cent");
                combo1.Items.Add("Single Image, Two Color, 75 Cent");
                combo1.Items.Add("Single Image, Full Color, $1.15");
                combo1.Items.Add("Varsity Formula, Spot Color, 1-3 Colors");
                combo1.Items.Add("Varsity Formula, Full Color");
                combo1.Items.Add("Varsity Neon");
                combo1.Items.Add("Stretch Formula");
                combo1.Items.Add("Classic Formula, Spot Color, 1-3 Colors");
                combo1.Items.Add("Classic Formula, Full Color");
                combo1.Items.Add("Classic Neon");
                combo1.Items.Add("Names");
                combo1.Items.Add("Numbers");
                combo1.Items.Add("Speciality");
                combo1.Items.Add("Other");
            }
            if (Convert.ToString(((BaseEdit)sender).EditValue) == "Apex") {
                combo2.Items.Clear();
                combo2.Items.Add("Screen Printed Transfers");
                combo2.Items.Add("Custom Applique");
                combo2.Items.Add("Custom Rhinestones");
                combo2.Items.Add("Digital Transfers");
            }
            if (Convert.ToString(((BaseEdit)sender).EditValue) == "F&M Expressions") {
                combo4.Items.Clear();
                combo4.Items.Add("One Color Spot (9 x 12.75)");
                combo4.Items.Add("One Color Spot (12 x 12)");
                combo4.Items.Add("One Color Spot Athletic Formula");
                combo4.Items.Add("Two Color Spot Athletic Formula");
                combo4.Items.Add("Three Color Spot Athletic Formula");
                combo4.Items.Add("Full Color Athletic Formula");
                combo4.Items.Add("One Color Spot Vintage Formula");
                combo4.Items.Add("Two Color Spot Vintage Formula");
                combo4.Items.Add("Three Color Spot Vintage Formula");
                combo4.Items.Add("One Color Spot Performance Formula");
                combo4.Items.Add("Two Color Spot Performance Formula");
                combo4.Items.Add("Three Color Spot Performance Formula");
                combo4.Items.Add("One Color Spot Fashion Formula");
                combo4.Items.Add("Two Color Spot Fashion Formula");
                combo4.Items.Add("Three Color Spot Fashion Formula");
                combo4.Items.Add("Full Color Fashion Light Formula");
                combo4.Items.Add("Full Color Fashion Dark Formula");
                combo4.Items.Add("Full Color Promo Dark");
                combo4.Items.Add("Full Color Promo Light");
                combo4.Items.Add("Names");
                combo4.Items.Add("Numbers");
                combo4.Items.Add("Other");
            }
            if (Convert.ToString(((BaseEdit)sender).EditValue) == "Other") {
                combo5.Items.Clear();
            }

        }

        public void clearData()
        {
            object item = UltraPrintSetupSP.FindName("LoopingStackPanel");
            StackPanel s = item as StackPanel;
            s.Children.RemoveRange(0, s.Children.Count);

            for (int i = 1; i <= dynamicCount; i++)
            {
                this.UnregisterName("Location" + i);
                this.UnregisterName("Qty" + i);
                this.UnregisterName("Type" + i);
                this.UnregisterName("Designname" + i);
                this.UnregisterName("Size" + i);
                this.UnregisterName("Vendor" + i);

            }
            dynamicCount = 0;
        }
    }
}
