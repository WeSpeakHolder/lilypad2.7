﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Management;
using System.Threading.Tasks;
using System.Windows;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ookii.Dialogs.Wpf;
using MahApps.Metro.Controls;
using MahApps.Metro;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.ViewModel;
using Lilypad.Model;
using Lilypad.Views;
using Lilypad.Controls.DesignCards;

namespace Lilypad.Dialogs
{

    class NetworkManagement
    {

        public string GetIP()
        {
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            string result = "";

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {

                    string[] thisIP = (string[])objMO["IPAddress"];
                    result = thisIP[0];
                }
            }

            return result;
        }
        public void setIP(string ip_address, string subnet_mask)
        {
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {
                    try
                    {
                        ManagementBaseObject setIP;
                        ManagementBaseObject newIP = objMO.GetMethodParameters("EnableStatic");

                        newIP["IPAddress"] = new string[] { ip_address };
                        newIP["SubnetMask"] = new string[] { subnet_mask };

                        setIP = objMO.InvokeMethod("EnableStatic", newIP, null);
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                }
            }
        }

        public void SetStaticIP()
        {
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {
                    try
                    {
                        ManagementBaseObject setIP;
                        ManagementBaseObject newIP = objMO.GetMethodParameters("EnableStatic");
                        ManagementBaseObject newGateway = objMO.GetMethodParameters("SetGateways");

                        string[] thisIP = (string[])objMO["IPAddress"];
                        string[] thisSubnet = (string[])objMO["IPSubnet"];
                        string[] thisGateway = (string[])objMO["DefaultIPGateway"];

                        newGateway["DefaultIPGateway"] = new string[] { thisGateway[0] };
                        newGateway["GatewayCostMetric"] = new int[] { 1 };

                        newIP["IPAddress"] = new string[] { thisIP[0] };
                        newIP["SubnetMask"] = new string[] { thisSubnet[0] };

                        setIP = objMO.InvokeMethod("EnableStatic", newIP, null);
                        setIP = objMO.InvokeMethod("SetGateways", newGateway, null);

                        MessageBox.Show("Updated IP Successfully.");
                    }

                    catch (Exception)
                    {
                        throw;
                    }

                }
            }
        }


        public void setDNS(string NIC, string DNS)
        {
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {
                    // if you are using the System.Net.NetworkInformation.NetworkInterface you'll need to change this line to if (objMO["Caption"].ToString().Contains(NIC)) and pass in the Description property instead of the name 
                    if (objMO["Caption"].Equals(NIC))
                    {
                        try
                        {
                            ManagementBaseObject newDNS =
                                objMO.GetMethodParameters("SetDNSServerSearchOrder");
                            newDNS["DNSServerSearchOrder"] = DNS.Split(',');
                            ManagementBaseObject setDNS =
                                objMO.InvokeMethod("SetDNSServerSearchOrder", newDNS, null);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
            }
        }

        public void setWINS(string NIC, string priWINS, string secWINS)
        {
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {
                    if (objMO["Caption"].Equals(NIC))
                    {
                        try
                        {
                            ManagementBaseObject setWINS;
                            ManagementBaseObject wins =
                            objMO.GetMethodParameters("SetWINSServer");
                            wins.SetPropertyValue("WINSPrimaryServer", priWINS);
                            wins.SetPropertyValue("WINSSecondaryServer", secWINS);

                            setWINS = objMO.InvokeMethod("SetWINSServer", wins, null);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
            }
        }
    }
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class InitialSetup : UserControl
    {
        public InitialSetup()
        {
            InitializeComponent();
        }

       
        private SetupCard selectedcard;
        public SetupCard SelectedCard
        {
            get { return selectedcard; }
            set { selectedcard = value; }
        }

        private void Grid_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            var parent = Utilities.FindParentByType<TransitioningContentControl>(this);
            var newPres = new MainMenu();
            parent.Content = newPres;
        }

        private void SubmitDialog(object sender, RoutedEventArgs e)
        {
            var main = (MainWindow)Application.Current.MainWindow;
            SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
            DesignSetupCardViewModel asvm = (DesignSetupCardViewModel)this.DataContext;
            asvm.SaveChanges();
            main.HideMetroDialogAsync(dialog);

        }

        private void CancelDialog(object sender, RoutedEventArgs e)
        {
            var main = (MainWindow)Application.Current.MainWindow;
            SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
            main.HideMetroDialogAsync(dialog);
        }

        private void SetupLoaded(object sender, RoutedEventArgs e)
        {

        }

        private void FindFolderResource(object sender, EventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            string path = dialog.SelectedPath;
            ResourceDataPath.Text = path;
        }

        private void FindFolderTemplates(object sender, EventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            string path = dialog.SelectedPath;
            TemplatePath.Text = path;
        }

        private void FindFolderPending(object sender, EventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            string path = dialog.SelectedPath;
            WorkingPath.Text = path;
        }

        private void FindFolderProduction(object sender, EventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            string path = dialog.SelectedPath;
            ProductionPath.Text = path;
        }

        private void FindFolderArchive(object sender, EventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            string path = dialog.SelectedPath;
            ArchivePath.Text = path;
        }

        private void FindFolderAlpha(object sender, EventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            string path = dialog.SelectedPath;
            AlphaPath.Text = path;
        }

        private void RunSetupClicked(object sender, EventArgs e)
        {
            if (SetupFile.Text.Length < 1)
            {
                new NetworkManagement().SetStaticIP();
                try
                {
                    string host = "localhost";
                    string user = "root";
                    string database = "mysql";
                    string password = "";
                    string ConnectionString = "Data Source=" + host + ";User ID=" + user + ";Password=" + password;
                    string Query0 = @"UPDATE mysql.user SET Password = PASSWORD('C$#Jh1AC&nY3') WHERE User = 'root';";
                    string Query1 = @"CREATE USER 'lilyAdmin'@'localhost' IDENTIFIED BY 'C$#Jh1AC&nY3';
GRANT ALL PRIVILEGES ON *.* TO 'lilyAdmin'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;";

                    MySqlConnection connection1 = new MySqlConnection(ConnectionString);
                    MySqlCommand command0 = new MySqlCommand(Query0, connection1);
                    MySqlCommand command1 = new MySqlCommand(Query1, connection1);

                    connection1.Open();
                    command0.ExecuteNonQuery();
                    command1.ExecuteNonQuery();
                    connection1.Close();

                    string output = "**IP:" + new NetworkManagement().GetIP() + "**PASS:C$#Jh1AC&nY3";
                    string path = ProductionPath.Text + "\\Lilypad_Config.txt";
                    System.IO.TextWriter writer = new System.IO.StreamWriter(path);
                    writer.WriteLine(output);
                    writer.Close();
                    MessageBox.Show("Completed");
                    Properties.Settings.Default.App_FirstRun = false;
                    Properties.Settings.Default.MySQL_ServerIP = new NetworkManagement().GetIP();
                    Properties.Settings.Default.MySQL_Password = "C$#Jh1AC&nY3";
                    Properties.Settings.Default.Data_AlphaData = AlphaPath.Text;
                    Properties.Settings.Default.Data_ArchiveFolderPath = ArchivePath.Text;
                    Properties.Settings.Default.Data_PendingFolderPath = WorkingPath.Text;
                    Properties.Settings.Default.Data_ProductionFolderPath = ProductionPath.Text;
                    Properties.Settings.Default.Data_ResourcePath = ResourceDataPath.Text;
                    Properties.Settings.Default.Data_TemplatesPath = TemplatePath.Text;
                    Properties.Settings.Default.Save();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            else
            {
                System.IO.TextReader reader = new System.IO.StreamReader(SetupFile.Text);
                string outputR = reader.ReadToEnd();
                try
                {
                    string[] hostsplit = new string[] { "**IP:" };
                    string[] passsplit = new string[] { "**PASS:" };
                    string host = outputR.Split(hostsplit, StringSplitOptions.RemoveEmptyEntries)[0];
                    string user = "root";
                    string database = "mysql";
                    string password = outputR.Split(passsplit, StringSplitOptions.RemoveEmptyEntries)[0];
                    string ConnectionString = "Data Source=" + host + ";User ID=" + user + ";Password=" + password;
                    string Query0 = @"UPDATE mysql.user SET Password = PASSWORD('C$#Jh1AC&nY3') WHERE User = 'root';";
                    string Query1 = @"CREATE USER 'lilyAdmin'@'localhost' IDENTIFIED BY 'C$#Jh1AC&nY3';
GRANT ALL PRIVILEGES ON *.* TO 'lilyAdmin'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;";

                    MySqlConnection connection1 = new MySqlConnection(ConnectionString);
                    MySqlCommand command0 = new MySqlCommand(Query0, connection1);
                    MySqlCommand command1 = new MySqlCommand(Query1, connection1);

                    connection1.Open();
                    command0.ExecuteNonQuery();
                    command1.ExecuteNonQuery();
                    connection1.Close();

                    string output = "**IP:" + new NetworkManagement().GetIP() + "**PASS:C$#Jh1AC&nY3";
                    string path = ProductionPath.Text + "\\Lilypad_Config.txt";
                    System.IO.TextWriter writer = new System.IO.StreamWriter(path);
                    writer.WriteLine(output);
                    writer.Close();
                    MessageBox.Show("Completed");
                    Properties.Settings.Default.App_FirstRun = false;
                    Properties.Settings.Default.MySQL_ServerIP = host;
                    Properties.Settings.Default.MySQL_Password = "C$#Jh1AC&nY3";
                    Properties.Settings.Default.Data_AlphaData = AlphaPath.Text;
                    Properties.Settings.Default.Data_ArchiveFolderPath = ArchivePath.Text;
                    Properties.Settings.Default.Data_PendingFolderPath = WorkingPath.Text;
                    Properties.Settings.Default.Data_ProductionFolderPath = ProductionPath.Text;
                    Properties.Settings.Default.Data_ResourcePath = ResourceDataPath.Text;
                    Properties.Settings.Default.Data_TemplatesPath = TemplatePath.Text;
                    Properties.Settings.Default.Save();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            CancelDialog(null, null);
        }

        private void RunWampSetupClicked(object sender, EventArgs e)
        {
            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = "wampinstall.exe";
            p.Start();
        }

        private void FindSetupFile(object sender, EventArgs e)
        {
            var dialog = new VistaOpenFileDialog();
            dialog.ShowDialog();
            string path = dialog.FileName;
            SetupFile.Text = path;
        }
    }
}
