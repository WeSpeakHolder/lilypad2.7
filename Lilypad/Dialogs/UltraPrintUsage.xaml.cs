﻿using Lilypad.Model;
using Lilypad.ViewModel;
using Lilypad.Views;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Lilypad.Dialogs
{
    /// <summary>
    /// Interaction logic for UltraPrint.xaml
    /// </summary>
    public partial class UltraPrintUsage : UserControl
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public int dynamicCount = 0;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public UltraPrintUsage()
        {
            InitializeComponent();
            OrderNewViewModel mvm = new OrderNewViewModel();
            List<ultraprintorderdata> data = new List<ultraprintorderdata>();
            mvm.SelectedultraData = data;
            this.DataContext = mvm;
        }
        
        public UltraPrintUsage(HopperViewModel mvm)
        {
            InitializeComponent();
            List<ultraprintorderdata> deet = new List<ultraprintorderdata>();
            this.DataContext = mvm;
            try
            {
                Label LableError;
                LableError = (Label)FindName("ErrorLabel");
                LableError.Visibility = System.Windows.Visibility.Hidden;

                object item = UltraPrintUsageSP.FindName("LoopingUsageStackPanel");
                List<ultraprintorderdata> list = new List<ultraprintorderdata>();

                if (mvm.SelectedOrder != null)
                {
                    using (Entities ctx = new Entities())
                    {
                        list =
                            (from k in ctx.ultraprintorderdatas where k.OrderID == mvm.SelectedOrder.OrderID select k)
                                .ToList();
                    }
                    if (item is StackPanel)
                    {
                        StackPanel wantedChild = item as StackPanel;
                        wantedChild.Width = 1100;
                        for (int i = 0; i < list.Count; i++)
                        {
                            wantedChild.VerticalAlignment = VerticalAlignment.Bottom;
                            wantedChild.Orientation = Orientation.Vertical;

                            StackPanel sp = new StackPanel();
                            sp.Orientation = Orientation.Horizontal;
                            sp.Name = "sp" + i;

                            Label txtLocation = new Label();
                            txtLocation.Name = "Location" + i;
                            txtLocation.Height = 30;
                            txtLocation.Width = 100;
                            txtLocation.FontSize = 15;
                            txtLocation.Margin = new Thickness(-5, 20, 10, 0);
                            txtLocation.Content = list[i].Location;
                            txtLocation.Foreground = new BrushConverter().ConvertFromString("#ffffff") as System.Windows.Media.Brush;
                            this.RegisterName("Location" + i, txtLocation);
                            wantedChild.Children.Add(sp);
                            sp.Children.Add(txtLocation);

                            Label txtQty = new Label();
                            txtQty.Name = "Qty" + i;
                            txtQty.Height = 30;
                            txtQty.Width = 48;
                            txtQty.FontSize = 15;
                            txtQty.Content = list[i].Qty;
                            txtQty.Margin = new Thickness(-5, 20, 30, 0);
                            txtQty.Foreground = new BrushConverter().ConvertFromString("#ffffff") as System.Windows.Media.Brush;
                            this.RegisterName("Qty" + i, txtQty);
                            sp.Children.Add(txtQty);

                            Label txtVendor = new Label();
                            txtVendor.Name = "Vendor" + i;
                            txtVendor.Height = 30;
                            txtVendor.Width = 156;
                            txtVendor.Content = list[i].Vendor;
                            txtVendor.FontSize = 15;
                            txtVendor.Foreground = new BrushConverter().ConvertFromString("#ffffff") as System.Windows.Media.Brush;
                            txtVendor.Margin = new Thickness(-10, 20, -140, 0);
                            this.RegisterName("Vendor" + i, txtVendor);
                            sp.Children.Add(txtVendor);


                            Label txtType = new Label();
                            txtType.Name = "Type" + i;
                            txtType.Height = 30;
                            txtType.Width = 156;
                            txtType.Content = list[i].Type;
                            txtType.FontSize = 15;
                            txtType.Foreground = new BrushConverter().ConvertFromString("#ffffff") as System.Windows.Media.Brush;
                            txtType.Margin = new Thickness(90, 20, -300, 0);
                            this.RegisterName("Type" + i, txtType);
                            sp.Children.Add(txtType);


                            Label txtDesignName = new Label();
                            txtDesignName.Name = "Designname" + i;
                            txtDesignName.Height = 30;
                            txtDesignName.Width = 171;
                            txtDesignName.FontSize = 15;
                            txtDesignName.Foreground = new BrushConverter().ConvertFromString("#ffffff") as System.Windows.Media.Brush;
                            txtDesignName.Content = list[i].DesignName;
                            txtDesignName.Margin = new Thickness(280, 20, -237, 0);
                            this.RegisterName("Designname" + i, txtDesignName);
                            sp.Children.Add(txtDesignName);

                            Label txtSize = new Label();
                            txtSize.Name = "Size" + i;
                            txtSize.Height = 30;
                            txtSize.Width = 151;
                            txtSize.FontSize = 15;
                            txtSize.Foreground = new BrushConverter().ConvertFromString("#ffffff") as System.Windows.Media.Brush;
                            txtSize.Content = list[i].size;
                            txtSize.Margin = new Thickness(280, 20, -247, 0);
                            this.RegisterName("Size" + i, txtSize);
                            sp.Children.Add(txtSize);


                            TextBox txtUsed = new TextBox();
                            txtUsed.Name = "Used" + i;
                            txtUsed.Height = 30;
                            txtUsed.Width = 50;
                            txtUsed.MaxLength = 4;
                            txtUsed.PreviewTextInput += Textbox_OnPreviewTextInput;
                            txtUsed.FontSize = 15;
                            txtUsed.Margin = new Thickness(250, 20, -240, 0);
                            this.RegisterName("Used" + i, txtUsed);
                            sp.Children.Add(txtUsed);

                            TextBox txtBin = new TextBox();
                            txtBin.Name = "Bin" + i;
                            txtBin.Height = 30;
                            txtBin.Width = 70;                           
                            txtBin.FontSize = 15;
                            txtBin.MaxLength = 3;                            
                            txtBin.PreviewTextInput += Textbox_OnPreviewTextInput;
                            // txtBin.CharacterCasing = CharacterCasing.Upper;
                            txtBin.Margin = new Thickness(290, 20, -287, 0);
                            this.RegisterName("Bin" + i, txtBin);
                            sp.Children.Add(txtBin);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void Textbox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsAllowed(e.Text);            
        }

        private static bool IsAllowed(string text)
        {            
            var regex = new Regex("[^0-9]+"); //regex that matches only allowed numbers
            return regex.IsMatch(text);
        }

        private async void SubmitDialog(object sender, RoutedEventArgs e)
        {
            try
            {
                List<ultraprintorderdata> list = new List<ultraprintorderdata>();
                ultraprintorderdata existinglist = new ultraprintorderdata();
                HopperViewModel mvm = (HopperViewModel)this.DataContext;

                using (Entities ctx = new Entities())
                {
                    list = (from t in ctx.ultraprintorderdatas where t.OrderID == mvm.SelectedOrder.OrderID select t).ToList();

                    for (int i = 0; i < list.Count; i++)
                    {
                        TextBox textbox1;
                        textbox1 = (TextBox)FindName("Used" + i);
                        TextBox textbox2;
                        textbox2 = (TextBox)FindName("Bin" + i);
                        textbox1.Text = textbox1.Text.Trim();
                        textbox2.Text = textbox2.Text.Trim();
                        textbox1.Text = Regex.Replace(textbox1.Text, @"\s+", "");
                        textbox2.Text = Regex.Replace(textbox1.Text, @"\s+", "");

                        if (String.IsNullOrEmpty(textbox1.Text) || String.IsNullOrEmpty(textbox2.Text))
                        {
                            Label LableError;
                            LableError = (Label)FindName("ErrorLabel");
                            LableError.Visibility = System.Windows.Visibility.Visible;
                            LableError.Content = "All fields required";
                            LableError.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF"));
                            LableError.FontSize = 25;
                            return;
                        }
                    }

                    for (int i = 0; i < list.Count; i++)
                    {
                        TextBox textbox1;
                        textbox1 = (TextBox)FindName("Used" + i);
                        list[i].Used = Convert.ToInt32(textbox1.Text);
                        TextBox textbox2;
                        textbox2 = (TextBox)FindName("Bin" + i);
                        list[i].Bin = textbox2.Text;

                        //Newly added
                        list[i].GridUsed = Convert.ToInt32(textbox1.Text);
                        list[i].GridBin = textbox2.Text;

                        ctx.SaveChanges();
                        int? id = list[i].TransferedFrom;
                        List<ultraprintorderdata> upod = new List<ultraprintorderdata>();
                        if (list[i].TransferedFrom != null && list[i].TransferedFrom > 0)
                        {
                            upod = (from t in ctx.ultraprintorderdatas where t.Id == id select t).ToList();
                            if (upod != null && upod.Count() > 0)
                            {
                                foreach (var item in upod)
                                {
                                    item.Used = Convert.ToInt32(textbox1.Text);
                                    item.Bin = textbox2.Text;
                                }
                            }
                            ctx.SaveChanges();
                 
                            upod = (from t in ctx.ultraprintorderdatas where t.TransferedFrom == id select t).ToList();

                            if (upod != null && upod.Count() > 0)
                            {
                                foreach (var item in upod)
                                {
                                    item.Used = Convert.ToInt32(textbox1.Text);
                                    item.Bin = textbox2.Text;
                                }
                            }
                            ctx.SaveChanges();
                        }
                        else
                        {//need to check this scenario/done
                            int? ids = list[i].Id;
                            upod = (from t in ctx.ultraprintorderdatas where t.TransferedFrom == ids select t).ToList();
                            if (upod != null && upod.Count() > 0)
                            {
                                foreach (var item in upod)
                                {
                                    item.Used = Convert.ToInt32(textbox1.Text);
                                    item.Bin = textbox2.Text;
                                }
                            }
                            ctx.SaveChanges();
                        }
                    }
                }

                var main = (MainWindow)Application.Current.MainWindow;
                SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
                await main.HideMetroDialogAsync(dialog);
                Hopper hop = new Hopper();

                hop.MarkCompletedConfirm((HopperViewModel)this.DataContext);
            }
            catch (Exception ex)
            {
            }
        }

        private void CancelDialog(object sender, RoutedEventArgs e)
        {
            var main = (MainWindow)Application.Current.MainWindow;
            SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
            main.HideMetroDialogAsync(dialog);
        }

        private void TextChangedEditor(object sender, TextChangedEventArgs e)
        {

            if ((sender as TextBox).Text == "")
            {
                DuplicateControls(0);
            }
            else
            {
                dynamicCount = Convert.ToInt32((sender as TextBox).Text);
                DuplicateControls(Convert.ToInt32((sender as TextBox).Text));
            }
        }


        public void DuplicateControls(int x)
        {
            try
            {
                object item = UltraPrintUsageSP.FindName("LoopingUsageStackPanel");
                if (x > 0)
                {
                    if (item is StackPanel)
                    {
                        StackPanel wantedChild = item as StackPanel;
                        //wantedChild.Width = 1100;
                        for (int i = 1; i <= x; i++)
                        {
                            wantedChild.VerticalAlignment = VerticalAlignment.Bottom;
                            wantedChild.Orientation = Orientation.Vertical;

                            StackPanel sp = new StackPanel();
                            sp.Orientation = Orientation.Horizontal;
                            sp.Name = "sp" + i;

                            TextBox txtLocation = new TextBox();
                            txtLocation.Name = "Location" + i;
                            txtLocation.Height = 28;
                            txtLocation.Width = 100;
                            txtLocation.FontSize = 18;
                            txtLocation.Margin = new Thickness(-20, 20, 0, 0);
                            txtLocation.Padding = new Thickness(0, 0, 0, 0);
                            this.RegisterName("Location" + i, txtLocation);
                            wantedChild.Children.Add(sp);
                            sp.Children.Add(txtLocation);

                            TextBox txtQty = new TextBox();
                            txtQty.Name = "Qty" + i;
                            txtQty.Height = 28;
                            txtQty.Width = 50;
                            txtQty.MaxLength = 2;
                            txtQty.FontSize = 18;
                            txtQty.Margin = new Thickness(20, 20, 50, 0);
                            this.RegisterName("Qty" + i, txtQty);
                            sp.Children.Add(txtQty);

                            TextBox txtVendor = new TextBox();
                            txtVendor.Name = "Vendor" + i;
                            txtVendor.Height = 28;
                            txtVendor.Width = 285;

                            txtVendor.FontSize = 18;
                            txtVendor.Margin = new Thickness(-28, 20, -250, 0);
                            this.RegisterName("Vendor" + i, txtVendor);
                            sp.Children.Add(txtVendor);

                            TextBox txtType = new TextBox();
                            txtType.Name = "Type" + i;
                            txtType.Height = 28;
                            txtType.Width = 285;

                            txtType.FontSize = 18;
                            txtType.Margin = new Thickness(-28, 20, -125, 0);
                            this.RegisterName("Type" + i, txtType);
                            sp.Children.Add(txtType);


                            TextBox txtDesignName = new TextBox();
                            txtDesignName.Name = "Designname" + i;
                            txtDesignName.Height = 28;
                            txtDesignName.Width = 266;
                            txtLocation.MaxLength = 50;
                            txtDesignName.FontSize = 18;
                            txtDesignName.Margin = new Thickness(140, 20, -237, 0);
                            this.RegisterName("Designname" + i, txtDesignName);
                            sp.Children.Add(txtDesignName);
                        }

                    }
                }
                else
                {
                    StackPanel s = item as StackPanel;
                    s.Children.RemoveRange(0, s.Children.Count);

                    for (int i = 1; i <= dynamicCount; i++)
                    {
                        this.UnregisterName("Location" + i);
                        this.UnregisterName("Qty" + i);
                        this.UnregisterName("Type" + i);
                        this.UnregisterName("Designname" + i);
                        this.UnregisterName("Size" + i);
                        this.UnregisterName("Used" + i);
                        this.UnregisterName("Bin" + i);
                        this.UnregisterName("Vendor" + i);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
