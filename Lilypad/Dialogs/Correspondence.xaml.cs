﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.ViewModel;
using Lilypad.Model;
using Lilypad.Views;
using Lilypad.Controls.DesignCards;

namespace Lilypad.Dialogs
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Correspondence : UserControl
    {

        public Correspondence()
        {
            InitializeComponent();
        }

        public int correspondenceType;

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            var main = (MainWindow)Application.Current.MainWindow;
            SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
            main.HideMetroDialogAsync(dialog);
        }

        private void pageLoaded(object sender, RoutedEventArgs e)
        {
            CorrespondenceViewModel cvm = (CorrespondenceViewModel)this.DataContext;
            DateTime dnow = DateTime.Now;
            DateTime now = dnow.AddDays(1);
            int nowhour;
            if (now.Minute > 29) nowhour = now.Hour + 1;
            else nowhour = now.Hour;
            string editorDate = now.Year + "-" + now.Month + "-" + now.Day + " " + nowhour + ":00:00";
            var dDate = DateTime.Parse(editorDate);
            cvm.CallTime = dDate;
            int corr = cvm.CompletedOrder.Correspondence;
            correspondenceType = corr;
            string corrtext;
            if (corr == 0)
            { corrtext = "Legacy"; SetEmailInvis(); SubmitButton.Content = "Complete Order"; }
            else if (corr == 1)
            { corrtext = "Call"; SetEmailInvis(); SubmitButton.Content = "Add to Call List and Complete Order"; }
            else if (corr == 2)
            { corrtext = "Email"; SetCallInvis(); SubmitButton.Content = "Email Customer and Complete Order"; }
            else
            { corrtext = "Call and Email"; SubmitButton.Content = "Add to Call List, Send Email, and Complete Order"; }
            StatusLabel.Content = corrtext;
        }

        private void SetEmailInvis()
        {
            EmailSP1.Visibility = System.Windows.Visibility.Collapsed;
            EmailSP2.Visibility = System.Windows.Visibility.Collapsed;
            EmailSP3.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void SetCallInvis()
        {
            CallSP.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void SubmitClick(object sender, RoutedEventArgs e)
        {
            try
            {
                CorrespondenceViewModel cvm = (CorrespondenceViewModel)this.DataContext;

                calllist newlist = new calllist() { HopperReceipt = cvm.CompletedOrder.ReceiptNum, Archived = false, CallDateTime = cvm.CallTime, Called = false, ThankYou = false };
                if (correspondenceType == 0)
                { using (Entities ctx = new Entities()) { ctx.calllists.Add(newlist); ctx.SaveChanges(); } }
                else if (correspondenceType == 1)
                { using (Entities ctx = new Entities()) { ctx.calllists.Add(newlist); ctx.SaveChanges(); } }
                else if (correspondenceType == 2)
                {
                    if (Properties.Settings.Default.Interop_UseGmail) { new Interop.GmailHelper().Send(cvm.EmailTo, cvm.EmailSubject, cvm.EmailText, null); }
                    else { new Interop.SMTPHelper().SendEmail(Properties.Settings.Default.Interop_SMTPServer, Properties.Settings.Default.Interop_SMTPPort, Properties.Settings.Default.Interop_SMTPUseSSL, cvm.EmailTo, cvm.EmailSubject, cvm.EmailText, null); }
                }
                else if (correspondenceType == 3)
                {
                    using (Entities ctx = new Entities())
                    {
                        if (string.IsNullOrEmpty(EmailBox.Text))
                        {
                            newlist.ThankYou = false;
                        }
                        else
                        {
                            newlist.ThankYou = true;
                        }

                        ctx.calllists.Add(newlist);
                        ctx.SaveChanges();
                    }

                    if (Properties.Settings.Default.Interop_UseGmail) { new Interop.GmailHelper().Send(cvm.EmailTo, cvm.EmailSubject, cvm.EmailText, null); }
                    else { new Interop.SMTPHelper().SendEmail(Properties.Settings.Default.Interop_SMTPServer, Properties.Settings.Default.Interop_SMTPPort, Properties.Settings.Default.Interop_SMTPUseSSL, cvm.EmailTo, cvm.EmailSubject, cvm.EmailText, null); }
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }

            var main = (MainWindow)Application.Current.MainWindow;
            SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
            main.HideMetroDialogAsync(dialog);
        }

    }
}
