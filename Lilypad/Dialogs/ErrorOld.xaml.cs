﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Management;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.ViewModel;
using Lilypad.Model;

namespace Lilypad.Dialogs
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class ErrorOld : UserControl
    {
        public ErrorOld(Exception ex)
        {
            InitializeComponent();
            ErrorViewModel mvm = new ErrorViewModel();
            mvm.CaughtException = ex;
            LocalException = ex;
            this.DataContext = mvm;
            EFrogger = ((MainViewModel)App.Current.MainWindow.DataContext).LoggedInFrogger.FrogFirstName.ToString();
            uploader = new BackgroundWorker();
            uploader.WorkerReportsProgress = true;
            uploader.WorkerSupportsCancellation = true;
            uploader.ProgressChanged += uploader_ProgressChanged;
            uploader.RunWorkerCompleted += uploader_RunWorkerCompleted;
            uploader.DoWork += uploader_DoWork;
        }

        public string EFrogger;

        Exception LocalException;
        public bool ErrorUploading;
        void uploader_DoWork(object sender, DoWorkEventArgs e)
        {
            // ErrorViewModel mvm = (ErrorViewModel)this.DataContext;
            Exception ex = LocalException;
            ErrorUploading = false;

            try
            {
                uploader.ReportProgress(15, "Collecting error information...");
                // Create new cloud model
                if (Utilities.CloudisOnline())
                {
                    using (Cloud cl = new Cloud())
                    {
                        error nE = new error();
                        nE.ErrorDateTime = DateTime.Now;
                        nE.ErrorMachineName = System.Environment.MachineName.ToString();
                        nE.ErrorMachineOS = (from x in new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem").Get().OfType<ManagementObject>() select x.GetPropertyValue("Caption")).FirstOrDefault().ToString();
                        nE.ErrorMachineUptime = (DateTime.Now - Process.GetCurrentProcess().StartTime).ToString();
                        nE.StoreID = Convert.ToInt32(Properties.Settings.Default.Store_ID);
                        nE.ErrorEmployee = EFrogger;
                        nE.Archived = false;

                        string sT = "";

                        if (ex is System.Data.Entity.Validation.DbEntityValidationException)
                        {
                            System.Data.Entity.Validation.DbEntityValidationException eVV = (System.Data.Entity.Validation.DbEntityValidationException)ex;
                            foreach (var evt in eVV.EntityValidationErrors)
                            {
                                sT += "ENTITYVLD: Type: " + evt.Entry.Entity.GetType().Name.ToString() + " | State: " + evt.Entry.State.ToString() + Environment.NewLine;
                                foreach (var evx in evt.ValidationErrors)
                                {
                                    sT += "--- Property: " + evx.PropertyName.ToString() + " | Error: " + evx.ErrorMessage.ToString() + Environment.NewLine;
                                }
                            }
                        }

                        if (ex.InnerException != null)
                        {
                            nE.ErrorExceptionClass = "INNER: " + ex.InnerException.ToString() + " | CLASS: " + ex.GetType().ToString();
                            nE.ErrorExceptionMessage = "INNER: " + ex.InnerException.ToString() + " | MSG: " + ex.Message.ToString();

                            if (ex.StackTrace != null)
                            {
                                sT += "INNER: " + ex.InnerException.ToString() + " | STACK: " + ex.StackTrace.ToString();
                            }
                            else
                            {
                                sT += "STACKTRACE_NULL";
                            }
                        }
                        else
                        {
                            nE.ErrorExceptionClass = ex.GetType().ToString();
                            nE.ErrorExceptionMessage = ex.Message.ToString();

                            if (ex.StackTrace != null)
                            {
                                sT += ex.StackTrace.ToString();
                            }
                            else
                            {
                                sT += "STACKTRACE_NULL";
                            }
                        }

                        nE.ErrorExceptionStackTrace = sT;

                        if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                        {
                            string host = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily.ToString() == "InterNetwork").ToString();
                            nE.ErrorMachineIP = host;
                        }
                        else
                        {
                            nE.ErrorMachineIP = "N/A";
                        }

                        nE.StoreIPAddress = "N/A";


                        uploader.ReportProgress(75, "Uploading error report to Lilypad cloud server...");

                        try
                        {
                            cl.errors.Add(nE);
                            cl.SaveChanges();
                            uploader.ReportProgress(100, "Error successfully reported.");
                        }
                        catch
                        {

                            uploader.ReportProgress(0, "Failed to upload error report to cloud server, though the local log has been saved.");
                            ErrorUploading = true;
                        }
                    }
                }
            }
            catch (Exception exe)
            {
                uploader.ReportProgress(0, exe.Message);
                ErrorUploading = true;
            }
        }

        void uploader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            if (ErrorUploading) { statusSpinner.IsError = true; }
            else
            {
                statusSpinner.IsDone = true;
            }
        }

        void uploader_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string status = e.UserState.ToString();
            StatusLabel.Content = status;
        }

        BackgroundWorker uploader;


        private void SubmitDialog(object sender, RoutedEventArgs e)
        {
            MainWindow main = (MainWindow)Application.Current.MainWindow;
            MainViewModel mvm = (MainViewModel)main.DataContext;
            SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
            main.HideMetroDialogAsync(dialog);
        }

        private void CompletelyLoaded(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.System_ErrorReportingEnabled)
            {
                RetrieverSP.Visibility = System.Windows.Visibility.Visible;
                uploader.RunWorkerAsync();
            }

        }
    }
}
