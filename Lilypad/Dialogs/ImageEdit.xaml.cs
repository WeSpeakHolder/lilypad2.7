﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.ViewModel;
using Lilypad.Model;
using Ookii.Dialogs.Wpf;

namespace Lilypad.Dialogs
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class ImageEdit : UserControl
    {
        public ImageEdit()
        {
            InitializeComponent();
            DesignSetupCardViewModel mvm = new DesignSetupCardViewModel();
            designdetail deet = new designdetail();
            mvm.SelectedDesignDetail = deet;
            this.DataContext = mvm;
        }

        public ImageEdit(EmployeesViewModel input)
        {
            InitializeComponent();
            this.DataContext = input;
        }

        private void SubmitDialog(object sender, RoutedEventArgs e)
        {
            var main = (MainWindow)Application.Current.MainWindow;
            SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
            main.HideMetroDialogAsync(dialog);

        }

        private void CancelDialog(object sender, RoutedEventArgs e)
        {
            var main = (MainWindow)Application.Current.MainWindow;
            SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
            main.HideMetroDialogAsync(dialog);
        }



        private void SetupLoaded(object sender, RoutedEventArgs e)
        {
            string win7dir = @"C:\ProgramData\Microsoft\User Account Pictures\Default Pictures";
            string win8dir = @"C:\ProgramData\Microsoft\User Account Pictures";

            if (Directory.Exists(win7dir))
            {
                foreach (string file in Directory.GetFiles(win7dir, "*.jpg", SearchOption.AllDirectories))
                {
                    DevExpress.Xpf.LayoutControl.Tile nt = new DevExpress.Xpf.LayoutControl.Tile();
                    nt.Margin = new Thickness(5, 2, 0, 2);
                    nt.Width = 100;
                    nt.Height = 100;
                    nt.Tag = file;
                    nt.Click += nt_Click;

                        BitmapImage newImg = new BitmapImage(new Uri(file, UriKind.Absolute));
                        ImageBrush nb = new ImageBrush(newImg);
                        nt.Background = nb;

                    ImagesSP.Children.Add(nt);
                }
            }
            else if (Directory.Exists(win8dir))
            {
                foreach (string file in Directory.GetFiles(win8dir, "*.bmp", SearchOption.AllDirectories))
                {
                    DevExpress.Xpf.LayoutControl.Tile nt = new DevExpress.Xpf.LayoutControl.Tile();
                    nt.Margin = new Thickness(5, 2, 0, 2);
                    nt.Width = 100;
                    nt.Height = 100;
                    nt.Tag = file;
                    nt.Click += nt_Click;

                    BitmapImage newImg = new BitmapImage(new Uri(file, UriKind.Absolute));
                    ImageBrush nb = new ImageBrush(newImg);
                    nt.Background = nb;

                    ImagesSP.Children.Add(nt);
                }
            }

        }

        void nt_Click(object sender, EventArgs e)
        {
            DevExpress.Xpf.LayoutControl.Tile tT = (DevExpress.Xpf.LayoutControl.Tile)sender;
            string path = tT.Tag.ToString();

            PathBox.Text = path;
        }

        private void EditPathClicked(object sender, EventArgs e)
        {
            StackPanel sp = Utilities.FindParentByType<StackPanel>(((DependencyObject)sender));
            TextBox tb = (TextBox)sp.FindChildren<TextBox>(true).First();
            var dialog = new VistaOpenFileDialog();
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.FileName))
            {
                tb.Text = dialog.FileName.ToString();
            }
        }
    }
}
