﻿using Lilypad.Model;
using Lilypad.ViewModel;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Lilypad.Dialogs
{
    /// <summary>
    /// Interaction logic for UltraPrintExistingTransfer.xaml
    /// </summary>
    public partial class UltraPrintExistingTransfer : UserControl
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public int dynamicCount = 0;
        List<ultraprintorderdata> globallist = new List<ultraprintorderdata>();
        List<int> selectedList = new List<int>();

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public UltraPrintExistingTransfer()
        {
            InitializeComponent();
            OrderNewViewModel mvm = new OrderNewViewModel();
            List<ultraprintorderdata> data = new List<ultraprintorderdata>();
            mvm.SelectedultraData = data;
            this.DataContext = mvm;
        }

        void chk_Click(object sender, RoutedEventArgs e)
        {
            CheckBox chx = sender as CheckBox;
            if (chx != null)
            {
                string temp = chx.Name;
                string x = string.Empty;
                if (temp.Length <= 9) { x = temp.Substring(temp.Length - 1, 1); }
                else { x = temp.Substring(temp.Length - 2); }

                if (chx.IsChecked == true)
                    selectedList.Add(Convert.ToInt32(x));
                else
                    selectedList.Remove(Convert.ToInt32(x));
            }
        }

        public UltraPrintExistingTransfer(OrderNewViewModel mvm)
        {
            InitializeComponent();
            List<ultraprintorderdata> deet = new List<ultraprintorderdata>();
            mvm.SelectedultraData = deet;
            this.DataContext = mvm;

            try
            {
                Label LableError;
                LableError = (Label)FindName("ErrorLabel");
                LableError.Visibility = System.Windows.Visibility.Hidden;
                object item = UltraPrintTransfer.FindName("LoopingUsageStackPanel");
                List<ultraprintorderdata> list = new List<ultraprintorderdata>();
                List<ultraprintorderdata> matchedTransferlist = new List<ultraprintorderdata>();
                List<ultraprintorderdata> finalTransferScreen = new List<ultraprintorderdata>();
                if (mvm.SelectedOrderNewVM != null)
                {
                    using (Entities ctx = new Entities())
                    {
                        list =
                           (from q in ctx.ultraprintorderdatas
                            where q.CustFirstName == mvm.SelectedOrderNewVM.NewHopperOrder.CustFirst &&
                                    q.CustLastName == mvm.SelectedOrderNewVM.NewHopperOrder.CustLast &&
                                    q.CustCompany == mvm.SelectedOrderNewVM.NewHopperOrder.CustCompany
                            select q).ToList();

                        if (list.Count() > 0)
                        {
                            foreach (var items in list)
                            {
                                ultraprintorderdata finaltest = new ultraprintorderdata();
                                finaltest = (from q in ctx.ultraprintorderdatas
                                             where
          q.CustFirstName == mvm.SelectedOrderNewVM.NewHopperOrder.CustFirst &&
 q.CustLastName == mvm.SelectedOrderNewVM.NewHopperOrder.CustLast &&
 q.CustCompany == mvm.SelectedOrderNewVM.NewHopperOrder.CustCompany &&
                     q.Id == items.TransferedFrom
                                             select q
                                         ).FirstOrDefault();
                                if (finaltest != null) { matchedTransferlist.Add(items); }
                            }
                            finalTransferScreen = list.Where(p => !matchedTransferlist.Any(p2 => p2.Id == p.Id)).ToList();
                            if (finalTransferScreen != null && finalTransferScreen.Count == 0) { finalTransferScreen = list; }
                        }
                    }
                    if (item is StackPanel)
                    {
                        StackPanel wantedChild = item as StackPanel;
                        wantedChild.Width = 1100;
                        for (int i = 0; i < finalTransferScreen.Count; i++)
                        {
                            wantedChild.VerticalAlignment = VerticalAlignment.Bottom;
                            wantedChild.Orientation = Orientation.Vertical;
                            StackPanel sp = new StackPanel();
                            sp.Orientation = Orientation.Horizontal;
                            sp.Name = "sp" + i;

                            Label txtLocation = new Label();
                            txtLocation.Name = "Location" + i;
                            txtLocation.Height = 30;
                            txtLocation.Width = 100;
                            txtLocation.FontSize = 15;
                            txtLocation.Margin = new Thickness(-5, 20, 10, 0);
                            txtLocation.Content = finalTransferScreen[i].Location;
                            txtLocation.Foreground = new BrushConverter().ConvertFromString("#ffffff") as System.Windows.Media.Brush;
                            this.RegisterName("Location" + i, txtLocation);
                            wantedChild.Children.Add(sp);
                            sp.Children.Add(txtLocation);

                            Label txtQty = new Label();
                            txtQty.Name = "Qty" + i;
                            txtQty.Height = 30;
                            txtQty.Width = 48;
                            txtQty.FontSize = 15;
                            txtQty.Content = finalTransferScreen[i].Qty;
                            txtQty.Margin = new Thickness(-5, 20, 30, 0);
                            txtQty.Foreground = new BrushConverter().ConvertFromString("#ffffff") as System.Windows.Media.Brush;
                            this.RegisterName("Qty" + i, txtQty);
                            sp.Children.Add(txtQty);

                            Label txtVendor = new Label();
                            txtVendor.Name = "Vendor" + i;
                            txtVendor.Height = 30;
                            txtVendor.Width = 156;
                            txtVendor.Content = finalTransferScreen[i].Vendor;
                            txtVendor.FontSize = 15;
                            txtVendor.Foreground = new BrushConverter().ConvertFromString("#ffffff") as System.Windows.Media.Brush;
                            txtVendor.Margin = new Thickness(-10, 20, -140, 0);
                            this.RegisterName("Vendor" + i, txtVendor);
                            sp.Children.Add(txtVendor);

                            Label txtType = new Label();
                            txtType.Name = "Type" + i;
                            txtType.Height = 30;
                            txtType.Width = 200;
                            txtType.Content = finalTransferScreen[i].Type;
                            txtType.FontSize = 15;
                            txtType.Foreground = new BrushConverter().ConvertFromString("#ffffff") as System.Windows.Media.Brush;
                            txtType.Margin = new Thickness(130, 20, -300, 0);
                            this.RegisterName("Type" + i, txtType);
                            sp.Children.Add(txtType);

                            Label txtDesignName = new Label();
                            txtDesignName.Name = "Designname" + i;
                            txtDesignName.Height = 30;
                            txtDesignName.Width = 171;
                            txtDesignName.FontSize = 15;
                            txtDesignName.Foreground = new BrushConverter().ConvertFromString("#ffffff") as System.Windows.Media.Brush;
                            txtDesignName.Content = finalTransferScreen[i].DesignName;
                            txtDesignName.Margin = new Thickness(360, 20, -280, 0);
                            this.RegisterName("Designname" + i, txtDesignName);
                            sp.Children.Add(txtDesignName);

                            Label txtSize = new Label();
                            txtSize.Name = "Size" + i;
                            txtSize.Height = 30;
                            txtSize.Width = 151;
                            txtSize.FontSize = 15;
                            txtSize.Foreground = new BrushConverter().ConvertFromString("#ffffff") as System.Windows.Media.Brush;
                            txtSize.Content = finalTransferScreen[i].size;
                            txtSize.Margin = new Thickness(365, 20, -320, 0);
                            this.RegisterName("Size" + i, txtSize);
                            sp.Children.Add(txtSize);

                            CheckBox chk = new CheckBox();
                            chk.Name = "Checkbox" + i;
                            chk.Click += chk_Click;
                            chk.Margin = new Thickness(380, 20, -240, 0);
                            this.RegisterName("Checkbox" + i, chk);
                            sp.Children.Add(chk);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private async void SubmitDialog(object sender, RoutedEventArgs e)
        {
            var vm = (OrderNewViewModel)this.DataContext;
            List<ultraprintorderdata> matchedTransferlist = new List<ultraprintorderdata>();
            List<ultraprintorderdata> finalTransferScreen = new List<ultraprintorderdata>();
            try
            {
                using (Entities ctx = new Entities())
                {
                    List<ultraprintorderdata> list =
                           (from q in ctx.ultraprintorderdatas
                            where
                                q.CustFirstName == vm.SelectedOrderNewVM.NewHopperOrder.CustFirst &&
                            q.CustLastName == vm.SelectedOrderNewVM.NewHopperOrder.CustLast &&
                                  q.CustCompany == vm.SelectedOrderNewVM.NewHopperOrder.CustCompany
                            select q).ToList();

                    if (list.Count() > 0)
                    {
                        foreach (var items in list)
                        {
                            ultraprintorderdata finaltest = new ultraprintorderdata();
                            finaltest = (from q in ctx.ultraprintorderdatas
                                         where
                                              q.CustFirstName == vm.SelectedOrderNewVM.NewHopperOrder.CustFirst &&
                                        q.CustLastName == vm.SelectedOrderNewVM.NewHopperOrder.CustLast &&
                                        q.CustCompany == vm.SelectedOrderNewVM.NewHopperOrder.CustCompany &&
                                                         q.Id == items.TransferedFrom
                                         select q
                                     ).FirstOrDefault();
                            if (finaltest != null) { matchedTransferlist.Add(items); }
                        }

                        finalTransferScreen = list.Where(p => !matchedTransferlist.Any(p2 => p2.Id == p.Id)).ToList();

                        if (finalTransferScreen != null && finalTransferScreen.Count == 0) { finalTransferScreen = list; }

                        if (selectedList.Count() > 0)
                        {
                            foreach (var item in selectedList)
                            {
                                ultraprintorderdata deet = new ultraprintorderdata();
                                deet.Location = finalTransferScreen[item].Location;
                                deet.Qty = finalTransferScreen[item].Qty;
                                deet.Type = finalTransferScreen[item].Type;
                                deet.DesignName = finalTransferScreen[item].DesignName;
                                deet.size = finalTransferScreen[item].size;
                                deet.Vendor = finalTransferScreen[item].Vendor;
                                deet.Used = finalTransferScreen[item].Used;
                                deet.Bin = finalTransferScreen[item].Bin;
                                deet.CustFirstName = finalTransferScreen[item].CustFirstName;
                                deet.CustLastName = finalTransferScreen[item].CustLastName;
                                deet.TransferedFrom = finalTransferScreen[item].Id;
                                vm.SelectedultraData.Add(deet);
                                ctx.SaveChanges();
                            }
                        }

                        var main = (MainWindow)Application.Current.MainWindow;
                        SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
                        await main.HideMetroDialogAsync(dialog);
                        Utilities.ShowUltraPrintExistingTransferSetup(vm);

                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void CancelDialog(object sender, RoutedEventArgs e)
        {
            var vm = (OrderNewViewModel)this.DataContext;
            List<ultraprintorderdata> matchedTransferlist = new List<ultraprintorderdata>();
            List<ultraprintorderdata> finalTransferScreen = new List<ultraprintorderdata>();
            try
             {
                using (Entities ctx = new Entities())
                {
                    List<ultraprintorderdata> list =
                           (from q in ctx.ultraprintorderdatas
                            where
                                q.CustFirstName == vm.SelectedOrderNewVM.NewHopperOrder.CustFirst &&
                            q.CustLastName == vm.SelectedOrderNewVM.NewHopperOrder.CustLast &&
                                  q.CustCompany == vm.SelectedOrderNewVM.NewHopperOrder.CustCompany
                            select q).ToList();

                    if (list.Count() > 0)
                    {
                        foreach (var items in list)
                        {
                            ultraprintorderdata finaltest = new ultraprintorderdata();
                            finaltest = (from q in ctx.ultraprintorderdatas
                                         where
                                              q.CustFirstName == vm.SelectedOrderNewVM.NewHopperOrder.CustFirst &&
                                        q.CustLastName == vm.SelectedOrderNewVM.NewHopperOrder.CustLast &&
                                        q.CustCompany == vm.SelectedOrderNewVM.NewHopperOrder.CustCompany &&
                                                         q.Id == items.TransferedFrom
                                         select q
                                     ).FirstOrDefault();
                            if (finaltest != null) { matchedTransferlist.Add(items); }
                        }

                        finalTransferScreen = list.Where(p => !matchedTransferlist.Any(p2 => p2.Id == p.Id)).ToList();

                        if (finalTransferScreen != null && finalTransferScreen.Count == 0)
                        {
                            finalTransferScreen = list;
                        }


                        if (selectedList.Count() > 0)
                        {
                            foreach (var item in selectedList)
                            {
                                ultraprintorderdata deet = new ultraprintorderdata();
                                deet.Location = finalTransferScreen[item].Location;
                                deet.Qty = finalTransferScreen[item].Qty;
                                deet.Type = finalTransferScreen[item].Type;
                                deet.DesignName = finalTransferScreen[item].DesignName;
                                deet.size = finalTransferScreen[item].size;
                                deet.Vendor = finalTransferScreen[item].Vendor;
                                deet.Used = finalTransferScreen[item].Used;
                                deet.Bin = finalTransferScreen[item].Bin;
                                deet.CustFirstName = finalTransferScreen[item].CustFirstName;
                                deet.CustLastName = finalTransferScreen[item].CustLastName;
                                deet.TransferedFrom = finalTransferScreen[item].Id;
                                vm.SelectedultraData.Add(deet);
                            }
                        }
                        else
                        {
                            Label LableError;
                            LableError = (Label)FindName("ErrorLabel");
                            LableError.Visibility = System.Windows.Visibility.Visible;
                            LableError.Content = "Please select atleast one transfer to link";
                            LableError.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF"));
                            LableError.FontSize = 20;
                            return;
                        }

                        var main = (MainWindow)Application.Current.MainWindow;
                        SimpleDialog dialog = Utilities.FindParentByType<SimpleDialog>(this);
                        main.HideMetroDialogAsync(dialog);
                    }
                }

            }
            catch (Exception ex)
            {
            }
        }

        private void TextChangedEditor(object sender, TextChangedEventArgs e)
        {
            if ((sender as TextBox).Text == "")
            {
                DuplicateControls(0);
            }
            else
            {
                dynamicCount = Convert.ToInt32((sender as TextBox).Text);
                DuplicateControls(Convert.ToInt32((sender as TextBox).Text));
            }
        }


        public void DuplicateControls(int x)
        {
            object item = UltraPrintTransfer.FindName("LoopingUsageStackPanel");
            if (x > 0)
            {
                if (item is StackPanel)
                {
                    StackPanel wantedChild = item as StackPanel;
                    //wantedChild.Width = 1100;
                    for (int i = 1; i <= x; i++)
                    {
                        wantedChild.VerticalAlignment = VerticalAlignment.Bottom;
                        wantedChild.Orientation = Orientation.Vertical;

                        StackPanel sp = new StackPanel();
                        sp.Orientation = Orientation.Horizontal;
                        sp.Name = "sp" + i;

                        TextBox txtLocation = new TextBox();
                        txtLocation.Name = "Location" + i;
                        txtLocation.Height = 28;
                        txtLocation.Width = 100;
                        txtLocation.FontSize = 18;
                        txtLocation.Margin = new Thickness(-20, 20, 0, 0);
                        txtLocation.Padding = new Thickness(0, 0, 0, 0);
                        this.RegisterName("Location" + i, txtLocation);
                        wantedChild.Children.Add(sp);
                        sp.Children.Add(txtLocation);


                        TextBox txtQty = new TextBox();
                        txtQty.Name = "Qty" + i;
                        txtQty.Height = 28;
                        txtQty.Width = 50;
                        txtQty.MaxLength = 2;
                        txtQty.FontSize = 18;
                        txtQty.Margin = new Thickness(20, 20, 50, 0);
                        this.RegisterName("Qty" + i, txtQty);
                        sp.Children.Add(txtQty);

                        TextBox txtVendor = new TextBox();
                        txtVendor.Name = "Vendor" + i;
                        txtVendor.Height = 28;
                        txtVendor.Width = 285;

                        txtVendor.FontSize = 18;
                        txtVendor.Margin = new Thickness(-28, 20, -250, 0);
                        this.RegisterName("Vendor" + i, txtVendor);
                        sp.Children.Add(txtVendor);

                        TextBox txtType = new TextBox();
                        txtType.Name = "Type" + i;
                        txtType.Height = 28;
                        txtType.Width = 285;

                        txtType.FontSize = 18;
                        txtType.Margin = new Thickness(-28, 20, -125, 0);
                        this.RegisterName("Type" + i, txtType);
                        sp.Children.Add(txtType);

                        TextBox txtDesignName = new TextBox();
                        txtDesignName.Name = "Designname" + i;
                        txtDesignName.Height = 28;
                        txtDesignName.Width = 266;
                        txtLocation.MaxLength = 50;
                        txtDesignName.FontSize = 18;
                        txtDesignName.Margin = new Thickness(140, 20, -237, 0);
                        this.RegisterName("Designname" + i, txtDesignName);
                        sp.Children.Add(txtDesignName);
                    }

                }
            }
            else
            {
                StackPanel s = item as StackPanel;
                s.Children.RemoveRange(0, s.Children.Count);

                for (int i = 1; i <= dynamicCount; i++)
                {
                    this.UnregisterName("Location" + i);
                    this.UnregisterName("Qty" + i);
                    this.UnregisterName("Type" + i);
                    this.UnregisterName("Designname" + i);
                    this.UnregisterName("Size" + i);
                    this.UnregisterName("Used" + i);
                    this.UnregisterName("Bin" + i);
                    this.UnregisterName("Vendor" + i);
                    this.UnregisterName("Checkbox" + i);
                }
            }
        }
    }
}
