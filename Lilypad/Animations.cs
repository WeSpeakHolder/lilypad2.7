﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Lilypad
{
    public class ProgressBarSmoother
    {
        public static readonly DependencyProperty SmoothValueProperty =
            DependencyProperty.RegisterAttached("SmoothValue", typeof(double), typeof(ProgressBarSmoother),
                new PropertyMetadata(0.0, Changing));

        public static double GetSmoothValue(DependencyObject obj)
        {
            return (double)obj.GetValue(SmoothValueProperty);
        }

        public static void SetSmoothValue(DependencyObject obj, double value)
        {
            obj.SetValue(SmoothValueProperty, value);
        }

        private static void Changing(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var anim = new DoubleAnimation((double)e.OldValue, (double)e.NewValue, new TimeSpan(0, 0, 0, 0, 250));
            var progressBar = d as ProgressBar;
            if (progressBar != null) progressBar.BeginAnimation(RangeBase.ValueProperty, anim, HandoffBehavior.Compose);
        }
    }

    public static class Animations
    {
        public static void Rotate(this FrameworkElement target, TimeSpan duration, double degrees)
        {
            var rt = new RotateTransform();
            target.RenderTransform = rt;
            var anim1 = new DoubleAnimation(degrees, duration);
            rt.BeginAnimation(RotateTransform.AngleProperty, anim1);
        }

        public static void RotateToFrom(this FrameworkElement target, TimeSpan duration, double degreesFrom,
            double degreesTo)
        {
            var rt = new RotateTransform();
            rt = new RotateTransform();
            target.RenderTransform = rt;
            var anim1 = new DoubleAnimation(degreesFrom, degreesTo, duration);
            rt.BeginAnimation(RotateTransform.AngleProperty, anim1);
        }

        public static void LeftSlide(this FrameworkElement target, TimeSpan duration)
        {
            var width = target.ActualWidth;
            var opTs = duration + TimeSpan.FromMilliseconds(150);
            var trans = new TranslateTransform();
            target.RenderTransform = trans;
            var anim1 = new DoubleAnimation(0, -width, duration);
            var anim2 = new DoubleAnimation(0, opTs);
            var qease = new QuadraticEase { EasingMode = EasingMode.EaseOut };
            anim1.EasingFunction = qease;
            anim2.EasingFunction = qease;
            trans.BeginAnimation(TranslateTransform.XProperty, anim1);
            target.BeginAnimation(UIElement.OpacityProperty, anim2);
        }

        public static void HeightWidthAnimation(this FrameworkElement target, double heightWidth, TimeSpan duration)
        {
            var anim1 = new DoubleAnimation(heightWidth, duration);
            var qease = new QuadraticEase { EasingMode = EasingMode.EaseInOut };
            anim1.EasingFunction = qease;
            if (target is Ellipse)
            {
                target.BeginAnimation(FrameworkElement.HeightProperty, anim1);
                target.BeginAnimation(FrameworkElement.WidthProperty, anim1);
            }
            if (target is Rectangle)
            {
                target.BeginAnimation(FrameworkElement.HeightProperty, anim1);
                target.BeginAnimation(FrameworkElement.WidthProperty, anim1);
            }
        }

        public static void WidthAnimation(this FrameworkElement target, double width, TimeSpan duration)
        {
            var anim1 = new DoubleAnimation(width, duration);
            var qease = new QuadraticEase { EasingMode = EasingMode.EaseInOut };
            anim1.EasingFunction = qease;
            if (target is Ellipse)
            {
                target.BeginAnimation(FrameworkElement.WidthProperty, anim1);
            }
            if (target is Rectangle)
            {
                target.BeginAnimation(FrameworkElement.WidthProperty, anim1);
            }
            if (target is Grid)
            {
                target.BeginAnimation(FrameworkElement.WidthProperty, anim1);
            }
            if (target is StackPanel)
            {
                target.BeginAnimation(FrameworkElement.WidthProperty, anim1);
            }
        }

        public static void HeightAnimation(this FrameworkElement target, double height, TimeSpan duration)
        {
            var anim1 = new DoubleAnimation(height, duration);
            var qease = new QuadraticEase { EasingMode = EasingMode.EaseInOut };
            anim1.EasingFunction = qease;
            if (target is Ellipse)
            {
                target.BeginAnimation(FrameworkElement.HeightProperty, anim1);
            }
            if (target is Rectangle)
            {
                target.BeginAnimation(FrameworkElement.HeightProperty, anim1);
            }
            if (target is Grid)
            {
                target.BeginAnimation(FrameworkElement.HeightProperty, anim1);
            }
            if (target is StackPanel)
            {
                target.BeginAnimation(FrameworkElement.HeightProperty, anim1);
            }
            else
            {
                try
                {
                    target.BeginAnimation(FrameworkElement.HeightProperty, anim1);
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }

        public static void SlideTo(this FrameworkElement target, double fromValue, double toValue, TimeSpan duration)
        {
            var trans = new TranslateTransform();
            target.RenderTransform = trans;
            var anim1 = new DoubleAnimation(fromValue, toValue, duration);
            var qease = new QuadraticEase { EasingMode = EasingMode.EaseInOut };
            anim1.EasingFunction = qease;
            trans.BeginAnimation(TranslateTransform.XProperty, anim1);
        }

        public static void RightSlide(this FrameworkElement target, TimeSpan duration)
        {
            var width = target.ActualWidth;
            var opTs = duration + TimeSpan.FromMilliseconds(150);
            var trans = new TranslateTransform();
            target.RenderTransform = trans;
            var anim1 = new DoubleAnimation(0, width, duration);
            var anim2 = new DoubleAnimation(0, opTs);
            var qease = new QuadraticEase { EasingMode = EasingMode.EaseOut };
            anim1.EasingFunction = qease;
            anim2.EasingFunction = qease;
            trans.BeginAnimation(TranslateTransform.XProperty, anim1);
            target.BeginAnimation(UIElement.OpacityProperty, anim2);
        }

        public static void DownwardSlide(this UIElement target, TimeSpan duration)
        {
            var opTs = duration + TimeSpan.FromMilliseconds(150);
            var trans = new TranslateTransform();
            target.RenderTransform = trans;
            var anim1 = new DoubleAnimation(-20, 0, duration);
            var anim2 = new DoubleAnimation(0, 1, opTs);
            trans.BeginAnimation(TranslateTransform.YProperty, anim1);
            target.BeginAnimation(UIElement.OpacityProperty, anim2);
        }

        public static void UpwardSlide(this UIElement target, TimeSpan duration)
        {
            var opTs = duration + TimeSpan.FromMilliseconds(150);
            var trans = new TranslateTransform();
            target.RenderTransform = trans;
            var anim1 = new DoubleAnimation(-20, 0, duration);
            var anim2 = new DoubleAnimation(0, 1, opTs);
            trans.BeginAnimation(TranslateTransform.YProperty, anim1);
          //  target.BeginAnimation(UIElement.OpacityProperty, anim2);
        }

        public static void OpacityAnimation(this FrameworkElement target, double toValue, TimeSpan duration)
        {
            var anim1 = new DoubleAnimation(toValue, duration);
            var qease = new QuadraticEase { EasingMode = EasingMode.EaseOut };
            anim1.EasingFunction = qease;
            target.BeginAnimation(UIElement.OpacityProperty, anim1);
        }
    }
}