﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows;
using System.Configuration;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using DevExpress.Xpf.Mvvm;
using DevExpress.Xpf.WindowsUI;
using Lilypad.ViewModel;
using System.Windows.Navigation;
using DevExpress.Xpf.Core;

namespace Lilypad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow, INotifyPropertyChanged
    {
        public MainWindow()
        {
            InitializeComponent();
            if (Properties.Settings.Default.App_FirstRun == true)
            {
                Properties.Settings.Default.App_FirstRun = false;
                Properties.Settings.Default.Save();
            }
            this.DataContext = new ViewModel.MainViewModel();
            Utilities.UpdateConfig();
        }

        private Theme selectedtheme;
        public Theme SelectedTheme
        { 
            get { return selectedtheme; }
            set
            {
                selectedtheme = value;
                OnPropertyChanged("SelectedTheme");
            }
        }

         #region PropertyChanged Controller

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler ResultPropertyChanged;

        protected virtual void OnResultPropertyChanged(string propertyName)
        {
            var handler = ResultPropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                MessageBox.Show("Property updated");
            }
        }

        #endregion PropertyChanged Controller

        private void MinButtonClick(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.WindowState = System.Windows.WindowState.Minimized;
        }

        private void MaxButtonClick(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.WindowState = System.Windows.WindowState.Maximized;
        }

        private void CloseButtonClick(object sender, MouseButtonEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Transitioned(object sender, RoutedEventArgs e)
        {

        }

        private void LilypadClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            ConfirmClose();
        }

        private void ConfirmClose()
        {
            Utilities.ExitLilypad();
        }

        private void FrameNavigated(object sender, DevExpress.Xpf.WindowsUI.Navigation.NavigationEventArgs e)
        {

        }

        private void LilyNavver_ManipulationStarted_1(object sender, ManipulationStartedEventArgs e)
        {
            MessageBox.Show("Test");
        }

        private void MainWindowLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Utilities.LoadTheme("Green");

                if (Properties.Settings.Default.App_FirstRun)
                {
                    if (Utilities.DBisOnline())
                    {
                        Properties.Settings.Default.App_FirstRun = false;
                        Properties.Settings.Default.Save();
                    }
                    else
                    {
                        MainViewModel mvm = (MainViewModel) App.Current.MainWindow.DataContext;
                        mvm.GoInitialSetup();
                    }
                }

                LoadLastModifiedDateTime();
            }
            catch (Exception ex)
            {
                new Error(ex);
            }

        }

        private void LoadLastModifiedDateTime()
        {
            try
            {
                System.Reflection.Assembly assy = System.Reflection.Assembly.GetExecutingAssembly();
                System.IO.FileInfo fi = new System.IO.FileInfo(assy.Location);
                Properties.Settings.Default.App_LastUpdateDate = (DateTime) fi.LastWriteTime;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private void SearchBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string toS = SearchBar.Text.ToString();
                MainWindow main = (MainWindow)App.Current.MainWindow;
             //   MainViewModel mvm = (MainViewModel)main.DataContext;
                Views.Hopper hop = new Views.Hopper();
                main.navFrame.Navigate(hop, toS);
                SearchButtonClick(null, null);

                var s1 = FocusManager.GetFocusedElement(this);
                if (s1 is FrameworkElement)
                {
                    ((FrameworkElement)s1).MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                }
            }
        }

        private void SearchGotFocus(object sender, RoutedEventArgs e)
        {
            if (SearchBar.Text == "Search...")
            {
                SearchBar.Clear();
            }
        }

        private void SearchLostFocus(object sender, RoutedEventArgs e)
        {
            if (SearchBar.Text == "")
            {
                SearchBar.Text = "Search...";
            }

       }

        int triggerThreshold = 500; // in milliseconds
        int lastCtrlTick = 0;

        private void ExitHotkey()
        {
            int thisCtrlTick = Environment.TickCount;
            int elapsed = thisCtrlTick - lastCtrlTick;
            if (elapsed <= triggerThreshold)
            {
                Utilities.ExitLilypad();
            }
            lastCtrlTick = thisCtrlTick;
        }

        private void MainKeyDown(object sender, KeyEventArgs e)
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            if (e.Key == Key.F12)
            {
                if (mvm.ScannerOpen)
                {
                    mvm.ScannerOpen = false;
                }
                else
                {
                    mvm.ScannerOpen = true;
                }
            }

            if (e.Key == Key.Escape)
            {
                ExitHotkey();
            }

            if (e.Key == Key.F6 && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control) && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift && (Keyboard.Modifiers & ModifierKeys.Alt) == ModifierKeys.Alt)
            {
                Utilities.ForceLogin();
            }
            if ((e.Key == Key.F1 && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control) && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift))
            {
                    if (Properties.Settings.Default.App_FirstRun == false)
                    {
                        Properties.Settings.Default.App_FirstRun = true;
                        Properties.Settings.Default.Save();
                    }
                    else
                    {
                        Properties.Settings.Default.App_FirstRun = false;
                        Properties.Settings.Default.Save();
                    }       
            }
        }

        private void BarcodeScanner_IsOpenChanged(object sender, EventArgs e)
        {
            Lilypad.Dialogs.BarcodeScanner bs = BarcodeScanner.Content as Lilypad.Dialogs.BarcodeScanner;
            bs.Spinner.Fill = App.Current.FindResource("AccentColorBrush") as Brush;
            bs.VisBrush.Visual = App.Current.FindResource("appbar_base_select") as Visual;
            bs.ItemTypeLabel.Visibility = System.Windows.Visibility.Collapsed;
            bs.ThisIdentifiedItem = "";
            bs.ItemNameLabel.Content = "Scan when ready";
            bs.ItemNameLabel.Foreground = App.Current.FindResource("AccentColorBrush") as Brush;
            bs.OrderControlButtons.Visibility = System.Windows.Visibility.Collapsed;
            bs.ScanBox.Clear();
            bs.ScanBox.Focus();
            Keyboard.Focus(bs.ScanBox);
        }

        private void StatusChanged(object sender, EventArgs e)
        {

        }

        private void ButtonEnter(object sender, MouseEventArgs e)
        {
            DevExpress.Xpf.LayoutControl.Tile bb = (DevExpress.Xpf.LayoutControl.Tile)sender;

            bb.BeginAnimation(OpacityProperty, new DoubleAnimation(0.95, TimeSpan.FromMilliseconds(125)));

          //  sp.Children.Add(srs); sp.Children.Add(tbs);
          //  bb.Content = sp;
           
        }

        private void ButtonLeft(object sender, MouseEventArgs e)
        {
            DevExpress.Xpf.LayoutControl.Tile bb = (DevExpress.Xpf.LayoutControl.Tile)sender;
            bb.BeginAnimation(OpacityProperty, new DoubleAnimation(0.5, TimeSpan.FromMilliseconds(250)));
        }

        private void FlyoutOpened(object sender, RoutedEventArgs e)
        {

        }

        private void SearchButtonClick(object sender, MouseButtonEventArgs e)
        {
            if (((MainViewModel) this.DataContext).FrogIsLoggedIn)
            {
                SearchBar.Clear();
                SearchBar.Visibility = SearchBox.Width == 0 ? Visibility.Visible : Visibility.Collapsed;
                SearchBox.WidthAnimation(SearchBox.Width > 0 ? 0 : 100, TimeSpan.FromMilliseconds(400));
                SearchBG.OpacityAnimation(SearchBox.Width > 0 ? 0 : 0.3, TimeSpan.FromMilliseconds(400));
                Keyboard.Focus(SearchBar);
            }
        }
    }
}
