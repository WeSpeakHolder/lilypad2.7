﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using DevExpress.Xpf.Printing;
using DevExpress.XtraReports.UI;
using Lilypad.Model;
using Lilypad.Properties;
using Lilypad.ViewModel;
using LilyReports;

namespace Lilypad
{
    public static class Reports
    {

        public static string SaveReport(this XtraReport report, string saveDirectory)
        {
            var r = report;
            var path = "";
         //   if (report is ProductionSheet) path = saveDirectory + "\\ProductionSheet.pdf";
            if (report is PreorderDetailsReport) path = saveDirectory + "\\PreorderDetails.pdf";
            r.ExportToPdf(path);
            return path;
        }

        public static void ViewReport(this string reportPath)
        {
            try
            {
                if (File.Exists(reportPath)) Process.Start(reportPath);
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        public static void PrintReport(this PreorderDetailsReport input)
        {
            try
            {
                input.PrintingSystem.ShowPrintStatusDialog = false;
                input.PrintingSystem.ShowMarginsWarning = false;
                PrintHelper.PrintDirect(input);
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        public static PreorderDetailsReport GetPreorderDetailsReport(preorder order)
        {
            var setL = GetPreorderDetailsDataset(order.PreorderID);
            var invoice = new PreorderDetailsReport();
            invoice.DataSource = setL;
            return invoice;
        }

        private static DataSet GetProductionDataSetHopperOrder(int receiptNo)
        {
            string process;
            string designer;
            string priority;

            var mavm = (MainViewModel)Application.Current.MainWindow.DataContext;

            customer thiscust;
            receipt thisrcpt;
            hopperorder CompletedOrder;
            IList<orderdetail> localList = new List<orderdetail>();

            using (var ctx = new Entities())
            {
                CompletedOrder = (from a in ctx.hopperorders where a.ReceiptNum == receiptNo select a).FirstOrDefault();
            }
            using (var ctx = new Entities())
            {
                localList = (from q in ctx.orderdetails where q.ReceiptNum == receiptNo select q).ToList();
                process =
                    (from q in ctx.printprocesses where q.ProcessID == CompletedOrder.PrintProcess select q.ProcessName)
                        .FirstOrDefault();
                designer =
                    (from k in ctx.froggers where k.FrogID == CompletedOrder.DesignFrog select k.FrogFirstName)
                        .FirstOrDefault();
                priority =
                    (from u in ctx.priorities where u.PriorityValue == CompletedOrder.Priority select u.PriorityAbbrev)
                        .FirstOrDefault();
                thiscust =
                    (from i in ctx.customers where i.CustomerID == CompletedOrder.CustomerID select i).FirstOrDefault();
                thisrcpt =
                    (from t in ctx.receipts where t.RcptNumber == CompletedOrder.ReceiptNum select t).FirstOrDefault();
            }

            var table = new DataTable("order");
            var tableKeyColumn = new DataColumn("ReceiptNum", typeof(int));
            var tableCKeyColumn = new DataColumn("CustID", typeof(int));
            table.Columns.Add(tableKeyColumn);
            table.Columns.Add("FirstName", typeof(string));
            table.Columns.Add("LastName", typeof(string));
            table.Columns.Add("OrderDate", typeof(string));
            table.Columns.Add("DueDate", typeof(string));
            table.Columns.Add("PrintProcess", typeof(string));
            table.Columns.Add("Designer", typeof(string));
            table.Columns.Add("BoxNum", typeof(string));
            table.Columns.Add("Priority", typeof(string));
            table.Columns.Add("PmtStatus", typeof(string));
            table.Columns.Add("Notes", typeof(string));
            table.Columns.Add(tableCKeyColumn);
            table.Columns.Add("Footer", typeof(string));
            table.Columns.Add("StoreInfo", typeof(string));

            // PAID POP Identifier And Logic.
            string pmtstat;
            decimal tendered;
            decimal total;
            bool result;

            tendered = thisrcpt.RcptTotalTendered;
            total = thisrcpt.RcptTotal;

            if (tendered < total)
            {
                result = false;
            }
            else
            {
                result = true;
            }
            if (result)
            {
                pmtstat = "PAID";
            }
            else
            {
                pmtstat = "POP";
            }

            var foot = Settings.Default.Invoices_Footer;
            var dueby = CompletedOrder.DueDate;
            var orddd = CompletedOrder.OrderDate;

            var dueStr = dueby.ToString("dddd, MMMM d") + " by " + dueby.ToString("h:mmtt");
            var ordStr = orddd.ToString("dddd, MMMM d") + " - " + orddd.ToString("h:mmtt");

            var sInfo = Settings.Default.Store_Phone + Environment.NewLine;
            sInfo += Settings.Default.Store_Address1;
            if (!string.IsNullOrEmpty(Settings.Default.Store_Address2))
            {
                sInfo += ", " + Settings.Default.Store_Address2;
            }
            sInfo += Environment.NewLine;
            sInfo += Settings.Default.Store_City + ", " + Settings.Default.Store_State + " " +
                     Settings.Default.Store_Zip;

            table.Rows.Add(CompletedOrder.ReceiptNum, CompletedOrder.CustFirst, CompletedOrder.CustLast, ordStr, dueStr,
                process, designer, CompletedOrder.BoxNum, priority, pmtstat, CompletedOrder.Notes,
                CompletedOrder.CustomerID, foot, sInfo);
            table.AcceptChanges();

            var lines = new DataTable("lineitems");
            var lineKeyColumn = new DataColumn("ReceiptNum", typeof(int));
            lines.Columns.Add("DetailID", typeof(int));
            lines.Columns.Add(lineKeyColumn);
            lines.Columns.Add("StockID", typeof(string));
            lines.Columns.Add("Description", typeof(string));
            lines.Columns.Add("Quantity", typeof(int));
            lines.Columns.Add("Color", typeof(string));
            lines.Columns.Add("Size", typeof(string));
            lines.Columns.Add("LinePrice", typeof(decimal));
            lines.Columns.Add("LineExtend", typeof(decimal));

            var receiptDT = new DataTable("receipt");
            var rcptKeyColumn = new DataColumn("ReceiptNum", typeof(int));
            receiptDT.Columns.Add(rcptKeyColumn);
            receiptDT.Columns.Add("SubTotal", typeof(decimal));
            receiptDT.Columns.Add("SalesTax", typeof(decimal));
            receiptDT.Columns.Add("Total", typeof(decimal));

            var customerDT = new DataTable("customer");
            var custKeyColumn = new DataColumn("CustID", typeof(int));
            customerDT.Columns.Add(custKeyColumn);
            customerDT.Columns.Add("CustomerName", typeof(string));
            customerDT.Columns.Add("Company", typeof(string));
            customerDT.Columns.Add("Street1", typeof(string));
            customerDT.Columns.Add("Street2", typeof(string));
            customerDT.Columns.Add("Phone", typeof(string));
            customerDT.Columns.Add("Email", typeof(string));


            foreach (var deet in localList)
            {
                lines.Rows.Add(deet.DetailID, deet.ReceiptNum, deet.StockIDPOS, deet.Description, deet.Quantity,
                    deet.Color, deet.Size, deet.LinePrice, deet.LineExtend);
            }

            var custname = thiscust.FirstName + " " + thiscust.LastName;
            var streetl2 = thiscust.City + " " + thiscust.State + " " + thiscust.Zip;

            customerDT.Rows.Add(thiscust.CustomerID, custname, thiscust.Company, thiscust.StreetAddress, streetl2,
                thiscust.Phone, thiscust.Email);
            receiptDT.Rows.Add(receiptNo, thisrcpt.RcptSTotal, thisrcpt.RcptTotalTax, thisrcpt.RcptTotal);

            var set = new DataSet("Dataset");
            set.Tables.Add(table);
            set.Tables.Add(lines);
            set.Tables.Add(customerDT);
            set.Tables.Add(receiptDT);
            set.AcceptChanges();
            var relation = new DataRelation("line_relation", tableKeyColumn, lineKeyColumn);
            var relation2 = new DataRelation("line_relation2", tableCKeyColumn, custKeyColumn);
            var relation3 = new DataRelation("line_relation3", tableKeyColumn, rcptKeyColumn);
            set.Relations.Add(relation);
            set.Relations.Add(relation2);
            set.Relations.Add(relation3);

            return set;
        }

        private static DataSet GetProductionDataSetArchiveOrder(int receiptNo)
        {
            string process;
            string designer;
            string priority;

            var mavm = (MainViewModel)Application.Current.MainWindow.DataContext;

            customer thiscust;
            receipt thisrcpt;
            order CompletedOrder;
            IList<orderdetail> localList = new List<orderdetail>();

            using (var ctx = new Entities())
            {
                CompletedOrder = (from a in ctx.orders where a.ReceiptNum == receiptNo select a).FirstOrDefault();
            }
            using (var ctx = new Entities())
            {
                localList = (from q in ctx.orderdetails where q.ReceiptNum == receiptNo select q).ToList();
                process =
                    (from q in ctx.printprocesses where q.ProcessID == CompletedOrder.PrintProcess select q.ProcessName)
                        .FirstOrDefault();
                designer =
                    (from k in ctx.froggers where k.FrogID == CompletedOrder.DesignFrog select k.FrogFirstName)
                        .FirstOrDefault();
                priority =
                    (from u in ctx.priorities where u.PriorityValue == CompletedOrder.Priority select u.PriorityAbbrev)
                        .FirstOrDefault();
                thiscust =
                    (from i in ctx.customers where i.CustomerID == CompletedOrder.CustomerID select i).FirstOrDefault();
                thisrcpt =
                    (from t in ctx.receipts where t.RcptNumber == CompletedOrder.ReceiptNum select t).FirstOrDefault();
            }

            var table = new DataTable("order");
            var tableKeyColumn = new DataColumn("ReceiptNum", typeof(int));
            var tableCKeyColumn = new DataColumn("CustID", typeof(int));
            table.Columns.Add(tableKeyColumn);
            table.Columns.Add("FirstName", typeof(string));
            table.Columns.Add("LastName", typeof(string));
            table.Columns.Add("OrderDate", typeof(string));
            table.Columns.Add("DueDate", typeof(string));
            table.Columns.Add("PrintProcess", typeof(string));
            table.Columns.Add("Designer", typeof(string));
            table.Columns.Add("BoxNum", typeof(string));
            table.Columns.Add("Priority", typeof(string));
            table.Columns.Add("PmtStatus", typeof(string));
            table.Columns.Add("Notes", typeof(string));
            table.Columns.Add(tableCKeyColumn);
            table.Columns.Add("Footer", typeof(string));
            table.Columns.Add("StoreInfo", typeof(string));

            // PAID POP Identifier And Logic.
            string pmtstat;
            decimal tendered;
            decimal total;
            bool result;

            tendered = thisrcpt.RcptTotalTendered;
            total = thisrcpt.RcptTotal;

            if (tendered < total)
            {
                result = false;
            }
            else
            {
                result = true;
            }
            if (result)
            {
                pmtstat = "PAID";
            }
            else
            {
                pmtstat = "POP";
            }

            var foot = Settings.Default.Invoices_Footer;
            var dueby = CompletedOrder.DueDate;
            var orddd = CompletedOrder.OrderDate;

            var dueStr = dueby.ToString("dddd, MMMM d") + " by " + dueby.ToString("h:mmtt");
            var ordStr = orddd.ToString("dddd, MMMM d") + " - " + orddd.ToString("h:mmtt");

            var sInfo = Settings.Default.Store_Phone + Environment.NewLine;
            sInfo += Settings.Default.Store_Address1;
            if (!string.IsNullOrEmpty(Settings.Default.Store_Address2))
            {
                sInfo += ", " + Settings.Default.Store_Address2;
            }
            sInfo += Environment.NewLine;
            sInfo += Settings.Default.Store_City + ", " + Settings.Default.Store_State + " " +
                     Settings.Default.Store_Zip;

            table.Rows.Add(CompletedOrder.ReceiptNum, thiscust.FirstName, thiscust.LastName, ordStr, dueStr, process,
                designer, CompletedOrder.BoxNum, priority, pmtstat, CompletedOrder.Notes, CompletedOrder.CustomerID,
                foot, sInfo);
            table.AcceptChanges();

            var lines = new DataTable("lineitems");
            var lineKeyColumn = new DataColumn("ReceiptNum", typeof(int));
            lines.Columns.Add("DetailID", typeof(int));
            lines.Columns.Add(lineKeyColumn);
            lines.Columns.Add("StockID", typeof(string));
            lines.Columns.Add("Description", typeof(string));
            lines.Columns.Add("Quantity", typeof(int));
            lines.Columns.Add("Color", typeof(string));
            lines.Columns.Add("Size", typeof(string));
            lines.Columns.Add("LinePrice", typeof(decimal));
            lines.Columns.Add("LineExtend", typeof(decimal));

            var receiptDT = new DataTable("receipt");
            var rcptKeyColumn = new DataColumn("ReceiptNum", typeof(int));
            receiptDT.Columns.Add(rcptKeyColumn);
            receiptDT.Columns.Add("SubTotal", typeof(decimal));
            receiptDT.Columns.Add("SalesTax", typeof(decimal));
            receiptDT.Columns.Add("Total", typeof(decimal));

            var customerDT = new DataTable("customer");
            var custKeyColumn = new DataColumn("CustID", typeof(int));
            customerDT.Columns.Add(custKeyColumn);
            customerDT.Columns.Add("CustomerName", typeof(string));
            customerDT.Columns.Add("Company", typeof(string));
            customerDT.Columns.Add("Street1", typeof(string));
            customerDT.Columns.Add("Street2", typeof(string));
            customerDT.Columns.Add("Phone", typeof(string));
            customerDT.Columns.Add("Email", typeof(string));


            foreach (var deet in localList)
            {
                lines.Rows.Add(deet.DetailID, deet.ReceiptNum, deet.StockIDPOS, deet.Description, deet.Quantity,
                    deet.Color, deet.Size, deet.LinePrice, deet.LineExtend);
            }

            var custname = thiscust.FirstName + " " + thiscust.LastName;
            var streetl2 = thiscust.City + " " + thiscust.State + " " + thiscust.Zip;

            customerDT.Rows.Add(thiscust.CustomerID, custname, thiscust.Company, thiscust.StreetAddress, streetl2,
                thiscust.Phone, thiscust.Email);
            receiptDT.Rows.Add(receiptNo, thisrcpt.RcptSTotal, thisrcpt.RcptTotalTax, thisrcpt.RcptTotal);

            var set = new DataSet("Dataset");
            set.Tables.Add(table);
            set.Tables.Add(lines);
            set.Tables.Add(customerDT);
            set.Tables.Add(receiptDT);
            set.AcceptChanges();
            var relation = new DataRelation("line_relation", tableKeyColumn, lineKeyColumn);
            var relation2 = new DataRelation("line_relation2", tableCKeyColumn, custKeyColumn);
            var relation3 = new DataRelation("line_relation3", tableKeyColumn, rcptKeyColumn);
            set.Relations.Add(relation);
            set.Relations.Add(relation2);
            set.Relations.Add(relation3);

            return set;
        }

        private static DataSet GetPreorderDetailsDataset(int preorderID)
        {
            string process;
            string designer;
            string priority;

            var mavm = (MainViewModel)Application.Current.MainWindow.DataContext;

            preorder preorder;
            IList<preorderitem> localList = new List<preorderitem>();

            using (var ctx = new Entities())
            {
                preorder = (from a in ctx.preorders where a.PreorderID == preorderID select a).FirstOrDefault();
                process =
                    (from q in ctx.printprocesses where q.ProcessID == preorder.Process select q.ProcessName)
                        .FirstOrDefault();
                designer =
                    (from k in ctx.froggers where k.FrogID == preorder.OrderFrog select k.FrogFirstName).FirstOrDefault();
                localList = (from q in ctx.preorderitems where q.PreorderID == preorderID && q.DesignID == null select q).ToList();
            }

            var table = new DataTable("preorder");
            var tableKeyColumn = new DataColumn("PreorderID", typeof(int));
            table.Columns.Add(tableKeyColumn);
            table.Columns.Add("CustName", typeof(string));
            table.Columns.Add("Company", typeof(string));
            table.Columns.Add("Process", typeof(string));
            table.Columns.Add("Status", typeof(string));
            table.Columns.Add("BinNum", typeof(string));
            table.Columns.Add("OrderFrog", typeof(string));
            table.Columns.Add("CreatedDate", typeof(string));
            table.Columns.Add("DueDateEnd", typeof(string));
            table.Columns.Add("ShirtsCount", typeof(int));
            table.Columns.Add("PhoneNum", typeof(string));
            table.Columns.Add("EmailAddr", typeof(string));
            table.Columns.Add("SearchTags", typeof(string));
            table.Columns.Add("JobFolderPath", typeof(string));
            table.Columns.Add("GarmentNotes", typeof(string));
            table.Columns.Add("ArtworkNotes", typeof(string));
            table.Columns.Add("MainNotes", typeof(string));
            table.Columns.Add("Footer", typeof(string));
            table.Columns.Add("StoreInfo", typeof(string));


            var foot = Settings.Default.Invoices_Footer;
            var dueby = preorder.DueDateEnd;
            var orddd = preorder.CreatedDate;
            var dueStr = string.Empty;
            var ordStr = string.Empty;

            if (dueby != null)
            {
                dueStr = dueby.Value.ToString("dddd, MMMM d") + " by " + dueby.Value.ToString("h:mmtt");
                ordStr = orddd.Value.ToString("dddd, MMMM d") + " - " + orddd.Value.ToString("h:mmtt");
            }
            else
            {
                dueStr = "";
                ordStr = orddd.Value.ToString("dddd, MMMM d") + " - " + orddd.Value.ToString("h:mmtt");
            }


            var sInfo = Settings.Default.Store_Phone + Environment.NewLine;
            sInfo += Settings.Default.Store_Address1;
            if (!string.IsNullOrEmpty(Settings.Default.Store_Address2))
            {
                sInfo += ", " + Settings.Default.Store_Address2;
            }
            sInfo += Environment.NewLine;
            sInfo += Settings.Default.Store_City + ", " + Settings.Default.Store_State + " " +
                     Settings.Default.Store_Zip;
            var cust = preorder.First + " " + preorder.Last;
            table.Rows.Add(preorder.PreorderID, cust, preorder.Company, process, preorder.Status, preorder.BinNum,
                designer, ordStr, dueStr, preorder.ShirtsCount,
                preorder.PhoneNum, preorder.EmailAddr, preorder.SearchTags, preorder.JobFolderPath,
                preorder.GarmentNotes, preorder.ArtworkNotes, preorder.MainNotes, foot, sInfo);
            table.AcceptChanges();

            var lines = new DataTable("lineitems");
            var lineKeyColumn = new DataColumn("PreorderID", typeof(int));
            lines.Columns.Add(lineKeyColumn);
            lines.Columns.Add("UniqueID", typeof(int));
            lines.Columns.Add("StockMfg", typeof(string));
            lines.Columns.Add("StockCode", typeof(string));
            lines.Columns.Add("StockColor", typeof(string));
            lines.Columns.Add("StockSize", typeof(string));
            lines.Columns.Add("StockQty", typeof(int));
            lines.Columns.Add("StockInvStatus", typeof(string));
            lines.Columns.Add("StockInvStatusID", typeof(int));
            lines.Columns.Add("StockDescrip", typeof(string));
            lines.Columns.Add("StockAlphaID", typeof(string));
            lines.Columns.Add("StockItemCost", typeof(decimal));
            lines.Columns.Add("EstimatedCost", typeof(decimal));

            foreach (var deet in localList)
            {
                lines.Rows.Add(deet.PreorderID, deet.UniqueID, deet.StockMfg, deet.StockCode, deet.StockColor,
                    deet.StockSize, deet.StockQty, deet.StockInvStatus,
                    deet.StockInvStatusID, deet.StockDescrip, deet.StockAlphaID, deet.StockItemCost, deet.EstimatedCost);
            }

            var set = new DataSet("Dataset");
            set.Tables.Add(table);
            set.Tables.Add(lines);
            set.AcceptChanges();

            var relation = new DataRelation("line_relation", tableKeyColumn, lineKeyColumn);
            set.Relations.Add(relation);
            return set;
        }
    }
}