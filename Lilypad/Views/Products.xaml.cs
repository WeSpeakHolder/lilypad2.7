﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Threading;
using System.Net;
using System.Windows.Input;
using System.Xml.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Lilypad.ViewModel;
using Lilypad.Model;

namespace Lilypad.Views
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Products : UserControl
    {
        public Products()
        {
            InitializeComponent();
            bWorker = new BackgroundWorker();
            bWorker.DoWork += bWorker_DoWork;
            bWorker.WorkerReportsProgress = true;
            bWorker.WorkerSupportsCancellation = true;
            bWorker.ProgressChanged += bWorker_ProgressChanged;
            bWorker.RunWorkerCompleted += bWorker_RunWorkerCompleted;
        }

        public alphaitem SelectedAlphaItem { get; private set; }
        public List<alphaitem> InputList { get; private set; }
        public List<string> SizeList { get; private set; }
        public List<string> SortedList { get; private set; }

        void bWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!bWorker.CancellationPending)
            {
                // File operations
                InputList = new List<alphaitem>();
                SizeList = new List<string>();
                SortedList = new List<string>();

                alphaitem selitem = SelectedAlphaItem;

                using (Entities ctx = new Entities())
                {
                    InputList = (from k in ctx.alphaitems where k.Style_Code == selitem.Style_Code && k.Color_Name == selitem.Color_Name orderby k.Size_Code ascending select k).ToList();
                }

                SizeList = InputList.Select(x => x.Size_Name).ToList();
                SortedList = Utilities.SizeOrderArray.Select(x => SizeList.Contains(x) ? x : "-1").Where(x => x != "-1").ToList<string>();
                int delay = Properties.Settings.Default.Interop_ThreadDelay;

                // Lookup in Background
                for (int i = 0; i < SortedList.Count; i++)
                {
                    if (!bWorker.CancellationPending)
                    {
                        System.Threading.Thread.Sleep(delay);
                        double perdd = Math.Round((double)(i / SortedList.Count) * 100);
                        int percent = Convert.ToInt32(perdd);
                        string sizeName = SortedList[i];
                        var item = (from q in InputList where q.Size_Name == sizeName select q).FirstOrDefault();

                        try
                        {
                            WebClient wc = new WebClient();
                            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                            //ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                            string reqURL = @"https://www.alphashirt.com/cgi-bin/online/xml/inv-request.w?in1=" + item.Item_Number + "&pr=y&zp=85719&userName=tucson1&password=asfrog9big10";

                            //string reqURL = @"https://www.alphashirt.com/cgi-bin/online/xml/inv-request.w?in1=" + item.Item_Number + "&pr=y&zp=85719&userName=tucson1&password=asfrog9big10";
                            string result = wc.DownloadString(new Uri(reqURL));
                            string[] output = new string[] { sizeName, i.ToString(), result };
                            bWorker.ReportProgress(percent, output);
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }
                }
            }

        }

        void bWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                ColorSelectionChanged(null, null);
            }
        }

        void bWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() =>
            {
                string[] input = (string[])e.UserState;
                string result = input[2];
                int index = Convert.ToInt32(input[1]);
                string sizename = input[0].ToString();

         //       try
                {
                    XDocument doc = XDocument.Parse(result);
                    string resultAsString = doc.ToString();

                    InventoryCheckViewModel newVM = new InventoryCheckViewModel();

                    newVM.WHSE_Fresno = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "CN").Value));
                    newVM.WHSE_Seattle = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "WA").Value));
                    newVM.WHSE_Denver = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "CD").Value));
                    newVM.WHSE_Lenexa = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "KC").Value));
                    newVM.WHSE_Dallas = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "TD").Value));
             //       newVM.WHSE_Houston = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "TE").Value));
                    newVM.WHSE_Houston = "0";
             //       newVM.WHSE_Verona = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "WI").Value));
                    newVM.WHSE_Verona = "0";
                    newVM.WHSE_Chicago = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "CC").Value));
                    newVM.WHSE_Plymouth = "0";
             //       newVM.WHSE_Plymouth = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "OP").Value));
                    newVM.WHSE_Middleboro = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "MA").Value));
                    newVM.WHSE_Harrisburg = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "PH").Value));
                    newVM.WHSE_Charlotte = "0";
             //       newVM.WHSE_Charlotte = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "CT").Value));
                    newVM.WHSE_Atlanta = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "GD").Value));
                    newVM.WHSE_Orlando = string.Format("{0:#,0}", Convert.ToInt32(doc.Root.Element("item").Elements("whse").First(x => x.Attribute("code").Value == "FO").Value));
                    newVM.SizeCode = doc.Root.Element("item").Attribute("size-code").Value.ToString();
                    newVM.ItemNumber = doc.Root.Element("item").Attribute("item-number").Value.ToString();
                    string priceA = doc.Root.Element("item").Attribute("price").Value.ToString();
                    newVM.SizeName = sizename;
                    newVM.SizeIndex = index;
                    newVM.IsSpecialPrice = false;
                    newVM.Price = Convert.ToDecimal(priceA.Split('$')[1]);
                    string search = "special-price=";

                    if (resultAsString.Contains(search))
                    {
                        string elem = doc.Root.Element("item").Attribute("special-price").Value.ToString();
                        string expiry = doc.Root.Element("item").Attribute("special-expiry").Value.ToString();

                        newVM.SpecialExpiry = expiry;
                        newVM.SpecialPrice = elem;
                        newVM.Price = Convert.ToDecimal(elem.Split('$')[1]);
                        newVM.IsSpecialPrice = true;
                    }

                    var newItem = new Controls.InventoryCheckOutputSize(newVM);
                    LookupContainer.Children.Add(newItem);
                }
            //    catch (Exception ex) { new Error(ex); }

            }));
        }

        private BackgroundWorker bWorker;


        private void ProductsLoaded(object sender, RoutedEventArgs e)
        {

        }

        private void SelectedOrderIndexChanged(object sender, RoutedEventArgs e)
        {

        }

        private void GridViewRowChanged(object sender, DevExpress.Xpf.Grid.FocusedRowChangedEventArgs e)
        {
            
        }

        private void SelectItem(object sender, MouseButtonEventArgs e)
        {
            ColorBox.SelectionChanged -= ColorSelectionChanged;
            ProductsViewModel pvm = (ProductsViewModel)this.DataContext;
            if (pvm.ColorsList != null) pvm.ColorsList.Clear();
            if (pvm.RelatedItems.Count > 0) pvm.RelatedItems.Clear();

                if (ProductsGrid.GetFocusedRowCellValue("Style_Code") != null)
                {
                    string input = ProductsGrid.GetFocusedRowCellValue("Style_Code").ToString();
                    alphaitem selecteditem;
                    IList<alphaitem> relateditems;

                    using (Entities ctx = new Entities())
                    {
                        relateditems = (from i in ctx.alphaitems where i.Style_Code == input select i).ToList();
                    }

                    selecteditem = relateditems.First();
                    pvm.SelectedItem = selecteditem;
                    pvm.RelatedItems = relateditems.OrderBy(p => p.Size_Code).DistinctBy(p => p.Color_Name).OrderBy(p => p.Color_Name).ToList();
                }

                ColorBox.SelectionChanged += ColorSelectionChanged;
        }

        private void TextboxFocused(object sender, RoutedEventArgs e)
        {

        }

        private void GroupExpanding(object sender, DevExpress.Xpf.Grid.RowAllowEventArgs e)
        {
            e.Allow = false;
        }

        private async void ColorSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (bWorker.IsBusy)
            {
                bWorker.CancelAsync();
                bWorker.Dispose();

                    if (LookupContainer.Children.Count > 0)
                    {
                        LookupContainer.Children.Clear();
                    }

                    await Task.Delay(500);

                    if (LookupContainer.Children.Count > 0)
                    {
                        LookupContainer.Children.Clear();
                    }
            }

            alphaitem selitem = (alphaitem)ColorBox.SelectedItem;
            ProductsViewModel pvm = (ProductsViewModel)this.DataContext;
            pvm.SelectedItem = selitem;
            SelectedAlphaItem = selitem;

                LookupContainer.Children.Clear();
                string[] sep = new string[] { " - " };
                System.Globalization.TextInfo tinfo = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo;
                string title = selitem.Description.Split(sep, StringSplitOptions.RemoveEmptyEntries)[0] + " - " + tinfo.ToTitleCase(selitem.Color_Name.ToLower());
                pvm.LookedUpItem = title;

                if (LookupSP.Opacity == 1)
                {
                    CollapseLookupSP();
                }

                ExpandOutLookupSP();


            await Task.Delay(100);
            //bWorker.RunWorkerAsync();
            if (!bWorker.IsBusy) {
                bWorker.RunWorkerAsync();
            }
            else {
                bWorker.CancelAsync();
                Utilities.ShowSimpleDialog("Notification", "Process was busy in previous action, please try again!");
            }

        }
        private void CollapseLookupSP()
        {
            DoubleAnimation anim = new DoubleAnimation(1, 0, TimeSpan.FromMilliseconds(300));
            ThicknessAnimation anim3 = new ThicknessAnimation(new Thickness(0, 0, 0, 0), new Thickness(-50, 0, 0, 0), new Duration(TimeSpan.FromMilliseconds(500)));

            LookupSP.BeginAnimation(OpacityProperty, anim);
            LookupSP.BeginAnimation(MarginProperty, anim3);
        }

        private void ExpandOutLookupSP()
        {
            DoubleAnimation anim = new DoubleAnimation(0, 1, TimeSpan.FromMilliseconds(300));
            ThicknessAnimation anim3 = new ThicknessAnimation(new Thickness(-50, 0, 0, 0), new Thickness(0, 0, 0, 0), new Duration(TimeSpan.FromMilliseconds(500)));

            LookupSP.BeginAnimation(OpacityProperty, anim);
            LookupSP.BeginAnimation(MarginProperty, anim3);
        }

        private void PreviewColorBox(object sender, MouseButtonEventArgs e)
        {

        }


    }
}
