﻿using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using Lilypad.Model;
using Lilypad.ViewModel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace Lilypad.Views
{
    /// <summary>
    /// Interaction logic for UltraPrint.xaml
    /// </summary>
    public partial class UltraPrint : UserControl
    {
        public bool IsFullyLoaded;
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private BackgroundWorker bWorker;
        private BackgroundWorker bLoader;

        public UltraPrint()
        {
            Toaster.Loading("Loading...", "");
            InitializeComponent();
            IsFullyLoaded = false;
            using (var ctx = new Entities())
            {
                var onload = ctx.ultraprintorderdatas.ToList();
                foreach (var item in onload)
                {
                    item.IsDeleted = false;
                }
                ctx.SaveChanges();
            }

            bWorker = new BackgroundWorker();
            bLoader = new BackgroundWorker();

            bWorker.DoWork += bWorker_DoWork;
            bWorker.WorkerReportsProgress = true;
            bWorker.WorkerSupportsCancellation = false;
            bWorker.ProgressChanged += bWorker_ProgressChanged;
            bWorker.RunWorkerCompleted += bWorker_RunWorkerCompleted;

            bLoader.DoWork += bLoader_DoWork;
            bLoader.WorkerReportsProgress = true;
            bLoader.WorkerSupportsCancellation = false;
            bLoader.ProgressChanged += bLoader_ProgressChanged;
            bLoader.RunWorkerCompleted += bLoader_RunWorkerCompleted;
        }
        void bWorker_DoWork(object sender, DoWorkEventArgs e)
        { }
        void bWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        { }
        void bWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        { }
        void bLoader_ProgressChanged(object sender, ProgressChangedEventArgs e)
        { }
        void bLoader_DoWork(object sender, DoWorkEventArgs e)
        { }

        void bLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            using (var ctx = new Entities())
            {
                ctx.ultraprintorderdatas.Load();
                grid.ItemsSource = ctx.ultraprintorderdatas.Local;
            }
            Toaster.DismissLoading();
        }

        ~UltraPrint()
        {
            using (var ctx = new Entities())
            {
                var onload = ctx.ultraprintorderdatas.ToList();
                foreach (var item in onload)
                {
                    item.IsDeleted = false;
                }
                ctx.SaveChanges();
            }
        }

        private void btnUPWDelete(object sender, EventArgs e)
        {
            bool flag = false;
            using (var ctx = new Entities())
            {
                var allitems = ctx.ultraprintorderdatas.ToList();
                foreach (var item in allitems)
                {
                    if (item.IsDeleted == true)
                    {
                        flag = true;
                        ctx.ultraprintorderdatas.Remove(item);
                    }
                }
                ctx.SaveChanges();
            }

            if (!flag)
            {
                Utilities.ShowSimpleDialog("UltraPrint Inventory Delete", "Please select order before deleting");
            }
            else
            {

                MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
                mvm.goUltraPrintcommand.Execute(null);
            }
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "CSV files (*.csv)|*.csv";
                if (openFileDialog.ShowDialog() == true)
                {
                    string csvPath = openFileDialog.FileName;

                    //Create a DataTable.  
                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[12] {
                    new DataColumn("Company", typeof(string)),
                    new DataColumn("FirstName", typeof(string)),
                    new DataColumn("LastName", typeof(string)),
                    new DataColumn("Invoice",typeof(string)),
                    new DataColumn("OrderDate",typeof(string)),
                    new DataColumn("Qty",typeof(string)),
                    new DataColumn("Location",typeof(string)),
                    new DataColumn("Type",typeof(string)),
                    new DataColumn("Size",typeof(string)),
                    new DataColumn("DesignName",typeof(string)),
                    new DataColumn("Vendor",typeof(string)),
                    new DataColumn("BoxNumber",typeof(string))
                });

                    //Read the contents of CSV file.  
                    string csvData = File.ReadAllText(csvPath);
                    csvData = csvData.Replace("\"", "");

                    if (!string.IsNullOrEmpty(csvData) && (csvData != "\r\n"))
                    {
                        //Execute a loop over the rows.  
                        foreach (string row in csvData.Split('\n'))
                        {
                            if (!string.IsNullOrEmpty(row))
                            {
                                dt.Rows.Add();
                                int i = 0;

                                //Execute a loop over the columns.  
                                foreach (string cell in row.Split(','))
                                {
                                    dt.Rows[dt.Rows.Count - 1][i] = cell;
                                    i++;
                                }
                            }
                        }
                    }
                    else
                    {
                        Utilities.ShowSimpleDialog("UltraPrint Inventory Upload", "Uploaded .csv is empty");
                        return;
                    }

                    int flag = 0;
                    using (Entities ctx = new Entities())
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            if (Convert.ToString(row.ItemArray[3]) != "Invoice")
                            {
                                int receiptnum = Convert.ToInt32(row.ItemArray[3]);
                                hopperorder thisOrd =
                                    (from q in ctx.hopperorders where q.ReceiptNum == receiptnum select q).FirstOrDefault();
                                ultraprintorderdata ultradata = new ultraprintorderdata();
                                if (thisOrd != null || thisOrd == null)
                                {
                                    flag = 1;
                                    ultradata.cusomerId = thisOrd != null ? thisOrd.CustomerID : 0;
                                    ultradata.OrderID = thisOrd != null ? thisOrd.OrderID : 0;
                                    ultradata.Location = row["Location"].ToString();
                                    ultradata.DesignName = row["DesignName"].ToString();
                                    ultradata.Type = row["Type"].ToString();
                                    ultradata.Qty = Convert.ToInt32(row["Qty"]);
                                    ultradata.receiptnum = Convert.ToInt32(row["Invoice"]);
                                    ultradata.size = row["size"].ToString();
                                    ultradata.createddate = DateTime.Now;
                                    ultradata.IsEditable = true;
                                    ultradata.CustLastName = row["LastName"].ToString();
                                    ultradata.CustCompany = row["Company"].ToString();
                                    ultradata.Vendor = row["Vendor"].ToString();
                                    ultradata.Bin = row["BoxNumber"].ToString();
                                    ultradata.CustFirstName = row["FirstName"].ToString();
                                    ultradata.expiredate = DateTime.Now.AddMonths
                                        (Properties.Settings.Default.UltraPrint_ExpirationDate);
                                    ctx.ultraprintorderdatas.Add(ultradata);
                                }
                            }
                        }
                        ctx.SaveChanges();
                        if (flag == 0)
                        {
                            Utilities.ShowSimpleDialog("UltraPrint Inventory Upload", "Nothing to upload. No records matched with hopper order Receipt number");
                        }
                        else
                        {
                            using (var ctxs = new Entities())
                            {
                                ctxs.ultraprintorderdatas.Load();
                                grid.ItemsSource = ctxs.ultraprintorderdatas.Local;
                            }
                            Utilities.ShowSimpleDialog("UltraPrint Inventory Upload", "Ultraprint Inventory uploaded successfully");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("committed successfully"))
                {
                    using (var ctxx = new Entities())
                    {
                        ctxx.ultraprintorderdatas.Load();
                        grid.ItemsSource = ctxx.ultraprintorderdatas.Local;
                    }
                    Utilities.ShowSimpleDialog("UltraPrint Inventory Upload", "Ultraprint Inventory uploaded successfully");
                }
                else
                {
                    Utilities.ShowSimpleDialog("UltraPrint Inventory Upload", "Error occurred on Inventory upload");
                }
            }

        }

        private void GridSelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {
            if (IsFullyLoaded)
            {
                if (this.DataContext.GetType() == typeof(UltraPrintViewModel))
                {
                    UltraPrintViewModel thisVM = (UltraPrintViewModel)this.DataContext;
                    thisVM.NeedsSave = false;
                }
            }
        }

        private void PrintGridClicked(object sender, EventArgs e)
        {
            DocumentPreviewWindow preview = new DocumentPreviewWindow();
            PrintableControlLink link = new PrintableControlLink(view as DevExpress.Xpf.Printing.IPrintableControl);
            LinkPreviewModel model = new LinkPreviewModel(link);
            preview.Model = model;
            link.Landscape = true;
            link.CreateDocument(true);
            ThemeManager.SetThemeName(preview, Theme.MetropolisLightName);
            preview.ShowDialog();
        }

        private void ultraprintLoaded(object sender, RoutedEventArgs e)
        {
            bWorker.RunWorkerAsync();
            bLoader.RunWorkerAsync();
        }

        private void view_ShowingEditor(object sender, DevExpress.Xpf.Grid.ShowingEditorEventArgs e)
        {
            if (e.Column.FieldName != "IsEditable")
            {
                if ((e.Column.FieldName == "receiptnum") || (e.Column.FieldName == "CustLastName") || (e.Column.FieldName == "CustCompany") || (e.Column.FieldName == "createddate") || (e.Column.FieldName == "expiredate") || (e.Column.FieldName == "Location") || (e.Column.FieldName == "Type") || (e.Column.FieldName == "DesignName") || (e.Column.FieldName == "size") || (e.Column.FieldName == "Vendor")) { e.Cancel = true; }
                else
                {
                    using (var ctx = new Entities())
                    {
                        ultraprintorderdata thisOrd =
(from q in ctx.ultraprintorderdatas where q.Id == ((Lilypad.Model.ultraprintorderdata)e.Row).Id select q).First();
                        if (((Lilypad.Model.ultraprintorderdata)e.Row).IsDeleted == true)
                        {
                            thisOrd.IsDeleted = false;

                        }
                        else
                        {
                            thisOrd.IsDeleted = true;
                        }
                        ctx.SaveChanges();
                    }
                    bool s = ((Lilypad.Model.ultraprintorderdata)e.Row).IsEditable;
                    e.Cancel = !s;
                }
            }
        }

        public void TableView_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            string original = string.Empty;
            try
            {
                using (Entities ctx = new Entities())
                {
                    ultraprintorderdata thisOrd =
                        (from q in ctx.ultraprintorderdatas where q.Id == ((Lilypad.Model.ultraprintorderdata)e.Row).Id select q).First();

                    string sampleheader = e.Column.ActualColumnChooserHeaderCaption.ToString();
                    sampleheader = Regex.Replace(sampleheader, @"\s+", "");
                    if (sampleheader == "Box#")
                    {
                        sampleheader = "GridBin";
                    }
                    if (sampleheader == "Used")
                    {
                        sampleheader = "GridUsed";
                    }
                    if (!string.IsNullOrEmpty(sampleheader) && !string.IsNullOrEmpty(original))
                    {
                        thisOrd.GetType().GetProperty(sampleheader).SetValue(thisOrd, e.Value);
                        List<ultraprintorderdata> order = new List<ultraprintorderdata>();
                        if (thisOrd.TransferedFrom == null && thisOrd.TransferedFrom == 0)
                            order = (from q in ctx.ultraprintorderdatas where q.TransferedFrom == thisOrd.Id select q).ToList();
                        else
                            order = (from t in ctx.ultraprintorderdatas where t.Id == thisOrd.TransferedFrom select t).ToList();

                        if (order != null && order.Count() > 0)
                        {
                            foreach (var item in order)
                            {
                                item.GetType().GetProperty(sampleheader).SetValue(item, e.Value);
                            }
                        }
                    }
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
