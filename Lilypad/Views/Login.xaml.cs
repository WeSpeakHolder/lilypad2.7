﻿using System;
using System.Management;
using System.ComponentModel;
using System.Windows.Threading;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.Text;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.LayoutControl;
using Lilypad.ViewModel;
using Lilypad.Model;
using Brushes = System.Windows.Media.Brushes;
using System.Diagnostics;

namespace Lilypad.Views
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Login : UserControl
    {
        public string UpdateVersion = string.Empty;
        public Login()
        {
            InitializeComponent();
            TriedRestart = false;
            bWorker = new BackgroundWorker();
            bWorker.DoWork += bWorker_DoWork;
            bWorker.WorkerReportsProgress = true;
            bWorker.WorkerSupportsCancellation = true;
            bWorker.ProgressChanged += bWorker_ProgressChanged;
            bWorker.RunWorkerCompleted += bWorker_RunWorkerCompleted;
        }

        private void YesClicked(object sender, RoutedEventArgs e)
        {
            OptionsPanel.Visibility = System.Windows.Visibility.Hidden;
            UpdateStatusSpinner.IsError = false;
            UpdateStatusSpinner.IsNotice = false;
            BeginUpdate();
        }

        private void NoClicked(object sender, RoutedEventArgs e)
        {
            OptionsPanel.Visibility = System.Windows.Visibility.Hidden;
            UpdateStatusSpinner.IsError = true;
            UpdateStatusSpinner.IsNotice = true;
            UpdateStatusLabel.Content = "Update postponed.";
            UpdateStatusLabel.Foreground = Brushes.Orange;
        }
        public void UpdateLilypad()
        {
            if (!System.Diagnostics.Debugger.IsAttached)
            {
                MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
                mvm.DigitalFlag = ApplicationDeployment.IsNetworkDeployed ? Visibility.Visible : Visibility.Collapsed;

                if (Utilities.InternetConnected())
                {
                    if (Utilities.TestSkyIslandServerHTTP() && ApplicationDeployment.IsNetworkDeployed)
                    {
                        UpdaterSP.Visibility = System.Windows.Visibility.Visible;
                        System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
                        ad.CheckForUpdateCompleted += ad_CheckForUpdateCompleted;
                        ad.CheckForUpdateProgressChanged += ad_CheckForUpdateProgressChanged;
                        ad.CheckForUpdateAsync();
                        UpdateStatusLabel.Content = "You already have the latest version of Lilypad.";
                        UpdateStatusSpinner.IsDone = true;
                    }
                    else if (ApplicationDeployment.IsNetworkDeployed)
                    {
                        UpdateStatusLabel.Content = "Updates unavailable.";
                        UpdateStatusLabel.Foreground = Brushes.Orange;
                        UpdateStatusSpinner.IsError = true;
                        UpdateStatusSpinner.IsNotice = true;
                    }
                    else
                    {
                        UpdateStatusLabel.Content = "You already have the latest version of Lilypad.";
                        UpdateStatusSpinner.IsDone = true;

                        if (RemoteFileExists("http://skyislandsoftware.com/lilypad/twopointfive/version.txt"))
                        {
                            try
                            {
                                using (WebClient client3 = new WebClient())
                                {
                                    client3.DownloadFile("http://skyislandsoftware.com/lilypad/twopointfive/version.txt",
                                                    @"C:\Program Files (x86)\Lilypad\version.txt");
                                }
                                if (File.Exists(@"C:\Program Files (x86)\Lilypad\version.txt"))
                                {
                                    System.IO.TextReader reader = new System.IO.StreamReader(@"C:\Program Files (x86)\Lilypad\version.txt");
                                    UpdateVersion = reader.ReadToEnd().Trim();
                                    reader.Close();

                                    if (RemoteFileExists("http://skyislandsoftware.com/lilypad/twopointfive/Lilypad_Upgrade_" + UpdateVersion + ".msi"))
                                    {
                                        string AppVersion = Properties.Settings.Default.App_Version.ToString();
                                        if (!string.IsNullOrEmpty(UpdateVersion) && (AppVersion != UpdateVersion))
                                        {
                                            OptionsPanel.Visibility = System.Windows.Visibility.Visible;
                                            statusSpinner.IsError = true;
                                            statusSpinner.IsNotice = true;
                                            UpdateStatusLabel.Content = "An update for Lilypad is available. Would you like to install it?";
                                        }
                                    }
                                }

                            }

                            catch (Exception ex)
                            {                                

                            }
                        }

                    }
                }
                else
                {
                    UpdateStatusLabel.Content = "Updates unavailable - this machine is not connected to the internet.";
                    UpdateStatusLabel.Foreground = Brushes.Orange;
                    UpdateStatusSpinner.IsError = true;
                    UpdateStatusSpinner.IsNotice = true;
                }
            }
            else
            {
                UpdateStatusLabel.Content = "Running in debug mode - update unavailable.";
                UpdateStatusLabel.Foreground = Brushes.Orange;
                UpdateStatusSpinner.IsError = true;
                UpdateStatusSpinner.IsNotice = true;
            }
        }

        public bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                //Any exception will returns false.
                return false;
            }
        }

        void ad_CheckForUpdateProgressChanged(object sender, DeploymentProgressChangedEventArgs e)
        {
            UpdateStatusLabel.Content = "Checking for updates...";
            UpdatePercentLabel.Content = e.ProgressPercentage + @"%";
        }

        long sizeOfUpdate = 0;

        async void ad_CheckForUpdateCompleted(object sender, CheckForUpdateCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                UpdateStatusLabel.Content = "Unable to retrieve update from server.";
                UpdateStatusLabel.Foreground = Brushes.Red;
                UpdateStatusSpinner.IsError = true;
            }
            else if (e.Cancelled == true)
            {
                UpdateStatusLabel.Content = "Update process cancelled.";
                UpdateStatusLabel.Foreground = Brushes.Orange;
                UpdateStatusSpinner.IsError = true;
                UpdateStatusSpinner.IsNotice = true;
            }

            // Ask the user if they would like to update the application now. 
            if (e.UpdateAvailable)
            {
                sizeOfUpdate = e.UpdateSizeBytes;

                if (!e.IsUpdateRequired)
                {
                    OptionsPanel.Visibility = System.Windows.Visibility.Visible;
                    statusSpinner.IsError = true;
                    statusSpinner.IsNotice = true;
                    UpdateStatusLabel.Content = "An update for Lilypad is available. Would you like to install it?";
                }
                else
                {
                    YesButton.Content = "Update";
                    NoButton.Visibility = System.Windows.Visibility.Collapsed;
                    OptionsPanel.Visibility = System.Windows.Visibility.Visible;
                    statusSpinner.IsError = true;
                    statusSpinner.IsNotice = true;
                    UpdateStatusLabel.Content = "A required update for Lilypad is available. Click Update to install.";
                }
            }
            else
            {
                UpdateStatusLabel.Content = "You already have the latest version of Lilypad.";
                UpdateStatusSpinner.IsDone = true;
            }
        }

        private void ForceLogin()
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            if (!mvm.FrogIsLoggedIn)
            {
                mvm.FrogIsAdmin = true;
                mvm.FrogIsLoggedIn = true;
            }
            else
            {
                mvm.FrogIsLoggedIn = false;
                mvm.FrogIsAdmin = false;
            }
        }

        private void BeginUpdate()
        {
            UpdatePercentLabel.Visibility = System.Windows.Visibility.Visible;
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
                ad.UpdateCompleted += ad_UpdateCompleted;
                ad.UpdateProgressChanged += ad_UpdateProgressChanged;
                ad.UpdateAsync();
            }
            else
            {                
                    try
                    {
                    using (WebClient client3 = new WebClient())
                    {
                        client3.DownloadFileAsync(new Uri("http://skyislandsoftware.com/lilypad/twopointfive/Lilypad_Upgrade_" + UpdateVersion + ".msi"),
                                        @"C:\Program Files (x86)\Lilypad\Lilypad_Upgrade_" + UpdateVersion + ".msi");
                       
                        client3.DownloadFileCompleted += Client3_DownloadFileCompleted;
                        client3.DownloadProgressChanged += Client3_DownloadProgressChanged;
                    }

                    }
                    catch (Exception ex)
                    {
                    }
               
            }
        }

        private void Client3_DownloadProgressChanged(object sender, System.Net.DownloadProgressChangedEventArgs e)
        {
            UpdateStatusLabel.Content = "Checking for updates...";
            UpdatePercentLabel.Content = e.ProgressPercentage + @"%";
        }

        private void Client3_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                UpdateStatusLabel.Content = "Unable to retrieve update from server.";
                UpdateStatusLabel.Foreground = Brushes.Red;
                UpdateStatusSpinner.IsError = true;
            }
            else if (e.Cancelled == true)
            {
                UpdateStatusLabel.Content = "Update process cancelled.";
                UpdateStatusLabel.Foreground = Brushes.Orange;
                UpdateStatusSpinner.IsError = true;
                UpdateStatusSpinner.IsNotice = true;
            }
            else
            {
                if (File.Exists(@"C:\Program Files (x86)\Lilypad\Lilypad_Upgrade_" + UpdateVersion + ".msi"))
                {
                    var process = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            FileName = @"C:\Program Files (x86)\Lilypad\Lilypad_Upgrade_" + UpdateVersion + ".msi"
                        }
                    };

                    process.Start();
                    Thread.Sleep(1000);
                    process.WaitForExit();
                    string AppLastVersion = Properties.Settings.Default.App_Version;
                    Properties.Settings.Default.App_Version = UpdateVersion;
                    Properties.Settings.Default.App_LastVersion = AppLastVersion;
                    Properties.Settings.Default.Save();
                    System.Windows.Forms.Application.Restart();
                    Application.Current.Shutdown();
                }
            }
        }

        void ad_UpdateProgressChanged(object sender, DeploymentProgressChangedEventArgs e)
        {
            UpdateStatusLabel.Content = string.Format("{0} // {1} downloaded", Utilities.BytesToString(e.BytesCompleted), Utilities.BytesToString(e.BytesTotal));
            UpdatePercentLabel.Content = e.ProgressPercentage + @"%";
        }

        async void ad_UpdateCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Utilities.ShowSimpleDialog("Error Updating", "Unable to retrieve new version of Lilypad at this moment.");
                return;
            }
            else if (e.Cancelled == true)
            {
                Utilities.ShowSimpleDialog("Update Cancelled", "The update was cancelled by user.");
            }

            Properties.Settings.Default.App_ChangesAfterUpdateCommitted = false;
            Properties.Settings.Default.App_FirstRunUpdate = true;
            Properties.Settings.Default.Save();

            UpdateStatusSpinner.IsDone = true;
            UpdateStatusLabel.Content = "Update successful!";

            var mdr = await Utilities.ShowMetroYNDialog("Update Successful", "Lilypad's update will only take effect after a restart - would you like to restart now?");
            if (mdr == MahApps.Metro.Controls.Dialogs.MessageDialogResult.Affirmative)
            {
                System.Windows.Forms.Application.Restart();
                Application.Current.Shutdown();
            }
        }

        public void StartWampRemote()
        {
            try
            {
                var process = new System.Diagnostics.Process
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo { FileName = Properties.Settings.Default.Data_WAMPPath }
                };

                if (File.Exists(Properties.Settings.Default.Data_WAMPPath))
                {
                    process.Start();
                }

                if (!process.MainWindowTitle.Contains("Online"))
                {
                    System.Threading.Thread.Sleep(1500);
                }

                if (!bWorker.IsBusy) { bWorker.RunWorkerAsync(); }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        public string FoundServer;

        void bWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ConnectionStatusLabel.Content = e.UserState.ToString();
        }

        void bWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            FroggerList = new List<frogger>();
            HasErrors = false;

            string curIP = Properties.Settings.Default.MySQL_ServerIP.ToString();
            string outIP = "";
            if (curIP == "localhost") { outIP = Utilities.GetLocalIPAddress(); }
            else outIP = curIP;

            if (string.IsNullOrEmpty(outIP))
            {
                outIP = "127.0.0.1";
            }

            IPEndPoint ihost = new IPEndPoint(IPAddress.Parse(outIP), 3306);
            string tosearch = "";

            if (!TimeOutSocket.Connect(ihost, 70))
            {
                bWorker.ReportProgress(0, "Unable to connect to specified server.");

                HasErrors = true;
                ServerFound = false;

                string localIP = Utilities.GetLocalIPAddress();
                string subIP = localIP.Substring(0, localIP.LastIndexOf('.') + 1);

                System.Threading.Thread.Sleep(900);
                bWorker.ReportProgress(0, "Scanning network for available Lilypad server...");
                System.Threading.Thread.Sleep(900);

                for (int ip = 1; ip < 251; ip++)
                {
                    tosearch = subIP + ip.ToString();

                    bWorker.ReportProgress(0, "Checking for server at " + tosearch);

                    IPEndPoint host = new IPEndPoint(IPAddress.Parse(tosearch), 3306);

                    if (TimeOutSocket.Connect(host, 70))
                    {
                        FoundServer = tosearch;
                        ServerFound = true;
                        HasErrors = false;

                        Properties.Settings.Default.MySQL_ServerIP = FoundServer;
                        Properties.Settings.Default.Save();
                        Utilities.UpdateConfig();
                        break;
                    }
                    else
                    {
                        ServerFound = false;
                    }
                }
            }
            else
            {
                FoundServer = outIP;
                ServerFound = true;
                HasErrors = false;
            }
        }

        public string ErrorMsg;
        public bool TriedRestart;
        public bool ServerFound;
        void bWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            if (ServerFound)
            {
                string localIP = FoundServer;

                IPEndPoint host = new IPEndPoint(IPAddress.Parse(localIP), 3306);

                if (TimeOutSocket.Connect(host, 70))
                {
                    statusSpinner.IsDone = true;
                    ConnectionStatusLabel.Content = "Lilypad server found at " + FoundServer + ", connecting now...";
                    LoadFroggersFromList();
                }
                else
                {
                    var pos = localIP.LastIndexOf('.') + 1;
                    string subIP = localIP.Substring(0, pos);
                    string pfix = localIP.Substring(pos, localIP.Length - pos);
                    int lastc = Convert.ToInt32(pfix);
                    string ltcs = Convert.ToString(lastc - 1);
                    string secIP = subIP + ltcs;
                    IPEndPoint host2 = new IPEndPoint(IPAddress.Parse(secIP), 3306);
                    if (TimeOutSocket.Connect(host2, 90))
                    {
                        Properties.Settings.Default.MySQL_ServerIP = secIP;
                        Properties.Settings.Default.Save();
                        Utilities.UpdateConfig();
                        statusSpinner.IsDone = true;
                        ConnectionStatusLabel.Content = "Lilypad server found at " + FoundServer + ", connecting now...";
                        LoadFroggersFromList();
                    }
                    else
                    {
                        statusSpinner.IsError = true;
                        statusSpinner.IsNotice = false;
                        statusSpinner.IsBusy = false;
                        ConnectionStatusLabel.Foreground = Brushes.Red;
                        ConnectionStatusLabel.Content = "Lilypad server not found. Click here to retry.";
                    }
                }
            }
            else
            {
                statusSpinner.IsError = true;
                statusSpinner.IsNotice = false;
                statusSpinner.IsBusy = false;
                ConnectionStatusLabel.Foreground = Brushes.Red;
                ConnectionStatusLabel.Content = "Lilypad server not found. Click here to retry.";
            }
        }

        private async void LoadFroggersFromList()
        {
            await Task.Delay(700);
            if (Utilities.DBisOnline())
            {
                try
                {
                    // Do operations to update databases after an update.
                    string path = "C:\\Program Files (x86)\\Lilypad";
                    string fullpath = System.IO.Path.Combine(path, "ScriptUpdateFlagFile.txt");
                    if (File.Exists(fullpath))
                    {
                        Utilities.InstallUpdateSQLScript();
                    }

                    // Return update bool to false.
                    // Properties.Settings.Default.App_FirstRunUpdate = false;
                    // Properties.Settings.Default.Save();
                    using (Entities ctx = new Entities())
                    {
                        FroggerList = ctx.froggers.Where(x => x.FrogID != 0 && x.CurrentlyEmployed).ToList();
                    }
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }

            if (FroggerList.Count > 0)
            {
                foreach (frogger frog in FroggerList)
                {
                    Tile nt = new Tile();
                    nt.Header = frog.FrogFirstName;
                    nt.Tag = frog;
                    nt.MouseDown += nt_MouseDown;
                    nt.Click += nt_Click;

                    if (File.Exists(frog.FrogImagePath))
                    {
                        var fsize = new FileInfo(frog.FrogImagePath).Length;
                        if (fsize > 0 && IsValidGDIPlusImage(frog.FrogImagePath))
                        {
                            BitmapImage newImg = new BitmapImage(new Uri(frog.FrogImagePath, UriKind.Absolute));
                            try
                            {
                                ImageBrush nb = new ImageBrush(newImg);
                                nt.Background = nb;
                            }
                            catch (Exception)
                            {
                                SolidColorBrush newBrush = (SolidColorBrush)FindResource("AccentColorBrush");
                                nt.Background = newBrush;
                            }
                        }
                        else
                        {
                            SolidColorBrush newBrush = (SolidColorBrush)FindResource("AccentColorBrush");
                            nt.Background = newBrush;
                        }
                    }
                    else
                    {
                        SolidColorBrush newBrush = (SolidColorBrush)FindResource("AccentColorBrush");
                        nt.Background = newBrush;
                    }

                    nt.Style = (Style)FindResource("FrogTile");
                    TileController.Children.Add(nt);
                }

                RetrieverSP.Visibility = Visibility.Hidden;
            }
        }

        public bool IsValidGDIPlusImage(string filename)
        {
            try
            {
                using (var bmp = new Bitmap(filename))
                {
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        void nt_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Toaster.Loading("Logging In...", "");
        }

        private BackgroundWorker bWorker;
        private BackgroundWorker updater;

        private frogger SelectedLoggingInFrogger;

        void nt_Click(object sender, EventArgs e)
        {

            MainViewModel mvm = (MainViewModel)Application.Current.MainWindow.DataContext;

            var input = (Tile)sender;
            frogger selectedFrogger = (frogger)input.Tag;

            if (!string.IsNullOrEmpty(selectedFrogger.Notes))
            {
                Utilities.LoadTheme(selectedFrogger.Notes);
            }

            if (selectedFrogger.RequiresPINCode)
            {
                LoginLabel.Content = "enter pin code:";
                SelectedLoggingInFrogger = selectedFrogger;
                Keyboard.Focus(passwordBox);
            }
            else
            {
                mvm.LoggedInFrogger = selectedFrogger;
                mvm.GoHomeCommand.Execute(null);
            }
        }

        private void PINKeyDown(object sender, KeyEventArgs e)
        {
            MainViewModel mvm = (MainViewModel)Application.Current.MainWindow.DataContext;

            if (e.Key == Key.Enter)
            {
                if (passwordBox.Password == SelectedLoggingInFrogger.PINCode)
                {
                    mvm.LoggedInFrogger = SelectedLoggingInFrogger;
                    mvm.GoHomeCommand.Execute(null);
                }
                else
                {
                    LoginLabel.Content = "invalid. try again.";
                    LoginLabel.Foreground = Brushes.Red;
                    passwordBox.Password = "";
                    Keyboard.Focus(passwordBox);

                }
            }
        }

        public List<frogger> FroggerList;
        public bool HasErrors;

        private void PageLoaded(object sender, RoutedEventArgs e)
        {
            try
            {

                if (!bWorker.IsBusy)
                {
                    bWorker.RunWorkerAsync();
                }

                LoadChangelog();
                SetGreeting();

                if (Properties.Settings.Default.System_UpdatesEnabled)
                {
                    UpdateLilypad();
                }
            }
            catch (Exception ex)
            {
                using (System.IO.StreamWriter file =
           new System.IO.StreamWriter(@"log.txt"))
                {
                    file.WriteLine(ex.Message.ToString());
                }
            }
        }

        private void SetGreeting()
        {
            int now = DateTime.Now.Hour;
            if (now >= 0 && now < 12)
            {
                GreetingLabel.Content = "Good Morning.";
            }
            else if (now >= 12 && now < 17)
            {
                GreetingLabel.Content = "Good Afternoon.";
            }
            else if (now >= 17 && now < 24)
            {
                GreetingLabel.Content = "Good Evening.";
            }
            else
            {
                GreetingLabel.Content = "Welcome.";
            }
        }

        private void LoadChangelog()
        {
            LoginViewModel lvm = (LoginViewModel)this.DataContext;

            lvm.MainLog = Properties.Resources.Changelog.ToString();

            try
            {
                System.Net.WebClient wc = new System.Net.WebClient();
                wc.DownloadStringCompleted += wc_DownloadStringCompleted;
                wc.DownloadStringAsync(new Uri(@"http://www.skyislandsoftware.com/lilypad/currentversion/changelog.txt"));
            }
            catch (Exception ex)
            {
                new Error(ex);
            }

        }

        void wc_DownloadStringCompleted(object sender, System.Net.DownloadStringCompletedEventArgs e)
        {
            if (e.Error != null || e.Result != null)
            {
                try
                {
                    LoginViewModel lvm = (LoginViewModel)this.DataContext;
                    lvm.MainLog = e.Result.ToString();
                }
                catch (System.Reflection.TargetInvocationException)
                {

                }
            }
        }

        private void StartClick(object sender, MouseButtonEventArgs e)
        {
            if (!bWorker.IsBusy)
            {
                if (TileController.Children.Count > 0)
                {
                    statusSpinner.IsError = false;
                    statusSpinner.IsDone = false;
                    statusSpinner.IsBusy = true;
                    ConnectionStatusLabel.Content = "Connecting to the Lilypad server";
                    ConnectionStatusLabel.Foreground = Brushes.Gray;
                    bWorker.RunWorkerAsync();
                }
            }
        }


    }
}
