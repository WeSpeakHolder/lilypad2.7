﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Ookii.Dialogs.Wpf;
using Lilypad.ViewModel;

namespace Lilypad.Views.Initial
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Page3PreSetup : UserControl
    {
        public Page3PreSetup()
        {
            InitializeComponent();
        }

        private void PageLoaded(object sender, RoutedEventArgs e)
        {

        }

        private void ContinueClicked(object sender, EventArgs e)
        {
            TransitioningContentControl tcc = this.TryFindParent<TransitioningContentControl>();
            InitialSetupViewModel isv = (InitialSetupViewModel)tcc.DataContext;
            isv.GmailPassword = gmailPass.Password.ToString();
            isv.Page4StoreSetup.Execute(null);
        }

        private void TextboxFocused(object sender, RoutedEventArgs e)
        {
            TextBox t = (TextBox)sender;
            t.Clear();
        }

        private void Filegotfocus(object sender, RoutedEventArgs e)
        {
            ShowFolderSelector(sender);
        }

        private void EditFileClicked(object sender, EventArgs e)
        {
            System.Globalization.TextInfo ti = new System.Globalization.CultureInfo("en-US", false).TextInfo;
            string cap = ti.ToTitleCase(ownerName.Text);
            ownerName.Text = cap;
            ShowFolderSelector(sender);
        }

        private void ShowFolderSelector(object sender)
        {
            StackPanel sp = Utilities.FindParentByType<StackPanel>(((DependencyObject)sender));
            TextBlock tb = (TextBlock)sp.FindChildren<TextBlock>(true).First();
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.SelectedPath))
            {
                tb.Text = dialog.SelectedPath.ToString();
            }

            Panel3.Visibility = System.Windows.Visibility.Visible;
            continueTile.IsEnabled = true;
        }

        private void ownerName_KeyDown(object sender, KeyEventArgs e)
        {
            if (ownerName.Text.Length > 0)
            {
                Panel2.Visibility = System.Windows.Visibility.Visible;
            }
        }
    }
}
