﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.IO;
using MahApps.Metro.Controls;
using Lilypad.ViewModel;
using Lilypad.Model;

namespace Lilypad.Views.Initial
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Page1Welcome : UserControl
    {
        public Page1Welcome()
        {
            InitializeComponent();
        }

        private void PageLoaded(object sender, RoutedEventArgs e)
        {

        }

        private void ServerClicked(object sender, EventArgs e)
        {
            TransitioningContentControl tcc = this.TryFindParent<TransitioningContentControl>();
            InitialSetupViewModel isv = (InitialSetupViewModel)tcc.DataContext;
            isv.Page2Agreement.Execute(null);
        }

        private void ClientClicked(object sender, EventArgs e)
        {
            try
            {
                InitialSetupViewModel ivm = (InitialSetupViewModel)this.DataContext;

                string dirr = ivm.Store.MainDirectory;
                string lpmain = Path.Combine(dirr, "Lilypad");
                string orders = Path.Combine(dirr, "Orders");

                string alphadata = Path.Combine(lpmain, "AlphaData");
                string resources = Path.Combine(lpmain, "Resources");
                string employees = Path.Combine(lpmain, "Employees");
                string templates = Path.Combine(lpmain, "Templates");

                string pending = Path.Combine(orders, "Pending");
                string production = Path.Combine(orders, "Production");
                string archive = Path.Combine(orders, "Archive");

                Properties.Settings.Default.Data_AlphaData = alphadata;
                Properties.Settings.Default.Data_ArchiveFolderPath = archive;
                Properties.Settings.Default.Data_PendingFolderPath = pending;
                Properties.Settings.Default.Data_ProductionFolderPath = production;
                Properties.Settings.Default.Data_ResourcePath = resources;
                Properties.Settings.Default.Data_TemplatesPath = templates;
                Properties.Settings.Default.MySQL_Password = ivm.Store.StoreLilyPass;
                Properties.Settings.Default.MySQL_UserName = "lily";
                Properties.Settings.Default.App_FirstRun = false;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private void ContinueClicked(object sender, EventArgs e)
        {
            ValidationPanel.Visibility = System.Windows.Visibility.Collapsed;
            StartContainer.Visibility = System.Windows.Visibility.Visible;
        }

        private void ValidateClicked(object sender, EventArgs e)
        {
            List<bigfrogstore> stores = new List<bigfrogstore>();
            InitialSetupViewModel ivm = (InitialSetupViewModel)this.DataContext;
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            if (!mvm.FrogIsAdmin)
            {
                try
                {
                    using (Cloud cl = new Cloud())
                    {
                        stores = (from t in cl.bigfrogstores select t).ToList();
                    }
                }
                catch
                {
                    ValidationLabel.Content = "Problem connecting to validation server.";
                    ValidationLabel.Foreground = Brushes.Red;
                }

                if (stores.Any(x => x.LicenseKey == ivm.LicenseKey))
                {
                    ivm.Store = stores.First(x => x.LicenseKey == ivm.LicenseKey);
                    ivm.LicenseValid = true;
                    ValidationLabel.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
                    ValidationLabel.Content = "License validated successfully." + Environment.NewLine + "Store: " + ivm.Store.StoreName + " has been activated.";
                    ValidationLabel.Foreground = Brushes.Green;
                }
                else
                {
                    ivm.LicenseValid = false;
                    ValidationLabel.Content = "Invalid license key.";
                    ValidationLabel.Foreground = Brushes.Red;
                }
            }
            else
            {
                ivm.LicenseValid = true;
                ValidationLabel.Content = "Bypassed";
            }
        }
    }
}
