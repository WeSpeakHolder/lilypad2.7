﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Lilypad.ViewModel;
using Ookii.Dialogs.Wpf;

namespace Lilypad.Views.Initial
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Page4StoreSetup : UserControl
    {
        public Page4StoreSetup()
        {
            InitializeComponent();
        }

        private void PageLoaded(object sender, RoutedEventArgs e)
        {

        }

        private void ContinueClicked(object sender, EventArgs e)
        {
            InitialSetupViewModel isvm = (InitialSetupViewModel)this.DataContext;
            isvm.Page5Install.Execute(null);
        }

        private void TextboxFocused(object sender, RoutedEventArgs e)
        {
            TextBox t = (TextBox)sender;
            t.Clear();
        }

    }
}
