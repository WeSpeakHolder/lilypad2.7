﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using MySql.Data.Entity;
using MySql.Data.MySqlClient;
using System.Net;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Lilypad.ViewModel;
using MahApps.Metro.Controls;
using Ookii.Dialogs.Wpf;

namespace Lilypad.Views.Initial
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Page5Install : UserControl
    {
        public Page5Install()
        {
            InitializeComponent();
            bWorker = new BackgroundWorker();
            bWorker.WorkerReportsProgress = true;
            bWorker.WorkerSupportsCancellation = false;
            bWorker.DoWork += bWorker_DoWork;
            bWorker.ProgressChanged += bWorker_ProgressChanged;
            bWorker.RunWorkerCompleted += bWorker_RunWorkerCompleted;

            iWorker = new BackgroundWorker();
            iWorker.WorkerReportsProgress = true;
            iWorker.WorkerSupportsCancellation = false;
            iWorker.DoWork += iWorker_DoWork;
            iWorker.ProgressChanged += iWorker_ProgressChanged;
            iWorker.RunWorkerCompleted += iWorker_RunWorkerCompleted;

        }

        void iWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            statusLabel.Content = "Installation successful!";
            statusLabel.Foreground = Brushes.YellowGreen;
            MetroBar.Visibility = System.Windows.Visibility.Hidden;
        }

        void iWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            statusLabel.Content = e.UserState.ToString();
        }

        void iWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                iWorker.ReportProgress(20, "Setting up MySQL user accounts...");
                MySqlConnectionStringBuilder sb = new MySqlConnectionStringBuilder();
                sb.Server = "localhost";
                sb.Port = 3306;
                sb.UserID = "root";
                sb.ConvertZeroDateTime = true;
                sb.Password = newPass;
                using (MySqlConnection conn = new MySqlConnection(sb.GetConnectionString(true)))
                {
                    conn.Open();

                    string command2 = @"CREATE USER 'lily'@'localhost' IDENTIFIED BY '" + ivm.Store.StoreLilyPass + "';";
                    MySqlCommand comm2 = new MySqlCommand(command2, conn);
                    comm2.ExecuteNonQuery();

                    string command3 = @"GRANT ALL PRIVILEGES ON *.* TO 'lily'@'localhost' WITH GRANT OPTION;";
                    MySqlCommand comm3 = new MySqlCommand(command3, conn);
                    comm3.ExecuteNonQuery();

                    string command4 = @"CREATE USER 'lily'@'%' IDENTIFIED BY '" + ivm.Store.StoreLilyPass + @"';";
                    MySqlCommand comm4 = new MySqlCommand(command4, conn);
                    comm4.ExecuteNonQuery();

                    string command5 = @"GRANT ALL PRIVILEGES ON *.* TO 'lily'@'%' WITH GRANT OPTION;";
                    MySqlCommand comm5 = new MySqlCommand(command5, conn);
                    comm5.ExecuteNonQuery();

                    string command7 = "CREATE DATABASE lilypad;";
                    MySqlCommand comm7 = new MySqlCommand(command7, conn);
                    comm7.ExecuteNonQuery();

                    string command82 = @"GRANT USAGE ON *.* TO ''@'localhost';";
                    MySqlCommand comm82 = new MySqlCommand(command82, conn);
                    comm82.ExecuteNonQuery();

                    string command =  @"DROP USER ''@'localhost';";
                    MySqlCommand comm = new MySqlCommand(command, conn);
                    comm.ExecuteNonQuery();

                    string command8 = "UPDATE mysql.user SET Password = PASSWORD('" + newPass + "') WHERE User = 'root';";
                    MySqlCommand comm8 = new MySqlCommand(command8, conn);
                    comm8.ExecuteNonQuery();

                    string command6 = "FLUSH PRIVILEGES;";
                    MySqlCommand comm6 = new MySqlCommand(command6, conn);
                    comm6.ExecuteNonQuery();

                    conn.Close();
                }

                string dirr = ivm.Store.MainDirectory;
                string lpmain = Path.Combine(dirr, "Lilypad");
                string orders = Path.Combine(dirr, "Orders");

                string alphadata = Path.Combine(lpmain, "AlphaData");
                string resources = Path.Combine(lpmain, "Resources");
                string employees = Path.Combine(lpmain, "Employees");
                string templates = Path.Combine(lpmain, "Templates");

                string pending = Path.Combine(orders, "Pending");
                string production = Path.Combine(orders, "Production");
                string archive = Path.Combine(orders, "Archive");

                Properties.Settings.Default.Data_AlphaData = alphadata;
                Properties.Settings.Default.Data_ArchiveFolderPath = archive;
                Properties.Settings.Default.Data_PendingFolderPath = pending;
                Properties.Settings.Default.Data_ProductionFolderPath = production;
                Properties.Settings.Default.Data_ResourcePath = resources;
                Properties.Settings.Default.Data_TemplatesPath = templates;
                Properties.Settings.Default.MySQL_Password = ivm.Store.StoreLilyPass;
                Properties.Settings.Default.MySQL_ServerIP = "localhost";
                Properties.Settings.Default.MySQL_UserName = "lily";
                Properties.Settings.Default.App_FirstRun = false;
                Properties.Settings.Default.Save();

                iWorker.ReportProgress(40, "Installing lilypad main database...");

            }
            catch (Exception ex)
            {
                new Error(ex);
            }
            try
            {
                Utilities.InitialDatabaseInstall();
            }
            catch (Exception ex)
            {
                new Error(ex);
            }

            iWorker.ReportProgress(99, "Cleaning up...");
        }

        public string wampDir;
        public string newPass;
        public InitialSetupViewModel ivm;

        void bWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                bWorker.ReportProgress(3, "Setting up Lilypad folders...");
                ivm.Store.MainDirectory = ivm.MainDirectory;
                string mainDir = ivm.MainDirectory;
                string lpmain = Path.Combine(mainDir, "Lilypad");
                string orders = Path.Combine(mainDir, "Orders");

                string alphadata = Path.Combine(lpmain, "AlphaData");
                string resources = Path.Combine(lpmain, "Resources");
                string employees = Path.Combine(lpmain, "Employees");
                string templates = Path.Combine(lpmain, "Templates");

                string pending = Path.Combine(orders, "Pending");
                string production = Path.Combine(orders, "Production");
                string archive = Path.Combine(orders, "Archive");

                wampDir = resources + "//wamp.exe";

                Directory.CreateDirectory(lpmain);
                Directory.CreateDirectory(orders);
                Directory.CreateDirectory(alphadata);
                Directory.CreateDirectory(resources);
                Directory.CreateDirectory(employees);
                Directory.CreateDirectory(templates);
                Directory.CreateDirectory(pending);
                Directory.CreateDirectory(production);
                Directory.CreateDirectory(archive);

                bWorker.ReportProgress(9, "Downloading WAMP installation package from cloud server...");

                using (WebClient wc = new WebClient())
                {
                    wc.DownloadFile(@"http://www.skyislandsoftware.com/lilypad/wamp.exe", wampDir);
                }

                bWorker.ReportProgress(15, "Download completed. Please run WAMP installer before continuing.");
                
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        void bWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            statusLabel.Content = e.UserState.ToString();
        }

        void bWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            System.Diagnostics.Process.Start(wampDir);
            MetroBar.Visibility = System.Windows.Visibility.Hidden;
            installTile.Background = Brushes.Orange;
            installTile.Content = "Continue Installation";
        }

        BackgroundWorker bWorker;
        BackgroundWorker iWorker;

        private void PageLoaded(object sender, RoutedEventArgs e)
        {

        }

        private void ContinueClicked(object sender, EventArgs e)
        {
            
        }

        private void TextboxFocused(object sender, RoutedEventArgs e)
        {
            TextBox t = (TextBox)sender;
            t.Clear();
        }

        private void InstallClicked(object sender, EventArgs e)
        {
            if (installTile.Content.ToString() == "Install Lilypad")
            {
                installTile.Content = "Installing, please wait...";
                MetroBar.Visibility = System.Windows.Visibility.Visible;
                ivm = (InitialSetupViewModel)this.DataContext;

                bWorker.RunWorkerAsync();
            }
            else if (installTile.Content.ToString() == "Continue Installation")
            {
                newPass = Utilities.PasswordGenerator(9, true);
                ivm.Store.RootPass = newPass;
                try
                {
                    using (Model.Cloud cl = new Model.Cloud())
                    {
                        var store = (from t in cl.bigfrogstores where t.StoreID == ivm.Store.StoreID select t).FirstOrDefault();
                        store.RootPass = newPass;
                        store.MainDirectory = ivm.Store.MainDirectory;
                        store.LicenseValidated = true;
                        store.LicenseEULAAccepted = true;
                        store.ValidationDate = DateTime.Now;
                        cl.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
                installTile.Content = "Installing, please wait...";
                installTile.Background = Brushes.YellowGreen;
                MetroBar.Visibility = System.Windows.Visibility.Visible;
                iWorker.RunWorkerAsync();
            }
        }

    }
}
