﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Lilypad.ViewModel;


namespace Lilypad.Views.Initial
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Page2Agreement : UserControl
    {
        public Page2Agreement()
        {
            InitializeComponent();
        }

        private void PageLoaded(object sender, RoutedEventArgs e)
        {

        }

        private void Page2Loaded(object sender, RoutedEventArgs e)
        {
            agreementBox.Text = Utilities.LoremIpsum(3, 15, 2, 9, 10);
        }

        private void ContinueClicked(object sender, EventArgs e)
        {
            TransitioningContentControl tcc = this.TryFindParent<TransitioningContentControl>();
            InitialSetupViewModel isv = (InitialSetupViewModel)tcc.DataContext;
            isv.Page3PreSetup.Execute(null);
        }
    }
}
