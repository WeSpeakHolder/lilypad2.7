﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using Lilypad.Model;
using Lilypad.ViewModel;

namespace Lilypad.View
{
    /// <summary>
    ///     Interaction logic for InventoryOrders.xaml
    /// </summary>
    public partial class InventoryOrders : UserControl
    {
        public ObservableCollection<alphaitem> SelectedQAItems = new ObservableCollection<alphaitem>();

        public InventoryOrders()
        {
            InitializeComponent();
        }

        private void InventoryOrdersLoaded(object sender, RoutedEventArgs e)
        {
            LoadBrands();
        }

        private void grid_GroupRowExpanding(object sender, RowAllowEventArgs e)
        {
        }

        private void GridSelectedItemChanged(object sender, SelectedItemChangedEventArgs e)
        {
        }

        private void SelectItem(object sender, MouseButtonEventArgs e)
        {
        }

        private void GridViewRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
        }

        private void PrintGridClicked(object sender, EventArgs e)
        {
            var preview = new DocumentPreviewWindow();
            var link = new PrintableControlLink(GridView);
            var model = new LinkPreviewModel(link);
            preview.Model = model;
            link.Landscape = true;
            link.CreateDocument(true);
            ThemeManager.SetThemeName(preview, Theme.MetropolisLightName);
            preview.ShowDialog();
        }

        private void GridDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var ovm = (InventoryOrdersViewModel)DataContext;
            var ord = ovm.SelectedPastOrder;
            ((MainViewModel)App.Current.MainWindow.DataContext).GoInventoryOrderDetails(ord);
            // UI.NavigateAlphaOrderDetails(ord);
        }

        private void ViewCurrentOrder(object sender, EventArgs e)
        {
            ((MainViewModel) App.Current.MainWindow.DataContext).GoInventory();
        }

        private void FindOrders(object sender, EventArgs e)
        {
            var ordList = new List<alphaorder>();
            var itemList = new List<alphaentries1>();

            using (var ctx = new Entities())
            {
                var ID = StockIDLookup.EditValue.ToString();
                var col = ColorsLookup.EditValue.ToString();
                var siz = SizeLookup.EditValue.ToString();

                itemList =
                    (from q in ctx.alphaentries1 where q.IDStockAlpha == ID && q.Color == col && q.Size == siz select q)
                        .ToList();

                foreach (var ent in itemList)
                {
                    var idPO = "";
                    var found = (from k in ctx.alphaorders where k.IDPO == ent.IDPO select k).FirstOrDefault();
                    ordList.Add(found);
                }
            }

            findergrid.ItemsSource = ordList;
        }

        private void LoadBrands()
        {
            using (var context = new Entities())
            {
                var stock =
                    (from k in context.alphaitems select new { Value = k.Mill_Name }).Distinct()
                        .OrderBy(x => x.Value)
                        .ToList();

                StockMfgLookup.ItemsSource = stock;
                StockMfgLookup.ValueMember = "Value";
                StockMfgLookup.DisplayMember = "Value";
            }
        }

        private void BrandChanged(object sender, EditValueChangedEventArgs e)
        {
            var mill = StockMfgLookup.EditValue.ToString();
            if (e.NewValue != null)
            {
                using (var ctx = new Entities())
                {
                    StockIDLookup.ItemsSource =
                        (from k in ctx.alphaitems where k.Mill_Name == mill select new { Value = k.Style_Code }).Distinct()
                            .OrderBy(x => x.Value)
                            .ToList();
                    StockIDLookup.ValueMember = "Value";
                    StockIDLookup.DisplayMember = "Value";
                }
            }
        }

        private void StockIDChanged(object sender, EditValueChangedEventArgs e)
        {
            var mill = StockMfgLookup.EditValue.ToString();
            var id = StockIDLookup.EditValue.ToString();

            var ti = new CultureInfo("en-US", false).TextInfo;

            if (e.NewValue != null)
            {
                using (var ctx = new Entities())
                {
                    var cList =
                        (from k in ctx.alphaitems
                         where k.Mill_Name == mill && k.Style_Code == id
                         select new { Value = k.Color_Name }).Distinct().OrderBy(x => x.Value).ToList();
                    var oList = cList.ConvertAll(d => ti.ToTitleCase(d.Value.ToLower()));
                    ColorsLookup.ItemsSource = oList;
                }
            }
        }

        private void ColorChanged(object sender, EditValueChangedEventArgs e)
        {
            var color = ColorsLookup.EditValue.ToString();
            var mill = StockMfgLookup.EditValue.ToString();
            var id = StockIDLookup.EditValue.ToString();

            IList<string> unSorted;
            IList<string> Sorted;

            if (e.NewValue != null)
            {
                using (var ctx = new Entities())
                {
                    SelectedQAItems.Clear();

                    var items =
                        (from k in ctx.alphaitems
                         where k.Mill_Name == mill && k.Style_Code == id && k.Color_Name == color
                         select k).ToList();
                    foreach (var item in items)
                    {
                        SelectedQAItems.Add(item);
                    }
                    unSorted = (from g in items select g.Size_Name).ToArray<string>();

                    Sorted =
                        Utilities.GarmentSizeArray.Select(x => unSorted.Contains(x) ? x : "-1")
                            .Where(x => x != "-1")
                            .ToList();
                    SizeLookup.ItemsSource = Sorted;
                }
            }
        }

        private void SizeValChanged(object sender, EditValueChangedEventArgs e)
        {
            if (!FindOrdersButton.IsEnabled)
            {
                FindOrdersButton.IsEnabled = true;
                ButtonBord.Background = (SolidColorBrush)FindResource("AccentColorBrush");
                OrderFinderGrid.Opacity = 1;
            }
        }

        private void QuantityChanged(object sender, EditValueChangedEventArgs e)
        {
        }

        private void FinderGridDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var ovm = (InventoryOrdersViewModel)DataContext;
            var ord = ovm.SelectedFoundOrder;
            ((MainViewModel)App.Current.MainWindow.DataContext).GoInventoryOrderDetails(ord);
           //  UI.NavigateAlphaOrderDetails(ord);
        }

        private void SearchProductsInOrders(object sender, EventArgs e)
        {
            if (ProductsLookupGrid.Opacity == 0)
            {
                ProductsLookupGrid.DownwardSlide(TimeSpan.FromMilliseconds(200));
                ProductsLookupGrid.OpacityAnimation(1.0, TimeSpan.FromMilliseconds(200));
            }
            else
            {
                ProductsLookupGrid.UpwardSlide(TimeSpan.FromMilliseconds(200));
                ProductsLookupGrid.OpacityAnimation(0, TimeSpan.FromMilliseconds(200));
            }
        }
    }
}