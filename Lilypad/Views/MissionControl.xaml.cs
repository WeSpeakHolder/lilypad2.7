﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Lilypad.ViewModel;

namespace Lilypad.Views
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MissionControl : UserControl
    {
        public MissionControl()
        {
            InitializeComponent();
        }

        private void SettingsAbout(object sender, MouseButtonEventArgs e)
        {
            MissionControlViewModel mvm = (MissionControlViewModel)this.DataContext;
        }

        private void SettingsClicked(object sender, MouseButtonEventArgs e)
        {
            foreach (Label child in ButtonsPanel.Children) { child.Foreground = new BrushConverter().ConvertFromString("#909090") as System.Windows.Media.Brush; }
            Label item = (Label)sender;
            item.Foreground = new BrushConverter().ConvertFromString("#303030") as System.Windows.Media.Brush;
        }

        private void MCLoaded(object sender, RoutedEventArgs e)
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            MissionControlViewModel mcvm = (MissionControlViewModel)this.DataContext;
            if (mvm.BrowsingMyProfile)
            {
                mcvm.OpenSettings_MyProfile();
                mvm.BrowsingMyProfile = false;
            }
        }

    }
}
