﻿using DevExpress.Xpf.Printing;
using DevExpress.Xpf.WindowsUI.Navigation;
using Lilypad.Controls.Tags;
using Lilypad.Model;
using Lilypad.ViewModel;
using LilyReports;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DevExpress.XtraReports.UI;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
using DevExpress.Xpf.Editors;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Deployment.Application;

namespace Lilypad.Views
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class NewOrder : UserControl, INavigationAware
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        //List<int> ReceiptNotLoaded = new List<int>();

        public NewOrder()
        {
            Toaster.Loading("Loading...", "");
            InitializeComponent();
            bWorker = new BackgroundWorker();
            bLoader = new BackgroundWorker();

            bWorker.DoWork += bWorker_DoWork;
            bWorker.WorkerReportsProgress = true;
            bWorker.WorkerSupportsCancellation = false;
            bWorker.ProgressChanged += bWorker_ProgressChanged;
            bWorker.RunWorkerCompleted += bWorker_RunWorkerCompleted;

            bLoader.DoWork += bLoader_DoWork;
            bLoader.WorkerReportsProgress = true;
            bLoader.WorkerSupportsCancellation = false;
            bLoader.ProgressChanged += bLoader_ProgressChanged;
            bLoader.RunWorkerCompleted += bLoader_RunWorkerCompleted;
        }

        void bLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Toaster.DismissLoading();
            //string newline = string.Join(",", ReceiptNotLoaded);
            //char delimiter = ',';
            //var allParts = newline.Split(delimiter);
            //string result = allParts.Select((item, index) => (index != 0 && (index + 1) % 15 == 0)
            //                    ? item + delimiter + "\n"
            //                    : index != allParts.Count() - 1 ? item + delimiter : item)
            //                    .Aggregate((i, j) => i + j);

            //if (ReceiptNotLoaded.Count() > 0)
            //    Utilities.ShowSimpleDialogReceipts("Receipt already exists", "Following invoices already exists \n" + result + " in Lillypad DB.");
        }

        void bLoader_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() =>
            {
                try
                {
                    var vm = (OrderNewViewModel)this.DataContext;
                    DataRow row = (DataRow)e.UserState;
                    var newVM = new OrderNewViewModel();
                    string cname = row["CustomerName"].ToString();
                    string cfirst = "";
                    string lrr = "";
                    string clast = "";

                    if (cname.Contains(' '))
                    {
                        cfirst = cname.Split(' ')[0];
                        lrr = cname.Remove(0, cname.IndexOf(' ')).Trim();
                        clast = lrr;
                    }
                    else
                    {
                        cfirst = cname;
                    }
                    newVM.CustomerFull = cname + " #" + row["ReceiptNum"].ToString();
                    newVM.POSDecortingType = row["GroupDescription"].ToString();
                    newVM.NewHopperOrder.CustFirst = cfirst;
                    newVM.NewHopperOrder.CustLast = clast;
                    newVM.NewHopperOrder.ReceiptNum = Convert.ToInt32(row["ReceiptNum"]);
                    newVM.NewHopperOrder.CustCompany = row["CustomerCompany"].ToString();
                    newVM.NewHopperOrder.OrderDate = Convert.ToDateTime(row["OrderDateTime"]);
                    vm.InputRows.Add(newVM);
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }

            }));
        }

        void bLoader_DoWork(object sender, DoWorkEventArgs e)
        {
            DataTable orders = new DataTable();

            string path = Properties.Settings.Default.Data_ResourcePath;
            int threshold = Convert.ToInt32(Properties.Settings.Default.Data_RecordSelectThreshold);
            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + @"\TJobs.mdb";
            string Select = @"SELECT DISTINCT TOP " + threshold + " GroupDescription, ReceiptNum, CustId, CustomerName, CustomerCompany, OrderDateTime, DueDateTime FROM TJobsTracking WHERE CustId<>1 ORDER BY ReceiptNum DESC";
            OleDbConnection pwConn = new OleDbConnection(dbConnect); OleDbDataAdapter adapter = new OleDbDataAdapter(Select, pwConn);
            try
            {
                pwConn.Open();
                adapter.Fill(orders);
                pwConn.Close();
            }
            catch (Exception ex)
            {
                new Error(ex);
            }

            List<int> DupeList = new List<int>();
            //List<string> Allnames = new List<string>();

            for (int i = 0; i < orders.Rows.Count; i++)
            {
                try
                {
                    double perdd = Math.Round((double)(i / orders.Rows.Count) * 100);
                    int percent = Convert.ToInt32(perdd);
                    bool Deleted = false;
                    DataRow row = orders.Rows[i];

                    //Allnames.Add(row["CustomerName"].ToString().Split(' ')[1]);

                    int receipt = Convert.ToInt32(row["ReceiptNum"]);
                    if (!DupeList.Contains(receipt))
                    {
                        DupeList.Add(receipt);
                    }
                    else
                    {
                        Deleted = true;
                        row.Delete();
                    }
                    using (Entities ctx = new Entities())
                    {
                        if (ctx.orders.Any(o => o.ReceiptNum == receipt))
                        {
                            Deleted = true;
                            row.Delete();
                            //if (!ReceiptNotLoaded.Contains(receipt))
                            //    ReceiptNotLoaded.Add(receipt);
                        }
                        else if (ctx.orderexceptions.Any(k => k.ReceiptNum == receipt))
                        {
                            Deleted = true;
                            row.Delete();
                            //if (!ReceiptNotLoaded.Contains(receipt))
                            //    ReceiptNotLoaded.Add(receipt);
                        }
                        else if (ctx.hopperorders.Any(p => p.ReceiptNum == receipt))
                        {
                            Deleted = true;
                            row.Delete();
                            //if (!ReceiptNotLoaded.Contains(receipt))
                            //    ReceiptNotLoaded.Add(receipt);
                        }
                        else if (ctx.receipts.Any(p => p.RcptNumber == receipt))
                        {
                            Deleted = true;
                            row.Delete();
                            //if (!ReceiptNotLoaded.Contains(receipt))
                            //    ReceiptNotLoaded.Add(receipt);
                        }
                        //    orders.AcceptChanges();
                    }

                    if (!Deleted)
                    {
                        bLoader.ReportProgress(percent, row);
                    }

                    //var duplicateKeys = Allnames.GroupBy(x => x)
                    //    .Where(group => group.Count() > 1)
                    //    .Select(group => group.Key);

                    //using (TextWriter tw = new StreamWriter(@"D:\dupename.txt"))
                    //{
                    //    foreach (String s in duplicateKeys)
                    //        tw.WriteLine(s);
                    //}
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }


        private void LoadCustomReceiptNumber(int receiptNum)
        {
            DataTable orders = new DataTable();

            string path = Properties.Settings.Default.Data_ResourcePath;
            int threshold = Convert.ToInt32(Properties.Settings.Default.Data_RecordSelectThreshold);
            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + @"\TJobs.mdb";
            string Select = @"SELECT TOP 1 GroupDescription, ReceiptNum, CustId, CustomerName, CustomerCompany, OrderDateTime, DueDateTime FROM TJobsTracking WHERE ReceiptNum = " + receiptNum + " ORDER BY ReceiptNum DESC";
            OleDbConnection pwConn = new OleDbConnection(dbConnect); OleDbDataAdapter adapter = new OleDbDataAdapter(Select, pwConn);
            try
            {
                pwConn.Open();
                adapter.Fill(orders);
                pwConn.Close();
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
            try
            {
                if (orders.Rows.Count > 0)
                {
                    List<int> DupeList = new List<int>();

                    var vm = (OrderNewViewModel)this.DataContext;
                    DataRow row = (DataRow)orders.Rows[0];
                    var newVM = new OrderNewViewModel();
                    newVM.CustomerFull = row["CustomerName"].ToString() + " #" + receiptNum;
                    newVM.NewHopperOrder.CustFirst = row["CustomerName"].ToString().Split(' ')[0];
                    newVM.NewHopperOrder.CustLast = row["CustomerName"].ToString().Split(' ')[1];
                    newVM.NewHopperOrder.ReceiptNum = Convert.ToInt32(row["ReceiptNum"]);
                    newVM.NewHopperOrder.POSDecortionType = row["GroupDescription"].ToString();
                    newVM.NewHopperOrder.CustCompany = row["CustomerCompany"].ToString();
                    newVM.NewHopperOrder.OrderDate = Convert.ToDateTime(row["OrderDateTime"]);
                    vm.InputRows.Add(newVM);
                }
                else
                {
                    Utilities.ShowSimpleDialog("Receipt Doesn't Exist", "Unable to find receipt #" + receiptNum + " in the ReSource jobs database.");
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private BackgroundWorker bWorker;
        private BackgroundWorker bLoader;
        private void TextboxKeydown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                OrderNewViewModel vm = (OrderNewViewModel)this.DataContext;
                string tagToadd = TagBox.Text.ToString();
                Tag newTag = new Tag(tagToadd);
                vm.TagCollection.Add(newTag);
                Keyboard.Focus(TagBox);
                TagBox.Clear();
                VerifyOrderDetails();
            }
        }

        private void TagBoxGotFocus(object sender, RoutedEventArgs e)
        {
            TagBox.Clear();
            Keyboard.Focus(TagBox);
        }

        private void TagBoxLostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TagBox.Text))
            {
                TagBox.Text = "Add a tag...";
            }
        }

        public int desnum = 0;

        public void ResetDesignNumOrders()
        {
            int reset = 1;
            foreach (Expander exx in expanderContainer.Children)
            {
                exx.Header = "Design " + reset;
                var card = (Controls.DesignCards.SetupCard)exx.Content;
                DesignSetupCardViewModel vm = (DesignSetupCardViewModel)card.DataContext;
                vm.SelectedDesignDetail.DesignNum = reset;
                vm.DesignNum = reset;
                reset++;
            }
        }

        private void AddDesignClick(object sender, RoutedEventArgs e)
        {
            desnum = expanderContainer.Children.Count + 1;
            foreach (Expander exx in expanderContainer.Children)
            {
                exx.IsExpanded = false;
            }
            Expander exp = new Expander();
            DesignSetupCardViewModel vm = new DesignSetupCardViewModel();
            designdetail detail = new designdetail();
            detail.DesignNum = desnum;
            detail.DesignDescription = "";
            detail.PrintProcess = Convert.ToInt32(PrintProcessEditor.EditValue);
            if (Properties.Settings.Default.Pref_DefaultSizeToGarment)
            {
                detail.SizeToGarment = true;
                detail.PrintToFile = false;
            }
            vm.SelectedDesignDetail = detail;
            vm.DesignNum = desnum;
            Controls.DesignCards.SetupCard card = new Controls.DesignCards.SetupCard(vm);
            TranslateTransform tran = new TranslateTransform(); tran.X = -60;
            Storyboard sb = this.FindResource("LeftOpenAnimation") as Storyboard;
            exp.HeaderTemplate = (DataTemplate)this.FindResource("ExpTemplate");
            exp.Tag = vm;
            exp.Opacity = 0;
            exp.RenderTransform = tran;
            exp.Margin = new Thickness(0);
            exp.Header = "Design " + desnum;
            exp.Content = card;
            exp.IsExpanded = true;
            expanderContainer.Children.Add(exp);
            exp.BeginStoryboard(sb);
        }

        private void KeyDownTest(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Storyboard sb = this.FindResource("LeftCloseAnimation") as Storyboard;
                OrderSetupMainSP.BeginStoryboard(sb);
            }
        }

        public void VerifyOrderDetails()
        {
            OrderNewViewModel vm = (OrderNewViewModel)this.DataContext;
            bool flag = true;

            // Verify Order Notes
            if (RichEditor.Text.Length > 0) { vm.Verify_Notes = true; }
            else { vm.Verify_Notes = false; flag = true; }

            // Verify Bin Number
            if (BinNumEditor.Text.Length > 0) { vm.Verify_BinNum = true; }
            else { vm.Verify_BinNum = false; flag = false; }

            // Verify Meta tags
            if (vm.TagCollection.Count > 0) { vm.Verify_Metatags = true; }
            else { vm.Verify_Metatags = false; flag = false; }

            // Verify Print Process
            if (PrintProcessEditor.EditValue != null) { vm.Verify_Process = true; }
            else { vm.Verify_Process = false; flag = false; }

            // Verify Artwork Process
            if (ArtworkEditor.EditValue != null) { vm.Verify_Artwork = true; }
            else { vm.Verify_Artwork = false; flag = false; }

            // Verify Inventory Process
            if (InventoryEditor.EditValue != null) { vm.Verify_Inventory = true; }
            else { vm.Verify_Inventory = false; flag = false; }

            // Verify Status
            if (StatusEditor.EditValue != null) { vm.Verify_Status = true; }
            else { vm.Verify_Status = false; flag = false; }

            // Verify Priority
            if (PriorityEditor.EditValue != null) { vm.Verify_Priority = true; }
            else { vm.Verify_Priority = false; flag = false; }

            // Verify Correspondence
            if (ContactEditor.EditValue != null) { vm.Verify_Correspondence = true; }
            else { vm.Verify_Correspondence = false; flag = false; }

            // Verify Due Date
            if (DateEditor.DateTime > DateTime.Now) { vm.Verify_DueDate = true; }
            else { vm.Verify_DueDate = false; flag = false; }

            if (flag) { vm.Verify_Master = true; }
            else { vm.Verify_Master = false; }
        }

        public void NavigatedFrom(DevExpress.Xpf.WindowsUI.Navigation.NavigationEventArgs e)
        {

        }

        public void NavigatedTo(DevExpress.Xpf.WindowsUI.Navigation.NavigationEventArgs e)
        {

        }

        public void NavigatingFrom(NavigatingEventArgs e)
        {
            MainWindow main = (MainWindow)Application.Current.MainWindow;
            MainViewModel mvm = (MainViewModel)main.DataContext;
            foreach (string item in mvm.DesignCardList)
            {
                main.UnregisterName(item);
            }
            mvm.DesignCardList.Clear();
        }

        private void TextChangedEditor(object sender, TextChangedEventArgs e)
        {
            VerifyOrderDetails();
        }

        private void RichEditorChanged(object sender, EventArgs e)
        {
            VerifyOrderDetails();
        }

        private void ContinueClicked(object sender, RoutedEventArgs e)
        {
            OrderSelectSP.Visibility = System.Windows.Visibility.Collapsed;
            OrderSetupMainSP.Margin = new Thickness(15, 0, 0, 0);
            OrderNewViewModel vm = (OrderNewViewModel)this.DataContext;
            Storyboard sb2 = this.FindResource("LeftCloseBigAnimation") as Storyboard;
            ChecklistPanel.BeginStoryboard(sb2);
            vm.DesignerOpen = true;
            vm.GetItemsForSelectedOrder();
            //-- Hide UltraPrint Option code disable 12262019
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                List<ultraprintorderdata> matchedTransferlist = new List<ultraprintorderdata>();
                List<ultraprintorderdata> finalTransferScreen = new List<ultraprintorderdata>();
                if (vm.NewHopperOrder.PrintProcess == 4)
                {
                    using (Entities ctx = new Entities())
                    {
                        hopperorder thisHopperOrd =
                        (from q in ctx.hopperorders
                         where q.CustFirst.ToLower() == vm.SelectedOrderNewVM.NewHopperOrder.CustFirst.ToLower() &&
                               q.CustLast.ToLower() == vm.SelectedOrderNewVM.NewHopperOrder.CustLast.ToLower() &&
                               q.CustCompany.ToLower() == vm.SelectedOrderNewVM.NewHopperOrder.CustCompany.ToLower() && q.PrintProcess == 4
                         select q).FirstOrDefault();

                        if (thisHopperOrd != null)
                        {

                            List<ultraprintorderdata> thisUpOrd =
                       (from q in ctx.ultraprintorderdatas
                        where q.CustFirstName.ToLower() == vm.SelectedOrderNewVM.NewHopperOrder.CustFirst.ToLower() &&
                                q.CustLastName.ToLower() == vm.SelectedOrderNewVM.NewHopperOrder.CustLast.ToLower() &&
                                q.CustCompany.ToLower() == vm.SelectedOrderNewVM.NewHopperOrder.CustCompany.ToLower()
                        select q).ToList();

                            if (thisUpOrd != null && thisUpOrd.Count > 0)
                            {
                                foreach (var item in thisUpOrd)
                                {
                                    ultraprintorderdata finaltest = new ultraprintorderdata();
                                    finaltest = (from q in ctx.ultraprintorderdatas
                                                 where
                                                     q.CustFirstName == vm.SelectedOrderNewVM.NewHopperOrder.CustFirst &&
                                                     q.CustLastName == vm.SelectedOrderNewVM.NewHopperOrder.CustLast &&
                                                     q.CustCompany == vm.SelectedOrderNewVM.NewHopperOrder.CustCompany &&
                                                     q.Id == item.TransferedFrom
                                                 select q
                                             ).FirstOrDefault();
                                    if (finaltest != null) { matchedTransferlist.Add(item); }
                                }
                                finalTransferScreen = thisUpOrd.Where(p => !matchedTransferlist.Any(p2 => p2.Id == p.Id)).ToList();
                                if (finalTransferScreen != null && finalTransferScreen.Count == 0) { finalTransferScreen = thisUpOrd; }
                            }


                            if (finalTransferScreen != null && finalTransferScreen.Count > 0)
                            {
                                var vm1 = (OrderNewViewModel)this.DataContext;
                                Utilities.ShowUltraPrintTransfer(vm1);
                            }
                            else
                            {
                                var vm1 = (OrderNewViewModel)this.DataContext;
                                Utilities.ShowUltraPrintSetup(vm1);
                            }
                        }
                        else
                        {
                            var vm1 = (OrderNewViewModel)this.DataContext;
                            Utilities.ShowUltraPrintSetup(vm1);
                        }
                    }
                }
            }

            //if (vm.NewHopperOrder.PrintProcess == 4) {
            //    var vm1 = (OrderNewViewModel)this.DataContext;
            //    Utilities.ShowUltraPrintSetup(vm1);
            //}

            //if ((vm.SelectedOrderNewVM.POSDecortingType.ToLower().Contains("ultraprint"))  || vm.SelectedOrderNewVM.POSDecortingType.ToLower().Contains("ultra print"))
            //{
            // var vm1 = (OrderNewViewModel)this.DataContext;
            // Utilities.ShowUltraPrintSetup(vm1);
            //}
        }

        //-- Hide UltraPrint Option code disable 12262019
        //    List<ultraprintorderdata> matchedTransferlist = new List<ultraprintorderdata>();
        //    List<ultraprintorderdata> finalTransferScreen = new List<ultraprintorderdata>();
        //    if (vm.NewHopperOrder.PrintProcess == 4)
        //    {
        //        using (Entities ctx = new Entities())
        //        {
        //            hopperorder thisHopperOrd =
        //            (from q in ctx.hopperorders
        //             where q.CustFirst.ToLower() == vm.SelectedOrderNewVM.NewHopperOrder.CustFirst.ToLower() &&
        //                   q.CustLast.ToLower() == vm.SelectedOrderNewVM.NewHopperOrder.CustLast.ToLower() &&
        //                   q.CustCompany.ToLower() == vm.SelectedOrderNewVM.NewHopperOrder.CustCompany.ToLower() && q.PrintProcess == 4
        //             select q).FirstOrDefault();

        //            if (thisHopperOrd != null)
        //            {

        //                List<ultraprintorderdata> thisUpOrd =
        //           (from q in ctx.ultraprintorderdatas
        //            where q.CustFirstName.ToLower() == vm.SelectedOrderNewVM.NewHopperOrder.CustFirst.ToLower() &&
        //                    q.CustLastName.ToLower() == vm.SelectedOrderNewVM.NewHopperOrder.CustLast.ToLower() &&
        //                    q.CustCompany.ToLower() == vm.SelectedOrderNewVM.NewHopperOrder.CustCompany.ToLower()
        //            select q).ToList();

        //                if (thisUpOrd != null && thisUpOrd.Count > 0)
        //                {
        //                    foreach (var item in thisUpOrd)
        //                    {
        //                        ultraprintorderdata finaltest = new ultraprintorderdata();
        //                        finaltest = (from q in ctx.ultraprintorderdatas
        //                                     where
        //                                         q.CustFirstName == vm.SelectedOrderNewVM.NewHopperOrder.CustFirst &&
        //                                         q.CustLastName == vm.SelectedOrderNewVM.NewHopperOrder.CustLast &&
        //                                         q.CustCompany == vm.SelectedOrderNewVM.NewHopperOrder.CustCompany &&
        //                                         q.Id == item.TransferedFrom
        //                                     select q
        //                                 ).FirstOrDefault();
        //                        if (finaltest != null) { matchedTransferlist.Add(item); }
        //                    }
        //                    finalTransferScreen = thisUpOrd.Where(p => !matchedTransferlist.Any(p2 => p2.Id == p.Id)).ToList();
        //                    if (finalTransferScreen != null && finalTransferScreen.Count == 0) { finalTransferScreen = thisUpOrd; }
        //                }


        //                if (finalTransferScreen != null && finalTransferScreen.Count > 0)
        //                {
        //                    var vm1 = (OrderNewViewModel)this.DataContext;
        //                    Utilities.ShowUltraPrintTransfer(vm1);
        //                }
        //                else
        //                {
        //                    var vm1 = (OrderNewViewModel)this.DataContext;
        //                    Utilities.ShowUltraPrintSetup(vm1);
        //                }
        //            }
        //            else
        //            {
        //                var vm1 = (OrderNewViewModel)this.DataContext;
        //                Utilities.ShowUltraPrintSetup(vm1);
        //            }
        //        }
        //    }

        //    //if (vm.NewHopperOrder.PrintProcess == 4) {
        //    //    var vm1 = (OrderNewViewModel)this.DataContext;
        //    //    Utilities.ShowUltraPrintSetup(vm1);
        //    //}

        //    //if ((vm.SelectedOrderNewVM.POSDecortingType.ToLower().Contains("ultraprint"))  || vm.SelectedOrderNewVM.POSDecortingType.ToLower().Contains("ultra print"))
        //    //{
        //    // var vm1 = (OrderNewViewModel)this.DataContext;
        //    // Utilities.ShowUltraPrintSetup(vm1);
        //    //}
        //}

        private void BigAnimationCompleted(object sender, EventArgs e)
        {
            ChecklistPanel.Visibility = System.Windows.Visibility.Collapsed;
            OrderNewViewModel vm = (OrderNewViewModel)this.DataContext;
            Storyboard sb = this.FindResource("LeftOpenAnimation") as Storyboard;
            DesignSetupMainSP.BeginStoryboard(sb);
            ChecklistPanel.Opacity = 0;
            vm.DesignerOpen = true;
        }


        void ComboBoxEdit_PopupOpening(object sender, DevExpress.Xpf.Editors.OpenPopupEventArgs e)
        {
            if (((System.Windows.FrameworkElement)sender).Name == "StatusEditor")
            {
                var list = ((ComboBoxEdit)sender).ItemsSource as ObservableCollection<setupstatu>;
                if (list != null)
                {
                    setupstatu obj3 = list.Where(x => x.Status == "Awaiting Artwork").Select(x => x).FirstOrDefault();
                    setupstatu obj4 = list.Where(x => x.Status == "Awaiting Art Approval").Select(x => x).FirstOrDefault();
                    setupstatu obj5 = list.Where(x => x.Status == "Awaiting Inventory").Select(x => x).FirstOrDefault();
                    setupstatu obj6 = list.Where(x => x.Status == "Working On Artwork").Select(x => x).FirstOrDefault();

                    if (obj3 != null && obj4 != null && obj5 != null && obj6 != null)
                    {
                        list.Remove(obj3); list.Remove(obj4); list.Remove(obj5); list.Remove(obj6);
                        ((ComboBoxEdit)sender).ItemsSource = list.OrderBy(x => x.Status).ToList();
                        OrderNewViewModel thisVM = (OrderNewViewModel)this.DataContext;
                    }
                }
            }
        }

        private void NewOrderLoaded(object sender, RoutedEventArgs e)
        {
            bLoader.RunWorkerAsync();

            using (var ctx = new Entities())
            {
                ctx.printprocesses.Load();
                PrintProcessEditor.ItemsSource = ctx.printprocesses.Local;
                var itemListDesignerEditor = PrintProcessEditor.ItemsSource as ObservableCollection<printprocess>;
                PrintProcessEditor.ItemsSource = null;
                PrintProcessEditor.ItemsSource = itemListDesignerEditor.OrderBy(x => x.ProcessOrderId).ToList();
            }
        }

        private void SelectedOrderIndexChanged(object sender, RoutedEventArgs e)
        {
            OrderNewViewModel vm = (OrderNewViewModel)this.DataContext;

            if (!vm.DesignerOpen)
            {
                if (!vm.SetupOpen)
                {
                    Storyboard sb = this.FindResource("LeftOpenAnimation") as Storyboard;
                    OrderSetupMainSP.BeginStoryboard(sb);
                    Storyboard sbx = this.FindResource("LeftOpenDelayedAnimation") as Storyboard;
                    ChecklistPanel.BeginStoryboard(sbx);
                    vm.SetupOpen = true;
                }
            }
            else
            {
                ChecklistPanel.Visibility = System.Windows.Visibility.Collapsed;
                Storyboard sb2 = this.FindResource("LeftCloseResetAnimation") as Storyboard;
                DesignSetupMainSP.BeginStoryboard(sb2);
                expanderContainer.Children.Clear();
                vm.DesignerOpen = false;
            }

            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            mvm.IsNavigationLocked = true;
        }

        private void ItemValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (((System.Windows.FrameworkElement)sender).Name == "StatusEditor")
            {
                var list = ((ComboBoxEdit)sender).ItemsSource as ObservableCollection<setupstatu>;
                if (list != null)
                {
                    setupstatu obj3 = list.Where(x => x.Status == "Awaiting Artwork").Select(x => x).FirstOrDefault();
                    setupstatu obj4 = list.Where(x => x.Status == "Awaiting Art Approval").Select(x => x).FirstOrDefault();
                    setupstatu obj5 = list.Where(x => x.Status == "Awaiting Inventory").Select(x => x).FirstOrDefault();
                    setupstatu obj6 = list.Where(x => x.Status == "Working On Artwork").Select(x => x).FirstOrDefault();

                    if (obj3 != null && obj4 != null && obj5 != null && obj6 != null)
                    {
                        list.Remove(obj3); list.Remove(obj4); list.Remove(obj5); list.Remove(obj6);
                        ((ComboBoxEdit)sender).ItemsSource = list.OrderBy(x => x.Status).ToList();
                        OrderNewViewModel thisVM = (OrderNewViewModel)this.DataContext;
                    }
                }
            }
            VerifyOrderDetails();
        }

        private void IndexChanged(object sender, RoutedEventArgs e)
        {
            VerifyOrderDetails();
        }

        private void AnimCompleted(object sender, EventArgs e)
        {

        }

        private void LeftCloseReset(object sender, EventArgs e)
        {
            ChecklistPanel.Visibility = System.Windows.Visibility.Visible;
            Storyboard sb = this.FindResource("LeftOpenDelayedAnimation") as Storyboard;
            ChecklistPanel.BeginStoryboard(sb);
        }

        private void DragFeedback(object sender, GiveFeedbackEventArgs e)
        {
            if (e.Effects == DragDropEffects.Copy)
            {
                e.UseDefaultCursors = false;
                Mouse.SetCursor(Cursors.Hand);
            }
            else e.UseDefaultCursors = true;
            e.Handled = true;
        }

        private void DeleteEnter(object sender, MouseEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            rect.Fill = Brushes.Red;
            rect.Opacity = 0.9;
        }

        private void DeleteLeave(object sender, MouseEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            rect.Fill = Brushes.Black;
            rect.Opacity = 0.1;
        }

        private async void DeleteClick(object sender, MouseButtonEventArgs e)
        {
            MessageDialogResult res = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you would like to delete this inventory item?");
            if (res == MessageDialogResult.Affirmative)
            {
                ListViewItem elem = Utilities.FindParentByType<ListViewItem>((DependencyObject)sender);
                LineViewModel vm = (LineViewModel)elem.Content;
                ListView view = Utilities.FindParentByType<ListView>((DependencyObject)elem);
                IList<LineViewModel> list = (IList<LineViewModel>)view.ItemsSource;
                list.Remove(vm);
            }
        }

        private void CollapseAllClicked(object sender, EventArgs e)
        {
            foreach (Expander exx in expanderContainer.Children)
            {
                exx.IsExpanded = false;
            }
        }

        private void ExpandAllClicked(object sender, EventArgs e)
        {
            foreach (Expander exx in expanderContainer.Children)
            {
                exx.IsExpanded = true;
            }
        }

        private async void PreflightSubmit()
        {
            OrderNewViewModel onvm = (OrderNewViewModel)this.DataContext;
            DateTime today = DateTime.Now;
            DateTime dtval = DateEditor.DateTime;
            bool isToday = false;
            if (dtval.DayOfYear == today.DayOfYear) isToday = true;
            bool canceled = false;

            if (isToday)
            {
                MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Due Date", "This order's due date is set to today. Are you sure this is the correct due date you want to submit?");
                if (result == MessageDialogResult.Affirmative) { canceled = false; }
                else { canceled = true; }
            }
            if (onvm.ToOrderFromAlpha.Count == 0)
            {
                MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm No Inventory", "Are you sure you want to continue without ordering any inventory from Alpha?");
                if (result == MessageDialogResult.Affirmative) { canceled = false; }
                else { canceled = true; }
            }
            if (expanderContainer.Children.Count == 0)
            {
                MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm No Artwork", "Are you sure you want to continue without adding any design files?");
                if (result == MessageDialogResult.Affirmative) { canceled = false; }
                else { canceled = true; }
            }
            int DesignNum = 0;
            try
            {
                foreach (Expander card in expanderContainer.Children)
                {
                    Controls.DesignCards.SetupCard sc = (Controls.DesignCards.SetupCard)card.Content;
                    var cardVM = (DesignSetupCardViewModel)sc.DataContext;
                    DesignNum = cardVM.SelectedDesignDetail.DesignNum;
                    string newPath = System.IO.Path.Combine("", cardVM.SelectedDesignDetail.DesignDescription).ToString();

                    if (cardVM.SelectedDesignDetail.DesignDescription.Length > 245)
                    {
                        Utilities.ShowSimpleDialog("Design Description Invalid", "The specified path, file name, or both are too long. The fully qualified file name must be less than 260 characters, \n and the directory name must be less than 248 characters.\n Design Description is used for creating design folder. Please Check DESIGN - " + cardVM.SelectedDesignDetail.DesignNum + " ...!!!");

                        canceled = true;
                    }
                    else
                    {
                        canceled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.ShowSimpleDialog("Design Description Invalid", "Illegal characters in Design Description which used for creating design folder. Please Check DESIGN - " + DesignNum + " ...!!!");
                canceled = true;

            }

            if (!canceled)
            {
                bWorker.RunWorkerAsync();
            }
        }


        void bWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                var vm = (OrderNewViewModel)this.DataContext;
                progressBar.Value = 100;
                StatusLabel.Content = "Completed successfully.";
                if (ListBoxx.Items.Count > 0)
                {
                    ListBoxx.SelectedIndex = 0;
                }
                RichEditor.Text = "";
                vm.OrderInputComplete = true;

                MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
                mvm.IsNavigationLocked = false;

                var onvm = (OrderNewViewModel)this.DataContext;
                var CompletedOrder = onvm.NewHopperOrder;

                DataSet setL = GetProductionDataSet();
                var bin = setL.Tables[0].Rows[0]["BoxNum"].ToString();
                InvoicePlusTest invoice;
                if (bin == "NBY" || bin == "") { invoice = new InvoicePlusTest(true); }
                else { invoice = new InvoicePlusTest(false); }
                invoice.DataSource = setL;
                string PDFPath = CompletedOrder.JobFolderPath + "\\ProductionSheet.pdf";
                string jpgPath = CompletedOrder.JobFolderPath + "\\ProductionSheet.jpg";

                try
                {
                    invoice.ExportToPdf(PDFPath);
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }

                if (Properties.Settings.Default.Auto_PrintInvoicesAfterOrder)
                {
                    invoice.PrintingSystem.ShowPrintStatusDialog = false;
                    invoice.PrintingSystem.ShowMarginsWarning = false;
                    try
                    {
                        PrintHelper.PrintDirect(invoice);
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }
            }
        }

        void bWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
                {
                    progressBar.Value = e.ProgressPercentage;
                    StatusLabel.Content = e.UserState.ToString();
                }));
        }


        private void CreateOrderNotesFile()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var vm = (OrderNewViewModel)this.DataContext;
                string notesPath = vm.NewHopperOrder.JobFolderPath.ToString() + @"\OrderNotes.Rtf";
                RichEditor.SaveDocument(notesPath, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
            }));
        }

        private void CreateBroSharedFolder()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var vm = (OrderNewViewModel)this.DataContext;
                string formattedDate = DateTime.Now.ToString("MM-dd-yyyy");
                string firstName = vm.SelectedOrderNewVM.NewHopperOrder.CustFirst.ToString();
                string lastName = vm.SelectedOrderNewVM.NewHopperOrder.CustLast.ToString();
                string company = vm.SelectedOrderNewVM.NewHopperOrder.CustCompany.ToString();
                string broshrd = Properties.Settings.Default.Data_ProductionFolderPath.ToString();

                string jobFolder = "";
                string fullpath = "";

                if (company == "")
                {
                    jobFolder = lastName + ", " + firstName + " - " + vm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum + " - " + formattedDate + "";
                }
                else
                {
                    jobFolder = company + " - " + lastName + ", " + firstName + " - " + vm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum + " - " + formattedDate + "";
                }

                jobFolder = System.IO.Path.GetInvalidFileNameChars().Aggregate(jobFolder, (current, c) => current.Replace(c.ToString(), string.Empty));
                fullpath = broshrd + "\\" + jobFolder;

                try
                {

                    if (!Directory.Exists(fullpath))
                    {
                        // cleaned = Utilities.GetCleanedFilename(jobFolder);
                        Directory.CreateDirectory(fullpath);
                    }
                    vm.NewHopperOrder.JobFolderPath = fullpath;
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }));
        }

        private DataTable GetReceiptData(int input, bool isCurrentData)
        {
            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();

            string dbConnect;
            if (isCurrentData)
            {
                dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\currentdata.mdb";
            }
            else
            {
                dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\ParcomW.mdb";
            }

            string SelectOrderData = @" SELECT TOP 1 Rcpt_Number, Rcpt_Node, Rcpt_CustNumber, Rcpt_CCardNumber, Rcpt_DiscPercentage, Rcpt_CashTendered, Rcpt_CheckTendered,
                                        Rcpt_CCardTendered, Rcpt_ChargeTendered, Rcpt_TotalTendered, Rcpt_TotalTax, Rcpt_STotal, Rcpt_Total, Rcpt_DateTimeStamp
                                        FROM Receipts 
                                        WHERE Rcpt_CustNumber = " + input +
                          " ORDER BY Rcpt_Number DESC";

            OleDbConnection tjConn = null;

            try
            {
                tjConn = new OleDbConnection(dbConnect);

                OleDbCommand orderComm = new OleDbCommand(SelectOrderData, tjConn);
                OleDbDataAdapter myDA1 = new OleDbDataAdapter(orderComm);

                DataTable output = new DataTable();
                tjConn.Open();
                myDA1.Fill(output);
                tjConn.Close();

                return output;
            }
            catch (Exception ex)
            {
                new Error(ex);
                return null;
            }
        }
        private void MoveFiles()
        {
            try
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    var vm = (OrderNewViewModel)this.DataContext;

                    foreach (Expander card in expanderContainer.Children)
                    {
                        try
                        {
                            Controls.DesignCards.SetupCard sc = (Controls.DesignCards.SetupCard)card.Content;
                            var cardVM = (DesignSetupCardViewModel)sc.DataContext;
                            string newFolder;
                            if (cardVM.SelectedDesignDetail.DesignDescription == "") newFolder = "Design " + cardVM.DesignNum;
                            else if (cardVM.SelectedDesignDetail.DesignDescription != "") newFolder = "Design " + cardVM.DesignNum + " - " + cardVM.SelectedDesignDetail.DesignDescription;
                            else newFolder = "Design " + cardVM.DesignNum;
                            string newPath = System.IO.Path.Combine(vm.NewHopperOrder.JobFolderPath, newFolder).ToString();

                            //if(cardVM.SelectedDesignDetail.DesignDescription.Length > 240) { return; }

                            if (!Directory.Exists(newPath))
                            {
                                // cleaned = Utilities.GetCleanedFilename(newPath);
                                try
                                {
                                    Directory.CreateDirectory(newPath);
                                }
                                catch (Exception ex)
                                {
                                    new Error(ex);
                                }
                            }
                            cardVM.SelectedDesignDetail.DesignFolderPath = newPath;

                            if (cardVM.CurrentArtworkItems.Any())
                            {
                                try
                                {
                                    foreach (string item in cardVM.CurrentArtworkItems)
                                    {
                                        string type = "";
                                        string destination = cardVM.SelectedDesignDetail.DesignFolderPath + @"\CDR\";
                                        bool isYouth = cardVM.SelectedDesignDetail.AP_YOUTH;

                                        if (item == "Apron" || item == "Bandana" || item == "CapFront" || item == "RoundCoasters" || item == "GolfFlag"
                                                || item == "GolfTowel" || item == "Koozies" || item == "Mousepad" || item == "Puzzle" || item == "SquareCoasters" || item == "ToteBag" || item == "WineBag")
                                        {
                                            type = "S";
                                            cardVM.CopyArtFile(item, type, destination);
                                        }
                                        else
                                        {
                                            if (isYouth)
                                            {
                                                type = "Y";
                                                cardVM.CopyArtFile("Youth" + item, type, destination);
                                            }
                                            else if (!isYouth)
                                            {
                                                type = "A";
                                                cardVM.CopyArtFile(item, type, destination);
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    new Error(ex);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                            return;
                        }

                    }
                }));

            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }
        private void CreateNewReceipt()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var vm = (OrderNewViewModel)this.DataContext;
                DataTable order1Data = vm.ReceiptData;

                var ordd = order1Data.Rows[0]["Rcpt_DateTimeStamp"];
                var today = Convert.ToDateTime(ordd);
                var format = today.ToString("MM-dd-yyyy");
                var node = order1Data.Rows[0]["Rcpt_Node"].ToString();
                var ccard = order1Data.Rows[0]["Rcpt_CCardNumber"].ToString();
                var disc = order1Data.Rows[0]["Rcpt_DiscPercentage"].ToString();
                var cashten = order1Data.Rows[0]["Rcpt_CashTendered"].ToString();
                var checkten = order1Data.Rows[0]["Rcpt_CheckTendered"].ToString();
                var ccardten = order1Data.Rows[0]["Rcpt_CCardTendered"].ToString();
                var chargeten = order1Data.Rows[0]["Rcpt_ChargeTendered"].ToString();
                var totalten = order1Data.Rows[0]["Rcpt_TotalTendered"].ToString();
                var totaltax = order1Data.Rows[0]["Rcpt_TotalTax"].ToString();
                var subtotal = order1Data.Rows[0]["Rcpt_STotal"].ToString();
                var total = order1Data.Rows[0]["Rcpt_Total"].ToString();
                var rcptdt = order1Data.Rows[0]["Rcpt_DateTimeStamp"];

                if (string.IsNullOrEmpty(ccard)) ccard = "";

                using (Entities ctx = new Entities())
                {
                    if (ctx.receipts.Any(x => x.RcptNumber == vm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum))
                    {
                        receipt toR = (from t in ctx.receipts where t.RcptNumber == vm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum select t).FirstOrDefault();
                        ctx.receipts.Remove(toR);
                        ctx.SaveChanges();
                    }

                    try
                    {
                        receipt rcpt = new receipt();

                        rcpt.RcptNumber = vm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum;
                        rcpt.RcptNode = Convert.ToInt32(node);
                        rcpt.RcptCustNumber = vm.SelectedOrderNewVM.NewHopperOrder.CustomerID;
                        rcpt.RcptCCardNumber = ccard;
                        rcpt.RcptDiscPercent = Convert.ToInt32(Math.Round(Convert.ToDouble(disc)));
                        rcpt.RcptCashTendered = Convert.ToDecimal(cashten);
                        rcpt.RcptCheckTendered = Convert.ToDecimal(checkten);
                        rcpt.RcptCCardTendered = Convert.ToDecimal(ccardten);
                        rcpt.RcptChargeTendered = Convert.ToDecimal(chargeten);
                        rcpt.RcptTotalTendered = Convert.ToDecimal(totalten);
                        rcpt.RcptTotalTax = Convert.ToDecimal(totaltax);
                        rcpt.RcptSTotal = Convert.ToDecimal(subtotal);
                        rcpt.RcptTotal = Convert.ToDecimal(total);
                        rcpt.RcptDateTime = Convert.ToDateTime(rcptdt);
                        rcpt.PrintProcess = Convert.ToInt32(vm.NewHopperOrder.PrintProcess);
                        ctx.receipts.Add(rcpt);
                        ctx.SaveChanges();
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                    {
                        new Error(ex);
                    }
                }
            }));
        }

        private void CreateNewHopperOrder()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var vm = (OrderNewViewModel)this.DataContext;

                Window parent = Window.GetWindow(this);
                MainViewModel mvm = (MainViewModel)parent.DataContext;

                using (Entities ctx = new Entities())
                {
                    hopperorder newOrder = new hopperorder();

                    if (vm.SelectedOrderNewVM != null)
                    {
                        if (vm.SelectedOrderNewVM.POSDecortingType == null)
                            vm.SelectedOrderNewVM.POSDecortingType = "";
                    }
                    if ((vm.SelectedOrderNewVM.POSDecortingType.ToLower().Contains("ultraprint")) || vm.SelectedOrderNewVM.POSDecortingType.ToLower().Contains("ultra print"))
                    {
                        newOrder.POSDecortionType = "UltraPrint";
                    }
                    else
                    {
                        newOrder.POSDecortionType = "";
                    }

                    try
                    {
                        newOrder.BoxNum = vm.NewHopperOrder.BoxNum;
                        newOrder.CareTags = 1;
                        newOrder.Correspondence = vm.NewHopperOrder.Correspondence;
                        newOrder.CustomerID = vm.SelectedOrderNewVM.NewHopperOrder.CustomerID;
                        newOrder.CustFirst = vm.SelectedOrderNewVM.NewHopperOrder.CustFirst;
                        newOrder.CustLast = vm.SelectedOrderNewVM.NewHopperOrder.CustLast;
                        newOrder.CustCompany = vm.SelectedOrderNewVM.NewHopperOrder.CustCompany;
                        newOrder.DesignFrog = mvm.LoggedInFrogger.FrogID;
                        newOrder.DueDate = vm.NewHopperOrder.DueDate;
                        newOrder.Irregulars = 0;
                        newOrder.JobFolderPath = vm.NewHopperOrder.JobFolderPath;
                        newOrder.Notes = "";
                        newOrder.ReceiptNum = vm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum;
                        newOrder.SearchTags = GetTagsAsString();
                        newOrder.Priority = vm.NewHopperOrder.Priority;
                        newOrder.OrderDate = GetOrderDate();
                        newOrder.OrderFrog = mvm.LoggedInFrogger.FrogID;
                        newOrder.PrintProcess = vm.NewHopperOrder.PrintProcess;
                        newOrder.Status = vm.NewHopperOrder.Status;
                        newOrder.ArtworkStatus = vm.NewHopperOrder.ArtworkStatus;
                        newOrder.InventoryStatus = vm.NewHopperOrder.InventoryStatus;
                        string s = vm.SelectedOrderNewVM.POSDecortingType;

                        newOrder.TotalCost = GetOrderTotal();
                        newOrder.TotalPrints = GetTotalPrints();
                        newOrder.TotalShirts = GetTotalShirts();

                        if (vm.NewHopperOrder.IsNoRush)
                        {
                            newOrder.IsNoRush = true;
                            newOrder.IsDropDead = false;
                        }
                        else
                        {
                            newOrder.IsNoRush = false;
                            newOrder.IsDropDead = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                    try
                    {
                        ctx.hopperorders.Add(newOrder);
                        ctx.SaveChanges();

                        ultraprintorderdata ultradata = new ultraprintorderdata();

                        if ((vm.SelectedultraData != null) && (vm.SelectedultraData.Count > 0))
                        {
                            foreach (var item in vm.SelectedultraData)
                            {
                                ultradata.cusomerId = newOrder.CustomerID;
                                ultradata.OrderID = newOrder.OrderID;
                                ultradata.Location = item.Location;
                                ultradata.DesignName = item.DesignName;
                                ultradata.Type = item.Type;
                                ultradata.Qty = item.Qty;
                                ultradata.receiptnum = newOrder.ReceiptNum;
                                ultradata.size = item.size;
                                ultradata.createddate = DateTime.Now;
                                ultradata.IsEditable = true;
                                ultradata.CustLastName = newOrder.CustLast;
                                ultradata.CustCompany = newOrder.CustCompany;
                                ultradata.Vendor = item.Vendor;
                                ultradata.Used = item.Used;
                                ultradata.Bin = item.Bin;
                                ultradata.CustFirstName = newOrder.CustFirst;
                                ultradata.TransferedFrom = item.TransferedFrom;

                                ultradata.expiredate = DateTime.Now.AddMonths
                                (Properties.Settings.Default.UltraPrint_ExpirationDate);
                                ctx.ultraprintorderdatas.Add(ultradata);
                                ctx.SaveChanges();
                            }

                        }
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                    {
                        new Error(ex);
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }
            }));
        }

        private int GetTotalPrints()
        {
            var vm = (OrderNewViewModel)this.DataContext;
            int total = 0;

            foreach (Expander card in expanderContainer.Children)
            {
                Controls.DesignCards.SetupCard sc = (Controls.DesignCards.SetupCard)card.Content;
                DesignSetupCardViewModel dvm = (DesignSetupCardViewModel)sc.DataContext;
                total += dvm.PrintCount;
            }
            return total;
        }

        private int GetTotalShirts()
        {
            var vm = (OrderNewViewModel)this.DataContext;
            int total = 0;
            foreach (Expander card in expanderContainer.Children)
            {
                Controls.DesignCards.SetupCard sc = (Controls.DesignCards.SetupCard)card.Content;
                DesignSetupCardViewModel dvm = (DesignSetupCardViewModel)sc.DataContext;
                total += dvm.ShirtCount;
            }
            return total;
        }

        private DateTime GetOrderDate()
        {
            var vm = (OrderNewViewModel)this.DataContext;
            using (Entities ctx = new Entities())
            {
                DateTime q = (from c in ctx.receipts where c.RcptNumber == vm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum select c.RcptDateTime).FirstOrDefault();
                return q;
            }
        }

        private decimal GetOrderTotal()
        {
            var vm = (OrderNewViewModel)this.DataContext;
            using (Entities ctx = new Entities())
            {
                decimal q = (from c in ctx.receipts where c.RcptNumber == vm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum select c.RcptTotal).FirstOrDefault();
                return q;
            }
        }

        public string GetTagsAsString()
        {
            string output = "";
            var vm = (OrderNewViewModel)this.DataContext;

            foreach (Tag tag in vm.TagCollection)
            {
                string tagtext = tag.PublicTag.TagText;
                output += tagtext + ",";
            }

            return output;
        }


        private void CreateOrderDetails()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var vm = (OrderNewViewModel)this.DataContext;
                var lineitems = vm.InputMasterLineItems;
                try
                {
                    foreach (LineViewModel line in lineitems)
                    {
                        using (Entities ctx = new Entities())
                        {
                            orderdetail newDT = new orderdetail()
                            {
                                ReceiptNum = vm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum,
                                StockIDPOS = line.SelectedLineItem.StockIDPOS,
                                Quantity = line.SelectedLineItem.Quantity,
                                Size = line.SelectedLineItem.Size,
                                Color = line.SelectedLineItem.Color,
                                Description = line.SelectedLineItem.Quantity + " " + line.SelectedLineItem.StockIDPOS + " - " + line.SelectedLineItem.Color + " - " + line.SelectedLineItem.Size + "",
                                LineCost = line.LineCost,
                                LinePrice = line.LinePrice,
                                LineExtend = line.LineExtend,
                                AlphaOrderID = 1
                            };
                            ctx.orderdetails.Add(newDT);
                            ctx.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }));
        }

        private void CreateDesignDetails()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var vm = (OrderNewViewModel)this.DataContext;
                foreach (Expander card in expanderContainer.Children)
                {
                    Controls.DesignCards.SetupCard sc = (Controls.DesignCards.SetupCard)card.Content;
                    var dsVM = (DesignSetupCardViewModel)sc.DataContext;
                    using (Entities ctx = new Entities())
                    {
                        designdetail newDD = new designdetail()
                        {
                            ReceiptNum = vm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum,
                            DesignNum = dsVM.DesignNum,
                            DesignFolderPath = dsVM.SelectedDesignDetail.DesignFolderPath,
                            DesignDescription = dsVM.SelectedDesignDetail.DesignDescription,
                            Notes = "",
                            PrintToFile = dsVM.SelectedDesignDetail.PrintToFile,
                            SizeToGarment = dsVM.SelectedDesignDetail.SizeToGarment,
                            AP_YOUTH = dsVM.SelectedDesignDetail.AP_YOUTH,
                            AP_Front = dsVM.SelectedDesignDetail.AP_Front,
                            AP_Back = dsVM.SelectedDesignDetail.AP_Back,
                            AP_LeftSide = dsVM.SelectedDesignDetail.AP_LeftSide,
                            AP_RightSide = dsVM.SelectedDesignDetail.AP_RightSide,
                            AP_LeftSleeve = dsVM.SelectedDesignDetail.AP_LeftSleeve,
                            AP_RightSleeve = dsVM.SelectedDesignDetail.AP_RightSleeve,
                            AP_FrontBottom = false,
                            AP_BackBottom = false,
                            AP_Yoke = false,
                            AP_Collar = false,
                            AP_FLC = dsVM.SelectedDesignDetail.AP_FLC,
                            AP_FRC = dsVM.SelectedDesignDetail.AP_FRC,
                            AP_FLCW = false,
                            AP_FRCW = false,
                            AP_Mousepad = dsVM.SelectedDesignDetail.AP_Mousepad,
                            AP_SqCoaster = dsVM.SelectedDesignDetail.AP_SqCoaster,
                            AP_ClCoaster = dsVM.SelectedDesignDetail.AP_ClCoaster,
                            AP_Bandana = dsVM.SelectedDesignDetail.AP_Bandana,
                            AP_GolfFlag = dsVM.SelectedDesignDetail.AP_GolfFlag,
                            AP_GolfTowel = dsVM.SelectedDesignDetail.AP_GolfTowel,
                            AP_Koozie = dsVM.SelectedDesignDetail.AP_Koozie,
                            AP_Cap = dsVM.SelectedDesignDetail.AP_Cap,
                            AP_Apron = dsVM.SelectedDesignDetail.AP_Apron,
                            AP_Puzzle = dsVM.SelectedDesignDetail.AP_Puzzle,
                            AP_ToteBag = dsVM.SelectedDesignDetail.AP_ToteBag,
                            AP_WineBag = dsVM.SelectedDesignDetail.AP_WineBag,
                            PrintProcess = dsVM.SelectedDesignDetail.PrintProcess
                        };

                        ctx.designdetails.Add(newDD);
                        try
                        {
                            ctx.SaveChanges();
                        }
                        catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                        {
                            var asd = ex.EntityValidationErrors;

                            foreach (System.Data.Entity.Validation.DbEntityValidationResult result in asd)
                            {
                                foreach (System.Data.Entity.Validation.DbValidationError error in result.ValidationErrors)
                                {
                                    MessageBox.Show(error.PropertyName + Environment.NewLine + error.ErrorMessage);
                                }
                            }

                        }

                        var thisID = ctx.designdetails.OrderByDescending(d => d.DesignID).FirstOrDefault().DesignID;
                        try
                        {
                            ListView parentControl = sc.LineContainerActual;
                            var lineVMs = ((IList<LineViewModel>)sc.LineContainerActual.ItemsSource);

                            foreach (LineViewModel line in parentControl.Items)
                            {
                                designdetailslineitem newline = new designdetailslineitem()
                                {
                                    ReceiptNum = vm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum,
                                    DesignID = thisID,
                                    StockIDPOS = line.SelectedLineItem.StockIDPOS,
                                    Quantity = line.SelectedLineItem.Quantity,
                                    Size = line.SelectedLineItem.Size,
                                    Color = line.SelectedLineItem.Color,
                                    Description = line.SelectedLineItem.Quantity + " " + line.SelectedLineItem.StockIDPOS + " - " + line.SelectedLineItem.Color + " - " + line.SelectedLineItem.Size + ""
                                };

                                ctx.designdetailslineitems.Add(newline);
                            }

                            ctx.SaveChanges();
                        }
                        catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                        {
                            new Error(ex);
                            break;
                        }
                    }
                }
            }));
        }

        private void AddToAlphaOrder()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var ONvm = (OrderNewViewModel)this.DataContext;
                foreach (Expander card in expanderContainer.Children)
                {
                    Controls.DesignCards.SetupCard sc = (Controls.DesignCards.SetupCard)card.Content;
                    var cardVM = (DesignSetupCardViewModel)sc.DataContext;
                    if (ONvm.ToOrderFromAlpha.Any())
                    {
                        try
                        {
                            using (Entities ctx = new Entities())
                            {
                                int custToSearch = cardVM.SelectedDesignDetail.ReceiptNum;
                                string decortionname = string.Empty;
                                var printprocess = ctx.printprocesses.Where(x => x.ProcessID == ONvm.NewHopperOrder.PrintProcess).ToList();

                                if (printprocess.Count > 0)
                                {
                                    decortionname = ((Lilypad.Model.printprocess)printprocess[0]).ProcessName;
                                }

                                foreach (LineViewModel vm in ONvm.ToOrderFromAlpha)
                                {
                                    alphacurrentorder newC = new alphacurrentorder();
                                    newC.ReceiptNum = ONvm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum;
                                    newC.IDStockAlpha = vm.SelectedLineItem.StockIDPOS.LimitTo(48);
                                    newC.IDItemCodeAlpha = "";
                                    newC.CustFirstName = ONvm.SelectedOrderNewVM.NewHopperOrder.CustFirst;
                                    newC.CustLastName = ONvm.SelectedOrderNewVM.NewHopperOrder.CustLast;
                                    newC.BoxNum = ONvm.NewHopperOrder.BoxNum;
                                    newC.Quantity = vm.SelectedLineItem.Quantity;
                                    newC.ItemDescription = vm.SelectedLineItem.Description;
                                    newC.Color = vm.SelectedLineItem.Color;
                                    newC.Size = vm.SelectedLineItem.Size;
                                    newC.Status = "Waiting To Order";
                                    newC.LineCost = vm.LineCost;
                                    newC.AddedDate = DateTime.Now;
                                    newC.IsIDVerified = false;
                                    newC.IsInStockClosest = false;
                                    newC.IsInStockElsewhere = false;
                                    newC.LineCost = vm.LineCost * vm.SelectedLineItem.Quantity;
                                    newC.AddedDate = DateTime.Now;
                                    newC.UnitPrice = vm.LineCost;
                                    newC.IsArchiveable = false;
                                    newC.DecorationType = decortionname;
                                    ctx.alphacurrentorders.Add(newC);
                                }

                                ctx.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }
                    //return;
                }
            }));
        }

        private void WriteToLog()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                Window parent = Window.GetWindow(this);
                MainViewModel mvm = (MainViewModel)parent.DataContext;
                var vm = (OrderNewViewModel)this.DataContext;
                using (Entities ctx = new Entities())
                {
                    try
                    {
                        log newlog = new log()
                        {
                            Action = mvm.LoggedInFrogger.FrogFirstName + " created this order",
                            LogDateTime = DateTime.Now,
                            ReceiptNum = vm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum
                        };

                        ctx.logs.Add(newlog);
                        ctx.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }
            }));
        }

        void bWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool cont = true;
            // Customer operations
            if (bWorker != null)
            {
                bWorker.ReportProgress(5, "Setting up customer data");
                this.Dispatcher.Invoke((Action)(() =>
                {
                    var vm = (OrderNewViewModel)this.DataContext;
                    int input = 0;
                    input = vm.SelectedOrderNewVM.NewHopperOrder.CustomerID;
                    bool exists;
                    try
                    {
                        using (Entities ctx = new Entities())
                        {
                            exists = (from q in ctx.customers where q.CustomerID == input select q).Any();
                        }

                        if (exists)
                        {
                            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();
                            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\ParcomW.mdb";

                            DataSet custData = new DataSet();

                            string Select2 = @"SELECT TOP 1 FirstName, LastName, Company, FirstDate, LastDate, Taxable, ResaleNo, RecognizedBy FROM CustsMain WHERE CustId = " + input + "";
                            string Select3 = @"SELECT TOP 1 TotalSales, NumTrans FROM CustsMarket WHERE ACustId = " + input + "";
                            string Select4 = @"SELECT TOP 1 StreetAddress, Address2, City, State, Zip, Phone, email, Pager FROM CustAddresses WHERE Customernumber = " + input + "";

                            OleDbConnection myDbConn = null;
                            try
                            {
                                myDbConn = new OleDbConnection(dbConnect);

                                OleDbCommand myComm = new OleDbCommand(Select2, myDbConn);
                                OleDbDataAdapter myDA = new OleDbDataAdapter(myComm);

                                myDbConn.Open();
                                myDA.Fill(custData, "custMainData");

                                OleDbCommand myComm2 = new OleDbCommand(Select3, myDbConn);
                                OleDbDataAdapter myDA2 = new OleDbDataAdapter(myComm2);

                                myDA2.Fill(custData, "custSalesData");

                                OleDbCommand myComm3 = new OleDbCommand(Select4, myDbConn);
                                OleDbDataAdapter myDA3 = new OleDbDataAdapter(myComm3);

                                myDA3.Fill(custData, "custAddrData");

                                myDbConn.Close();
                                var custMainData = custData.Tables["custMainData"];
                                var custAddrData = custData.Tables["custAddrData"];
                                var custSalesData = custData.Tables["custSalesData"];
                                if (custMainData != null && custAddrData != null && custSalesData != null)
                                {
                                    try
                                    {
                                        using (Entities context = new Entities())
                                        {
                                            var cust = (from c in context.customers
                                                        where c.CustomerID == input
                                                        select c).FirstOrDefault();

                                            cust.FirstName = custMainData.Rows[0]["FirstName"].ToString();
                                            cust.LastName = custMainData.Rows[0]["Lastname"].ToString();
                                            cust.Company = custMainData.Rows[0]["Company"].ToString();
                                            cust.StreetAddress = custAddrData.Rows[0]["StreetAddress"].ToString();
                                            cust.StreetAddress2 = custAddrData.Rows[0]["Address2"].ToString();
                                            cust.City = custAddrData.Rows[0]["City"].ToString();
                                            cust.State = custAddrData.Rows[0]["State"].ToString();
                                            string zipin = custAddrData.Rows[0]["Zip"].ToString();
                                            if (String.IsNullOrEmpty(zipin)) { cust.Zip = ""; } else { cust.Zip = zipin.LimitTo(7); }
                                            cust.Phone = custAddrData.Rows[0]["Phone"].ToString();
                                            cust.Phone2 = custAddrData.Rows[0]["Pager"].ToString();
                                            cust.Email = custAddrData.Rows[0]["Email"].ToString();
                                            var lastdate = custMainData.Rows[0]["LastDate"];
                                            cust.LastDate = Convert.ToDateTime(lastdate);
                                            var taxable = custMainData.Rows[0]["Taxable"];
                                            var taxableB = Convert.ToBoolean(taxable);
                                            var taxexmpt = !taxableB;
                                            cust.TaxExempt = taxexmpt;
                                            cust.ResellerID = custMainData.Rows[0]["ResaleNo"].ToString().LimitTo(30);
                                            var totalsales = custSalesData.Rows[0]["TotalSales"].ToString();
                                            cust.TotalSales = Convert.ToDecimal(totalsales);
                                            var totalorders = custSalesData.Rows[0]["NumTrans"].ToString();
                                            cust.TotalOrders = Convert.ToInt32(totalorders);
                                            context.SaveChanges();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        new Error(ex);
                                        cont = false;
                                    }
                                }
                                else
                                {
                                    throw new Exception("Unable to retrieve customer data");
                                    cont = false;
                                }
                            }

                            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                            {
                                new Error(ex);
                                foreach (System.Data.Entity.Validation.DbEntityValidationResult err in ex.EntityValidationErrors)
                                {
                                    string ree = err.Entry + System.Environment.NewLine + err.ValidationErrors.ToString();
                                    Utilities.ShowSimpleDialog("Entity Validation Error", ree);
                                }

                            }
                            catch (Exception ex)
                            {
                                new Error(ex);
                                cont = false;
                            }
                        }
                        else
                        {
                            string datapth = Properties.Settings.Default.Data_ResourcePath.ToString();
                            string dbConnect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + datapth + @"\ParcomW.mdb";
                            DataSet custData = new DataSet();

                            string Select2 = @"SELECT TOP 1 FirstName, LastName, Company, FirstDate, LastDate, Taxable, ResaleNo, RecognizedBy FROM CustsMain WHERE CustId = " + input + "";
                            string Select3 = @"SELECT TOP 1 TotalSales, NumTrans FROM CustsMarket WHERE ACustId = " + input + "";
                            string Select4 = @"SELECT TOP 1 StreetAddress, Address2, City, State, Zip, Phone, email, Pager FROM CustAddresses WHERE Customernumber = " + input + "";

                            OleDbConnection myDbConn = null;

                            try
                            {
                                myDbConn = new OleDbConnection(dbConnect);

                                OleDbCommand myComm = new OleDbCommand(Select2, myDbConn);
                                OleDbDataAdapter myDA = new OleDbDataAdapter(myComm);

                                myDbConn.Open();
                                myDA.Fill(custData, "custMainData");

                                OleDbCommand myComm2 = new OleDbCommand(Select3, myDbConn);
                                OleDbDataAdapter myDA2 = new OleDbDataAdapter(myComm2);

                                myDA2.Fill(custData, "custSalesData");

                                OleDbCommand myComm3 = new OleDbCommand(Select4, myDbConn);
                                OleDbDataAdapter myDA3 = new OleDbDataAdapter(myComm3);

                                myDA3.Fill(custData, "custAddrData");

                                myDbConn.Close();

                                var custMainData = custData.Tables["custMainData"];
                                var custAddrData = custData.Tables["custAddrData"];
                                var custSalesData = custData.Tables["custSalesData"];

                                if (custMainData != null && custAddrData != null && custSalesData != null)
                                {
                                    try
                                    {
                                        using (Entities context = new Entities())
                                        {
                                            customer cust = new customer();
                                            cust.CustomerID = Convert.ToInt32(input);
                                            cust.FirstName = custMainData.Rows[0]["FirstName"].ToString();
                                            cust.LastName = custMainData.Rows[0]["Lastname"].ToString();
                                            cust.Company = custMainData.Rows[0]["Company"].ToString();
                                            cust.StreetAddress = custAddrData.Rows[0]["StreetAddress"].ToString();
                                            cust.StreetAddress2 = custAddrData.Rows[0]["Address2"].ToString();
                                            cust.City = custAddrData.Rows[0]["City"].ToString();
                                            cust.State = custAddrData.Rows[0]["State"].ToString();
                                            string zipin = custAddrData.Rows[0]["Zip"].ToString();
                                            if (String.IsNullOrEmpty(zipin)) { cust.Zip = ""; } else { cust.Zip = zipin.LimitTo(7); }
                                            cust.Phone = custAddrData.Rows[0]["Phone"].ToString();
                                            cust.Phone2 = custAddrData.Rows[0]["Pager"].ToString();
                                            cust.Email = custAddrData.Rows[0]["Email"].ToString();
                                            var firstdate = custMainData.Rows[0]["FirstDate"];
                                            cust.FirstDate = Convert.ToDateTime(firstdate);
                                            var lastdate = custMainData.Rows[0]["LastDate"];
                                            cust.LastDate = Convert.ToDateTime(lastdate);
                                            var taxable = custMainData.Rows[0]["Taxable"];
                                            var taxableB = Convert.ToBoolean(taxable);
                                            var taxexmpt = !taxableB;
                                            cust.TaxExempt = taxexmpt;
                                            cust.ResellerID = custMainData.Rows[0]["ResaleNo"].ToString().LimitTo(30);
                                            var totalsales = custSalesData.Rows[0]["TotalSales"].ToString();
                                            cust.TotalSales = Convert.ToDecimal(totalsales);
                                            var totalorders = custSalesData.Rows[0]["NumTrans"].ToString();
                                            cust.TotalOrders = Convert.ToInt32(totalorders);
                                            cust.Loyalty = 1;
                                            cust.Notes = "";

                                            context.customers.Add(cust);
                                            context.SaveChanges();
                                        }
                                    }
                                    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                                    {
                                        new Error(ex);
                                        cont = false;
                                    }
                                    catch (Exception ex)
                                    {
                                        new Error(ex);
                                        cont = false;
                                    }
                                }
                                else
                                {
                                    throw new Exception("Unable to retrieve customer data.");
                                    cont = false;
                                }
                            }
                            catch (Exception ex)
                            {
                                new Error(ex);
                                cont = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                        cont = false;
                    }
                    try
                    {
                        using (Entities ctx = new Entities())
                        {
                            vm.SelectedOrderNewVM.NewHopperOrder.customer = (from q in ctx.customers where q.CustomerID == input select q).FirstOrDefault();
                        }
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                        cont = false;
                    }
                }));
                if (!cont)
                {
                    bWorker.ReportProgress(0, "Cancelled to due error");
                }
                else
                {
                    try
                    {
                        // File operations
                        bWorker.ReportProgress(15, "Creating production folder");
                        bWorker.ReportProgress(20, "Creating design folders");
                        CreateBroSharedFolder();
                        bWorker.ReportProgress(35, "Creating design files");
                        MoveFiles();
                        bWorker.ReportProgress(50, "Creating order notes");
                        CreateOrderNotesFile();
                        bWorker.ReportProgress(55, "Creating receipt data");
                        CreateNewReceipt();
                        bWorker.ReportProgress(65, "Creating new hopper order");
                        CreateNewHopperOrder();
                        bWorker.ReportProgress(75, "Creating order details");
                        CreateOrderDetails();
                        bWorker.ReportProgress(80, "Creating design details");
                        CreateDesignDetails();
                        bWorker.ReportProgress(90, "Creating alpha orders");
                        AddToAlphaOrder();
                        bWorker.ReportProgress(95, "Writing to event log");
                        WriteToLog();
                        bWorker.ReportProgress(99, "Cleaning up");
                        System.Threading.Thread.Sleep(500);
                        bWorker.ReportProgress(100, "Completed");

                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }
            }
        }

        private void SubmitClick(object sender, EventArgs e)
        {
            PreflightSubmit();
        }

        private void AddAnotherClick(object sender, RoutedEventArgs e)
        {
            ChecklistPanel.Visibility = System.Windows.Visibility.Collapsed;
            Storyboard sb2 = this.FindResource("LeftCloseResetAnimation") as Storyboard;
            sb2.Completed += sb2_Completed;
            DesignSetupMainSP.BeginStoryboard(sb2);
        }

        void sb2_Completed(object sender, EventArgs e)
        {
            MainViewModel main = (MainViewModel)App.Current.MainWindow.DataContext;
            main.GoNewOrderCommand.Execute(null);

        }

        private void ViewDetailsClick(object sender, RoutedEventArgs e)
        {
            MainWindow main = (MainWindow)App.Current.MainWindow;
            MainViewModel mvm = (MainViewModel)main.DataContext;
            OrderNewViewModel onvm = (OrderNewViewModel)this.DataContext;

            //   MainViewModel mvm = (MainViewModel)main.DataContext;
            Views.Hopper hop = new Views.Hopper();
            main.navFrame.Navigate(hop, onvm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum);

        }

        private void ShowOverride(object sender, MouseButtonEventArgs e)
        {
            if (overrideButton.Visibility == System.Windows.Visibility.Hidden)
            {
                overrideButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                overrideButton.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private IList<orderdetail> GetDetailLineItems()
        {
            IList<orderdetail> intrnl;

            var onvm = (OrderNewViewModel)this.DataContext;

            using (Entities ctx = new Entities())
            {
                intrnl = (from q in ctx.orderdetails where q.ReceiptNum == onvm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum select q).ToList();
            }

            return intrnl;
        }

        private DataSet GetProductionDataSet()
        {
            string process;
            string designer;
            string priority;

            var mavm = (MainViewModel)App.Current.MainWindow.DataContext;
            var onvm = (OrderNewViewModel)this.DataContext;

            customer thiscust;
            receipt thisrcpt;
            hopperorder CompletedOrder;

            using (Entities ctx = new Entities())
            {
                CompletedOrder = (from a in ctx.hopperorders where a.ReceiptNum == onvm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum select a).FirstOrDefault();
            }
            using (Entities ctx = new Entities())
            {
                process = (from q in ctx.printprocesses where q.ProcessID == CompletedOrder.PrintProcess select q.ProcessName).FirstOrDefault();
                designer = (from k in ctx.froggers where k.FrogID == CompletedOrder.DesignFrog select k.FrogFirstName).FirstOrDefault();
                priority = (from u in ctx.priorities where u.PriorityValue == CompletedOrder.Priority select u.PriorityAbbrev).FirstOrDefault();
                thiscust = (from i in ctx.customers where i.CustomerID == CompletedOrder.CustomerID select i).FirstOrDefault();
                thisrcpt = (from t in ctx.receipts where t.RcptNumber == CompletedOrder.ReceiptNum select t).FirstOrDefault();
            }

            DataTable table = new DataTable("order");
            DataColumn tableKeyColumn = new DataColumn("ReceiptNum", typeof(int));
            DataColumn tableCKeyColumn = new DataColumn("CustID", typeof(int));
            table.Columns.Add(tableKeyColumn);
            table.Columns.Add("FirstName", typeof(string));
            table.Columns.Add("LastName", typeof(string));
            table.Columns.Add("OrderDate", typeof(string));
            table.Columns.Add("DueDate", typeof(string));
            table.Columns.Add("PrintProcess", typeof(string));
            table.Columns.Add("Designer", typeof(string));
            table.Columns.Add("BoxNum", typeof(string));
            table.Columns.Add("Priority", typeof(string));
            table.Columns.Add("PmtStatus", typeof(string));
            table.Columns.Add(tableCKeyColumn);
            table.Columns.Add("Delivered", typeof(string));
            table.Columns.Add("Footer", typeof(string));
            table.Columns.Add("StoreInfo", typeof(string));

            // PAID POP Identifier And Logic.
            string pmtstat;
            decimal tendered;
            decimal total;
            bool result;

            tendered = thisrcpt.RcptTotalTendered;
            total = thisrcpt.RcptTotal;

            if (tendered < total) { result = false; }
            else { result = true; }
            if (result) { pmtstat = "PAID"; }
            else { pmtstat = "POP"; }

            string foot = Properties.Settings.Default.Invoices_Footer;
            string deliver = Properties.Settings.Default.Invoices_Acknowledgment;
            var dueby = CompletedOrder.DueDate;
            var orddd = CompletedOrder.OrderDate;

            string dueStr = dueby.ToString("dddd, MMMM d") + " by " + dueby.ToString("h:mmtt");
            string ordStr = orddd.ToString("dddd, MMMM d") + " - " + orddd.ToString("h:mmtt");

            string sInfo = Properties.Settings.Default.Store_Phone.ToString() + Environment.NewLine;
            sInfo += Properties.Settings.Default.Store_Address1.ToString();
            if (!string.IsNullOrEmpty(Properties.Settings.Default.Store_Address2))
            {
                sInfo += ", " + Properties.Settings.Default.Store_Address2.ToString();
            }
            sInfo += Environment.NewLine;
            sInfo += Properties.Settings.Default.Store_City + ", " + Properties.Settings.Default.Store_State + " " + Properties.Settings.Default.Store_Zip;

            table.Rows.Add(CompletedOrder.ReceiptNum, CompletedOrder.CustFirst, CompletedOrder.CustLast, ordStr, dueStr, process, designer, CompletedOrder.BoxNum, priority, pmtstat, CompletedOrder.CustomerID, deliver, foot, sInfo);
            table.AcceptChanges();

            DataTable lines = new DataTable("lineitems");
            DataColumn lineKeyColumn = new DataColumn("ReceiptNum", typeof(int));
            lines.Columns.Add("DetailID", typeof(int));
            lines.Columns.Add(lineKeyColumn);
            lines.Columns.Add("StockID", typeof(string));
            lines.Columns.Add("Description", typeof(string));
            lines.Columns.Add("CkIn", typeof(bool));
            lines.Columns.Add("Prod", typeof(bool));
            lines.Columns.Add("PostProd", typeof(bool));
            lines.Columns.Add("Quantity", typeof(int));
            lines.Columns.Add("Color", typeof(string));
            lines.Columns.Add("Size", typeof(string));
            lines.Columns.Add("LinePrice", typeof(decimal));
            lines.Columns.Add("LineExtend", typeof(decimal));

            DataTable receiptDT = new DataTable("receipt");
            DataColumn rcptKeyColumn = new DataColumn("ReceiptNum", typeof(int));
            receiptDT.Columns.Add(rcptKeyColumn);
            receiptDT.Columns.Add("SubTotal", typeof(decimal));
            receiptDT.Columns.Add("SalesTax", typeof(decimal));
            receiptDT.Columns.Add("Total", typeof(decimal));

            DataTable customerDT = new DataTable("customer");
            DataColumn custKeyColumn = new DataColumn("CustID", typeof(int));
            customerDT.Columns.Add(custKeyColumn);
            customerDT.Columns.Add("CustomerName", typeof(string));
            customerDT.Columns.Add("Company", typeof(string));
            customerDT.Columns.Add("Street1", typeof(string));
            customerDT.Columns.Add("Street2", typeof(string));
            customerDT.Columns.Add("Phone", typeof(string));
            customerDT.Columns.Add("Email", typeof(string));

            IList<orderdetail> localList = GetDetailLineItems();

            foreach (orderdetail deet in localList)
            {
                lines.Rows.Add(deet.DetailID, deet.ReceiptNum, deet.StockIDPOS, deet.Description, false, false, false, deet.Quantity, deet.Color, deet.Size, deet.LinePrice, deet.LineExtend);
            }

            string custname = thiscust.FirstName + " " + thiscust.LastName;
            string streetl2 = thiscust.City + " " + thiscust.State + " " + thiscust.Zip;

            customerDT.Rows.Add(onvm.SelectedOrderNewVM.NewHopperOrder.CustomerID, custname, thiscust.Company, thiscust.StreetAddress, streetl2, thiscust.Phone, thiscust.Email);
            receiptDT.Rows.Add(onvm.SelectedOrderNewVM.NewHopperOrder.ReceiptNum, thisrcpt.RcptSTotal, thisrcpt.RcptTotalTax, thisrcpt.RcptTotal);

            DataSet set = new DataSet("Dataset");
            set.Tables.Add(table);
            set.Tables.Add(lines);
            set.Tables.Add(customerDT);
            set.Tables.Add(receiptDT);
            set.AcceptChanges();
            DataRelation relation = new DataRelation("line_relation", tableKeyColumn, lineKeyColumn);
            DataRelation relation2 = new DataRelation("line_relation2", tableCKeyColumn, custKeyColumn);
            DataRelation relation3 = new DataRelation("line_relation3", tableKeyColumn, rcptKeyColumn);
            set.Relations.Add(relation);
            set.Relations.Add(relation2);
            set.Relations.Add(relation3);

            return set;
        }

        private void PrintInvoiceClick(object sender, RoutedEventArgs e)
        {
            var onvm = (OrderNewViewModel)this.DataContext;
            var CompletedOrder = onvm.NewHopperOrder;

            DataSet setL = GetProductionDataSet();
            var bin = setL.Tables[0].Rows[0]["BoxNum"].ToString();
            InvoicePlusTest invoice;
            if (bin == "NBY" || bin == "") { invoice = new InvoicePlusTest(true); }
            else { invoice = new InvoicePlusTest(false); }
            invoice.DataSource = setL;
            string PDFPath = CompletedOrder.JobFolderPath + "\\ProductionSheet.pdf";
            string jpgPath = CompletedOrder.JobFolderPath + "\\ProductionSheet.jpg";
            invoice.ExportToPdf(PDFPath);

            PrintHelper.ShowPrintPreview(this, invoice);
        }

        private void PrintGrid(DevExpress.Xpf.Grid.GridControl grid)
        {
            DocumentPreviewWindow preview = new DocumentPreviewWindow();
            PrintableControlLink link = new PrintableControlLink(grid.View as DevExpress.Xpf.Printing.IPrintableControl);
            LinkPreviewModel model = new LinkPreviewModel(link);
            preview.Model = model;
            link.Landscape = true;
            link.CreateDocument(true);
            DevExpress.Xpf.Core.ThemeManager.SetThemeName(preview, DevExpress.Xpf.Core.ThemeManager.GetThemeName(grid));
            preview.ShowDialog();
        }

        private async void ListboxKeydown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you want to permanently exclude this order from Lilypad?");
                if (result == MessageDialogResult.Affirmative)
                {
                    OrderNewViewModel bigvm = (OrderNewViewModel)this.DataContext;
                    OrderNewViewModel vm = ListBoxx.SelectedItem as OrderNewViewModel;
                    int recpt = vm.NewHopperOrder.ReceiptNum;
                    using (Entities ctx = new Entities())
                    {
                        orderexception newx = new orderexception();
                        newx.ReceiptNum = recpt;
                        newx.Text = @"Expired Order Removed From Auto-Queue";
                        ctx.orderexceptions.Add(newx);
                        ctx.SaveChanges();
                    }
                    bigvm.InputRows.Remove(vm);
                }
            }
        }

        private void EditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                MoveToNextUIElement(e);
            }
        }

        void MoveToNextUIElement(KeyEventArgs e)
        {
            FocusNavigationDirection focusDirection = FocusNavigationDirection.Next;
            TraversalRequest request = new TraversalRequest(focusDirection);
            UIElement elementWithFocus = Keyboard.FocusedElement as UIElement;

            if (elementWithFocus != null)
            {
                if (elementWithFocus.MoveFocus(request)) e.Handled = true;
            }
        }

        private void MarkCompletedClicked(object sender, EventArgs e)
        {

        }

        private void DeleteClick(object sender, EventArgs e)
        {
            OrderNewViewModel bigvm = (OrderNewViewModel)this.DataContext;
            if (ListBoxx.SelectedItem == null)
            {
                Utilities.ShowSimpleDialog("No Input", "Please select an order to exclude first.");
            }
            else
            {
                OrderNewViewModel vm = ListBoxx.SelectedItem as OrderNewViewModel;
                if (vm.NewHopperOrder != null)
                {
                    ExcludeOrder();
                }
            }
        }

        private async void ExcludeOrder()
        {
            MessageDialogResult result = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you want to permanently exclude this order from Lilypad?");
            if (result == MessageDialogResult.Affirmative)
            {
                OrderNewViewModel bigvm = (OrderNewViewModel)this.DataContext;
                OrderNewViewModel vm = ListBoxx.SelectedItem as OrderNewViewModel;
                int recpt = vm.NewHopperOrder.ReceiptNum;
                using (Entities ctx = new Entities())
                {
                    orderexception newx = new orderexception();
                    newx.ReceiptNum = recpt;
                    newx.Text = @"Expired Order Removed From Auto-Queue";
                    ctx.orderexceptions.Add(newx);
                    ctx.SaveChanges();
                }
                bigvm.InputRows.Remove(vm);
            }
        }

        private void AddReceiptKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                LoadCustomReceiptNumber(int.Parse(AddCustReceipt.Text.ToString()));
                AddCustReceipt.Text = "Add a receipt...";
            }
        }

        private void ReceiptGotFocus(object sender, RoutedEventArgs e)
        {
            AddCustReceipt.Clear();
            Keyboard.Focus(AddCustReceipt);
        }

        private void ReceiptLostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(AddCustReceipt.Text))
            {
                AddCustReceipt.Text = "Add a receipt...";
            };
        }

        private void AddCustomReceipt(object sender, EventArgs e)
        {
            if (AddCustReceipt.Text.Length > 0)
            {
                LoadCustomReceiptNumber(int.Parse(AddCustReceipt.Text.ToString()));
                AddCustReceipt.Text = "Add a receipt...";
            }
        }

        private void FilterGotFocus(object sender, RoutedEventArgs e)
        {
            if (Filterbox.Text == "Search")
            {
                Filterbox.Clear();
            }
        }

        private void FilterLostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(Filterbox.Text))
            {
                Filterbox.Text = "Search";
            };
        }


        private void SortAscending(object sender, EventArgs e)
        {
            OrderNewViewModel onvm = (OrderNewViewModel)this.DataContext;
            onvm.FilteredRowView.CustomSort = new AscendingSorter();
        }

        private void SortDescend(object sender, EventArgs e)
        {
            OrderNewViewModel onvm = (OrderNewViewModel)this.DataContext;
            onvm.FilteredRowView.CustomSort = new DescendingSorter();
        }

        private void PreviewInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }

        private void ResetClick(object sender, EventArgs e)
        {
            ChecklistPanel.Visibility = System.Windows.Visibility.Collapsed;
            Storyboard sb2 = this.FindResource("LeftCloseResetAnimation") as Storyboard;
            DesignSetupMainSP.BeginStoryboard(sb2);
            expanderContainer.Children.Clear();
            OrderSelectSP.Visibility = System.Windows.Visibility.Visible;
        }

        private void OrderAllAlpha(object sender, EventArgs e)
        {
            var vm = (OrderNewViewModel)this.DataContext;
            var lineitems = vm.InputMasterLineItems;
            try
            {
                foreach (LineViewModel line in lineitems)
                {
                    var newLine = new LineViewModel();
                    newLine.DesignNum = line.DesignNum;
                    newLine.LineCost = line.LineCost;
                    newLine.LineExtend = line.LineExtend;
                    newLine.LineNum = line.LineNum;
                    newLine.LinePrice = line.LinePrice;
                    newLine.LineStockID = line.LineStockID;
                    newLine.SelectedLineItem.Color = line.SelectedLineItem.Color;
                    newLine.SelectedLineItem.Description = line.SelectedLineItem.Description;
                    newLine.SelectedLineItem.DesignID = line.SelectedLineItem.DesignID;
                    newLine.SelectedLineItem.Quantity = line.SelectedLineItem.Quantity;
                    newLine.SelectedLineItem.ReceiptNum = line.SelectedLineItem.ReceiptNum;
                    newLine.SelectedLineItem.Size = line.SelectedLineItem.Size;
                    newLine.SelectedLineItem.StockIDPOS = line.SelectedLineItem.StockIDPOS;
                    newLine.SelectedLineItem.UniqueID = line.SelectedLineItem.UniqueID;
                    vm.ToOrderFromAlpha.Add(newLine);
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private void AddToCard(object sender, EventArgs e)
        {
            var vm = (OrderNewViewModel)this.DataContext;
            var lineitems = vm.InputMasterLineItems;
            try
            {
                foreach (LineViewModel line in lineitems)
                {
                    var newLine = new LineViewModel();
                    newLine.DesignNum = line.DesignNum;
                    newLine.LineCost = line.LineCost;
                    newLine.LineExtend = line.LineExtend;
                    newLine.LineNum = line.LineNum;
                    newLine.LinePrice = line.LinePrice;
                    newLine.LineStockID = line.LineStockID;
                    newLine.SelectedLineItem.Color = line.SelectedLineItem.Color;
                    newLine.SelectedLineItem.Description = line.SelectedLineItem.Description;
                    newLine.SelectedLineItem.DesignID = line.SelectedLineItem.DesignID;
                    newLine.SelectedLineItem.Quantity = line.SelectedLineItem.Quantity;
                    newLine.SelectedLineItem.ReceiptNum = line.SelectedLineItem.ReceiptNum;
                    newLine.SelectedLineItem.Size = line.SelectedLineItem.Size;
                    newLine.SelectedLineItem.StockIDPOS = line.SelectedLineItem.StockIDPOS;
                    newLine.SelectedLineItem.UniqueID = line.SelectedLineItem.UniqueID;

                    List<Expander> exp = expanderContainer.Children.OfType<Expander>().ToList();
                    if (exp.Any(x => x.IsExpanded == true))
                    {
                        Expander tE = exp.FirstOrDefault(x => x.IsExpanded == true);
                        Controls.DesignCards.SetupCard sc = tE.FindChildren<Controls.DesignCards.SetupCard>().First();
                        DesignSetupCardViewModel dcvm = (DesignSetupCardViewModel)sc.DataContext;
                        dcvm.LineItems.Add(newLine);
                    }
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }
    }
}
