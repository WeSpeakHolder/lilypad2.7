﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using DevExpress.XtraRichEdit;
using DevExpress.Office.Utils;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Printing;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.Xpf.WindowsUI.Navigation;
using Lilypad.Model;
using Lilypad.ViewModel;
using Lilypad.Controls.Tags;


namespace Lilypad.Views
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class HopperNew : UserControl
    {
        public HopperNew()
        {
                InitializeComponent();
        }


        private void hopperLoaded(object sender, RoutedEventArgs e)
        {

        }

        private void ItemLoaded(object sender, RoutedEventArgs e)
        {

        }

        private void GridViewRowChanged(object sender, DevExpress.Xpf.Grid.FocusedRowChangedEventArgs e)
        {

        }

        private void GridView_RowDoubleClick(object sender, DevExpress.Xpf.Grid.RowDoubleClickEventArgs e)
        {
            HopperNewViewModel hnvm = (HopperNewViewModel)this.DataContext;
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            if (mvm.DetailsShown)
            {
                mvm.DetailsShown = false;
                mvm.SelectedDetailHopperOrder = hnvm.SelectedOrder;
                mvm.DetailsShown = true;
            }
            else
            {
                mvm.SelectedDetailHopperOrder = hnvm.SelectedOrder;
                mvm.DetailsShown = true;
            }
        }

      

    }
}
