﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.ViewModel;
using Lilypad.Model;
using DevExpress.Xpf.WindowsUI.Navigation;

namespace Lilypad.Views
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Crashboard : UserControl, INavigationAware
    {
        public Crashboard()
        {
            InitializeComponent();
        }

        public void NavigatedFrom(DevExpress.Xpf.WindowsUI.Navigation.NavigationEventArgs e)
        {

        }

        public void NavigatedTo(DevExpress.Xpf.WindowsUI.Navigation.NavigationEventArgs e)
        {

        }

        public void NavigatingFrom(NavigatingEventArgs e)
        {

        }

        private void DismissErrorClicked(object sender, EventArgs e)
        {
            try
            {
                using (Cloud ctx = new Cloud())
                {
                    CrashboardViewModel cvm = (CrashboardViewModel)this.DataContext;
                    foreach (error ee in cvm.SelectedErrors)
                    {
                        int ID = ee.UniqueID;
                        error er = (from s in ctx.errors where s.UniqueID == ID select s).FirstOrDefault();
                        er.Archived = true;
                    }
                    ctx.SaveChanges();
                }
                ((MainViewModel)App.Current.MainWindow.DataContext).GoCrashboardCommand.Execute(null);
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }
    }
}
