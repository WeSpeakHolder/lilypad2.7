﻿using DevExpress.Xpf.Printing;
using DevExpress.Xpf.WindowsUI.Navigation;
using Lilypad.Controls.Tags;
using Lilypad.Model;
using Lilypad.ViewModel;
using LilyReports;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Core.ServerMode;
using DevExpress.Xpf.Grid;
using Lilypad.Controls;


namespace Lilypad.Views
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Preorders : UserControl, INavigationAware
    {
        #region PropertyChanged Controller
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public Preorders()
        {
            InitializeComponent();
        }


        public void NavigatedTo(NavigationEventArgs e)
        {
            
        }

        public void NavigatingFrom(NavigatingEventArgs e)
        {
            
        }

        public void NavigatedFrom(NavigationEventArgs e)
        {
            
        }

        private void PreordersLoaded(object sender, RoutedEventArgs e)
        {
            if (!Utilities.IsInDesignTime)
            {
                if (grid.VisibleRowCount > 0)
                {
                    // grid.SelectItem(0);
                    GridView.MoveFirstRow();
                    GridView.Focus();
                }
            }
        }

        private void LoadGridData()
        {
            if (!Utilities.IsInDesignTime)
            {
                var entityServerModeDataSource = FindResource("ordersSource") as EntityServerModeDataSource;
                if (entityServerModeDataSource != null)
                {
                    grid.ItemsSource = entityServerModeDataSource.Data;
                    grid.SortInfo.Add(new GridSortInfo("CreatedDate", ListSortDirection.Ascending));
                }

                //   grid.SortInfo.Add(new DevExpress.Xpf.Grid.GridSortInfo("Priority", System.ComponentModel.ListSortDirection.Descending));
            }

        }



        private void GridDoubleClick(object sender, MouseButtonEventArgs e)
        {
            preorder row = (preorder)grid.GetFocusedRow();
            preorder ro = new preorder();
            using (Entities ctx = new Entities())
            {
                ro = ctx.preorders.FirstOrDefault(x => x.PreorderID == row.PreorderID);
            }
         //   UI.NavigateDetails(ro);
        }

        private void OpenSelectedRow(GridView view, Point pt)
        {
        }

        private void PrintGridClicked(object sender, EventArgs e)
        {
            var preview = new DocumentPreviewWindow();
            var link = new PrintableControlLink(GridView);
            var model = new LinkPreviewModel(link);
            preview.Model = model;
            link.Landscape = true;
            link.CreateDocument(true);
            var t = Theme.MetropolisLight;
            ThemeManager.SetTheme(preview, t);
            preview.ShowDialog();
        }

        private void ViewArchivedOrder(object sender, EventArgs e)
        {
            //UI.Navigate("OrdersArchived");
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var pvm = (PreordersViewModel)DataContext;
            pvm.ValidateNewOrder();
        }

        private void LoadDesigns()
        {
            if (DesignsSP.Children.Count > 0) DesignsSP.Children.Clear();
            var nvm = (PreordersViewModel)DataContext;
            var details = new List<preorderdesigndetail>();
            using (Entities ctx = new Entities())
            {
                details = ctx.preorderdesigndetails.Where(x => x.PreorderID == nvm.SelectedOrder.PreorderID).ToList();
            }
            Logger.Log("Loading designs for preorder #" + nvm.SelectedOrder.PreorderID);
            foreach (preorderdesigndetail d in details)
            {
                var dc = new Controls.MetroDesignCard(d);
                dc.IsExpanded = true;
                dc.IsHorizontal = false;
              //  Controls.MetroCardLarge mc = new Controls.MetroCardLarge();
              //  mc.CardContent = dc;
                DesignsSP.Children.Add(dc);
                Logger.Log("Loaded design #" + d.DesignNum + " for preorder #" + nvm.SelectedOrder.PreorderID);
            }

            Logger.Log("Loading notes for preorder #" + nvm.SelectedOrder.PreorderID);
            if (string.IsNullOrEmpty(nvm.SelectedOrder.MainNotes))
            {
                
                nvm.SelectedOrder.MainNotes = "No notes";
            }

            if (string.IsNullOrEmpty(nvm.SelectedOrder.ArtworkNotes))
            {
                nvm.SelectedOrder.ArtworkNotes = "No notes";
            }

            nvm.OriginalNotes = nvm.SelectedOrder.MainNotes;
            nvm.OriginalArtNotes = nvm.SelectedOrder.ArtworkNotes;

            nvm.LoadOrderItems();

          //  DesignCount.Content = DesignsSP.Children.OfType<MetroCardLarge>().Count();

        }

        private void Test(object sender, EventArgs e)
        {
            var pvm = (PreordersViewModel)DataContext;
            if (pvm.NewOrderValid)
            {
                pvm.NewOrderValid = false;
            }
            else
            {
                pvm.NewOrderValid = true;
            }
        }

        private void CreatePreorder(preorder p)
        {
            var pvm = (PreordersViewModel)DataContext;
            if (NewPreorderGrid.Opacity == 1.0)
            {
                NewPreorderGrid.OpacityAnimation(0, TimeSpan.FromMilliseconds(400));
                NewPreorderGrid.SetValue(Grid.ZIndexProperty, -1);
                preorder pp = pvm.PreordersList.FirstOrDefault(x => x.PreorderID == p.PreorderID);
                if (pp != null)
                {
                    grid.SelectedItem = pp;
                }
            }
            else
            {
                NewPreorderGrid.OpacityAnimation(1.0, TimeSpan.FromMilliseconds(200));
                NewPreorderGrid.SetValue(Grid.ZIndexProperty, 900);
                pvm.SelectedNewOrder = new preorder();
            }
            SaveAnimation();
        }

        private void CreatePreorderClick(object sender, EventArgs e)
        {
            CreateNewPreOrder();
        }

        private void CreateNewPreOrder()
        {
            var pvm = (PreordersViewModel)DataContext;
            preorder p = pvm.CreateNewPreorderEntry();
            CreatePreorder(p);
            HideNewOrderAndFocus();
        }

        private void ViewSelectedItems(object sender, EventArgs e)
        {
            foreach (var rowH in grid.GetSelectedRowHandles())
            {
                var ord = (preorder)grid.GetRow(rowH);
               // UI.NavigateDetails(ord);
            }
        }

        private void AssignmentChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private async void SaveAnimation()
        {
            SaveButton.Icon = "appbar_check";
            await Task.Delay(200);
            SaveButton.Icon = "appbar_save";
            UpdateGrid();
        }

        private preorder RecentlySelectedOrderHandle { get; set; }

        private async void UpdateGrid()
        {
            PreordersViewModel pvm = (PreordersViewModel) DataContext;
            pvm.ReloadGridData();
            grid.RefreshData();
            await Task.Delay(10);
         //   grid.SelectedItem = null;
         //   await Task.Delay(10);
            if (RecentlySelectedOrderHandle != null && RecentlySelectedOrderHandle.PreorderID > 0)
            {
                preorder pp =
                    (((PreordersViewModel) DataContext).PreordersList.FirstOrDefault(
                        x => x.PreorderID == RecentlySelectedOrderHandle.PreorderID));
                GridView.Focus();
                pvm.SelectedOrder = pp;
               // grid.SelectedItem = pp;
                RecentlySelectedOrderHandle = null;
            }
            else
            {
                GridView.Focus();
               // grid.Focus();
            }
        }

        public void SaveChanges()
        {
            PreordersViewModel pvm = (PreordersViewModel)DataContext;
            preorder p = pvm.SelectedOrder;
            RecentlySelectedOrderHandle = (preorder)grid.SelectedItem;
            try
            {
                using (Entities ctx = new Entities())
                {
                    var tp = ctx.preorders.FirstOrDefault(x => x.PreorderID == p.PreorderID);
                    tp.ArtworkNotes = p.ArtworkNotes;
                    tp.BinNum = p.BinNum;
                    tp.Company = p.Company;
                    tp.CreatedDate = p.CreatedDate;
                    tp.DueDateBegin = p.DueDateBegin;
                    tp.DueDateEnd = p.DueDateEnd;
                    tp.EmailAddr = p.EmailAddr;
                    tp.First = p.First;
                    tp.GarmentNotes = p.GarmentNotes;
                    tp.JobFolderPath = p.JobFolderPath;
                    tp.Last = p.Last;
                    tp.MainNotes = p.MainNotes;
                    tp.OrderFrog = p.OrderFrog;
                    tp.PhoneNum = p.PhoneNum;
                    tp.Process = p.Process;
                    tp.QuoteID = p.QuoteID;
                    tp.SearchTags = p.SearchTags;
                    tp.ShirtsCount = p.ShirtsCount;
                    tp.Status = p.Status;
                    tp.StatusID = p.StatusID;
                    ctx.SaveChanges();
                }

                SaveAnimation();
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private void SaveClicked(object sender, EventArgs e)
        {
            SaveChanges();
        }

        private async void LoadPreorder()
        {
            
        }

        private void grid_SelectionChanged(object sender, GridSelectionChangedEventArgs e)
        {

        }

        private void GridItemChanged(object sender, SelectedItemChangedEventArgs e)
        {
            if (IsLoaded && grid.SelectedItem != null) { LoadDesigns();}
        }

        private void ExpandAllClick(object sender, EventArgs e)
        {
            foreach (MetroDesignCard mc in DesignsSP.FindChildren<MetroDesignCard>(true))
            {
                mc.ExpandCollapse();
            }
        }

        private void MarkAsClick(object sender, EventArgs e)
        {
            ShowCompleteWindow();
        }

        private async void ShowCompleteWindow()
        {
            CompletePreorderGrid.SetValue(Grid.ZIndexProperty, 100);
            CompletePreorderGrid.OpacityAnimation(1, TimeSpan.FromMilliseconds(900));
        }

        private async void DeletePreorderClick(object sender, EventArgs e)
        {
            PreordersViewModel pvm = (PreordersViewModel) DataContext;
            MessageDialogResult del = await Utilities.ShowMetroYNDialog("Confirm Deletion",
                "Are you sure you want to delete this pre-order's folder? This will only delete the folder, not the order itself.");
            if (del == MessageDialogResult.Affirmative)
            {
                if (Directory.Exists(pvm.SelectedOrder.JobFolderPath)) Utilities.DeleteDirectoryRecursive(pvm.SelectedOrder.JobFolderPath);
            }
            
        }

        private void AddItemsClick(object sender, EventArgs e)
        {
            if (ItemsCard.Height < 250)
            {
                ItemsCard.HeightAnimation(250, TimeSpan.FromSeconds(0.5));
                ItemsCard.OpacityAnimation(0.8, TimeSpan.FromSeconds(0.4));
                PreordersViewModel ovm = (PreordersViewModel)this.DataContext;
                ovm.LoadOrderItems();
            }
            else
            {
                ItemsCard.HeightAnimation(0, TimeSpan.FromSeconds(0.5));
                ItemsCard.OpacityAnimation(0, TimeSpan.FromSeconds(0.4));
            }
        }

        private void AddDesignClick(object sender, EventArgs e)
        {
            AddDesign();
        }

        private void AddDesign()
        {
            int cc = ((DesignsSP.Children.OfType<MetroDesignCard>().Count()) + 1);
            PreordersViewModel vm = (PreordersViewModel)DataContext;
            preorderdesigndetail dd = new preorderdesigndetail();
            using (Entities ctx = new Entities())
            {
                int id = 0;
                if (vm.SelectedOrder != null)
                {
                    id = vm.SelectedOrder.PreorderID;
                    dd.PrintProcess = vm.SelectedOrder.Process.HasValue ? vm.SelectedOrder.Process.Value : 1;
                    dd.DesignFolderPath = vm.SelectedOrder.JobFolderPath;
                }
                dd.SizeToGarment = true;
                dd.AP_YOUTH = false;
                dd.PreorderID = id;
                dd.DesignDescription = string.Empty;
                dd.Notes = string.Empty;
                dd.DesignNum = cc;

                try
                {
                    ctx.preorderdesigndetails.Add(dd);
                    ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
            using (Entities ctx = new Entities())
            {
                int id = 0;
                if (vm.SelectedOrder != null)
                {
                    id = vm.SelectedOrder.PreorderID;
                }
                preorderdesigndetail entry = ctx.preorderdesigndetails.FirstOrDefault(x => x.PreorderID == id && x.DesignNum == cc);

                if (entry != null)
                {
                    MetroDesignCard mdc = new MetroDesignCard(entry);
                    mdc.IsHorizontal = false; mdc.IsExpanded = true;
                    MoveFiles(mdc);
                    entry.DesignFolderPath = mdc.SelectedDesignDetail.DesignFolderPath;
                    ctx.SaveChanges();
                    DesignsSP.Children.Add(mdc);
                }
                else
                {
                    MetroDesignCard mdc = new MetroDesignCard(dd);
                    mdc.IsHorizontal = false; mdc.IsExpanded = true;
                    MoveFiles(mdc);
                    entry.DesignFolderPath = mdc.SelectedDesignDetail.DesignFolderPath;
                    ctx.SaveChanges();
                    DesignsSP.Children.Add(mdc);
                }

            }

          //  DesignCount.Content = DesignsSP.Children.OfType<MetroCardLarge>().Count();
        }

        private void MoveFiles(MetroDesignCard card)
        {
            PreordersViewModel vm = (PreordersViewModel)DataContext;
            string newFolder;
            if (card.SelectedDesignDetail.DesignDescription == "") newFolder = "Design " + card.SelectedDesignDetail.DesignNum;
            else if (card.SelectedDesignDetail.DesignDescription != "") newFolder = "Design " + card.SelectedDesignDetail.DesignNum + " - " + card.SelectedDesignDetail.DesignDescription;
            else newFolder = "Design " + card.SelectedDesignDetail.DesignNum;
            string newPath = System.IO.Path.Combine(vm.SelectedOrder.JobFolderPath, newFolder);


            if (!Directory.Exists(newPath))
            {
                // cleaned = Utilities.GetCleanedFilename(newPath);
                try
                {
                    Directory.CreateDirectory(newPath);
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
            card.SelectedDesignDetail.DesignFolderPath = newPath;

            if (card.CurrentArtworkItems.Any())
            {
                try
                {
                    foreach (string item in card.CurrentArtworkItems)
                    {
                        string type = "";
                        string destination = card.SelectedDesignDetail.DesignFolderPath + @"\CDR\";
                        bool isYouth = card.SelectedDesignDetail.AP_YOUTH;

                        if (item == "Apron" || item == "Bandana" || item == "CapFront" || item == "RoundCoasters" || item == "GolfFlag"
                                || item == "GolfTowel" || item == "Koozies" || item == "Mousepad" || item == "Puzzle" || item == "SquareCoasters" || item == "ToteBag" || item == "WineBag")
                        {
                            type = "S";
                            card.CopyArtFile(item, type, destination);
                        }
                        else
                        {
                            if (isYouth)
                            {
                                type = "Y";
                                card.CopyArtFile("Youth" + item, type, destination);
                            }
                            else if (!isYouth)
                            {
                                type = "A";
                                card.CopyArtFile(item, type, destination);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }

        private void OpenFolderClick(object sender, EventArgs e)
        {
            try
            {
                PreordersViewModel pvm = (PreordersViewModel) DataContext;
                System.Diagnostics.Process.Start(pvm.SelectedOrder.JobFolderPath);
            }
            catch (Exception ex)
            {
                
            }
        }

        private void ViewPSheetClick(object sender, EventArgs e)
        {
            var odvm = (PreordersViewModel)DataContext;

            if (odvm.SelectedOrder != null)
            {
                var dir = odvm.SelectedOrder.JobFolderPath;
                if (Directory.Exists(dir))
                {
                    Reports.GetPreorderDetailsReport(odvm.SelectedOrder).SaveReport(dir).ViewReport();
                }
                
            }
        }

        private void AddItemToGarmentsList(preorderitem i)
        {
            var pvm = (PreordersViewModel)DataContext;
            var lp = new LineViewModel();
            lp.SelectedLineItem.Quantity = i.StockQty.Value;
            lp.SelectedLineItem.Description = i.StockDescrip;
            lp.SelectedLineItem.StockIDPOS = i.StockCode;
            lp.SelectedLineItem.Color = i.StockColor;
            lp.SelectedLineItem.Size = i.StockSize;
            lp.SelectedLineItem.UniqueID = i.UniqueID;
            lp.SelectedLineItem.ReceiptNum = i.PreorderID;
            if (i.EstimatedCost.HasValue) lp.LineCost = i.EstimatedCost.Value;
            else lp.LineCost = 0.00m;
            pvm.InputMasterLineItems.Add(lp);

        }

        private void AddNewLineItem(object sender)
        {
            var pvm = (PreordersViewModel)DataContext;
            var id = pvm.SelectedOrder.PreorderID;
            var grid = (Grid)Utilities.FindParentByType<Grid>((DependencyObject)sender);
            var AddItemControl = grid.Children.OfType<Controls.LineItemLookupNoMill>().FirstOrDefault();
            if (AddItemControl != null)
            {
                using (var ctx = new Entities())
                {
                    var it = new preorderitem();
                    it.EstimatedCost = AddItemControl.EstCost;
                    it.PreorderID = id;
                    it.StockCode = AddItemControl.StockID;
                    it.StockColor = AddItemControl.Color;
                    it.StockInvStatus = null;
                    it.StockInvStatusID = null;
                    it.StockMfg = AddItemControl.Manufacturer;
                    it.StockQty = AddItemControl.Quantity;
                    it.StockSize = AddItemControl.Size;
                    ctx.preorderitems.Add(it);
                    ctx.SaveChanges();
                    AddItemToGarmentsList(it);
                }

                AddItemControl.ResetFields();
            }
        }
        private void AddClick(object sender, EventArgs e)
        {
            AddNewLineItem(sender);
        }

        private void ClearClick(object sender, EventArgs e)
        {

        }

        private async void DeleteItemClick(object sender, EventArgs e)
        {
            // delete item
            MessageDialogResult mdr = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you want to delete this line item?");
            if (mdr == MessageDialogResult.Affirmative)
            {
                if (((MetroButton)sender).Tag != null)
                {
                    int id = Convert.ToInt32(((MetroButton)sender).Tag);
                    PreordersViewModel pvm = (PreordersViewModel)DataContext;
                    LineViewModel lv = pvm.InputMasterLineItems.FirstOrDefault(x => x.SelectedLineItem.UniqueID == id);
                    pvm.InputMasterLineItems.Remove(lv);
                    using (Entities ctx = new Entities())
                    {
                            var dd = ctx.preorderitems.FirstOrDefault(x => x.UniqueID == id);
                            ctx.preorderitems.Remove(dd);
                            ctx.SaveChanges();
                    }
                }
            }
        }

        private void ShowPreorderGrid()
        {
            NewPreorderGrid.SetValue(Grid.ZIndexProperty, 100);
            NewPreorderGrid.OpacityAnimation(1, TimeSpan.FromMilliseconds(900));
            FirstnameBox.Focus();
        }
        private void AddPreorderClick(object sender, EventArgs e)
        {
            ShowPreorderGrid();
        }

        private async void HideNewOrderAndFocus()
        {
            FirstnameBox.Clear();
            LastnameBox.Clear();
            CompanyBox.Clear();
            EmailBox.Clear();
            PhoneBox.Clear();
            ShirtsBox.Text = null;
            NewPreorderGrid.OpacityAnimation(0, TimeSpan.FromMilliseconds(400));
            await Task.Delay(420);
            NewPreorderGrid.SetValue(Grid.ZIndexProperty, -1);
            Dispatcher.Invoke(() =>
            {
                GridView.MoveLastRow();
                GridView.Focus();
            });

        }

        private void CancelNewOrder(object sender, MouseButtonEventArgs e)
        {
            CancelNew();
        }

        private async void CancelNew()
        {
            FirstnameBox.Clear();
            LastnameBox.Clear();
            CompanyBox.Clear();
            EmailBox.Clear();
            PhoneBox.Clear();
            ShirtsBox.Text = null;
            NewPreorderGrid.OpacityAnimation(0, TimeSpan.FromMilliseconds(400));
            await Task.Delay(400);
            NewPreorderGrid.SetValue(Grid.ZIndexProperty, -1);
        }

        private async void CancelMark()
        {
            ReceiptLookup.SelectedItem = null;
            BecameOrderGrid.HeightAnimation(0, TimeSpan.FromMilliseconds(100));
            CompletePreorderGrid.OpacityAnimation(0, TimeSpan.FromMilliseconds(400));
            await Task.Delay(400);
            CompletePreorderGrid.SetValue(Grid.ZIndexProperty, -1);
            await Task.Delay(500);
            if (grid.VisibleRowCount > 0) grid.SelectItem(0);
        }

        private void LoadCustomersList()
        {
            using (var context = new Entities())
            {
                var found = (from q in context.hopperorders
                             orderby q.customer.LastName ascending
                             select new { Value = q.ReceiptNum, q.customer.LastName, q.customer.FirstName, q.ReceiptNum }).ToList();
                var stock =
                    (from k in context.alphaitems select new { Value = k.Mill_Name }).Distinct()
                        .OrderBy(x => x.Value)
                        .ToList();

                var cc =
                    found.AsEnumerable()
                        .Select(
                            x =>
                                new
                                {
                                    Value = x.ReceiptNum,
                                    Name = x.LastName + ", " + x.FirstName + " #" + x.ReceiptNum.ToString() + ""
                                })
                        .ToList();

                ReceiptLookup.ItemsSource = cc;
                ReceiptLookup.ValueMember = "Value";
                ReceiptLookup.DisplayMember = "Name";
            }
        }

        private void CompletedOrderClick(object sender, EventArgs e)
        {
            PreordersViewModel pvm = (PreordersViewModel)DataContext;
            using (Entities ctx = new Entities())
            {
                preorder p = ctx.preorders.FirstOrDefault(x => x.PreorderID == pvm.SelectedOrder.PreorderID);
                archivedpreorder ap = new archivedpreorder();
                ap.ArtworkNotes = p.ArtworkNotes;
                ap.BinNum = p.BinNum;
                ap.Company = p.Company;
                ap.CompletedReceiptID = null;
                ap.CompletionDate = DateTime.Now;
                ap.CompletionResult = "Became an order";
                ap.CompletionResultID = null;
                ap.CreatedDate = p.CreatedDate;
                ap.DueDateBegin = p.DueDateBegin;
                ap.DueDateEnd = p.DueDateEnd;
                ap.EmailAddr = p.EmailAddr;
                ap.First = p.First;
                ap.GarmentNotes = p.GarmentNotes;
                ap.JobFolderPath = p.JobFolderPath;
                ap.Last = p.Last;
                ap.MainNotes = p.MainNotes;
                ap.OrderFrog = p.OrderFrog;
                ap.PhoneNum = p.PhoneNum;
                ap.PreorderID = p.PreorderID;
                ap.Process = p.Process;
                ap.QuoteID = p.QuoteID;
                ap.SearchTags = p.SearchTags;
                ap.ShirtsCount = p.ShirtsCount;
                ap.Status = "Completed and became an order";
                ap.StatusID = 10;
                ctx.archivedpreorders.Add(ap);
                ctx.SaveChanges();
            }
            using (Entities ctx = new Entities())
            {
                preorder p = ctx.preorders.FirstOrDefault(x => x.PreorderID == pvm.SelectedOrder.PreorderID);
                ctx.preorders.Remove(p);
                ctx.SaveChanges();
            }
            CancelMark();
            UpdateGrid();
        }



        private void CancelMarkAs(object sender, MouseButtonEventArgs e)
        {
            CancelMark();
        }

        private async void CheckReceipt()
        {
            await Task.Delay(10);
            if (ReceiptLookup.EditValue != null)
            {
                ExpButton.IsEnabled = true;
                ExpButton.Background = Brushes.YellowGreen;
            }
            else
            {
                ExpButton.IsEnabled = false;
                ExpButton.Background = Brushes.DarkGray;
            }
        }

        private void ReceiptChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            CheckReceipt();
        }

        private void ExportCompleteClick(object sender, MouseButtonEventArgs e)
        {
            var rl = ReceiptLookup.SelectedItemValue;
            PreordersViewModel pvm = (PreordersViewModel)DataContext;
            using (Entities ctx = new Entities())
            {
                preorder p = ctx.preorders.FirstOrDefault(x => x.PreorderID == pvm.SelectedOrder.PreorderID);
                archivedpreorder ap = new archivedpreorder();
                ap.ArtworkNotes = p.ArtworkNotes;
                ap.BinNum = p.BinNum;
                ap.Company = p.Company;
                ap.CompletedReceiptID = null;
                ap.CompletionDate = DateTime.Now;
                ap.CompletionResult = "Became order #" + rl.ToString();
                ap.CompletionResultID = Convert.ToInt16(rl);
                ap.CreatedDate = p.CreatedDate;
                ap.DueDateBegin = p.DueDateBegin;
                ap.DueDateEnd = p.DueDateEnd;
                ap.EmailAddr = p.EmailAddr;
                ap.First = p.First;
                ap.GarmentNotes = p.GarmentNotes;
                ap.JobFolderPath = p.JobFolderPath;
                ap.Last = p.Last;
                ap.MainNotes = p.MainNotes;
                ap.OrderFrog = p.OrderFrog;
                ap.PhoneNum = p.PhoneNum;
                ap.PreorderID = p.PreorderID;
                ap.Process = p.Process;
                ap.QuoteID = p.QuoteID;
                ap.SearchTags = p.SearchTags;
                ap.ShirtsCount = p.ShirtsCount;
                ap.Status = "Completed and became an order";
                ap.StatusID = 10;
                ctx.archivedpreorders.Add(ap);
                ctx.SaveChanges();
            }
            using (Entities ctx = new Entities())
            {
                preorder p = ctx.preorders.FirstOrDefault(x => x.PreorderID == pvm.SelectedOrder.PreorderID);
                ctx.preorders.Remove(p);
                ctx.SaveChanges();
            }
            int rid = Convert.ToInt16(rl);
            hopperorder ho;
            using (Entities ctx = new Entities())
            {
                ho = ctx.hopperorders.FirstOrDefault(x => x.ReceiptNum == rid);
            }
            if (ho != null)
            {
                // Move artwork from preorder to hopper order
                foreach (string dir in Directory.GetDirectories(pvm.SelectedOrder.JobFolderPath, "Design", SearchOption.AllDirectories))
                {
                    DirectoryInfo di = new DirectoryInfo(dir);
                    string newPath = System.IO.Path.Combine(ho.JobFolderPath, di.Name).ToString();
                    Utilities.MoveDirectory(dir, newPath);
                }   
            }

            CancelMark();
            UpdateGrid();
        }

        private void AddTimestampToNotes()
        {
            var mvm = (MainViewModel)Application.Current.MainWindow.DataContext;
            var nvm = (PreordersViewModel)DataContext;
            if (nvm.SelectedOrder != null)
            {
                if (!string.IsNullOrEmpty(nvm.SelectedOrder.MainNotes))
                {
                    if (!nvm.SelectedOrder.MainNotes.Trim().EndsWith("->"))
                    {
                        if (!nvm.SelectedOrder.MainNotes.Equals(nvm.OriginalNotes))
                        {
                            nvm.SelectedOrder.MainNotes += Environment.NewLine;
                            nvm.SelectedOrder.MainNotes += "<- " + mvm.LoggedInFrogger.FrogFirstName + " - " +
                                                           DateTime.Now.ToString("MMMM d") + " at " +
                                                           DateTime.Now.ToString("h:mmtt") + " ->";
                            nvm.SelectedOrder.MainNotes += Environment.NewLine;
                            nvm.SelectedOrder.MainNotes += Environment.NewLine;
                         //   MainNotesBox.Text = nvm.SelectedOrder.MainNotes;
                            nvm.OriginalNotes = nvm.SelectedOrder.MainNotes;
                        }
                    }
                    if (!nvm.SelectedOrder.ArtworkNotes.Trim().EndsWith("->"))
                    {
                        if (!nvm.SelectedOrder.ArtworkNotes.Equals(nvm.OriginalArtNotes))
                        {
                            nvm.SelectedOrder.ArtworkNotes += Environment.NewLine;
                            nvm.SelectedOrder.ArtworkNotes += "<- " + mvm.LoggedInFrogger.FrogFirstName + " - " +
                                                           DateTime.Now.ToString("MMMM d") + " at " +
                                                           DateTime.Now.ToString("h:mmtt") + " ->";
                            nvm.SelectedOrder.ArtworkNotes += Environment.NewLine;
                            nvm.SelectedOrder.ArtworkNotes += Environment.NewLine;
                            //   MainNotesBox.Text = nvm.SelectedOrder.MainNotes;
                            nvm.OriginalArtNotes = nvm.SelectedOrder.ArtworkNotes;
                        }
                    }
                }
            }
        }

        private void DeclinedOrderClick(object sender, EventArgs e)
        {
            PreordersViewModel pvm = (PreordersViewModel)DataContext;
            using (Entities ctx = new Entities())
            {
                preorder p = ctx.preorders.FirstOrDefault(x => x.PreorderID == pvm.SelectedOrder.PreorderID);
                archivedpreorder ap = new archivedpreorder();
                ap.ArtworkNotes = p.ArtworkNotes;
                ap.BinNum = p.BinNum;
                ap.Company = p.Company;
                ap.CompletedReceiptID = null;
                ap.CompletionDate = DateTime.Now;
                ap.CompletionResult = "Did not become an order";
                ap.CompletionResultID = null;
                ap.CreatedDate = p.CreatedDate;
                ap.DueDateBegin = p.DueDateBegin;
                ap.DueDateEnd = p.DueDateEnd;
                ap.EmailAddr = p.EmailAddr;
                ap.First = p.First;
                ap.GarmentNotes = p.GarmentNotes;
                ap.JobFolderPath = p.JobFolderPath;
                ap.Last = p.Last;
                ap.MainNotes = p.MainNotes;
                ap.OrderFrog = p.OrderFrog;
                ap.PhoneNum = p.PhoneNum;
                ap.PreorderID = p.PreorderID;
                ap.Process = p.Process;
                ap.QuoteID = p.QuoteID;
                ap.SearchTags = p.SearchTags;
                ap.ShirtsCount = p.ShirtsCount;
                ap.Status = "Completed but did not become an order";
                ap.StatusID = 11;
                ctx.archivedpreorders.Add(ap);
                ctx.SaveChanges();
            }
            using (Entities ctx = new Entities())
            {
                preorder p = ctx.preorders.FirstOrDefault(x => x.PreorderID == pvm.SelectedOrder.PreorderID);
                ctx.preorders.Remove(p);
                ctx.SaveChanges();
            }
            CancelMark();
            UpdateGrid();
        }

        private void JustCompleteClick(object sender, EventArgs e)
        {
            PreordersViewModel pvm = (PreordersViewModel)DataContext;
            using (Entities ctx = new Entities())
            {
                preorder p = ctx.preorders.FirstOrDefault(x => x.PreorderID == pvm.SelectedOrder.PreorderID);
                archivedpreorder ap = new archivedpreorder();
                ap.ArtworkNotes = p.ArtworkNotes;
                ap.BinNum = p.BinNum;
                ap.Company = p.Company;
                ap.CompletedReceiptID = null;
                ap.CompletionDate = DateTime.Now;
                ap.CompletionResult = "Became an order";
                ap.CompletionResultID = null;
                ap.CreatedDate = p.CreatedDate;
                ap.DueDateBegin = p.DueDateBegin;
                ap.DueDateEnd = p.DueDateEnd;
                ap.EmailAddr = p.EmailAddr;
                ap.First = p.First;
                ap.GarmentNotes = p.GarmentNotes;
                ap.JobFolderPath = p.JobFolderPath;
                ap.Last = p.Last;
                ap.MainNotes = p.MainNotes;
                ap.OrderFrog = p.OrderFrog;
                ap.PhoneNum = p.PhoneNum;
                ap.PreorderID = p.PreorderID;
                ap.Process = p.Process;
                ap.QuoteID = p.QuoteID;
                ap.SearchTags = p.SearchTags;
                ap.ShirtsCount = p.ShirtsCount;
                ap.Status = "Completed and became an order";
                ap.StatusID = 10;
                ctx.archivedpreorders.Add(ap);
                ctx.SaveChanges();
            }
            using (Entities ctx = new Entities())
            {
                preorder p = ctx.preorders.FirstOrDefault(x => x.PreorderID == pvm.SelectedOrder.PreorderID);
                ctx.preorders.Remove(p);
                ctx.SaveChanges();
            }
            CancelMark();
            UpdateGrid();
        }

        private void NotesGotFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text == "No notes")
            {
                tb.Text = string.Empty;
                Keyboard.Focus(tb);
            }
        }

        private void NotesLostFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text == string.Empty)
            {
                tb.Text = "No notes";
            }
            else
            {
                AddTimestampToNotes();
                SaveNotes();
            }
        }

        private void SaveNotes()
        {
            PreordersViewModel nvm = (PreordersViewModel) DataContext;
            string main = nvm.SelectedOrder.MainNotes;
            string art = nvm.SelectedOrder.ArtworkNotes;
            string fold = nvm.SelectedOrder.JobFolderPath;
            string mPath = System.IO.Path.Combine(fold, "OrderNotes.txt");
            string aPath = System.IO.Path.Combine(fold, "ArtworkNotes.txt");
            try
            {
                File.WriteAllText(mPath, main);
                File.WriteAllText(aPath, art);
            }
            catch (Exception ex)
            {
                new Error(ex);  
            }
        }

        private void NewPreorderKeydown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                CreateNewPreOrder();
            }
        }

        private void itemLookupKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                LineItemLookupNoMill lmn = (LineItemLookupNoMill) sender;
                var kids = lmn.FindChildren<DevExpress.Xpf.Editors.TextEdit>();
                if (kids.Any())
                {
                    var editor = kids.FirstOrDefault();
                    if (editor.IsFocused)
                    {
                        AddNewLineItem(sender);
                        lmn.ResetFields();

                    }
                }
            }

        }

        private void AddNewItem(object sender, MouseButtonEventArgs e)
        {
            PreordersViewModel pvm = (PreordersViewModel)this.DataContext;
            int id = pvm.SelectedOrder.PreorderID;
            using (var ctx = new Entities())
            {
                var it = new preorderitem();
                it.EstimatedCost = pvm.AddNewQuote;
                it.PreorderID = id;
                it.StockCode = pvm.AddNewStock;
                it.StockColor = pvm.AddNewColor;
                it.StockInvStatus = null;
                it.StockInvStatusID = null;
                it.StockMfg = string.Empty;
                it.StockQty = pvm.AddNewQty;
                it.StockSize = pvm.AddNewSize;
                ctx.preorderitems.Add(it);
                ctx.SaveChanges();
                AddItemToGarmentsList(it);
            }

            pvm.AddNewColor = null;
            pvm.AddNewQty = null;
            pvm.AddNewQuote = null;
            pvm.AddNewSize = null;
            pvm.AddNewStock = null;
            pvm.AddNewStock = null;
        }

        private void ViewOrderNoteslick(object sender, EventArgs e)
        {
            PreordersViewModel pvm = (PreordersViewModel) DataContext;
            string path = System.IO.Path.Combine(pvm.SelectedOrder.JobFolderPath, "OrderNotes.txt");
            if (File.Exists(path)) System.Diagnostics.Process.Start(path);
        }

        private void ViewArtNotesClick(object sender, EventArgs e)
        {
            PreordersViewModel pvm = (PreordersViewModel)DataContext;
            string path = System.IO.Path.Combine(pvm.SelectedOrder.JobFolderPath, "ArtworkNotes.txt");
            if (File.Exists(path)) System.Diagnostics.Process.Start(path);
        }

        private void ShirtsBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
    }
}
