﻿using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Ookii.Dialogs.Wpf;

namespace Lilypad.Views.Settings
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Databases : UserControl
    {
        public Databases()
        {
            InitializeComponent();
        }

        private void SaveChangesClick(object sender, EventArgs e)
        {
            Properties.Settings.Default.MySQL_ServerIP = Host.Text;
            Properties.Settings.Default.MySQL_UserName = Username.Text;
            Properties.Settings.Default.MySQL_Password = PasswordBox.Text;
            Properties.Settings.Default.Data_RecordSelectThreshold = Convert.ToInt32(RecordsBox.Text);
            Properties.Settings.Default.Data_ServerName = Hostname.Text;
            Properties.Settings.Default.Data_AlphaData = DataAlphaBox.Text;
            Properties.Settings.Default.Data_ArchiveFolderPath = DataArchiveBox.Text;
            Properties.Settings.Default.Data_PendingFolderPath = DataPendingBox.Text;
            Properties.Settings.Default.Data_ProductionFolderPath = DataProductionBox.Text;
            Properties.Settings.Default.Data_ResourcePath = DataResourceBox.Text;
            Properties.Settings.Default.Data_TemplatesPath = DataTemplatesBox.Text;
            Properties.Settings.Default.Save();
            Utilities.UpdateConfig();
            Toaster.SavedChanges();
        }

        private void EditPathClicked(object sender, EventArgs e)
        {
            StackPanel sp = Utilities.FindParentByType<StackPanel>(((DependencyObject)sender));
            TextBox tb = (TextBox)sp.FindChildren<TextBox>(true).First();
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.SelectedPath))
            {
                tb.Text = dialog.SelectedPath.ToString();
            }
        }

        private void SwitchEnvironment(object sender, EventArgs e)
        {
            string host = Properties.Settings.Default.MySQL_ServerIP.ToString();
            if (host != "localhost")
            {
                Host.Text = "localhost";
                Username.Text = "lily";
                PasswordBox.Text = ApplicationDeployment.IsNetworkDeployed? "$$frog9big10" : "L1lyP4duS3r";
                DataAlphaBox.Text = @"C:\Users\Tyler\Documents\Lilypad\Resources\alphadata";
                DataArchiveBox.Text = @"C:\Users\Tyler\Documents\Lilypad\Resources\customer";
                DataPendingBox.Text = @"C:\Users\Tyler\Documents\Lilypad\Resources\working";
                DataProductionBox.Text = @"C:\Users\Tyler\Documents\Lilypad\Resources\bro shared";
                DataResourceBox.Text = @"C:\Users\Tyler\Documents\Lilypad\Resources\posdata";
                DataTemplatesBox.Text = @"C:\Users\Tyler\Documents\Lilypad\Resources\templates\Lilypad";
                EnvironmentLabel.Content = "SWITCH TO PRODUCTION ENVIRONMENT";
            }
            else 
            {
                Host.Text = "192.168.0.106";
                Username.Text = "lily";
                PasswordBox.Text = ApplicationDeployment.IsNetworkDeployed ? "$$frog9big10" : "L1lyP4duS3r";
                DataAlphaBox.Text = @"\\PRINTSTATION\Server\alphadata";
                DataArchiveBox.Text = @"\\PRINTSTATION\Store Data\Orders\Customers";
                DataPendingBox.Text = @"\\PRINTSTATION\Store Data\Orders\Working";
                DataProductionBox.Text = @"\\PRINTSTATION\Store Data\Orders\Brother Shared";
                DataResourceBox.Text = @"\\SERVERC-PC\ReSource\ReSourcePOS";
                DataTemplatesBox.Text = @"\\PRINTSTATION\Store Data\Locked Templates\Lilypad";
                EnvironmentLabel.Content = "SWITCH TO DEVELOPMENT ENVIRONMENT";
            }

            SaveChangesClick(null, null);
        }


        private void ChangeEnvClick(object sender, MouseButtonEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift)
            {
                if (EnvTile.Visibility == System.Windows.Visibility.Collapsed)
                {
                    EnvTile.Visibility = System.Windows.Visibility.Visible;
                    EnvTile2.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    EnvTile.Visibility = System.Windows.Visibility.Collapsed;
                    EnvTile2.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        private void EditFileClicked(object sender, EventArgs e)
        {
            StackPanel sp = Utilities.FindParentByType<StackPanel>(((DependencyObject)sender));
            TextBox tb = (TextBox)sp.FindChildren<TextBox>(true).First();
            var dialog = new VistaOpenFileDialog();
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.FileName))
            {
                tb.Text = dialog.FileName.ToString();
            }
        }

        private void SwitchEnvironment2(object sender, EventArgs e)
        {
            Host.Text = "192.168.1.70";
            Username.Text = "lily";
            PasswordBox.Text = "C1zJh1ACKnY3";
            DataAlphaBox.Text = @"\\PRINTSTATION\Server\alphadata";
            DataArchiveBox.Text = @"\\192.168.1.253\SharedFiles\Customers\0_Brother Shared\0_Completed Customers";
            DataPendingBox.Text = @"\\192.168.1.253\SharedFiles\Customers";
            DataProductionBox.Text = @"\\192.168.1.253\SharedFiles\Customers\0_Brother Shared";
            DataResourceBox.Text = @"\\Serverc\c\ReSource\ReSourcePOS";
            DataTemplatesBox.Text = @"\\192.168.1.253\SharedFiles\Lilypad\Templates";
            EnvironmentLabel.Content = "SWITCH TO DEVELOPMENT ENVIRONMENT";

            SaveChangesClick(null, null);
        }
    }
}
