﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace Lilypad.Views.Settings
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Business : UserControl
    {
        public Business()
        {
            InitializeComponent();
        }

        private void SaveChangesClick(object sender, EventArgs e)
        {
            Properties.Settings.Default.Store_Address1 = StoreAddress1Box.Text;
            Properties.Settings.Default.Store_Address2 = StoreAddress2Box.Text;
            Properties.Settings.Default.Store_City = StoreCityBox.Text;
            Properties.Settings.Default.Store_Email = StoreEmailBox.Text;
            Properties.Settings.Default.Store_Name = StoreNameBox.Text;
            Properties.Settings.Default.Store_HoursMF = StoreHoursMFBox.Text;
            Properties.Settings.Default.Store_HoursSat = StoreHoursSatBox.Text;
            Properties.Settings.Default.Store_HoursSun = StoreHoursSunBox.Text;
            Properties.Settings.Default.Store_Owner = StoreOwnerBox.Text;
            Properties.Settings.Default.Store_Phone = StorePhoneBox.Text;
            Properties.Settings.Default.Store_State = StoreStateBox.Text;
            Properties.Settings.Default.Store_Zip = StoreZipBox.Text;
            Properties.Settings.Default.Store_ID = StoreIDBox.Text;
            Properties.Settings.Default.Interop_Email_Message = EmailMessageBox.Text;
            Properties.Settings.Default.Save();
            Utilities.UpdateConfig();
            Toaster.SavedChanges();
        }

        private void CheckUncheck(object sender, RoutedEventArgs e)
        {
            Toaster.SavedChanges();
        }
    }
}
