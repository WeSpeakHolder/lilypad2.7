﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Lilypad.Properties;
using Lilypad.ViewModel;
using Lilypad.Model;
using MahApps.Metro;

namespace Lilypad.Views.Settings
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Interface : UserControl
    {
        public Interface()
        {
            InitializeComponent();
            LoadedFully = false;
        }

        public bool LoadedFully { get; private set; }

        private void Grid_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            var parent = Utilities.FindParentByType<TransitioningContentControl>(this);
            var newPres = new Dialogs.MainMenu();
            parent.Content = newPres;
        }

        private void SettingChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LoadedFully)
            {
                MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
                ComboBoxItem item = (ComboBoxItem)ThemeBox.SelectedItem;
                string accName = item.Content.ToString();

                mvm.LoggedInFrogger.Notes = accName;

                using (Entities ctx = new Entities())
                {
                    frogger thisFrog = (from c in ctx.froggers where c.FrogID == mvm.LoggedInFrogger.FrogID select c).FirstOrDefault();
                    thisFrog.Notes = accName;
                    ctx.SaveChanges();
                }

                Toaster.SavedChanges();
                Utilities.LoadTheme(accName);
            }
        }

        private void AccentSettingChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void InterfaceLoaded(object sender, RoutedEventArgs e)
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            if (mvm.LoggedInFrogger != null)
            {
                if (!string.IsNullOrEmpty(mvm.LoggedInFrogger.Notes))
                {
                    ThemeBox.SelectedValue = mvm.LoggedInFrogger.Notes;
                }
            }

            LoadedFully = true;
        }

        private void InterfaceUnloaded(object sender, RoutedEventArgs e)
        {
            LoadedFully = false;
        }
    }
}
