﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lilypad.Model;
using MahApps.Metro.Controls;

namespace Lilypad.Views.Settings
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Automation : UserControl
    {
        public Automation()
        {
            InitializeComponent();
            LoadedFully = false;
        }
        public bool LoadedFully { get; private set; }

        private void CheckUncheck(object sender, RoutedEventArgs e)
        {
            if (LoadedFully)
            {
                Properties.Settings.Default.Save();
                Toaster.SavedChanges();
            }
        }
        //-- Hide UltraPrint Option code disable 12262019
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
           if (LoadedFully)
            {            
                Properties.Settings.Default.Save();
               Toaster.SavedChanges();
           }
       }

        private void pageLoaded(object sender, RoutedEventArgs e)
        {
            LoadedFully = true;
        }

        private void pageUnloaded(object sender, RoutedEventArgs e)
        {
            LoadedFully = false;
            List<ultraprintorderdata> ultradata = new List<ultraprintorderdata>();
            using (Entities ctx = new Entities())
            {
                ultradata = (from t in ctx.ultraprintorderdatas select t).ToList();
                foreach (var item in ultradata)
                {
                    DateTime dt = (DateTime)item.createddate;
                    item.expiredate = dt.AddMonths(Properties.Settings.Default.UltraPrint_ExpirationDate);

                    ctx.SaveChanges();
                }
            }

        }
    }
}
