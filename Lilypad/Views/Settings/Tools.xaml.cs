﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lilypad.Model;
using MahApps.Metro.Controls;
using Lilypad.Properties;
using MahApps.Metro;
using Ookii.Dialogs.Wpf;

namespace Lilypad.Views.Settings
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Tools : UserControl
    {
        public Tools()
        {
            InitializeComponent();
            pickupWorker = new BackgroundWorker();
            pickupWorker.ProgressChanged += PickupWorker_ProgressChanged;
            pickupWorker.DoWork += PickupWorker_DoWork;
            pickupWorker.WorkerReportsProgress = true;
            pickupWorker.WorkerSupportsCancellation = false;
        }

        private void PickupWorker_DoWork(object sender, DoWorkEventArgs e)
        { 
            try
            {
                pickupWorker.ReportProgress(0, "Total jobs to be archived: " + PickedUpOrders.Count);


                for (int i = 0; i < PickedUpOrders.Count; i++)
                {
                    int perc = i * 100 / PickedUpOrders.Count();
                    pickupWorker.ReportProgress(perc, "Working on order " + i + " of " + PickedUpOrders.Count + ": " + PickedUpOrders[i].CustLast + "-" + PickedUpOrders[i].ReceiptNum);
                    MarkAsPickedUp(PickedUpOrders[i].ReceiptNum);
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private void MarkAsPickedUp(int thisID)
        {
            hopperorder thisorder = new hopperorder();
            order neworder = new order();

            using (Entities ctx = new Entities())
            {
                if (ctx.hopperorders.Any(x => x.ReceiptNum == thisID))
                {
                    thisorder = (from k in ctx.hopperorders where k.ReceiptNum == thisID select k).FirstOrDefault();

                    if (ctx.calllists.Any(x => x.ReceiptNum == thisID))
                    {
                        calllist cl = (calllist) ctx.calllists.First(x => x.ReceiptNum == thisID);
                        try
                        {
                            ctx.calllists.Remove(cl);
                            ctx.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }

                    if (ctx.calllists.Any(x => x.HopperReceipt == thisID))
                    {
                        calllist cl = (calllist) ctx.calllists.First(x => x.HopperReceipt == thisID);
                        try
                        {
                            ctx.calllists.Remove(cl);
                            ctx.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }

                    if (ctx.orders.Any(x => x.ReceiptNum == thisID))
                    {
                        order cl = (order) ctx.orders.First(x => x.ReceiptNum == thisID);
                        try
                        {
                            ctx.orders.Remove(cl);
                            ctx.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            new Error(ex);
                        }
                    }

                    neworder.BoxNum = thisorder.BoxNum;
                    neworder.Archived = true;
                    neworder.CareTags = 1;
                    neworder.Correspondence = thisorder.Correspondence;
                    neworder.CustomerID = thisorder.CustomerID;
                    neworder.DesignFrog = thisorder.DesignFrog;
                    neworder.DueDate = thisorder.DueDate;
                    neworder.Irregulars = thisorder.Irregulars;
                    neworder.JobFolderPath = thisorder.JobFolderPath;
                    neworder.Notes = thisorder.Notes;
                    neworder.OrderDate = thisorder.OrderDate;
                    neworder.OrderFrog = thisorder.OrderFrog;
                    neworder.PrintFrog = thisorder.OrderFrog;
                    neworder.PrintProcess = thisorder.PrintProcess;
                    neworder.Priority = thisorder.Priority;
                    neworder.ReceiptNum = thisorder.ReceiptNum;
                    neworder.SearchTags = thisorder.SearchTags;
                    neworder.TotalCost = thisorder.TotalCost;
                    neworder.TotalShirts = thisorder.TotalShirts;
                    neworder.Status = 11;
                    neworder.CompletedDate = DateTime.Now;
                    neworder.ArtworkStatus = thisorder.ArtworkStatus;
                    neworder.InventoryStatus = thisorder.InventoryStatus;

                    try
                    {
                        ctx.orders.Add(neworder);
                        ctx.SaveChanges();
                        ctx.hopperorders.Remove(thisorder);
                        ctx.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }

                    try
                    {
                        using (Entities ctx2 = new Entities())
                        {
                            log thislog = new log();
                            thislog.Action = "Someone" + " marked this order as picked up.";
                            thislog.LogDateTime = DateTime.Now;
                            thislog.ReceiptNum = thisID;
                            ctx2.logs.Add(thislog);
                            ctx2.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }

                    try
                    {
                        string arc = Properties.Settings.Default.Data_ArchiveFolderPath.ToString();
                        string path = thisorder.JobFolderPath;
                        System.IO.DirectoryInfo dd = new System.IO.DirectoryInfo(path);
                        string des = System.IO.Path.Combine(arc, dd.Name);
                        Utilities.MoveDirectory(path, des);
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }

                    Task.Factory.StartNew(() => { Utilities.CheckIOQueueForPendingOperations(); });
                }
            }
        }

        private void PickupWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.Dispatcher.Invoke(() => { PickupStatusLabel.Content = e.ProgressPercentage == 0 ? e.UserState.ToString() : e.UserState.ToString() + " (" + e.ProgressPercentage + @"%)"; }); 
        }

        private void EditPathClicked(object sender, EventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.SelectedPath))
            {
                LilypadDirPathLabel.Content = dialog.SelectedPath.ToString();
            }
        }

        private void TaffyConsistency(object sender, EventArgs e)
        {
            if (TaFFFy.RunFileConsistencyCheck_Hopper())
            {
                SuccessLabel.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void TaffyCleanup(object sender, EventArgs e)
        {
            if (TaFFFy.RunStaleOrderCleanup_Hopper())
            {
                SuccessLabel2.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void ManualUpdate(object sender, EventArgs e)
        {
            Utilities.InstallUpdateSQLScript();
        }

        private void ExportSettingsFile(object sender, EventArgs e)
        {
            SuccessLabel5.Visibility = Utilities.ExportLilypadOptionsToLocalFile()
                ? Visibility.Visible
                : Visibility.Collapsed;
        }

        private void SelImportPath(object sender, EventArgs e)
        {
            var dialog = new VistaOpenFileDialog();
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.FileName))
            {
                ImportPathLabel.Content = dialog.FileName.ToString();
            }
        }

        private void ImportSettingsClicked(object sender, EventArgs e)
        {
            SuccessLabel6.Visibility = Utilities.ImportLilypadOptionsFromFile(ImportPathLabel.Content.ToString())
                ? Visibility.Visible
                : Visibility.Collapsed;
        }

        private void CreateDirClicked(object sender, EventArgs e)
        {
            SuccessLabel4.Visibility = Utilities.InitialFolderInstall(LilypadDirPathLabel.Content.ToString())
    ? Visibility.Visible
    : Visibility.Collapsed;
        }

        private void NewFavoritesClick(object sender, EventArgs e)
        {
            try
            {
                Utilities.CreateFavoriteFolderLinks();
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private void FirewallsClick(object sender, EventArgs e)
        {
            SuccessLabel7.Visibility = Utilities.CreateFirewallExceptions()
                ? System.Windows.Visibility.Visible
                : System.Windows.Visibility.Collapsed;
        }

        private void GetDirClicked(object sender, EventArgs e)
        {
            Clipboard.SetText(System.AppDomain.CurrentDomain.BaseDirectory);
            SuccessLabel8.Visibility = System.Windows.Visibility.Visible;
        }

        private void RunLastUpdate(object sender, EventArgs e)
        {
            Utilities.InstallUpdateSQLScript();
        }

        private IList<hopperorder> pickeduporders;

        public IList<hopperorder> PickedUpOrders
        {
            get { return pickeduporders; }
            set { pickeduporders = value; }
        }

        private BackgroundWorker pickupWorker;

        private void PickupBatchClick(object sender, EventArgs e)
        {
            PickedUpOrders = new List<hopperorder>();
            using (Entities ctx = new Entities())
            {
                PickedUpOrders = ctx.hopperorders.Where(x => x.Status == "Completed" && x.DueDate < dueCompleted.SelectedDate).ToList();
            }
            pickupWorker.RunWorkerAsync();
        }

        private void ForceArchive(object sender, EventArgs e)
        {
            try
            {
                List<alphacurrentorder> itemsOnOrder = new List<alphacurrentorder>();
                int TotalItemCount;
                decimal EstimatedPrice;

                using (var ctx = new Entities())
                {
                    itemsOnOrder = ctx.alphacurrentorders.ToList();
                }

                var sum = 0.00m;

                foreach (var order in itemsOnOrder)
                {
                    if (order.SalePrice == 0.00m)
                    {
                        var quant = order.Quantity * order.UnitPrice;
                        order.LineCost = quant;
                        sum += quant;
                    }
                    else
                    {
                        var quant = order.Quantity * order.SalePrice;
                        order.LineCost = quant;
                        sum += quant;
                    }
                }

                var sum3 = itemsOnOrder.Sum(x => x.Quantity);
                TotalItemCount = sum3;
                EstimatedPrice = sum;

                var mvm = (ViewModel.MainViewModel)Application.Current.MainWindow.DataContext;

                var newOrderID = Utilities.GenerateAlphaPONumber();

                using (var ctx = new Entities())
                {
                    var newOrd = new alphaorder();
                    newOrd.IDPO = newOrderID;
                    newOrd.IDAlpha = "";
                    newOrd.Status = "Ordered " + DateTime.Now.ToString("M/d") + " - Processing";
                    newOrd.TotalItems = TotalItemCount;
                    newOrd.TotalCost = EstimatedPrice;
                    newOrd.OrderDate = DateTime.Now;
                    newOrd.ShipDate = DateTime.MinValue;
                    newOrd.ReceivedDate = DateTime.MinValue;
                    newOrd.Archived = false;
                    newOrd.TrackingNum = "";
                    newOrd.OrderFrog = mvm.LoggedInFrogger.FrogID;
                    newOrd.ReceivedFrog = mvm.LoggedInFrogger.FrogID;

                    ctx.alphaorders.Add(newOrd);
                    ctx.SaveChanges();
                }

                using (var ctx = new Entities())
                {
                    foreach (var order in itemsOnOrder)
                    {
                        var entry = new alphaentries1();
                        entry.ReceiptNum = order.ReceiptNum;
                        entry.CustFirstName = string.IsNullOrEmpty(order.CustFirstName) ? "First" : order.CustFirstName;
                        entry.CustLastName = string.IsNullOrEmpty(order.CustLastName) ? "Last" : order.CustLastName;
                        entry.BoxNum = string.IsNullOrEmpty(order.BoxNum) ? "NBY" : order.BoxNum;
                        entry.IDPO = newOrderID;
                        entry.IDAlpha = string.IsNullOrEmpty(order.IDStockAlpha) ? "Stock" : order.IDStockAlpha.LimitTo(100);
                        entry.IDItemCodeAlpha = string.IsNullOrEmpty(order.IDItemCodeAlpha) ? "ItemCode" : order.IDItemCodeAlpha.LimitTo(100);
                        entry.IDStockAlpha = string.IsNullOrEmpty(order.IDStockAlpha) ? "Stock ID" : order.IDStockAlpha.LimitTo(100);
                        entry.Quantity = order.Quantity;
                        entry.ItemDescription = string.IsNullOrEmpty(order.ItemDescription) ? "Stock Description" : order.ItemDescription;
                        entry.Color = string.IsNullOrEmpty(order.Color) ? "Color" : order.Color;
                        entry.Size = string.IsNullOrEmpty(order.Size) ? "SZ" : order.Size;
                        entry.Status = string.IsNullOrEmpty(order.Status) ? "Status" : order.Status;
                        entry.LineCost = 0.00m;
                        entry.AddedDate = DateTime.Now;
                        ctx.alphaentries1.Add(entry);
                    }
                    ctx.SaveChanges();
                }

                using (var ctx = new Entities())
                {
                    var allitems = ctx.alphacurrentorders.ToList();
                    foreach (var item in allitems)
                    {
                        ctx.alphacurrentorders.Remove(item);
                    }
                    ctx.SaveChanges();
                }

                SuccessLabel9.Visibility = Visibility.Visible;
            }

            catch (Exception ex)
            {
                new Error(ex);
            }

        }
    }
}
