﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Ookii.Dialogs.Wpf;

namespace Lilypad.Views.Settings
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class SystemSettings : UserControl
    {
        public SystemSettings()
        {
            InitializeComponent();
            FullyLoaded = false;
        }

        public bool FullyLoaded { get; private set; }

        private void CheckUncheck(object sender, RoutedEventArgs e)
        {
            if (FullyLoaded)
            {
                Properties.Settings.Default.Save();
                Toaster.SavedChanges();
            }
        }

        private void pageLoaded(object sender, RoutedEventArgs e)
        {
            FullyLoaded = true;
        }

        private void pageUnloaded(object sender, RoutedEventArgs e)
        {
            FullyLoaded = false;
        }
    }
}
