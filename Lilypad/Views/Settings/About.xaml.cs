﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace Lilypad.Views.Settings
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class About : UserControl
    {
        public About()
        {
            InitializeComponent();
        }

        private void Grid_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            var parent = Utilities.FindParentByType<TransitioningContentControl>(this);
            var newPres = new Dialogs.MainMenu();
            parent.Content = newPres;
        }
    }
}
