﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lilypad.Interop;
using MahApps.Metro.Controls;
using Ookii.Dialogs.Wpf;

namespace Lilypad.Views.Settings
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Interop : UserControl
    {
        public Interop()
        {
            InitializeComponent();
        }

        private void SaveChangesClick(object sender, EventArgs e)
        {
            Properties.Settings.Default.Interop_Email_Username = UsernameBox.Text.ToString().Trim();
            Properties.Settings.Default.Interop_Email_Password = PasswordBox2.Password.ToString().Trim();
            Properties.Settings.Default.Interop_SMTPServer = SMTPServer.Text.ToString().Trim();
            Properties.Settings.Default.Interop_SMTPPort = Convert.ToInt32(SMTPPort.Text.ToString().Trim());
            Properties.Settings.Default.Interop_SMTPUseSSL = ssltoggle.IsChecked;
            Properties.Settings.Default.Interop_Email_From = FromBox.Text.ToString().Trim();
            Properties.Settings.Default.Interop_Email_Message = HolidayEmailText.Text.ToString().Trim();
            Properties.Settings.Default.Interop_Email_OverrideMessage = overrideToggle.IsChecked;
            Properties.Settings.Default.Save();
            Utilities.UpdateConfig();
            Toaster.SavedChanges();
        }

        private void EditPathClicked(object sender, EventArgs e)
        {
            StackPanel sp = Utilities.FindParentByType<StackPanel>(((DependencyObject)sender));
            TextBox tb = (TextBox)sp.FindChildren<TextBox>(true).First();
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.SelectedPath))
            {
                tb.Text = dialog.SelectedPath.ToString();
            }
        }

        private void InteropLoaded(object sender, RoutedEventArgs e)
        {
            PasswordBox2.Password = Properties.Settings.Default.Interop_Email_Password.ToString().Trim();
           
        }

        private void CheckUncheck(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void TestServerClick(object sender, EventArgs e)
        {
            ResultsLabel.Visibility = System.Windows.Visibility.Visible;

            if (string.IsNullOrEmpty(SMTPPort.Text) || string.IsNullOrEmpty(SMTPServer.Text))
            {
                ResultsLabel.Content = "SMTP Server and Port are required fields";
                ResultsLabel.Foreground = Brushes.Red;
            }
            else
            {

                bool result = Utilities.TestSMTP(SMTPServer.Text, Convert.ToInt32(SMTPPort.Text));

                if (result)
                {
                    ResultsLabel.Content = "SMTP Settings Validated";
                    ResultsLabel.Foreground = Brushes.YellowGreen;
                }
                else
                {
                    ResultsLabel.Content = "SMTP Settings are Invalid";
                    ResultsLabel.Foreground = Brushes.Red;
                }
            }
        }

        private async void SendTestEmail()
        {
            try
            {
                new GmailHelper().Send(emailTestBox.Text, "Test Email From Lilypad", "Testing email...", null);
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
            EmailTestButton.Background = Brushes.YellowGreen;
            EmailTestResultText.Content = "Sent!";

            await Task.Delay(500);

            EmailTestButton.Background = Brushes.Orange;
            EmailTestResultText.Content = "Test";
        }
        private void TestEmailAddrClick(object sender, EventArgs e)
        {
            SendTestEmail();
        }
    }
}
