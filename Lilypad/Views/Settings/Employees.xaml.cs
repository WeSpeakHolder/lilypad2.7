﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.ViewModel;
using Lilypad.Model;
using Ookii.Dialogs.Wpf;

namespace Lilypad.Views.Settings
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Employees : UserControl
    {
        public Employees()
        {
            InitializeComponent();
            EmployeesViewModel evm = new EmployeesViewModel();
            if (!evm.CanEdit) { myprofile = true; }
            else { myprofile = false; }
            this.DataContext = evm;
            EditorPanel.Opacity = 0.0;
        }

        public Employees(bool myProfile)
        {
            InitializeComponent();
            myprofile = true;
            adorner.Header = "My Profile";
            EditorPanel.Opacity = 0.0;
        }

        private bool myprofile;
        public bool MyProfile
        {
            get { return myprofile; }
            set { myprofile = value; }
        }

        void nt_Click(object sender, EventArgs e)
        {
            EmployeesViewModel evm = (EmployeesViewModel)this.DataContext;

            var input = (DevExpress.Xpf.LayoutControl.Tile)sender;
            frogger selectedFrogger = (frogger)input.Tag;

            evm.SelectedFrogger = selectedFrogger;
            SelectedFrogTile.Tag = selectedFrogger;

            if (!string.IsNullOrEmpty(selectedFrogger.PINCode.ToString()))
            {
                PINBox.Password = selectedFrogger.PINCode.ToString();
            }

            if(string.IsNullOrEmpty(selectedFrogger.Notes))
            {
                ThemeBox.SelectedValue = null;
            }

            Animations.DownwardSlide(EditorPanel, TimeSpan.FromMilliseconds(300));
        }


        private void LoadFroggerTiles()
        {

            SolidColorBrush newBrush = (SolidColorBrush) FindResource("AccentColorBrush");
            MainViewModel mvm = (MainViewModel) App.Current.MainWindow.DataContext;
            List<frogger> FroggerList = new List<frogger>();
            frogger selectedFrogger;

            if (MyProfile)
            {
                using (Entities ctx = new Entities())
                {
                    selectedFrogger =
                        ctx.froggers.Where(x => x.FrogID == mvm.LoggedInFrogger.FrogID).FirstOrDefault();
                }
                DevExpress.Xpf.LayoutControl.Tile nt = new DevExpress.Xpf.LayoutControl.Tile();
                nt.Header = selectedFrogger.FrogFirstName;
                nt.Tag = selectedFrogger;
                nt.Margin = new Thickness(5, 2, 0, 2);
                nt.Click += nt_Click;

                if (File.Exists(selectedFrogger.FrogImagePath))
                {
                    try
                    {
                        BitmapImage newImg = new BitmapImage(new Uri(selectedFrogger.FrogImagePath, UriKind.Absolute));
                        ImageBrush nb = new ImageBrush(newImg);
                        nt.Background = nb;
                    }
                    catch (Exception ex)
                    {
                        nt.Background = newBrush;
                    }
                }
                else
                {
                    nt.Background = newBrush;
                }

                nt.Style = (Style) FindResource("FrogTile");
                TilesSP.Children.Add(nt);

                EmployeesViewModel evm = (EmployeesViewModel) this.DataContext;

                evm.SelectedFrogger = selectedFrogger;
                SelectedFrogTile.Tag = selectedFrogger;

                if (!string.IsNullOrEmpty(selectedFrogger.PINCode.ToString()))
                {
                    PINBox.Password = selectedFrogger.PINCode.ToString();
                }

                if (string.IsNullOrEmpty(selectedFrogger.Notes))
                {
                    ThemeBox.SelectedValue = null;
                }

                Animations.DownwardSlide(EditorPanel, TimeSpan.FromMilliseconds(300));
            }
            else
            {
                using (Entities ctx = new Entities())
                {
                    FroggerList = ctx.froggers.Where(x => x.FrogID != 0).ToList();
                }

                foreach (frogger frog in FroggerList)
                {
                    DevExpress.Xpf.LayoutControl.Tile nt = new DevExpress.Xpf.LayoutControl.Tile();
                    nt.Header = frog.FrogFirstName;
                    nt.Tag = frog;
                    nt.Margin = new Thickness(5, 2, 0, 2);
                    nt.Click += nt_Click;

                    if (File.Exists(frog.FrogImagePath))
                    {
                        try
                        {
                            BitmapImage newImg = new BitmapImage(new Uri(frog.FrogImagePath, UriKind.Absolute));
                            ImageBrush nb = new ImageBrush(newImg);
                            nt.Background = nb;
                        }
                        catch (Exception ex)
                        {
                            nt.Background = newBrush;
                        }
                    }
                    else
                    {
                        nt.Background = newBrush;
                    }

                    nt.Style = (Style) FindResource("FrogTile");
                    TilesSP.Children.Add(nt);
                }
            }
        }

        private void SettingChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void PageLoaded(object sender, RoutedEventArgs e)
        {
            LoadFroggerTiles();
        }

        private void SaveChangesToSelected(object sender, EventArgs e)
        {
            MissionControlViewModel mcvm = (MissionControlViewModel)this.TryFindParent<MahApps.Metro.Controls.TransitioningContentControl>().DataContext;
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            EmployeesViewModel evm = (EmployeesViewModel)this.DataContext;

            frogger thisFrog = evm.SelectedFrogger;
            frogger toEdit = new frogger();

            if (thisFrog.FrogID == 0)
            {
                using (Entities ctx = new Entities())
                {
                    toEdit.FrogAddress = thisFrog.FrogAddress;
                    toEdit.FrogFirstName = thisFrog.FrogFirstName;
                    toEdit.FrogHireDate = thisFrog.FrogHireDate;
                    toEdit.FrogImagePath = thisFrog.FrogImagePath;
                    toEdit.FrogInitials = thisFrog.FrogInitials;
                    toEdit.FrogLastDate = thisFrog.FrogLastDate;
                    toEdit.FrogLastName = thisFrog.FrogLastName;
                    toEdit.FrogPhone = thisFrog.FrogPhone;
                    toEdit.FrogTitle = thisFrog.FrogTitle;
                    toEdit.IsManager = thisFrog.IsManager;
                    toEdit.IsStoreOwner = thisFrog.IsStoreOwner;
                    toEdit.Notes = thisFrog.Notes;
                    toEdit.PINCode = PINBox.Password.ToString();
                    toEdit.RequiresPINCode = thisFrog.RequiresPINCode;
                    toEdit.CurrentlyEmployed = thisFrog.CurrentlyEmployed;
                    toEdit.Birthday = thisFrog.Birthday;
                    toEdit.EmergencyContact = thisFrog.EmergencyContact;
                    ctx.froggers.Add(toEdit);
                    try
                    {
                        ctx.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        new Error(ex);
                    }
                }

                mcvm.OpenSettings_Employees();
            }
            else
            {

                using (Entities ctx = new Entities())
                {
                    toEdit = (from f in ctx.froggers where f.FrogID == thisFrog.FrogID select f).FirstOrDefault();
                    toEdit.FrogAddress = thisFrog.FrogAddress;
                    toEdit.FrogFirstName = thisFrog.FrogFirstName;
                    toEdit.FrogHireDate = thisFrog.FrogHireDate;
                    toEdit.FrogImagePath = thisFrog.FrogImagePath;
                    toEdit.FrogInitials = thisFrog.FrogInitials;
                    toEdit.FrogLastDate = thisFrog.FrogLastDate;
                    toEdit.FrogLastName = thisFrog.FrogLastName;
                    toEdit.FrogPhone = thisFrog.FrogPhone;
                    toEdit.FrogTitle = thisFrog.FrogTitle;
                    toEdit.IsManager = thisFrog.IsManager;
                    toEdit.IsStoreOwner = thisFrog.IsStoreOwner;
                    toEdit.Notes = thisFrog.Notes;
                    toEdit.PINCode = PINBox.Password.ToString();
                    toEdit.RequiresPINCode = thisFrog.RequiresPINCode;
                    toEdit.CurrentlyEmployed = thisFrog.CurrentlyEmployed;
                    toEdit.Birthday = thisFrog.Birthday;
                    toEdit.EmergencyContact = thisFrog.EmergencyContact;
                    ctx.SaveChanges();
                }

                if (mvm.LoggedInFrogger.FrogID == evm.SelectedFrogger.FrogID)
                {
                    mvm.LoggedInFrogger = new frogger() { FrogFirstName = "Lilypad" };
                    mvm.LoggedInFrogger = toEdit;
                    Utilities.LoadTheme(evm.SelectedFrogger.Notes.ToString());
                }

                if (MyProfile)
                {
                    mcvm.OpenSettings_MyProfile();
                }
                else
                {
                    mcvm.OpenSettings_Employees();
                }
            }

            Toaster.SavedChanges();
        }

        private void EditImageClicked(object sender, EventArgs e)
        {
            EmployeesViewModel evm = (EmployeesViewModel)this.DataContext;
            Utilities.ShowImageEdit(evm);
        }

        private void AddNewClick(object sender, EventArgs e)
        {
            frogger frog = new frogger();
            frog.FrogID = 0;
            frog.FrogFirstName = "First";
            frog.FrogLastName = "Last";
            frog.FrogTitle = "Title";
            frog.FrogInitials = "";
            frog.FrogLastDate = DateTime.MinValue;
            frog.FrogPhone = "";
            frog.FrogImagePath = "";
            frog.FrogHireDate = DateTime.Now;
            frog.FrogAddress = "";
            frog.EmergencyContact = "";
            frog.Birthday = DateTime.MinValue;
            frog.RequiresPINCode = false;
            frog.PINCode = "";
            frog.Notes = "";
            frog.CurrentlyEmployed = true;

            DevExpress.Xpf.LayoutControl.Tile nt = new DevExpress.Xpf.LayoutControl.Tile();
            nt.Header = "New User";
            nt.Tag = frog;
            nt.Margin = new Thickness(5, 2, 0, 2);
            nt.Click += nt_Click;

            if (File.Exists(frog.FrogImagePath))
            {
                BitmapImage newImg = new BitmapImage(new Uri(frog.FrogImagePath, UriKind.Absolute));
                ImageBrush nb = new ImageBrush(newImg);
                nt.Background = nb;
            }
            else
            {
                SolidColorBrush newBrush = (SolidColorBrush)FindResource("AccentColorBrush");
                nt.Background = newBrush;
            }

            nt.Style = (Style)FindResource("FrogTile");
            TilesSP.Children.Insert(0, nt);
           // TilesSP.Children.Add(nt);
        }
    }
}
