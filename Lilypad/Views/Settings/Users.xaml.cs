﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Lilypad.Properties;
using MahApps.Metro;
using Ookii.Dialogs.Wpf;

namespace Lilypad.Views.Settings
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Users : UserControl
    {
        public Users()
        {
            InitializeComponent();
        }


        private void EditPathClicked(object sender, EventArgs e)
        {
            StackPanel sp = Utilities.FindParentByType<StackPanel>(((DependencyObject)sender));
            TextBox tb = (TextBox)sp.FindChildren<TextBox>(true).First();
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.SelectedPath))
            {
                tb.Text = dialog.SelectedPath.ToString();
            }
        }
    }
}
