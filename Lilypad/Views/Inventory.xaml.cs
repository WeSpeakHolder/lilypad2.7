﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using Lilypad.Model;
using Lilypad.ViewModel;
using MahApps.Metro.Controls.Dialogs;

namespace Lilypad.View
{
    /// <summary>
    ///     Interaction logic for Inventory.xaml
    /// </summary>
    public partial class Inventory : UserControl
    {
        public ObservableCollection<alphaitem> SelectedQAItems = new ObservableCollection<alphaitem>();

        public Inventory()
        {
            InitializeComponent();
        }

        private void InventoryLoaded(object sender, RoutedEventArgs e)
        {
            LoadCustomersList();
        }

        private void LoadCustomersList()
        {
            using (var context = new Entities())
            {
                var found = (from q in context.hopperorders
                             orderby q.customer.LastName ascending
                             select new { Value = q.ReceiptNum, q.customer.LastName, q.customer.FirstName, q.ReceiptNum }).ToList();
                var stock =
                    (from k in context.alphaitems select new { Value = k.Mill_Name }).Distinct()
                        .OrderBy(x => x.Value)
                        .ToList();

                var cc =
                    found.AsEnumerable()
                        .Select(
                            x =>
                                new
                                {
                                    Value = x.ReceiptNum,
                                    Name = x.LastName + ", " + x.FirstName + " #" + x.ReceiptNum.ToString() + ""
                                })
                        .ToList();

                ReceiptLookup.ItemsSource = cc;
                ReceiptLookup.ValueMember = "Value";
                ReceiptLookup.DisplayMember = "Name";

                StockMfgLookup.ItemsSource = stock;
                StockMfgLookup.ValueMember = "Value";
                StockMfgLookup.DisplayMember = "Value";
            }
        }
        private void view_ShowingEditor(object sender, DevExpress.Xpf.Grid.ShowingEditorEventArgs e)
        {
            using (var ctx = new Entities())
            {
                alphacurrentorder thisOrd =
(from q in ctx.alphacurrentorders where q.IDUnique == ((Lilypad.Model.alphacurrentorder)e.Row).IDUnique select q).First();
                if (((Lilypad.Model.alphacurrentorder)e.Row).IsArchiveable == true)
                {
                    thisOrd.IsArchiveable = false;
                }
                else
                {
                    thisOrd.IsArchiveable = true;
                }
                ctx.SaveChanges();
            }

            if (e.Column.FieldName != "IsArchiveable")
            {
                if ((e.Column.FieldName == "StatusLight") || (e.Column.FieldName == "CustLastName") || (e.Column.FieldName == "Status") || (e.Column.FieldName == "IDItemCodeAlpha") || (e.Column.FieldName == "Quantity") || (e.Column.FieldName == "IDStockAlpha") || (e.Column.FieldName == "Color") || (e.Column.FieldName == "Size") || (e.Column.FieldName == "order.BoxNum") || (e.Column.FieldName == "UnitPrice") || (e.Column.FieldName == "BoxNum") || (e.Column.FieldName == "SalePrice") || (e.Column.FieldName == "SaleExpiry") || (e.Column.FieldName == "DecorationType")) { e.Cancel = true; }
                else
                {
                    //if (((Lilypad.Model.alphacurrentorder)e.Row).IsArchiveable == true) { ((Lilypad.Model.alphacurrentorder)e.Row).IsArchiveable = false; }
                    //else { ((Lilypad.Model.alphacurrentorder)e.Row).IsArchiveable = true; }
                    //((Lilypad.Model.alphacurrentorder)e.Row).IsArchiveable = true;
                    e.Cancel = false;
                }
            }
        }

        private void grid_GroupRowExpanding(object sender, RowAllowEventArgs e)
        {
        }

        private void GridSelectedItemChanged(object sender, SelectedItemChangedEventArgs e)
        {
        }

        private void SelectItem(object sender, MouseButtonEventArgs e)
        {
        }

        private void GridViewRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
        }


        private void PrintGridClicked(object sender, EventArgs e)
        {
            var preview = new DocumentPreviewWindow();
            var link = new PrintableControlLink(gridView);
            StatusColumn.Visible = false;
            var model = new LinkPreviewModel(link);
            preview.Model = model;
            link.Landscape = true;
            link.CreateDocument(true);
            ThemeManager.SetThemeName(preview, Theme.MetropolisLightName);
            preview.ShowDialog();
            StatusColumn.Visible = true;
        }

        private void ViewArchived(object sender, EventArgs e)
        {
            ((MainViewModel)App.Current.MainWindow.DataContext).GoInventoryOrders();
            // UI.Navigate("InventoryOrders");
        }

        private void AddItemClicked(object sender, EventArgs e)
        {
            CustomerAddGrid.IsEnabled = false;
            if (NewItemPanel.Opacity == 1.0)
            {
                NewItemPanel.UpwardSlide(TimeSpan.FromMilliseconds(200));
                NewItemPanel.OpacityAnimation(0, TimeSpan.FromMilliseconds(200));
                PlusSignRect.Rotate(TimeSpan.FromMilliseconds(200), 90);
                ResetAddGrid();
            }
            else
            {
                NewItemPanel.DownwardSlide(TimeSpan.FromMilliseconds(200));
                NewItemPanel.OpacityAnimation(1.0, TimeSpan.FromMilliseconds(200));
                PlusSignRect.Rotate(TimeSpan.FromMilliseconds(200), -90);
            }
        }

        private async void ResetAddGrid()
        {
            await Task.Delay(200);
            CustomerAddGrid.SlideTo(0, 50, TimeSpan.FromSeconds(0.3));
            CustomerAddGrid.OpacityAnimation(0, TimeSpan.FromSeconds(0.2));
            QuestionPanel.SlideTo(-50, 0, TimeSpan.FromSeconds(0.5));
            ReceiptLookupSP.Opacity = 0.0;
            FirstLastSP.Opacity = 0.0;
            QuestionPanel.OpacityAnimation(1, TimeSpan.FromSeconds(0.3));
            FirstLastSP.OpacityAnimation(0, TimeSpan.FromSeconds(0.5));
            ReceiptLookupSP.OpacityAnimation(0, TimeSpan.FromSeconds(0.5));
            ReceiptLookup.EditValueChanged -= ReceiptChanged;
            StockMfgLookup.EditValueChanged -= BrandChanged;
            StockIDLookup.EditValueChanged -= StockIDChanged;
            ColorsLookup.EditValueChanged -= ColorChanged;
            SizeLookup.EditValueChanged -= SizeChanged;
            QtyEditor.EditValueChanged -= QtyChanged;
            ReceiptLookup.SelectedIndex = -1;
            FirstNameEditor.Text = string.Empty;
            LastNameEditor.Text = string.Empty;
            StockMfgLookup.Visibility = Visibility.Hidden;
            StockMfgLookup.SelectedIndex = -1;
            StockIDLookup.Visibility = Visibility.Hidden;
            StockIDLookup.SelectedIndex = -1;
            ColorsLookup.Visibility = Visibility.Hidden;
            ColorsLookup.SelectedIndex = -1;
            SizeLookup.Visibility = Visibility.Hidden;
            SizeLookup.SelectedIndex = -1;
            QtyEditor.Visibility = Visibility.Hidden;
            QtyEditor.Text = string.Empty;
            AddButtonCustomer.Visibility = Visibility.Hidden;
            ReceiptLookup.EditValueChanged += ReceiptChanged;
            StockMfgLookup.EditValueChanged += BrandChanged;
            StockIDLookup.EditValueChanged += StockIDChanged;
            ColorsLookup.EditValueChanged += ColorChanged;
            SizeLookup.EditValueChanged += SizeChanged;
            QtyEditor.EditValueChanged += QtyChanged;
        }

        private async void ArchiveOrderClicked(object sender, EventArgs e)
        {
            var result = await Utilities.ShowMetroYNDialog("Confirm Archive", "Are you sure you want to archive this order and start a new one?");
            if (result == MessageDialogResult.Affirmative)
            {
                ArchiveThisOrder();
                MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
                mvm.GoInventoryCommand.Execute(null);
            }
        }

        private void ArchiveThisOrder()
        {
            try
            {
                var mvm = (MainViewModel)Application.Current.MainWindow.DataContext;
                var ivm = (InventoryViewModel)DataContext;

                var newOrderID = Utilities.GenerateAlphaPONumber();

                using (var ctx = new Entities())
                {
                    var newOrd = new alphaorder();
                    newOrd.IDPO = newOrderID;
                    newOrd.IDAlpha = "";
                    newOrd.Status = "Ordered " + DateTime.Now.ToString("M/d") + " - Processing";
                    newOrd.TotalItems = ivm.TotalItemCount;
                    newOrd.TotalCost = ivm.EstimatedPrice;
                    newOrd.OrderDate = DateTime.Now;
                    newOrd.ShipDate = DateTime.MinValue;
                    newOrd.ReceivedDate = DateTime.MinValue;
                    newOrd.Archived = false;
                    newOrd.TrackingNum = "";
                    newOrd.OrderFrog = mvm.LoggedInFrogger.FrogID;
                    newOrd.ReceivedFrog = mvm.LoggedInFrogger.FrogID;

                    ctx.alphaorders.Add(newOrd);
                    ctx.SaveChanges();
                }

                using (var ctx = new Entities())
                {
                    var allitems = ctx.alphacurrentorders.ToList();
                    foreach (var item in allitems)
                    {
                        if (item.IsArchiveable == true)
                        {
                            var entry = new alphaentries1();
                            entry.ReceiptNum = item.ReceiptNum;
                            entry.CustFirstName = item.CustFirstName;
                            entry.CustLastName = item.CustLastName;
                            entry.BoxNum = item.BoxNum;
                            entry.IDPO = newOrderID;
                            entry.IDAlpha = item.IDStockAlpha.LimitTo(100);
                            entry.IDItemCodeAlpha = item.IDItemCodeAlpha.LimitTo(100);
                            entry.IDStockAlpha = item.IDStockAlpha.LimitTo(100);
                            entry.Quantity = item.Quantity;
                            entry.ItemDescription = item.ItemDescription;
                            entry.Color = item.Color;
                            entry.Size = item.Size;
                            entry.Status = item.Status;
                            entry.LineCost = item.LineCost;
                            entry.AddedDate = item.AddedDate;
                            ctx.alphaentries1.Add(entry);
                            alphacurrentorder thisOrd =
     (from q in ctx.alphacurrentorders where q.IDUnique == item.IDUnique select q).First();
                            thisOrd.IsArchiveable = item.IsArchiveable;
                        }
                    }
                    ctx.SaveChanges();
                }

                using (var ctx = new Entities())
                {
                    var allitems = ctx.alphacurrentorders.ToList();
                    foreach (var item in allitems)
                    {
                        if (item.IsArchiveable == true)
                        {
                            ctx.alphacurrentorders.Remove(item);
                        }
                    }
                    ctx.SaveChanges();
                }

                ivm.CurrentOrderItems.Clear();
                ivm.CurrentOrderItemsObservable.Clear();
                //    UI.Navigate("Inventory");
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                new Error(ex);
                foreach (System.Data.Entity.Validation.DbEntityValidationResult err in ex.EntityValidationErrors)
                {
                    string ree = err.Entry + System.Environment.NewLine + err.ValidationErrors.ToString();
                    Utilities.ShowSimpleDialog("Entity Validation Error", ree);
                }

            }
        }

        private void CustomerAddClick(object sender, EventArgs e)
        {
            AnimateQuestionAway();
            ((InventoryViewModel)DataContext).NewItemForCustomer = true;
            CustomerAddGrid.SlideTo(50, 0, TimeSpan.FromSeconds(0.5));
            CustomerAddGrid.OpacityAnimation(1, TimeSpan.FromMilliseconds(350));
            ReceiptLookupSP.OpacityAnimation(1, TimeSpan.FromSeconds(0.5));
            CustomerAddGrid.IsEnabled = true;
            ReceiptLookupSP.IsEnabled = true;
            FirstLastSP.IsEnabled = false;
        }

        private void CustomAddClick(object sender, EventArgs e)
        {
            AnimateQuestionAway();
            ((InventoryViewModel)DataContext).NewItemForCustomer = false;
            CustomerAddGrid.SlideTo(50, 0, TimeSpan.FromSeconds(0.5));
            CustomerAddGrid.OpacityAnimation(1, TimeSpan.FromMilliseconds(350));
            FirstLastSP.OpacityAnimation(1, TimeSpan.FromSeconds(0.5));
            CustomerAddGrid.IsEnabled = true;
            ReceiptLookupSP.IsEnabled = false;
            FirstLastSP.IsEnabled = true;
            StockMfgLookup.Visibility = Visibility.Visible;
        }

        private void AnimateQuestionAway()
        {
            QuestionPanel.LeftSlide(TimeSpan.FromSeconds(1));
            QuestionPanel.OpacityAnimation(0, TimeSpan.FromMilliseconds(250));
        }

        private void RemoveItem()
        {
            var t = grid.SelectedItems;
            List<int> deleteUniquiID = new List<int>();

            foreach (var item in t)
            {
                var s = (alphacurrentorder)item;
                deleteUniquiID.Add(s.IDUnique);
            }


            foreach (var item in deleteUniquiID)
            {
                var vm = (InventoryViewModel)DataContext;

                using (var ctx = new Entities())
                {
                    var ord = (from k in ctx.alphacurrentorders where k.IDUnique == item select k).FirstOrDefault();
                    vm.RemoveItemFromOrder(ord);


                    var cc = new alphacurrentorder();
                    cc = ctx.alphacurrentorders.First(x => x.IDUnique == ord.IDUnique);
                    ctx.alphacurrentorders.Remove(cc);
                    ctx.SaveChanges();
                }
            }


            //foreach (var rowH in grid.GetSelectedRowHandles())
            //{

            //    var ord = (alphacurrentorder)grid.GetRow(rowH);
            //    var vm = (InventoryViewModel)DataContext;
            //    vm.RemoveItemFromOrder(ord);

            //    using (var ctx = new Entities())
            //    {
            //        var cc = new alphacurrentorder();
            //        cc = ctx.alphacurrentorders.First(x => x.IDUnique == ord.IDUnique);
            //        ctx.alphacurrentorders.Remove(cc);
            //        ctx.SaveChanges();
            //    }

            //}
            grid.RefreshData();
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            mvm.GoInventoryCommand.Execute(null);

        }

        private async void DeleteItem()
        {
            var result = await Utilities.ShowMetroYNDialog("Confirm Deletion", "Are you sure you want to delete the selected items?");
            if (result == MessageDialogResult.Affirmative)
            {
                RemoveItem();
            }
        }

        private void DeleteItemClicked(object sender, EventArgs e)
        {
            DeleteItem();
        }

        private void AddToOrderButtonClicked(object sender, EventArgs e)
        {
            var ivm = (InventoryViewModel)DataContext;
            var newOrd = new alphacurrentorder();
            int recept = Convert.ToInt16(ReceiptLookup.EditValue);
            int qty = Convert.ToInt16(QtyEditor.Text);
            var mill = StockMfgLookup.EditValue.ToString();
            var style = StockIDLookup.EditValue.ToString();
            var color = ColorsLookup.EditValue.ToString();
            var size = SizeLookup.EditValue.ToString();

            if (ivm.NewItemForCustomer)
            {
                using (var ctx = new Entities())
                {
                    var ord = (from k in ctx.hopperorders where k.ReceiptNum == recept select k).FirstOrDefault();
                    var c = (from f in ctx.customers where f.CustomerID == ord.CustomerID select f).FirstOrDefault();
                    var i = (from g in ctx.alphaitems
                             where
                                 g.Mill_Name == mill && g.Style_Code == style && g.Color_Name == color && g.Size_Name == size
                             select g).FirstOrDefault();

                    newOrd.AddedDate = DateTime.Now;
                    newOrd.BoxNum = ord.BoxNum;
                    newOrd.Color = ColorsLookup.EditValue.ToString();
                    newOrd.CustFirstName = c.FirstName;
                    newOrd.CustLastName = c.LastName;
                    newOrd.IDItemCodeAlpha = i.Item_Number;
                    newOrd.IDStockAlpha = i.Style_Number;
                    newOrd.IsIDVerified = false;
                    newOrd.IsInStockClosest = false;
                    newOrd.IsInStockElsewhere = false;
                    newOrd.ItemDescription = i.Description;
                    newOrd.LineCost = i.P7_Pricing * qty;
                    newOrd.Quantity = qty;
                    newOrd.ReceiptNum = ord.ReceiptNum;
                    newOrd.SaleExpiry = DateTime.MinValue;
                    newOrd.SalePrice = i.P7_Pricing;
                    newOrd.Size = i.Size_Name;
                    newOrd.Status = "Pending Order";
                    newOrd.UnitPrice = i.P7_Pricing;
                    newOrd.IsArchiveable = false;

                    ivm.AddItemToOrder(newOrd);
                    ctx.alphacurrentorders.Add(newOrd);
                    ctx.SaveChanges();
                }
            }
            else
            {
                using (var ctx = new Entities())
                {
                    var i = (from g in ctx.alphaitems
                             where
                                 g.Mill_Name == mill && g.Style_Code == style && g.Color_Name == color && g.Size_Name == size
                             select g).FirstOrDefault();

                    newOrd.AddedDate = DateTime.Now;
                    newOrd.BoxNum = "";
                    newOrd.Color = ColorsLookup.EditValue.ToString();
                    newOrd.CustFirstName = FirstNameEditor.Text;
                    newOrd.CustLastName = LastNameEditor.Text;
                    newOrd.IDItemCodeAlpha = i.Item_Number;
                    newOrd.IDStockAlpha = i.Style_Number;
                    newOrd.IsIDVerified = false;
                    newOrd.IsInStockClosest = false;
                    newOrd.IsInStockElsewhere = false;
                    newOrd.ItemDescription = i.Description;
                    newOrd.LineCost = i.P7_Pricing * qty;
                    newOrd.Quantity = qty;
                    newOrd.ReceiptNum = 0;
                    newOrd.SaleExpiry = DateTime.MinValue;
                    newOrd.SalePrice = i.P7_Pricing;
                    newOrd.Size = i.Size_Name;
                    newOrd.Status = "Pending Order";
                    newOrd.UnitPrice = i.P7_Pricing;
                    newOrd.IsArchiveable = false;

                    ivm.AddItemToOrder(newOrd);
                    ctx.alphacurrentorders.Add(newOrd);
                    ctx.SaveChanges();
                }
            }
        }

        private void BrandChanged(object sender, EditValueChangedEventArgs e)
        {
            StockIDLookup.Visibility = Visibility.Visible;
            var mill = StockMfgLookup.EditValue.ToString();
            if (e.NewValue != null)
            {
                using (var ctx = new Entities())
                {
                    StockIDLookup.ItemsSource =
                        (from k in ctx.alphaitems where k.Mill_Name == mill select new { Value = k.Style_Code }).Distinct()
                            .OrderBy(x => x.Value)
                            .ToList();
                    StockIDLookup.ValueMember = "Value";
                    StockIDLookup.DisplayMember = "Value";
                }
            }
        }

        private void StockIDChanged(object sender, EditValueChangedEventArgs e)
        {
            ColorsLookup.Visibility = Visibility.Visible;
            var mill = StockMfgLookup.EditValue.ToString();
            var id = StockIDLookup.EditValue.ToString();

            var ti = new CultureInfo("en-US", false).TextInfo;

            if (e.NewValue != null)
            {
                using (var ctx = new Entities())
                {
                    var cList =
                        (from k in ctx.alphaitems
                         where k.Mill_Name == mill && k.Style_Code == id
                         select new { Value = k.Color_Name }).Distinct().OrderBy(x => x.Value).ToList();
                    var oList = cList.ConvertAll(d => ti.ToTitleCase(d.Value.ToLower()));
                    ColorsLookup.ItemsSource = oList;
                }
            }
        }

        private void ColorChanged(object sender, EditValueChangedEventArgs e)
        {
            SizeLookup.Visibility = Visibility.Visible;
            var color = ColorsLookup.EditValue.ToString();
            var mill = StockMfgLookup.EditValue.ToString();
            var id = StockIDLookup.EditValue.ToString();

            IList<string> unSorted;
            IList<string> Sorted;

            if (e.NewValue != null)
            {
                using (var ctx = new Entities())
                {
                    SelectedQAItems.Clear();

                    var items =
                        (from k in ctx.alphaitems
                         where k.Mill_Name == mill && k.Style_Code == id && k.Color_Name == color
                         select k).ToList();
                    foreach (var item in items)
                    {
                        SelectedQAItems.Add(item);
                    }
                    unSorted = (from g in items select g.Size_Name).ToArray<string>();

                    Sorted =
                        Utilities.GarmentSizeArray.Select(x => unSorted.Contains(x) ? x : "-1")
                            .Where(x => x != "-1")
                            .ToList();
                    SizeLookup.ItemsSource = Sorted;
                }
            }
        }

        private void ReceiptChanged(object sender, EditValueChangedEventArgs e)
        {
            StockMfgLookup.Visibility = Visibility.Visible;
        }

        private void QtyChanged(object sender, EditValueChangedEventArgs e)
        {
            AddButtonCustomer.Visibility = Visibility.Visible;
        }

        private void SizeChanged(object sender, EditValueChangedEventArgs e)
        {
            QtyEditor.Visibility = Visibility.Visible;
        }



        public void CheckEdit_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            FrameworkElement editor = (FrameworkElement)sender;
            GridGroupValueData data = (GridGroupValueData)editor.DataContext;
            GridControl grid = ((TableView)data.View).Grid;
            int currentGroupRowHandle = data.RowData.RowHandle.Value;
            int receiptnum = ((Lilypad.Model.alphacurrentorder)data.RowData.Row).ReceiptNum;

            if (e.NewValue != null)
            {
                grid.BeginSelection();
                SetIsGroupSelected(grid, currentGroupRowHandle, (bool)e.NewValue);
                grid.EndSelection();
            }
        }
        void SetIsGroupSelected(GridControl grid, int rowHandle, bool value)
        {

            int childrenCount = grid.GetChildRowCount(rowHandle);
            for (int i = 0; i < childrenCount; i++)
            {
                int childrenHandle = grid.GetChildRowHandle(rowHandle, i);

                if (grid.IsGroupRowHandle(childrenHandle))
                    SetIsGroupSelected(grid, childrenHandle, value);
                else
                    grid.SetCellValue(childrenHandle, SelectAllColumn.ColumnFieldName, value);
            }
        }


        private void SelectionView_SelectionChanged(object sender, DevExpress.Xpf.Grid.GridSelectionChangedEventArgs e)
        {
            using (var ctx = new Entities())
            {
                List<alphacurrentorder> thisOrds = (from q in ctx.alphacurrentorders select q).ToList();
                foreach (var items in thisOrds)
                {
                    items.IsArchiveable = false;
                }
                ctx.SaveChanges();
                if (((DevExpress.Xpf.Grid.DataViewBase)sender).SelectedRows.Count > 0)
                {

                    foreach (var item in ((DevExpress.Xpf.Grid.DataViewBase)sender).SelectedRows)
                    {
                        List<alphacurrentorder> thisOrd = (from q in ctx.alphacurrentorders where q.IDUnique == ((Lilypad.Model.alphacurrentorder)item).IDUnique select q).ToList();
                        foreach (var items in thisOrd)
                        {
                            items.IsArchiveable = true;
                        }
                    }
                }

                ctx.SaveChanges();
            }
        }

    }
}