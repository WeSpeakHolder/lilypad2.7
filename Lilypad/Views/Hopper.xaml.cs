﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Data.Filtering;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using DevExpress.XtraRichEdit;
using DevExpress.Office.Utils;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Printing;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.Xpf.WindowsUI.Navigation;
using Lilypad.Model;
using Lilypad.ViewModel;

using System.Data.Entity;
using System.Collections.ObjectModel;
using System.Deployment.Application;


namespace Lilypad.Views
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Hopper : UserControl, INavigationAware
    {
        //ObservableCollection<status> StatusCollection1;
        public Hopper()
        {
            //   try
            {
                InitializeComponent();
                IsFullyLoaded = false;
            }
            //   catch (Exception ex)
            {
                //   new Error(ex);
            }

            //expanderContainer.IsEnabled = false;

        }

        public void UnloadChangedEvents()
        {
            foreach (BaseEdit editor in ChangesPanel.FindChildren<BaseEdit>())
            {
                editor.EditValueChanged -= ValueChanged;
            }
        }

        public void ReloadChangedEvents()
        {
            foreach (BaseEdit editor in ChangesPanel.FindChildren<BaseEdit>())
            {
                editor.EditValueChanged += ValueChanged;
            }
        }

        public bool HasCurrentSelection;

        public bool IsFullyLoaded;

        private void hopperLoaded(object sender, RoutedEventArgs e)
        {

            using (var ctx = new Entities())
            {
                ctx.froggers.Load();
                DesignerEditor.ItemsSource = ctx.froggers.Local;
                var itemListDesignerEditor = DesignerEditor.ItemsSource as ObservableCollection<frogger>;
                DesignerEditor.ItemsSource = null;
                DesignerEditor.ItemsSource = itemListDesignerEditor;

                ctx.printprocesses.Load();
                ProcessEditor.ItemsSource = ctx.printprocesses.Local;
                var itemListProcessEditor = ProcessEditor.ItemsSource as ObservableCollection<printprocess>;
                ProcessEditor.ItemsSource = null;
                ProcessEditor.ItemsSource = itemListProcessEditor.OrderBy(x => x.ProcessOrderId).ToList();
            }
            UnloadChangedEvents();
            grid.ItemsSource =
                (FindResource("EntityCollectionViewSource") as DevExpress.Xpf.Core.ServerMode.EntityServerModeDataSource)
                    .Data;
            grid.SortInfo.Add(new DevExpress.Xpf.Grid.GridSortInfo("DueDate",
                System.ComponentModel.ListSortDirection.Ascending));
            grid.SortInfo.Add(new DevExpress.Xpf.Grid.GridSortInfo("Priority",
                System.ComponentModel.ListSortDirection.Descending));
            IsFullyLoaded = true;
            GridView.MoveFirstRow();
            GridView.Focus();
            //grid.SelectItem(0);
            ReloadChangedEvents();
            if (grid.SelectedItem != null)
            {
                ReloadRTFEditor();
                ReloadDesigns();
            }
        }

        private void ItemLoaded(object sender, RoutedEventArgs e)
        {
            if (HasSearch)
            {
                SearchBox.SearchText = ToSearch;
                HasSearch = false;
                ToSearch = "";
            }
        }

        void ComboBoxEdit_PopupOpening(object sender, DevExpress.Xpf.Editors.OpenPopupEventArgs e)
        {
            var list = ((ComboBoxEdit)sender).ItemsSource as ObservableCollection<status>;

            if (list != null)
            {
                status obj1 = list.Where(x => x.Status1 == "Completed").Select(x => x).FirstOrDefault();
                status obj2 = list.Where(x => x.Status1 == "Picked Up").Select(x => x).FirstOrDefault();
                status obj3 = list.Where(x => x.Status1 == "Awaiting Artwork").Select(x => x).FirstOrDefault();
                status obj4 = list.Where(x => x.Status1 == "Awaiting Art Approval").Select(x => x).FirstOrDefault();
                status obj5 = list.Where(x => x.Status1 == "Awaiting Inventory").Select(x => x).FirstOrDefault();
                status obj6 = list.Where(x => x.Status1 == "Working On Artwork").Select(x => x).FirstOrDefault();

                if (obj1 != null && obj2 != null && obj3 != null && obj4 != null && obj5 != null && obj6 != null)
                {
                    list.Remove(obj1);
                    list.Remove(obj2); list.Remove(obj3); list.Remove(obj4); list.Remove(obj5); list.Remove(obj6);
                    ((ComboBoxEdit)sender).ItemsSource = list.OrderBy(x => x.Status1).ToList();


                    if (this.DataContext.GetType() == typeof(HopperViewModel))
                    {
                        HopperViewModel thisVM = (HopperViewModel)this.DataContext;

                        if (thisVM.SelectedOrder.Status == null)
                            thisVM.SelectedOrder.Status = "Completed";
                    }
                }
            }
        }

        private void GridViewRowChanged(object sender, DevExpress.Xpf.Grid.FocusedRowChangedEventArgs e)
        {

        }

        public void ReloadRTFEditor()
        {
            string notespath = "";

            if (this.DataContext.GetType() == typeof (HopperViewModel))
            {
                HopperViewModel vm = (HopperViewModel) this.DataContext;
                if (vm.SelectedOrder != null)
                {
                    notespath = vm.SelectedOrder.JobFolderPath + "\\OrderNotes.Rtf";
                }

            }
            else if (this.DataContext.GetType() == typeof (HopperArchivedViewModel))
            {
                HopperArchivedViewModel vm = (HopperArchivedViewModel) this.DataContext;
                if (vm.SelectedOrder != null)
                {
                    notespath = vm.SelectedOrder.JobFolderPath + "\\OrderNotes.Rtf";
                }

            }


            if (System.IO.File.Exists(notespath))
            {
                try
                {
                    // RichEditor.LoadNewDocumentBackend(notespath);
                    RichEditor.RichEditorActual.LoadDocument(notespath);
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
            else
            {
                try
                {
                    RichEditor.RichEditorActual.CreateNewDocument();
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }

        private void TextboxKeydown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                HopperViewModel vm = (HopperViewModel) this.DataContext;
                string tagToadd = TagBox.Text.ToString();
                Controls.Tags.Tag newTag = new Controls.Tags.Tag(tagToadd);
                vm.TagCollection.Add(newTag);
                Keyboard.Focus(TagBox);
                TagBox.Clear();
                string output = "," + tagToadd;

                string currTags = vm.SelectedOrder.SearchTags.ToString();
                vm.SelectedOrder.SearchTags = currTags += output;
                vm.NeedsSave = true;
            }
        }

        private void TagBoxGotFocus(object sender, RoutedEventArgs e)
        {
            TagBox.Clear();
            Keyboard.Focus(TagBox);
        }

        private void TagBoxLostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TagBox.Text))
            {
                TagBox.Text = "Add a tag...";
            }
        }

        private void ReloadDesigns()
        {
            if (expanderContainer != null && expanderContainer.Children.Count > 0)
            {
                expanderContainer.Children.Clear();
            }
            List<designdetail> list = new List<designdetail>();

            if (this.DataContext.GetType() == typeof (HopperViewModel))
            {
                HopperViewModel thisVM = (HopperViewModel) this.DataContext;
                if (thisVM.SelectedOrder != null)
                {
                    using (Entities ctx = new Entities())
                    {
                        list =
                            (from k in ctx.designdetails where k.ReceiptNum == thisVM.SelectedOrder.ReceiptNum select k)
                                .ToList();
                    }
                }
            }
            else if (this.DataContext.GetType() == typeof (HopperArchivedViewModel))
            {
                HopperArchivedViewModel thisVM = (HopperArchivedViewModel) this.DataContext;
                if (thisVM.SelectedOrder != null)
                {
                    using (Entities ctx = new Entities())
                    {
                        list =
                            (from k in ctx.designdetails where k.ReceiptNum == thisVM.SelectedOrder.ReceiptNum select k)
                                .ToList();
                    }
                }
            }

            foreach (designdetail dt in list)
            {
                DesignSetupCardViewModel newVM = new DesignSetupCardViewModel();
                newVM.SelectedDesignDetail = dt;
                Controls.DesignCards.ViewCard dc = new Controls.DesignCards.ViewCard(newVM);
                dc.Margin = new Thickness(0, 0, 10, 0);
                Expander exp = new Expander();
                exp.Width = 315;
                exp.HeaderTemplate = (DataTemplate) this.FindResource("ExpTemplate");
                exp.Opacity = 1.0;
                exp.Tag = newVM;
                exp.Margin = new Thickness(0);
                exp.Header = "Design " + dt.DesignNum;
                exp.Content = dc;
                exp.IsExpanded = true;
                expanderContainer.Children.Add(exp);
            }

        }

        private void ValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (this.DataContext.GetType() == typeof (HopperViewModel))
            {
                HopperViewModel thisVM = (HopperViewModel) this.DataContext;
                if (thisVM.SelectedOrder != null)
                {
                    if (thisVM.SelectedOrder.Status == null)
                        thisVM.SelectedOrder.Status = "Completed";
                }
                thisVM.NeedsSave = true;
            }
            else if (this.DataContext.GetType() == typeof (HopperArchivedViewModel))
            {
                HopperArchivedViewModel thisVM = (HopperArchivedViewModel) this.DataContext;
                thisVM.NeedsSave = true;
            }
        }


        private void CollapseAllClicked(object sender, EventArgs e)
        {
            foreach (Expander exx in expanderContainer.Children)
            {
                exx.IsExpanded = false;
            }
        }

        private void ExpandAllClicked(object sender, EventArgs e)
        {
            foreach (Expander exx in expanderContainer.Children)
            {
                exx.IsExpanded = true;
            }
        }

        public string ToSearch;
        public bool HasSearch;


        public void NavigatedFrom(DevExpress.Xpf.WindowsUI.Navigation.NavigationEventArgs e)
        {

        }

        public void NavigatedTo(DevExpress.Xpf.WindowsUI.Navigation.NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                ToSearch = e.Parameter.ToString();
                HasSearch = true;
            }
            ReloadDesigns();
            ReloadRTFEditor();
        }

        public void NavigatingFrom(NavigatingEventArgs e)
        {

        }

        private void SourceSwitchClicked(object sender, EventArgs e)
        {
            if (this.DataContext.GetType() == typeof (HopperArchivedViewModel))
            {
                try
                {
                    var source =
                        (DevExpress.Xpf.Core.ServerMode.EntityServerModeDataSource)
                            FindResource("EntityCollectionViewSource");
                    grid.ItemsSource = source.Data;
                    CompletedTile.Visibility = System.Windows.Visibility.Visible;
                    PickedUpTile.Visibility = System.Windows.Visibility.Visible;
                    StatusColumn.SetValue(DevExpress.Xpf.Grid.GridColumn.FieldNameProperty, "Status");
                    ProcessColumn.SetValue(DevExpress.Xpf.Grid.GridColumn.FieldNameProperty, "PrintProcess");
                    PrintsColumn.SetValue(DevExpress.Xpf.Grid.GridColumn.VisibleProperty, true);
                    HopperViewModel newVM = new HopperViewModel();
                    this.DataContext = newVM;
                    ArchiveLabel.Content = "SWITCH TO ARCHIVED";
                    GridView.MoveFirstRow();
                    GridView.Focus();
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
            else
            {
                try
                {
                    var source =
                        (DevExpress.Xpf.Core.ServerMode.EntityServerModeDataSource)
                            FindResource("EntityCollectionArchivedViewSource");
                    grid.ItemsSource = source.Data;
                    CompletedTile.Visibility = System.Windows.Visibility.Collapsed;
                    PickedUpTile.Visibility = System.Windows.Visibility.Collapsed;
                    StatusColumn.SetValue(DevExpress.Xpf.Grid.GridColumn.FieldNameProperty, "status1.Status1");
                    ProcessColumn.SetValue(DevExpress.Xpf.Grid.GridColumn.FieldNameProperty, "printprocess1.ProcessName");
                    PrintsColumn.SetValue(DevExpress.Xpf.Grid.GridColumn.VisibleProperty, false);
                    HopperArchivedViewModel newVM = new HopperArchivedViewModel();
                    this.DataContext = newVM;
                    ArchiveLabel.Content = "SWITCH TO CURRENT";
                    GridView.MoveFirstRow();
                    GridView.Focus();
                    //   grid.SelectItem(0);
                }
                catch (Exception ex)
                {
                    new Error(ex);
                }
            }
        }

        private void PrintGridClicked(object sender, EventArgs e)
        {
            DocumentPreviewWindow preview = new DocumentPreviewWindow();
            PrintableControlLink link = new PrintableControlLink(GridView as DevExpress.Xpf.Printing.IPrintableControl);
            LinkPreviewModel model = new LinkPreviewModel(link);
            preview.Model = model;
            link.Landscape = true;
            link.CreateDocument(true);
            ThemeManager.SetThemeName(preview, Theme.MetropolisLightName);
            preview.ShowDialog();
        }

        private void MarkPickedUpClicked(object sender, EventArgs e)
        {
            MarkPickedUpConfirm();
        }

        private void MarkCompletedClicked(object sender, EventArgs e)
        {
            HopperViewModel thisVM = (HopperViewModel)this.DataContext;
            List<ultraprintorderdata> list = new List<ultraprintorderdata>();


            if (ApplicationDeployment.IsNetworkDeployed)
            {
                // --Hide UltraPrint Option code disable 12262019
                if (thisVM.SelectedOrder.PrintProcess == 4) //need to check this scenario
                {
                    using (Entities ctx = new Entities())
                    {
                        list =
                            (from k in ctx.ultraprintorderdatas
                             where
    k.CustFirstName.ToLower() == thisVM.SelectedOrder.CustFirst.ToLower() &&
    k.CustLastName.ToLower() == thisVM.SelectedOrder.CustLast.ToLower() &&
    k.CustCompany.ToLower() == thisVM.SelectedOrder.CustCompany.ToLower() &&
    k.OrderID == thisVM.SelectedOrder.OrderID && k.Bin != null && k.Used != null
                             select k).ToList();

                        if (list != null && list.Count > 0)
                        {
                            Utilities.ShowUltraPrintTransferUsage(thisVM);
                        }
                        else
                        {
                            Utilities.ShowUltraPrintUsage(thisVM);
                        }
                    }
                }
                else
                {
                    MarkCompletedConfirm(thisVM);
                }
                //if (thisVM.SelectedOrder.POSDecortionType.ToLower() == "ultraprint")
                //{
                //    bool result = Utilities.ShowUltraPrintUsage(thisVM);
                //}
                //else
                //{
                //    MarkCompletedConfirm(thisVM);
                //}
            }
            else
            {
                MarkCompletedConfirm(thisVM);
            }
        }

        private async void MarkPickedUpConfirm()
        {
            HopperViewModel thisVM = (HopperViewModel) this.DataContext;
            if (thisVM.SelectedOrder != null)
            {
                MessageDialogResult result =
                    await
                        Utilities.ShowMetroYNDialog("Confirm Picked Up",
                            "Are you sure you would like to mark this order as picked up?");
                if (result == MessageDialogResult.Affirmative)
                {
                    if (expanderContainer.Children.Count > 0)
                    {
                        foreach (Expander ex in expanderContainer.Children)
                        {
                            Controls.DesignCards.ViewCard vc = (Controls.DesignCards.ViewCard) ex.Content;
                            vc.FileSystemWatcherActivated = false;
                            if (vc.fileSystemWatcher != null) cEventHelper.RemoveAllEventHandlers(vc.fileSystemWatcher);
                        }
                    }

                    System.Threading.Thread.Sleep(500);
                    using (Entities ctx = new Entities())
                    {
                        hopperorder thisOrd =
                            (from q in ctx.hopperorders where q.ReceiptNum == thisVM.SelectedOrder.ReceiptNum select q)
                                .FirstOrDefault();
                        thisOrd.Status = "Picked Up";
                        thisOrd.StatusLID = 11;
                        ctx.SaveChanges();
                    }
                    Utilities.MarkHopperOrderPickedUp(thisVM.SelectedOrder.ReceiptNum);
                    MainViewModel mvm = (MainViewModel) App.Current.MainWindow.DataContext;
                    mvm.GoHopperCommand.Execute(null);
                }
            }
        }

        public async void MarkCompletedConfirm(HopperViewModel hvm)
        {

            //HopperViewModel thisVM = (HopperViewModel) this.DataContext;

            if (hvm.SelectedOrder != null)
            {
                MessageDialogResult result =
                    await
                        Utilities.ShowMetroYNDialog("Confirm Completion",
                            "Are you sure you would like to mark this order as completed?");
                if (result == MessageDialogResult.Affirmative)
                {
                    using (Entities ctx = new Entities())
                    {
                        hopperorder thisOrd =
                            (from q in ctx.hopperorders where q.ReceiptNum == hvm.SelectedOrder.ReceiptNum select q)
                                .FirstOrDefault();
                        thisOrd.Status = "Completed";
                        thisOrd.StatusLID = 10;
                        ctx.SaveChanges();
                    }

                    Utilities.MarkHopperOrderCompleted(hvm.SelectedOrder.ReceiptNum);
                    hvm.SelectedOrder.Status = "Completed";
                    hvm.NeedsSave = false;
                    hvm.SaveChanges();
                    grid.RefreshData();
                    hvm.NeedsSave = false;

                }
                else if (result == MessageDialogResult.SecondAuxiliary || result == MessageDialogResult.Negative)
                {
                    hvm.NeedsSave = true;
                }
            }
        }

        private void GridMouseLeft(object sender, MouseEventArgs e)
        {

        }

        private void ViewProductionSheetClick(object sender, EventArgs e)
        {
            if (this.DataContext.GetType() == typeof (HopperViewModel))
            {
                HopperViewModel thisVM = (HopperViewModel) this.DataContext;
                if (thisVM.SelectedOrder != null)
                {
                    string notespath = thisVM.SelectedOrder.JobFolderPath + @"\Invoice.pdf";
                    string notespath2 = thisVM.SelectedOrder.JobFolderPath + @"\ProductionSheet.pdf";
                    if (File.Exists(notespath))
                    {
                        Process.Start(notespath);
                    }
                    if (File.Exists(notespath2))
                    {
                        Process.Start(notespath2);
                    }
                    else
                    {
                        if (Directory.Exists(thisVM.SelectedOrder.JobFolderPath))
                        {
                            LilyReports.InvoicePlusTest inv = Utilities.GetProductionSheet(thisVM.SelectedOrder);
                            string PDFPath = thisVM.SelectedOrder.JobFolderPath + "\\ProductionSheet.pdf";
                            inv.ExportToPdf(PDFPath);
                            ViewProductionSheetClick(null, null);
                        }
                    }
                }
            }
            else if (this.DataContext.GetType() == typeof (HopperArchivedViewModel))
            {
                HopperArchivedViewModel thisVM = (HopperArchivedViewModel) this.DataContext;
                if (thisVM.SelectedOrder != null)
                {
                    string notespath = thisVM.SelectedOrder.JobFolderPath + @"\Invoice.pdf";
                    string notespath2 = thisVM.SelectedOrder.JobFolderPath + @"\ProductionSheet.pdf";
                    if (File.Exists(notespath))
                    {
                        Process.Start(notespath);
                    }
                    if (File.Exists(notespath2))
                    {
                        Process.Start(notespath2);
                    }
                }
            }
        }

        private void BeforeRefresh(object sender, CancelRoutedEventArgs e)
        {
            if (IsFullyLoaded)
            {
                UnloadChangedEvents();
            }
        }


        private void GridSelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {
            if (IsFullyLoaded)
            {
                if (this.DataContext.GetType() == typeof (HopperViewModel))
                {
                    HopperViewModel thisVM = (HopperViewModel) this.DataContext;
                    thisVM.NeedsSave = false;
                }
                else if (this.DataContext.GetType() == typeof (HopperArchivedViewModel))
                {
                    HopperArchivedViewModel thisVM = (HopperArchivedViewModel) this.DataContext;
                    thisVM.NeedsSave = false;
                }
                ReloadChangedEvents();
                ReloadDesigns();
                ReloadRTFEditor();
            }
        }

        private void DeleteClick(object sender, EventArgs e)
        {
            if (this.DataContext.GetType() == typeof (HopperViewModel))
            {
                HopperViewModel thisVM = (HopperViewModel) this.DataContext;
                Utilities.DeleteHopperOrderAndRelatedData(thisVM.SelectedOrder.ReceiptNum);
            }
            else if (this.DataContext.GetType() == typeof (HopperArchivedViewModel))
            {
                HopperArchivedViewModel thisVM = (HopperArchivedViewModel) this.DataContext;
                thisVM.NeedsSave = false;
                Utilities.DeleteOrderAndRelatedData(thisVM.SelectedOrder.ReceiptNum);
            }
            MainViewModel mvm = (MainViewModel) App.Current.MainWindow.DataContext;
            mvm.GoHopperCommand.Execute(null);
        }

        private void SaveChangesClick(object sender, EventArgs e)
        {
            if (this.DataContext.GetType() == typeof (HopperViewModel))
            {
                HopperViewModel thisVM = (HopperViewModel) this.DataContext;
                thisVM.DispatchSaveConfirmScreen();
            }
            else if (this.DataContext.GetType() == typeof (HopperArchivedViewModel))
            {
                HopperArchivedViewModel thisVM = (HopperArchivedViewModel) this.DataContext;
                thisVM.DispatchSaveConfirmScreen();
            }
        }

        private void EmailCustomer(object sender, MouseButtonEventArgs e)
        {
            Process.Start("mailto:" + EmailLabel.Content.ToString());
        }

        private void RefreshClicked(object sender, EventArgs e)
        {
            MainViewModel mvm = (MainViewModel) App.Current.MainWindow.DataContext;
            mvm.GoHopperCommand.Execute(null);
        }

        private void OpenFolderClicked(object sender, EventArgs e)
        {
            if (this.DataContext.GetType() == typeof (HopperViewModel))
            {
                HopperViewModel thisVM = (HopperViewModel) this.DataContext;
                if (thisVM.SelectedOrder != null)
                {
                    string path = thisVM.SelectedOrder.JobFolderPath;
                    if (Directory.Exists(path))
                    {
                        Process.Start(path);
                    }
                }
            }
            else if (this.DataContext.GetType() == typeof (HopperArchivedViewModel))
            {
                HopperArchivedViewModel thisVM = (HopperArchivedViewModel) this.DataContext;
                if (thisVM.SelectedOrder != null)
                {
                    string path = thisVM.SelectedOrder.JobFolderPath;
                    if (Directory.Exists(path))
                    {
                        Process.Start(path);
                    }
                }
            }

        }

        private void RunTafffy(object sender, EventArgs e)
        {
            TaFFFy.RunFileConsistencyCheck_Hopper();
            ((MainViewModel) App.Current.MainWindow.DataContext).GoHopperCommand.Execute(null);
        }

        private void HideShowCompleted(object sender, EventArgs e)
        {
            string t = HideShowText.Content.ToString();
            grid.FilterCriteria = t == "HIDE COMPLETED" ? new BinaryOperator("Status", "Completed", BinaryOperatorType.NotEqual) : null;
            HideShowText.Content = t == "HIDE COMPLETED" ? "SHOW COMPLETED" : "HIDE COMPLETED";

        }
    }
}
