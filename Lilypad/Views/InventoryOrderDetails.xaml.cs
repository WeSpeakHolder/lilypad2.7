﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using Lilypad.Model;
using Lilypad.ViewModel;

namespace Lilypad.View
{
    /// <summary>
    ///     Interaction logic for InventoryOrders.xaml
    /// </summary>
    public partial class InventoryOrderDetails : UserControl
    {
        public InventoryOrderDetails()
        {
            InitializeComponent();
            
        }

        public async void LoadDetails()
        {
            await Task.Delay(100);
            alphaorder inputorder = ((alphaorder)((MainViewModel)App.Current.MainWindow.DataContext).InventoryOrderDetailsParameter);
            var vm = (InventoryOrderDetailsViewModel)DataContext;
            using (var ctx = new Entities())
            {
                vm.CurrentOrderItems = ctx.alphaentries1.Where(x => x.IDPO == inputorder.IDPO).ToList();
                vm.ThisOrderID = inputorder.IDPO;
                vm.ThisOrderAlphaID = inputorder.IDAlpha;
            }
        }

        public InventoryOrderDetails(alphaorder inputorder)
        {
            InitializeComponent();
            var vm = (InventoryOrderDetailsViewModel)DataContext;
            using (var ctx = new Entities())
            {
                vm.CurrentOrderItems = ctx.alphaentries1.Where(x => x.IDPO == inputorder.IDPO).ToList();
                vm.ThisOrderID = inputorder.IDPO;
                vm.ThisOrderAlphaID = inputorder.IDAlpha;
            }
        }

        private void InventoryOrderDetailsLoaded(object sender, RoutedEventArgs e)
        {
            LoadDetails();
        }

        private void grid_GroupRowExpanding(object sender, RowAllowEventArgs e)
        {
        }

        private void GridSelectedItemChanged(object sender, SelectedItemChangedEventArgs e)
        {
        }

        private void SelectItem(object sender, MouseButtonEventArgs e)
        {
        }

        private void GridViewRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
        }

        private void PrintGridClicked(object sender, EventArgs e)
        {
            var preview = new DocumentPreviewWindow();
            var link = new PrintableControlLink(GridView);
            var model = new LinkPreviewModel(link);
            preview.Model = model;
            link.Landscape = true;
            link.CreateDocument(true);
            ThemeManager.SetThemeName(preview, Theme.MetropolisLightName);
            preview.ShowDialog();
        }

        private void GridDoubleClick(object sender, MouseButtonEventArgs e)
        {
        }

        private void ViewCurrentOrder(object sender, EventArgs e)
        {
            ((MainViewModel)App.Current.MainWindow.DataContext).GoInventoryOrders();
        }
    }
}