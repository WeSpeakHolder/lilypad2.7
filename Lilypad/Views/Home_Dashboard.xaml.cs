﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Windows.Media.Animation;
using DevExpress.Xpf.WindowsUI.Navigation;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Printing;
using DevExpress.XtraReports.UI;
using Lilypad.Model;
using Lilypad.ViewModel;
using Lilypad.Controls;

namespace Lilypad.Views
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Home_Dashboard : UserControl
    {
        public Home_Dashboard()
        {
            InitializeComponent();
        }

        private void GetCallList()
        {
            List<calllist> MList = new List<calllist>();
            List<calllist> OutputList = new List<calllist>();

            try
            {
                using (Entities ctx = new Entities())
                {
                    MList = (from q in ctx.calllists.Include("order").Include("hopperorder").Include("order.customer").Include("hopperorder.customer") orderby q.CallDateTime ascending select q).ToList();

                    if (MList.Count > 0)
                    {
                        foreach (calllist item in MList)
                        {
                            calllist editable = item;

                            if (item.order != null)
                            {
                                hopperorder newh = new hopperorder();
                                newh.CustFirst = item.order.customer.FirstName;
                                newh.CustLast = item.order.customer.LastName;
                                newh.CustCompany = item.order.customer.Company;
                                newh.TotalShirts = item.order.TotalShirts;
                                customer cust = new customer();
                                cust.Phone = item.order.customer.Phone;
                                newh.customer = cust;
                                editable.hopperorder = newh;
                                editable.HopperReceipt = item.order.ReceiptNum;
                                OutputList.Add(editable);
                            }
                            else
                            {
                                OutputList.Add(editable);
                            }

                            CallCard callcard = new CallCard();
                            callcard.Width = 250;
                            callcard.Height = 35;
                            callcard.Margin = new Thickness(0, 0, 0, 3);

                            if (item.Called)
                            {
                                callcard.CheckIcon.Fill = Brushes.YellowGreen;
                                callcard.CrossOutBar.Visibility = System.Windows.Visibility.Visible;
                                callcard.NameLabel.Foreground = Brushes.LightGray;
                                callcard.CallTimeLabel.Foreground = Brushes.LightGray;
                                callcard.PhoneLabel.Foreground = Brushes.LightGray;
                            }

                            if (item.ThankYou)
                            {
                                callcard.EmailIcon.Fill = Brushes.DodgerBlue;
                            }

                            string timetocallDay = "";
                            DateTime itemtime = item.CallDateTime;
                            if (itemtime.DayOfYear == DateTime.Now.DayOfYear) { timetocallDay = "Today"; }
                            else if (itemtime.DayOfYear == DateTime.Now.DayOfYear + 1) { timetocallDay = "Tomorrow"; }
                            else { timetocallDay = itemtime.ToString("ddd"); }
                            double phoneasInt = Double.Parse("0000000000");

                            CallCardViewModel ccvm = new CallCardViewModel();

                            if (item.order != null)
                            {
                                if (item.order.customer.Phone != "")
                                {
                                    string extr = item.order.customer.Phone.ToString().Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty);
                                    phoneasInt = Double.Parse(extr);
                                    ccvm.CustomerCompany = item.order.customer.Company;
                                    ccvm.CustomerName = item.order.customer.FirstName.Trim() + " " + item.order.customer.LastName.Trim();
                                    ccvm.PhoneIn = item.order.customer.Phone;
                                }
                            }

                            else if (item.hopperorder != null)
                            {
                                if (item.hopperorder.customer.Phone != "")
                                {
                                    string extr = item.hopperorder.customer.Phone.ToString().Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty);
                                    phoneasInt = Double.Parse(extr);
                                    ccvm.CustomerCompany = item.hopperorder.customer.Company;
                                    ccvm.CustomerName = item.hopperorder.CustFirst.Trim() + " " + item.hopperorder.CustLast.Trim();
                                    ccvm.PhoneIn = item.hopperorder.customer.Phone;
                                }
                            }

                            string timetocallTime = "";
                            timetocallTime = itemtime.ToString("h tt");


                            ccvm.CallCommon = timetocallDay + " by " + timetocallTime;
                            ccvm.CallID = item.UniqueID;
                            ccvm.CallTime = item.CallDateTime;
                            ccvm.IsCalled = item.Called;
                            ccvm.PhoneFormatted = String.Format("{0:(###) ###-####}", phoneasInt);
                            ccvm.ReceiptNum = Convert.ToInt32(item.ReceiptNum);

                            callcard.DataContext = ccvm;
                            CallListSP.Children.Add(callcard);

                        }
                    }

                    CallGrid.ItemsSource = OutputList;
                }
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        private void HomeLoaded(object sender, RoutedEventArgs e)
        {
            GetCallList();
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            HomeDashboardViewModel dvm = (HomeDashboardViewModel)this.DataContext;
            dvm.CalculateCounts();
            dvm.RecalculateGoals();
           // dvm.CalculateGoalsViaFRED();
            dvm.FillOrdersCollection();
            dvm.FillTasksCollection();
            SetMyOrdersLoad();
            LoadTasks();
            mvm.ChangesSaved = false;
        }

        private void LoadTasks()
        {
            HomeDashboardViewModel dvm = (HomeDashboardViewModel)this.DataContext;
            expanderContainer.Children.Clear();
            if (dvm.MyTasksCollection.Count > 0)
            {
                var tf = (from a in dvm.MyTasksCollection orderby a.TaskPriority descending, a.TaskDate ascending select a);
                foreach (task t in tf)
                {
                    Controls.TaskCardTwo tc = new Controls.TaskCardTwo(t);
                    expanderContainer.Children.Add(tc);
                }
            }
        }

        private void SetMyOrdersLoad()
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            if (mvm.LoggedInFrogger.FrogID > 0)
            {
                HomeDashboardViewModel hdvm = (HomeDashboardViewModel)this.DataContext;
                CollectionViewSource cV = (CollectionViewSource)this.FindResource("OrdersSource");
                hdvm.IsMyOrders = true;
                AllTile.Background = Brushes.Orange;
                cV.Source = hdvm.HopperOrdersCollection;
                AllClicked(AllTile, null);
            }
        }
        private void GoalChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {

        }

        private void EditGoalClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MainViewModel mvm = (MainViewModel)Application.Current.MainWindow.DataContext;
            if (mvm.LoggedInFrogger.IsAdministrator || mvm.LoggedInFrogger.IsManager || mvm.LoggedInFrogger.IsStoreOwner)
            {
                if (GoalEditor.Visibility == System.Windows.Visibility.Hidden)
                {
                    GoalEditor.Visibility = System.Windows.Visibility.Visible;
                    GoalLabel.Visibility = System.Windows.Visibility.Collapsed;
                }
                else
                {
                    GoalEditor.Visibility = System.Windows.Visibility.Hidden;
                    GoalLabel.Visibility = System.Windows.Visibility.Visible;
                }
            }
        }

        private void GoalEditor_KeyDown_1(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                HomeDashboardViewModel dvm = (HomeDashboardViewModel)this.DataContext;
                dvm.EditGoal();
             //   dvm.RecalculateGoals();
                dvm.CalculateGoalsViaFRED();
                EditGoalClick(null, null);
            }
        }

        private void ShowFiguresClicked(object sender, MouseButtonEventArgs e)
        {
            if (GoalContainer1.Height != 110)
            {
                GoalContainer1.Height = 110;
                FiguresLabel.Content = "HIDE FIGURES";
            }
            else
            {
                GoalContainer1.Height = 65;
                FiguresLabel.Content = "SHOW FIGURES";
            }

            GoalLabel.Visibility = System.Windows.Visibility.Visible;
        }

        private void CallAreaClicked(object sender, MouseButtonEventArgs e)
        {
            HomeDashboardViewModel thisVM = (HomeDashboardViewModel)this.DataContext;
            thisVM.CalculateCounts();
        }

        private void PrintCallGridClicked(object sender, EventArgs e)
        {
            PrintGrid(CallGrid);
        }

        private void PrintGrid(DevExpress.Xpf.Grid.GridControl grid)
        {
            DocumentPreviewWindow preview = new DocumentPreviewWindow();
            PrintableControlLink link = new PrintableControlLink(grid.View as DevExpress.Xpf.Printing.IPrintableControl);
            LinkPreviewModel model = new LinkPreviewModel(link);
            preview.Model = model;
            link.Landscape = true;
            link.CreateDocument(true);
            ThemeManager.SetThemeName(preview, Theme.MetropolisLightName);
            preview.ShowDialog();
        }

        private void PrintWIPGridClicked(object sender, EventArgs e)
        {
            PrintGrid(HopperGrid);
        }

        private void CallGrid_CustomUnboundColumnData(object sender, DevExpress.Xpf.Grid.GridColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                string first; string last;

                if (e.GetListSourceFieldValue("hopperorder.CustFirst") != null)
                {
                    first = e.GetListSourceFieldValue("hopperorder.CustFirst").ToString();
                    last = e.GetListSourceFieldValue("hopperorder.CustLast").ToString();
                    e.Value = first + " " + last;
                }
                else if (e.GetListSourceFieldValue("order.customer.FirstName") != null)
                {
                    first = e.GetListSourceFieldValue("order.customer.FirstName").ToString();
                    last = e.GetListSourceFieldValue("order.customer.LastName").ToString();
                    e.Value = first + " " + last;
                }
                else
                {
                    e.Value = "";
                }
            }
        }

        private bool IsMyOrders
        {
            get;
            set;
        }

        private void OrderFilterTodayClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void OrderFilterTmwClick(object sender, MouseButtonEventArgs e)
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            HomeDashboardViewModel hdvm = (HomeDashboardViewModel)this.DataContext;
            CollectionViewSource cV = (CollectionViewSource)this.FindResource("OrdersSource");
            cV.Source = hdvm.HopperOrdersCollection;
            if (hdvm.IsMyOrders)
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;
                    if (ho != null)
                    {
                        if (ho.DueDate.Date != DateTime.Now.AddDays(1).Date)
                        {
                            return false;
                        }
                        if (ho.DesignFrog != mvm.LoggedInFrogger.FrogID)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else return true;
                };
            }
            else
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;
                    if (ho != null)
                    {
                        if (ho.DueDate.Date != DateTime.Now.AddDays(1).Date)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else return true;
                };
            }
            HideWhenFilter();
        }

        private void OrderFilterAnyClick(object sender, MouseButtonEventArgs e)
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            HomeDashboardViewModel hdvm = (HomeDashboardViewModel)this.DataContext;
            CollectionViewSource cV = (CollectionViewSource)this.FindResource("OrdersSource");
            cV.Source = hdvm.HopperOrdersCollection;
            if (hdvm.IsMyOrders)
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;
                    if (ho != null)
                    {
                        if (ho.DesignFrog != mvm.LoggedInFrogger.FrogID)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else return true;
                };
            }
            else
            {
                cV.View.Filter = item =>
                {
                        return true;
                };
            }
            HideWhenFilter();
        }

        private void ShowWhoFilter(object sender, MouseButtonEventArgs e)
        {

        }

        private void ShowWhenFilter(object sender, MouseButtonEventArgs e)
        {

        }

        private void HideWhoFilter()
        {

        }

        private void HideWhenFilter()
        {

        }

        private List<SolidColorBrush> brushes;

        private void AddTaskClicked(object sender, EventArgs e)
        {
            if (addPanel.Height == 0)
            {
                addPanel.HeightAnimation(190, TimeSpan.FromMilliseconds(400));
            }
            else
            {
                addPanel.HeightAnimation(0, TimeSpan.FromMilliseconds(300));
            }
        }

        private void TitleGotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tt = sender as TextBox;
            if (tt.Text == "New Task")
            {
                tt.Clear();
                tt.Focus();
                Keyboard.Focus(tt);
            }
        }

        private void StartSPHover(object sender, MouseEventArgs e)
        {
            StackPanel sp = (StackPanel)sender;
            sp.SmoothOpacityFadeInOut(0.95, TimeSpan.FromMilliseconds(600));
        }

        private void StartSPLeave(object sender, MouseEventArgs e)
        {
            StackPanel sp = (StackPanel)sender;
            sp.SmoothOpacityFadeInOut(0.25, TimeSpan.FromMilliseconds(400));
        }

        private void TitleChanged(object sender, TextChangedEventArgs e)
        {


        }

        private void ExpandAllClicked(object sender, EventArgs e)
        {
            foreach (TaskCardTwo exx in expanderContainer.Children)
            {
                exx.Expand();
            }
        }

        private void CollapseAllClicked(object sender, EventArgs e)
        {
            foreach (TaskCardTwo exx in expanderContainer.Children)
            {
                exx.Collapse();
            }
        }

        private void ClearClicked(object sender, EventArgs e)
        {
            List<CallCard> toremove = new List<CallCard>();

            foreach (CallCard card in CallListSP.Children.OfType<CallCard>())
            {
                CallCardViewModel vm = (CallCardViewModel)card.DataContext;
                if (vm.IsCalled)
                {
                    vm.DeleteItem();
                    toremove.Add(card);
                }
            }

            if (toremove.Any())
            {
                foreach (CallCard card in toremove)
                {
                    CallListSP.Children.Remove(card);
                }
            }

        }

        private void SelectTile(DevExpress.Xpf.LayoutControl.Tile entry)
        {
            foreach (DevExpress.Xpf.LayoutControl.Tile tile in OrderTilesSP.Children.OfType<DevExpress.Xpf.LayoutControl.Tile>())
            {
                if (tile.Background != Brushes.Green)
                {
                    tile.Background = (SolidColorBrush)this.FindResource("AccentColorBrush");
                }
            }

            entry.Background = Brushes.Orange;
        }

        private void todayClicked(object sender, EventArgs e)
        {
            SelectTile(sender as DevExpress.Xpf.LayoutControl.Tile);
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            HomeDashboardViewModel hdvm = (HomeDashboardViewModel)this.DataContext;
            CollectionViewSource cV = (CollectionViewSource)this.FindResource("OrdersSource");
            cV.Source = hdvm.HopperOrdersCollection;
            if (hdvm.IsMyOrders)
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;
                    if (ho != null)
                    {
                        if (ho.Status == "Completed")
                        {
                            return false;
                        }
                        if (ho.DueDate.Date != DateTime.Now.Date)
                        {
                            return false;
                        }
                        else if (ho.DesignFrog != mvm.LoggedInFrogger.FrogID)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else return true;
                };
            }
            else
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;
                    if (ho != null)
                    {
                        if (ho.Status == "Completed")
                        {
                            return false;
                        }
                        if (ho.DueDate.Date != DateTime.Now.Date)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else return true;
                };
            }
        }

        private void ReloadActiveTileOrders()
        {
            foreach (DevExpress.Xpf.LayoutControl.Tile tile in OrderTilesSP.Children.OfType<DevExpress.Xpf.LayoutControl.Tile>())
            {
                if (tile.Background == Brushes.Orange)
                {
                    string header = tile.Header.ToString();

                    if (header == "Today") todayClicked(tile, null);
                    if (header == "Tomorrow") tomorrowClicked(tile, null);
                    if (header == "Next 3 Days") next3Clicked(tile, null);
                    if (header == "All") AllClicked(tile, null);
                    if (header == "Past Due") pastdueClicked(tile, null);
                    break;
                }
            }
        }

        private void tomorrowClicked(object sender, EventArgs e)
        {
            SelectTile(sender as DevExpress.Xpf.LayoutControl.Tile);
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            HomeDashboardViewModel hdvm = (HomeDashboardViewModel)this.DataContext;
            CollectionViewSource cV = (CollectionViewSource)this.FindResource("OrdersSource");
            cV.Source = hdvm.HopperOrdersCollection;
            if (hdvm.IsMyOrders)
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;
                    if (ho != null)
                    {
                        if (ho.Status == "Completed")
                        {
                            return false;
                        }

                        if (ho.DueDate.Date != DateTime.Now.AddDays(1).Date)
                        {
                            return false;
                        }
                        else if (ho.DesignFrog != mvm.LoggedInFrogger.FrogID)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else return true;
                };
            }
            else
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;
                    if (ho != null)
                    {
                        if (ho.Status == "Completed")
                        {
                            return false;
                        }

                        if (ho.DueDate.Date != DateTime.Now.AddDays(1).Date)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else return true;
                };
            }
        }

        private void next3Clicked(object sender, EventArgs e)
        {
            SelectTile(sender as DevExpress.Xpf.LayoutControl.Tile);
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            HomeDashboardViewModel hdvm = (HomeDashboardViewModel)this.DataContext;
            CollectionViewSource cV = (CollectionViewSource)this.FindResource("OrdersSource");
            cV.Source = hdvm.HopperOrdersCollection;
            if (hdvm.IsMyOrders)
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;
                    if (ho != null)
                    {
                        if (ho.Status == "Completed")
                        {
                            return false;
                        }

                        if (ho.DesignFrog != mvm.LoggedInFrogger.FrogID)
                        {
                            return false;
                        }

                        if (ho.DueDate.Date != DateTime.Now.AddDays(1).Date || ho.DueDate.Date != DateTime.Now.AddDays(2).Date || ho.DueDate.Date != DateTime.Now.AddDays(3).Date)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else return false;
                };
            }
            else
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;
                    if (ho != null)
                    {
                        if (ho.Status == "Completed")
                        {
                            return false;
                        }
                        if (ho.DueDate.Date == DateTime.Now.AddDays(1).Date || ho.DueDate.Date == DateTime.Now.AddDays(2).Date || ho.DueDate.Date == DateTime.Now.AddDays(3).Date)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else return true;
                };
            }
        }

        private void AllClicked(object sender, EventArgs e)
        {
            SelectTile(sender as DevExpress.Xpf.LayoutControl.Tile);
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            HomeDashboardViewModel hdvm = (HomeDashboardViewModel)this.DataContext;
            CollectionViewSource cV = (CollectionViewSource)this.FindResource("OrdersSource");
            cV.Source = hdvm.HopperOrdersCollection;
            if (hdvm.IsMyOrders)
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;
                    if (ho != null)
                    {
                        if (ho.Status == "Completed")
                        {
                            return false;
                        }

                        if (ho.DesignFrog != mvm.LoggedInFrogger.FrogID)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else return false;
                };
            }
            else
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;

                    if (ho != null)
                    {
                        if (ho.Status == "Completed")
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                        else
                        {
                            return false;
                        }
                };
            }
        }

        private void CheckedUnchecked(object sender, RoutedEventArgs e)
        {
            ReloadActiveTileOrders();
        }

        private void pastdueClicked(object sender, EventArgs e)
        {
            SelectTile(sender as DevExpress.Xpf.LayoutControl.Tile);
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;
            HomeDashboardViewModel hdvm = (HomeDashboardViewModel)this.DataContext;
            CollectionViewSource cV = (CollectionViewSource)this.FindResource("OrdersSource");
            cV.Source = hdvm.HopperOrdersCollection;
            if (hdvm.IsMyOrders)
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;
                    if (ho != null)
                    {
                        if (ho.Status == "Completed")
                        {
                            return false;
                        }
                        if (ho.DueDate > DateTime.Now)
                        {
                            return false;
                        }
                        else if (ho.DesignFrog != mvm.LoggedInFrogger.FrogID)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else return true;
                };
            }
            else
            {
                cV.View.Filter = item =>
                {
                    hopperorder ho = item as hopperorder;
                    if (ho != null)
                    {
                        if (ho.Status == "Completed")
                        {
                            return false;
                        }
                        if (ho.DueDate > DateTime.Now)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else return true;
                };
            }
        }

        private void ChangeGlobalClicked(object sender, EventArgs e)
        {
            if (IsLoaded)
            {
                if (AssignIcon.Icon == "appbar_people")
                {
                    AssignIcon.Icon = "appbar_globe_wire";
                    EverybodyLabel.Opacity = 1;
                    TaskFrogSelector.Opacity = 0;
                    TaskFrogSelector.IsEnabled = false;
                }
                else
                {
                    AssignIcon.Icon = "appbar_people";
                    EverybodyLabel.Opacity = 0;
                    TaskFrogSelector.Opacity = 1;
                    TaskFrogSelector.IsEnabled = true;
                }
            }
        }

        private void ChangePriorityClick(object sender, MouseButtonEventArgs e)
        {

                var priority = PriorityAbbrevLabel.Content;

                if (priority == "S")
                {
                    PriorityBG.Fill = Brushes.Orange;
                    PriorityAbbrevLabel.Content = "H";
                    PriorityLabel.Content = "High";
                }
                else if (priority == "H")
                {
                    PriorityBG.Fill = Brushes.Red;
                    PriorityAbbrevLabel.Content = "R";
                    PriorityLabel.Content = "Rush";
                }
                else
                {
                    PriorityBG.Fill = Brushes.YellowGreen;
                    PriorityAbbrevLabel.Content = "S";
                    PriorityLabel.Content = "Standard";
                }
        }

        private void TaskTextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(TaskTitleBox.Text) || string.IsNullOrEmpty(TaskDescripBox.Text))
            {
                FinalAddButton.Background = Brushes.LightGray;
                FinalAddButton.IsEnabled = false;
                FinalAddButton.Circle = true;
            }
            else
            {
                FinalAddButton.Background = Brushes.YellowGreen;
                FinalAddButton.IsEnabled = true;
                FinalAddButton.Circle = true;
            }
        }

        private void AddTaskFinalClicked(object sender, EventArgs e)
        {
            AddNewTask();
        }

        private void AddNewTask()
        {
            MainViewModel mvm = (MainViewModel) App.Current.MainWindow.DataContext;
            HomeDashboardViewModel dvm = (HomeDashboardViewModel) this.DataContext;
            int thisFrogID = mvm.LoggedInFrogger.FrogID;

            task nt = new task();
            nt.TaskTitle = TaskTitleBox.Text;
            nt.TaskDescrip = TaskDescripBox.Text;
            nt.FrogCreated = thisFrogID;
            nt.IsCompleted = false;
            nt.IsWorking = true;
            nt.TaskDate = DateTime.Now;
            nt.FolderPath = string.Empty;
            nt.BackgroundColor = string.Empty;
            nt.Related_Ticket = string.Empty;
            if (PriorityBG.Fill == Brushes.YellowGreen) { nt.TaskPriority = 1; }
            else if (PriorityBG.Fill == Brushes.Orange) { nt.TaskPriority = 2; }
            else { nt.TaskPriority = 3;}
            if (AssignIcon.Icon == "appbar_globe_wire")
            {
                nt.IsGlobal = true;
                nt.FrogAssigned = 0;
            }
            else
            {
                nt.IsGlobal = false;
                nt.FrogAssigned = (int)TaskFrogSelector.SelectedValue;
            }
            try
            {
                using (Entities ctx = new Entities())
                {
                    ctx.tasks.Add(nt);
                    ctx.SaveChanges();
                }
                TaskTitleBox.Clear();
                TaskDescripBox.Clear();
                dvm.FillTasksCollection();
                LoadTasks();
                addPanel.HeightAnimation(0, TimeSpan.FromMilliseconds(300));

            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }
    }
}
