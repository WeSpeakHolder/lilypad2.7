﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.ViewModel;
using Lilypad.Model;
using Ookii.Dialogs.Wpf;
using DevExpress.Xpf.WindowsUI.Navigation;

namespace Lilypad.Views
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class Sandbox : UserControl, INavigationAware
    {
        public Sandbox()
        {
            InitializeComponent();
            ipTester = new BackgroundWorker();
            ipTester.DoWork += ipTester_DoWork;
            ipTester.RunWorkerCompleted += ipTester_RunWorkerCompleted;
            ipTester.ProgressChanged += ipTester_ProgressChanged;
            ipTester.WorkerSupportsCancellation = false;
            ipTester.WorkerReportsProgress = true;
        }

        void ipTester_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Output.Content = e.UserState.ToString();
        }

        void ipTester_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (string.IsNullOrEmpty(FoundServer))
            {
                Output.Content = "No servers found";
            }
            else
            {
                Output.Content = "MySQL Server found at " + FoundServer;
            }
        }

        public string FoundServer;

        void ipTester_DoWork(object sender, DoWorkEventArgs e)
        {
            string localIP = Utilities.GetLocalIPAddress();
            string subIP = localIP.Substring(0, localIP.LastIndexOf('.') + 1);

            for (int ip = 1; ip < 251; ip++)
            {
                string tosearch = subIP + ip.ToString();

                ipTester.ReportProgress(0, "Pinging MySQL Server at " + tosearch);

                IPEndPoint host = new IPEndPoint(IPAddress.Parse(tosearch), 3306);

                if (TimeOutSocket.Connect(host, 10))
                {
                    FoundServer = tosearch;
                    // Save found server IP into application settings and reset config
                    break;
                }
            }
        }


        private void CollapseAll(object sender, MouseButtonEventArgs e)
        {
            MainViewModel mvm = (MainViewModel)App.Current.MainWindow.DataContext;

            if (mvm.DetailsShown)
            {
                mvm.DetailsShown = false;
            }
            else
            {
                mvm.DetailsShown = true;
            }
        }

        public BackgroundWorker ipTester;

        private void ExpandAll(object sender, MouseButtonEventArgs e)
        {
            ipTester.RunWorkerAsync();
        }

        private void Dialog(object sender, MouseButtonEventArgs e)
        {
            Utilities.InstallUpdateSQLScript();
        }

        private void Divide(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string result;
                int num = 0;
                result = (15 / num).ToString();
            }
            catch (Exception ex)
            {
                new Error(ex);
            }
        }

        public int desnum = 1;

        public void NavigatedFrom(DevExpress.Xpf.WindowsUI.Navigation.NavigationEventArgs e)
        {

        }

        public void NavigatedTo(DevExpress.Xpf.WindowsUI.Navigation.NavigationEventArgs e)
        {

        }

        public void NavigatingFrom(NavigatingEventArgs e)
        {
            MainWindow main = (MainWindow)Application.Current.MainWindow;
            MainViewModel mvm = (MainViewModel)main.DataContext;
            foreach (string item in mvm.DesignCardList)
            {
                main.UnregisterName(item);
            }
            mvm.DesignCardList.Clear();
        }

        private void CheckDB(object sender, MouseButtonEventArgs e)
        {
            if (Utilities.DBisOnline())
            {
                dbchecktile.Fill = Brushes.Green;
            }
            else
            {
                dbchecktile.Fill = Brushes.Red;
            }
        }

        private void CheckDB2(object sender, MouseButtonEventArgs e)
        {
            if (Utilities.CloudisOnline())
            {
                dbchecktile2.Fill = Brushes.Green;
            }
            else
            {
                dbchecktile2.Fill = Brushes.Red;
            }
        }

        private void UnzipTest(object sender, MouseButtonEventArgs e)
        {
            if (Utilities.InitialDatabaseInstall())
            {
                ziptile.Fill = Brushes.Green;
            }
            else
            {
                ziptile.Fill = Brushes.Red;
            }
        }

        private void copy1(object sender, MouseButtonEventArgs e)
        {
            string kk = @"C:\Users\Tyler\Documents\Lilypad\templates\Front.cdr";
            Output.Content = Interop.CorelX7.ExtractThumbnail(kk);
        }

        private void copy2(object sender, MouseButtonEventArgs e)
        {

        }

        private void sourceClicked(object sender, EventArgs e)
        {
            StackPanel sp = Utilities.FindParentByType<StackPanel>(((DependencyObject)sender));
            TextBox tb = (TextBox)sp.FindChildren<TextBox>(true).First();
            var dialog = new VistaFolderBrowserDialog();
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.SelectedPath))
            {
                tb.Text = dialog.SelectedPath.ToString();
            }
        }

        private void copy3(object sender, MouseButtonEventArgs e)
        {
            Output.Content = Utilities.PasswordGenerator(9, true);
        }
    }
}
