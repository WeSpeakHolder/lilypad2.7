﻿using System;
using System.IO;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Shapes;
using System.Threading;
using System.Threading.Tasks;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Lilypad.Properties;
using MahApps.Metro;
using Lilypad.ViewModel;

namespace Lilypad
{
    public class QuietError
    {
        public QuietError(Exception ex, [CallerLineNumber] int lineNo = 0, [CallerMemberName] string callNm = "",
            [CallerFilePath] string callPt = "")
        {
            WriteExceptionToLog(ex, lineNo, callNm, callPt);
            var error = "Error Encountered: " + ex.Message;
          //  UI.SetStatusFlash(error, TimeSpan.FromSeconds(5), "Ready");
        }


        public void WriteExceptionToLog(Exception ex, int lineNo, string callNm, string callPt)
        {
            var error = new StringBuilder();
            error.AppendLine("ERROR REPORT --------------------------------------------------------");
            error.AppendLine("Version:           " + "Lilypad V" + Settings.Default.App_Version);
            error.AppendLine("Date:              " + DateTime.Now.ToString("MM/dd/yy h:mm:ss"));
            error.AppendLine("Computer name:     " + Environment.MachineName);
            error.AppendLine("OS:                " + "Unknown"); // Utilities.ThisComputerOSNameAndArch
            error.AppendLine("Caller member:     " + callNm);
            error.AppendLine("Caller file:       " + callPt.Substring(callPt.LastIndexOf('\\') + 1));
            error.AppendLine("Line:              " + lineNo);
            error.AppendLine("");

            error.AppendLine("Exception Message:   ");
            error.Append(ex.Message);
            error.AppendLine("");

            error.AppendLine("Exception Source:   ");
            error.Append(ex.Source);
            error.AppendLine("");

            if (ex.InnerException != null)
            {
                error.AppendLine("Inner Exception:   ");
                error.Append(ex.InnerException.Message);
                error.AppendLine("");
            }

            if (ex is DbEntityValidationException)
            {
                var evv = (DbEntityValidationException)ex;
                foreach (var evt in evv.EntityValidationErrors)
                {
                    error.AppendLine("EntityValidationErrors: Type: " + evt.Entry.Entity.GetType().Name + " | State: " +
                                     evt.Entry.State);
                    foreach (var evx in evt.ValidationErrors)
                    {
                        error.AppendLine("--- Property: " + evx.PropertyName + " | Error: " + evx.ErrorMessage);
                    }
                }
            }

            var Path = @"C:\LilypadLogs";

            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }

            File.AppendAllText(System.IO.Path.Combine(Path, "errorlog.txt"), error.ToString());
        }
    }
    public class SilentError
    {
        public SilentError(Exception ex, [CallerLineNumber] int lineNo = 0, [CallerMemberName] string callNm = "",
            [CallerFilePath] string callPt = "")
        {
            WriteExceptionToLog(ex, lineNo, callNm, callPt);
        }


        public void WriteExceptionToLog(Exception ex, int lineNo, string callNm, string callPt)
        {
            var error = new StringBuilder();
            error.AppendLine("ERROR REPORT --------------------------------------------------------");
            error.AppendLine("Version:           " + "Lilypad V" + Settings.Default.App_Version);
            error.AppendLine("Date:              " + DateTime.Now.ToString("MM/dd/yy h:mm:ss"));
            error.AppendLine("Computer name:     " + Environment.MachineName);
            error.AppendLine("OS:                " + "Unknown"); // Utilities.ThisComputerOSNameAndArch
            error.AppendLine("Caller member:     " + callNm);
            error.AppendLine("Caller file:       " + callPt.Substring(callPt.LastIndexOf('\\') + 1));
            error.AppendLine("Line:              " + lineNo);
            error.AppendLine("");

            error.AppendLine("Exception Message:   ");
            error.Append(ex.Message);
            error.AppendLine("");

            error.AppendLine("Exception Source:   ");
            error.Append(ex.Source);
            error.AppendLine("");

            if (ex.InnerException != null)
            {
                error.AppendLine("Inner Exception:   ");
                error.Append(ex.InnerException.Message);
                error.AppendLine("");
            }

            if (ex is DbEntityValidationException)
            {
                var evv = (DbEntityValidationException)ex;
                foreach (var evt in evv.EntityValidationErrors)
                {
                    error.AppendLine("EntityValidationErrors: Type: " + evt.Entry.Entity.GetType().Name + " | State: " +
                                     evt.Entry.State);
                    foreach (var evx in evt.ValidationErrors)
                    {
                        error.AppendLine("--- Property: " + evx.PropertyName + " | Error: " + evx.ErrorMessage);
                    }
                }
            }

            var Path = @"C:\LilypadLogs";

            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }

            File.AppendAllText(System.IO.Path.Combine(Path, "errorlog.txt"), error.ToString());
        }
    }

    public class Error
    {
        public Exception CaughtEx;
        public SimpleDialog Dialog;

        public Error(Exception ex, [CallerLineNumber] int lineNo = 0, [CallerMemberName] string callNm = "",
            [CallerFilePath] string callPt = "")
        {
            CaughtEx = ex;
            WriteExceptionToLog(ex, lineNo, callNm, callPt);
            ShowError(ex, lineNo, callNm, callPt);
        }

        public Error()
        {
        }

        public string FullReport { get; set; }

        public void ShowError(Exception ex, int lineNo, string callNm, string callPt)
        {
            Application.Current.Dispatcher.Invoke(async () =>
            {
                var wind = (MetroWindow)Application.Current.MainWindow;
                wind.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
                var newError = new Dialogs.Error(ex, FullReport);
                Dialog = new SimpleDialog();
                Dialog.Background = Brushes.Transparent;
                Dialog.DialogBody = newError;
                await wind.ShowMetroDialogAsync(Dialog);
            });
        }

        private string GetExceptionTypeStack(Exception e)
        {
            if (e.InnerException != null)
            {
                var message = new StringBuilder();
                message.AppendLine(GetExceptionTypeStack(e.InnerException));
                message.AppendLine("   " + e.GetType());
                return (message.ToString());
            }
            return "   " + e.GetType();
        }

        private string GetExceptionMessageStack(Exception e)
        {
            if (e.InnerException == null) return "   " + e.Message;

            var message = new StringBuilder();
            message.AppendLine(GetExceptionMessageStack(e.InnerException));
            message.AppendLine("   " + e.Message);
            return (message.ToString());
        }

        private string GetExceptionCallStack(Exception e)
        {
            var errBuild = new StringBuilder();

            errBuild.AppendLine(" ");
            errBuild.AppendLine("------ BEGIN CALLSTACK ------");

            if (e.InnerException != null)
            {
                var sTe = new StackTrace(e.InnerException, true);

                var sTef = sTe.GetFrames();
                if (sTef != null)
                {
                    for (var i = 0; i < sTef.Count(); i++)
                    {
                        var f = sTef[i];
                        errBuild.AppendLine("--- Inner Exception " + i++ + " of (" + sTef.Count() + ")");
                        errBuild.AppendLine("- Type: " + f.GetType());
                        errBuild.AppendLine("- Method: " + f.GetMethod());
                        errBuild.AppendLine(" ");
                    }
                }
            }

            var sT = new StackTrace(e, true);

            var sTf = sT.GetFrames();
            if (sTf != null)
            {
                for (var i = 0; i < sTf.Count(); i++)
                {
                    var f = sTf[i];
                    errBuild.AppendLine("--- Exception " + i++ + " of (" + sTf.Count() + ")");
                    errBuild.AppendLine("- Type: " + f.GetType());
                    errBuild.AppendLine("- Method: " + f.GetMethod());
                    errBuild.AppendLine(" ");
                }
            }

            errBuild.AppendLine(" ");
            errBuild.AppendLine("------ END CALLSTACK ------");
            return errBuild.ToString();
        }

        public void WriteExceptionToLog(Exception ex, int lineNo, string callNm, string callPt)
        {
            var error = new StringBuilder();
            error.AppendLine("ERROR REPORT --------------------------------------------------------");
            error.AppendLine("Version:           " + "Lilypad V" + Settings.Default.App_Version);
            error.AppendLine("Date:              " + DateTime.Now.ToString("MM/dd/yy h:mm:ss"));
            error.AppendLine("Computer name:     " + Environment.MachineName);
            error.AppendLine("OS:                " + "Unknown"); //Utilities.ThisComputerOSNameAndArch
            error.AppendLine("Caller member:     " + callNm);
            error.AppendLine("Caller file:       " + callPt.Substring(callPt.LastIndexOf('\\') + 1));
            error.AppendLine("Line:              " + lineNo);
            error.AppendLine("");

            if (CaughtEx is DbEntityValidationException)
            {
                var evv = (DbEntityValidationException)ex;
                foreach (var evt in evv.EntityValidationErrors)
                {
                    error.AppendLine("EntityValidationErrors: Type: " + evt.Entry.Entity.GetType().Name + " | State: " +
                                     evt.Entry.State);
                    foreach (var evx in evt.ValidationErrors)
                    {
                        error.AppendLine("--- Property: " + evx.PropertyName + " | Error: " + evx.ErrorMessage);
                    }
                }
            }

            error.AppendLine("Exception classes:   ");
            error.Append(GetExceptionTypeStack(ex));
            error.AppendLine("");
            error.AppendLine("Exception messages: ");
            error.Append(GetExceptionMessageStack(ex));

            error.AppendLine("");
            error.AppendLine("Stack Traces:");
            error.Append(GetExceptionCallStack(ex));
            error.AppendLine("");

            var Path = @"C:\LilypadLogs";

            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }

            File.AppendAllText(System.IO.Path.Combine(Path, "errorlog.txt"), error.ToString());
            FullReport = error.ToString();
        }
    }
}