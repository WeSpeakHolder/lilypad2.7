﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;

namespace Lilypad
{
    public class Logger
    {
        public Exception caughtEx;

        public Logger(string input, [CallerLineNumber] int lineNo = 0, [CallerMemberName] string callNm = "",
            [CallerFilePath] string callPt = "")
        {
            LogEntry(input, lineNo, callNm, callPt);
        }

        public static void Log(string input, [CallerLineNumber] int lineNo = 0, [CallerMemberName] string callNm = "",
            [CallerFilePath] string callPt = "")
        {
            LogEntry(input, lineNo, callNm, callPt);
        }

        private static void LogEntry(string input, int lineNo, string callNm, string callPt)
        {
            Console.WriteLine(input);
            Debug.WriteLine(input);
            try
            {
                WriteToFile(input, lineNo, callNm, callPt);
            }
            catch (Exception ex)
            {
                // Do nothing. 
            }
        }

        private static void WriteToFile(string input, int lineNo, string callNm, string callPt)
        {
            var output = DateTime.Now.ToString("MM/dd/yy h:mm:sstt") + ":: " + input + " |ln:" + lineNo + "|nm:" +
                         callNm + Environment.NewLine;

            var Path = @"C:\LilypadLogs";

            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }

            File.AppendAllText(System.IO.Path.Combine(Path, "activity.txt"), output);
        }
    }
}