alter table ultraprintorderdata add column `TransferedFrom` int(6) DEFAULT NULL;

ALTER TABLE `ultraprintorderdata` 
DROP COLUMN `IsLastUpdated`,
DROP COLUMN `IsTransfered`;

alter table ultraprintorderdata add column `GridUsed` int(11) DEFAULT NULL;
alter table ultraprintorderdata add column `GridBin` varchar(100) DEFAULT NULL;