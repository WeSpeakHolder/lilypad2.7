DROP TABLE IF EXISTS `preorderstatus`;
CREATE TABLE IF NOT EXISTS `preorderstatus` (
  `StatusID` int(2) NOT NULL AUTO_INCREMENT,
  `StatusText` varchar(250) NOT NULL,
  `StatusSecondary` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`StatusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `preorderstatus`
--

INSERT INTO `preorderstatus` (`StatusID`, `StatusText`, `StatusSecondary`) VALUES
(1, 'Awaiting artwork from customer', 'Awaiting Art'),
(2, 'Awaiting further order details from customer', 'Awaiting Details'),
(3, 'Awaiting a proof confirmation from customer', 'Awaiting Proof Confirmation'),
(4, 'Awaiting final response from customer', 'Awaiting Final Response'),
(5, 'Awaiting payment from customer', 'Awaiting Payment'),
(6, 'Working on artwork for customer', 'Working on Art'),
(7, '**Customer requires artwork proof', 'Cust. Requires Proofs'),
(8, '**Customer requires a cost estimate', 'Cust. Requires Estimate'),
(9, '**Customer requires proofs and an estimate', 'Cust. Requires Proofs & Estimate'),
(10, 'Completed and became an order', 'Completed'),
(11, 'Completed but did not become an order', 'Declined'),
(12, 'Other - see preorder notes', 'Other'),
(13, 'New Pre-Order', 'New Pre-Order');

DROP TABLE IF EXISTS `preorderitems`;
CREATE TABLE IF NOT EXISTS `preorderitems` (
  `UniqueID` int(11) NOT NULL AUTO_INCREMENT,
  `DesignID` int(7) DEFAULT NULL,
  `PreorderID` int(11) DEFAULT NULL,
  `StockMfg` varchar(50) DEFAULT NULL,
  `StockCode` varchar(50) DEFAULT NULL,
  `StockColor` varchar(100) DEFAULT NULL,
  `StockSize` varchar(25) DEFAULT NULL,
  `StockQty` int(6) DEFAULT NULL,
  `StockInvStatus` varchar(50) DEFAULT NULL,
  `StockInvStatusID` int(2) DEFAULT NULL,
  `StockDescrip` varchar(250) DEFAULT NULL,
  `StockAlphaID` varchar(30) DEFAULT NULL,
  `StockItemCost` decimal(8,2) DEFAULT NULL,
  `EstimatedCost` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`UniqueID`),
  KEY `PreorderID` (`PreorderID`,`StockInvStatusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `preorderdesigndetails`;
CREATE TABLE IF NOT EXISTS `preorderdesigndetails` (
  `DesignID` int(7) NOT NULL AUTO_INCREMENT,
  `PreorderID` int(7) NOT NULL,
  `DesignNum` int(2) NOT NULL,
  `DesignFolderPath` varchar(1000) NOT NULL,
  `DesignDescription` varchar(255) NOT NULL,
  `Notes` mediumtext NOT NULL,
  `PrintToFile` tinyint(1) NOT NULL DEFAULT '0',
  `SizeToGarment` tinyint(1) NOT NULL DEFAULT '0',
  `AP_YOUTH` tinyint(1) NOT NULL DEFAULT '0',
  `AP_Front` tinyint(1) NOT NULL DEFAULT '0',
  `AP_Back` tinyint(1) NOT NULL DEFAULT '0',
  `AP_LeftSide` tinyint(1) NOT NULL DEFAULT '0',
  `AP_RightSide` tinyint(1) NOT NULL DEFAULT '0',
  `AP_LeftSleeve` tinyint(1) NOT NULL DEFAULT '0',
  `AP_RightSleeve` tinyint(1) NOT NULL DEFAULT '0',
  `AP_FrontBottom` tinyint(1) NOT NULL DEFAULT '0',
  `AP_BackBottom` tinyint(1) NOT NULL DEFAULT '0',
  `AP_Yoke` tinyint(1) NOT NULL DEFAULT '0',
  `AP_Collar` tinyint(1) NOT NULL DEFAULT '0',
  `AP_FLC` tinyint(1) NOT NULL DEFAULT '0',
  `AP_FRC` tinyint(1) NOT NULL DEFAULT '0',
  `AP_FLCW` tinyint(1) NOT NULL DEFAULT '0',
  `AP_FRCW` tinyint(1) NOT NULL DEFAULT '0',
  `AP_Mousepad` tinyint(1) NOT NULL,
  `AP_SqCoaster` tinyint(1) NOT NULL,
  `AP_ClCoaster` tinyint(1) NOT NULL,
  `AP_Bandana` tinyint(1) NOT NULL,
  `AP_GolfFlag` tinyint(1) NOT NULL,
  `AP_GolfTowel` tinyint(1) NOT NULL,
  `AP_Koozie` tinyint(1) NOT NULL,
  `AP_Cap` tinyint(1) NOT NULL,
  `AP_Apron` tinyint(1) NOT NULL,
  `AP_Puzzle` tinyint(1) NOT NULL,
  `AP_ToteBag` tinyint(1) NOT NULL,
  `AP_WineBag` tinyint(1) NOT NULL,
  `PrintProcess` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`DesignID`),
  KEY `PreorderID` (`PreorderID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `preorders`;
CREATE TABLE IF NOT EXISTS `preorders` (
  `PreorderID` int(8) NOT NULL AUTO_INCREMENT,
  `First` varchar(250) DEFAULT NULL,
  `Last` varchar(250) DEFAULT NULL,
  `Company` varchar(500) DEFAULT NULL,
  `Process` int(2) DEFAULT NULL,
  `Status` varchar(500) DEFAULT NULL,
  `StatusID` int(2) DEFAULT NULL,
  `BinNum` varchar(10) DEFAULT NULL,
  `OrderFrog` int(2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `DueDateBegin` datetime DEFAULT NULL,
  `DueDateEnd` datetime DEFAULT NULL,
  `ShirtsCount` int(6) DEFAULT NULL,
  `PhoneNum` varchar(50) DEFAULT NULL,
  `EmailAddr` varchar(500) DEFAULT NULL,
  `QuoteID` int(8) DEFAULT NULL,
  `SearchTags` varchar(500) DEFAULT NULL,
  `JobFolderPath` text,
  `GarmentNotes` text,
  `ArtworkNotes` text,
  `MainNotes` text,
  PRIMARY KEY (`PreorderID`),
  KEY `StatusID` (`StatusID`),
  KEY `OrderFrog` (`OrderFrog`),
  KEY `QuoteID` (`QuoteID`),
  KEY `Process` (`Process`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

  DROP TABLE IF EXISTS `archivedpreorders`;
CREATE TABLE IF NOT EXISTS `archivedpreorders` (
  `PreorderID` int(8) NOT NULL AUTO_INCREMENT,
  `First` varchar(250) DEFAULT NULL,
  `Last` varchar(250) DEFAULT NULL,
  `Company` varchar(500) DEFAULT NULL,
  `Process` int(2) DEFAULT NULL,
  `Status` varchar(500) DEFAULT NULL,
  `StatusID` int(2) DEFAULT NULL,
  `BinNum` varchar(10) DEFAULT NULL,
  `OrderFrog` int(2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `DueDateBegin` datetime DEFAULT NULL,
  `DueDateEnd` datetime DEFAULT NULL,
  `ShirtsCount` int(6) DEFAULT NULL,
  `PhoneNum` varchar(50) DEFAULT NULL,
  `EmailAddr` varchar(500) DEFAULT NULL,
  `QuoteID` int(8) DEFAULT NULL,
  `SearchTags` varchar(500) DEFAULT NULL,
  `JobFolderPath` text,
  `GarmentNotes` text,
  `ArtworkNotes` text,
  `MainNotes` text,
  `CompletionDate` datetime DEFAULT NULL,
  `CompletionResult` varchar(250) DEFAULT NULL,
  `CompletionResultID` int(2) DEFAULT NULL,
  `CompletedReceiptID` int(8) DEFAULT NULL,
  PRIMARY KEY (`PreorderID`),
  KEY `StatusID` (`StatusID`),
  KEY `Process` (`Process`),
  KEY `QuoteID` (`QuoteID`),
  KEY `OrderFrog` (`OrderFrog`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `preorders`
  ADD CONSTRAINT `froggerLookup` FOREIGN KEY (`OrderFrog`) REFERENCES `froggers` (`FrogID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `processLookup` FOREIGN KEY (`Process`) REFERENCES `printprocesses` (`ProcessID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `statusLookup` FOREIGN KEY (`StatusID`) REFERENCES `preorderstatus` (`StatusID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

  ALTER TABLE `archivedpreorders`
  ADD CONSTRAINT `archiveFroggersLookup` FOREIGN KEY (`OrderFrog`) REFERENCES `froggers` (`FrogID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `archiveProcessLookup` FOREIGN KEY (`Process`) REFERENCES `printpositions` (`UniqueID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `archiveStatusLookup` FOREIGN KEY (`StatusID`) REFERENCES `preorderstatus` (`StatusID`) ON DELETE NO ACTION ON UPDATE NO ACTION;