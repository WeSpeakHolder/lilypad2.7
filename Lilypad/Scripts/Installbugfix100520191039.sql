DROP TABLE IF EXISTS `ultraprintorderdata`;
CREATE TABLE `ultraprintorderdata` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `OrderID` int(11) NOT NULL,
  `Location` varchar(100) DEFAULT NULL,
  `Qty` int(11) DEFAULT NULL,
  `Type` varchar(1000) DEFAULT NULL,
  `DesignName` varchar(1000) DEFAULT NULL,
  `size` varchar(50) DEFAULT NULL,
  `Used` int(11) DEFAULT NULL,
  `Bin` varchar(100) DEFAULT NULL,
  `receiptnum` int(6) NOT NULL,
  `cusomerId` int(6) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `completeddate` datetime DEFAULT NULL,
  `expiredate` datetime DEFAULT NULL,
  `IsEditable` bit(1) DEFAULT b'1',
  `CustLastName` nvarchar(1000) DEFAULT NULL,
  `CustCompany` nvarchar(1000) DEFAULT NULL,
  `Vendor` nvarchar(1000) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;

alter table ultraprintorderdata add column `IsDeleted` bit(1) DEFAULT b'0';


alter table alphacurrentorder add column `IsArchiveable` bit(1) DEFAULT b'0';
alter table alphacurrentorder add column `DecorationType` nvarchar(100) ;

UPDATE alphacurrentorder ao
join hopperorders ho on ao.ReceiptNum = ho.ReceiptNum 
join printprocesses pp on ho.PrintProcess = pp.ProcessID 
SET ao.DecorationType = pp.ProcessName where ao.ReceiptNum = ho.ReceiptNum && ao.IDUnique > 0;

ALTER TABLE `printprocesses` 
ADD COLUMN `ProcessOrderId` INT(11) NULL AFTER `ProcessName`;

UPDATE `printprocesses` SET ProcessName='DTG' WHERE ProcessID = 1;
UPDATE `printprocesses` SET ProcessName='Mixed' WHERE ProcessID = 6;

ALTER TABLE `printprocesses` AUTO_INCREMENT = 7;


Insert into `printprocesses` (ProcessName) values ('DTG-W'),('Dye Sublimation'),('Poly Pretreat'),('Bling'),( 'Printable Vinyl'),('Blank Shirts'),('Digital Heat Transfer'),('None');

UPDATE `printprocesses` SET ProcessOrderId =  
CASE
  WHEN ProcessName='DTG' THEN 1
  WHEN ProcessName='Embroidery' THEN 2
  WHEN ProcessName='UltraPrint' THEN 3
  WHEN ProcessName='Screenprint' THEN 4
  WHEN ProcessName='Vinyl' THEN 5
  WHEN ProcessName='DTG-W' THEN 6
  WHEN ProcessName='Dye Sublimation' THEN 7
  WHEN ProcessName='Poly Pretreat' THEN 8
  WHEN ProcessName='Bling' THEN 9
  WHEN ProcessName='Mixed' THEN 10
  WHEN ProcessName='Printable Vinyl' THEN 11
  WHEN ProcessName='Blank Shirts' THEN 12
  WHEN ProcessName='Digital Heat Transfer' THEN 13
  WHEN ProcessName='None' THEN 14
  
END
WHERE ProcessID > 0;



ALTER TABLE `hopperorders` 
ADD COLUMN `POSDecortionType` Varchar(100) NULL AFTER `IsPreorder`;

update `hopperorders` set POSDecortionType = '' where OrderID > 0;



DROP TABLE IF EXISTS `artworkstatus`;
CREATE TABLE `artworkstatus` (
  `StatusID` INT(11) NOT NULL AUTO_INCREMENT,
  `Status` VARCHAR(100) NULL,
  PRIMARY KEY (`StatusID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

INSERT INTO `artworkstatus` (`Status`) VALUES ('Complete'),('Proof Requested'), ('Proof Sent'), ('Proof Approved / Proceed'), ('Working On Artwork'), ('In Process');


DROP TABLE IF EXISTS `inventorydatastatus`;
CREATE TABLE `inventorydatastatus` (
  `StatusID` INT(11) NOT NULL AUTO_INCREMENT,
  `Status` VARCHAR(100) NULL,
  PRIMARY KEY (`StatusID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


INSERT INTO `inventorydatastatus` (`Status`) VALUES ('In-Stock / Proceed'),('Partial In-Stock / Awaiting Inv.'), ('Awaiting Inventory');


ALTER TABLE `hopperorders` 
ADD COLUMN `ArtworkStatus` INT(11) NOT NULL DEFAULT '1'  AFTER `POSDecortionType`,
ADD COLUMN `InventoryStatus` INT(11) NOT NULL DEFAULT '1'  AFTER `ArtworkStatus`,
ADD INDEX `ArtworkStatus` (`ArtworkStatus` ASC),
ADD INDEX `InventoryStatus` (`InventoryStatus` ASC);
ALTER TABLE `hopperorders` 
ADD CONSTRAINT `hopperorders_ibfk_25`
  FOREIGN KEY (`ArtworkStatus`)
  REFERENCES `artworkstatus` (`StatusID`)
ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `hopperorders_ibfk_30`
  FOREIGN KEY (`InventoryStatus`)
  REFERENCES `inventorydatastatus` (`StatusID`)
  ON DELETE NO ACTION ON UPDATE NO ACTION;



ALTER TABLE `orders` 
ADD COLUMN `ArtworkStatus` INT(11) NOT NULL DEFAULT '1'  AFTER `Irregulars`,
ADD COLUMN `InventoryStatus` INT(11) NOT NULL DEFAULT '1'  AFTER `ArtworkStatus`,
ADD INDEX `ArtworkStatus` (`ArtworkStatus` ASC),
ADD INDEX `InventoryStatus` (`InventoryStatus` ASC);
ALTER TABLE `orders` 
ADD CONSTRAINT `orders_ibfk_25`
  FOREIGN KEY (`ArtworkStatus`)
  REFERENCES `artworkstatus` (`StatusID`)
ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `orders_ibfk_30`
  FOREIGN KEY (`InventoryStatus`)
  REFERENCES `inventorydatastatus` (`StatusID`)
  ON DELETE NO ACTION ON UPDATE NO ACTION;


INSERT INTO `setupstatus` (`StatusID`, `Status`) VALUES ('9', 'In Process');

INSERT INTO `status` (`StatusID`, `Status`) VALUES ('12', 'In Process');

