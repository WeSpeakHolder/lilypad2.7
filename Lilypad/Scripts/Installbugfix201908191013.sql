alter table ultraprintorderdata add column `IsTransfered` bit(1) DEFAULT b'0';

alter table ultraprintorderdata add column `CustFirstName` nvarchar(1000) DEFAULT NULL;

alter table ultraprintorderdata add column `IsLastUpdated` bit(1) DEFAULT b'0';