CREATE TABLE IF NOT EXISTS `productionstatus` (
  `StatusID` int(2) NOT NULL,
  `Status` varchar(40) NOT NULL,
  PRIMARY KEY (`StatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `productionstatus` (`StatusID`, `Status`) VALUES
(1, 'Awaiting Artwork'),
(2, 'Awaiting Art Approval'),
(3, 'Awaiting Payment'),
(4, 'Awaiting Inventory'),
(5, 'Working On Artwork'),
(6, 'On Hold'),
(7, 'Out For Production'),
(8, 'Awaiting Ultraprints'),
(9, 'Ready To Print');

CREATE TABLE IF NOT EXISTS `setupstatus` (
  `StatusID` int(2) NOT NULL,
  `Status` varchar(40) NOT NULL,
  PRIMARY KEY (`StatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `setupstatus` (`StatusID`, `Status`) VALUES
(1, 'Awaiting Artwork'),
(2, 'Awaiting Art Approval'),
(3, 'Awaiting Payment'),
(4, 'Awaiting Inventory'),
(5, 'Working On Artwork'),
(6, 'On Hold'),
(7, 'Out For Production'),
(8, 'Awaiting Ultraprints');