CREATE TABLE IF NOT EXISTS `ioqueue` (
  `OperationID` int(11) NOT NULL AUTO_INCREMENT,
  `OperationType` varchar(15) NOT NULL,
  `Source` varchar(500) NOT NULL,
  `Destination` varchar(500) NOT NULL,
  `Attributes` varchar(500) NOT NULL,
  `LastAttempt` datetime NOT NULL,
  `OperationSuccessful` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`OperationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;