﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Threading;
using DevExpress.Xpf.Core;
using MahApps.Metro.Controls;
using Lilypad.Properties;

namespace Lilypad
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Process thisProc = Process.GetCurrentProcess();
            if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            {
         //       MessageBox.Show("Lilypad is already running");
         //       Application.Current.Shutdown();
         //       return;
            }

          //  Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
          //  AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
         //   Dispatcher.UnhandledException += Dispatcher_UnhandledException;
                base.OnStartup(e);

                Utilities.UpdateConfig();
        }

        private void Dispatcher_UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Logger.Log("Unhandled error occured in UIDispatcher. Attempting termination prevention.");
            new Error(e.Exception);
            e.Handled = true;
        }

        private void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Logger.Log("Unhandled error occured in UIDispatcher. Attempting termination prevention.");
            new Error(e.Exception);
            e.Handled = true;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logger.Log("Fatal error occured in CurrentDomain.UnhandledException. Lilypad will now terminate.");
            new Error((Exception)e.ExceptionObject);
        }
    }
}
